<?php

$SettingsToothFairyOnlineForm = [
    'periodDays'=> 14,  // показывать график на ближайшие "n" дней
    'periodMinutes' => 30,  // задаем сколько будет длится прием в минутах // указывать число кратное 0 или 5
    'keyReCaptcha' => '6LdNEU4cAAAAAABzf21jgk7VtVO_Q0q-qO3RMwml', // ключ для добавления в HTML-код
    'secretReCaptcha' => '6LdNEU4cAAAAAAM9ik3sfNTkWzeZ0J1DYDgS9Dba',  // секретный ключ для google reCaptcha2
    'SMTP' => [     // Настройки SMTP
        'smtpServer' => 'mail.adm.tools',     //  Сервер
        'userName' => 'mail',    //  Логин пользователя
        'userPassword' => 'pass',    //  Пароль пользователя
        'smtpPort' => '465',     //  Порт
        'smtpSecure' => 'ssl'   // Тип шифрования (TLS,ssl)
    ],
    'emailClinic' => [  // Эмайлы, кому присылать письма (Записывать через запятую в кавычках)
        'support@violadent.com',
//        'seocraft@medtehnika.org.ua',
    ]
];

<?php
require_once "Connection.php";

 class ChiefMapModulePosition
{
	/**
    * @var float
    */
    var $LONGTITUDE;
	
	/**
    * @var float
    */
    var $LATITUDE;
	/**
    * @var int
    */
	var $COUNTT;
}

 class ChiefMapModuleClinicPosition
{
	/**
    * @var float
    */
    var $LONGTITUDE;
	
	/**
    * @var float
    */
    var $LATITUDE;
	/**
    * @var String
    */
	var $NAME;
}


class ChiefMapModuleData
{
	/**
    * @var $ChiefMapModuleData[]
    */ 
	var $POSITIONS;
	/**
    * @var int
    */
	var $ALLCOUNT = 0;
	/**
    * @var int
    */
	var $FINDCOUNT = 0;
	/**
    * @var int
    */
	var $NOTFINDCOUNT = 0;
}

class ChiefMapModule
{	 
    /**
     * @param ChiefMapModuleData $p1
	 * @param ChiefMapModulePosition $p2
	 * @param ChiefMapModuleClinicPosition $p3
    */

    public function registertypes($p1,$p2,$p3)
    {}
	/** 
	* @return ChiefMapModulePosition
	*/   
    public function getPositionClinics() 
    {
		$data = new ChiefMapModuleData();
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select t1.NAME, t1.LONGTITUDE, t1.LATITUDE, 
						t2.NAME, t2.LONGTITUDE, t2.LATITUDE
						from CLINICREGISTRATIONDATA t1, SUBDIVISION t2
						WHERE (t1.ID is not null) and (t2.ID is not null)";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$check=0;
		while ($row = ibase_fetch_row($result))
		{	
			if ($check==0)
			{
				$check=1;
				$obj = new ChiefMapModuleClinicPosition();
				$obj->NAME=$row[0];
				$obj->LONGTITUDE=$row[1];
				$obj->LATITUDE=$row[2];
				$data->POSITIONS[]=$obj;
			}
			$obj1 = new ChiefMapModuleClinicPosition();
			$obj1->NAME=$row[3];
			$obj1->LONGTITUDE=$row[4];
			$obj1->LATITUDE=$row[5];
			$data->POSITIONS[]=$obj1;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);			
		

		return $data;
	}
	
	
	/** 
	* @return ChiefMapModuleData
	*/   
    public function getMarkings() 
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select LONGTITUDE, LATITUDE from PATIENTS
						
						where (LONGTITUDE is not null) and (LATITUDE is not null)
						and (LONGTITUDE!=0) and (LATITUDE!=0)
						ORDER BY LONGTITUDE, LATITUDE";
		
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$data = new ChiefMapModuleData();
		$data->FINDCOUNT=0;
		$PREV_LONGTITUDE=0;
        $PREV_LATITUDE=0;
		while ($row = ibase_fetch_row($result))
		{	
            if(($row[0] == $PREV_LONGTITUDE) && ($row[1] == $PREV_LATITUDE))
            {
                $obj->COUNTT++;
            }
            else
            {
                $obj = new ChiefMapModulePosition();
                $obj->LONGTITUDE=$row[0];
                $obj->LATITUDE=$row[1];
                $obj->COUNTT=1;
                $data->POSITIONS[] = $obj;              
            }
            $PREV_LONGTITUDE = $row[0];
            $PREV_LATITUDE = $row[1];
            $data->FINDCOUNT++;    
          
			
		}	
		
		
		$QueryText = "select count(ID) FROM PATIENTS WHERE (LONGTITUDE is null) or (LONGTITUDE=0)";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		while ($row = ibase_fetch_row($result))
		{	
			$data->NOTFINDCOUNT=$row[0];	
		}	
		$data->ALLCOUNT=$data->NOTFINDCOUNT+$data->FINDCOUNT;
				
		ibase_commit($trans);
		
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $data; 
    }
}

?>
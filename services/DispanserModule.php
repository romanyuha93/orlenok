<?php
require_once "Connection.php";
require_once "Utils.php";


class DispanserModule_Task
{
    /**
     * @var string
     */ 
    var $distask_id;
    /**
     * @var string
     */ 
    var $patient_id;
    
        /**
         * @var string
         */ 
        var $patient_fname;
        /**
         * @var string
         */ 
        var $patient_lname;
        /**
         * @var string
         */ 
        var $patient_sname;
        /**
         * @var string
         */ 
        var $patient_cardnum;
        /**
         * @var DispanserModule_mobilephone[]
         */ 
        var $patient_mobiles;
    
    /**
     * @var string
     */ 
    var $distask_date;
    
    /**
     * @var string
     */
     var $task_id;
     
        /**
         * @var string
         */
         var $task_date;
        /**
         * @var string
         */
         var $task_begin_time;
        /**
         * @var string
         */
         var $task_end_time;
        /**
         * @var int
         */
         var $task_isVisited;
     
     /**
     * @var string
     */
     var $distask_comment;
     
     /**
     * @var string
     */
     var $doctor_id;
     
        /**
         * @var string
         */ 
        var $doctor_fname;
        /**
         * @var string
         */ 
        var $doctor_lname;
        
        
     /**
     * @var string
     */
     var $disptype_id;
     
        /**
         * @var string
         */ 
        var $disptype_label;
        /**
         * @var int
         */ 
        var $disptype_color;
        
            
     /**
     * @var boolean
     */
     var $smsstatus;
}

class DispanserModule_mobilephone
{
    /**
    * @var string
    */
    var $id; 
    /**
    * @var string
    */
    var $parent_fk; 
    /**
    * @var string
    */
    var $phone; 
    /**
    * @var int
    */
    var $isActive; 
    /**
    * @var string
    */
    var $comments; 
}

class DispanserModule_Tasks
{
    /**
    * @var DispanserModule_Task[]
    */
    var $remined; 
    /**
     * @var int
     */
    var $reminedCount; 
    /**
    * @var DispanserModule_Task[]
    */
    var $complete;
    /**
     * @var int
     */
    var $completeCount; 
}

class DispanserModule_Doctor
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var string
     */
    var $name;
    /**
     * @var string
     */
    var $lname;
    /**
     * @var string
     */
    var $fname;
    /**
     * @var int
     */
    var $healthtype;
}

class DispanserModule_Patient
{
    /**
     * @var string
     */
    var $name;
    /**
     * @var string
     */
    var $lname;
    /**
     * @var string
     */
    var $fname;
    /**
     * @var string
     */
    var $cardnum;
}


class DispanserModule_Disptype
{
    /**
     * @var string
     */
     var $id;
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $color;
}


class DispanserModule_PatientRecord
{
    
    /**
     * @var string
     */
    var $patient_id;
    /**
     * @var string
     */
    var $patient_sname;
    /**
     * @var string
     */
    var $patient_lname;
    /**
     * @var string
     */
    var $patient_fname;
    /**
     * @var string
     */
    var $patient_cardnum;
    /**
    * @var DispanserModule_mobilephone[]
    */
    var $mobiles; 
    //$obj->mobiles = $this->getMobilePhones($obj->ID, false, $trans);
    
    /**
    * @var DispanserModule_PatientDispancerRecord[]
    */
    var $dispTasks;
}

class DispanserModule_PatientDispancerRecord
{
    /**
     * @var string
     */
    var $doctor_id;
    /**
     * @var string
     */
    var $doctor_sname;
    /**
     * @var string
     */
    var $doctor_lname;
    /**
     * @var string
     */
    var $doctor_fname;
    
    
    /**
     * @var string
     */
    var $task_id;
    /**
     * @var int
     */
    var $task_factofvisit;
    /**
     * @var int
     */
     var $task_noticed;
    
    /**
    * @var DispanserModule_Disptype
    */
    var $dispanceType;
    /**
     * @var string
     */
    var $dispance_id;
    /**
     * @var string
     */
    var $dispance_date;
    /**
     * @var string
     */
    var $dispance_comment;
    /**
     * @var string
     */
    var $task_date;
    
        /**
     * @var boolean
     */
     var $smsstatus;
}


class DispanserModule
{	 
     /**
     * @param DispanserModule_Task $p1
     * @param DispanserModule_mobilephone $p2
     * @param DispanserModule_Tasks $p3
     * @param DispanserModule_Doctor $p4
     * @param DispanserModule_Disptype $p5
     * @param DispanserModule_PatientDispancerRecord $p6
     * @param DispanserModule_mobilephone $p7
     * @param DispanserModule_PatientRecord $p8
     * @return void
     */

    public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8)
    {
    }
    
    /**
     * @return DispanserModule_Doctor[]
     */
    public function getDoctors()//sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            //doctors
            $QueryText = "SELECT ID, SHORTNAME, HEALTHTYPE FROM DOCTORS WHERE (DATEWORKEND is null) or (DATEWORKEND >= 'now')";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            
            $diags = array();
            $doc = new DispanserModule_Doctor();
            $doc->id = "empty";
            $doc->name = '-';
            $doctors[] = $doc;
                
            while ($row = ibase_fetch_row($result))
            {
                $doc = new DispanserModule_Doctor();
                $doc->id = $row[0];
                $doc->name = $row[1];
                $doc->healthtype = $row[2];
                $doctors[] = $doc;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $doctors;
    }
    
     
    /**
     * @return DispanserModule_Disptype[]
     */
    public function getDisptypes()//sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            //doctors
            $QueryText = "select ID, DESCRIPTION, COLOR from PatientCardDictionaries where TYPE=4 order by NUMBER";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            
            $diags = array();
            $disp = new DispanserModule_Disptype();
            $disp->id = "empty";
            $disp->name = '-';
            $disp->color = 0;
            $disps[] = $disp;
                
            while ($row = ibase_fetch_row($result))
            {
                $disp = new DispanserModule_Disptype();
                $disp->id = $row[0];
                $disp->name = $row[1];
                $disp->color = $row[2];
                $disps[] = $disp;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $disps;
    }
    
    /**
     * @param string $patinet_id
     * @return DispanserModule_Patient
     */
    public function getPatientData($patinet_id)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            //doctors
            $QueryText = "select    patients.firstname,
                                    patients.lastname,
                                    patients.CARDNUMBER,
                                    patients.CARDBEFORNUMBER,
                                    patients.CARDAFTERNUMBER
                       from patients
                       where patients.id = $patinet_id";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            
            
            $row = ibase_fetch_row($result);
            $pat = new DispanserModule_Patient();
            $pat->name = $row[1]." ".$row[0];
            $pat->fname = $row[0];
            $pat->lname = $row[1];
            $pat->cardnum = $row[3].strval($row[2]).$row[4];
                
            ibase_free_query($query);
            ibase_free_result($result);
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $pat;
    }
    
    /** 
 	* @param string $patient_id
 	* @param string $doctor_id
 	* @param string $task_comment
    * 
 	* @param int $taskEachPeriod
 	* @param string $taskModePeriod
 	* @param int $taskCount
    * 
    * @param string $disptype_id
    * @param boolean $smsstatus
    * 
 	* @return boolean
	**/  
    public function addDispancerTask($patient_id, $doctor_id, $task_comment, $taskEachPeriod, $taskModePeriod, $taskCount, $disptype_id, $smsstatus)
    {
        $connection = new Connection();  
        $connection->EstablishDBConnection();		
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$setDisp_string = "execute procedure SETDISPANCERPATIENT($patient_id, 1)";
        $setDisp_query = ibase_prepare($trans, $setDisp_string);
		ibase_execute($setDisp_query);
        
        for ($i = 1; $i <= $taskCount; $i++) 
        {
            $distasks_sql = "insert into tasksdispanser(
                                    tasksdispanser.patient_id,
                                    tasksdispanser.taskdate,
                                    tasksdispanser.task_comment,
                                    tasksdispanser.doctor_id,
                                    tasksdispanser.disptype_fk,
                                    tasksdispanser.smsstatus)
                                    values
                                    ($patient_id, ";
            if($taskModePeriod != "week")
            {
                $periods = $taskEachPeriod * $i;
                $distasks_sql .= "dateadd($periods $taskModePeriod to current_date), ";
            }
            else
            {
                $daysWeek = $taskEachPeriod * 7 * $i;
                $distasks_sql .= "dateadd($daysWeek day to current_date), ";
            }
            
            $distasks_sql .= "'$task_comment', ";
            
            if(($doctor_id != null)&&($doctor_id != "")&&($doctor_id != "empty"))
            {
                $distasks_sql .= "$doctor_id, ";
            }
            else
            {
                $distasks_sql .= "null, ";
            }
            
            
            if(($disptype_id != null)&&($disptype_id != "")&&($disptype_id != "empty"))
            {
                $distasks_sql .= "$disptype_id, ";
            }
            else
            {
                $distasks_sql .= "null, ";
            }
            $distasks_sql.=$smsstatus?'1)':'0)';
            $distasks_query = ibase_prepare($trans, $distasks_sql);
            $distasks_result=ibase_execute($distasks_query);
            
            ibase_free_query($distasks_query);
        }
        
        $result = ibase_commit($trans);
		$connection->CloseDBConnection();
        
        return $result;		
    }
    
	/** 
 	* @param int $daysRemined
 	* @return DispanserModule_Tasks
	**/  		
	public function getDispancerTasks($daysRemined)
	{   
			$connection = new Connection();  
			$connection->EstablishDBConnection();		
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
            $distasks_sql = "select
                    tasksdispanser.id,
                    tasksdispanser.patient_id,
                       patients.firstname,
                       patients.lastname,
                       patients.CARDNUMBER,
                       patients.CARDBEFORNUMBER,
                       patients.CARDAFTERNUMBER,
                    
                    tasksdispanser.taskdate,
                    tasksdispanser.task_id,
                       tasks.taskdate,
                       tasks.beginoftheinterval,
                       tasks.endoftheinterval,
                       tasks.factofvisit,
                    
                    tasksdispanser.task_comment,
                    tasksdispanser.doctor_id,
                       doctors.firstname,
                       doctors.lastname,
                    patients.middlename,

                    patientcarddictionaries.id,
                    patientcarddictionaries.description,
                    patientcarddictionaries.color,

                    tasksdispanser.smsstatus
                    
                    from tasksdispanser
                    inner join patients
                    on patients.id = tasksdispanser.patient_id
                    left join tasks
                    on tasks.id = tasksdispanser.task_id
                    left join doctors
                    on doctors.id = tasksdispanser.doctor_id
                    left join patientcarddictionaries
                    on patientcarddictionaries.id = tasksdispanser.disptype_fk
                    
                    
                    where tasksdispanser.taskdate > current_date
                    and tasksdispanser.taskdate <= dateadd($daysRemined day to current_date)";
    
        $distasks_query = ibase_prepare($trans, $distasks_sql);
        $distasks_result=ibase_execute($distasks_query);
        
        $result = new DispanserModule_Tasks;
        
        $reminedCount = 0;
        $completeCount = 0;
        
    
		while ($row = ibase_fetch_row($distasks_result))
		{ 
            $obj = new DispanserModule_Task;
            $obj->distask_id = $row[0];
            $obj->patient_id = $row[1];
            $obj->patient_fname = $row[2];
            $obj->patient_lname = $row[3];
            $obj->patient_cardnum = $row[5].strval($row[4]).$row[6];
            $obj->patient_mobiles = $this->getMobilePhones($obj->patient_id, $trans);
            $obj->distask_date = $row[7];
            $obj->task_id = $row[8];
            $obj->task_date = $row[9];
            $obj->task_begin_time = $row[10];
            $obj->task_end_time = $row[11];
            $obj->task_isVisited = $row[12];
            $obj->distask_comment = $row[13];
            if($row[14])
            {
                $obj->doctor_id = $row[14];
                $obj->doctor_fname = $row[15];
                $obj->doctor_lname = $row[16];
            }
            else
            {
                $obj->doctor_id = null;
                $obj->doctor_fname = '';
                $obj->doctor_lname = ''; 
            }
            $obj->patient_sname = $row[17];
            $obj->disptype_id = $row[18];
            $obj->disptype_label = $row[19];
            $obj->disptype_color = $row[20];
            $obj->smsstatus = $row[21];
            
            if($obj->task_id != null)
            {
                $result->complete[] = $obj;
                $completeCount++;
            }
            else
            {
                $result->remined[] = $obj;
                $reminedCount++;
            }
		}	
        $result->reminedCount = $reminedCount;
        $result->completeCount = $completeCount;
        
        ibase_free_query($distasks_query);
        ibase_free_result($distasks_result);
        
        ibase_commit($trans);
		$connection->CloseDBConnection();
        
        return $result;		
	}
    
   /** 
 	* @param boolean $smsstatus
 	* @param string $distaskId
    * 
 	* @return boolean
	**/      
    public function updateDispancerTasksSMSStatus($smsstatus, $distaskId)
	{
	    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update tasksdispanser
                        set tasksdispanser.smsstatus = ".($smsstatus?"1":"0")." 
                        where tasksdispanser.id = $distaskId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /** 
 	* @param string $date
 	* @param string $distaskId
    * 
 	* @return boolean
	**/      
    public function updateDispancerTasksDate($date, $distaskId)
	{
	    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update tasksdispanser
                        set tasksdispanser.taskdate = '$date'
                        where tasksdispanser.id = $distaskId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    /** 
 	* @param string $comnt
 	* @param string $distaskId
    * 
 	* @return boolean
	**/      
    public function updateDispancerTasksComment($comnt, $distaskId)
	{
	    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update tasksdispanser
                        set tasksdispanser.task_comment = '".safequery($comnt)."'
                        where tasksdispanser.id = $distaskId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    /** 
 	* @param string $type_fk
 	* @param string $distaskId
    * 
 	* @return boolean
	**/      
    public function updateDispancerTasksType($type_fk, $distaskId)
	{
	    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update tasksdispanser
                        set tasksdispanser.DISPTYPE_FK = ";
        if($type_fk != "empty" && $type_fk != null)
        {
            $QueryText .= $type_fk;
        }
        else
        {
            $QueryText .= "null ";
        }
        $QueryText .= "where tasksdispanser.id = $distaskId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /** 
 	* @param string $distaskId
 	* @param string $taskId
    * 
 	* @return boolean
	**/      
    public function addTaskKeyToDispancerTask($distaskId, $taskId)
	{
	    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update tasksdispanser
                        set tasksdispanser.task_id = $taskId
                        where tasksdispanser.id = $distaskId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    /** 
 	* @param string $distaskId
 	* @param string $patientId
    * 
 	* @return boolean
	**/      
    public function deleteDispancerTask($distaskId, $patientId)
	{
	    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from tasksdispanser where tasksdispanser.id = $distaskId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        
		$setDisp_string = "execute procedure CHECKDISPANCERPATIENT($patientId)";
        $setDisp_query = ibase_prepare($trans, $setDisp_string);
		ibase_execute($setDisp_query);
        
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @param string $searchText
    * @param string $dispType_id
    * @param string $doc_id
	* @return DispanserModule_PatientRecord[]
	*/ 
    public function getDispancerPatients($startDate, $endDate, $searchText, $dispType_id, $doc_id)
    {
        
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        $dispancerRecords_sql = "select
                                    patients.id,
                                    patients.cardbefornumber,
                                    patients.cardnumber,
                                    patients.cardafternumber,
                                    patients.lastname,
                                    patients.firstname,
                                    patients.middlename,
                            
                                    doctors.id,
                                    doctors.lastname,
                                    doctors.firstname,
                                    doctors.middlename,
                            
                                    tasks.factofvisit,
                            
                                    patientcarddictionaries.description,
                                    patientcarddictionaries.color,
                            
                                    tasksdispanser.id,
                                    tasksdispanser.task_id,
                                    tasksdispanser.taskdate,
                                    tasksdispanser.task_comment,
                                    tasksdispanser.disptype_fk,
                                    
                                    tasks.taskdate,
                                    tasks.noticed,
                                    tasksdispanser.smsstatus 
                            from tasksdispanser
                            
                            inner join patients
                            on patients.id = tasksdispanser.patient_id
                            left join doctors
                            on doctors.id = tasksdispanser.doctor_id
                            left join tasks
                            on tasks.id = tasksdispanser.task_id
                            left join patientcarddictionaries
                            on patientcarddictionaries.id = tasksdispanser.disptype_fk
                            
                            where tasksdispanser.patient_id is null ";
        
        // get patinets id with filters START
        $patientids_sql =
			"select distinct(tasksdispanser.patient_id) from tasksdispanser
            
            inner join patients
            on patients.id = tasksdispanser.patient_id
            
            where tasksdispanser.taskdate >= '$startDate' and tasksdispanser.taskdate <= '$endDate' ";
        if($searchText != "" && $searchText != null)	
        {
            $patientids_sql .= "and (lower( patients.LASTNAME||' '||patients.FIRSTNAME||' '||patients.MIDDLENAME) starting lower('$searchText')
                or lower(patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER) containing lower('$searchText')) ";
        }         
	    if($dispType_id != null && $dispType_id != "empty" && $dispType_id != "")	
        {
            $patientids_sql .= "and tasksdispanser.disptype_fk = $dispType_id ";
        }       
	    if($doc_id != null && $doc_id != "empty" && $doc_id != "")	
        {
            $patientids_sql .= "and tasksdispanser.DOCTOR_ID = $doc_id ";
        }
        
//        file_put_contents("C:/tmp.txt",file_get_contents("C:/tmp.txt")."\n".$patientids_sql);
   	    $patientids_query = ibase_prepare($trans, $patientids_sql);
        $patientids_result = ibase_execute($patientids_query);
		while ($row = ibase_fetch_row($patientids_result))
		{ 
            $dispancerRecords_sql .= "or (tasksdispanser.patient_id = $row[0] and tasksdispanser.taskdate >= '$startDate' and tasksdispanser.taskdate <= '$endDate') ";
		}	
        $dispancerRecords_sql .= "order by patients.lastname, patients.firstname, patients.middlename, patients.id, tasksdispanser.taskdate ";
        ibase_free_query($patientids_query);
        ibase_free_result($patientids_result);
        // get patinets id with filters END
        
       //file_put_contents("C:/tmp.txt",file_get_contents("C:/tmp.txt")."\n".$dispancerRecords_sql);
        // get dispancer records START
   	    $dispancerRecords_query = ibase_prepare($trans, $dispancerRecords_sql);
        $dispancerRecords_result = ibase_execute($dispancerRecords_query);
        $dispancerRecords = array();
        
        $tmp_patient_id = "new";
        $obj = new DispanserModule_PatientRecord();
            
        while ($row = ibase_fetch_row($dispancerRecords_result))
		{ 
            if($tmp_patient_id != $row[0])
            {
                if($tmp_patient_id != "new")
                    $dispancerRecords[] = $obj;
                
                $tmp_patient_id = $row[0];
                $obj = new DispanserModule_PatientRecord();
                $obj->patient_id = $row[0];
                $obj->mobiles = $this->getMobilePhones($obj->patient_id, $trans);
                $obj->patient_cardnum = $row[2].strval($row[1]).$row[3];
                $obj->patient_lname = $row[4];
                $obj->patient_fname = $row[5];
                $obj->patient_sname = $row[6];
                 
                $dispRecord = new DispanserModule_PatientDispancerRecord();
                if($row[7])
                {
                    $dispRecord->doctor_id = $row[7];                   
                    $dispRecord->doctor_lname = $row[8];                   
                    $dispRecord->doctor_fname = $row[9];                   
                    $dispRecord->doctor_sname = $row[10];
                }
                else
                {              
                    $dispRecord->doctor_lname = '';                   
                    $dispRecord->doctor_fname = '';                   
                    $dispRecord->doctor_sname = ''; 
                }
                $dispRecord->task_id = $row[15];
                $dispRecord->task_factofvisit = $row[11];
                    if($row[18])
                    {
                        $dType = new DispanserModule_Disptype();   
                        $dType->id = $row[18];
                        $dType->name = $row[12];
                        $dType->color = $row[13];
                        $dispRecord->dispanceType = $dType;    
                    }                              
                $dispRecord->dispance_id = $row[14];
                $dispRecord->dispance_date = $row[16];
                $dispRecord->dispance_comment = $row[17];
                $dispRecord->task_date = $row[19];
                $dispRecord->task_noticed = $row[20];
                $dispRecord->smsstatus = $row[21];
                $obj->dispTasks[] = $dispRecord;
                
            }
            else
            {
                
                $dispRecord = new DispanserModule_PatientDispancerRecord();
                if($row[7])
                {
                    $dispRecord->doctor_id = $row[7];                   
                    $dispRecord->doctor_lname = $row[8];                   
                    $dispRecord->doctor_fname = $row[9];                   
                    $dispRecord->doctor_sname = $row[10];
                }
                else
                {              
                    $dispRecord->doctor_lname = '';                   
                    $dispRecord->doctor_fname = '';                   
                    $dispRecord->doctor_sname = ''; 
                }
                $dispRecord->task_id = $row[15];
                $dispRecord->task_factofvisit = $row[11];
                    if($row[18])
                    {
                        $dType = new DispanserModule_Disptype();   
                        $dType->id = $row[18];
                        $dType->name = $row[12];
                        $dType->color = $row[13];
                        $dispRecord->dispanceType = $dType;    
                    }                                                
                $dispRecord->dispance_id = $row[14];
                $dispRecord->dispance_date = $row[16];
                $dispRecord->dispance_comment = $row[17];
                $dispRecord->task_date = $row[19];
                $dispRecord->task_noticed = $row[20];
                $dispRecord->smsstatus = $row[21];
                $obj->dispTasks[] = $dispRecord;
            }
		}
        if($tmp_patient_id != "new")
                    $dispancerRecords[] = $obj;
        ibase_commit($trans);
        ibase_free_query($dispancerRecords_query);
        ibase_free_result($dispancerRecords_result);
        // get dispancer records END
        return $dispancerRecords;
    }
    
    
    /** 
 	* @param string $patient_id
    * @return DispanserModule_mobilephone[]
	*/ 
    private function getMobilePhones($patient_id, $trans)
    {
         $mobile_sql =
			"select patientsmobile.id,
                patientsmobile.parent_fk,
                patientsmobile.mobilenumber,
                patientsmobile.senderactive,
                patientsmobile.comments
            from patientsmobile
                where patientsmobile.patient_fk = $patient_id
            order by patientsmobile.senderactive desc, patientsmobile.id desc";
        			
        $mobile_query = ibase_prepare($trans, $mobile_sql);
        $mobile_result=ibase_execute($mobile_query);
        $rows = array();
		while ($row = ibase_fetch_row($mobile_result))
		{ 
            $obj = new DispanserModule_mobilephone;
            $obj->id = $row[0];
            $obj->parent_fk = $row[1];
            $obj->phone = $row[2];
            if($row[3] == 1)
            {
                $obj->isActive = true;
            }
            else
            {
                $obj->isActive = false; 
            }
            $obj->comments = $row[4];
            $rows[] = $obj;
		}	
        ibase_free_query($mobile_query);
        ibase_free_result($mobile_result);
        return $rows;
    }
    
    
    
}

?>

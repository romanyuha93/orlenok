<?php

require_once "Connection.php";
require_once "Utils.php";
 
class AssistantsModuleAssistant
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $TIN;
    
    /**
    * @var string
    */
    var $FIRSTNAME;
     
    /**
    * @var string
    */
    var $MIDDLENAME;
     
    /**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $FULLNAME;
     
    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var string
    */
    var $BIRTHDAY; 

    /**
    * @var string
    */
    var $SEX;     

    /**
    * @var string
    */
    var $DATEWORKSTART;

    /**
    * @var string
    */
    var $DATEWORKEND;

    /**
    * @var string
    */
    var $MOBILEPHONE;

    /**
    * @var string
    */
    var $HOMEPHONE;

    /**
    * @var string
    */
    var $EMAIL;

    /**
    * @var string
    */
    var $SKYPE;

    /**
    * @var string
    */
    var $HOMEADRESS; 

    /**
    * @var string
    */
    var $POSTALCODE;
    
     /**
    * @var string
    */
    var $STATE;  
    
    /**
    * @var string
    */
    var $REGION;
    
    /**
    * @var string
    */
    var $CITY;  
    
    
    /**
    * @var string
    */
    var $BARCODE;  
    
    /**
     * @var int
     */
    var $isFired;
    
    /**
     * @var string
     */
    var $fingerid;
}

class AssistantsModule
{	
    /**
     * @param AssistantsModuleAssistant $p1
     */
    public function registertypes($p1)
    {}
    
    
    /** 
 	* @param string $lastname
    * @param int $firedFilter
	* @param string $subdivId
	* @return AssistantsModuleAssistant[]
	*/    
    public function getAssistantsFiltered($lastname, $firedFilter, $subdivId)
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, TIN, FIRSTNAME, MIDDLENAME, LASTNAME, FULLNAME, SHORTNAME, BIRTHDAY,
			SEX, DATEWORKSTART, DATEWORKEND, MOBILEPHONE, HOMEPHONE,
			EMAIL, SKYPE, HOMEADRESS, POSTALCODE, STATE, REGION, CITY, BARCODE, isfired, FINGERID from ASSISTANTS where (ID is not null)";
		if ($lastname!="")	
		  { $QueryText=$QueryText." and (lower(LASTNAME) starting lower('$lastname') or BARCODE = '$lastname')";}
        if($firedFilter == 0)
		  $QueryText=$QueryText." and isFired = 0";
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $QueryText .= " and SUBDIVISIONID = $subdivId ";
		  //$QueryText=$QueryText." and (LASTNAME containing '$lastname')"; }				
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new AssistantsModuleAssistant;
			$obj->ID = $row[0];
			$obj->TIN = $row[1];
			$obj->FIRSTNAME = $row[2];
			$obj->MIDDLENAME = $row[3];
			$obj->LASTNAME = $row[4];
			$obj->FULLNAME = $row[5];
			$obj->SHORTNAME = $row[6];
			$obj->BIRTHDAY = $row[7];
			$obj->SEX = $row[8];
			$obj->DATEWORKSTART = $row[9];
			$obj->DATEWORKEND = $row[10];
			$obj->MOBILEPHONE = $row[11];
			$obj->HOMEPHONE = $row[12];
			$obj->EMAIL = $row[13];
			$obj->SKYPE = $row[14];
			$obj->HOMEADRESS = $row[15];
			$obj->POSTALCODE = $row[16];
			$obj->STATE = $row[17];
			$obj->REGION = $row[18];
			$obj->CITY = $row[19];
            $obj->BARCODE = $row[20];
            $obj->isFired = $row[21];
            $obj->fingerid = $row[22];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /**
 	* @param AssistantsModuleAssistant $newAssistant
	* @return int
 	*/
    public function addAssistant($newAssistant) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = 		
		"insert into ASSISTANTS(tin, firstname, middlename, lastname, fullname, shortname, birthday, sex, dateworkstart, dateworkend, mobilephone, homephone, email, skype, homeadress, postalcode, state, region, city, isfired, FINGERID) 
        values( 
        '$newAssistant->TIN', 
        '".safequery($newAssistant->FIRSTNAME)."', 
        '".safequery($newAssistant->MIDDLENAME)."', 
        '".safequery($newAssistant->LASTNAME)."', 
        '".safequery($newAssistant->FULLNAME)."', 
        '".safequery($newAssistant->SHORTNAME)."', 
        '$newAssistant->BIRTHDAY', 
        $newAssistant->SEX, 
        '$newAssistant->DATEWORKSTART', "; 
        if($newAssistant->DATEWORKEND != '')
        {
            $QueryText .= "'$newAssistant->DATEWORKEND', ";
        }
        else
        {
            $QueryText .= "null, ";
        }
        $QueryText .= "'".safequery($newAssistant->MOBILEPHONE)."', 
        '".safequery($newAssistant->HOMEPHONE)."', 
        '".safequery($newAssistant->EMAIL)."', 
        '".safequery($newAssistant->SKYPE)."', 
        '".safequery($newAssistant->HOMEADRESS)."', 
        '".safequery($newAssistant->POSTALCODE)."', 
        '".safequery($newAssistant->STATE)."', 
        '".safequery($newAssistant->REGION)."', 
        '".safequery($newAssistant->CITY)."', 
        '".safequery($newAssistant->isFired)."', ";
        if(is_numeric($newAssistant->fingerid) && $newAssistant->fingerid != 0)
        {
            $QueryText .= $newAssistant->fingerid.") ";
        }
        else
        {
            $QueryText .= " null) ";
        }
        
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_commit($trans);
		ibase_free_query($query);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = 'select max(ID) from ASSISTANTS';
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$row = ibase_fetch_row($result);
        ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);	
		$connection->CloseDBConnection();	
		return $row[0];		
    }      
            
    /**
 	* @param AssistantsModuleAssistant $newAssistant
	* @return boolean
 	*/
    public function updateAssistant($newAssistant) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = "update ASSISTANTS set "; 
		if ("$newAssistant->TIN"=="")
		{ $QueryText = $QueryText."TIN=null, "; }
		else { $QueryText = $QueryText."TIN=$newAssistant->TIN, "; }	
		if ("$newAssistant->FIRSTNAME"=="")
		{ $QueryText = $QueryText."FIRSTNAME=null, "; }
		else { $QueryText = $QueryText."FIRSTNAME='".safequery($newAssistant->FIRSTNAME)."', "; }	
		if ("$newAssistant->MIDDLENAME"=="")
		{ $QueryText = $QueryText."MIDDLENAME=null, "; }
		else { $QueryText = $QueryText."MIDDLENAME='".safequery($newAssistant->MIDDLENAME)."', "; }
		if ("$newAssistant->LASTNAME"=="")
		{ $QueryText = $QueryText."LASTNAME=null, "; }
		else { $QueryText = $QueryText."LASTNAME='".safequery($newAssistant->LASTNAME)."', "; }
		if ("$newAssistant->FULLNAME"=="")
		{ $QueryText = $QueryText."FULLNAME=null, "; }
		else { $QueryText = $QueryText."FULLNAME='".safequery($newAssistant->FULLNAME)."', "; }
		if ("$newAssistant->SHORTNAME"=="")
		{ $QueryText = $QueryText."SHORTNAME=null, "; }
		else { $QueryText = $QueryText."SHORTNAME='".safequery($newAssistant->SHORTNAME)."', "; }
		if ("$newAssistant->BIRTHDAY"=="")
		{ $QueryText = $QueryText."BIRTHDAY=null, "; }
		else { $QueryText = $QueryText."BIRTHDAY='".safequery($newAssistant->BIRTHDAY)."', "; }
		if ("$newAssistant->SEX"=="")
		{ $QueryText = $QueryText."SEX=null, "; }
		else { $QueryText = $QueryText."SEX=$newAssistant->SEX, "; }
		if ("$newAssistant->DATEWORKSTART"=="")
		{ $QueryText = $QueryText."DATEWORKSTART=null, "; }
		else { $QueryText = $QueryText."DATEWORKSTART='".safequery($newAssistant->DATEWORKSTART)."', "; }
		if ("$newAssistant->DATEWORKEND"=="")
		{ $QueryText = $QueryText."DATEWORKEND=null, "; }
		else { $QueryText = $QueryText."DATEWORKEND='".safequery($newAssistant->DATEWORKEND)."', "; }
		if ("$newAssistant->MOBILEPHONE"=="")
		{ $QueryText = $QueryText."MOBILEPHONE=null, "; }
		else { $QueryText = $QueryText."MOBILEPHONE='".safequery($newAssistant->MOBILEPHONE)."', "; }
		if ("$newAssistant->HOMEPHONE"=="")
		{ $QueryText = $QueryText."HOMEPHONE=null, "; }
		else { $QueryText = $QueryText."HOMEPHONE='".safequery($newAssistant->HOMEPHONE)."', "; } 
		if ("$newAssistant->EMAIL"=="")
		{ $QueryText = $QueryText."EMAIL=null, "; }
		else { $QueryText = $QueryText."EMAIL='".safequery($newAssistant->EMAIL)."', "; }
		if ("$newAssistant->SKYPE"=="")
		{ $QueryText = $QueryText."SKYPE=null, "; }
		else { $QueryText = $QueryText."SKYPE='".safequery($newAssistant->SKYPE)."', "; } 
		if ("$newAssistant->HOMEADRESS"=="")
		{ $QueryText = $QueryText."HOMEADRESS=null, "; }
		else { $QueryText = $QueryText."HOMEADRESS='".safequery($newAssistant->HOMEADRESS)."', "; }
		if ("$newAssistant->POSTALCODE"=="")
		{ $QueryText = $QueryText."POSTALCODE=null, "; }
		else { $QueryText = $QueryText."POSTALCODE='".safequery($newAssistant->POSTALCODE)."', "; } 
        if ("$newAssistant->STATE"=="")
		{ $QueryText = $QueryText."STATE=null, "; }
		else { $QueryText = $QueryText."STATE='".safequery($newAssistant->STATE)."', "; }
        if ("$newAssistant->REGION"=="")
		{ $QueryText = $QueryText."REGION=null, "; }
		else { $QueryText = $QueryText."REGION='".safequery($newAssistant->REGION)."', "; }
        if ("$newAssistant->CITY"=="")
		{ $QueryText = $QueryText."CITY=null, "; }
		else { $QueryText = $QueryText."CITY='".safequery($newAssistant->CITY)."', "; }
        if ("$newAssistant->isFired"=="")
        { $QueryText = $QueryText."isfired=0 "; }
		else { $QueryText = $QueryText."isfired='".safequery($newAssistant->isFired)."', "; }
        
        if(is_numeric($newAssistant->fingerid) && $newAssistant->fingerid != 0)
        {
            $QueryText .= "FINGERID=".$newAssistant->fingerid." ";
        }
        else
        {
            $QueryText .= "FINGERID=null ";
        }
        
		$QueryText = $QueryText." where (ID=$newAssistant->ID)";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();	
		return $success;		
    }
    
    /**
 	* @param string[] $Arguments,
	* @return boolean
 	*/ 		 		
    public function deleteAssistants($Arguments) 
     {           
     	/*$connection = new Connection();   	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from ASSISTANTS where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{ $QueryText = $QueryText." or (ID=$Arguments[$i])"; }		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);
        
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from AVATARIMAGES where ((ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{ $QueryText = $QueryText." or (ID=$Arguments[$i])"; }	
        $QueryText = $QueryText.") and TYPE='A'";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;*/
		return true;		
    }     
  
}

?>
<?php

require_once 'tbs/tbs_class.php';
require_once 'tbs/tbs_plugin_opentbs.php';

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

//$t = new PatientReportsModule();
//$t->createReport('docx', 9, 1, false, 'uk_UA');

class PatientReportsModule
{

    
    /** 
  * @param string $file_name
  * @return boolean
  */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);
        
    }
    
    /**
    * @param string $docType
    * @param string $patient_id
    * @param string $doctor_id
    * @param boolean $isSyncpatientFolder
    * @param string $locale
    * @param int $treatType
    * @return string
    */
    public function createReport($docType, $patient_id, $doctor_id, $isSyncpatientFolder, $locale, $treatType)
    {
        
        $res=(dirname(__FILE__).'/tbs/res/');
        
        
        if($docType == 'docx')
        {
            if(!file_exists($res.'PatientAgreementH.docx') && $treatType == 1)
            {
                return "TEMPLATE_NOTFOUND";
            }
            else if(!file_exists($res.'PatientAgreementP.docx') && $treatType == 2)
            {
                return "TEMPLATE_NOTFOUND";
            }
            else if(!file_exists($res.'PatientAgreement.docx'))
            {
                return "TEMPLATE_NOTFOUND";
            }
        }
        else if($docType == 'odt')
        {
            
            if(!file_exists($res.'PatientAgreementH.odt') && $treatType == 1)
            {
                return "TEMPLATE_NOTFOUND";
            }
            else if(!file_exists($res.'PatientAgreementP.odt') && $treatType == 2)
            {
                return "TEMPLATE_NOTFOUND";
            }
            else if(!file_exists($res.'PatientAgreement.odt'))
            {
                return "TEMPLATE_NOTFOUND";
            }
        }
        
        
        
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
       
        $ClinicRegistrationData = new stdclass();
        $Patient = new stdclass();
        $DOCTOR = new stdclass();
        $QueryText = "
        select 
            NAME,
            LEGALNAME,
            ADDRESS,
            ORGANIZATIONTYPE,
            CODE,
            DIRECTOR,
            TELEPHONENUMBER,
            FAX,
            SITENAME,
            EMAIL,
            ICQ,
            SKYPE,
            CHIEFACCOUNTANT,
            LEGALADDRESS,
            BANK_NAME,
            BANK_MFO,
            BANK_CURRENTACCOUNT,
            LICENSE_NUMBER,
            LICENSE_SERIES,
            LICENSE_STARTDATE,
            LICENSE_ENDDATE,
            LICENSE_ISSUED,
            LOGO,
            AUTHORITY
            
        from CLINICREGISTRATIONDATA";
        
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        
        if ($row = ibase_fetch_row($result))
        {
            $ClinicRegistrationData->NAME = $row[0];
            $ClinicRegistrationData->CLINICNAME = $row[1];
            $ClinicRegistrationData->CLINICADDRESS = $row[2];
            $ClinicRegistrationData->ORGANIZATIONTYPE = $row[3];
            $ClinicRegistrationData->CODE = $row[4];
            $ClinicRegistrationData->DIRECTOR = $row[5];
            $ClinicRegistrationData->TELEPHONENUMBER = $row[6];
            $ClinicRegistrationData->FAX = $row[7];
            $ClinicRegistrationData->SITENAME = $row[8];
            $ClinicRegistrationData->EMAIL = $row[9];
            $ClinicRegistrationData->ICQ = $row[10];
            $ClinicRegistrationData->SKYPE = $row[11];
            $ClinicRegistrationData->CHIEFACCOUNTANT = $row[12];
            $ClinicRegistrationData->LEGALADDRESS = $row[13];
            $ClinicRegistrationData->BANK_NAME = $row[14];
            $ClinicRegistrationData->BANK_MFO = $row[15];
            $ClinicRegistrationData->BANK_CURRENTACCOUNT = $row[16];
            $ClinicRegistrationData->LICENSE_NUMBER = $row[17];
            $ClinicRegistrationData->LICENSE_SERIES = $row[18];
            $ClinicRegistrationData->LICENSE_STARTDATE = $row[19];
            $ClinicRegistrationData->LICENSE_ENDDATE = $row[20];
            $ClinicRegistrationData->LICENSE_ISSUED = $row[21];
            $ClinicRegistrationData->AUTHORITY = $row[23];
            
            $imageBlob = null;
            $blob_info = ibase_blob_info($row[22]);
            if ($blob_info[0]!=0){
                $blob_hndl = ibase_blob_open($row[22]);
                $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
            }
            $ClinicRegistrationData->LOGO = base64_encode($imageBlob);
            
        }
        
        if($ClinicRegistrationData->CODE!=null)
        {
            $CODE_ARRAY=str_split($ClinicRegistrationData->CODE,1);
            $ClinicRegistrationData->CODE1 = $CODE_ARRAY[0];
            $ClinicRegistrationData->CODE2 = $CODE_ARRAY[1];
            $ClinicRegistrationData->CODE3 = $CODE_ARRAY[2];
            $ClinicRegistrationData->CODE4 = $CODE_ARRAY[3];
            $ClinicRegistrationData->CODE5 = $CODE_ARRAY[4];
            $ClinicRegistrationData->CODE6 = $CODE_ARRAY[5];
            $ClinicRegistrationData->CODE7 = $CODE_ARRAY[6];
            $ClinicRegistrationData->CODE8 = $CODE_ARRAY[7];
        }
        
        ibase_free_query($query);
        ibase_free_result($result);
        
        
        //$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);       
        
        $QueryText = "            select                  
        coalesce(r.fullname, p.FULLNAME),                 
        coalesce(r.SHORTNAME, p.SHORTNAME),                 
        coalesce(r.PASSPORT_SERIES, p.PASSPORT_SERIES),                 
        coalesce(r.PASSPORT_NUMBER, p.PASSPORT_NUMBER),                 
        coalesce(r.PASSPORT_ISSUING, p.PASSPORT_ISSUING),                 
        coalesce(r.PASSPORT_DATE, p.PASSPORT_DATE),
		coalesce(r.ADDRESS, p.ADDRESS),
		coalesce(r.POSTALCODE, p.POSTALCODE),
		coalesce(r.COUNTRY, p.COUNTRY),
		coalesce(r.STATE, p.STATE),
		coalesce(r.CITY, p.CITY),
		coalesce(r.EMAIL, p.EMAIL),
		coalesce(r.EMPLOYMENTPLACE, p.EMPLOYMENTPLACE),
		coalesce(r.JOB, p.JOB),
        p.carddate,
        d.shortname,
        d.fullname,
        d.speciality,
		p.fatherlastname,
        p.fatherfirstname,
        p.fathermiddlename,
        p.fatherbirthdate,
        p.motherlastname,
        p.motherfirstname,
        p.mothermiddlename,
        p.motherbirthdate,
		coalesce(r.BIRTHDAY, p.BIRTHDAY),
		p.CARDBEFORNUMBER,
		p.CARDNUMBER,
		p.CARDAFTERNUMBER
        from PATIENTS p             
        left join patients r on (r.id=p.legalguardianid) 
        left join doctors d on (d.id=p.leaddoctor_fk)           
         where p.ID = $patient_id"; 			         
         $query = ibase_prepare($trans, $QueryText);         
         $result = ibase_execute($query);                  
         if ($row = ibase_fetch_row($result))         
         {             
            $Patient->FullName = $row[0];             
            $Patient->ShortName = $row[1];             
            $Patient->PASSPORT_SERIES = $row[2];             
            $Patient->PASSPORT_NUMBER = $row[3];             
            $Patient->PASSPORT_ISSUING = $row[4];             
            $Patient->PASSPORT_DATE = $row[5]!=null?date("d.m.Y", strtotime($row[5])):"";
            $Patient->ADDRESS = $row[6];
            $Patient->POSTALCODE = $row[7];
            $Patient->COUNTRY = $row[8];
            $Patient->STATE = $row[9];
            $Patient->CITY = $row[10];
            $Patient->EMAIL = $row[11];
            $Patient->EMPLOYMENTPLACE = $row[12];
            $Patient->JOB = $row[13];
            $Patient->CARDDATE=  $row[14]!=null?date("d.m.Y", strtotime($row[14])):"";
            $DOCTOR->ShortName = $row[15]; 
            $DOCTOR->FullName = $row[16]; 
            $DOCTOR->SPECIALITY = $row[17];
          
            $Patient->FATHERLASTNAME = $row[18]; 
            $Patient->FATHERFIRSTNAME = $row[19]; 
            $Patient->FATHERMIDDLENAME = $row[20]; 
            $Patient->FATHERSHORTNAME = shortname($row[18], $row[19],$row[20]);
            $Patient->FATHERBIRTHDATE = $row[21]!=null?date("d.m.Y", strtotime($row[21])):"";
            
            $Patient->MOTHERLASTNAME = $row[22]; 
            $Patient->MOTHERFIRSTNAME = $row[23]; 
            $Patient->MOTHERMIDDLENAME = $row[24]; 
			$Patient->MOTHERSHORTNAME = shortname($row[22], $row[23],$row[24]);
            $Patient->MOTHERBIRTHDATE = $row[25]!=null?date("d.m.Y", strtotime($row[25])):""; 

			$Patient->BIRTHDAY = $row[26]!=null?date("d.m.Y", strtotime($row[26])):"";  
			$Patient->CARDNUMBER = "{$row[27]}{$row[28]}{$row[29]}";

 
           }
			
        ibase_free_query($query);
        ibase_free_result($result);
        $Patient->PHONESTRING=PrintDataModule_PatientData::getMobileNumbersString($patient_id,$trans);
        $Date = new stdclass();
        $currDate = time();
        
        $Date->CurrDate = ua_date('"d" M Y', $currDate );
        $Date->Day = ua_date("d", $currDate );
        $Date->Month = ua_date("M", $currDate );
        $Date->Year = ua_date("y", $currDate );
        
        
        
        
        $TBS = new clsTinyButStrong();
        $TBS -> NoErr=true;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        
        
        if($docType == 'odt')
        {
            if($treatType == 1)
            {
                //TODO openoffice
                $TBS->LoadTemplate($res.'PatientAgreementH.odt', OPENTBS_ALREADY_XML);
                //$TBS->LoadTemplate($res.'PatientAgreement_A5.odt', OPENTBS_ALREADY_XML);
            }
            else if($treatType == 2)
            {
                //TODO openoffice
                $TBS->LoadTemplate($res.'PatientAgreementP.odt', OPENTBS_ALREADY_XML);
                //$TBS->LoadTemplate($res.'PatientAgreement_A5.odt', OPENTBS_ALREADY_XML);
            }
            else
            {
                //TODO openoffice
                $TBS->LoadTemplate($res.'PatientAgreement.odt', OPENTBS_ALREADY_XML);
                //$TBS->LoadTemplate($res.'PatientAgreement_A5.odt', OPENTBS_ALREADY_XML);
            }
            
        }
        else if($docType == 'docx')
        {
            if($treatType == 1)
            {
                $TBS->LoadTemplate($res.'PatientAgreementH.docx', OPENTBS_ALREADY_XML);
                //$TBS->LoadTemplate($res.'PatientAgreement_A5.docx', OPENTBS_ALREADY_XML);
            }
            else if($treatType == 2)
            {
                $TBS->LoadTemplate($res.'PatientAgreementP.docx', OPENTBS_ALREADY_XML);
                //$TBS->LoadTemplate($res.'PatientAgreement_A5.docx', OPENTBS_ALREADY_XML);
            }
            else
            {
                $TBS->LoadTemplate($res.'PatientAgreement.docx', OPENTBS_ALREADY_XML);
                //$TBS->LoadTemplate($res.'PatientAgreement_A5.docx', OPENTBS_ALREADY_XML);
            }
        }
        
        
        
        $TBS->MergeField("ClinicRegistrationData", $ClinicRegistrationData);
        $TBS->MergeField("Date", $Date);
        $TBS->MergeField("Patient", $Patient);
         $TBS->MergeField("DOCTOR", $DOCTOR);
        
        
        $date = date("d-m-Y_H-i-s", time());
        
        if($treatType == 1)
        {
            $file_name = '_PatientAgreementH__pId_' .$patient_id. '__' . $date;
        }
        else if($treatType == 2)
        {
            $file_name = '_PatientAgreementP__pId_' .$patient_id. '__' . $date;
        }
        else
        {
            $file_name = '_PatientAgreement__pId_' .$patient_id. '__' . $date;
        }
            
        $file_name = preg_replace('/[^A-Za-z0-9_-]/', '', $file_name);
        
        $file_name = uniqid().$file_name.'.'.$docType;
        
        
        
        
        
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$file_name );
        
       
        $timestamp = date("Y-m-d H:i:s", time());
        $timestamp_sql = "update PATIENTS set 		  
    		  AGREEMENTTIMESTAMP = '$timestamp' where (ID=$patient_id)";
		$timestamp_query = ibase_prepare($trans, $timestamp_sql);
		ibase_execute($timestamp_query);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        
        // если $isSyncpatientFolder == true синхронизировать с папкой пациентов
        /*if($isSyncpatientFolder == true) 
        {
            addServerFile($patient_id,dirname(__FILE__).'/docs/'.$file_name);
        }*/
        
            
       return $file_name;
    }
    
}



?>

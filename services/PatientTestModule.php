<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once "F43OBJECT.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');


//$ptm=new PatientTestModule;
//$ptm->printTestQuestions('odt',1,0);
//print_r($ptm->setPatientTest("3",""));



class PatientTestModule
{
    /** 
  * @param PatientTestModule_TestQuestionPrintData $p1
  * @param PatientTestModule_TestQuestion $p2
  * @param PatientTestModule_TestResult $p3
  * @return void
  */    
	public function registertypes($p1, $p2, $p3) 
	{
	} 
    
    
     /**
     * @param string $doc_type
     * @param string $id_patient
     * @param string $id_test
     * @param resource $trans
     * @return PatientTestModule_TestQuestionPrintData[]
     */

    public function getTestQuestionsPrintData($doc_type, $id_patient, $id_test, $trans=null)
    {
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($trans,true));
        $test_question_array=$this->getTestQuestions(true, $id_patient, $id_test, $trans);
        
        usort($test_question_array,'cmp_number');
        $print_data_array=array();
        foreach($test_question_array as $test_question)
        {
            if($test_question->isenabled)
            {
                $print_data=new PatientTestModule_TestQuestionPrintData;
                $print_data->question=$test_question->question;
                $print_data->number=$test_question->number;
                $print_data->testdate=$test_question->testdate;
                
                $xml = simplexml_load_string($test_question->answers);
                $res_xml = simplexml_load_string($test_question->result);
                $print_data->type=strval($xml['type']);               
                
                $answers=array();
                $is_correct_type=(strval($xml['type'])==strval($res_xml['type']));
            
               
                    switch($print_data->type)
                    {
                        case 'DropDownList':
                        {
                           foreach($xml->answer as $answer)
                           {
                                $tmp=strval($answer);
                                if ($is_correct_type&&strstr($test_question->result,'>'.strval($answer).'<'))
                                {
                                    switch($doc_type)
                                    {    
                                        case "odt":
                                        case "docx":
                                            $tmp='&#9745; '.$tmp;
                                            break;  
                                    }
                                    
                                }
                                else
                                {
                                    switch($doc_type)
                                    {   
                                        case "odt":
                                        case "docx":
                                            $tmp='&#9744; '.$tmp;
                                            break;  
                                    }
                                } 
                                
                                $answers[]=(count($answers)==0?'':' ').$tmp;
                           }
                           break; 
                        }
                        
                        case 'RadioButton':
                        {
                            
                            foreach($xml->answer as $answer)
                            {
                                $tmp=strval($answer);
                                if ($is_correct_type&&strstr($test_question->result,'>'.strval($answer).'<')) $tmp=$tmp.'&#10004;';
                                $answers[]=$tmp;
                            }
                            break;
                        }
                        case 'CheckBox':
                        {
                            
                            foreach($xml->answer as $answer)
                            {
                                $tmp=strval($answer);if ($is_correct_type&&strstr(strval($test_question->result),'>'.$tmp.'<'))
                                if ($is_correct_type&&strstr($test_question->result,'>'.strval($answer).'<'))
                                {
                                    switch($doc_type)
                                    {    
                                        case "docx":
                                            $tmp='</w:t></w:r><w:r><w:rPr><w:u w:val="single"/></w:rPr><w:t>'.$tmp.'</w:t></w:r><w:r><w:t>';
                                            break;  
                                        case "odt":
                                            $tmp='<text:span text:style-name="T5">'.$tmp.'</text:span>';
                                            break;  
                                    }
                                    
                                }
                                     
                                $answers[]=(count($answers)==0?'':', ').$tmp;
                            }
                            break;
                        }
                        
                        case 'TextInput':
                        {
                            if($is_correct_type) $answers[]=strval($res_xml->result);
                            break;
                        }
                        case 'LabelsTextInput':
                        {
                            
                            foreach($xml->answer as $answer)
                            { 
                                $tmp=strval($answer);
                                    if ($is_correct_type&&strstr($test_question->result,'label="'.strval($answer).'"')) 
                                    {  
                                        
                                        preg_match("/<result label=\"".strval($answer)."\">[^<]*<\/result>/", $test_question->result, $match);
                                        $ans=simplexml_load_string('<xml>'.$match[0].'</xml>');
                                        switch($doc_type)
                                        {    
                                            case "docx":
                                                $tmp=$tmp.(strval($ans->result[0])==''?'____':'</w:t></w:r><w:r><w:rPr><w:u w:val="single"/></w:rPr><w:t> '.strval($ans->result[0]).'</w:t></w:r><w:r><w:t>');
                                                break;
                                            case "odt":
                                                $tmp=$tmp.(strval($ans->result[0])==''?'____':'<text:span text:style-name="T5"> '.strval($ans->result[0]).'</text:span>');
                                                break;  
                                        }
                                        
                                    }
                                    else $tmp=$tmp.'_____';
                                    $answers[]=(count($answers)==0?'':', ').$tmp;
                            }
                            break;
                        }
                    }
                    

                $print_data->answers=$answers;
                $print_data->answers_count=count($answers);
                
                $print_data_array[]=$print_data;

            }
        }

        return $print_data_array;
                
    }
    
    
    /**
     * @param string $doc_type
     * @param string $id_patient
     * @param string $id_test
     * @return string
     */
     public function printTestQuestions($doc_type, $id_patient, $id_test)
     {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
            $print_data_array = $this->getTestQuestionsPrintData($doc_type, $id_patient, $id_test, $trans);
            
            if(count($print_data_array)>0) 
            {
                $TestDate=$print_data_array[0]->testdate;
            }
            if(!nonull($TestDate)) 
            {
              $TestDate=date("d/m/Y");  
            }
            
            $pat_data=new PrintDataModule_PatientData($id_patient, $trans);
            $clin_data=new PrintDataModule_ClinicRegistrationData(false, $trans);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
                
        //print_r($print_data_array);
        $TBS = new clsTinyButStrong;
        
     $file_name = translit(str_replace(' ','_',$pat_data->LASTNAME.'_'.$pat_data->FIRSTNAME.'_'.$pat_data->MIDDLENAME.'_'.$pat_data->CARDNUMBER));
     $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
     $form_path = uniqid().'_test_'.$file_name.'.'.$doc_type;  
                        
       
             $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
             $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/PatientTest.'.$doc_type, OPENTBS_ALREADY_XML);
             $TBS->MergeBlock('Test',$print_data_array);
             $TBS->MergeField('CLINIC_DATA', $clin_data); 
            $TBS->MergeField('PATIENT_DATA',$pat_data);
			//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$TestDate);
            $TBS->MergeField('TestDate',$TestDate);
            //$TBS->NoErr=true;
            $TBS->MergeBlock('Test',$print_data_array);
            //file_put_contents('c:/tmp.txt',$TBS->Source);
            


            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
            return $form_path; 
     }
     

    
     /**
     * @param boolean $isTest
     * @param string $id_patient
     * @param string $id_test
     * @param resource $trans
     * @return PatientTestModule_TestQuestion[]
     */

    public function getTestQuestions($isTest, $id_patient, $id_test, $trans=null)//sqlite
    {
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($trans,true));
        if(nonull($id_patient)&&$isTest&&nonull($id_test)) $test_results=$this->getPatientTest($id_patient, $id_test, $trans);
        $questions=array();
        
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
    
    	$QueryText = "select ID, ISENABLED, SEQNUMBER, QUESTION, CONFIGXML from TEMPLATETEST ";
        if($isTest == true)
        {
            $QueryText .= "where ISENABLED = 1 ";
        }
        $QueryText .= "order by ID";
        
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
		while ($row = ibase_fetch_row($result))
		{	
            $question=new PatientTestModule_TestQuestion;
            if(count($questions)==0&&nonull($id_patient)&&$isTest&&nonull($id_test)) $question->testdate=$test_results[0]->testdate;
            $question->id = $row[0]; 
            $question->isenabled = $row[1]==1?true:false; 
            $question->number = $row[2]; 
            $question->question = $row[3]; 
            $question->answers = $row[4];
            $question->changes = "";
            $question->result = "";
            if(nonull($id_patient)&&$isTest&&nonull($id_test))
            {    
                    
                    $xml = simplexml_load_string($test_results[0]->result); 
                    if(strstr($test_results[0]->result,'id="'.$question->id.'"'))
                    {
                        $tmp=($xml->xpath('/test/results[@id="'.$question->id.'"]'));
                        $question->result=html_entity_decode($tmp[0]->asXML()); 
                        //echo($question->result."\n");                    
                    }
  
            }
            $questions[]=$question;
		}			
		
		ibase_free_query($query);
		ibase_free_result($result);                       
         if($local)
         {
                ibase_commit($trans);
                $connection->CloseDBConnection();
         }
        
        return $questions;
    }
    
    
    /**
     * @param PatientTestModule_TestQuestion[] $questions
     * @return boolean
     */
    public function setTestQuestions($questions)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $question=new PatientTestModule_TestQuestion();
		foreach($questions as $question)
        {
            switch($question->changes)
            {
                case 'insert':
                    $QueryText="insert into TEMPLATETEST (ID, ISENABLED, SEQNUMBER, QUESTION, CONFIGXML) 
                                values (null,".($question->isenabled==true?1:0).",'".safequery($question->number)."','".safequery($question->question)."','".safequery($question->answers)."')";
                    break;
                    
                case 'update':
                    $QueryText="update TEMPLATETEST set ISENABLED = ".($question->isenabled==true?1:0).",
											SEQNUMBER ='".safequery($question->number)."',
                                            QUESTION = '".safequery($question->question)."',
                                            CONFIGXML = '".safequery($question->answers)."'
                                where ID =".$question->id;                                
                    break;
                    
                case 'deleteupdate':
                case 'delete':
                    $QueryText="delete from TEMPLATETEST where (ID is null) or (ID=".$question->id.")";
                    break;
                    
                default:
                    $QueryText='';
                    break;
                
            }
            if($QueryText!='')
            {               
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            } 
        }
        ibase_free_query($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
     * @param string $id_patient
     * @param string $id_test
     * @param resource $trans
     * @return PatientTestModule_TestResult[]
     */
    public function getPatientTest($id_patient, $id_test, $trans=null)
    {
        $res='';
        
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }      
    
    	$QueryText = "select ID, TESTDATE, TESTRESULT from PATIENTSTESTCARDS where (PATIENTID=$id_patient  and TESTRESULT is not null ".(nonull($id_test)?(" and ID=$id_test)"):")")." order by TESTDATE";
              
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
        $res_array=array();
		while ($row = ibase_fetch_row($result))
		{	
            $res=new PatientTestModule_TestResult;
            $res->result=(nonull($row[2]))?text_blob_encode($row[2]):'';
            $res->testdate=date("d/m/Y",strtotime($row[1]));
            $res->id=$row[0];
            $res_array[]=$res;
		}			
		ibase_free_query($query);
		ibase_free_result($result);                       
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        
        return $res_array;
    }
    
    /**
     * @param string $id_patient
     * @param string $test_result
     * @return PatientTestModule_TestResult
     */
    public function setPatientTest($id_patient, $test_result)
    {  
        $connection = new Connection();
        $connection->EstablishDBConnection();
        
       	$res=new PatientTestModule_TestResult;
        $res->result=$test_result;
        
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText="insert into PATIENTSTESTCARDS (TESTRESULT, PATIENTID)  values('".(nonull($test_result)?safequery($test_result):"null")."', $id_patient) returning ID, TESTDATE";                                
        
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
        if ($row = ibase_fetch_row($result))
		{	
           $res->id=$row[0];
           $res->testdate=date("d/m/Y",strtotime($row[1]));
		}	
		ibase_free_query($query);
		ibase_free_result($result);                       
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $res;
    }
    
       
  /**
 * @param string $id_test
 * @param string $passForDelete
 * @return boolean
  */      
 public function deletePatientTest($id_test, $passForDelete) 
 {           
  $connection = new Connection();
  $connection->EstablishDBConnection();  
  $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForDel'";
  $query = ibase_prepare($trans, $QueryText);
  $result = ibase_execute($query);
  if($row = ibase_fetch_row($result))
  {
   if($row[0] == $passForDelete)
   {
               ibase_free_result($result);
            ibase_free_query($query);
               
             $QueryText = "delete from PATIENTSTESTCARDS where ID = $id_test";
                               
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            ibase_free_query($query);
            $success = ibase_commit($trans);
            $connection->CloseDBConnection();
            return $success;
   }
  }
  ibase_free_result($result);
  ibase_free_query($query);
  ibase_commit($trans);
  $connection->CloseDBConnection();
  return false;
 }
  
   /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteTestDoc($form_path)
    {
        return deleteFileUtils($form_path);
    }
    
            
}


    
class PatientTestModule_TestQuestion
{
     /**
    * @var int
    */
    var $id;

    /**
    * @var boolean
    */
    var $isenabled;

    /**
    * @var string
    */
    var $number;
    
    /**
    * @var string
    */
    var $question;
	
	/**
    * @var string
    */
    var $answers;
	
	/**
    * @var string
    */
    var $changes;
    
    /**
    * @var string
    */
    var $testdate;
    
   	 /**
    * @var string
    */
    var $result;
} 

class PatientTestModule_TestQuestionPrintData
{
    /**
    * @var string
    */
    var $number;
    
    /**
    * @var string
    */
    var $question;
	
	/**
    * @var string[]
    */
    var $answers;
	   
   	/**
    * @var string
    */
    var $results;
    
     /**
    * @var string
    */
    var $testdate;
    
   	/**
    * @var string
    */
    var $type;
    
    /**
    * @var int
    */
    var $answers_count;
    
} 

class PatientTestModule_TestResult
{   
    
    /**
    * @var int
    */
    var $id;
    
    /**
    * @var string
    */
    var $testdate;
	   
   	/**
    * @var string
    */
    var $result;
} 
?>
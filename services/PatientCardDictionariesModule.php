<?php

require_once "Connection.php";
require_once "Utils.php";

class PatientCardDictionariesModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;
    
    /**
    * @var int
    */
    var $TYPE; 
    
    /**
    * @var int
    */
    var $COLOR;
}

 
class PatientCardDictionariesModule
{	
    /**  
    * @param PatientCardDictionariesModuleDictionaryObject $p1
    */    
	public function registertypes($p1) 
	{
	}    
    
    /** 
     * @param int $type
 	*  @return PatientCardDictionariesModuleDictionaryObject[]
	*/ 
    public function getPatientCardDictionary($type) //sqlite
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION, TYPE, COLOR from PatientCardDictionaries where TYPE=".$type." order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new PatientCardDictionariesModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
            $obj->TYPE = $row[3];
            $obj->COLOR = $row[4];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param PatientCardDictionariesModuleDictionaryObject $ovo 
     *@param int $type
    * @return string
 	*/
    public function addPatientCardDictionary($ovo, $type) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into PatientCardDictionaries (ID, NUMBER, DESCRIPTION, TYPE, COLOR) values (null,$ovo->NUMBER, '".safequery($ovo->DESCRIPTION)."', $type, ".nullcheck($ovo->COLOR).") returning ID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        $res="";
        if($row = ibase_fetch_row($result))
		{ 
			$res = $row[0];
		}
		ibase_commit($trans);
		ibase_free_query($query);
        ibase_free_result($result);
		$connection->CloseDBConnection();
		return $res;
    }
    
    /**
 	* @param PatientCardDictionariesModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updatePatientCardDictionary ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update PatientCardDictionaries
					 set DESCRIPTION ='".safequery($ovo->DESCRIPTION)."',
                            COLOR=".nullcheck($ovo->COLOR)."
					     where ID = '$ovo->ID' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deletePatientCardDictionary($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from PatientCardDictionaries where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param PatientCardDictionariesModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updatePatientCardDictionaryNumber ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update PatientCardDictionaries set NUMBER ='$ovo->NUMBER' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;	
    }      
  }
?>
<?php

require_once "Connection.php";
require_once "Utils.php";
 
class EmployeeModule_Employee
{
    /**
    * @var string
    */
    var $id;
    
    /**
    * @var string
    */
    var $fname;
     
    /**
    * @var string
    */
    var $sname;
     
    /**
    * @var string
    */
    var $lname;
    
    /**
    * @var string
    */
    var $fullname;
     
    /**
    * @var string
    */
    var $shortname;
    
    /**
    * @var string
    */
    var $birthday; 

    /**
    * @var int
    */
    var $sex;     

    /**
    * @var string
    */
    var $phone1;  
    
    /**
    * @var string
    */
    var $phone2; 
    
    /**
    * @var string
    */
    var $specialty; 
}

class EmployeeModule
{	
    /**
     * @param EmployeeModule_Employee $p1
     */
    public function registertypes($p1)
    {}
    
    /** 
 	* @param string $lastname
	* @param string $subdivId
	* @return EmployeeModule_Employee[]
	*/    
    public function getEmployeeFiltered($lastname, $subdivId)
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, FNAME, LNAME, SNAME, PHONE1, PHONE2, BIRTHDAY, SPECIALTY, SEX
                      from EMPLOYEE where (ID is not null)";
		if ($lastname != "")	
		{ 
			$QueryText .= " and lower(LNAME) starting lower('$lastname')";
		}			
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $QueryText .= " and SUBDIVISIONID = $subdivId ";	
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new EmployeeModule_Employee;
			$obj->id = $row[0];
			$obj->fname = $row[1];
			$obj->lname = $row[2];
			$obj->sname = $row[3];
			$obj->fullname = $row[2].' '.$row[1].' '.$row[3];
			$obj->shortname = shortname($row[2],$row[1],$row[3]);
			$obj->phone1 = $row[4];
			$obj->phone2 = $row[5];
			$obj->birthday = $row[6];
			$obj->specialty = $row[7];
			$obj->sex = $row[8];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /**
 	* @param EmployeeModule_Employee $newEmployee
	* @return boolean
 	*/
    public function addEmployee($newEmployee) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = 		
		"insert into EMPLOYEE (FNAME, LNAME, SNAME, PHONE1, PHONE2, BIRTHDAY, SPECIALTY, SEX)
                                    values (
                                    '".safequery($newEmployee->fname)."', 
                                    '".safequery($newEmployee->lname)."', 
                                    '".safequery($newEmployee->sname)."', 
                                    '".safequery($newEmployee->phone1)."', 
                                    '".safequery($newEmployee->phone2)."', 
                                    '".$newEmployee->birthday."', 
                                    '".safequery($newEmployee->specialty)."', 
                                    '".$newEmployee->sex."')"; 
        
        
        
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;		
    }      
            
    /**
 	* @param EmployeeModule_Employee $newEmployee
	* @return boolean
 	*/
    public function updateEmployee($newEmployee) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = "update EMPLOYEE
                        set FNAME = '".safequery($newEmployee->fname)."',
                            LNAME = '".safequery($newEmployee->lname)."',
                            SNAME = '".safequery($newEmployee->sname)."',
                            PHONE1 = '".safequery($newEmployee->phone1)."',
                            PHONE2 = '".safequery($newEmployee->phone2)."',
                            BIRTHDAY = '".$newEmployee->birthday."',
                            SPECIALTY = '".safequery($newEmployee->specialty)."',
                            SEX = '".$newEmployee->sex."'
                        where (ID = '".$newEmployee->id."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
        
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();	
		return $success;		
    }
    
    
    /**
 	* @param string $id,
	* @return boolean
 	*/ 		 		
    public function deleteEmployee($id) 
     {           
     	$connection = new Connection();   	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$QueryText = "delete from EMPLOYEE
                            where (ID = ".$id.")";
    	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);
        
		$connection->CloseDBConnection();
		return $success;	
    }     
  
}

?>
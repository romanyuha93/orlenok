<?php

  	require_once "Connection.php";
	require_once "Utils.php";
	define('tfClientId','online');
	ini_set("max_execution_time", "90000");
	ini_set('memory_limit', '-1');
	ini_set('realpath_cache_ttl', '90000');

  // Errors
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
  //
    
//$locale = 'ru_RU';//uk_UA, en_US, ke_GE, pl_PL, ru_RU

$locale_1 = array('uk_UA' => 'Назад', 
                  'en_US' => 'back', 
                  'ka_GE' => 'უკან',
                  'pl_PL' => 'do tyłu', 
                  'ru_RU' => 'Назад');
                  
$locale_2 = array('uk_UA' => '!НЕ ВКАЗАНО!', 
                  'en_US' => '!NOT INDICATED!', 
                  'ka_GE' => '!არ არის რეკომენდირებული!',
                  'pl_PL' => '!NIE SPECYFIKOWANY!', 
                  'ru_RU' => '!НЕ УКАЗАНО!');
                  
$locale_3 = array('uk_UA' => 'Вибірка первинних пацієнтів в розрізі інформації про клініку за період з ', 
                  'en_US' => 'The sample of primary patients in the context of information about the clinic for the period from  ', 
                  'ka_GE' => 'პირველადი პაციენტების ნიმუში კლინიკის შესახებ ინფორმაციის კონტექსტში პერიოდში ',
                  'pl_PL' => 'Próba pierwotnych pacjentów w kontekście informacji o klinice w okresie od ', 
                  'ru_RU' => 'Выборка первичных пациентов в разрезе информации о клинике за период c ');
                  
$locale_4 = array('uk_UA' => 'по', 
                  'en_US' => 'to', 
                  'ka_GE' => 'оn',
                  'pl_PL' => 'na', 
                  'ru_RU' => 'по');
                  
$locale_5 = array('uk_UA' => 'Звідки дізналися про клініку', 
                  'en_US' => 'How did you know about the clinic', 
                  'ka_GE' => 'როგორ იცით კლინიკის შესახებ',
                  'pl_PL' => 'Skąd wiedzieli o klinice', 
                  'ru_RU' => 'Откуда узнали о клинике');
                  
$locale_6 = array('uk_UA' => 'Кільк-ть первинних пацієнтів', 
                  'en_US' => 'Number of primary patients', 
                  'ka_GE' => 'პირველადი პაციენტების რაოდენობა',
                  'pl_PL' => 'Liczba pacjentów pierwotnych', 
                  'ru_RU' => 'Кол-во первичных пациентов');

$locale_7 = array('uk_UA' => '% від загальної кількості', 
                  'en_US' => '% of the total', 
                  'ka_GE' => '% საერთო ჯამში',
                  'pl_PL' => '% całości', 
                  'ru_RU' => '% от общего кол-ва');
                  
$locale_8 = array('uk_UA' => 'РАЗОМ:', 
                  'en_US' => 'TOTAL:', 
                  'ka_GE' => 'ტოტალი:',
                  'pl_PL' => 'OGÓŁEM:', 
                  'ru_RU' => 'ИТОГО:');                  

$locale_9 = array('uk_UA' => 'Список пацієнтів, у яких не вказано джерело інформації про клініку:', 
                  'en_US' => 'List of patients who do not have a source of information about the clinic:', 
                  'ka_GE' => 'პაციენტების სია, რომლებსაც არ გააჩნიათ ინფორმაცია წყაროდ კლინიკის შესახებ:',
                  'pl_PL' => 'Lista pacjentów, którzy nie mają informacji o klinice:', 
                  'ru_RU' => 'Список пациентов, у которых не указан источник информации о клинике:');  
                  
$locale_10 = array('uk_UA' => 'ПІБ', 
                   'en_US' => 'Full name', 
                   'ka_GE' => 'სრული სახელი',
                   'pl_PL' => 'Imię i nazwisko', 
                   'ru_RU' => 'ФИО');  

$locale_11 = array('uk_UA' => 'Оберіть період відбору:', 
                   'en_US' => 'Select the selection period:', 
                   'ka_GE' => 'აირჩიეთ შერჩევის პერიოდი:',
                   'pl_PL' => 'Wybierz okres wyboru:', 
                   'ru_RU' => 'Выберите период отбора:');
                   
                   
$locale_12 = array('uk_UA' => 'Показати', 
                   'en_US' => 'Show', 
                   'ka_GE' => 'ჩვენება',
                   'pl_PL' => 'Pokaż', 
                   'ru_RU' => 'Показать');

$locale_13 = array('uk_UA' => 'з', 
                   'en_US' => 'from', 
                   'ka_GE' => 'ერთად',
                   'pl_PL' => 'z', 
                   'ru_RU' => 'c');

if($_GET['locale'])
{
    $locale = $_GET['locale'];
}
    
    
if($_GET['CLINETID'])
{
    
	$connection = new Connection();
	$connection->EstablishDBConnection("main",false);   
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    
    $client_id = $_GET['CLINETID'];
    $checked = false;
    if($client_id == $locale_1[$locale])  // "на"
    {
       $checked = true; 
    }
    else
    {
       $sql_key = "select first 1 ACCOUNTKEY from ACCOUNT
                where ACCOUNTKEY = '$client_id'";
                                			
		$query_key = ibase_prepare($trans, $sql_key);
		$result_key = ibase_execute($query_key);
        	
		if($r_key = ibase_fetch_row($result_key))
		{
		  if($r_key[0] != null) $checked = true;
		}	
		ibase_free_query($query_key);
		ibase_free_result($result_key);     
    }

    


        if($checked)
        {
            echo("<head>
            <meta charset='UTF-8'/>
            </head><body>");
            
            
            
            if(!empty($_POST['between']) and !empty($_POST['and']))
            { 	
            	echo '<form action="OLAP_fwp.php?"><input type="submit" name="CLIN" value="'.$locale_1[$locale].'"><input type="hidden" name="locale" value="'.$_GET['locale'].'"><input type="hidden" name="CLINETID" value="'.$_GET['CLINETID'].'"></form>'; // Два поля hidden для возврата GET
             
                $between = str_replace('-','.',$_POST ['between']);
                $and = str_replace('-','.',$_POST ['and']);
                
                   $QueryText="select pd.DESCRIPTION, count(pd.PATIENTID) as total, 
                (cast(count(pd.PATIENTID)as NUMERIC(10,2))*100 / (SELECT sum(total) FROM (
                select count(pd.PATIENTID) as total from (select * from (SELECT k.PATIENT_FK as PATIENTID, min(t.EXAMDATE) as EXAMDATE,
                                coalesce((select DESCRIPTION from PATIENTCARDDICTIONARIES where id = min(p.WHEREFROMID)),'".$locale_2[$locale]."') as DESCRIPTION
                                FROM TOOTHEXAM t
                                left join TREATMENTCOURSE k on t.TREATMENTCOURSE_FK = k.id
                                inner join patients p on k.PATIENT_FK = p.ID
                                where t.AFTERFLAG = 1
                                group by k.PATIENT_FK) where EXAMDATE BETWEEN '$between' AND '$and') as pd
                    group by pd.DESCRIPTION))) as present
                from (select * from (SELECT k.PATIENT_FK as PATIENTID, min(p.LASTNAME||' '||p.FIRSTNAME||' '||p.middlename) as PATIENT, min(t.EXAMDATE) as EXAMDATE,
                                coalesce((select DESCRIPTION from PATIENTCARDDICTIONARIES where id = min(p.WHEREFROMID)),'".$locale_2[$locale]."') as DESCRIPTION
                                FROM TOOTHEXAM t
                                left join TREATMENTCOURSE k on t.TREATMENTCOURSE_FK = k.id
                                inner join patients p on k.PATIENT_FK = p.ID
                                where t.AFTERFLAG = 1
                                group by k.PATIENT_FK) where EXAMDATE BETWEEN '$between' AND '$and') as pd
                    group by pd.DESCRIPTION order by total desc";
                
                      
                	$query = ibase_prepare($trans, $QueryText);
                	$result = ibase_execute($query);
                
                	$QueryTextP="select * from (SELECT min(p.LASTNAME||' '||p.FIRSTNAME||' '||p.middlename) as PATIENT, min(t.EXAMDATE) as EXAMDATE
                                FROM TOOTHEXAM t
                                left join TREATMENTCOURSE k on t.TREATMENTCOURSE_FK = k.id
                                inner join patients p on k.PATIENT_FK = p.ID
                                where t.AFTERFLAG = 1  and p.WHEREFROMID is NULL --and t.EXAMDATE BETWEEN '$between' AND '$and'
                                group by k.PATIENT_FK) where EXAMDATE BETWEEN '$between' AND '$and'";
                	$queryP = ibase_prepare($trans, $QueryTextP);
                	$resultP = ibase_execute($queryP);
                
                
                echo("<b>".$locale_3[$locale]." $between ".$locale_4[$locale]." $and </b><br/><br/>
                <table border='1' cellpadding='5'><tr>
                <td style='font-weight:bold'>".$locale_5[$locale]."</td>
                <td style='font-weight:bold'>".$locale_6[$locale]."</td>
                <td style='font-weight:bold'>".$locale_7[$locale]."</td>
                </tr>");
                
                		$total =0;
                		$unknow =0;
                    while ($obj = ibase_fetch_row($result)) {
                
                	echo("<tr><td".($obj[0]==$locale_2[$locale]?" style='color:red; '>$obj[0]":">$obj[0]")."</td><td style='text-align:center'>$obj[1]</td><td style='text-align:center'>$obj[2]</td></tr>");
                	$total = $total + $obj[1]; 
                	if($obj[0]==$locale_2[$locale]){
                			$unknow=1;
                			}	
                  	}
                
                	echo("<tr><td style='font-weight:bold'>".$locale_8[$locale]."</td><td style='font-weight:bold; text-align:center;'>$total</td><td style='font-weight:bold; text-align:center;'>100%</td></tr>");
                
                echo ("</table><br/><br/>");
                
                	if($unknow =="1"){
                			echo ($locale_9[$locale]."<br/>");
                			echo("<br/><br/>
                			<table border='1' cellpadding='5'><tr>
                			<td style='font-weight:bold'>".$locale_10[$locale]."</td>
                			</tr>");
                		while ($objP = ibase_fetch_row($resultP)) {
                		echo("<tr><td style='text-align:center'>$objP[0]</td></tr>");
                
                			}
                			echo ("</table><br/><br/>");
                
                
                			}	
                //file_put_contents("/home/zvitman/tmp.txt",var_export($unknow, true));
                
                		ibase_free_result($result);
                		ibase_free_result($resultP);
                
                }
                else
                
                {
                      
                
                echo '<script src="calendar_ru.js" type="text/javascript"></script>
                <form method="post" action="">
                
                
                <p><b>'.$locale_11[$locale].'</b><br>
                '.$locale_13[$locale].' <input type="text" name="between" value="" onfocus="this.select();lcs(this)"
                    onclick="event.cancelBubble=true;this.select();lcs(this)">
                
                '.$locale_4[$locale].' <input type="text" name="and" value="" onfocus="this.select();lcs(this)"
                    onclick="event.cancelBubble=true;this.select();lcs(this)"><br/><br/>
                
                <input type="submit" value="'.$locale_12[$locale].'">
                </p>
                </form>';
                
                }
                
                echo("</body>");
	
        
    }
    
    $connection->CloseDBConnection();
 }
?>

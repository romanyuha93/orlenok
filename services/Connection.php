<?php

//define('tfClientId','online');
date_default_timezone_set("Europe/Kiev");
mb_internal_encoding("UTF-8");

class Connection
{
	var $server = array("main"=>"localhost",
                        "log"=>"localhost");
	var $databasename = array("main"=>"C:/ToothFairyServer/Firebird/stomatology.fdb",
                                "log"=>"C:/ToothFairyServer/Firebird/stomatology_log.fdb");
	var $username = array("main"=>"SYSDBA",
                            "log"=>"SYSDBA");
	var $password = array("main"=>"masterkey",
                            "log"=>"masterkey");
 	var $role= array("main"=>"TFADMIN",
                            "log"=>null);

	var $connection;

	private static $openConnectionCount = 0;

	public function EstablishDBConnection($type="main",$withRole=true)
	{
		$curr_user = $this->username[$type];
		$curr_password = $this->password[$type];
		$tfClientId = tfClientId;
//        $tfClientId='online';

		if ($withRole && $type == "main" && $tfClientId != 'online') {
			$conn = ibase_connect($this->databasename[$type], $this->username[$type], $this->password[$type], null, null, null, $this->role[$type]);


			$QueryText = "Select upper(ca.tfuser) from CLIENTACCOUNT ca WHERE ca.CLIENT_ADDRESS='$tfClientId'";

			$result = ibase_query($conn, $QueryText);
			if ($row = ibase_fetch_row($result)) {
				if ($row[0] != null) {
					$curr_user = $row[0];
					$curr_password = "masterkey";
				}
			}
			ibase_free_result($result);
			ibase_close($conn);
		}

		$this->connection = ibase_connect($this->databasename[$type], $curr_user, $curr_password,
			null, null, null, $this->role[$type]) or null;

		if ($this->connection == null) {
			$e = new Exception('Create connection error. Possible multiple invokes from code during one call.');
			throw $e;
//		 	file_put_contents("C:/e.txt", $e->getTraceAsString());
		}

		Connection::$openConnectionCount++;
	}

	public function CloseDBConnection()
	{
		Connection::$openConnectionCount--;

		if (Connection::$openConnectionCount < 1) ibase_close($this->connection);
	}
}
?>

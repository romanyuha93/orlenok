<?php

require_once "Connection.php";
require_once "Utils.php";

class ChiefSalaryModuleOLAPData
{
    /**
     * @var int
     */
    var $doctorId;// DOCTORS -> ID
    /**
     * @var string
     */
    var $doctorName;// DOCTORS --> LASTNAME -> FIRSTNAME -> MIDDLENAME               Ivanov I.V.
    /**
     * @var number
     */
    var $doctorSalaryPercent;//in percents 20.00%
    /**
     * @var number
     */
    var $doctorSalaryStabil;//ставка врача
    /**
     * @var int
     */
    var $patientId;// PATIENTS -> ID
    /**
     * @var string
     */
    var $patientName;// PATIENTS --> LASTNAME -> FIRSTNAME -> MIDDLENAME            Petrov V.V.
    /**
     * @var int
     */
    var $procedureId;// TREATMENTPROC -> ID
    /**
     * @var string
     */
    var $procedureName;// TREATMENTPROC -> NAMEFORPLAN
    /**
     * @var string
     */
    var $procedurDate;// TREATMENTPROC -> NAMEFORPLAN
    /**
     * @var number
     */
    var $procedureSales;//TREATMENTPROC -> PRICE
    /**
     * @var number
     */
    var $procedureCount;//TREATMENTPROC -> PROC_COUNT
    /**
     * @var number
     */
    var $procedurExpenses;//TREATMENTPROC -> EXPENSES to-do
    /**
     * @var number
     */
    var $procedurExpensesPlane;//TREATMENTPROC -> EXPENSES to-do
    /**
     * @var int
     */
    var $procedurType;//TREATMENTPROC -> HEALTHTYPE
    /**
     * @var number
     */
    var $procedurSalary;//($ProcedureSales-$ProcedurExpenses)*(get DOCTOR percent by (TREATMENTPROC -> HEALTHTYPE))
    
    /**
     * @var number
     */
    var $doctorRewardPlus;
    
    /**
     * @var number
     */
    var $doctorRewardMinus;


    /**
     * @var number
     */
    var $procedurMaterialsExpenses;
    
    
    /**
     * @var string
     */
    var $invoiceNumber;
    /**
     * @var string
     */
    var $invoiceCreatedate;
    /*
    +DoctorName, 
    +PatientName, 
    ProcedurDate, 
    ProcedurType, 
    Sales,
    ProcedurName, 
    Salary,
    Expenses,
    SalaryPercent
    */
   
}


class ChiefSalaryModuleDoctorSalaryObject
{
    /**
     * @var string
     */
    var $doctorId;
    /**
     * @var string
     */
    var $schema_id;
    /**
     * @var string
     */
    var $createDate;
    /**
     * @var string
     */
    var $enabledFromDate;
    /**
     * @var string
     */
    var $salarySchema;
}


class ChiefSalaryModuleDoctorAwards
{
    /**
     * @var string
     */
    var $Id;
    /**
     * @var string
     */
    var $doctorId;
    /**
     * @var number
     */
    var $awardValue;
    /**
     * @var string
     */
    var $awardNotice;
    /**
     * @var string
     */
    var $awardDate;
    /**
     * @var string
     */
     var $datefoundaward;
}

//****************************** ASSISTANTS OBJECTS START ********************************
class ChiefSalaryModuleAssistantSalaryObject
{
    /**
     * @var string
     */
    var $assistantId;
    /**
     * @var string
     */
    var $schema_id;
    /**
     * @var string
     */
    var $createDate;
    /**
     * @var string
     */
    var $enabledFromDate;
    /**
     * @var string
     */
    var $salarySchema;
}
//****************************** ASSISTANTS OBJECTS END ********************************


class ChiefSalaryModuleDoctorSalarySummaryObject
{
    /**
     * @var string
     */
    var $doctor_id;
    /**
     * @var number
     */
    var $doctor_work_sum;
    /**
     * @var number
     */
    var $doctor_precent_work_sum;
    /**
     * @var number
     */
    var $doctor_salary;
    /**
     * @var number
     */
    var $doctor_in_salary_sum;
    /**
     * @var number
     */
    var $doctor_out_salary_sum;
}




//****************************** PROCEDURE EXPENSES START ********************************
class ChiefSalaryModule_TreatmentExpensesObject
{
    /**
     * @var string
     */
    var $accountflow_id;
    /**
     * @var number
     */
    var $accountflow_sum;
    /**
     * @var string
     */
    var $accountflow_note;
    /**
     * @var int
     */
    var $is_cashpayment;
    /**
     * @var string
     */
    var $accountflow_opertimestamp;
    /**
     * @var string
     */
    var $accountflow_createdate;
    
    
    /**
     * @var string
     */
    var $TEXP_TREATMENTPROC_FK;
    /**
     * @var string
     */
    var $TEXP_GROUP_FK;
    /**
     * @var string
     */
    var $TEXP_GROUP_NAME;
    /**
     * @var int
     */
    var $TEXP_GROUP_COLOR;
    /**
     * @var int
     */
    var $TEXP_GROUP_GRIDSEQUENCE;
}

class ChiefSalaryModule_ExpensesGroup
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $is_cashpayment;
    /**
     * @var int
     */
    var $group_color;
    /**
     * @var int
     */
    var $group_sequince;
}
class ChiefSalaryModule_ExpensesGroupSplit
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var int
     */
    var $sequince;
}
//****************************** PROCEDURE EXPENSES END **********************************

class ChiefSalaryModule
{
    
    /**
     * @param ChiefSalaryModuleOLAPData $p1
     * @param ChiefSalaryModuleDoctorSalaryObject $p2
     * @param ChiefSalaryModuleDoctorAwards $p3
     * @param ChiefSalaryModuleAssistantSalaryObject $p4
     * @param ChiefSalaryModule_TreatmentExpensesObject $p5
     * @param ChiefSalaryModule_ExpensesGroup $p6
     * @param ChiefSalaryModule_ExpensesGroupSplit $p7
     * @return void
     */
    public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7){}
    
    /**
     * @param string $startDate
     * @param string $endDate
     * @param string $startAwardDate
     * @param string $endAwardDate
     * @param string $moduleworktype //DOCTOR, ASSITANT
     * @param boolean $payidInProceduresMode
     * @param number $payidInProceduresPrecents
     * @param boolean $isBookkeppingExpenses
     * @param boolean $byInvoiceDate //default = false
     * @param boolean $considerDiscount
     * @param boolean $isCoursename
     * @param boolean $isFactExpenses
     * @return ChiefSalaryModuleOLAPData[]
     */
    public function getOLAPData($startDate, $endDate, $startAwardDate, $endAwardDate, $moduleworktype, $payidInProceduresMode, $payidInProceduresPrecents, $isBookkeppingExpenses, $byInvoiceDate, $considerDiscount, $isCoursename, $isFactExpenses)//MM/DD/YYYY
    {
        if($moduleworktype == 'ASSISTANT')
        {
            $workertabelname = 'ASSISTANTS';
            $workersalaryschematabelname = 'ASSISTANTSAWARDS';
            $workersalarytabelfc = 'assistantfc';
            $treatmentprocfield = 'assistant';
            $querySalarySettings = ' and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 3))';
        }
        else//if($moduleworktype == 'DOCTOR')
        {
            $workertabelname = 'DOCTORS';
            $workersalaryschematabelname = 'DOCTORSAWARDS';
            $workersalarytabelfc = 'doctorfc';
            $treatmentprocfield = 'doctor';
            $querySalarySettings = ' and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 2))';
        }
        
        if($payidInProceduresMode == true)
        {
            $calculationmode = "inner"; //left
        }
        else
        {
            $calculationmode = "left"; //
        }
        
        if($byInvoiceDate == true)
        {
            $calculationmode = "inner";
            $queryByDate = "and invoices.createdate >= '$startDate 00:00:00'
                            and invoices.createdate <= '$endDate 23:59:59'"; //left
        }
        else
        {
            $queryByDate = "and TREATMENTPROC.dateclose >= '$startDate 00:00:00'
                            and TREATMENTPROC.dateclose <= '$endDate 23:59:59'"; //
        }
        
        
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
	    $QueryText =
			"select 
            ".$workertabelname.".id,
            ".$workertabelname.".lastname, 
            ".$workertabelname.".firstname, 
            ".$workertabelname.".middlename, 
               
            PATIENTS.id, 
            PATIENTS.lastname, 
            PATIENTS.firstname, 
            PATIENTS.middlename, 
            
            
            TREATMENTPROC.id, 
            TREATMENTPROC.nameforplan, 
            TREATMENTPROC.dateclose, 
            TREATMENTPROC.price, 
            TREATMENTPROC.expenses, 
            TREATMENTPROC.healthtype, 
            
            treatmentproc.proc_count, 
            treatmentproc.discount_flag, 
            invoices.discount_amount, 
            invoices.id, 
            (select coalesce(sum(cast(treatmentprocfarm.farmprice*treatmentprocfarm.quantity as decimal(15,2))),0) 
                from treatmentprocfarm where treatmentprocfarm.treatmentproc = TREATMENTPROC.id),
                
                                                
            treatmentproc.acts_insurance,
            
            invoices.INVOICENUMBER,
            invoices.CREATEDATE,
            TREATMENTPROC.name,
            TREATMENTPROC.EXPENSES_FACT
            
              from TREATMENTPROC
                ".$calculationmode." join invoices on invoices.id = treatmentproc.invoiceid
                left join ".$workertabelname." on (TREATMENTPROC.".$treatmentprocfield." = ".$workertabelname.".id)
            
                left join DIAGNOS2 on (TREATMENTPROC.diagnos2 = DIAGNOS2.id)
                left join TREATMENTCOURSE on (DIAGNOS2.treatmentcourse = TREATMENTCOURSE.id)
                left join PATIENTS on (TREATMENTCOURSE.PATIENT_FK = PATIENTS.id)
            
              where  TREATMENTPROC.dateclose is not null
              and TREATMENTPROC.".$treatmentprocfield." is not null
              ".$queryByDate." ".$querySalarySettings." 
              order by ".$workertabelname.".id, TREATMENTPROC.dateclose desc, TREATMENTPROC.id";
                    
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
        
        $row19 = 'null';  
        $row20 = 'null';  
        $row21 = 'null'; 
        $row22 = 'null'; 
        $row23 = 'null';  
        
        $temp_user_id = '';
        $temp_patient_id = ''; 
        $temp_invoice_id = '';
        
		while ($row = ibase_fetch_row($result))
		{ 
		  // ********************* START ***********************
          if($temp_user_id != $row[0])
          {
             $temp_user_id = $row[0];
             
             $QueryTextR19 = "select  first 1 ".$workertabelname."_salary_schema.salaryschema
                    from ".$workertabelname."_salary_schema
                    where ".$workertabelname."_salary_schema.".$treatmentprocfield."id = $row[0]
                    and ".$workertabelname."_salary_schema.enabledfromdate <= '$row[10]'
                    order by  ".$workertabelname."_salary_schema.enabledfromdate desc";
            
             $QueryTextR20 = "select coalesce(sum(".$workersalaryschematabelname.".valueaward),0) 
                    from ".$workersalaryschematabelname."
                    where  ".$workersalaryschematabelname.".".$workersalarytabelfc." = $row[0]
                    and ".$workersalaryschematabelname.".valueaward >= 0
                    and ".$workersalaryschematabelname.".datefoundaward >= '$startAwardDate'
                    and ".$workersalaryschematabelname.".datefoundaward <= '$endAwardDate'";
                    
             $QueryTextR21 = "select coalesce(sum(".$workersalaryschematabelname.".valueaward),0) 
                    from ".$workersalaryschematabelname."
                    where  ".$workersalaryschematabelname.".".$workersalarytabelfc." = $row[0]
                    and ".$workersalaryschematabelname.".valueaward <= 0
                    and ".$workersalaryschematabelname.".datefoundaward >= '$startDate'
                    and ".$workersalaryschematabelname.".datefoundaward <= '$endDate'";
                    
             $queryR19 = ibase_prepare($trans, $QueryTextR19);
             $resultR19 = ibase_execute($queryR19);
             $rowR19 = ibase_fetch_row($resultR19);
             $row19 = $rowR19[0];
             $queryR20 = ibase_prepare($trans, $QueryTextR20);
             $resultR20 = ibase_execute($queryR20);
             $rowR20 = ibase_fetch_row($resultR20);
             $row20 = $rowR20[0];
             $queryR21 = ibase_prepare($trans, $QueryTextR21);
             $resultR21 = ibase_execute($queryR21);
             $rowR21 = ibase_fetch_row($resultR21);
             $row21 = $rowR21[0];
             
          }
          if(($temp_invoice_id != $row[17])&&($payidInProceduresMode == true))
          {
             $temp_invoice_id = $row[17]; 
             /*$QueryTextR22 = "select 
                                                    
                coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                 coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0)
                 
                        from treatmentproc
                    inner join invoices
                    on invoices.id = treatmentproc.invoiceid
                    where treatmentproc.invoiceid = $row[17]";
             $queryR22 = ibase_prepare($trans, $QueryTextR22);
             $resultR22 = ibase_execute($queryR22);
             $rowR22 = ibase_fetch_row($resultR22);
             $row22 = $rowR22[0];*/
            
              
             $QueryTextR23 = "select invoices.paidincash, invoices.summ
                    from invoices where invoices.id = $row[17]";
             $queryR23 = ibase_prepare($trans, $QueryTextR23);
             $resultR23 = ibase_execute($queryR23);
             $rowR23 = ibase_fetch_row($resultR23);
             $row23 = $rowR23[0];
             $row22 = $rowR23[1];
          }
          if($temp_patient_id != $row[4])
          { 
             $temp_patient_id = $row[4];
          }
          // *********************  END  ***********************          
          
		  if($payidInProceduresMode == true)
          {
            if(($row22 > 0)&&(($row23/$row22 >= $payidInProceduresPrecents)||(($row[19] != null)&&($row[19] != '')&&($row[19] != 'null'))))///$row[22] - сумма оплаты по счету / $row[21] - сумма выполненых процедур
            //if(($row[19] != null)&&($row[19] != '')&&($row[19] != 'null'))//$row[22] - сумма оплаты по счету / $row[21] - сумма выполненых процедур
            {
                $obj = new ChiefSalaryModuleOLAPData;
                $obj->procedureCount = $row[14];
                $obj->doctorId = $row[0];
                $obj->doctorName = "{$row[1]} {$row[2]} {$row[3]} (d)";
                $obj->patientId = $row[4];
                $obj->patientName = "{$row[5]} {$row[6]} {$row[7]} (p)";
                $obj->procedureId = $row[8];
                $obj->invoiceNumber = $row[20];
                $obj->invoiceCreatedate = $row[21];
                if(($row[19] != null)&&($row[19] != '')&&($row[19] != 'null'))
                {
                    $obj->procedureName = '(СТР) ';
                }
                else
                {
                    $obj->procedureName = '';
                }
                
                if($isCoursename == true)
                {
                   $obj->procedureName .= $row[22];
                }
                else
                {
                   $obj->procedureName .= $row[9];
                }
                
                $obj->procedurDate = date("d.m.y", strtotime($row[10]))." (c)";
                if($considerDiscount == true)
                {
                    $obj->procedureSales = $row[11]*$obj->procedureCount - round($row[11]*$obj->procedureCount*$row[15]*$row[16]/100, 2);
                }
                else
                {
                    $obj->procedureSales = $row[11]*$obj->procedureCount;
                }
                
                
                if($isFactExpenses == true) {
                    $obj->procedurExpenses = $row[23];
                    $obj->procedurExpensesPlane = $row[12]*$obj->procedureCount;
                }
                else {
                    $obj->procedurExpenses = $row[12]*$obj->procedureCount;
                    $obj->procedurExpensesPlane = $row[23];
                }
                
                $obj->procedurType = $row[13];
                
                $obj->doctorSalaryPercent = 0;
                $obj->doctorSalaryStabil = 0;
                if($row19)
                {
                    /*
                        <doctorsalary>
                			<operationtype type="0" precent="10"/>
                			<operationtype type="1" precent="10"/>
                			<operationtype type="2" precent="10"/>
                			<operationtype type="3" precent="10"/>
                			<operationtype type="4" precent="10"/>
                			<operationtype type="5" precent="10"/>
                			<operationtype type="6" precent="10"/>
                			<operationtype type="-1" precent="10"/>
                			
                			<salary positionsalary="1000"/>
            			
        			     </doctorsalary>
                    */
                    $salarySchemaXML = new SimpleXMLElement($row19);
                    foreach ($salarySchemaXML->children() as $item)
                    {
                        if(((string) $item['type'] == $obj->procedurType)&&($item['precent']))
                        {
                            $obj->doctorSalaryPercent = (float) $item['precent'];
                        }

                        if($item['positionsalary'])
                        {
                            $obj->doctorSalaryStabil = (float) $item['positionsalary'];
                        }
                    }
                }
                $obj->procedurMaterialsExpenses = $row[18]*$obj->procedureCount;
                if($isBookkeppingExpenses == true)
                {
                    $obj->procedurSalary = round(($obj->procedureSales - $obj->procedurExpenses)*$obj->doctorSalaryPercent/100, 2); 
                }
                else
                {
                    $obj->procedurSalary = round($obj->procedureSales*$obj->doctorSalaryPercent/100, 2);  
                }
    
                $obj->doctorRewardPlus = $row20;
                $obj->doctorRewardMinus = $row21;
                
                $rows[] = $obj;
             }
            }
            else
            {
                $obj = new ChiefSalaryModuleOLAPData;
                $obj->procedureCount = $row[14];
                $obj->doctorId = $row[0];
                $obj->doctorName = "{$row[1]} {$row[2]} {$row[3]} (d)";
                $obj->patientId = $row[4];
                $obj->patientName = "{$row[5]} {$row[6]} {$row[7]} (p)";
                $obj->procedureId = $row[8];
                $obj->invoiceNumber = $row[20];
                $obj->invoiceCreatedate = $row[21];
                if(($row[19] != null)&&($row[19] != '')&&($row[19] != 'null'))
                {
                    $obj->procedureName = '(СТР) ';
                }
                else
                {
                    $obj->procedureName = '';
                }
                 if($isCoursename == true)
                {
                   $obj->procedureName .= $row[22];
                }
                else
                {
                   $obj->procedureName .= $row[9];
                }
                $obj->procedurDate = date("d.m.y", strtotime($row[10]))." (c)";
                if($considerDiscount == true)
                {
                    $obj->procedureSales = $row[11]*$obj->procedureCount - round($row[11]*$obj->procedureCount*$row[15]*$row[16]/100, 2);
                }
                else
                {
                    $obj->procedureSales = $row[11]*$obj->procedureCount;
                }
                //$obj->procedureSales = $row[11]*$obj->procedureCount;//001 - round($row[11]*$obj->procedureCount*$row[15]*$row[16]/100, 2);
                if($isFactExpenses == true) {
                    $obj->procedurExpenses = $row[23];
                    $obj->procedurExpensesPlane = $row[12]*$obj->procedureCount;
                }
                else {
                    $obj->procedurExpenses = $row[12]*$obj->procedureCount;
                    $obj->procedurExpensesPlane = $row[23];
                }
                $obj->procedurType = $row[13];
                
                $obj->doctorSalaryPercent = 0;
                $obj->doctorSalaryStabil = 0;
                if($row19)
                {
                    /*
                        <doctorsalary>
                			<operationtype type="0" precent="10"/>
                			<operationtype type="1" precent="10"/>
                			<operationtype type="2" precent="10"/>
                			<operationtype type="3" precent="10"/>
                			<operationtype type="4" precent="10"/>
                			<operationtype type="5" precent="10"/>
                			<operationtype type="6" precent="10"/>
                			<operationtype type="-1" precent="10"/>
                			
                			<salary positionsalary="1000"/>
            			
        			     </doctorsalary>
                    */
                    $salarySchemaXML = new SimpleXMLElement($row19);
                    foreach ($salarySchemaXML->children() as $item)
                    {
                        if(((string) $item['type'] == $obj->procedurType)&&($item['precent']))
                        {
                            $obj->doctorSalaryPercent = (float) $item['precent'];
                        }   
                        
                        if($item['positionsalary'])
                        {
                            $obj->doctorSalaryStabil = (float) $item['positionsalary'];
                        }
                    }
                }
                $obj->procedurMaterialsExpenses = $row[18]*$obj->procedureCount;
                
                
                if($isBookkeppingExpenses == true)
                {
                    $obj->procedurSalary = round(($obj->procedureSales - $obj->procedurExpenses)*$obj->doctorSalaryPercent/100, 2); 
                }
                else
                {
                    $obj->procedurSalary = round($obj->procedureSales*$obj->doctorSalaryPercent/100, 2);  
                }
    
                $obj->doctorRewardPlus = $row20;
                $obj->doctorRewardMinus = $row21;
                
                $rows[] = $obj;
            }
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
        
    }
    

    
    /**
     * @param int $procedureId
     * @param number $procedureExpensesValue
     * @return boolean
     */
    public function updateProcedurExpensesValue($procedureId, $procedureExpensesValue)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update 
                    TREATMENTPROC set 
                    EXPENSES='$procedureExpensesValue'
                    where ID=$procedureId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
     * @param int $doctorId
     * @param number $awardValue
     * @param string $awardNotice
     * @param string $awardDate
     * @param string $awardFoundDate
     * @param string $moduleworktype
     * @param boolean $isAddToFlow
	 * @param int $isCashpayment
     * @return boolean
     */
    public function addDoctorAward($doctorId, $awardValue, $awardNotice, $awardDate, $awardFoundDate, $moduleworktype, $isAddToFlow, $isCashpayment)
    {
        if($moduleworktype == 'ASSISTANT')
        {
            $workerawardstabelname = 'ASSISTANTSAWARDS';
            $workersalarytabelfc = 'assistantfc';
        }
        else//if($moduleworktype == 'DOCTOR')
        {
            $workerawardstabelname = 'DOCTORSAWARDS';
            $workersalarytabelfc = 'doctorfc';
        }
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "INSERT into ".$workerawardstabelname."(".$workersalarytabelfc.", valueaward, notice, dateaward, datefoundaward)
                    values ($doctorId, $awardValue, '".safequery($awardNotice)."', '$awardDate', '$awardFoundDate')
                    returning id";
                    
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $rowId = ibase_fetch_row($result);
        $awardID = $rowId[0];
        ibase_free_query($query);
        ibase_free_result($result);
        
        // add accountflow for salary
        if(($isAddToFlow == true)&&($moduleworktype == 'DOCTOR'))
        {
            // query accountflow for salary
            	$QueryText = "insert into accountflow (accountflow.summ, accountflow.operationtype, 
                                accountflow.operationnote, accountflow.operationtimestamp, accountflow.is_cashpayment, accountflow.group_fk,
                                accountflow.award_fk)
                    values('".-floatval($awardValue)."', 2, '', ";
                if(($awardFoundDate != null)&&($awardFoundDate != 'null')&&($awardFoundDate != '')&&($awardFoundDate != '-'))
                {
                     $QueryText .= "'".$awardDate."', ";
                }
                else
                {
                    $QueryText .= "null, ";
                }           
                $QueryText .= "$isCashpayment, null, $awardID)";         
                $query = ibase_prepare($trans, $QueryText);
        		ibase_execute($query);
        		ibase_free_query($query);
        }
        
        
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
     * @param int $doctorId
     * @param string $startDate
     * @param string $endDate
     * @param boolean $isPlus
     * @param string $moduleworktype
     * @return ChiefSalaryModuleDoctorAwards[]
     */
    public function getDoctorAwards($doctorId, $startDate, $endDate, $isPlus, $moduleworktype)
    {
        
        if($moduleworktype == 'ASSISTANT')
        {
            $workerawardstabelname = 'ASSISTANTSAWARDS';
            $workersalarytabelfc = 'assistantfc';
        }
        else//if($moduleworktype == 'DOCTOR')
        {
            $workerawardstabelname = 'DOCTORSAWARDS';
            $workersalarytabelfc = 'doctorfc';
        }
        
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        /*if($isPlus)
        {
	       $QueryText =
			"select ".$workerawardstabelname.".id, ".$workerawardstabelname.".".$workersalarytabelfc.", ".$workerawardstabelname.".valueaward, 
                ".$workerawardstabelname.".notice, ".$workerawardstabelname.".dateaward, ".$workerawardstabelname.".datefoundaward
                from ".$workerawardstabelname." 
                where ".$workerawardstabelname.".valueaward > 0
                and ".$workerawardstabelname.".".$workersalarytabelfc." = $doctorId
                and ".$workerawardstabelname.".dateaward >= '$startDate'
                and ".$workerawardstabelname.".dateaward <= '$endDate'";
        }
        else
        {
	       $QueryText =
			"select ".$workerawardstabelname.".id, ".$workerawardstabelname.".".$workersalarytabelfc.", ".$workerawardstabelname.".valueaward, 
                ".$workerawardstabelname.".notice, ".$workerawardstabelname.".dateaward
                from ".$workerawardstabelname." 
                where ".$workerawardstabelname.".valueaward < 0
                and ".$workerawardstabelname.".".$workersalarytabelfc." = $doctorId
                and ".$workerawardstabelname.".dateaward >= '$startDate'
                and ".$workerawardstabelname.".dateaward <= '$endDate'";
        }*/
        
        if($isPlus)
        {
	       $QueryText =
			"select ".$workerawardstabelname.".id, ".$workerawardstabelname.".".$workersalarytabelfc.", ".$workerawardstabelname.".valueaward, 
                ".$workerawardstabelname.".notice, ".$workerawardstabelname.".dateaward, ".$workerawardstabelname.".datefoundaward
                from ".$workerawardstabelname." 
                where ".$workerawardstabelname.".valueaward > 0
                and ".$workerawardstabelname.".".$workersalarytabelfc." = $doctorId
                and ".$workerawardstabelname.".datefoundaward >= '$startDate'
                and ".$workerawardstabelname.".datefoundaward <= '$endDate'";
        }
        else
        {
	       $QueryText =
			"select ".$workerawardstabelname.".id, ".$workerawardstabelname.".".$workersalarytabelfc.", ".$workerawardstabelname.".valueaward, 
                ".$workerawardstabelname.".notice, ".$workerawardstabelname.".dateaward, ".$workerawardstabelname.".datefoundaward
                from ".$workerawardstabelname." 
                where ".$workerawardstabelname.".valueaward < 0
                and ".$workerawardstabelname.".".$workersalarytabelfc." = $doctorId
                and ".$workerawardstabelname.".datefoundaward >= '$startDate'
                and ".$workerawardstabelname.".datefoundaward <= '$endDate'";
        }
              
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new ChiefSalaryModuleDoctorAwards;
            $obj->Id = $row[0];
            $obj->doctorId = $row[1];
            $obj->awardValue = $row[2];
            $obj->awardNotice = $row[3];
            $obj->awardDate = $row[4];
            $obj->datefoundaward = $row[5];
            
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    /**
     * @param int $awardId
     * @param string $moduleworktype
     * @return boolean
     */
    public function deleteDoctorAward($awardId, $moduleworktype)
    {
        
        if($moduleworktype == 'ASSISTANT')
        {
            $workerawardstabelname = 'ASSISTANTSAWARDS';
        }
        else//if($moduleworktype == 'DOCTOR')
        {
            $workerawardstabelname = 'DOCTORSAWARDS';
        }
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from ".$workerawardstabelname." where ".$workerawardstabelname.".id = $awardId";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
     * @param string $doctorID
     * @return ChiefSalaryModuleDoctorSalaryObject[]
     */
    public function getDoctorSalarySchema($doctorID)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		/*$QueryText = "select SALARYSCHEMA from DOCTORS where ID=$doctorID";*/
        $QueryText = "select doctors_salary_schema.salaryschema, doctors_salary_schema.createdate, doctors_salary_schema.enabledfromdate, doctors_salary_schema.id
                    from doctors_salary_schema
                    where doctors_salary_schema.doctorid = $doctorID
                    order by doctors_salary_schema.enabledfromdate desc";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new ChiefSalaryModuleDoctorSalaryObject;
          $obj->doctorId = $doctorID;
          $obj->salarySchema = $row[0];
          $obj->createDate = $row[1];
          $obj->enabledFromDate = $row[2];
          $obj->schema_id = $row[3];
          $rows[] = $obj;
        }
        ibase_commit($trans);
		ibase_free_result($result);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $rows;
    }
    /**
     * @param string $schemaID
     * @return boolean
     */
    public function deleteDoctorSalarySchemaById($schemaID)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		/*$QueryText = "select SALARYSCHEMA from DOCTORS where ID=$doctorID";*/
        $QueryText = "delete from doctors_salary_schema
                    where doctors_salary_schema.id = $schemaID";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $result = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $result;
    }
    
    /**
     * @param ChiefSalaryModuleDoctorSalaryObject $schemaObject
     * @return boolean
     */
    public function saveDoctorSalarySchema($schemaObject)//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		/*$QueryText = "update 
                    DOCTORS set 
                    SALARYSCHEMA='$schemaObject->salarySchema'
                    where ID=$schemaObject->doctorId";*/
        $QueryText = "insert into doctors_salary_schema(doctors_salary_schema.doctorid, doctors_salary_schema.salaryschema, doctors_salary_schema.enabledfromdate)
                values($schemaObject->doctorId, '$schemaObject->salarySchema', '$schemaObject->enabledFromDate 00:00:00')";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
     * @param string $doctorID
     * @param string $reportDate
     * @return ChiefSalaryModuleDoctorSalarySummaryObject
     */
    public function getDoctorSalary($doctorID, $reportDate)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select 
                 coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                 coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0)
                          from invoices
                    inner join treatmentproc
                    on treatmentproc.invoiceid = invoices.id
                    where treatmentproc.doctor = $doctorID";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$row = ibase_fetch_row($result);
        
		  $obj = new ChiefSalaryModuleDoctorSalarySummaryObject;
          $obj->doctor_id = $doctorID;
          $obj->doctor_work_sum = $row[0];
          
        ibase_commit($trans);
		ibase_free_result($result);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $obj;
    }
    
//****************************** ASSISTANTS FUNCTIONS START ********************************
    /**
     * @param string $assistantID
     * @return ChiefSalaryModuleAssistantSalaryObject[]
     */
    public function getAssistantSalarySchema($assistantID)//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		/*$QueryText = "select SALARYSCHEMA from ASSISTANTS where ID=$assistantID";*/
         $QueryText = "select assistants_salary_schema.salaryschema, assistants_salary_schema.createdate, assistants_salary_schema.enabledfromdate, assistants_salary_schema.id
                    from assistants_salary_schema
                    where assistants_salary_schema.assistantid = $assistantID
                    order by  assistants_salary_schema.enabledfromdate desc";
                    
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new ChiefSalaryModuleAssistantSalaryObject;
          $obj->assistantId = $assistantID;
          $obj->salarySchema = $row[0];
          $obj->createDate = $row[1];
          $obj->enabledFromDate = $row[2];
          $obj->schema_id = $row[3];
          $rows[] = $obj;
        }
        ibase_commit($trans);
		ibase_free_result($result);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $rows;
    }
    
    /**
     * @param string $schemaID
     * @return boolean
     */
    public function deleteAssistantSalarySchemaById($schemaID)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		/*$QueryText = "select SALARYSCHEMA from DOCTORS where ID=$doctorID";*/
        $QueryText = "delete from assistants_salary_schema
                    where assistants_salary_schema.id = $schemaID";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $result = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $result;
    }
    
    /**
     * @param ChiefSalaryModuleAssistantSalaryObject $schemaObject
     * @return boolean
     */
    public function saveAssistantSalarySchema($schemaObject)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		/*$QueryText = "update 
                    ASSISTANTS set 
                    SALARYSCHEMA='$schemaObject->salarySchema'
                    where ID=$schemaObject->assistantId";*/
        $QueryText = "insert into assistants_salary_schema(assistants_salary_schema.assistantid, assistants_salary_schema.salaryschema, assistants_salary_schema.enabledfromdate)
                        values($schemaObject->assistantId, '$schemaObject->salarySchema', '$schemaObject->enabledFromDate 00:00:00')";
                        
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
//****************************** ASSISTANTS FUNCTIONS END ********************************


//****************************** PROCEDURE EXPENSES START ********************************
    /**
     * @param string $treatmentprocId
     * @return ChiefSalaryModule_TreatmentExpensesObject[]
     */
    public function getTreatmentExpenses($treatmentprocId)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
         $expenses_sql = "select  
                            accountflow.id,
                            accountflow.summ,
                            accountflow.operationnote,
                            accountflow.is_cashpayment,
                            accountflow.operationtimestamp, 
                            accountflow.createdate,
                    
                            accountflow_texp_groups.id,
                            accountflow_texp_groups.name,
                            accountflow_texp_groups.color,
                            accountflow_texp_groups.gridsequence
                
                                    from accountflow
                                    left join accountflow_texp_groups
                                    on accountflow.texp_group_fk = accountflow_texp_groups.id
                                    
                        where accountflow.texp_treatmentproc_fk = $treatmentprocId";
                    
		$expenses_query = ibase_prepare($trans, $expenses_sql);
		$expenses_result = ibase_execute($expenses_query);
        $rows = array();
		while ($row = ibase_fetch_row($expenses_result))
		{ 
		  $obj = new ChiefSalaryModule_TreatmentExpensesObject;
          $obj->accountflow_id = $row[0];
          $obj->accountflow_sum = $row[1];
          $obj->accountflow_note = $row[2];
          $obj->is_cashpayment = $row[3];
          $obj->accountflow_opertimestamp = $row[4];
          $obj->accountflow_createdate = $row[5];
          
          $obj->TEXP_GROUP_FK = $row[6];
          $obj->TEXP_GROUP_NAME = $row[7];
          $obj->TEXP_GROUP_COLOR = $row[8];
          $obj->TEXP_GROUP_GRIDSEQUENCE = $row[9];
          $obj->TEXP_TREATMENTPROC_FK = $treatmentprocId;
          $rows[] = $obj;
        }
        ibase_commit($trans);
		ibase_free_result($expenses_result);
		ibase_free_query($expenses_query);
		$connection->CloseDBConnection();
		return $rows;
    }
    /**
     * @param ChiefSalaryModule_TreatmentExpensesObject $treatmentprocExpenses
     * @return boolean
     */
    public function saveTreatmentExpenses($treatmentprocExpenses)
    {
        
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if( ($treatmentprocExpenses->accountflow_id == null)||($treatmentprocExpenses->accountflow_id == 'null')||($treatmentprocExpenses->accountflow_id == '-') ) {
            $treatmentprocExpenses->accountflow_id = 'null';
        }
        $expenses_sql = "insert into accountflow 
                                        (ID, SUMM, OPERATIONTYPE, OPERATIONNOTE, OPERATIONTIMESTAMP,
                                          IS_CASHPAYMENT, TEXP_GROUP_FK, TEXP_TREATMENTPROC_FK)
                                values (
                                $treatmentprocExpenses->accountflow_id,
                                '-$treatmentprocExpenses->accountflow_sum', 
                                3, 
                                '".safequery($treatmentprocExpenses->accountflow_note)."',
                                '$treatmentprocExpenses->accountflow_opertimestamp', 
                                $treatmentprocExpenses->is_cashpayment,
                                $treatmentprocExpenses->TEXP_GROUP_FK,
                                $treatmentprocExpenses->TEXP_TREATMENTPROC_FK)
                                ";
                    
		$expenses_query = ibase_prepare($trans, $expenses_sql);
		ibase_execute($expenses_query);
        $success = ibase_commit($trans);
		ibase_free_query($expenses_query);
		$connection->CloseDBConnection();
		return $success;
    }
      /** 
      * @param ChiefSalaryModule_TreatmentExpensesObject $treatmentprocExpenses
      * @return boolean
      */  
      public function updateTreatmentExpenses($treatmentprocExpenses)
      {
            $connection = new Connection();
            $connection->EstablishDBConnection();
    		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
    		$update_sql = "update ACCOUNTFLOW set
                                ACCOUNTFLOW.OPERATIONNOTE = '".safequery($treatmentprocExpenses->accountflow_note)."'
                            where ACCOUNTFLOW.ID = ".$treatmentprocExpenses->accountflow_id;
                 
            $update_query = ibase_prepare($trans, $update_sql);
    		ibase_execute($update_query);
    		ibase_free_query($update_query);
            $success = ibase_commit($trans);
    		$connection->CloseDBConnection();
    		return $success;
      }
    /**
     * @param string $expensesId
     * @return boolean
     */
    public function deleteTreatmentExpenses($expensesId)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $expenses_sql = "delete from accountflow
                    where accountflow.id = $expensesId";
		$expenses_query = ibase_prepare($trans, $expenses_sql);
		ibase_execute($expenses_query);
        $success = ibase_commit($trans);
		ibase_free_query($expenses_query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    
    /** 
    * @param boolean $isEmptyGroup
    * @return ChiefSalaryModule_ExpensesGroup[]
    */  
    public function getExpensesGroups($isEmptyGroup)//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$group_sql = "select 
                            ACCOUNTFLOW_TEXP_GROUPS.ID, 
                            ACCOUNTFLOW_TEXP_GROUPS.NAME, 
                            ACCOUNTFLOW_TEXP_GROUPS.IS_CASHPAYMENT, 
                            ACCOUNTFLOW_TEXP_GROUPS.COLOR, 
                            ACCOUNTFLOW_TEXP_GROUPS.GRIDSEQUENCE
                        from ACCOUNTFLOW_TEXP_GROUPS 
                       
                        order by ACCOUNTFLOW_TEXP_GROUPS.GRIDSEQUENCE asc, ACCOUNTFLOW_TEXP_GROUPS.NAME asc";                                        
        
        $group_query = ibase_prepare($trans, $group_sql);
        $group_result=ibase_execute($group_query);
        $rows = array();
        
        if($isEmptyGroup == true)
        {
            $emptyGroup = new ChiefSalaryModule_ExpensesGroup;
            $emptyGroup->id = 'null';
            $emptyGroup->name = 'emptyGroup';
            $rows[] = $emptyGroup; 
        }
		while ($row = ibase_fetch_row($group_result))
		{ 
            $obj = new ChiefSalaryModule_ExpensesGroup;
            $obj->id = $row[0];
            $obj->name = $row[1];
            $obj->is_cashpayment = $row[2];
            $obj->group_color = $row[3];
            $obj->group_sequince = $row[4];
            
            $rows[] = $obj;
		}
        ibase_free_query($group_query);
        ibase_free_result($group_result);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
    }
    
    /**
	* @param string $groupId
	* @param string $passForDelete
	* @return boolean
 	*/  
    public function removeExpensesGroup($groupId, $passForDelete) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               ibase_free_result($result);
        	   ibase_free_query($query);
        	   $QueryText = "delete from accountflow_texp_groups where accountflow_texp_groups.id = '$groupId'";
        	   $query = ibase_prepare($trans, $QueryText);
        	   ibase_execute($query);
        	   ibase_free_query($query);
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
    }
    /** 
    * @param ChiefSalaryModule_ExpensesGroup $group
    * @return ChiefSalaryModule_ExpensesGroup
    */  
    public function saveExpensesGroup($group)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
          
		$QueryText = "update or insert into accountflow_texp_groups
                            (accountflow_texp_groups.id,
                            accountflow_texp_groups.name,
                            accountflow_texp_groups.is_cashpayment,
                            accountflow_texp_groups.color)
                        
                        values (";
           
        if(($group->id != null)&&($group->id != 'null')&&($group->id != ''))
        {
            $QueryText .=  "$group->id, "; 
        } 
        else
        {
            $QueryText .=  "null, ";
        } 
        
        $QueryText .=  "'".safequery($group->name)."', ";                 
         
           
        $QueryText .=  "$group->is_cashpayment, $group->group_color) matching (id) returning id";
             
        $query = ibase_prepare($trans, $QueryText);
        $resultQuery = ibase_execute($query);
        $rowIdOrder = ibase_fetch_row($resultQuery);
        $group->id = $rowIdOrder[0];
        ibase_free_query($query);
        ibase_free_result($resultQuery);
           
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $group;
    }
    /** 
    * @param ChiefSalaryModule_ExpensesGroupSplit[] $groups
    * @return boolean
    */
    public function splitExpensesGroups($groups)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        foreach ($groups as $group)
        { 
            $QueryText = "update accountflow_texp_groups set
                            accountflow_texp_groups.gridsequence = $group->sequince
                        where accountflow_texp_groups.id = $group->id";
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            ibase_free_query($query);
        }
           
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $success;
    } 
//****************************** PROCEDURE EXPENSES END **********************************

}
?>
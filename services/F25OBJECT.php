<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

define('dentdis', serialize(array('C', 'Cd', 'Pl', 'F', 'r', 'H', 'P', 'Pt', 'Lp', '', 'R', 'A', 'ar', 'Am', 'rez', 'pin', 'I', 'Rp', 'Z', 'Lp', 'Lp', 'Gv', 'Gv', 'Gv', 'I', 'tPL', 'Lps', 'Htd', 'Fl', 'Tb', 'OP', 'SC', 'BT', 'WSD','INT', 'Сob','Pldf')));
define('dentdiscrown', serialize(array('C', 'Cd', 'Pl', 'F', 'r', 'H', 'P', 'Pt', '', '', 'R', 'A', 'ar', 'Am', 'rez', 'pin', 'I', 'Rp', '', '', '', '', '', '', 'I', 'tPL', '', 'Htd', 'Fl', 'Tb', 'OP', 'SC', 'BT', 'WSD','INT', 'Сob','Pldf')));
define('dentdisgum', serialize(array('', '', '', '', '', '', '', '', 'Lp', '', '', '', '', '', '', '', '', '', 'Z', 'Lp', 'Lp', 'Gv', 'Gv', 'Gv', '', '', 'Lps', '', '', '', '', '', '', '','', '','')));

//UA
define('dentdiswordall', serialize(array('карiєс', 'коронка', 'пломба', 'фасетка', 'реставрація', 'гемісекція', 'пульпіт', 'періодонтит', 'пародонтит', '', 'корінь', 'відсутній зуб', 'штучний зуб', 'ампутація', 'резекція', 'штифт', 'імплантат з коронкою', 'реплантація', 'зубний камінь', 'генералізований пародонтит', 'пародонтит', 'генералізований гінгівіт', 'гінгівіт', 'гінгівіт', 'імплантат без коронки', 'тимчасова пломба', 'пародонтоз', 'дефект твердих тканин', 'флюороз', 'вкладка', 'ортодонтична патологія', 'вторинний карієс', 'скол', 'клиновидний дефект','інтактний', 'карієс під спостереженням', ' пломба дефект')));
define('dentdisword', serialize(array('карiєс', '', '', '', '', '', 'пульпіт', 'періодонтит', 'пародонтит', '', '', '', '', '', '', '', '', '', 'зубний камінь', 'генеральный пародонтит', 'пародонтит', 'генеральный гінгівіт', 'гінгівіт', 'гінгівіт', '', '', 'пародонтоз', 'дефект твердих тканин', 'флюороз', '', 'ортодонтична патологія', 'вторинний карієс', 'скол', 'клиновидний дефект','інтактний', 'карієс під спостереженням', ' пломба дефект')));
//RU
//define('dentdiswordall', serialize(array('кариес', 'коронка', 'пломба', 'фасетка', 'реставрация', 'гемисекция', 'пульпит', 'периодонтит', 'пародонтит', '', 'корень', 'отсутствует зуб', 'одиночный зуб', 'ампутация', 'резекция', 'штифт', 'имплантат с коронкой', 'реплантация', 'зубной камень', 'генерализованный пародонтит', 'пародонтит', 'генерализованный гингивит', 'гингивит', 'гингивит', 'имплантат без коронки', 'временная пломба', 'пародонтоз', 'дефект твердых тканей', 'флюороз', 'вкладка', 'ортодонтическая патология', 'вторинний кариес', 'скол', 'клиновидний дефект','интактный', 'кариес под наблюдением', ' пломба дефект')));
//define('dentdisword', serialize(array('кариес', '', '', '', '', '', 'пульпит', 'периодонтит', 'пародонтит', '', '', '', '', '', '', '', '', '', 'зубной камень', 'генеральный пародонтит', 'пародонтит', 'генеральный гингивит', 'гингивит', 'гингивит', '', '', 'пародонтоз', 'дефект твердых тканей', 'флюороз', '', 'ортодонтическая патология', 'вторичный кариес', 'скол', 'клиновидный дефект','интактный', 'кариес под наблюдением', ' пломба дефект')));


define('toothimgs' , serialize(array( 'b3u','b3u','b3u','b1u','b2u','l1u','l1u','l1u','l1u','l1u','l1u','b2u','b1u','b3u','b3u','b3u',
                            'b2d','b2d','b2d','b1d','b1d','l1d','l1d','l1d','l1d','l1d','l1d','b1d','b1d','b2d','b2d','b2d')));
define('toothimgsmilk' , serialize(array( 'Null','Null','Null','b3u','b3u','b1u','l1u','l1u','l1u','l1u','b1u','b3u','b3u','Null','Null','Null',
                                'Null','Null','Null','b2d','b2d','b1d','l1d','l1d','l1d','l1d','b1d','b2d','b2d','Null','Null','Null')));                               
//define('toothtexts' , serialize(array('8','7','6','5','4','3','2','1','1','2','3','4','5','6','7','8')));
//define('toothtextsmilk' , serialize(array('','','','V','IV','III','II','I','I','II','III','IV','V','','','')));
define('toothtexts' , serialize(array('8','7','6','5(V)','4(IV)','3(III)','2(II)','1(I)','1(I)','2(II)','3(III)','4(IV)','5(V)','6','7','8')));
define('toothtextsmilk' , serialize(array('8','7','6','5(V)','4(IV)','3(III)','2(II)','1(I)','1(I)','2(II)','3(III)','4(IV)','5(V)','6','7','8')));

define('toothid' , serialize(array('18', '17', '16', '15', '14', '13', '12', '11', '21', '22', '23', '24', '25', '26', '27', '28', '48', '47', '46', '45', '44', '25', '42', '41', '31', '32', '33', '34', '35', '36', '37', '38')));
define('toothbone', serialize(array('N','¾','½','¼')));
define('toothmobility', serialize(array('','I','II','III')));
//$st=timeMeasure();
//$F25=new F25OBJECT('2', true, true, true, true, true, true, true);
//print((timeMeasure()-$st)."\n");
//print_r($F25);

class F25OBJECT
{	
    /**
	* @var string
	*/
   var $PATIENT_ID;
    
    /**
    * @var PrintDataModule_ClinicRegistrationData
    */
    var $CLINIC_DATA;
    
    /**
    * @var PrintDataModule_PatientData
    */  
    var $PATIENT_DATA;
    
	/**
    * @var PrintDataModule_TEST
    */  
    var $TEST;
	
	
    /**
    * @var PatientAmbcard
    */  
    var $PATIENT_AMB;
    
    /**
    * @var ToothExam[]
    */  
    var $TOOTHEXAM_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_FIRST_SPECIFIED;
    
    /**
    * @var string
    */  
    var $DIAGNOS_FIRST_SPECIFIED_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_FIRST_ALL_SPECIFIED;
    
    /**
    * @var array
    */  
    var $DIAGNOS_FIRST_ALL_SPECIFIED_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_ALL_SPECIFIED;
    
    /**
    * @var array
    */  
    var $DIAGNOS_ALL_SPECIFIED_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_25_FIRST_ALL;
    
    /**
    * @var array
    */  
    var $DIAGNOS_25_FIRST_ALL_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_25_ALL;
    
    /**
    * @var array
    */  
    var $DIAGNOS_25_ALL_ARRAY;
    
    /**
    * @var string
    */  
    var $COMPLAINTS_FIRST_EXAM;
    
    /**
    * @var array
    */  
    var $COMPLAINTS_FIRST_EXAM_ARRAY;
    
    /**
    * @var string
    */  
    var $COMPLAINTS_ALL_EXAM;
    
    /**
    * @var array
    */  
    var $COMPLAINTS_ALL_EXAM_ARRAY;
    
    /**
    * @var string
    */
    var $DISEASEHISTORY_FIRST_EXAM; 
    
    /**
    * @var array
    */
    var $DISEASEHISTORY_FIRST_EXAM_ARRAY; 
    
    /**
    * @var string
    */
    var $DISEASEHISTORY_ALL_EXAM; 
    
    /**
    * @var array
    */
    var $DISEASEHISTORY_ALL_EXAM_ARRAY; 
    
    /**
    * @var string
    */  
    var $DATE_FIRST_EXAM;
    
    /**
    * @var string
    */
    var $COMORBIDITIES; 
    
    /**
    * @var array
    */
    var $COMORBIDITIES_ARRAY; 
    
    /**
    * @var date
    */  
    var $DATE_CURRENT;

    /**
    * @var Array
    */  
    var $DIARY;
	
	/**
    * @var array()
    */  
    var $DIAGNOSIS;
	

    
    function __construct($patient_id, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag, $sql_choice="TOOTHEXAM.AFTERFLAG = 0")     
    { 
        $connection = new Connection();
  		$connection->EstablishDBConnection();
  		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection); 
        
        $this->PATIENT_ID=$patient_id;
        
        
        $this->CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(false, $trans);
        $this->PATIENT_DATA=new PrintDataModule_PatientData($patient_id, $trans);
		$this->TEST=new PrintDataModule_TEST($patient_id, $trans);
		$this->DIAGNOSIS=PrintDataModule_DIAGNOSIS($patient_id, $trans);
        
        //$this->PATIENT_AMB=new PatientAmbcard($patient_id, $trans);
        //$this->COMORBIDITIES=$this->PATIENT_AMB->COMORBIDITIES;
        $this->TOOTHEXAM_ARRAY=$this->getToothexamArray($patient_id, $sql_choice, $trans);
        $complaints_all_exam='';
        $diseasehistory_all_exam='';
        $diagnos_all_specified='';  
        $diagnos_25_all_specified=''; 
        $diagnos_first_all_specified=''; 
        $diary=array();  
        foreach($this->TOOTHEXAM_ARRAY as $key=>$toothexam)
        {
            if($toothexam->COMPLAINTS != "")
                $complaints_all_exam.=($complaints_all_exam==''?'':'; ').$toothexam->EXAMDATE.': '.$toothexam->COMPLAINTS;
            if($toothexam->DISEASEHISTORY != "")
            $diseasehistory_all_exam.=($diseasehistory_all_exam==''?'':'; ').$toothexam->EXAMDATE.': '.$toothexam->DISEASEHISTORY;
            if($toothexam->DIAGNOSIS != "")
                $diagnos_25_all_specified.=($diagnos_25_all_specified==''?'':'; ').$toothexam->EXAMDATE.': '.$toothexam->DIAGNOSIS;
            //file_put_contents("D:/tmp.txt",  file_get_contents("D:/tmp.txt")."\n".$complaints_all_exam.var_export($diary, true));
            $diary1=F25OBJECT::getDoctorDiary($toothexam->TREATMENT_COURSE_ID, null, null, null, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag, $trans);
//             file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt")."\nDiary1".var_export($diary1, true));
            $diary=array_merge_recursive($diary,$diary1);                        
//            file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt")."\nDiaryAfter".var_export($diary, true));
            $diagnos_treatment=getDiagnosTreatment($toothexam->TREATMENT_COURSE_ID, null, null, null, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag, $trans);

            foreach($diagnos_treatment[0] as $d=>$dd)
            {
                $diagnos_all_specified.=($diagnos_all_specified==''?'':'; ').$dd["DATE"].': '.$dd["TEXT"];
                if($key==0)
                {
                    $diagnos_first_all_specified.=($diagnos_first_all_specified==''?'':'; ').$dd["TEXT"];
                    if($d==0)
                    {
                        $this->DIAGNOS_FIRST_SPECIFIED=$dd["TEXT"];
                    }
                }
            }
             if($key==0)
             {
                $this->DIAGNOS_25_FIRST_ALL=$toothexam->DIAG_F25; 
                $this->DATE_FIRST_EXAM=$toothexam->EXAMDATE;
                $this->COMPLAINTS_FIRST_EXAM=$toothexam->COMPLAINTS;
                $this->DISEASEHISTORY_FIRST_EXAM=$toothexam->DISEASEHISTORY;                 
             }            
            $toothexam->PROC_PLAN=$diagnos_treatment[3];
            
        }
        
        uksort($diary,"datecmp");
		$this->DAIRY=PrintDataModule_DIARY($patient_id, $trans);
        file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt").var_export($this->DAIRY, true));
        $this->DIARY=$diary;
        $this->DISEASEHISTORY_ALL_EXAM=$diseasehistory_all_exam;
        $this->COMPLAINTS_ALL_EXAM=$complaints_all_exam;
        $this->DIAGNOS_ALL_SPECIFIED=$diagnos_all_specified;
        $this->DIAGNOS_25_ALL=$diagnos_25_all_specified;
        $this->DIAGNOS_FIRST_ALL_SPECIFIED=$diagnos_first_all_specified;
		
		
        $date_current = time();
        $this->DATE_CURRENT=array(
        'FULL_DATE'=>ua_date('"d" M Y', $date_current ),
        'DAY'=>ua_date("d", $date_current ),
        'MONTH'=>ua_date("M", $date_current ),
        'YEAR'=>ua_date("y", $date_current ));
		
		$this->DATE_CURRENT['MONTH'] = dateDeclineOnCases($this->DATE_CURRENT['MONTH']);
		
       
        ibase_commit($trans);
        $connection->CloseDBConnection();   
            
    }

    public function generateFieldArray($field, $len, $lenstart)
    {
        $field_array=$field.'_ARRAY';
        $this->$field_array=space_split($this->$field, $len, $lenstart);
    }
    
    public function generateCheckupArrays($len, $lenstart)
    {
        foreach($this->TOOTHEXAM_ARRAY as $toothexam)
        {
            $toothexam->CHECKUP_ARRAY=space_split($toothexam->CHECKUP, $len, $lenstart);
        }
        
    }
    
    /**
     * getDoctorDiary()
     * 
     * @param int $treatment_course_id
     * @param string $patient_id
     * @param string $start_date
     * @param string $end_date
     * @param boolean $includePriceInReport
     * @param boolean $includeMaterialsInReport
     * @param boolean $includeDoctorSignatureInReport=false, 
     * @param boolean $includeNoncompleteProcedures025Flag=false
     * @param boolean $includeShifrProcedures025Flag
     * @param boolean $includeCountProcedures025Flag
     * @param resource $trans
     * @return array
     */            
    public static function getDoctorDiary($treatment_course_id=null, $patient_id=null, $start_date=null, $end_date=null, $includePriceInReport=false, $includeMaterialsInReport=false, $includeDoctorSignatureInReport=false, $includeNoncompleteProcedures025Flag=false, $includeShifrProcedures025Flag=false, $includeCountProcedures025Flag=false, $trans=null)
    {
            $diary=array();
            //$additions=ToothExam::getAdditions($treatment_course_id, $patient_id, $start_date, $end_date, $includePriceInReport, $includeDoctorSignatureInReport, $trans);

            //foreach($additions[0] as $dd)
            //{
              //  $diary[$dd["DATE"]]->ANAMNESIS[]=$dd["TEXT"];
            //}                
            //foreach($additions[1] as $dd)
            //{
              //  $diary[$dd["DATE"]]->STATUS[]=$dd["TEXT"];
            //}
            //foreach($additions[2] as $dd)
            //{
              //  $diary[$dd["DATE"]]->RECOMENDATIONS[]=$dd["TEXT"];
            //}                
            //foreach($additions[3] as $dd)
            //{
              //  $diary[$dd["DATE"]]->EPICRISIS[]=$dd["TEXT"];
            //}
                
            $diagnos_treatment=getDiagnosTreatment($treatment_course_id, $patient_id, $start_date, $end_date, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag, $trans);
            
            foreach($diagnos_treatment[1] as $dd)
            {
                $diary[$dd["DATE"]]->DIAG_DIARY[]=$dd["TEXT"];
            }

            foreach($diagnos_treatment[2] as $dd)
            {
                $diary[$dd["DATE"]]->PROC_DIARY[]=$dd["TEXT"];
            }
            //$plans=ToothExam::getPlan($treatment_course_id, $patient_id, $start_date, $end_date, $trans);
            //foreach($plans as $dd)
            //{
              //  $diary[$dd["DATE"]]->PLAN[]=$dd["TEXT"];
            //}        
            uksort($diary,"datecmp");
            return $diary;
    }
        

    public static function getToothexamArray($patient_id, $sql_choice, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
        	$QueryText = "
            select	EXAMID from TOOTHEXAM	
            			where ($sql_choice) and (TOOTHEXAM.ID = $patient_id) order by TOOTHEXAM.EXAMDATE";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
			$toothexam_array=array();
			while ($row = ibase_fetch_row($result))
			{	
                $toothexam_array[]=new ToothExam($row[0], $trans);                	      
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }     
            
            return $toothexam_array; 
    }         
}

function getDiagnosTreatment($treatment_course_id=null, $patient_id=null, $start_date=null, $end_date=null, $includePriceInReport=false, $includeMaterialsInReport=false, $includeDoctorSignatureInReport=false, $includeNoncompleteProcedures025Flag=false, $includeShifrProcedures025Flag=false, $includeCountProcedures025Flag=false, $trans=null)
	{		
            if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            } 

//			$QueryText = "
//    			select
//    				DIAGNOS2.ID,
//    				DOCTORS.SHORTNAME,
//    				DIAGNOS2.DIAGNOSDATE,
//    				DIAGNOS2.STACKNUMBER,	
//    				PROTOCOLS.NAME						
//    			from DIAGNOS2
//    				left join PROTOCOLS on (DIAGNOS2.PROTOCOL_FK = PROTOCOLS.ID)
//    				left join DOCTORS on (DOCTORS.ID = DIAGNOS2.DOCTOR)
//                    left join TREATMENTCOURSE on (TREATMENTCOURSE.ID = DIAGNOS2.TREATMENTCOURSE)"; 
            $QueryText="
                			select
                				DIAGNOS2.ID,
                				DOCTORS.SHORTNAME,
                				(select first 1 examdate from toothexam where TREATMENTCOURSE_FK=DIAGNOS2.TREATMENTCOURSE),
                				DIAGNOS2.STACKNUMBER,	
								PROTOCOLS.NAME						
                			from DIAGNOS2
                				left join PROTOCOLS on (DIAGNOS2.PROTOCOL_FK = PROTOCOLS.ID)
                				left join DOCTORS on (DOCTORS.ID = DIAGNOS2.DOCTOR)
								left join TREATMENTCOURSE on (TREATMENTCOURSE.ID = DIAGNOS2.TREATMENTCOURSE)"; 
                $where="";
                if(nonull($treatment_course_id))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  else
                  {
                    $where.=" where";
                  }
                  $where.=" TREATMENTCOURSE.ID = $treatment_course_id";  
                }
                if(nonull($patient_id))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  else
                  {
                    $where.=" where";
                  }
                  $where.=" TREATMENTCOURSE.PATIENT_FK = $patient_id";  
                }
    			$QueryText.=$where;
                $QueryText.=" order by STACKNUMBER";	
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
            $diag_array=array();
            $diag_diary_array=array();
            $proc_diary_array=array();
            $proc_plan_array=array();
					
			while ($row = ibase_fetch_row($result))
			{       
                    if($row[3]!=-1)
                    {
                        if(((nonull($start_date)==false)||strtotime($row[2])>=strtotime($start_date))&&((nonull($end_date)==false)||strtotime($row[2])<=strtotime($end_date)))
                        {
                            $text=$row[4];
                            $diag_array[]=array("DATE"=>($row[2]==null?"":date("d.m.Y", strtotime($row[2]))), "TEXT"=>$text);
                            $diag_diary_array[]=array("DATE"=>($row[2]==null?"":date("d.m.Y", strtotime($row[2]))), "TEXT"=>$text);   
                        }

                    }
						
					//лікування
					$procedures = getTreatmentProcedures($row[0], $start_date, $end_date, $includeMaterialsInReport, $includeShifrProcedures025Flag, $includeCountProcedures025Flag, $trans);
					
                    $proc_diary=array();
                    $proc_plan=array();
                    foreach($procedures as $proc)
                    {                                              
                        if($proc->NAMEFORPLAN!="")
                         {
                           $proc_plan[]=$proc->NAMEFORPLAN;
                         }
                          if($proc->NAME!="")
                          {
                              $text=$proc->NAME.($includePriceInReport?"($proc->PRICE)":"").
                                    ($includeMaterialsInReport?(($proc->FARMS==""?"":"\r\n").$proc->FARMS):"");
                              if($includeNoncompleteProcedures025Flag||$proc->DOCTORSHORTNAME!='')
                              {
								 //лікування
                                $proc_diary[]=array("DATE"=>($proc->DATECLOSE), "TEXT"=>$text);
                              }
                          }
                    }
                    
                    if(count($proc_diary)>0)
                    {
						//Лікування
                        $proc_diary_array=$proc_diary;
                    }
                    
                    if(count($proc_plan)>0)
                    {
                        $proc_plan_array=array_merge($proc_plan_array,$proc_plan);
                    }
			}
		
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			
			//$diag_array;  - ?????
			//$proc_plan_array - ????		
						
			//$proc_diary_array; - лікування
			//$diag_diary_array; - діагноз

			
			return array($diag_array, $diag_diary_array, $proc_diary_array, $proc_plan_array);
	}
    
    
         /**
     * ToothExam::getTreatmentProcedures()
     * возвращает массив процедур для данного диагноза
     * 
     * @param int $diagnos_id
     * @param boolean $includeMaterialsInReport
     * @param boolean $includeShifrProcedures025Flag
     * @param boolean $includeCountProcedures025Flag
     * @param resource $trans
     * @return array
     */
     
   	function getTreatmentProcedures($diagnos_id,  $start_date=null, $end_date=null, $includeMaterialsInReport=false, $includeShifrProcedures025Flag=false, $includeCountProcedures025Flag=false, $trans=null)
	{		
            if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            } 
        
			$QueryTreatmentProcText = "
			select
				TREATMENTPROC.ID, 		
				TREATMENTPROC.NAME, 
				TREATMENTPROC.NAMEFORPLAN,					
				TREATMENTPROC.PRICE, 			
				DOCTORS.SHORTNAME,
				TREATMENTPROC.DATECLOSE,
                TREATMENTPROC.PROC_COUNT,
                TREATMENTPROC.SHIFR
                		
			from TREATMENTPROC 				
				left join DOCTORS	on (TREATMENTPROC.DOCTOR = DOCTORS.ID)					
			where TREATMENTPROC.DIAGNOS2 = $diagnos_id";		
            if(nonull($start_date))
            {
              $QueryTreatmentProcText.=" AND (TREATMENTPROC.DATECLOSE >= '$start_date')";  
            }
            if(nonull($end_date))
            {
              $QueryTreatmentProcText.=" AND (TREATMENTPROC.DATECLOSE <= '$end_date')";  
            }
                $QueryTreatmentProcText.=" order by TREATMENTPROC.STACKNUMBER, TREATMENTPROC.DATECLOSE";	
			$TreatmentProcQuery = ibase_prepare($trans, $QueryTreatmentProcText);
			$TreatmentProcResult = ibase_execute($TreatmentProcQuery);

			$returnObj = array();

			while ($procRow = ibase_fetch_row($TreatmentProcResult))
			{
				$obj = new stdclass();
				
				$obj->ID = $procRow[0];				
				$obj->NAME = $procRow[1]==null?'':($includeCountProcedures025Flag?($procRow[6].'x '):'').($includeShifrProcedures025Flag?$procRow[7].' ':'').$procRow[1];
				$obj->NAMEFORPLAN = $procRow[2]==null?'':($includeCountProcedures025Flag?($procRow[6].'x '):'').($includeShifrProcedures025Flag?$procRow[7].' ':'').$procRow[2];			
				$obj->PRICE = $procRow[3]*$procRow[6];
				$obj->DOCTORSHORTNAME = $procRow[4];		
				$obj->DATECLOSE = $procRow[5]!=null?date("d.m.Y", strtotime($procRow[5])):"";
            
                $farms="";
                if($includeMaterialsInReport)
                {
    				$FarmQueryText = "
    				select					
    					FARM.NAME, 					
    					TREATMENTPROCFARM.QUANTITY, 
    					UNITOFMEASURE.NAME						
    				from TREATMENTPROCFARM 					
    					left join FARM on (TREATMENTPROCFARM.FARM = FARM.ID)					
    					left join UNITOFMEASURE	on (FARM.ID = UNITOFMEASURE.FARMID)							
    				where TREATMENTPROCFARM.TREATMENTPROC = $obj->ID and UNITOFMEASURE.isbase = 1";
    
    				$FarmQuery = ibase_prepare($trans, $FarmQueryText);
    				$FarmResult = ibase_execute($FarmQuery);
    						
                    while ($farmRow = ibase_fetch_row($FarmResult))
    				{             
                        $farms.=($farms==''?'':', ').
                                $farmRow[0].' ('.$farmRow[1]*$procRow[6].' '.$farmRow[2].')';	
                    }	
    				
    				ibase_free_query($FarmQuery);
    				ibase_free_result($FarmResult);	
    					
                }			

				
                $obj->FARMS = $farms;
				$returnObj[] = $obj;
			}
			
			//лікування
			return $returnObj;
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
	}



class ToothExam
{   
    /**
    * @var string
    */
    var $EXAMID;
    
    /**
    * @var string
    */
    var $TREATMENT_COURSE_ID; 
    
    /**
    * @var string
    */
    var $EXAM;
    
    /**
    * @var string
    */
    var $CHANNELEXAM;
    
    /**
    * @var string
    */
    var $EXAMDATE;
    
    /**
    * @var string
    */
    var $COMPLAINTS;
    
    /**
    * @var string
    */
    var $CHECKUP;
    
    /**
    * @var string
    */
    var $DISEASEHISTORY;
    
    /**
    * @var array
    */
    var $CHECKUP_ARRAY;
    
    /**
    * @var string
    */
    var $BITETYPE;
    
    /**
    * @var string
    */
    var $DOCTOR;
    
    /**
    * @var string
    */
    var $DIAGNOSIS;
    
    /**
    * @var Tooth[]
    */
    var $TEETH;
       
    /**
    * @var array
    */
    var $TOOTH_TEXT;
       
    /**
    * @var string
    */
    var $EXAM_AFTER; 
    
    /**
    * @var array
    */
    //var $ANAMNESIS; 
    
    /**
    * @var array
    */
    ///var $STATUS; 
    
    /**
    * @var array
    */
    //var $RECOMENDATIONS; 
    
    /**
    * @var array
    */
    //var $EPICRISIS; 
    
    /**
    * @var array
    */
    var $DIAG; 
    
    /**
    * @var array
    */
    var $DIAG_F25; 
    
    /**
    * @var array
    */
    var $DIAG_DIARY; 
    
    /**
    * @var array
    */
    var $PROC_DIARY; 
    
    /**
    * @var array
    */
    var $PROC_PLAN; 
   
    
    function __construct($exam_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }          
        
        $QueryText = "
            select        tb.examid,
                            tb.exam,
                        tb.examdate,
                        tb.complaints,
                        tb.checkup,
                        tb.bitetype,
                        tb.treatmentcourse_fk,
                        d.shortname,
                        tb.diseasehistory,
                        (select first 1 ta.exam from toothexam ta where ta.treatmentcourse_fk=tb.treatmentcourse_fk and ta.afterflag=1  order  by ta.examdate),
                        tb.channelsexam
                        from toothexam tb
                        LEFT JOIN doctors d ON (d.ID = tb.DOCTORID)
                        where tb.examid = $exam_id";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
			$toothexam_array=array();
			while ($row = ibase_fetch_row($result))
			{	
		        $this->EXAMID = $row[0]; 
                $this->EXAM = $row[1];
                $this->EXAMDATE = $row[2]==null?"":date("d.m.Y", strtotime($row[2]));
                $this->COMPLAINTS = $row[3];
                $this->CHECKUP = $row[4];
                $this->BITETYPE = $row[5];
                $this->TREATMENT_COURSE_ID=$row[6];
                $this->DOCTOR=$row[7];
                $this->DISEASEHISTORY=$row[8];
                $this->TOOTH_TEXT=$this->BITETYPE==0?unserialize(toothtexts):unserialize(toothtextsmilk);          
                $diagnos_array=getSortTeethF43($this->EXAM, $this->BITETYPE, unserialize(toothid), unserialize(dentdisword));
                if(nonull($row[9])) 
                {
                    $this->EXAM_AFTER=$row[9];
                } 
                $this->CHANNELEXAM = text_blob_encode($row[10]);
                $teeth=array();
                
                $this->TEETH=$teeth;
                
                $diagnosis='';
                foreach($diagnos_array as $diagnos)
                {
                    $diagnosis.=($diagnosis==''?'':', ').$diagnos["ID"].' - '.$diagnos["DIAG"];
                }
                $this->DIAGNOSIS=$diagnosis;  
                         	      
			}
			ibase_free_query($query);
			ibase_free_result($result);                       

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
//             file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($this->PLAN, true));

    }
    

    

}
?>

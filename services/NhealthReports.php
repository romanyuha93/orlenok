<?php 

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";


require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');


class NhealthReports
{
  /** 
  * @param string $file_name
  */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);        
    }

	/**
	* @param string $docType
    * @param string $patientID
    * @return string
    */
	public function createExtract($docType,$patientID)
	{
        $connection = new Connection();
	    $connection->EstablishDBConnection();
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'extract.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection); 
        
        
		$PATIENT_DATA= new stdclass();
		$QueryText = "
            select 
				FIRSTNAME,
				MIDDLENAME,              
				LASTNAME,
				BIRTHDAY,
				CITY,
				ADDRESS,
				EMPLOYMENTPLACE,
				JOB
            from PATIENTS 
			where ID = $patientID";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
		if ($row = ibase_fetch_row($result))
			{				
				$PATIENT_DATA->FIRSTNAME = $row[0];
				$PATIENT_DATA->MIDDLENAME = $row[1];
				$PATIENT_DATA->LASTNAME = $row[2];
				$PATIENT_DATA->BIRTHDAY = $row[3]!=null?date("d.m.Y", strtotime($row[3])):"";
                $PATIENT_DATA->CITY = $row[4];
				$PATIENT_DATA->ADDRESS = $row[5];
				$PATIENT_DATA->EMPLOYMENTPLACE = $row[6];
				$PATIENT_DATA->JOB = $row[7];
			}
		ibase_free_query($query);
        ibase_free_result($result);
				
        $TBS = new clsTinyButStrong;

        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		
        if(file_exists(dirname(__FILE__).'/tbs/res/extract.'.$docType))
        {
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/extract.'.$docType, OPENTBS_ALREADY_UTF8);  
        }
        $TBS->NoErr=true;
		
		$TBS->MergeField("PATIENT_DATA", $PATIENT_DATA);
				
		$file_name = translit(str_replace(' ','_',$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
        ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $form_path;
	}
	
	
	/**
	* @param string $docType
    * @param string $patientID
    * @return string
    */
	public function create014($docType,$patientID)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form014_0_man.'.$docType) or !file_exists($res.'form014_0_woman.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$PATIENT_DATA=new stdclass;
		$CLINIC_DATA=new stdclass;
		$QueryText = "
            select 
				patients.FIRSTNAME,
				patients.MIDDLENAME,              
				patients.LASTNAME,
				patients.SEX,
				datediff(year from patients.BIRTHDAY to current_date),
				CLINICREGISTRATIONDATA.LEGALNAME  
            from PATIENTS, CLINICREGISTRATIONDATA
			where patients.ID = $patientID";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		if ($row = ibase_fetch_row($result))
			{				
				$PATIENT_DATA->FIRSTNAME = $row[0];
				$PATIENT_DATA->MIDDLENAME = $row[1];
				$PATIENT_DATA->LASTNAME = $row[2];

				$PATIENT_DATA->AGE = $row[4];
				if ($row[3]=='0')
				{
					$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form014_0_man.'.$docType, OPENTBS_ALREADY_UTF8);
				}
				else if ($row[3]=='1')
				{
					$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form014_0_woman.'.$docType, OPENTBS_ALREADY_XML);
				}
				$CLINIC_DATA->LEGALNAME = $row[5];
			}
		ibase_free_query($query);
        ibase_free_result($result);

		$DATE_CURRENT= new stdclass;
		
		$date_current1 = time();
        
		$DATE_CURRENT->DAY=ua_date("d", $date_current1);
		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
        $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
		$DATE_CURRENT->HOURS=ua_date("H",$date_current1);		
		
		$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
		
		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
		
		$TBS->NoErr=true;
		
		$TBS->MergeField("PATIENT_DATA", $PATIENT_DATA);
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
				
		$file_name = translit(str_replace(' ','_',$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
	
	/**
	* @param string $docType
    * @param string $patientID
    * @return string
    */
	public function create003($docType,$patientID)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form003_0.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$PATIENT_DATA= new stdclass(); 
		$CLINIC_DATA=new stdclass();
		$QueryText = "
            select 
				patients.FIRSTNAME,
				patients.MIDDLENAME,              
				patients.LASTNAME,
				patients.SEX,
				datediff(year from patients.BIRTHDAY to current_date),
				CLINICREGISTRATIONDATA.LEGALNAME,
				patients.BIRTHDAY,
				patients.MOBILENUMBER,
				patients.EMPLOYMENTPLACE,
				patients.JOB,
				patients.VILLAGEORCITY,
				patients.CITY,
				patients.ADDRESS
            from PATIENTS, CLINICREGISTRATIONDATA
			where patients.ID = $patientID";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		

		if ($row = ibase_fetch_row($result))
			{				
				$PATIENT_DATA->FIRSTNAME = $row[0];
				$PATIENT_DATA->MIDDLENAME = $row[1];
				$PATIENT_DATA->LASTNAME = $row[2];
				if ($row[3]==0)
				{
					$PATIENT_DATA->SEX='1';
				}
				else if ($row[3]==1)
				{
					$PATIENT_DATA->SEX='2';
				}
				$PATIENT_DATA->AGE=$row[4];
				$CLINIC_DATA->LEGALNAME = $row[5];
				$PATIENT_DATA->BIRTHDAY=$row[6];
				$PATIENT_DATA->MOBILENUMBER=$row[7];
				$PATIENT_DATA->EMPLOYMENTPLACE=$row[8];
				$PATIENT_DATA->JOB=$row[9];
				if ($row[10]==0)
				{
					$PATIENT_DATA->VORC=2;
				}
				else if ($row[10]==1)
				{
					$PATIENT_DATA->VORC=1;
				}
				$PATIENT_DATA->CITY=$row[11];
				$PATIENT_DATA->ADDRESS=$row[12];
			}
		ibase_free_query($query);
        ibase_free_result($result);

		//$date_current1 = time();
       // $DATE_CURRENT=array
	//	(
        //'DAY'=>ua_date("d", $date_current1),
        //'MONTH'=>ua_date("M", $date_current1),
        //'YEAR'=>ua_date("y", $date_current1),
		//'HOURS'=>ua_date("H",$date_current1)
		//);

	//	$DATE_CURRENT[MONTH] = str_replace
	//							   (array("ень",'ий','пад'),
	//								array("ня",'ого','пада'),
	//								$DATE_CURRENT[MONTH]);

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form003_0.'.$docType, OPENTBS_ALREADY_UTF8);

		$TBS->NoErr=true;
		
		$TBS->MergeField("PATIENT_DATA", $PATIENT_DATA);
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
		//$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
				
		$file_name = translit(str_replace(' ','_',$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
	
	/**
	* @param string $docType
    * @param string $patientID
    * @return string
    */
	public function create066($docType,$patientID)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form066_0.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$PATIENT_DATA=new stdclass();
		$CLINIC_DATA=new stdclass();
		$QueryText = "
            select 
				patients.FIRSTNAME,
				patients.MIDDLENAME,              
				patients.LASTNAME,
				patients.SEX,
				CLINICREGISTRATIONDATA.LEGALNAME,
				patients.BIRTHDAY,
				patients.CITY,
				patients.ADDRESS,
				patients.VILLAGEORCITY
            from PATIENTS, CLINICREGISTRATIONDATA
			where patients.ID = $patientID";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		
		if ($row = ibase_fetch_row($result))
			{
				$PATIENT_DATA->FIRSTNAME = $row[0];
				$PATIENT_DATA->MIDDLENAME = $row[1];
				$PATIENT_DATA->LASTNAME = $row[2];
				if ($row[3]==0)
				{
					$PATIENT_DATA->SEX='1';
				}
				else if ($row[3]==1)
				{
					$PATIENT_DATA->SEX='2';
				}
				$CLINIC_DATA->LEGALNAME = $row[4];
				$PATIENT_DATA->BIRTHDAY_ARRAY = str_split($row[5]!=null?date("dmy", strtotime($row[5])):"");
				$PATIENT_DATA->CITY=$row[6];
				$PATIENT_DATA->ADDRESS=$row[7];
				if ($row[8]==0)
				{
					$PATIENT_DATA->VORC=2;
				}
				else if ($row[8]==1)
				{
					$PATIENT_DATA->VORC=1;
				}
			}
		ibase_free_query($query);
        ibase_free_result($result);

		// $date_current1 = time();
        // $DATE_CURRENT=array
		// (
        // 'DAY'=>ua_date("d", $date_current1),
        // 'MONTH'=>ua_date("M", $date_current1),
        // 'YEAR'=>ua_date("y", $date_current1),
		// 'HOURS'=>ua_date("H",$date_current1)
		// );

	//	$DATE_CURRENT[MONTH] = str_replace
	//							   (array("ень",'ий','пад'),
	//								array("ня",'ого','пада'),
	//								$DATE_CURRENT[MONTH]);

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form066_0.'.$docType, OPENTBS_ALREADY_UTF8);

		$TBS->NoErr=true;
		
		$TBS->MergeField("PATIENT_DATA", $PATIENT_DATA);
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
		//$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
				
		$file_name = translit(str_replace(' ','_',$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
	
	
	/**
	* @param string $docType
    * @param string $patientID
    * @return string
    */
	public function create007($docType,$patientID)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form007_0.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$PATIENT_DATA=new stdclass();
		$CLINIC_DATA=new stdclass();
		$QueryText = "
            select 
				patients.FIRSTNAME, patients.MIDDLENAME, patients.LASTNAME,
				CLINICREGISTRATIONDATA.LEGALNAME, CLINICREGISTRATIONDATA.ADDRESS 
            from patients, CLINICREGISTRATIONDATA
			where patients.ID=$patientID";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		
		if ($row = ibase_fetch_row($result))
			{
				$CLINIC_DATA->LEGALNAME = $row[3];
				$CLINIC_DATA->ADDRESS = $row[4];
				$PATIENT_DATA->LASTNAME = $row[2];
				$PATIENT_DATA->MIDDLENAME = $row[1];
				$PATIENT_DATA->FIRSTNAME = $row[0];
			}
		ibase_free_query($query);
        ibase_free_result($result);

		$DATE_CURRENT= new stdclass;
		
		$date_current1 = time();
        
		$DATE_CURRENT->DAY=ua_date("d", $date_current1);
		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
        $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
		
		$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
		

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form007_0.'.$docType, OPENTBS_ALREADY_UTF8);

		$TBS->NoErr=true;
		
		//$TBS->MergeField("PATIENT_DATA", $PATIENT_DATA);
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
		$TBS->MergeField("CURRENT_DATE", $DATE_CURRENT);
				
		$file_name = translit(str_replace(' ','_',$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
    
    
    /**
	* @param string $docType
    * @param string $month
    * @param string $year
    * @return string
    */
	public function create039($docType, $month, $year)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form039.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		
        $CLINIC_DATA = new PrintDataModule_ClinicRegistrationData(true, $trans);
        
       	$sql_report = "select

                        distinct(tasks.taskdate),
                        
                          
                        extract(DAY from tasks.taskdate) as D,
                        
                        extract(MONTH from tasks.taskdate) as M,
                        
                                                                                      
                        (select count(t2.id) from tasks as t2 where t2.taskdate = tasks.taskdate) as allcount,
                        
                        (select count(t2.id) from tasks as t2 where t2.taskdate = tasks.taskdate
                                                                    and patients.villageorcity = 0) as villiage,
                        
                        (select count(t2.id) from tasks as t2 where t2.taskdate = tasks.taskdate
                                                                    and DATEDIFF(YEAR, patients.birthday, current_date) > 18) as adult,
                        
                        (select count(t2.id) from tasks as t2 where t2.taskdate = tasks.taskdate
                                                                    and DATEDIFF(YEAR, patients.birthday, current_date) > 18
                                                                    and patients.villageorcity = 0) as adult_villiage
                        
                        
                        
                        from tasks
                            inner join patients
                            on patients.id = tasks.patientid
                        
                        where
                            extract(MONTH from tasks.taskdate) = ".$month."
                                and
                            extract(YEAR from tasks.taskdate) = ".$year."
                            
                        order by tasks.taskdate asc";
			
		$query_report = ibase_prepare($trans, $sql_report);
		$result_report = ibase_execute($query_report);
		
        $REPORT1 = array();
        $REPORT2 = array();
        $index = 0;
		while ($row = ibase_fetch_row($result_report))
		{	
		    $index++;
            $report_item = new stdClass();
            $report_item->DAY = $row[1];
            $report_item->MONTH = $row[2];
            $report_item->ALL = $row[3];
            $report_item->VIL = $row[4];
            $report_item->ADULT = $row[5];
            $report_item->ADULT_VIL = $row[6];
            if($index < 6)
            {
                $REPORT1[] = $report_item;
            }
            else
            {
                $REPORT2[] = $report_item;
            }
        }
		ibase_free_query($query_report);
        ibase_free_result($result_report);
        
        
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form039.'.$docType, OPENTBS_ALREADY_XML);
        
		$DATE_CURRENT= new stdclass;
		
		$date_current1 = time();
        
		$DATE_CURRENT->DAY=ua_date("d", $date_current1);
		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
        $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
		$DATE_CURRENT->HOURS=ua_date("H",$date_current1);		
		
		$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
		
		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
		
		$TBS->NoErr=true;
		
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
        $TBS->MergeBlock("REPORT1", $REPORT1);
        $TBS->MergeBlock("REPORT2", $REPORT2);
				
        $file_name = 'form039';
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
    
    /**
	* @param string $docType
    * @param string $year
    * @return string
    */
	public function create039Month($docType, $year)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form039month.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		
        $CLINIC_DATA = new PrintDataModule_ClinicRegistrationData(true, $trans);
        
       	$sql_report = " select

                            distinct(extract(MONTH from tasks.taskdate)) as M,
                            extract(YEAR from tasks.taskdate) as Y,
                            
                                                                                          
                            (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)) as allcount,
                            
                            (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)
                                                                        and
                                                                        (select patients.villageorcity from patients where patients.id = t2.patientid) = 0) as villiage,
                            
                            (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)
                                                                        and DATEDIFF(YEAR, (select patients.birthday from patients where patients.id = t2.patientid), current_date) > 18) as adult,
                            
                            (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)
                                                                        and DATEDIFF(YEAR, (select patients.birthday from patients where patients.id = t2.patientid), current_date) > 18
                                                                        and
                                                                        (select patients.villageorcity from patients where patients.id = t2.patientid) = 0) as adult_villiage
                            
                            
                            
                            from tasks
                            
                            where
                                extract(YEAR from tasks.taskdate) = ".$year."
                            
                            
                            order by extract(MONTH from tasks.taskdate) asc";
			
		$query_report = ibase_prepare($trans, $sql_report);
		$result_report = ibase_execute($query_report);
		
        $REPORT1 = array();
        $REPORT2 = array();
        $index = 0;
		while ($row = ibase_fetch_row($result_report))
		{	
		    $index++;
            $report_item = new stdClass();
            $report_item->MONTH = $row[0];
            $report_item->YEAR = $row[1];
            $report_item->ALL = $row[2];
            $report_item->VIL = $row[3];
            $report_item->ADULT = $row[4];
            $report_item->ADULT_VIL = $row[5];
            if($index < 6)
            {
                $REPORT1[] = $report_item;
            }
            else
            {
                $REPORT2[] = $report_item;
            }
        }
		ibase_free_query($query_report);
        ibase_free_result($result_report);
        
        
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form039month.'.$docType, OPENTBS_ALREADY_XML);
        
		$DATE_CURRENT= new stdclass;
		
		$date_current1 = time();
        
		$DATE_CURRENT->DAY=ua_date("d", $date_current1);
		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
        $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
		$DATE_CURRENT->HOURS=ua_date("H",$date_current1);		
		
		$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
		
		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
		
		$REPORT_DATE= new stdclass;
        $REPORT_DATE->YEAR = $year;
		$TBS->MergeField("REPORT_DATE", $REPORT_DATE);
        
		$TBS->NoErr=true;
		
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
        $TBS->MergeBlock("REPORT1", $REPORT1);
        $TBS->MergeBlock("REPORT2", $REPORT2);
				
        $file_name = 'form039month';
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
    
    
    /*
    select

        distinct(extract(MONTH from tasks.taskdate)) as M,
        extract(YEAR from tasks.taskdate) as Y,
        
                                                                      
        (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)) as allcount,
        
        (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)
                                                    and
                                                    (select patients.villageorcity from patients where patients.id = t2.patientid) = 0) as villiage,
        
        (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)
                                                    and DATEDIFF(YEAR, (select patients.birthday from patients where patients.id = t2.patientid), current_date) > 18) as adult,
        
        (select count(t2.id) from tasks as t2 where extract(MONTH from t2.taskdate) = extract(MONTH from tasks.taskdate)
                                                    and DATEDIFF(YEAR, (select patients.birthday from patients where patients.id = t2.patientid), current_date) > 18
                                                    and
                                                    (select patients.villageorcity from patients where patients.id = t2.patientid) = 0) as adult_villiage
        
        
        
        from tasks
        
        
        
        
        order by extract(MONTH from tasks.taskdate) asc
    */
    
    
    /**
	* @param string $docType
    * @param string $month
    * @param string $year
    * @return string
    */
	public function create039_8($docType, $month, $year)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form039_8.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		
        $CLINIC_DATA = new PrintDataModule_ClinicRegistrationData(true, $trans);
        
        //---------- 1 ----------
		$sql_getdic = "select
                            nh_ultrasonography_dict.id,
                            nh_ultrasonography_dict.item_label,
                            nh_ultrasonography_dict.row_index
                        from nh_ultrasonography_dict
                        
                        order by nh_ultrasonography_dict.id";
			
		$query_getdic = ibase_prepare($trans, $sql_getdic);
		$result_getdic = ibase_execute($query_getdic);
		
        $templates = array();
        
        $template_item = new stdClass();
        $template_item->id = null;
        $template_item->item_label = 'other';
        $template_item->row_index = 23;
        $templates[] = $template_item;
        
		while ($row = ibase_fetch_row($result_getdic))
		{		
            $template_item = new stdClass();
            $template_item->id = $row[0];
            $template_item->item_label = $row[1];
            $template_item->row_index = $row[2];
            $templates[] = $template_item;
        }
		ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
		/*
        if(NHUltra::$isDebugLog)
		{
				//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($templates,true));
		}
        */
        //---------- 1 ----------
        
        //---------- 2 ----------
        $REPORT = array();
        
        for($i = 0; $i < count($templates); ++$i)
        {
            $_id = $templates[$i]->id;
            
            if($_id == null)
            {
                $_id = 0;
                $sql_id = " is null ";    
            }
            else
            {
                $sql_id = " = ".$_id; 
            }
            
            $sql_report = "select
                                distinct(DT),
                                D,
                                M,
                                Y,
                            
                                (select count(id) from NH_ULTRASONOGRAPHY where
                                    extract(DAY from NH_ULTRASONOGRAPHY.docdate) = D and
                                    extract(MONTH from NH_ULTRASONOGRAPHY.docdate) = M and
                                    extract(YEAR from NH_ULTRASONOGRAPHY.docdate) = Y and
                                    NH_ULTRASONOGRAPHY.fk_nh_ultrasonography_dict ".$sql_id.")
                                
                            
                            from
                                (select
                                    NH_ULTRASONOGRAPHY.docdate as DT,
                                    extract(DAY from NH_ULTRASONOGRAPHY.docdate) as D,
                                    extract(MONTH from NH_ULTRASONOGRAPHY.docdate) as M,
                                    extract(YEAR from NH_ULTRASONOGRAPHY.docdate) as Y
                                from NH_ULTRASONOGRAPHY
                        
                                where NH_ULTRASONOGRAPHY.fk_nh_ultrasonography_dict ".$sql_id."
                    
                                order by NH_ULTRASONOGRAPHY.docdate, NH_ULTRASONOGRAPHY.fk_nh_ultrasonography_dict)
                            where Y = ".$year." and M = ".$month;
			
    		$query_report = ibase_prepare($trans, $sql_report);
    		$result_report = ibase_execute($query_report);
    		
            
    		while ($row = ibase_fetch_row($result_report))
    		{	      
                $D = $row[1];
                
                //initialize day array element
                if(!array_key_exists($D, $REPORT))
                {
                    $report_item = new stdClass();
                    $report_item->C1 = $D; // day
                    $report_item->C2 = 0; // all
                    $report_item->C3 = 0;
                    $report_item->C4 = 0;
                    $report_item->C5 = 0;
                    $report_item->C6 = 0;
                    $report_item->C7 = 0;
                    $report_item->C8 = 0;
                    $report_item->C9 = 0;
                    $report_item->C10 = 0;
                    $report_item->C11 = 0;
                    $report_item->C12 = 0;
                    $report_item->C13 = 0;
                    $report_item->C14 = 0;
                    $report_item->C15 = 0;
                    $report_item->C16 = 0;
                    $report_item->C17 = 0;
                    $report_item->C18 = 0;
                    $report_item->C19 = 0;
                    $report_item->C20 = 0;
                    $report_item->C21 = 0;
                    $report_item->C22 = 0;
                    $report_item->C23 = 0;//other
                    $REPORT[$D] = $report_item;
                }        
                
                //set template row by row_index
                $_report_item = $REPORT[$D];
                $_row = "C".$templates[$i]->row_index;
                $_report_item->$_row = $row[4];
				
                //calculate all row count
                $_report_item->C2 =
                    $_report_item->C3 + 
                    $_report_item->C4 + 
                    $_report_item->C5 + 
                    $_report_item->C6 + 
                    $_report_item->C7 + 
                    $_report_item->C8 + 
                    $_report_item->C9 + 
                    $_report_item->C10 + 
                    $_report_item->C11 + 
                    $_report_item->C12 + 
                    $_report_item->C13 + 
                    $_report_item->C14 + 
                    $_report_item->C15 + 
                    $_report_item->C16 + 
                    $_report_item->C17 + 
                    $_report_item->C18 + 
                    $_report_item->C19 + 
                    $_report_item->C20 + 
                    $_report_item->C21 + 
                    $_report_item->C22 + 
                    $_report_item->C23;
                
                
            }
    		ibase_free_query($query_report);
            ibase_free_result($result_report);
        }
        ksort($REPORT);
        
		if(false)
       	{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($REPORT,true));
		}
        //---------- 2 ----------


        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form039_8.'.$docType, OPENTBS_ALREADY_UTF8);

		$DATE_CURRENT= new stdclass;
		
		$date_current1 = time();
        
		$DATE_CURRENT->DAY=ua_date("d", $date_current1);
		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
        $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
		$DATE_CURRENT->HOURS=ua_date("H",$date_current1);		
		
		$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
		
		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
        
		$REPORT_DATE= new stdclass;
        $REPORT_DATE->YEAR = $year;
        $REPORT_DATE->MONTH = $month;
        
		$TBS->MergeField("REPORT_DATE", $REPORT_DATE);
        
		
		$TBS->NoErr=true;
		
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
        $TBS->MergeBlock("REPORT", $REPORT);
				
        $file_name = 'form039_8';
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
    
     /**
	* @param string $docType
    * @param string $year
    * @return string
    */
	public function create039_8Month($docType, $year)
	{
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'form039_8month.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		$connection = new Connection();
  		$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		
        $CLINIC_DATA = new PrintDataModule_ClinicRegistrationData(true, $trans);
        
        //---------- 1 ----------
		$sql_getdic = "select
                            nh_ultrasonography_dict.id,
                            nh_ultrasonography_dict.item_label,
                            nh_ultrasonography_dict.row_index
                        from nh_ultrasonography_dict
                        
                        order by nh_ultrasonography_dict.id";
			
		$query_getdic = ibase_prepare($trans, $sql_getdic);
		$result_getdic = ibase_execute($query_getdic);
		
        $templates = array();
        
        $template_item = new stdClass();
        $template_item->id = null;
        $template_item->item_label = 'other';
        $template_item->row_index = 23;
        $templates[] = $template_item;
        
		while ($row = ibase_fetch_row($result_getdic))
		{		
            $template_item = new stdClass();
            $template_item->id = $row[0];
            $template_item->item_label = $row[1];
            $template_item->row_index = $row[2];
            $templates[] = $template_item;
        }
		ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
		/*
        if(NHUltra::$isDebugLog)
		{
				//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($templates,true));
		}
        */
        //---------- 1 ----------
        
        //---------- 2 ----------
        $REPORT = array();
        
        for($i = 0; $i < count($templates); ++$i)
        {
            $_id = $templates[$i]->id;
            
            if($_id == null)
            {
                $_id = 0;
                $sql_id = " is null ";    
            }
            else
            {
                $sql_id = " = ".$_id; 
            }
            
            $sql_report = "select
                                distinct(M),
                                Y,
                            
                                (select count(id) from NH_ULTRASONOGRAPHY where
                                    extract(MONTH from NH_ULTRASONOGRAPHY.docdate) = M and
                                    extract(YEAR from NH_ULTRASONOGRAPHY.docdate) = Y and
                                    NH_ULTRASONOGRAPHY.fk_nh_ultrasonography_dict ".$sql_id.")
                                
                            
                            from
                                (select
                                    NH_ULTRASONOGRAPHY.docdate as DT,
                                    extract(MONTH from NH_ULTRASONOGRAPHY.docdate) as M,
                                    extract(YEAR from NH_ULTRASONOGRAPHY.docdate) as Y
                                from NH_ULTRASONOGRAPHY
                        
                                where NH_ULTRASONOGRAPHY.fk_nh_ultrasonography_dict ".$sql_id."
                    
                                order by NH_ULTRASONOGRAPHY.docdate, NH_ULTRASONOGRAPHY.fk_nh_ultrasonography_dict)
                            where Y = ".$year;
			
    		$query_report = ibase_prepare($trans, $sql_report);
    		$result_report = ibase_execute($query_report);
    		
            
    		while ($row = ibase_fetch_row($result_report))
    		{	      
                $D = $row[0];
                
                //initialize day array element
                if(!array_key_exists($D, $REPORT))
                {
                    $report_item = new stdClass();
                    $report_item->C1 = $D; // day
                    $report_item->C2 = 0; // all
                    $report_item->C3 = 0;
                    $report_item->C4 = 0;
                    $report_item->C5 = 0;
                    $report_item->C6 = 0;
                    $report_item->C7 = 0;
                    $report_item->C8 = 0;
                    $report_item->C9 = 0;
                    $report_item->C10 = 0;
                    $report_item->C11 = 0;
                    $report_item->C12 = 0;
                    $report_item->C13 = 0;
                    $report_item->C14 = 0;
                    $report_item->C15 = 0;
                    $report_item->C16 = 0;
                    $report_item->C17 = 0;
                    $report_item->C18 = 0;
                    $report_item->C19 = 0;
                    $report_item->C20 = 0;
                    $report_item->C21 = 0;
                    $report_item->C22 = 0;
                    $report_item->C23 = 0;//other
                    $REPORT[$D] = $report_item;
                }        
                
                //set template row by row_index
                $_report_item = $REPORT[$D];
                $_row = "C".$templates[$i]->row_index;
                $_report_item->$_row = $row[2];
				
                //calculate all row count
                $_report_item->C2 =
                    $_report_item->C3 + 
                    $_report_item->C4 + 
                    $_report_item->C5 + 
                    $_report_item->C6 + 
                    $_report_item->C7 + 
                    $_report_item->C8 + 
                    $_report_item->C9 + 
                    $_report_item->C10 + 
                    $_report_item->C11 + 
                    $_report_item->C12 + 
                    $_report_item->C13 + 
                    $_report_item->C14 + 
                    $_report_item->C15 + 
                    $_report_item->C16 + 
                    $_report_item->C17 + 
                    $_report_item->C18 + 
                    $_report_item->C19 + 
                    $_report_item->C20 + 
                    $_report_item->C21 + 
                    $_report_item->C22 + 
                    $_report_item->C23;
                
                
            }
    		ibase_free_query($query_report);
            ibase_free_result($result_report);
        }
        ksort($REPORT);
        
		if(false)
       	{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($REPORT,true));
		}
        //---------- 2 ----------


        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form039_8month.'.$docType, OPENTBS_ALREADY_UTF8);

		$DATE_CURRENT= new stdclass;
		
		$date_current1 = time();
        
		$DATE_CURRENT->DAY=ua_date("d", $date_current1);
		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
        $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
		$DATE_CURRENT->HOURS=ua_date("H",$date_current1);		
		
		$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
		
		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
        
		$REPORT_DATE= new stdclass;
        $REPORT_DATE->YEAR = $year;
        
		$TBS->MergeField("REPORT_DATE", $REPORT_DATE);
        
		
		$TBS->NoErr=true;
		
		$TBS->MergeField("CLINIC_DATA", $CLINIC_DATA);
        $TBS->MergeBlock("REPORT", $REPORT);
				
        $file_name = 'form039_8month';
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $form_path;
	}
}
?>
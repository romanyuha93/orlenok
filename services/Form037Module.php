<?php

require_once "F37OBJECT.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
require_once "Connection.php";
require_once "Utils.php";

//Form037Module::create037('odt',1,"2013-06-26",370);

class Form037Module
{

   /** 
 * @param string $docType
 * @param int $doctor_id
 * @param string $date
 *  @param int $form
 * @return string
 */  
 public static function create037($docType, $doctor_id, $date, $form)
 {
       $TBS = new clsTinyButStrong;
       switch($form)
        {
            case 370:
                $fpath='form037_0';
                break;
            case 371:
                $fpath='form037_1';
                break;
            case 372:
                $fpath='form037_2';
                break;
            default:
                $fpath='form037_0';
                break;
        }
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/'.$fpath.'.'.$docType, OPENTBS_ALREADY_UTF8);
        $TBS->NoErr=true;
        $FORM=new F37OBJECT($doctor_id, $date);
        $TBS->MergeBlock('TABLE_DATA',$FORM->TABLE_DATA);            
        $TBS->MergeField('FORM',$FORM);    
         $file_name = translit(str_replace(' ','_',$FORM->DOCTOR_DATA->LASTNAME.' '.$FORM->DOCTOR_DATA->FIRSTNAME.' '.$FORM->DOCTOR_DATA->MIDDLENAME.'_'.$fpath.'_'.$date));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;

        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);  
   
        return $form_path;   
 }
 
             
    /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteForm($form_path)
    {
        return deleteFileUtils($form_path);
    }
}

?>

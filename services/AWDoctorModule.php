<?php

require_once "Connection.php";
require_once "Utils.php";
//define('tfClientId','online');

class AWDDoctorModuleTaskPatient
{
    /**
    * @var string
    */
    var $TaskID;
    
    /**
    * @var string
    */
    var $TaskBeginTime;
    
    /**
    * @var string
    */
    var $TaskEndTime;  
      
    /**
    * @var string
    */
    var $TaskFactVisit;
    
    /**
    * @var string
    */
    var $TaskFactOutVisit;
    
    /**
    * @var string
    */
    var $TaskNoticed;
    
    /**
    * @var string
    */
    var $TaskWorkDescription;
     
    /**
    * @var string
    */
    var $TaskComment;
     
    /**
    * @var string
    */
    var $PatientID;
    
    /**
    * @var string
    */
    var $PatientLastname;
     
    /**
    * @var string
    */
    var $PatientFirstname;
    
    /**
    * @var string
    */
    var $PatientMiddlename;    
   
    /**
    * @var string
    */
    var $PatientFullCardNumber;
    
    /**
    * @var string
    */
    var $PatientPrimaryFlag;
     
    /**
    * @var string
    */
    var $PatientTelephone;
    
    /**
    * @var string
    */
    var $PatientMobile;
     
    /**
    * @var string
    */
    var $PatientBirthday;
     
    /**
    * @var string
    */
    var $PatientSex;  
          
    /**
    * @var string
    */
    var $PatientTestFlag;
    
    /**
    * @var integer
    */
    var $InvoiceToSaveEnableFlag;  
    
    //количество осмотров
    /**
    * @var int
    */
    var $COUNTS_TOOTHEXAMS;
    //количество выполоненых процедур
    /**
    * @var int
    */
    var $COUNTS_PROCEDURES_CLOSED;
    //количество не выполоненых процедур
    /**
    * @var int
    */
    var $COUNTS_PROCEDURES_NOTCLOSED;
    //количество файлов
    /**
    * @var int
    */
    var $COUNTS_FILES;
    
    
	/**
    * @var string
    */
    var $task_range;
	/**
    * @var string
    */
    var $full_addres;
    /**
    * @var string
    */
    var $full_fio;
    /**
    * @var number
    */
    var $discount_amount;
    /**
    * @var int
    */
    var $is_returned;
    /**
    * @var int
    */
    var $patient_ages;
    /**
    * @var string
    */
    var $patient_notes;
    /**
    * @var string
    */
    var $patient_leaddoctor;
    
    /**
    * @var AWDDoctorModule_mobilephone[]
    */
    var $mobiles; 
}

class AWDDoctorModule_mobilephone
{
    /**
    * @var string
    */
    var $id; 
    /**
    * @var string
    */
    var $parent_fk; 
    /**
    * @var string
    */
    var $phone; 
    /**
    * @var int
    */
    var $isActive; 
    /**
    * @var string
    */
    var $comments; 
}

class AWDDoctorModuleTasksTimes
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $startTime;
     
    /**
    * @var string
    */
    var $endTime;
}


class AWDDoctorModuleRoom
{
    /**
    * @var string
    */
    var $ID;
       
    /**
    * @var string
    */
    var $roomNumber;
     
    /**
    * @var string
    */
    var $roomName;
    
    /**
    * @var AWDDoctorModuleWPalce[]
    */
    var $workPlaces;	
}

        class AWDDoctorModuleWPalce
        {
            /**
            * @var string
            */
            var $ID;
             
            /**
            * @var string
            */
            var $wplaceNumber;
            
            /**
            * @var string
            */
            var $wplaceName;	
        }

class AWDDoctorModuleTableTask
{
    /**
    * @var string
    */
    var $TASKDATE;
     
    /**
    * @var string
    */
    var $TASKBEGINOFTHEINTERVAL;
     
    /**
    * @var string
    */
    var $TASKENDOFTHEINTERVAL;
    
    /**
    * @var string
    */
    var $TASKPATIENTID;
     
    /**
    * @var string
    */
    var $TASKDOCTORID;
    
    /**
    * @var string
    */
    var $TASKROOMID;
     
    /**
    * @var string
    */
    var $TASKWORKPLACEID;
    
    /**
    * @var string
    */
    var $TASKWORKDESCRIPTION;
     
    /**
    * @var string
    */
    var $TASKCOMMENT;	
}


class AWDDoctorModuleAssistant
{
   /**
   * @var int
   */
	var $assid;
   /**
   * @var string
   */
	var $name; 
}

class AWDoctorModule
{
    
     /**
     * 
     * @param AWDDoctorModuleTaskPatient $p1
     * @param AWDDoctorModuleTasksTimes $p2
     * @param AWDDoctorModuleTableTask $p3
     * @param AWDDoctorModuleRoom $p4
     * @param AWDDoctorModuleWPalce $p5
     * @param AWDDoctorModuleAssistant $p6
     * @param AWDDoctorModule_mobilephone $p7
     * @return void
     */
    public function registerTypes($p1,$p2,$p3,$p4,$p5,$p6,$p7){}
    
    /** 
 	* @param string $patient_id
    * @return AWDDoctorModule_mobilephone[]
	*/ 
    private function getMobilePhones($patient_id, $trans)
    {
         $mobile_sql =
			"select patientsmobile.id,
                patientsmobile.parent_fk,
                patientsmobile.mobilenumber,
                patientsmobile.senderactive,
                patientsmobile.comments
            from patientsmobile
                where patientsmobile.patient_fk = $patient_id
            order by patientsmobile.senderactive desc, patientsmobile.id desc";
        			
        $mobile_query = ibase_prepare($trans, $mobile_sql);
        $mobile_result=ibase_execute($mobile_query);
        $rows = array();
		while ($row = ibase_fetch_row($mobile_result))
		{ 
            $obj = new AWDDoctorModule_mobilephone;
            $obj->id = $row[0];
            $obj->parent_fk = $row[1];
            $obj->phone = $row[2];
            if($row[3] == 1)
            {
                $obj->isActive = true;
            }
            else
            {
                $obj->isActive = false; 
            }
            $obj->comments = $row[4];
            $rows[] = $obj;
		}	
        ibase_free_query($mobile_query);
        ibase_free_result($mobile_result);
        return $rows;
    }
    
    /** 
 	* @param string $lastname
    * @param string $doctorID
    * @param string $selecteddate
    * @return AWDDoctorModuleTaskPatient[]
	*/    
    public function getPatientsFiltered($lastname, $doctorID, $selecteddate) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select
                TASKS.ID,
                TASKS.BEGINOFTHEINTERVAL,
                TASKS.ENDOFTHEINTERVAL,
                TASKS.FACTOFVISIT,
                TASKS.NOTICED,
                TASKS.WORKDESCRIPTION,
                TASKS.COMMENT,
                PATIENTS.ID,
                PATIENTS.LASTNAME,
                PATIENTS.FIRSTNAME,
                PATIENTS.MIDDLENAME,
                PATIENTS.CARDBEFORNUMBER,
                PATIENTS.CARDNUMBER,
                PATIENTS.CARDAFTERNUMBER,
                PATIENTS.PRIMARYFLAG,
                PATIENTS.TELEPHONENUMBER,
                PATIENTS.MOBILENUMBER,
                PATIENTS.BIRTHDAY,
                PATIENTS.SEX,
                0,
                patients.testpatologyflag,
                patients.counts_toothexams, 
                patients.counts_procedures_closed, 
                patients.counts_procedures_notclosed, 
                patients.counts_files,
                    patients.is_returned,
                    patients.country, patients.state, patients.ragion, patients.city, patients.address, patients.hobby,
                    patients.discount_amount,
                    doctors.shortname,
                    
                TASKS.FACTOFOUT
                
            
            from TASKS
                left join PATIENTS on (TASKS.PATIENTID = PATIENTS.ID)

                left join doctors on (patients.leaddoctor_fk = doctors.id)
            
           where TASKS.DOCTORID = '$doctorID'
                and TASKS.taskdate = '$selecteddate'";
       
	    if ($lastname!="-")	
            {$QueryText=$QueryText." and LOWER(patients.LASTNAME) LIKE LOWER('$lastname%')";}
        			
        $QueryText=$QueryText." order by TASKS.BEGINOFTHEINTERVAL asc;";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AWDDoctorModuleTaskPatient;
            $obj->TaskID = $row[0];
            $obj->TaskBeginTime = $row[1];
            $obj->TaskEndTime = $row[2];
            $obj->TaskFactVisit = $row[3];
            $obj->TaskNoticed = $row[4];
            $obj->TaskWorkDescription = $row[5];
            $obj->TaskComment = $row[6];
            $obj->PatientID = $row[7];
            $obj->PatientLastname = $row[8];
            $obj->PatientFirstname = $row[9];
            $obj->PatientMiddlename = $row[10];
            $obj->PatientFullCardNumber = "{$row[11]}{$row[12]}{$row[13]}";
            $obj->PatientPrimaryFlag = $row[14];
			$obj->PatientTelephone = $row[15];
			$obj->PatientMobile = $row[16];
			$obj->PatientBirthday = $row[17];
			$obj->PatientSex = $row[18];
            $obj->InvoiceToSaveEnableFlag = $row[19];
            $obj->PatientTestFlag = $row[20];
            $obj->COUNTS_TOOTHEXAMS = $row[21];
            //$obj->COUNTS_PROCEDURES_CLOSED = $row[22];
            //$obj->COUNTS_PROCEDURES_NOTCLOSED = $row[23];
            $obj->COUNTS_FILES = $row[24];
            
            
            $obj->task_range = substr($obj->TaskBeginTime, 0, 5).' - '.substr($obj->TaskEndTime, 0, 5);
            $obj->full_fio = $obj->PatientLastname.' '.$obj->PatientFirstname.' '.$obj->PatientMiddlename;
            $obj->is_returned = $row[25];
            $obj->full_addres = '';
            if(($row[26] != '')&&($row[26] != null))
            {
              $obj->full_addres .= $row[26].', ';  
            }
            if(($row[27] != '')&&($row[27] != null))
            {
              $obj->full_addres .= $row[27].', ';  
            }
            if(($row[28] != '')&&($row[28] != null))
            {
              $obj->full_addres .= $row[28].', ';  
            }
            if(($row[29] != '')&&($row[29] != null))
            {
              $obj->full_addres .= $row[29].', ';  
            }
            if(($row[30] != '')&&($row[30] != null))
            {
              $obj->full_addres .= $row[30];  
            }
            $obj->patient_ages = $this->getAges($obj->PatientBirthday);
            $obj->patient_notes = $row[31];
            $obj->discount_amount = $row[32];
            $obj->patient_leaddoctor = $row[33];
            $obj->TaskFactOutVisit = $row[34];
            
            $obj->mobiles = $this->getMobilePhones($obj->PatientID, $trans);
            
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
       /** 
 	* @param string $lastname
    * @param string $doctorID
    * @return AWDDoctorModuleTaskPatient[]
	*/    
    public function getDoctorsPatientsFiltered($lastname, $doctorID) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select distinct
                PATIENTS.ID as PatientID,
                PATIENTS.LASTNAME,
                PATIENTS.FIRSTNAME,
                PATIENTS.MIDDLENAME,
                PATIENTS.CARDBEFORNUMBER,
                PATIENTS.CARDNUMBER,
                PATIENTS.CARDAFTERNUMBER,
                PATIENTS.PRIMARYFLAG,
                PATIENTS.TELEPHONENUMBER,
                PATIENTS.MOBILENUMBER,
                PATIENTS.BIRTHDAY,
                PATIENTS.SEX,
                patients.counts_toothexams, 
                patients.counts_procedures_closed, 
                patients.counts_procedures_notclosed, 
                patients.counts_files,
                0
            
            from TASKS
                left join PATIENTS on (TASKS.PATIENTID = PATIENTS.ID)
            
           where TASKS.DOCTORID = '$doctorID'";
	    if ($lastname!="-")	
            {$QueryText=$QueryText." and LOWER(LASTNAME) LIKE LOWER('$lastname%')";}
        		
        $QueryText=$QueryText." and PRIMARYFLAG = 1 order by CARDNUMBER";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AWDDoctorModuleTaskPatient;
            $obj->PatientID = $row[0];
            $obj->PatientLastname = $row[1];
            $obj->PatientFirstname = $row[2];
            $obj->PatientMiddlename = $row[3];
            $obj->PatientFullCardNumber = "{$row[4]}{$row[5]}{$row[6]}";
            $obj->PatientPrimaryFlag = $row[7];
			$obj->PatientTelephone = $row[8];
			$obj->PatientMobile = $row[9];
			$obj->PatientBirthday = $row[10];
			$obj->PatientSex = $row[11];
            $obj->COUNTS_TOOTHEXAMS = $row[12];
            $obj->COUNTS_PROCEDURES_CLOSED = $row[13];
            $obj->COUNTS_PROCEDURES_NOTCLOSED = $row[14];
            $obj->COUNTS_FILES = $row[15];
            $obj->InvoiceToSaveEnableFlag = $row[16];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    /** 
    * @param string $doctorID
    * @return int
	*/    
    public function getDoctorsPatientsCount($doctorID) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection); 
        $patientsCount = 0; 
	    $QueryText =
			"select count(distinct PATIENTS.ID)
            
            from TASKS
                left join PATIENTS on (TASKS.PATIENTID = PATIENTS.ID)
            
           where TASKS.DOCTORID = '$doctorID'
         and PRIMARYFLAG = 1";
         
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        if ($row = ibase_fetch_row($result))
        {
            $patientsCount=$row[0];
        }
		
        
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $patientsCount;
    }    
    
    
    /** 
 	* @param string $lastname
    * @return AWDDoctorModuleTaskPatient[]
	*/    
    public function getAllPatientsFiltered($lastname) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select distinct
                PATIENTS.ID as PatientID,
                PATIENTS.LASTNAME,
                PATIENTS.FIRSTNAME,
                PATIENTS.MIDDLENAME,
                PATIENTS.CARDBEFORNUMBER,
                PATIENTS.CARDNUMBER,
                PATIENTS.CARDAFTERNUMBER,
                PATIENTS.PRIMARYFLAG,
                PATIENTS.TELEPHONENUMBER,
                PATIENTS.MOBILENUMBER,
                PATIENTS.BIRTHDAY,
                PATIENTS.SEX,
                patients.counts_toothexams, 
                patients.counts_procedures_closed, 
                patients.counts_procedures_notclosed, 
                patients.counts_files
                from PATIENTS where";
	    if ($lastname!="-")	
            {$QueryText=$QueryText." LOWER(LASTNAME) LIKE LOWER('$lastname%') and";}
        		
        $QueryText=$QueryText." PRIMARYFLAG = 1 order by patients.LASTNAME";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AWDDoctorModuleTaskPatient;
            $obj->PatientID = $row[0];
            $obj->PatientLastname = $row[1];
            $obj->PatientFirstname = $row[2];
            $obj->PatientMiddlename = $row[3];
            $obj->PatientFullCardNumber = "{$row[4]}{$row[5]}{$row[6]}";
            $obj->PatientPrimaryFlag = $row[7];
			$obj->PatientTelephone = $row[8];
			$obj->PatientMobile = $row[9];
			$obj->PatientBirthday = $row[10];
			$obj->PatientSex = $row[11];
            $obj->COUNTS_TOOTHEXAMS = $row[12];
            //$obj->COUNTS_PROCEDURES_CLOSED = $row[13];
            //$obj->COUNTS_PROCEDURES_NOTCLOSED = $row[14];
            $obj->COUNTS_FILES = $row[15];
            $obj->mobiles = $this->getMobilePhones($obj->PatientID, $trans);
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    
    
    
    
        /**
     * 
     * @param string $doctorId
     * @param string $selectedDate
    * @return AWDDoctorModuleTasksTimes[]
	*/    
    public function getDoctorTasks($doctorId,$selectedDate) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select
                    TASKS.ID,
                    TASKS.BEGINOFTHEINTERVAL,
                    TASKS.ENDOFTHEINTERVAL
                
                from TASKS
                
                where TASKS.DOCTORID = '$doctorId'
                    and TASKS.taskdate = '$selectedDate'
                
                order by TASKS.BEGINOFTHEINTERVAL asc;";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
        
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AWDDoctorModuleTasksTimes;
            $obj->ID = $row[0];
            $obj->startTime = $row[1];
            $obj->endTime = $row[2];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    /**
 	* @param  AWDDoctorModuleTableTask $task
    * @return boolean
 	*/
    public function addTableTask($task) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = format_for_query("insert into TASKS (
                                        id, 
                                        taskdate, 
                                        beginoftheinterval, 
                                        endoftheinterval, 
                                        patientid, 
                                        doctorid, 
                                        roomid, 
                                        workplaceid, 
                                        noticed, 
                                        factofvisit, 
                                        dateofvisit, 
                                        timeofvisit, 
                                        workdescription, 
                                        comment)
        		values(null, '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '0', '0', null, null, '{7}', '{8}')", 
                $task->TASKDATE, 
                $task->TASKBEGINOFTHEINTERVAL, 
                $task->TASKENDOFTHEINTERVAL,
                $task->TASKPATIENTID, 
                $task->TASKDOCTORID, 
                $task->TASKROOMID, 
                $task->TASKWORKPLACEID,
                $task->TASKWORKDESCRIPTION, 
                $task->TASKCOMMENT);
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		//$success = ibase_commit($trans);
    		ibase_free_query($query);
        
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;		
    }    
    
    /** 
    * @return AWDDoctorModuleRoom[]
 	*/
    public function getRooms() //sqlite
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText = "select ROOMS.ID, ROOMS.NUMBER, ROOMS.NAME from ROOMS";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
        
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AWDDoctorModuleRoom;
            $obj->ID = $row[0];
            $obj->roomNumber = $row[1];
            $obj->roomName = $row[2];
             $TemplQuery = "select WORKPLACES.ID, WORKPLACES.NUMBER, WORKPLACES.DESCRIPTION from WORKPLACES where WORKPLACES.roomid = '$row[0]'";
             $templquery = ibase_prepare($trans, $TemplQuery);
             $templresult = ibase_execute($templquery);
             while ($templrow = ibase_fetch_row($templresult))
             {
                $templ = new AWDDoctorModuleWPalce;
                $templ->ID = $templrow[0];
                $templ->wplaceNumber = $templrow[1];
                $templ->wplaceName = $templrow[2];
                $obj->workPlaces[] = $templ;
             }
             ibase_free_query($templquery);
             ibase_free_result($templresult);
            
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    // ********************************************* ASSISTANTS START *******************************************
    /**
    * @return AWDDoctorModuleAssistant[]
	*/    
    public function getAssistantsList() //sqlite
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	   $QueryText = "select ID, LASTNAME || ' ' || FIRSTNAME FROM ASSISTANTS  WHERE 
                            (DATEWORKEND >= cast('NOW' as date) or DATEWORKEND IS null)";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AWDDoctorModuleAssistant;
            $obj->assid = $row[0];
            $obj->name = $row[1];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    // ********************************************* ASSISTANTS END *******************************************
    
    //***********************************************PRIVATE FUNCTIONS START***********************************************
     /**
     * AWDoctorModule::getAges()
     * 
     * @param string $birthdate
     * @return int
     */
    private function getAges($birthdate)
    {
        if(($birthdate != null)&&($birthdate != ''))
        {
            $date = new DateTime($birthdate);
            $now = new DateTime();
            $interval = $now->diff($date);
            return $interval->y;
        }
        else
        {
            return -1;
        }
    }
    //***********************************************PRIVATE FUNCTIONS END*************************************************
}
?>
<?php

require_once 'Connection.php';
require_once "Utils.php";

class HygieneModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

   /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $HYGDESCRIPT;
}
 
class HygieneModule
{		
    /**
     * @param HygieneModuleDictionaryObject $p1
     * @return void
     */
    public function registertypes($p1)
    {}
    
    /** 
 	*  @return HygieneModuleDictionaryObject[]
	*/ 
    public function getHygiene() //sqlite
    { 
   	    $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, HYGDESCRIPT from Hygiene order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
			
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new HygieneModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->HYGDESCRIPT = $row[2];
			$objs[] = $obj;
		}	
		
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param HygieneModuleDictionaryObject $hvo
    * @return boolean
 	*/
    public function addHygiene($hvo) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into Hygiene (ID, NUMBER, HYGDESCRIPT) values (null,$hvo->NUMBER, '".safequery($hvo->HYGDESCRIPT)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param HygieneModuleDictionaryObject $hvo
    * @return boolean
 	*/
    public function updateHygiene($hvo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Hygiene
					 set HYGDESCRIPT ='".safequery($hvo->HYGDESCRIPT)."'
					     where ID = '$hvo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }
    
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteHygiene($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Hygiene where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param HygieneModuleDictionaryObject $hvo
    * @return boolean
 	*/
    public function updateHygieneNumber($hvo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Hygiene
					 set NUMBER ='$hvo->NUMBER'
					     where ID = '$hvo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }     
  }
?>
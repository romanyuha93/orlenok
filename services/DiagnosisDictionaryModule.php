<?php

require_once 'Connection.php';
require_once "Utils.php";

class DiagnosisDictionaryModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

  /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;

 }
 
class DiagnosisDictionaryModule
{	
    /**
     * @param DiagnosisDictionaryModuleDictionaryObject $p1
     * @return void
     */
    public function registertypes($p1)
    {}
    
    /** 
 	*  @return DiagnosisDictionaryModuleDictionaryObject[]
	*/ 
    public function getDiagnosis() 
    { 
   	    $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from Diagnos order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new DiagnosisDictionaryModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param DiagnosisDictionaryModuleDictionaryObject $ovo, 
    * @return boolean
 	*/
    public function addDiagnosis ($ovo) 
    {
		$connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into Diagnos (id, number, description) values (null, $ovo->NUMBER, '".safequery($ovo->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param DiagnosisDictionaryModuleDictionaryObject $ovo, 
    * @return boolean
 	*/
    public function updateDiagnosis ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Diagnos set DESCRIPTION ='".safequery($ovo->DESCRIPTION)."' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }
    /**
 	* @param string[] $Arguments, 
    * @return boolean
 	*/ 		 		
    public function deleteDiagnosis($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Diagnos where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param DiagnosisDictionaryModuleDictionaryObject $ovo, 
    * @return boolean
 	*/
    public function updateDiagnosisNumber ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Diagnos set NUMBER ='$ovo->NUMBER' where ID = '$ovo->ID' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }      
  }
?>
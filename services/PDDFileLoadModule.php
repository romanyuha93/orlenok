<?php
require_once "Connection.php";
require_once "Settings.php";
require_once "Utils.php";
require_once "PDDService.php";
require_once (dirname(__FILE__).'/thumbnail/ThumbLib.inc.php');

if($_SERVER['REQUEST_METHOD']=='POST')
{
    $isThumbnailable=true;
    chmod($_FILES['Filedata']['tmp_name'],0777);
    try 
    {
        $thumb = PhpThumbFactory::create($_FILES['Filedata']['tmp_name']);  
    } 
    catch (Exception $e) 
    {
        $isThumbnailable=false;
    } 
    if($isThumbnailable) 
    {
        $thumb->resize(160, 160)->save(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$_GET['servername'].'_thumb');
        chmod(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$_GET['servername'].'_thumb',0777);
    }
   $sf=new PDD_ServerFile;
   $sf->name=$_GET['name'];
   $sf->extention=$_GET['extention'];
   $sf->serverName=$_GET['servername'];
   $sf->patientId=$_GET['patientId'];
   $sf->type=$_GET['type'];
   $sf->isThumbnailable=$isThumbnailable;
   $sf->comment='';
   PDDService::addServerFile($sf);
   
    move_uploaded_file($_FILES['Filedata']['tmp_name'], dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$_GET['servername']); 
    chmod(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$_GET['servername'],0777);
}
else if($_SERVER['REQUEST_METHOD']=='GET')
{
    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($_GET,true));
    readfile(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$_GET['servername']);
}
?>

<?php
if($_SERVER['QUERY_STRING']!=null&&$_SERVER['QUERY_STRING']!='')
{
    //print_r($_SERVER);
    if($_GET["app"] == 'TF')
    {
        
    	if($_GET["img"])
    	{
    		echo '<head>
                	<meta charset="UTF-8"/>
                </head>
            <body>'.ToothFairyReleases::getReleasePageContent($_GET["v"], $_GET["img"]).'</body>'; 
    	}
        else
        {
            echo '<head>
                	<meta charset="UTF-8"/>
                </head>
            <body>'.ToothFairyReleases::getReleasePageContent($_GET["v"]).'</body>'; 
        }
    } 
    if($_GET["app"] == 'NH')
    {
    	if($_GET["img"])
    	{
    		echo '<head>
                	<meta charset="UTF-8"/>
                </head>
            <body>'.ToothFairyReleases::getNHReleasePageContent($_GET["v"], $_GET["locale"], $_GET["img"]).'</body>'; 
    	}
        else
        {
            echo '<head>
                	<meta charset="UTF-8"/>
                </head>
            <body>'.ToothFairyReleases::getNHReleasePageContent($_GET["v"], $_GET["locale"]).'</body>'; 
        }
    }
}
//echo ToothFairyReleases::getReleasePageContent("3-1-76");

class ToothFairyReleases
{
       /**
     * @param string $versionStr  
     * @param string $img   
     * @return string
     */
    public static function getReleasePageContent($versionStr, $img = null)
    {
        if($img != null)
        {
            $result='<a href="'.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'/?app=TF&v='.$versionStr.'">Назад</a><br/><img src="'.$img.'"/>';
        }
        else
        {
            $page=file_get_contents ("http://royal.co.ua/releases/$versionStr/");
            $start=strpos($page,"<div class='post-body'>");
            $end=strpos($page,"</div>",$start);
            $nextDiv=strpos($page,"<div",$start);
         //echo $end."<br/>";
        //echo $nextDiv."<br/>";
        while($nextDiv<$end)
        {
                if($nextDiv==-1)
                {
                    break;
                }
                else
                {
                  $end=strpos($page,"</div>",$end+1);
                  $nextDiv=strpos($page,"<div",$end);
                  //echo $end."<br/>";
                  //echo $nextDiv."<br/>";
                }
            }
            
            $result=substr($page,$start,$end-$start+6);
            preg_match_all("/http:\/\/royal.co.ua\/product\/[^\/]+\//", $result, $matches);
            foreach($matches as $ms)
            {
               foreach($ms as $match)
                {
                    //app=TF&v=4-0-001
                    $result=str_replace($match,"http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?app=TF&v=".substr($match,27,strlen($match)-28),$result);
                }
            }

            preg_match_all("/href=\"http:\/\/royal.co.ua\/wp-content[^\"]+/", $result, $matches2);
            foreach($matches2 as $ms)
            {
               foreach($ms as $match)
                {
                    $result=str_replace($match,"href=\"http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?app=TF&v=".$versionStr."&img=".substr($match,6),$result);
                }
            }
            
            $result=str_replace("/gateway.php","/ToothFairyReleases.php",$result);
        }
       ///file_put_contents("C:/tmp.txt", $result);
       //$result=' ';	  
      return  $result;
    }
    
       /**
     * @param string $versionStr 
     * @param string $local   
     * @param string $img   
     * @return string
     */
    public static function getNHReleasePageContent($versionStr, $local, $img = null)
    {
        $localPrefix = $local;
        if($local != null && $local != '')
        {
            $localPrefix = $local.'.';
        }
        else
        {
            $localPrefix = '';
        }
            
        if($img != null)
        {
            $result='<a href="'.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'/?app=NH&v='.$versionStr.'&locale='.$local.'">Назад</a><br/><img src="'.$img.'"/>';
        }
        else
        {
            $page=file_get_contents ("http://".$localPrefix."vikisoft.com.ua/nhealth/$versionStr/");
            $start=strpos($page,"<section>");
            $end=strpos($page,"</section>",$start);
            /*$nextDiv=strpos($page,"<div",$start);
         
        while($nextDiv<$end)
        {
                if($nextDiv==-1)
                {
                    break;
                }
                else
                {
                  $end=strpos($page,"</div>",$end+1);
                  $nextDiv=strpos($page,"<div",$end);
                  //echo $end."<br/>";
                  //echo $nextDiv."<br/>";
                }
            }
            */
            $result=substr($page,$start,$end-$start+10);
            preg_match_all("/http:\/\/".$localPrefix."vikisoft.com.ua\/nhealth\/[^\/]+\//", $result, $matches);
            foreach($matches as $ms)
            {
               foreach($ms as $match)
                {
                    //file_put_contents("C:/tmp.txt", substr($match,34,strlen($match)-35));
                    //app=NH&v=3-13-194&locale=ru
                    $result=str_replace($match,"http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?app=NH&v=".substr($match,34,strlen($match)-35)."&locale=".$local,$result);
                }
            }

            preg_match_all("/href=\"http:\/\/".$localPrefix."vikisoft.com.ua\/wp-content[^\"]+/", $result, $matches2);
            foreach($matches2 as $ms)
            {
               foreach($ms as $match)
                {
                    $result=str_replace($match,"href=\"http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?app=NH&v=".$versionStr."&locale=".$local."&img=".substr($match,6),$result);
                }
            }
            
            $result=str_replace("/gateway.php","/ToothFairyReleases.php",$result);
        }
       ///file_put_contents("C:/tmp.txt", $result);
       //$result=' ';	  
      return  $result;
    }
}

?>
<?php

require_once 'Connection.php';
require_once "Utils.php";

class AnamnesisDictionaryModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;

}

 
class AnamnesisDictionaryModule
{		
    
     /**
     * @param AnamnesisDictionaryModuleDictionaryObject $p1
     * 
     * @return void
     */
    public function registerTypes($p1){}
    
    /** 
 	*  @return AnamnesisDictionaryModuleDictionaryObject[]
	*/ 
    public function getAnamnesis() 
    { 
   	    $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from Anamnes order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new AnamnesisDictionaryModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param AnamnesisDictionaryModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function addAnamnesis ($ovo) 
    {
		$connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into Anamnes (ID, NUMBER, DESCRIPTION) values (null, $ovo->NUMBER, '".safequery($ovo->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param AnamnesisDictionaryModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updateAnamnesis ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Anamnes set DESCRIPTION ='".safequery($ovo->DESCRIPTION)."' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);	
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteAnamnesis($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Anamnes where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param AnamnesisDictionaryModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updateAnamnesisNumber ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Anamnes set NUMBER ='$ovo->NUMBER' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }      
  }
?>
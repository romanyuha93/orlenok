<?php

require_once "Connection.php";

class StorageDocumentsModuleInvoice
{				
	/**
	* @var string
	*/
	var $ID;          

	/**
	* @var string
	*/
	var $DIRECTION;
	 
	/**
	* @var string
	*/
	var $DESTINATIONID;  
	
	/**
	* @var string
	*/
	var $DESTINATIONNAME;
	
	/**
	* @var string
	*/
	var $DATETIME;
	
	/**
	* @var string
	*/
	var $INVOICENUMBER;
	 
	/**
	* @var string
	*/
	var $SUMM;  
	
	/**
	* @var string
	*/
	var $TYPE;
	
	/**
	* @var object
	*/
	var $records;
}

class StorageDocumentsModuleData
{				
	/**
	* @var StorageDocumentsModuleTableRecord[]
	*/
	var $invoices;  	
	
	/**
	* @var StorageDocumentsModuleClinicRegistrationData
	*/
	var $clinicRegistrationData; 
}

class StorageDocumentsModuleTableRecord
{		
	/**
	* @var string
	*/
	var $YEAR;          

	/**
	* @var string
	*/
	var $MONTH;
	 
	/**
	* @var string
	*/
	var $DAYGROUP;  
	 
	/**
	* @var string
	*/
	var $DAY;  
	
	/**
	* @var string
	*/
	var $TYPE;
	
	/**
	* @var string
	*/
	var $DIRECTION;	
}

class StorageDocumentsModuleClinicRegistrationData
{
	/**
	* @var string
	*/
	var $NAME;
	
	/**
	* @var string
	*/
	var $ADDRESS;
	
	/**
	* @var string
	*/
	var $CODE;	
	
	/**
	* @var string
	*/
	var $TELEPHONENUMBER;		
}

class StorageDocumentsModuleInvoiceRecord
{
	/**
	* @var string
	*/
	var $FARMNAME;
	
	/**
	* @var string
	*/
	var $QUANTITY;
	
	/**
	* @var string
	*/
	var $UNITOFMEASURENAME;
	
	/**
	* @var string
	*/
	var $PRICE;	
	
	/**
	* @var string
	*/
	var $SUMM;
}

class StorageDocumentsModule
{	
	/**
    * @param StorageDocumentsModuleInvoice $p1
    * @param StorageDocumentsModuleData $p2
    * @param StorageDocumentsModuleTableRecord $p3
    * @param StorageDocumentsModuleClinicRegistrationData $p4
    * @param StorageDocumentsModuleInvoiceRecord $p5    
    */    
	public function registertypes($p1, $p2, $p3, $p4, $p5) {}
		
	/** 	
	* @return StorageDocumentsModuleData
	*/ 
	public function getStorageDocumentsModuleData() 
	{
			$connection = new Connection();			
			$connection->EstablishDBConnection();			
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			$returnObj = new StorageDocumentsModuleData();
			
			// ôîðìóºìî òåêñò çàïèòó
			$QueryText = '
			select distinct
				EXTRACT(year FROM DATETIME) "YEAR",
				EXTRACT(month FROM DATETIME) "MONTH",
				EXTRACT(day FROM DATETIME) "DAY",
				0 "TYPE",
				DIRECTION
			from externalmovementregister
			union all
			select distinct
				EXTRACT(year FROM DATETIME) "YEAR",
				EXTRACT(month FROM DATETIME) "MONTH",
				EXTRACT(day FROM DATETIME) "DAY",
				1 "TYPE",
				DIRECTION ds
			from internalmovementregister
			order by 1 desc, 2, 3, 4, 5';
			
			// ãîòóºìî ñåðâåð äî çàïèòó
			$query = ibase_prepare($trans, $QueryText);
			
			// âèêîíóºìî çàïèò
			$result = ibase_execute($query);
				
			// îãîëîøóºìî ìàñèâ çàïèñ³â ðåçóëüòàòó çàïèòó
			$rows = array();		
			
			while ($row = ibase_fetch_row($result))				    		// öèêë ïî çàïèñàì ðåçóëüòàòó çàïèòó
			{ 
				$obj = new StorageDocumentsModuleTableRecord();		 	// ñòâîðþºìî íîâèé îá'ºêò StorageDocumentsModuleTableRecord
				
				$obj->YEAR = $row[0];						
				$obj->MONTH = $row[1];
				$obj->DAY = $row[2];
				
				if ($obj->DAY < 11)	$obj->DAYGROUP = "1";
				if (($obj->DAY > 10) && ($obj->DAY < 21))	$obj->DAYGROUP = "2";
				if ($obj->DAY > 20)	$obj->DAYGROUP = "3";
										
				$obj->TYPE = $row[3];
				$obj->DIRECTION = $row[4];
					
				$rows[] = $obj;																	// çàïèñóºìî îá'ºêò $obj òèïó StorageDocumentsModuleTableRecord â ìàñèâ ðåçóëüòàò³â
			}				
				
			$returnObj->invoices = $rows;		
			
			// ôîðìóºìî òåêñò çàïèòó		
			$QueryText = 
			"select 
				LEGALNAME,
				ADDRESS,
				CODE,
				TELEPHONENUMBER
			from CLINICREGISTRATIONDATA";
			
			// ãîòóºìî ñåðâåð äî çàïèòó
			$query = ibase_prepare($trans, $QueryText);
			
			// âèêîíóºìî çàïèò
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))   											// ÿêùî ðåçóëüòàò íå ïîðîæí³é
			{
				$obj = new StorageDocumentsModuleClinicRegistrationData;	// ñòâîðþºìî íîâèé îá'ºêò StorageDocumentsModuleClinicRegistrationData
				
				$obj->NAME = $row[0];	
				$obj->ADDRESS = $row[1];
				$obj->CODE = $row[2];
				$obj->TELEPHONENUMBER = $row[3];
				
				$returnObj->clinicRegistrationData = $obj;
			}			
		
			$success = ibase_commit($trans);
			
			$connection->CloseDBConnection();
				
			
			return $returnObj;
	}	
	
	/** 	
	* @param object $newStorageDocumentsModuleInvoice
	*
	* @return StorageDocumentsModuleInvoice[]
	*/ 
	public function getStorageDocumentsInvoices($newStorageDocumentsModuleInvoice) 
	{
			$connection = new Connection();			
			$connection->EstablishDBConnection();			
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			if ($newStorageDocumentsModuleInvoice->TYPE == 0)
			{
				// ôîðìóºìî òåêñò çàïèòó
				$QueryText = "
				select
						externalmovementregister.ID,
						externalmovementregister.DIRECTION,
						externalmovementregister.SUPPLIERID DESTINATIONID,
						supplier.NAME DESTINATIONNAME,
						externalmovementregister.DATETIME,
						externalmovementregister.INVOICENUMBER,
						externalmovementregister.SUMM
				from externalmovementregister
				left join supplier on (supplier.ID = externalmovementregister.SUPPLIERID)			
				where (EXTRACT(year FROM DATETIME) = $newStorageDocumentsModuleInvoice->YEAR) and 
							(EXTRACT(month FROM DATETIME) = $newStorageDocumentsModuleInvoice->MONTH) and 
							(EXTRACT(day FROM DATETIME) = $newStorageDocumentsModuleInvoice->DAY)	and
							(DIRECTION = $newStorageDocumentsModuleInvoice->DIRECTION)
				order by DATETIME desc";
			}
			
			if ($newStorageDocumentsModuleInvoice->TYPE == 1)
			{
				// ôîðìóºìî òåêñò çàïèòó
				$QueryText = "				
				select
					internalmovementregister.ID,
					internalmovementregister.DIRECTION,
					internalmovementregister.DOCTORID DESTINATIONID,
					doctors.SHORTNAME DESTINATIONNAME,
					internalmovementregister.DATETIME,
					internalmovementregister.INVOICENUMBER,
					internalmovementregister.SUMM					
				from internalmovementregister
				left join doctors on (doctors.ID = internalmovementregister.DOCTORID)
				where (EXTRACT(year FROM DATETIME) = $newStorageDocumentsModuleInvoice->YEAR) and 
							(EXTRACT(month FROM DATETIME) = $newStorageDocumentsModuleInvoice->MONTH) and 
							(EXTRACT(day FROM DATETIME) = $newStorageDocumentsModuleInvoice->DAY)	and
							(DIRECTION = $newStorageDocumentsModuleInvoice->DIRECTION)
				order by DATETIME desc";
			}						
						
			// ãîòóºìî ñåðâåð äî çàïèòó
			$query = ibase_prepare($trans, $QueryText);
			
			// âèêîíóºìî çàïèò
			$result = ibase_execute($query);
				
			// îãîëîøóºìî ìàñèâ çàïèñ³â ðåçóëüòàòó çàïèòó
			$rows = array();		
			
			while ($row = ibase_fetch_row($result))				    		// öèêë ïî çàïèñàì ðåçóëüòàòó çàïèòó
			{ 
				$obj = new StorageDocumentsModuleInvoice();		 			// ñòâîðþºìî íîâèé îá'ºêò StorageDocumentsModuleInvoice
				
				$obj->NODETYPE = "item";
				$obj->ID = $row[0];						
				$obj->DIRECTION = $row[1];		
				$obj->DESTINATIONID = $row[2];	
				$obj->DESTINATIONNAME	 = $row[3];					
				$obj->DATETIME = $row[4];
				$obj->INVOICENUMBER = $row[5];
				$obj->SUMM = $row[6];
				$obj->TYPE = $newStorageDocumentsModuleInvoice->TYPE;
					
				$rows[] = $obj;																			// çàïèñóºìî îá'ºêò $obj òèïó StorageDocumentsModuleInvoice â ìàñèâ ðåçóëüòàò³â
			}					
		
			// ï³äòâåðäæóºìî òðàíçàêö³þ
			$success = ibase_commit($trans);
		
			// âèäàëÿºìî çàïèò
			ibase_free_query($query);
			
			// âèäàëºìî ðåçóëüòàò çàïèòó
			ibase_free_result($result);

			// çàêðèâàºìî ç'ºäíàííÿ ç ÁÄ
			$connection->CloseDBConnection();			
				
			
			return $rows;
	}	
	
	/** 	
	* @param StorageDocumentsModuleInvoice $invoice
	*
	* @return StorageDocumentsModuleInvoiceRecord[]
	*/ 
	public function getStorageDocumentsInvoiceRecords($invoice) 
	{
			$connection = new Connection();			
			$connection->EstablishDBConnection();			
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		
			if ($invoice->TYPE == 0)
			{
				// ôîðìóºìî òåêñò çàïèòó
				$QueryText = "
				select
						farm.NAME,
						extmovementregrecords.QUANTITY,
						extmovementregrecords.UNITOFMEASURENAME,
						extmovementregrecords.PRICE,
						extmovementregrecords.SUMM
				from extmovementregrecords
				left join farm on (farm.ID = extmovementregrecords.FARMID)
				where extmovementregrecords.EXTMOVEMENTREGID = $invoice->ID";
			}
			
			if ($invoice->TYPE == 1)
			{
				// ôîðìóºìî òåêñò çàïèòó
				$QueryText = "				
				select
						farm.NAME,
						intmovementregrecords.QUANTITY,
						intmovementregrecords.UNITOFMEASURENAME,
						intmovementregrecords.PRICE,
						intmovementregrecords.SUMM
				from intmovementregrecords
				left join farm on (farm.ID = intmovementregrecords.FARMID)
				where intmovementregrecords.INTMOVEMENTREGID = $invoice->ID";
			}						
			
			// ãîòóºìî ñåðâåð äî çàïèòó
			$query = ibase_prepare($trans, $QueryText);
			
			// âèêîíóºìî çàïèò
			$result = ibase_execute($query);
				
			// îãîëîøóºìî ìàñèâ çàïèñ³â ðåçóëüòàòó çàïèòó
			$rows = array();	
			
			while ($row = ibase_fetch_row($result))				    					// öèêë ïî çàïèñàì ðåçóëüòàòó çàïèòó
			{ 
				$obj = new StorageDocumentsModuleInvoiceRecord();		 			// ñòâîðþºìî íîâèé îá'ºêò StorageDocumentsModuleInvoiceRecord
							
				$obj->FARMNAME = $row[0];						
				$obj->QUANTITY = $row[1];		
				$obj->UNITOFMEASURENAME = $row[2];	
				$obj->PRICE	 = $row[3];					
				$obj->SUMM = $row[4];				
					
				$rows[] = $obj;																						// çàïèñóºìî îá'ºêò $obj òèïó StorageDocumentsModuleInvoiceRecord â ìàñèâ ðåçóëüòàò³â
			}					
		
			// ï³äòâåðäæóºìî òðàíçàêö³þ
			$success = ibase_commit($trans);
		
			// âèäàëÿºìî çàïèò
			ibase_free_query($query);
			
			// âèäàëºìî ðåçóëüòàò çàïèòó
			ibase_free_result($result);

			// çàêðèâàºìî ç'ºäíàííÿ ç ÁÄ
			$connection->CloseDBConnection();			
				
			
			return $rows;
	}
}
?>
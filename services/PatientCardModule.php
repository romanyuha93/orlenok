<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');

class PatientCardModuleAddPatientObject
{
    /**
    * @var int
    */
    var $ID;
    
    /**
    * @var string
    */
    var $CARDNUMBER_FULL;
    
    /**
    * @var int
    */
    var $CARDNUMBER;
    
    /**
    * @var string
    */
    var $CARDDATE;  
      
    /**
    * @var string
    */
    var $CARDBEFORNUMBER;
    
    /**
    * @var string
    */
    var $CARDAFTERNUMBER;
    
    /**
    * @var string
    */
    var $FIRSTNAME;
     
    /**
    * @var string
    */
    var $MIDDLENAME;
     
    /**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $FULLNAME;
     
    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var string
    */
    var $BIRTHDAY;    
   
    /**
    * @var int
    */
    var $SEX;
    
    /**
    * @var string
    */
    var $ADDRESS;
     
    /**
    * @var string
    */
    var $POSTALCODE;
    
    /**
    * @var string
    */
    var $TELEPHONENUMBER;
     
    /**
    * @var string
    */
    var $MOBILENUMBER;  
    
    /**
    * @var string
    */
    var $EMPLOYMENTPLACE;
    
    /**
    * @var string
    */
    var $JOB;
     
    /**
    * @var string
    */
    var $WORKPHONE;

    /**
    * @var int
    */
    var $VILLAGEORCITY;
 
    /**
    * @var int
    */
    var $POPULATIONGROUPE;
    
    /**
    * @var object
    */
    var $ISDISPANCER;
    
    /**
    * @var string
    */
    var $HOBBY;
    
    /**
    * @var string
    */
    var $COUNTRY;
    
    /**
    * @var string
    */
    var $STATE;
    
    /**
    * @var string
    */
    var $RAGION;
    
    /**
    * @var string
    */
    var $EMAIL;
    
    /**
    * @var string
    */
    var $CITY;
    
    /**
    * @var string
    */
    var $PASSPORT_SERIES;
    
    /**
    * @var string
    */
    var $PASSPORT_NUMBER;
    
    /**
    * @var string
    */
    var $PASSPORT_ISSUING;
    
    /**
    * @var string
    */
    var $PASSPORT_DATE;

    /**
    * @var string
    */
    var $WHEREFROMID;

    /**
    * @var string
    */
    var $FATHERFIRSTNAME;
    
    
    /**
    * @var string
    */
    var $MOTHERFIRSTNAME;
    
    
    /**
    * @var string
    */
    var $FATHERMIDDLENAME;
    
    
    /**
    * @var string
    */
    var $MOTHERMIDDLENAME;
    
    
    /**
    * @var string
    */
    var $FATHERLASTNAME;
    
    
    /**
    * @var string
    */
    var $MOTHERLASTNAME;
    
    
    /**
    * @var string
    */
    var $FATHERBIRTHDATE;
    
    
    /**
    * @var string
    */
    var $MOTHERBIRTHDATE;    
    
        /**
    * @var string
    */
    var $GROUPID;
    
        
    /**
    * @var string
    */
    var $RELATIVEID;
    
    /**
    * @var string
    */
    var $LEGALGUARDIANID;
    
	/**
    * @var int
    */
    var $INSURANCEID;
    
    	/**
    * @var int
    */
    var $INSURANCECOUNT;
    
    /**
    * @var int
    */
    var $TESTPATOLOGYFLAG;
    
    //количество осмотров
    /**
    * @var int
    */
    var $COUNTS_TOOTHEXAMS;
    //количество выполоненых процедур
    /**
    * @var int
    */
    var $COUNTS_PROCEDURES_CLOSED;
    //количество не выполоненых процедур
    /**
    * @var int
    */
    var $COUNTS_PROCEDURES_NOTCLOSED;
    //количество файлов
    /**
    * @var int
    */
    var $COUNTS_FILES;
    
	/**
    * @var string
    */
    var $full_addres;
    
    /**
    * @var string
    */
    var $full_fio;
    /**
    * @var number
    */
    var $discount_amount;
     /**
    * @var number
    */
    var $discount_amount_auto;
    
    /**
    * @var boolean
    */
    var $INDDISCOUNT;
    
    /**
    * @var string
    */
    var $DISCOUNTCARDNUMBER;
    
    /**
    * @var int
    */
    var $is_returned;
    
    /**
    * @var int
    */
    var $patient_ages;
    
    
    /**
    * @var string
    */
    var $patient_notes;
    
    /**
     * @var string
     */
     var $doctors;
    /**
     * @var string
     */
     var $doctor_lead;
    /**
     * @var string
     */
     var $agreementTimestamp;
     
     
     
    /**
    * @var PatientCardModule_mobilephone[]
    */
    var $mobiles;  
    
    
    /**
     * @var boolean
     */
    var $is_archived; 
    /**
     * @var string
     */
     var $subdivision_id;
    /**
     * @var string
     */
     var $subdivision_label;
     
     /**
      * @var int
      */
     var $NH_TREATTYPE;
}

class PatientCardModule_mobilephone
{
    /**
    * @var string
    */
    var $id; 
    /**
    * @var string
    */
    var $parent_fk; 
    /**
    * @var string
    */
    var $phone; 
    /**
    * @var int
    */
    var $isActive; 
    /**
    * @var string
    */
    var $comments; 
}

class PatientCardModuleDataGridPatientObject
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $CARDNUMBER_FULL;
    
    /**
    * @var number
    */
    var $CARDNUMBER;
    
    /**
    * @var string
    */
    var $CARDBEFORNUMBER;
    
    /**
    * @var string
    */
    var $CARDAFTERNUMBER;
    
    /**
    * @var string
    */
    var $CARDDATE;
    
    /**
    * @var string
    */
    var $FIRSTNAME;
     
    /**
    * @var string
    */
    var $MIDDLENAME;
     
    /**
    * @var string
    */
    var $LASTNAME;
    
    
    
    /**
    * @var string
    */
    var $BIRTHDAY;    
   
    /**
    * @var int
    */
    var $SEX;
    
    /**
    * @var string
    */
    var $TELEPHONENUMBER;
     
    /**
    * @var string
    */
    var $MOBILENUMBER;  
 
    /**
    * @var string
    */
    var $EMAIL;
		
	/**
    * @var string
    */
    var $ADDRESS;
    
    
    //ID страфовки пациента
	/**
    * @var int
    */
    var $INSURANCEID;
    
   	
    //флаг определяющий есть ли заболевания у пациента
    /**
    * @var int
    */
    var $TESTPATOLOGYFLAG;
    
    //количество осмотров
    /**
    * @var int
    */
    var $COUNTS_TOOTHEXAMS;
    //количество выполоненых процедур
    /**
    * @var int
    */
    var $COUNTS_PROCEDURES_CLOSED;
    //количество не выполоненых процедур
    /**
    * @var int
    */
    var $COUNTS_PROCEDURES_NOTCLOSED;
    //количество файлов
    /**
    * @var int
    */
    var $COUNTS_FILES;
    /**
    * @var number
    */
    var $discount_amount;
    /**
    * @var number
    */
    var $discount_amount_auto;  
    /**
    * @var boolean
    */
    var $INDDISCOUNT;
    
    /**
    * @var string
    */
    var $DISCOUNTCARDNUMBER;
    
	/**
    * @var string
    */
    var $full_addres;
    
    /**
    * @var string
    */
    var $full_fio;
    
    /**
    * @var int
    */
    var $is_returned;
    
    /**
    * @var int
    */
    var $patient_ages;
    
    
    /**
    * @var string
    */
    var $patient_notes;
    
    /**
     * @var string
     */
     var $doctors;
    /**
     * @var string
     */
     var $doctor_lead;
    /**
     * @var string
     */
     var $agreementTimestamp;
     
    /**
     * @var string
     */
     var $group_descript;
    /**
     * @var int
     */
     var $group_color;
     
     
    /**
    * @var PatientCardModule_mobilephone[]
    */
    var $mobiles;  
    
    /**
    * @var int
    */
    var $patientsCount;  
    
    /**
     * @var boolean
     */
    var $is_archived; 
    
    /**
     * @var string
     */
     var $subdivision_id;
    /**
     * @var string
     */
     var $subdivision_label;
     /**
      * @var int
      */
     var $NH_TREATTYPE;
}


class PatientCardModulePatientTestCard
{
	/**
    * @var string
    */
    var $ID;
    
    /**
    * @var int
    */
    var $PATIENTID;
    
    /**
    * @var date
    */
    var $TESTDATE;
    
    /**
    * @var int
    */
    var $CARDIACDISEASE;
    
    /**
    * @var int
    */
    var $KIDNEYDISEASE;
    
    /**
    * @var int
    */
    var $LIVERDISEASE;
    
    /**
    * @var int
    */
    var $OTHERDISEASE;
    
    /**
    * @var int
    */
    var $ARTERIALPRESSURE;
    
    /**
    * @var int
    */
    var $RHEUMATISM;
    
    /**
    * @var int
    */
    var $HEPATITIS;
   
    /**
    * @var string
    */
    var $HEPATITISDATE;
    
    /**
    * @var int
    */
    var $HEPATITISTYPE;
    
    /**
    * @var int
    */
    var $EPILEPSIA;
   
    /**
    * @var int
    */
    var $DIABETES;
    
    /**
    * @var int
    */
    var $FIT;
    
    /**
    * @var int
    */
    var $BLEEDING; 

    /**
    * @var int
    */    
    var $ALLERGYFLAG;   

    /**
    * @var string
    */    
    var $ALLERGY;
    
    /**
    * @var int
    */
    var $PREGNANCY;
    
    /**
    * @var string
    */
    var $MEDICATIONS;
    
    /**
    * @var int
    */
    var $BLOODGROUP;
    
    /**
    * @var int
    */
    var $RHESUSFACTOR;
    
    /**
    * @var int
    */
    var $MOUTHULCER;
    
    /**
    * @var int
    */
    var $FUNGUS;
    
    /**
    * @var int
    */
    var $FEVER;
    
    /**
    * @var int
    */
    var $THROATACHE;
    
    /**
    * @var int
    */
    var $LYMPHNODES;
    
    /**
    * @var int
    */
    var $REDSKINAREA;
    
    /**
    * @var int
    */
    var $HYPERHIDROSIS;
    
    /**
    * @var int
    */
    var $DIARRHEA;
    
    /**
    * @var int
    */
    var $AMNESIA;
    
    /**
    * @var int
    */
    var $WEIGHTLOSS;
    
    /**
    * @var int
    */
    var $HEADACHE;
    
    /**
    * @var int
    */
    var $AIDS;
    
    /**
    * @var int
    */
    var $WORKWITHBLOOD;
    
    /**
    * @var int
    */
    var $NARCOMANIA;   
}

class PatientCardModuleClinicRegistrationData
{
	/**
	* @var string
	*/
	var $NAME;
	
	/**
	* @var string
	*/
	var $ADDRESS;
	
	/**
	* @var string
	*/
	var $TELEPHONENUMBER;	
    /**
    * @var string
    */
    var $FIRSTNAME;
     
    /**
    * @var string
    */
    var $MIDDLENAME;
     
    /**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $BIRTHDAY;
    
    /**
    * @var string
    */
    var $PATIENTADDRESS;	
}

class PatientCardModulePatientTasksHistoryObject
{
    /**
    * @var string
    */
    var $TaskID;
    
    /**
    * @var string
    */
    var $TaskDATE;
     
    /**
    * @var string
    */
    var $TaskBEGINOFTHEINTERVAL;
    
    /**
    * @var string
    */
    var $TaskENDOFTHEINTERVAL;
    
    /**
    * @var string
    */
    var $TaskNOTICED;
     
    /**
    * @var string
    */
    var $TaskFACTOFVISIT;
    
    
    /**
    * @var string
    */
    var $DoctorSHORTNAME;
}

/////////////////////////////////////////////////PRIMARY PATIENTS OBJECTS START//////////////////////////////////////////////////////////
class PatientCardModulePrimaryDataGridPatientObject
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $PFname;
     
    /**
    * @var string
    */
    var $PLname;
     
    /**
    * @var string
    */
    var $PMname;
     
    /**
    * @var string
    */
    var $PPphone;
     
    /**
    * @var string
    */
    var $PCDate;
     
    /**
    * @var string
    */
    var $DoctorName;
}
/////////////////////////////////////////////////PRIMARY PATIENTS OBJECTS END//////////////////////////////////////////////////////////


/////////////////////////////////////////////////INSURANCE OBJECTS START//////////////////////////////////////////////////////////
class PatientCardModuleInsurancePolisObject
{
    /**
    * @var string
    */
    var $Id;
    
    /**
    * @var string
    */
    var $policyNumber;
     
    /**
    * @var string
    */
    var $toDate;
     
    /**
    * @var string
    */
    var $fromDate;
     
    /**
    * @var string
    */
    var $Comments;
    
    /**
    * @var PatientCardModuleInsuranceCompanyObject
    */
    var $company;
    
    /**
    * @var string
    */
    var $patientId;
}

class PatientCardModuleInsuranceCompanyObject
{
    /**
    * @var string
    */
    var $Id;
    
    /**
    * @var string
    */
    var $Name;
    
    /**
    * @var string
    */
    var $Phone;
    
    
    /**
    * @var string
    */
    var $Address;
}

class PatientCardModuleDoctorObject
{
   /**
   * @var string
   */
	var $fname;
   /**
   * @var string
   */
	var $lname;
   /**
   * @var string
   */
	var $pname;
   /**
   * @var string
   */
	var $shortname;
   /**
   * @var int
   */
	var $docid;  
   /**
   * @var boolean
   */
	var $isWork;  
}

class PatientCardModuleSubdivisonObject
{
   /**
   * @var string
   */
	var $id;
   /**
   * @var string
   */
	var $name;
}

class PatientCardModuleTreatmentBalanceData
{   
    /**
   * @var number
   */
	var $accountflow_sum; 
    /**
   * @var PatientCardModuleTreatmentBalanceObject[]
   */
	var $balance_objects;
}

class PatientCardModuleTreatmentBalanceObject
{
   /**
   * @var string
   */
	var $proc_id;
   /**
   * @var string
   */
	var $proc_nameforplan;
   /**
   * @var number
   */
	var $proc_price;
    /**
    * @var number
    */
    var $proc_count;
   /**
   * @var string
   */
	var $dateclose; 
   /**
   * @var string
   */
	var $doctor_fname; 
   /**
   * @var string
   */
	var $doctor_lname; 
   /**
   * @var string
   */
	var $doctor_pname; 
    
   /**
   * @var string
   */
	var $invoice_id; 
   /**
   * @var string
   */
	var $invoice_num;
   /**
   * @var number
   */
	var $invoice_sortField_num;
   /**
   * @var number
   */
	var $invoice_sum;
   /**
   * @var number
   */
	var $invoice_paidin; 
   /**
   * @var number
   */
	var $invoice_discount;
   /**
   * @var int
   */
	var $procedure_discountflag;
   /**
   * @var number
   */
	var $proc_discountprice;
   /**
   * @var int //1-procedure, 2-invoice
   */
	var $procedureFlag; 
    /**
     * @var string
     */
    var $proc_shifr;
   /**
   * @var int
   */
	var $invoice_isCashpayment;
    
    /**
   * @var string
   */
	var $accountflow_number;
    
}
class PatientCardModulePatientForLegalGuardian
{  
      /**
   * @var int
   */
	var $ID;
    
    /**
   * @var string
   */
	var $FULLNAME;
}
/////////////////////////////////////////////////INSURANCE OBJECTS END//////////////////////////////////////////////////////////


class PatientCardModule
{
    
    /**
     * PatientCardModule::registerTypes()
     * 
     * @param PatientCardModuleAddPatientObject $p1
     * @param PatientCardModuleClinicRegistrationData $p2
     * @param PatientCardModuleDataGridPatientObject $p3
     * @param PatientCardModulePatientTestCard $p4
     * @param PatientCardModulePrimaryDataGridPatientObject $p5
     * @param PatientCardModuleInsurancePolisObject $p6
     * @param PatientCardModuleInsuranceCompanyObject $p7
     * @param PatientCardModulePatientTasksHistoryObject $p8
     * @param PatientCardModuleDoctorObject $p9
     * @param PatientCardModuleTreatmentBalanceObject $p10
     * @param PatientCardModuleTreatmentBalanceData $p11
     * @param PatientCardModule_mobilephone $p12
     * @param PatientCardModulePatientForLegalGuardian $p13
     * @param PatientCardModuleSubdivisonObject $p14
     * 
     * @return void
     */
    public function registerTypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10, $p11, $p12, $p13, $p14){}
    
    
    
      
  
   /**
     * @param string $paramName  
     * @param string $paramValue  
     * @return boolean
     */
    public static function setApplicationSetting($paramName, $paramValue)
    {
        return setApplicationSetting($paramName, $paramValue);
    } 
    
        /**
     * @param string[] $paramNames  
     * @return string[]
     */
    public static function getApplicationSettings($paramNames)
	{
	   $res=array();
       foreach($paramNames as $paramName)
       {
            $res[]=getApplicationSetting($paramName);
       }
	   
	   return $res;
	} 
    
    
    /** 
    * @param string $id
 	* @return PatientCardModuleAddPatientObject
	*/ 
    public function getPatient($id)  
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select
                    patients.id,
                    patients.cardnumber,
                    patients.cardbefornumber,
                    patients.cardafternumber,
                    patients.carddate,
                    patients.firstname,
                    patients.middlename,
                    patients.lastname,
                    patients.fullname,
                    patients.shortname,
                    patients.birthday,
                    patients.sex,
                    patients.villageorcity,
                    patients.populationgroupe,
                    patients.address,
                    patients.postalcode,
                    patients.telephonenumber,
                    patients.mobilenumber,
                    patients.employmentplace,
                    patients.job,
                    patients.workphone,
                    patients.hobby,
                    patients.country,
                    patients.state,
                    patients.ragion,
                    patients.email,
                    patients.city,
                    patients.counts_toothexams,
                    patients.counts_procedures_closed,
                    patients.counts_procedures_notclosed,
                    patients.counts_files,
                    patients.discount_amount,
                    patients.is_returned,
                    patients.country,
                    patients.state,
                    patients.ragion,
                    patients.city,
                    patients.hobby,
                    patients.leaddoctor_fk,
                    patients.agreementtimestamp,
                    patients.WHEREFROMID,
                    patients.LEGALGUARDIANID,
                    patients.RELATIVEID,
                    (
                        select count(patientsinsurance.id)
                        from patientsinsurance
                        where patientsinsurance.patientid = patients.id
                        and  patientsinsurance.policetodate > 'now'
                    ), 
                    (
                        select count(patientsinsurance.id)
                        from patientsinsurance
                        where patientsinsurance.patientid = patients.id
                    ),
                    patients.PASSPORT_SERIES,
                    patients.PASSPORT_NUMBER,
                    patients.PASSPORT_ISSUING,
                    patients.PASSPORT_DATE,
                    patients.GROUPID,
                    patients.FATHERFIRSTNAME,
                    patients.MOTHERFIRSTNAME,
                    patients.FATHERMIDDLENAME,
                    patients.MOTHERMIDDLENAME,
                    patients.FATHERLASTNAME,
                    patients.MOTHERLASTNAME,
                    patients.FATHERBIRTHDATE,
                    patients.MOTHERBIRTHDATE,
                    coalesce((select first 1 ds.percent from discount_schema ds where ds.summ<=patients.ACCUMULATION order by ds.summ desc),0),
                    patients.INDDISCOUNT,
                    (select first 1 dc.number from discountcard dc where dc.enabled=1 and dc.barcode is not null and  dc.barcode!='' and dc.patientid=patients.id order by dc.id desc),
                    patients.is_archived,
                    (select subdivision.name from subdivision where subdivision.id = patients.subdivisionid),
                    patients.subdivisionid,
                    patients.NH_TREATTYPE
                    
                 from PATIENTS 
                 where patients.id='$id'  and patients.PRIMARYFLAG = 1";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);	
		$row = ibase_fetch_row($result);
        $obj = new PatientCardModuleAddPatientObject;
        $obj->ID = $row[0];
        $obj->CARDNUMBER = $row[1];
        $obj->CARDBEFORNUMBER = $row[2];
        $obj->CARDAFTERNUMBER = $row[3];
        $obj->CARDNUMBER_FULL = $row[2].strval($row[1]).$row[3];
        $obj->CARDDATE = $row[4];
        $obj->FIRSTNAME = $row[5];
        $obj->MIDDLENAME = $row[6];
        $obj->LASTNAME = $row[7];
        $obj->FULLNAME = $row[8];
        $obj->SHORTNAME = $row[9];
        $obj->BIRTHDAY = $row[10];
        $obj->SEX = $row[11];
        $obj->VILLAGEORCITY = $row[12];
        if($row[13]>9)
        {
            $obj->ISDISPANCER = 10; 
        }
        else
        { 
            $obj->ISDISPANCER = 0;
        }
        $obj->POPULATIONGROUPE = $row[13]-$obj->ISDISPANCER;
        $obj->ADDRESS = $row[14];
        $obj->POSTALCODE = $row[15];
        $obj->TELEPHONENUMBER = $row[16];
        $obj->MOBILENUMBER = $row[17];
        $obj->EMPLOYMENTPLACE = $row[18];
        $obj->JOB = $row[19];
        $obj->WORKPHONE = $row[20];
        $obj->HOBBY = $row[21];
        $obj->COUNTRY = $row[22];
        $obj->STATE = $row[23];
        $obj->RAGION = $row[24];
        $obj->EMAIL = $row[25];
        $obj->CITY = $row[26];
        $obj->COUNTS_TOOTHEXAMS = $row[27];
        $obj->COUNTS_PROCEDURES_CLOSED = $row[28];
        $obj->COUNTS_PROCEDURES_NOTCLOSED = $row[29];
        $obj->COUNTS_FILES = $row[30];
        $obj->discount_amount = $row[31];
        
            $obj->is_returned = $row[32];
            $obj->full_addres = '';
            if(($row[33] != '')&&($row[33] != null))
            {
              $obj->full_addres .= $row[33].', ';  
            }
            if(($row[34] != '')&&($row[34] != null))
            {
              $obj->full_addres .= $row[34].', ';  
            }
            if(($row[35] != '')&&($row[35] != null))
            {
              $obj->full_addres .= $row[35].', ';  
            }
            if(($row[36] != '')&&($row[36] != null))
            {
              $obj->full_addres .= $row[36].', ';  
            }
            if(($obj->ADDRESS != '')&&($obj->ADDRESS != null))
            {
              $obj->full_addres .= $obj->ADDRESS;  
            }
            $obj->patient_ages = $this->getAges($obj->BIRTHDAY);
            $obj->patient_notes = $row[37];
            $obj->full_fio = $obj->LASTNAME.' '.$obj->FIRSTNAME.' '.$obj->MIDDLENAME;
            $obj->doctor_lead = $row[38];
            $obj->agreementTimestamp = $row[39];
            $obj->WHEREFROMID=$row[40];
            $obj->LEGALGUARDIANID=$row[41];
            $obj->RELATIVEID=$row[42];
            if($row[43]==0&&$row[44]==0)
            {
           	    $obj->INSURANCECOUNT = 0;
            }
            else if($row[43]>0)
            {
                $obj->INSURANCECOUNT = $row[43];
            }
            else
            {
                $obj->INSURANCECOUNT = -$row[44];
            }
            $obj->PASSPORT_SERIES=$row[45];
            $obj->PASSPORT_NUMBER=$row[46];
            $obj->PASSPORT_ISSUING=$row[47];
            $obj->PASSPORT_DATE=$row[48];
            $obj->GROUPID=$row[49];
            $obj->FATHERFIRSTNAME=$row[50];
            $obj->MOTHERFIRSTNAME=$row[51];
            $obj->FATHERMIDDLENAME=$row[52];
            $obj->MOTHERMIDDLENAME=$row[53];
            $obj->FATHERLASTNAME=$row[54];
            $obj->MOTHERLASTNAME=$row[55];
            $obj->FATHERBIRTHDATE=$row[56];
            $obj->MOTHERBIRTHDATE=$row[57];
            $obj->discount_amount_auto=$row[58];
            $obj->INDDISCOUNT=$row[59];
            $obj->DISCOUNTCARDNUMBER=$row[60];
            if($row[61] == 1)
            {
                $obj->is_archived = true;
            }
            else
            {
                $obj->is_archived = false;
            }
            $obj->subdivision_label=$row[62];
            $obj->subdivision_id=$row[63];
            $obj->NH_TREATTYPE=$row[64];
            
            $obj->mobiles = $this->getMobilePhones($id, true, $trans);
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $obj; 
    }
    /** 
 	* @param string $patient_id
 	* @param boolean $isAddObject
    * @return PatientCardModule_mobilephone[]
	*/ 
    private function getMobilePhones($patient_id, $isAddObject, $trans)
    {
         $mobile_sql =
			"select patientsmobile.id,
                patientsmobile.parent_fk,
                patientsmobile.mobilenumber,
                patientsmobile.senderactive,
                patientsmobile.comments
            from patientsmobile
                where patientsmobile.patient_fk = $patient_id
            order by patientsmobile.senderactive desc, patientsmobile.id desc";
        			
        $mobile_query = ibase_prepare($trans, $mobile_sql);
        $mobile_result=ibase_execute($mobile_query);
        $rows = array();
		while ($row = ibase_fetch_row($mobile_result))
		{ 
            $obj = new PatientCardModule_mobilephone;
            $obj->id = $row[0];
            $obj->parent_fk = $row[1];
            $obj->phone = $row[2];
            $obj->isActive = $row[3];
            $obj->comments = $row[4];
            $rows[] = $obj;
		}	
        
        if($isAddObject == true)
        {
            $empty_obj = new PatientCardModule_mobilephone;
            $empty_obj->id = 'empty';
            $empty_obj->phone = '';
            $rows[] = $empty_obj;
            $add_obj = new PatientCardModule_mobilephone;
            $add_obj->id = 'add';
            $add_obj->phone = '';
            $rows[] = $add_obj;
        }
        ibase_free_query($mobile_query);
        ibase_free_result($mobile_result);
        return $rows;
    }
    
    /** 
 	* @param string $patient_id
 	* @param boolean $isAddObject
    * @return PatientCardModule_mobilephone[]
	*/ 
    public function getMobilePhoneByPatientID($patient_id, $isAddObject)
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        $rows = $this->getMobilePhones($patient_id, $isAddObject, $trans);
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $rows;
    }
    
    /** 
 	* @param string $patient_id
 	* @param PatientCardModule_mobilephone[] $phones
    * @return boolean
	*/ 
    private function saveMobilePhones($patient_id, $phones, $trans)
    {
        $delete_sql =  "delete from patientsmobile where patientsmobile.patient_fk = $patient_id";
        if($phones)
        { 
            foreach ($phones as $phone)
            {
                if(($phone->id != null)&&($phone->id != '')&&($phone->id != 'add')&&($phone->id != 'empty'))
                {
                    $delete_sql .= " and patientsmobile.id <> ".$phone->id;
                    
                    $update_sql = "update patientsmobile set ";
                    if($phone->parent_fk != null)
                    {
                        $update_sql .= "patientsmobile.parent_fk = $phone->parent_fk, ";
                    }
                    $update_sql .= "patientsmobile.mobilenumber = '$phone->phone',
                                    patientsmobile.senderactive = '$phone->isActive', 
                                    patientsmobile.comments = '".safequery($phone->comments)."'
                                 where patientsmobile.id = $phone->id";
                    $update_query = ibase_prepare($trans, $update_sql);
                    ibase_execute($update_query);
                    ibase_free_query($update_query);
                }
                else if(($phone->id != 'add')&&($phone->phone != ''))
                {
                    $insert_sql = "insert into patientsmobile(patient_fk, parent_fk, mobilenumber, senderactive, comments) values ($patient_id, ";
                    if($phone->parent_fk != null)
                    {
                        $insert_sql .= "$phone->parent_fk, ";
                    }
                    else
                    {
                        $insert_sql .= "null, ";
                    }
                    $insert_sql .= "'$phone->phone',
                                    '$phone->isActive', 
                                    '".safequery($phone->comments)."')
                                     returning id";
                    $insert_query = ibase_prepare($trans, $insert_sql);
                    $insert_result = ibase_execute($insert_query);
                    $new_id = ibase_fetch_row($insert_result);
                    $delete_sql .= " and patientsmobile.id <> ".$new_id[0];
                    ibase_free_query($insert_query);
                    ibase_free_result($insert_result);
                }
            }
        }
        $delete_query = ibase_prepare($trans, $delete_sql);
        
        ibase_execute($delete_query);
        ibase_free_query($delete_query);
        
        return true;
    }

    
    //*****************************************************LAZY LOADING START*****************************************
    /** 
 	* @param string $searchLabel 
 	* @param string $searchText
 	* @param boolean $includePhoneSearch
    * @param int $pageSize
    * @param int $pageIndex
    * @param string $patientID
 	* @param boolean $includeArchived
    * @param int $nhTreatType
    * @return PatientCardModuleDataGridPatientObject[]
	*/    
    public function getPatientsLazyFiltered($searchLabel, $searchText, $includePhoneSearch, $pageSize, $pageIndex, $patientID, $includeArchived, $nhTreatType) 
    { 
          $pageSkip=$pageSize*$pageIndex;
          
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        /*
         $QueryCount='select count(*) from patients 
                        where patients.primaryflag = 1 ';
                        
	    $QueryText ="select 				
                    patients.ID,
                    patients.CARDNUMBER, 
                    patients.CARDBEFORNUMBER, 
                    patients.CARDAFTERNUMBER, 
                    patients.CARDDATE,
                    patients.FIRSTNAME, 
                    patients.MIDDLENAME, 
                    patients.LASTNAME, 
                    patients.BIRTHDAY,
					
					
					
					
                    ( select count(patientsinsurance.id) from patientsinsurance where patientsinsurance.patientid = patients.id and  patientsinsurance.policetodate > 'now' ), 
                    ( select count(patientsinsurance.id) from patientsinsurance where patientsinsurance.patientid = patients.id ), 
                    patients.testpatologyflag,
                    patients.counts_toothexams,
					
					
					
					
                    patients.is_returned,
                    
					
					
					
					patients.hobby,
					(select doctors.shortname from doctors where doctors.id = patients.leaddoctor_fk),
                    patients.agreementtimestamp,
                    (select patientcarddictionaries.description from patientcarddictionaries where patients.groupid = patientcarddictionaries.id),
                    (select patientcarddictionaries.color from patientcarddictionaries where patients.groupid = patientcarddictionaries.id),
                    patients.TELEPHONENUMBER,
                    
                    CAST( (select LIST(distinct(doctors.shortname)) from doctors inner join toothexam on toothexam.doctorid = doctors.id where toothexam.id = patients.id) as VARCHAR(1000) ),
                    patients.is_archived


                 from PATIENTS";
        */
        $QueryText ='select * from ';// patients_grid as p where p.id is not null ';
        $QueryCount='select count(*) from ';// patients_grid as p where p.id is not null ';
        if($includeArchived)
        {
            $QueryText .= ' patients_grid_all ';
            $QueryCount .= ' patients_grid_all ';
        }
        else
        {
            $QueryText .= ' patients_grid ';
            $QueryCount .= ' patients_grid ';
        }
        $QueryText .= ' as p ';
        
        //$QueryCount .= ' as p where p.id is not null ';
        if($nhTreatType == -1)
        {
            $QueryCount .= "  as p where p.id is not null ";
        }
        else if($nhTreatType == 0)
        {
            $QueryCount .= "  as p where (p.nh_treattype = 0 or p.nh_treattype is null) ";
        }
        else
        {
            $QueryCount .= "  as p where p.nh_treattype = $nhTreatType "; 
        }
                        
	    
        if($includePhoneSearch == true && is_numeric($searchText))
        {
            $QueryText .= " inner join patientsmobile
                     on p.id = patientsmobile.patient_fk ";
        }
        
        if($nhTreatType == -1)
        {
            $QueryText .= " where p.id is not null ";
        }
        else if($nhTreatType == 0)
        {
            $QueryText .= " where (p.nh_treattype = 0 or p.nh_treattype is null) ";
        }
        else
        {
            $QueryText .= " where p.nh_treattype = $nhTreatType "; 
        }
        
                      
        if($patientID != null)
        {
            $QueryText .= " and p.ID = $patientID ";
            $QueryCount .= "  and p.ID = $patientID";
        }
        else if($searchText != "*")
        {                   
            if($searchLabel == 'FIOCARDNUMBER')
            {
                $QueryText .= " and (lower( p.LASTNAME||' '||p.FIRSTNAME||' '||p.MIDDLENAME) starting lower('".safequery($searchText)."')
                        or lower(p.CARDBEFORNUMBER||p.CARDNUMBER||p.CARDAFTERNUMBER) containing lower('".safequery($searchText)."')";
                  
                  $QueryCount .= " and (lower( p.LASTNAME||' '||p.FIRSTNAME||' '||p.MIDDLENAME) starting lower('".safequery($searchText)."')
                        or lower(p.CARDBEFORNUMBER||p.CARDNUMBER||p.CARDAFTERNUMBER) containing lower('".safequery($searchText)."')";        
                if($includePhoneSearch == true && is_numeric($searchText))
                {
                    $QueryText .= " or lower(patientsmobile.mobilenumber) containing lower('".safequery($searchText)."'))";
                    $QueryCount .= " or lower(patientsmobile.mobilenumber) containing lower('".safequery($searchText)."'))";
                }
				else
				{
                    $QueryText .= ")";
					$QueryCount .=  ")";
				}
            }    
            else if($searchLabel == 'FIO')
            {
                $QueryText.=" and lower ( p.LASTNAME||' '||p.FIRSTNAME||' '||p.MIDDLENAME) starting lower('".safequery($searchText)."')";
                $QueryCount.=" and lower ( p.LASTNAME||' '||p.FIRSTNAME||' '||p.MIDDLENAME) starting lower('".safequery($searchText)."')";
    
            }   
            else if($searchLabel == 'CARDNUMBER_FULL')  
            {
                $QueryText.=" and lower( p.CARDBEFORNUMBER||p.CARDNUMBER||p.CARDAFTERNUMBER) containing lower('".safequery($searchText)."')"; 
                $QueryCount.=" and lower( p.CARDBEFORNUMBER||p.CARDNUMBER||p.CARDAFTERNUMBER) containing lower('".safequery($searchText)."')"; 
    
            }   
            else
            {
                $QueryText.=" and lower(p.".$searchLabel.") like lower('%".safequery($searchText)."%')";
                $QueryCount.=" and lower(p.".$searchLabel.") like lower('%".safequery($searchText)."%')";
            } 
                 //where lower(".$searchLabel.") starting lower('$searchText')";
                 
            //$QueryText .= " order by patients_grid.LASTNAME";
        }
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);   
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new PatientCardModuleDataGridPatientObject;
            $obj->ID = $row[0];
            $obj->CARDNUMBER = $row[1];
            $obj->CARDBEFORNUMBER = $row[2];
            $obj->CARDAFTERNUMBER = $row[3];
            $obj->CARDNUMBER_FULL = $row[2].strval($row[1]).$row[3];
            $obj->CARDDATE = $row[4];
            $obj->FIRSTNAME = $row[5];
            $obj->MIDDLENAME = $row[6];
            $obj->LASTNAME = $row[7];
            $obj->full_fio = $obj->LASTNAME.' '.$obj->FIRSTNAME.' '.$obj->MIDDLENAME;
            $obj->BIRTHDAY = $row[8];
            //$obj->SEX = $row[9];
            $obj->TELEPHONENUMBER = $row[19];
            //$obj->EMAIL = $row[11];
			//$obj->ADDRESS = $row[12];
            if($row[9]==0&&$row[10]==0)
            {
           	    $obj->INSURANCEID = 0;
            }
            else if($row[9]>0)
            {
                $obj->INSURANCEID = $row[9];
            }
            else
            {
                $obj->INSURANCEID = -$row[10];
            }
            $obj->TESTPATOLOGYFLAG = $row[11];
            $obj->COUNTS_TOOTHEXAMS = $row[12];
            //$obj->COUNTS_PROCEDURES_CLOSED = $row[18];
            //$obj->COUNTS_PROCEDURES_NOTCLOSED = $row[19];
            //$obj->COUNTS_FILES = $row[19];
            //$obj->discount_amount = $row[20];
            $obj->is_returned = $row[13];
            
            $obj->full_addres = '';
            /*if(($row[22] != '')&&($row[22] != null))
            {
              $obj->full_addres .= $row[22].', ';  
            }
            if(($row[23] != '')&&($row[23] != null))
            {
              $obj->full_addres .= $row[23].', ';  
            }
            if(($row[24] != '')&&($row[24] != null))
            {
              $obj->full_addres .= $row[24].', ';  
            }
            if(($row[25] != '')&&($row[25] != null))
            {
              $obj->full_addres .= $row[25].', ';  
            }
            if(($obj->ADDRESS != '')&&($obj->ADDRESS != null))
            {
              $obj->full_addres .= $obj->ADDRESS;  
            }*/
            $obj->patient_ages = $this->getAges($obj->BIRTHDAY);
            $obj->patient_notes = $row[14];
            $obj->group_descript = $row[17];
            $obj->group_color = $row[18]; 
            //$obj->discount_amount_auto = $row[31];
            //$obj->INDDISCOUNT = $row[32];
            //$obj->DISCOUNTCARDNUMBER=$row[33];
                //$nowTimestamp = new DateTime();
                /*
                $QueryDoctorText =
    			"select distinct(doctors.id), doctors.shortname
                from doctors
                
                left join toothexam
                on toothexam.doctorid = doctors.id
                
                where toothexam.id = '$obj->ID'";
                
                $queryDoctor = ibase_prepare($trans, $QueryDoctorText);
                $resultDoctor=ibase_execute($queryDoctor);
                $doctorsString = '';
        		while($rowDoctor = ibase_fetch_row($resultDoctor))
        		{ 
        		  $doctorsString .= $rowDoctor[1].' ';
        		}	
        		ibase_free_query($queryDoctor);
        		ibase_free_result($resultDoctor);*/
            $obj->doctors = $row[20];
            $obj->doctor_lead = $row[15];
            $obj->agreementTimestamp = $row[16];
            if($row[21] == 1)
            { 
                $obj->is_archived = true;
            }
            else
            {
                $obj->is_archived = false;
            }
                
            $obj->subdivision_label = $row[22];
            $obj->subdivision_id = $row[23];
            $obj->NH_TREATTYPE = $row[24];
            
            //$obj->mobiles = $this->getMobilePhones($obj->ID, false, $trans);
            
            $rows[] = $obj;
		}
        ibase_free_query($query);
        ibase_free_result($result);
        if(count($rows)>0 && (!$includePhoneSearch || !is_numeric($searchText)) )
        {	
            $query = ibase_prepare($trans, $QueryCount);
            $result=ibase_execute($query);
    		if ($row = ibase_fetch_row($result))
            {
                $rows[0]->patientsCount=$row[0];
            }
            ibase_free_query($query);
            ibase_free_result($result);
        }
        else if(count($rows)>0)
        {
            $rows[0]->patientsCount = null;
        }	
        ibase_commit($trans);
        
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    //*****************************************************LAZY LOADING END*****************************************
    
    /**
     * @param string $patient_id
     * @param int $is_returned
     * @return boolean
     */
     public function updateReturningCardByPatientID($patient_id, $is_returned)
     {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update patients set patients.is_returned = $is_returned
                      where patients.id = '$patient_id'";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
        
     }
     
      /**
     * @param string $patient_id
     * @param boolean $is_archived
     * @return boolean
     */
     public function updateArchivedByPatientID($patient_id, $is_archived)
     {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$QueryText = "update patients set patients.is_archived = ".($is_archived ? "1" : "0")." where patients.id = '$patient_id'";
        
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
        
     }
    
     /**
     * @param string $patientID 
     * @param boolean $isCoursename
     * @return PatientCardModuleTreatmentBalanceData
     */
    public function getPatientsTreatmentBalanceHistoryByID($patientID, $isCoursename) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        //result object
        $resultObject = new PatientCardModuleTreatmentBalanceData;
        
        //accountflowquery
        $QueryText = "select coalesce(sum(accountflow.summ),0) from accountflow where accountflow.patientid = $patientID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);	
		$accountflow_sum_result=ibase_fetch_row($result);//$cardNumber[0]
        $resultObject->accountflow_sum = $accountflow_sum_result[0];
        ibase_free_query($query);
        ibase_free_result($result);
        
        //procedure query
	    $ProcedureQueryText =
			"select 
                    treatmentproc.id, 
                    treatmentproc.nameforplan, 
                    treatmentproc.price, 
                    treatmentproc.dateclose,
                    doctors.firstname, 
                    doctors.lastname, 
                    doctors.middlename,
                    invoices.invoicenumber, 
                    invoices.id,
                    treatmentproc.proc_count,
                    invoices.discount_amount,
                    treatmentproc.discount_flag,
                    treatmentproc.shifr, 
                    treatmentproc.name
                from treatmentproc
                    left join doctors
                    on doctors.id = treatmentproc.doctor
                    left join invoices
                    on invoices.id = treatmentproc.invoiceid
                where
                    treatmentproc.patient_fk = $patientID
                    and treatmentproc.invoiceid is not null
                order by treatmentproc.dateclose asc";
        $ProcedureQuery = ibase_prepare($trans, $ProcedureQueryText);
        $ProcedureResult=ibase_execute($ProcedureQuery);
		while ($row = ibase_fetch_row($ProcedureResult))
		{ 
            $obj = new PatientCardModuleTreatmentBalanceObject;
            $obj->proc_id = $row[0];
            if($isCoursename == true)
            {
                $obj->proc_nameforplan = $row[13];
            }
            else
            {
                $obj->proc_nameforplan = $row[1];
            }
            $obj->proc_price = $row[2];
            $obj->dateclose = $row[3];
            $obj->doctor_fname = $row[4];
            $obj->doctor_lname = $row[5];
            $obj->doctor_pname = $row[6];
            $obj->invoice_num = $row[7];
            $obj->invoice_sortField_num = $row[7] - 0.5;
            $obj->invoice_id = $row[8];
            $obj->proc_count = $row[9];
            $obj->procedureFlag = 1;//procedure flag
            $obj->invoice_discount = $row[10];
            $obj->procedure_discountflag = $row[11];
            $obj->proc_discountprice = $obj->proc_price*$obj->proc_count - round($obj->proc_price*$obj->proc_count*$obj->invoice_discount*$obj->procedure_discountflag/100,2);
            $obj->proc_shifr = $row[12];
            $resultObject->balance_objects[] = $obj;
		}
        ibase_free_query($ProcedureQuery);
        ibase_free_result($ProcedureResult);
        
        //invoice query
	    $InvoiceQueryText =
            "select
                    invoices.ID,
                    invoices.summ,
                    invoices.PAIDINCASH,
                    invoices.CREATEDATE,
                    doctors.firstname, 
                    doctors.lastname, 
                    doctors.middlename,
                    invoices.INVOICENUMBER,
                    invoices.discount_amount,
                    invoices.is_cashpayment
                from invoices
                    left join doctors
                    on doctors.id = invoices.DOCTORID
                where  
                    invoices.PATIENTID = $patientID
                order by invoices.createdate desc";
        $InvoiceQuery = ibase_prepare($trans, $InvoiceQueryText);
        $InvoiceResult=ibase_execute($InvoiceQuery);
		while ($row = ibase_fetch_row($InvoiceResult))
		{ 
            $obj = new PatientCardModuleTreatmentBalanceObject;
            $obj->invoice_id = $row[0];
            $obj->invoice_sum = $row[1];
            $obj->invoice_paidin = 0;
            $obj->dateclose = $row[3];//.' 00:00:00';
            $obj->doctor_fname = $row[4];
            $obj->doctor_lname = $row[5];
            $obj->doctor_pname = $row[6];
            $obj->invoice_num = $row[7];
            $obj->invoice_sortField_num = $row[7];
            $obj->procedureFlag = 2;//invoice flag
            $obj->invoice_discount = $row[8];
            $obj->invoice_isCashpayment = $row[9];
            $resultObject->balance_objects[] = $obj;
		}
        ibase_free_query($InvoiceQuery);
        ibase_free_result($InvoiceResult);
        
        //accontflows query
        $FlowsQueryText =
            "select
                        accountflow.id,
                        0,
                        accountflow.summ,
                        accountflow.createdate,
                        account.firstname,
                        account.lastname,
                        account.middlename,
                        accountflow.ordernumber,
                        0,
                        accountflow.is_cashpayment,
                        (select invoices.invoicenumber from invoices where invoices.id = accountflow.oninvoice_fk)

                from ACCOUNTFLOW
                left join account on account.id = accountflow.authorid
                where PATIENTID = $patientID
                    and OPERATIONTYPE = 0
                order by CREATEDATE desc";
        $FlowsQuery = ibase_prepare($trans, $FlowsQueryText);
        $FlowsResult=ibase_execute($FlowsQuery);
		while ($row = ibase_fetch_row($FlowsResult))
		{ 
            $obj = new PatientCardModuleTreatmentBalanceObject;
            $obj->invoice_id = $row[0];
            $obj->invoice_sum = $row[1];
            $obj->invoice_paidin = $row[2];
            $obj->dateclose = $row[3];//.' 00:00:00';
            $obj->doctor_fname = $row[4];
            $obj->doctor_lname = $row[5];
            $obj->doctor_pname = $row[6];
            if($row[10] != null && $row[10] != '')
            {
                $obj->invoice_sortField_num = $row[10] - 0.3;
                $obj->invoice_num = $row[10];
            }
            else
            {
                $obj->invoice_sortField_num = $row[7];
            }
            $obj->accountflow_number = $row[7];
            $obj->procedureFlag = 3;//accountflow flag
            $obj->invoice_discount = $row[8];
            $obj->invoice_isCashpayment = $row[9];
            $resultObject->balance_objects[] = $obj;
		}
        ibase_free_query($FlowsQuery);
        ibase_free_result($FlowsResult);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $resultObject;
        
    }
    
    
    /**
     * @param string $patientID 
     * @return PatientCardModuleDoctorObject[]
     */
    public function getPatientsDoctorHistoryByID($patientID) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    
        $QueryText =
			"select distinct(doctors.id), doctors.firstname, doctors.middlename, doctors.lastname
            from doctors
            left join treatmentproc
            on treatmentproc.doctor = doctors.id
            left join diagnos2
            on diagnos2.id = treatmentproc.diagnos2
            left join treatmentcourse
            on treatmentcourse.id = diagnos2.treatmentcourse
            left join toothexam
            on toothexam.treatmentcourse_fk = treatmentcourse.id
            where toothexam.id = '$patientID'";
            
            
        /*$QueryText =
			"select distinct(doctors.id), doctors.shortname
                from doctors
                
                left join toothexam
                on toothexam.doctorid = doctors.id
                
                where toothexam.id = '$patientID'";*/
            
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new PatientCardModuleDoctorObject;
            $obj->docid = $row[0];
            $obj->fname = $row[1];
            $obj->pname = $row[2];
            $obj->lname = $row[3];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
        
    }
    
    /** 
 	* @param string $patientID 
    * @return PatientCardModulePatientTasksHistoryObject[]
	*/    
    public function getPatientsTaskHistoryByID($patientID) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select tasks.id, tasks.taskdate, tasks.noticed, tasks.factofvisit, doctors.shortname, tasks.beginoftheinterval, tasks.endoftheinterval
                    from tasks
                    left join doctors
                    on tasks.doctorid = doctors.id
                    where tasks.patientid = '$patientID'
                    order by tasks.taskdate desc";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new PatientCardModulePatientTasksHistoryObject;
            $obj->TaskID = $row[0];
            $obj->TaskDATE = $row[1];
            $obj->TaskNOTICED = $row[2];
            $obj->TaskFACTOFVISIT = $row[3];
            $obj->DoctorSHORTNAME = $row[4];
            $obj->TaskBEGINOFTHEINTERVAL = $row[5];
            $obj->TaskENDOFTHEINTERVAL = $row[6];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    
    
    
    
    /**
 	* @param PatientCardModuleAddPatientObject $newPatient
    * @return int
 	*/
    public function addPatient($newPatient) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $position = null;
        if($newPatient->ADDRESS != null && $newPatient->CITY != null && $newPatient->ADDRESS != '' && $newPatient->CITY != '')
        {
            $position = getLocation($newPatient->ADDRESS, $newPatient->CITY);
        }
        
        
        
            $QueryText = "INSERT into PATIENTS (
                                                id, 
                                                cardnumber, 
                                                carddate, 
                                                firstname, 
                                                middlename, 
                                                lastname, 
                                                fullname, 
                                                shortname,
                                                birthday,
                                                sex, 
                                                villageorcity, 
                                                populationgroupe, 
                                                address, 
                                                postalcode, 
                                                telephonenumber, 
                                                mobilenumber, 
                                                employmentplace, 
                                                job, 
                                                workphone, 
                                                hobby, 
                                                country, 
                                                state, 
                                                ragion, 
                                                email, 
                                                city, 
                                                wherefromid,
                                                groupid,
                                                passport_series,
                                                passport_number,
                                                passport_issuing,
                                                passport_date,
                                                legalguardianid,
                                                relativeid,
                                                cardbefornumber, 
                                                cardafternumber, 
                                                primaryflag,
                                                FATHERFIRSTNAME,
                                                MOTHERFIRSTNAME,
                                                FATHERMIDDLENAME,
                                                MOTHERMIDDLENAME,
                                                FATHERLASTNAME,
                                                MOTHERLASTNAME,
                                                FATHERBIRTHDATE,
                                                MOTHERBIRTHDATE,
                                                leaddoctor_fk,
                                                LONGTITUDE,
                                                LATITUDE)
            values (null, 
            ".$newPatient->CARDNUMBER.", 
            '".$newPatient->CARDDATE."', 
            '".safequery($newPatient->FIRSTNAME)."', 
            '".safequery($newPatient->MIDDLENAME)."', 
            '".safequery($newPatient->LASTNAME)."', 
            '".safequery($newPatient->LASTNAME)." ".safequery($newPatient->FIRSTNAME)." ".safequery($newPatient->MIDDLENAME)."',
            '".safequery($newPatient->LASTNAME)." ".safequery(mb_substr($newPatient->FIRSTNAME,0,1)).".".safequery(mb_substr($newPatient->MIDDLENAME,0,1)).".',";
          //  '".safequery($newPatient->FULLNAME)."', 
          //  '".safequery($newPatient->SHORTNAME)."',";
        if(($newPatient->BIRTHDAY != '')&&($newPatient->BIRTHDAY != 'null'))
        {
            $QueryText .= "'$newPatient->BIRTHDAY', ";
        }
        else
        {
            $QueryText .= "null, ";
        }
        $QueryText .= $newPatient->SEX.", 
            ".$newPatient->VILLAGEORCITY.", 
            ".($newPatient->POPULATIONGROUPE+$newPatient->ISDISPANCER).", 
            '".safequery($newPatient->ADDRESS)."', 
            '".safequery($newPatient->POSTALCODE)."', 
            '".safequery($newPatient->TELEPHONENUMBER)."', 
            '".safequery($newPatient->MOBILENUMBER)."', 
            '".safequery($newPatient->EMPLOYMENTPLACE)."', 
            '".safequery($newPatient->JOB)."', 
            '".safequery($newPatient->WORKPHONE)."', 
            '".safequery($newPatient->HOBBY)."', 
            '".safequery($newPatient->COUNTRY)."', 
            '".safequery($newPatient->STATE)."', 
            '".safequery($newPatient->RAGION)."', 
            '".safequery($newPatient->EMAIL)."', 
            '".safequery($newPatient->CITY)."', 
             ".nullcheck($newPatient->WHEREFROMID).",
             ".nullcheck($newPatient->GROUPID).",
             '".safequery($newPatient->PASSPORT_SERIES)."', 
             '".safequery($newPatient->PASSPORT_NUMBER)."', 
             '".safequery($newPatient->PASSPORT_ISSUING)."', 
             ".(nonull($newPatient->PASSPORT_DATE)?"'$newPatient->PASSPORT_DATE'":"null").",  
             ".nullcheck($newPatient->LEGALGUARDIANID).", 
             ".nullcheck($newPatient->RELATIVEID).",
             '".safequery($newPatient->CARDBEFORNUMBER)."', 
            '".safequery($newPatient->CARDAFTERNUMBER)."', 1,  
            '".safequery($newPatient->FATHERFIRSTNAME)."', 
            '".safequery($newPatient->MOTHERFIRSTNAME)."', 
            '".safequery($newPatient->FATHERMIDDLENAME)."',
            '".safequery($newPatient->MOTHERMIDDLENAME)."', 
            '".safequery($newPatient->FATHERLASTNAME)."', 
            '".safequery($newPatient->MOTHERLASTNAME)."',
             ".(nonull($newPatient->FATHERBIRTHDATE)?"'$newPatient->FATHERBIRTHDATE'":"null").",   
             ".(nonull($newPatient->MOTHERBIRTHDATE)?"'$newPatient->MOTHERBIRTHDATE'":"null").", "  ; 
        if(($newPatient->doctor_lead != null)&&($newPatient->doctor_lead != 'null')&&($newPatient->doctor_lead != ''))
        {
           $QueryText .= $newPatient->doctor_lead.", "; 
        }
        else
        {
           $QueryText .= "null, "; 
        }
        if($position != null)
        {
           $QueryText .= $position[0].", ".$position[1]; 
        }
        else
        {
           $QueryText .= "null, null "; 
        }
        
        
        $QueryText .= ") RETURNING id";

		$query = ibase_prepare($trans, $QueryText);
		//ibase_execute($query);
		//ibase_commit($trans);
		//ibase_free_query($query);
		//$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		//$QueryText = 'select max(ID) from PATIENTS where  PRIMARYFLAG = 1';
		//$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$row = ibase_fetch_row($result);
        
        //$this->saveMobilePhones($row[0], $newPatient->mobiles, $trans);
        
        ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $row[0]; 	
    }      
            
    /**
 	* @param PatientCardModuleAddPatientObject $newPatient
    * @return boolean
 	*/
    public function updatePatient($newPatient) 
     { 
        $position = null;
        if($newPatient->ADDRESS != null && $newPatient->CITY != null && $newPatient->ADDRESS != '' && $newPatient->CITY != '')
        {
            $position = getLocation($newPatient->ADDRESS, $newPatient->CITY);
        }
        
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$QueryText = "update PATIENTS set 		  
    		  FIRSTNAME='".safequery($newPatient->FIRSTNAME)."',
    		  MIDDLENAME='".safequery($newPatient->MIDDLENAME)."',
    		  LASTNAME='".safequery($newPatient->LASTNAME)."',
    		  FULLNAME='".safequery($newPatient->LASTNAME)." ".safequery($newPatient->FIRSTNAME)." ".safequery($newPatient->MIDDLENAME)."',
    		  SHORTNAME='".safequery($newPatient->LASTNAME)." ".safequery(mb_substr($newPatient->FIRSTNAME,0,1)).".".safequery(mb_substr($newPatient->MIDDLENAME,0,1)).".',";
        if(($newPatient->BIRTHDAY != '')&&($newPatient->BIRTHDAY != 'null'))
        {
            $QueryText .= "BIRTHDAY='$newPatient->BIRTHDAY',";
        }
        
        if(($newPatient->CARDDATE != '')&&($newPatient->CARDDATE != 'null'))
        {
            $QueryText .= "CARDDATE='$newPatient->CARDDATE',";
        }
        
        
        $QueryText .=  "SEX=$newPatient->SEX,
    		  VILLAGEORCITY=$newPatient->VILLAGEORCITY,
    		  POPULATIONGROUPE=".($newPatient->POPULATIONGROUPE+$newPatient->ISDISPANCER).",
              ADDRESS='".safequery($newPatient->ADDRESS)."',
              POSTALCODE='".safequery($newPatient->POSTALCODE)."',
              TELEPHONENUMBER='".safequery($newPatient->TELEPHONENUMBER)."', 
              MOBILENUMBER='".safequery($newPatient->MOBILENUMBER)."',
              EMPLOYMENTPLACE='".safequery($newPatient->EMPLOYMENTPLACE)."',
              JOB='".safequery($newPatient->JOB)."',
              WORKPHONE='".safequery($newPatient->WORKPHONE)."',
              HOBBY='".safequery($newPatient->HOBBY)."',
              COUNTRY='".safequery($newPatient->COUNTRY)."',
              STATE='".safequery($newPatient->STATE)."',
              RAGION='".safequery($newPatient->RAGION)."',
              EMAIL='".safequery($newPatient->EMAIL)."',
              CITY='".safequery($newPatient->CITY)."',
              CARDNUMBER='$newPatient->CARDNUMBER',
              CARDAFTERNUMBER='".safequery($newPatient->CARDAFTERNUMBER)."',
              CARDBEFORNUMBER='".safequery($newPatient->CARDBEFORNUMBER)."',
              discount_amount = '$newPatient->discount_amount',
              INDDISCOUNT = ".($newPatient->INDDISCOUNT?'1':'0').", 
              wherefromid = ".nullcheck($newPatient->WHEREFROMID).",
              groupid = ".nullcheck($newPatient->GROUPID).",
              PASSPORT_SERIES='".safequery($newPatient->PASSPORT_SERIES)."',
              PASSPORT_NUMBER='".safequery($newPatient->PASSPORT_NUMBER)."',
              PASSPORT_ISSUING='".safequery($newPatient->PASSPORT_ISSUING)."',
              PASSPORT_DATE=".(nonull($newPatient->PASSPORT_DATE)?"'$newPatient->PASSPORT_DATE'":"null").", 
              LEGALGUARDIANID = ".nullcheck($newPatient->LEGALGUARDIANID).",
              RELATIVEID = ".nullcheck($newPatient->RELATIVEID).",
              FATHERFIRSTNAME='".safequery($newPatient->FATHERFIRSTNAME)."',
              MOTHERFIRSTNAME='".safequery($newPatient->MOTHERFIRSTNAME)."',
              FATHERMIDDLENAME='".safequery($newPatient->FATHERMIDDLENAME)."',
              MOTHERMIDDLENAME='".safequery($newPatient->MOTHERMIDDLENAME)."',
              FATHERLASTNAME='".safequery($newPatient->FATHERLASTNAME)."',
              MOTHERLASTNAME='".safequery($newPatient->MOTHERLASTNAME)."',
              FATHERBIRTHDATE=".(nonull($newPatient->FATHERBIRTHDATE)?"'$newPatient->FATHERBIRTHDATE'":"null").", 
              MOTHERBIRTHDATE=".(nonull($newPatient->MOTHERBIRTHDATE)?"'$newPatient->MOTHERBIRTHDATE'":"null").", ";
        if(($newPatient->doctor_lead != 0)&&($newPatient->doctor_lead != null)&&($newPatient->doctor_lead != 'null')&&($newPatient->doctor_lead != ''))
        {
           $QueryText .= "LEADDOCTOR_FK = ".$newPatient->doctor_lead.", "; 
        }
        else
        {
           $QueryText .= "LEADDOCTOR_FK = null, "; 
        }    
        
         if(($newPatient->subdivision_id != 0)&&($newPatient->subdivision_id != null)&&($newPatient->subdivision_id != 'null')&&($newPatient->subdivision_id != ''))
        {
           $QueryText .= "SUBDIVISIONID = ".$newPatient->subdivision_id.", "; 
        }
        else
        {
           $QueryText .= "SUBDIVISIONID = null, "; 
        }    
        
        if($position != null)
        {
           $QueryText .= "LONGTITUDE=".$position[0].", LATITUDE=".$position[1]; 
        }
        else
        {
           $QueryText .= "LONGTITUDE=null, LATITUDE=null "; 
        }
        $QueryText .= " where (ID=$newPatient->ID)";
		
       //  file_put_contents("C:/tmp.txt", $QueryText);
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
        
        $this->saveMobilePhones($newPatient->ID, $newPatient->mobiles, $trans);
        
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;		
    }
    
    /**
 	* @param PatientCardModuleAddPatientObject $newPatient
    * @return string
 	*/
    public function updatePatientForCalendar($newPatient) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update PATIENTS set 		  
    		  FIRSTNAME='".safequery($newPatient->FIRSTNAME)."',
    		  MIDDLENAME='".safequery($newPatient->MIDDLENAME)."',
    		  LASTNAME='".safequery($newPatient->LASTNAME)."',
              FULLNAME='".safequery($newPatient->LASTNAME)." ".safequery($newPatient->FIRSTNAME)." ".safequery($newPatient->MIDDLENAME)."',
   		       SHORTNAME='".safequery($newPatient->LASTNAME)." ".safequery(mb_substr($newPatient->FIRSTNAME,0,1)).".".safequery(mb_substr($newPatient->MIDDLENAME,0,1)).".',";
              //FULLNAME='".safequery($newPatient->FULLNAME)."',
    		  //SHORTNAME='".safequery($newPatient->SHORTNAME)."',";
        if(($newPatient->BIRTHDAY != '')&&($newPatient->BIRTHDAY != 'null'))
        {
            $QueryText .= "BIRTHDAY='$newPatient->BIRTHDAY',";
        }
        if(($newPatient->CARDDATE != '')&&($newPatient->CARDDATE != 'null'))
        {
            $QueryText .= "CARDDATE='$newPatient->CARDDATE',";
        }
        $QueryText .= "SEX=$newPatient->SEX,
    		  VILLAGEORCITY=$newPatient->VILLAGEORCITY,
    		  POPULATIONGROUPE=".($newPatient->POPULATIONGROUPE+$newPatient->ISDISPANCER).",
              ADDRESS='".safequery($newPatient->ADDRESS)."',
              POSTALCODE='".safequery($newPatient->POSTALCODE)."',
              TELEPHONENUMBER='".safequery($newPatient->TELEPHONENUMBER)."', 
              MOBILENUMBER='".safequery($newPatient->MOBILENUMBER)."',
              EMPLOYMENTPLACE='".safequery($newPatient->EMPLOYMENTPLACE)."',
              JOB='".safequery($newPatient->JOB)."',
              WORKPHONE='".safequery($newPatient->WORKPHONE)."',
              HOBBY='".safequery($newPatient->HOBBY)."',
              COUNTRY='".safequery($newPatient->COUNTRY)."',
              STATE='".safequery($newPatient->STATE)."',
              RAGION='".safequery($newPatient->RAGION)."',
              EMAIL='".safequery($newPatient->EMAIL)."',
              CITY='".safequery($newPatient->CITY)."',
              CARDNUMBER='$newPatient->CARDNUMBER',
              CARDAFTERNUMBER='".safequery($newPatient->CARDAFTERNUMBER)."',
              CARDBEFORNUMBER='".safequery($newPatient->CARDBEFORNUMBER)."',
              DISCOUNT_AMOUNT = '$newPatient->discount_amount',
              INDDISCOUNT = ".($newPatient->INDDISCOUNT?'1':'0').", 
              wherefromid = ".nullcheck($newPatient->WHEREFROMID).",
              groupid = ".nullcheck($newPatient->GROUPID).",
              PASSPORT_SERIES='".safequery($newPatient->PASSPORT_SERIES)."',
              PASSPORT_NUMBER='".safequery($newPatient->PASSPORT_NUMBER)."',
              PASSPORT_ISSUING='".safequery($newPatient->PASSPORT_ISSUING)."',
              PASSPORT_DATE=".(nonull($newPatient->PASSPORT_DATE)?"'$newPatient->PASSPORT_DATE'":"null").",   
              LEGALGUARDIANID = ".nullcheck($newPatient->LEGALGUARDIANID).",  
              RELATIVEID = ".nullcheck($newPatient->RELATIVEID).", 
              FATHERFIRSTNAME='".safequery($newPatient->FATHERFIRSTNAME)."',
              MOTHERFIRSTNAME='".safequery($newPatient->MOTHERFIRSTNAME)."',
              FATHERMIDDLENAME='".safequery($newPatient->FATHERMIDDLENAME)."',
              MOTHERMIDDLENAME='".safequery($newPatient->MOTHERMIDDLENAME)."',
              FATHERLASTNAME='".safequery($newPatient->FATHERLASTNAME)."',
              MOTHERLASTNAME='".safequery($newPatient->MOTHERLASTNAME)."',
              FATHERBIRTHDATE=".(nonull($newPatient->FATHERBIRTHDATE)?"'$newPatient->FATHERBIRTHDATE'":"null").", 
              MOTHERBIRTHDATE=".(nonull($newPatient->MOTHERBIRTHDATE)?"'$newPatient->MOTHERBIRTHDATE'":"null").",              
			  PRIMARYFLAG = 1, ";
        
        if(($newPatient->doctor_lead != null)&&($newPatient->doctor_lead != 'null')&&($newPatient->doctor_lead != ''))
        {
           $QueryText .= "LEADDOCTOR_FK = ".$newPatient->doctor_lead; 
        }
        else
        {
           $QueryText .= "LEADDOCTOR_FK = null"; 
        }      
        $QueryText .= " where ID = $newPatient->ID";

        $this->saveMobilePhones($newPatient->ID, $newPatient->mobiles, $trans);
		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
        
        
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $newPatient->ID;		
    }
    
    
    /**
	* @param string $passForDel
 	* @param string[] $Arguments
	* @return boolean
 	*/ 		 		
    public function deletePatients($passForDel, $Arguments) 
     {           
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "SELECT PASSWRD FROM USERS WHERE NAME='passForDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if ($row = ibase_fetch_row($result))
		{
			if ($row[0] != $passForDel)
			{
				ibase_free_result($result);
				ibase_free_query($query);
				ibase_commit($trans);
				$connection->CloseDBConnection();
				return false;
			}
		}
		
		ibase_free_result($result);
		ibase_free_query($query);
       	for ($i=0; isset($Arguments[$i]); $i++)
    	{			
			$QueryText = "delete from PATIENTS where (ID is null) or (ID = '$Arguments[$i]')";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
			ibase_free_query($query);
    	}
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
    * @param string 
 	* @return number
 	*/
    public function getNextCardNumber($currentYear)
     {      
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select first 1 CARDNUMBER, CARDDATE from PATIENTS where PRIMARYFLAG = 1";
        if($currentYear != "false")
        {
            $QueryText .= " and CARDDATE >= '$currentYear-01-01'";
        }
        $QueryText .= " order by CARDNUMBER desc";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);	
		$cardNumber=ibase_fetch_row($result);
		ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
        if($cardNumber[0]==null) return 0;
        else if($currentYear=="false") return $cardNumber[0];
        else return $cardNumber[0];		
    }
    ////////////////////////////////////////
    
    /**
 	* @param PatientCardModulePatientTestCard $newPatientTestCard 
    * @return boolean
 	*/
    public function AddUpdateTestCard($newPatientTestCard) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if($newPatientTestCard->ID == null)
        {
            $QueryText =  "insert into 
                            PATIENTSTESTCARDS 
                            ( ID, PATIENTID, TESTDATE, CARDIACDISEASE, KIDNEYDISEASE, LIVERDISEASE, OTHERDISEASE, ARTERIALPRESSURE, 
                              RHEUMATISM, HEPATITIS, HEPATITISDATE, HEPATITISTYPE, EPILEPSIA, DIABETES, FIT, BLEEDING, ALLERGYFLAG,
                              ALLERGY, PREGNANCY, MEDICATIONS, BLOODGROUP, RHESUSFACTOR, MOUTHULCER, FUNGUS, FEVER, THROATACHE,
                              LYMPHNODES, REDSKINAREA, HYPERHIDROSIS, DIARRHEA, AMNESIA, WEIGHTLOSS, HEADACHE, 
                              AIDS, WORKWITHBLOOD, NARCOMANIA)
                            values
                              (null, 
                              '$newPatientTestCard->PATIENTID', 
                              '$newPatientTestCard->TESTDATE', 
                              '$newPatientTestCard->CARDIACDISEASE',
                              '$newPatientTestCard->KIDNEYDISEASE', 
                              '$newPatientTestCard->LIVERDISEASE', 
                              '$newPatientTestCard->OTHERDISEASE',
                              '$newPatientTestCard->ARTERIALPRESSURE', 
                              '$newPatientTestCard->RHEUMATISM', 
                              '$newPatientTestCard->HEPATITIS',
                              '$newPatientTestCard->HEPATITISDATE',
                              '$newPatientTestCard->HEPATITISTYPE',
                              '$newPatientTestCard->EPILEPSIA',
                              '$newPatientTestCard->DIABETES', 
                              '$newPatientTestCard->FIT', 
                              '$newPatientTestCard->BLEEDING',
                              '$newPatientTestCard->ALLERGYFLAG',
                              '".safequery($newPatientTestCard->ALLERGY)."', 
                              '$newPatientTestCard->PREGNANCY',
                              '".safequery($newPatientTestCard->MEDICATIONS)."', 
                              '$newPatientTestCard->BLOODGROUP', 
                              '$newPatientTestCard->RHESUSFACTOR',
                              '$newPatientTestCard->MOUTHULCER', 
                              '$newPatientTestCard->FUNGUS',
                              '$newPatientTestCard->FEVER', 
                              '$newPatientTestCard->THROATACHE',
                              '$newPatientTestCard->LYMPHNODES', 
                              '$newPatientTestCard->REDSKINAREA',
                              '$newPatientTestCard->HYPERHIDROSIS', 
                              '$newPatientTestCard->DIARRHEA',
                              '$newPatientTestCard->AMNESIA', 
                              '$newPatientTestCard->WEIGHTLOSS',
                              '$newPatientTestCard->HEADACHE', 
                              '$newPatientTestCard->AIDS',
                              '$newPatientTestCard->WORKWITHBLOOD', 
                              '$newPatientTestCard->NARCOMANIA')";
        } 
        else
        {
            $QueryText = "update 
                            PATIENTSTESTCARDS
                         set 
                            TESTDATE='$newPatientTestCard->TESTDATE', 
                            CARDIACDISEASE='$newPatientTestCard->CARDIACDISEASE', 
                            KIDNEYDISEASE='$newPatientTestCard->KIDNEYDISEASE', 
                            LIVERDISEASE='$newPatientTestCard->LIVERDISEASE', 
                            OTHERDISEASE='$newPatientTestCard->OTHERDISEASE', 
                            ARTERIALPRESSURE='$newPatientTestCard->ARTERIALPRESSURE', 
                            RHEUMATISM='$newPatientTestCard->RHEUMATISM', 
                            HEPATITIS='$newPatientTestCard->HEPATITIS', 
                            HEPATITISDATE='$newPatientTestCard->HEPATITISDATE', 
                            HEPATITISTYPE='$newPatientTestCard->HEPATITISTYPE', 
                            EPILEPSIA='$newPatientTestCard->EPILEPSIA', 
                            DIABETES='$newPatientTestCard->DIABETES', 
                            FIT='$newPatientTestCard->FIT', 
                            BLEEDING='$newPatientTestCard->BLEEDING', 
                            ALLERGYFLAG='$newPatientTestCard->ALLERGYFLAG',
                            ALLERGY='".safequery($newPatientTestCard->ALLERGY)."', 
                            PREGNANCY='$newPatientTestCard->PREGNANCY', 
                            MEDICATIONS='".safequery($newPatientTestCard->MEDICATIONS)."', 
                            BLOODGROUP='$newPatientTestCard->BLOODGROUP', 
                            RHESUSFACTOR='$newPatientTestCard->RHESUSFACTOR', 
                            MOUTHULCER='$newPatientTestCard->MOUTHULCER', 
                            FUNGUS='$newPatientTestCard->FUNGUS', 
                            FEVER='$newPatientTestCard->FEVER', 
                            THROATACHE='$newPatientTestCard->THROATACHE',
                            LYMPHNODES='$newPatientTestCard->LYMPHNODES', 
                            REDSKINAREA='$newPatientTestCard->REDSKINAREA', 
                            HYPERHIDROSIS='$newPatientTestCard->HYPERHIDROSIS', 
                            DIARRHEA='$newPatientTestCard->DIARRHEA', 
                            AMNESIA='$newPatientTestCard->AMNESIA', 
                            WEIGHTLOSS='$newPatientTestCard->WEIGHTLOSS', 
                            HEADACHE='$newPatientTestCard->HEADACHE', 
                            AIDS='$newPatientTestCard->AIDS', 
                            WORKWITHBLOOD='$newPatientTestCard->WORKWITHBLOOD', 
                            NARCOMANIA='$newPatientTestCard->NARCOMANIA'                            
                         where ID='$newPatientTestCard->ID'";
        }
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;		
    }
    
    /** 
 	* @param int $patientID
    * @return PatientCardModulePatientTestCard
	*/    
    public function getTestCard($patientID) 
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText =
		      "select ID, PATIENTID, TESTDATE, CARDIACDISEASE, KIDNEYDISEASE, LIVERDISEASE, OTHERDISEASE, ARTERIALPRESSURE, 
                              RHEUMATISM, HEPATITIS, HEPATITISDATE, HEPATITISTYPE, EPILEPSIA, DIABETES, FIT, BLEEDING, ALLERGYFLAG,
                              ALLERGY, PREGNANCY, MEDICATIONS, BLOODGROUP, RHESUSFACTOR, MOUTHULCER, FUNGUS, FEVER, THROATACHE,
                              LYMPHNODES, REDSKINAREA, HYPERHIDROSIS, DIARRHEA, AMNESIA, WEIGHTLOSS, HEADACHE, 
                              AIDS, WORKWITHBLOOD, NARCOMANIA
              from PATIENTSTESTCARDS
		      where (PATIENTID=$patientID)";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);	
		$row = ibase_fetch_row($result);
        
        $obj = new PatientCardModulePatientTestCard;
        $obj->ID=$row[0];
        $obj->PATIENTID=$row[1];
        $obj->TESTDATE=$row[2];
        $obj->CARDIACDISEASE=$row[3];
        $obj->KIDNEYDISEASE=$row[4];
        $obj->LIVERDISEASE=$row[5];
        $obj->OTHERDISEASE=$row[6];
        $obj->ARTERIALPRESSURE=$row[7];
        $obj->RHEUMATISM=$row[8];
        $obj->HEPATITIS=$row[9];
        $obj->HEPATITISDATE=$row[10];
        $obj->HEPATITISTYPE=$row[11];
        $obj->EPILEPSIA=$row[12];
        $obj->DIABETES=$row[13];
        $obj->FIT=$row[14];
        $obj->BLEEDING=$row[15];
        $obj->ALLERGYFLAG=$row[16];
            $txtBlob = null;
            $blob_info = ibase_blob_info($row[17]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[17]);
                $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
                
            }
            $obj->ALLERGY=$txtBlob;
        //$obj->ALLERGY=$row[17];
        $obj->PREGNANCY=$row[18];
            $txtBlob = null;
            $blob_info = ibase_blob_info($row[19]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[19]);
                $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
                
            }
            $obj->MEDICATIONS=$txtBlob;
        //$obj->MEDICATIONS=$row[19];
        $obj->BLOODGROUP=$row[20];
        $obj->RHESUSFACTOR=$row[21];
        $obj->MOUTHULCER=$row[22];
        $obj->FUNGUS=$row[23];
        $obj->FEVER=$row[24];
        $obj->THROATACHE=$row[25];
        $obj->LYMPHNODES=$row[26];
        $obj->REDSKINAREA=$row[27];
        $obj->HYPERHIDROSIS=$row[28];
        $obj->DIARRHEA=$row[29];
        $obj->AMNESIA=$row[30];
        $obj->WEIGHTLOSS=$row[31];
        $obj->HEADACHE=$row[32];
        $obj->AIDS=$row[33];
        $obj->WORKWITHBLOOD=$row[34];
        $obj->NARCOMANIA=$row[35];
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $obj; 
    } 
		
	/** 
 	* @param int $patientID
 	* @return PatientCardModuleClinicRegistrationData
	*/ 
  public function getClinicRegistrationData($patientID)  
  {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = 
			"select 
				LEGALNAME,
				ADDRESS,				
				TELEPHONENUMBER
			from CLINICREGISTRATIONDATA";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{
				$obj = new PatientCardModuleClinicRegistrationData;	
				
				$obj->NAME = $row[0];	
				$obj->ADDRESS = $row[1];			
				$obj->TELEPHONENUMBER = $row[2];
			}	
			ibase_free_query($query);
            $QueryText = 
    			"select 
    				FIRSTNAME,
    				MIDDLENAME,				
    				LASTNAME,
                    BIRTHDAY,
                    ADDRESS
    			from PATIENTS where ID = '$patientID'";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            if ($row = ibase_fetch_row($result))
			{
				
				$obj->FIRSTNAME = $row[0];	
				$obj->MIDDLENAME = $row[1];			
				$obj->LASTNAME = $row[2];			
				$obj->BIRTHDAY = $row[3];			
				$obj->PATIENTADDRESS = $row[4];
			}	
			ibase_free_query($query);
			$success = ibase_commit($trans);
			ibase_free_result($result);
			$connection->CloseDBConnection();
			if (ibase_errmsg() != false)
			{
				trigger_error(ibase_errmsg(), E_USER_ERROR);				
			}
			return $obj;
    }
   
    
    /////////////////////////////////////////////////PRIMARY PATIENTS START//////////////////////////////////////////////////////////
    /**  
    * @return PatientCardModulePrimaryDataGridPatientObject[]
	*/    
    public function getPrimaryPatients() 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			/*"select PATIENTS.ID, PATIENTS.FIRSTNAME, PATIENTS.MIDDLENAME, PATIENTS.LASTNAME, PATIENTS.MOBILENUMBER, TASKS.TASKDATE
                 from PATIENTS
                 left join TASKS on (PATIENTS.ID = TASKS.PATIENTID)
                 where (PATIENTS.ID is not null) and PATIENTS.PRIMARYFLAG = 0 order by TASKS.TASKDATE desc";*/
                "select PATIENTS.ID, PATIENTS.FIRSTNAME, PATIENTS.MIDDLENAME, PATIENTS.LASTNAME, PATIENTS.MOBILENUMBER, TASKS.TASKDATE, DOCTORS.SHORTNAME
                     from PATIENTS
                     left join TASKS on (PATIENTS.ID = TASKS.PATIENTID)
                     left join DOCTORS on (TASKS.DOCTORID = DOCTORS.ID)
                 where (PATIENTS.ID is not null) and PATIENTS.PRIMARYFLAG = 0 order by TASKS.TASKDATE desc";
        			
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new PatientCardModulePrimaryDataGridPatientObject;
            $obj->ID = $row[0];
            $obj->PFname = $row[1];
            $obj->PMname = $row[2];
            $obj->PLname = $row[3];
            $obj->PPphone = $row[4];
			$obj->PCDate = $row[5];
            $obj->DoctorName = $row[6];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    /**
 	* @param string[] $Arguments
	* @return boolean
 	*/ 		 		
    public function deletePrimayPatients($Arguments) 
     {           
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
       	for ($i=0; isset($Arguments[$i]); $i++)
    	{			
			$QueryText = "delete from PATIENTS where ((ID is null) or (ID = $Arguments[$i])) and PRIMARYFLAG = 0";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
			ibase_free_query($query);
    	}
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;			
    }
    /////////////////////////////////////////////////PRIMARY PATIENTS END//////////////////////////////////////////////////////////
    
    
    /////////////////////////////////////////////////INSURANCE START//////////////////////////////////////////////////////////
    /** 
    * @param string $patientId
 	* @return PatientCardModuleInsurancePolisObject[]
	*/ 
    public function getInsurancePoliceByPatientId($patientId) 
     { 
        //PatientCardModuleInsuranceCompanyObject
        //PatientCardModuleInsurancePolisObject
        //-2147483501
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	    $QueryText ="select ID, POLICENUMBER, POLICETODATE, POLICEFROMDATE, COMPANYID, COMMENTS from PATIENTSINSURANCE where PATIENTID = $patientId
                        order by POLICETODATE desc";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new PatientCardModuleInsurancePolisObject;
          $obj->Id = $row[0];
          $obj->policyNumber = $row[1];
          $obj->toDate = $row[2];
          $obj->fromDate = $row[3];
          $obj->patientId = $patientId;
          $obj->Comments = $row[5];
          if($row[4])
          {
              $QueryText2 ="select ID, NAME, PHONE, ADDRESS from INSURANCECOMPANYS where ID = $row[4]";
    		  $query2 = ibase_prepare($trans, $QueryText2);
    		  $result2=ibase_execute($query2);
              $companyRow = ibase_fetch_row($result2);
              $comp_obj = new PatientCardModuleInsuranceCompanyObject;
              $comp_obj->Id = $companyRow[0];
              $comp_obj->Name = $companyRow[1];
              $comp_obj->Phone = $companyRow[2];
              $comp_obj->Address = $companyRow[3];
              
              $obj->company = $comp_obj;
    		  ibase_free_query($query2);
    		  ibase_free_result($result2);
          }
		  $objs[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
        
    /** 
    * @return PatientCardModuleInsuranceCompanyObject[]
 	*/
    public function getInsuranceCompanys() //sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, NAME, PHONE, ADDRESS from INSURANCECOMPANYS";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new PatientCardModuleInsuranceCompanyObject;
          $obj->Id = $row[0];
          $obj->Name = $row[1];
          $obj->Phone = $row[2];
          $obj->Address = $row[3];
		  $objs[] = $obj;
		}	
        ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $objs;
    }
          
    /**
 	* @param PatientCardModuleInsurancePolisObject $insurance
    * @return boolean
 	*/
    public function addInsuranceToPatient($insurance) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into PATIENTSINSURANCE (ID, POLICENUMBER, POLICETODATE, POLICEFROMDATE, COMPANYID, PATIENTID, COMMENTS) 
            values (null,'".safequery($insurance->policyNumber)."','$insurance->toDate','$insurance->fromDate','".$insurance->company->Id."','$insurance->patientId','".safequery($insurance->Comments)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param PatientCardModuleInsurancePolisObject $insurance
    * @return boolean
 	*/
    public function updateInsuranceToPatient($insurance) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update PATIENTSINSURANCE set 
            POLICENUMBER='".safequery($insurance->policyNumber)."', 
            POLICETODATE='$insurance->toDate', 
            POLICEFROMDATE='$insurance->fromDate', 
            COMPANYID='".$insurance->company->Id."',
            COMMENTS='".safequery($insurance->Comments)."' 
            where ID=$insurance->Id";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param string $insuranceId
    * @return boolean
 	*/
    public function deleteInsuranceToPatient($insuranceId) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from PATIENTSINSURANCE where ID=$insuranceId";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }

    
   
    /////////////////////////////////////////////////INSURANCE END//////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////LEAD DOCTOR START////////////////////////////////////////////////////////// 
    /**
    * @return PatientCardModuleDoctorObject[]
 	*/
    //public function getDoctors($nowdate)
    public function getDoctors() //sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $nowTimestamp = new DateTime();
		$QueryText = "select 
                        doctors.id,
                        doctors.firstname,
                        doctors.lastname,
                        doctors.middlename,
                        doctors.shortname
                    from doctors
                    where doctors.isfired is null or doctors.isfired != 1";
                    // WHERE 
                    //    (DATEWORKEND >= '".$nowdate."')or(DATEWORKEND IS null)";
                    
        /*        where (doctors.dateworkend is null or doctors.dateworkend < '".$nowTimestamp->format("D M j G:i:s")."')";
         $nowTimestamp = new DateTime();
                $QueryDoctorText =
    			"select distinct(doctors.id), doctors.shortname
                from doctors
                
                left join toothexam
                on toothexam.doctorid = doctors.id
                
                where toothexam.id = '$obj->ID'";
                and (doctors.dateworkend is null or doctors.dateworkend < '".$nowTimestamp->format("D M j G:i:s")."')";*/
                
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new PatientCardModuleDoctorObject;
          $obj->docid = $row[0];
          $obj->fname = $row[1];
          $obj->lname = $row[2];
          $obj->pname = $row[3];
          $obj->shortname = $row[4];
		  $objs[] = $obj;
		}	
        ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $objs;
    }
    /////////////////////////////////////////////////LEAD DOCTOR END//////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////SUBDIVISON START////////////////////////////////////////////////////////// 
    /**
    * @return PatientCardModuleSubdivisonObject[]
 	*/
    public function getSubdivisons() //sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $nowTimestamp = new DateTime();
		$QueryText = "select subdivision.id,
                        subdivision.name
                        from subdivision";
                
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new PatientCardModuleSubdivisonObject;
          $obj->id = $row[0];
          $obj->name = $row[1];
		  $objs[] = $obj;
		}	
        ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $objs;
    }
    /////////////////////////////////////////////////SUBDIVISON END//////////////////////////////////////////////////////////
    
    //***********************************************PRIVATE FUNCTIONS START***********************************************
     /**
     * PatientCardModule::getAges()
     * 
     * @param string $birthdate
     * @return int
     */
    private function getAges($birthdate)
    {
        if(($birthdate != null)&&($birthdate != ''))
        {
            $date = new DateTime($birthdate);
            $now = new DateTime();
            $interval = $now->diff($date);
            return $interval->y;
        }
        else
        {
            return -1;
        }
    }
    //***********************************************PRIVATE FUNCTIONS END*************************************************
    /** 
 	* @param string $search
    * @param string $id
    * @return PatientCardModulePatientForLegalGuardian[]
	*/    
    public function getPatientsForLegalGuardian($search,$id) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        $QueryText =
			"select first 10 
                    patients.ID,
                    patients.FULLNAME
                 from PATIENTS";
                 
       $where=array();          
	    if(nonull($search))	
        {
            $where[] = "lower(patients.FULLNAME) like lower('%$search%')";
        }
   	    if(nonull($id))		
        {
            $where[] = "patients.ID <> $id";
        }
        if(count($where)>0)
        {
           $QueryText.=" where ".implode(' and ',$where); 
        }
        $QueryText.=" order by patients.FULLNAME";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new PatientCardModulePatientForLegalGuardian;
            $obj->ID = $row[0];
            $obj->FULLNAME = $row[1];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
     /** 
    * @param string $id
    * @return PatientCardModulePatientForLegalGuardian
	*/    
    public function getPatientForLegalGuardianByID($id) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        $QueryText =
			"select patients.ID,
                    patients.FULLNAME
                 from PATIENTS where id=$id";
                 
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
		if ($row = ibase_fetch_row($result))
		{ 
            $obj = new PatientCardModulePatientForLegalGuardian;
            $obj->ID = $row[0];
            $obj->FULLNAME = $row[1];
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $obj;
    }
    
    // export patients data *********** START **************
    
    /** 
    * @param string $docType
    * @return string
    */ 
    public static function exportPatientsData($docType)
    {   
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
           
           
            $maxPhoneCount_sql = "select max(count3) from (select count(patientsmobile.id) as count3 from patientsmobile group by patientsmobile.patient_fk)";
            $maxPhoneCount_query = ibase_prepare($trans, $maxPhoneCount_sql);
            $maxPhoneCount_result = ibase_execute($maxPhoneCount_query);
            $row_mpc = ibase_fetch_row($maxPhoneCount_result);
            $phoneMaxCount = $row_mpc[0];
            ibase_free_query($maxPhoneCount_query);
            ibase_free_result($maxPhoneCount_result);
            
            
            $patient_sql = "select

                                patients.lastname,
                                patients.firstname,
                                patients.middlename,
                                patients.sex,
                                (select cast(list(patientsmobile.mobilenumber, '|') as VARCHAR(1000)) from patientsmobile where patientsmobile.patient_fk = patients.id),
                                datediff (year from patients.birthday to current_date),
                                patients.birthday,
                                patients.city,
                                patients.address,
                                patients.discount_amount,
                                patients.carddate
                        
                            from patients
                            where patients.primaryflag = 1";
                    
            $patient_query = ibase_prepare($trans, $patient_sql);
            $patient_result = ibase_execute($patient_query);
            
            $data = array();
            while ($row = ibase_fetch_row($patient_result))
            { 
                $item = new stdClass();
                $item->lastname = $row[0];
                $item->firstname = $row[1];
                $item->middlename = $row[2];
                $item->sex = ($row[3] == 1 ? "Ж" : "М") ;
                
                $phoneArray = explode('|', $row[4]);
                for($i = 0; $i < $phoneMaxCount; ++$i) 
                {
                    if($i < count($phoneArray))
                    {
                        $item->{"phone".chr(100+$i)} = $phoneArray[$i];
                    }
                    else
                    {
                        $item->{"phone".chr(100+$i)} = '';
                    }
                }
                
                $item->age = $row[5];
                $item->birthday = $row[6];
                $item->city = $row[7];
                $item->address = $row[8];
                $item->discount_amount = $row[9];
                $item->carddate = $row[10];
                $data[] = $item;
            }
            ibase_free_query($patient_query);
            ibase_free_result($patient_result);
            
            
            $TBS = new clsTinyButStrong;
            $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/patients_export.'.$docType, OPENTBS_ALREADY_UTF8);  
            
            
            if($docType=='ods')
            {
                $rowBlock='table:table-row';
                $numOpe='odsNum';
                $dateOpe='odsDate'; 
            }
            else if($docType=='xlsx')
            {
                $rowBlock='row';
                $numOpe='xlsxNum';
                $dateOpe='xlsxDate'; 
            }
            else
            {
                $rowBlock='';
                $numOpe='';
                $dateOpe='';  
            }
            
            $fieldNames = array();
            $template = array();
            
            
            $fields = array();
            
            $field = new stdClass();
            $field->label = 'фамилия';
            $field->name = 'lastname';
            $field->type = 'string';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'имя';
            $field->name = 'firstname';
            $field->type = 'string';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'отчество';
            $field->name = 'middlename';
            $field->type = 'string';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'пол';
            $field->name = 'sex';
            $field->type = 'string';
            $fields[] = $field;
            
            for($i = 0; $i < $phoneMaxCount; ++$i) 
            {
                $field = new stdClass();
                $field->label = 'телефон '.($i+1);
                $field->name = 'phone'.chr(100+$i);
                $field->type = 'string';
                $fields[] = $field;
            }
            
            $field = new stdClass();
            $field->label = 'возраст';
            $field->name = 'age';
            $field->type = 'numeric';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'дата рождения';
            $field->name = 'birthday';
            $field->type = 'date';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'город';
            $field->name = 'city';
            $field->type = 'string';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'адрес';
            $field->name = 'address';
            $field->type = 'string';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'скидка';
            $field->name = 'discount_amount';
            $field->type = 'numeric';
            $fields[] = $field;
            
            $field = new stdClass();
            $field->label = 'дата создания карточки';
            $field->name = 'carddate';
            $field->type = 'date';
            $fields[] = $field;
            
            
            foreach($fields as $key=>$field)
            {
                $fieldNames[] = $field->label;
                switch($field->type)
                {
                    case 'numeric':
                    {
                        $template[]='[DATA.'.$field->name.';ope='.$numOpe.']';    
                        break;                    
                    }
                    case 'string':
                    case 'date':
                    default:
                    {
                        $template[]='[DATA.'.$field->name.']';
                        break;
                    }                                                    
                }   
            }
            
            if(count($template)>0)
            {
               $template[0]='[DATA;block='.$rowBlock.']'.$template[0];                     
            } 
            
            
            $TBS->SetOption('protect',false);
            $TBS->MergeBlock('FIELDS',$fieldNames);            
            $TBS->MergeBlock('TEMPLATE',$template);
            $TBS->MergeBlock('DATA',$data);
            
            /*
            file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($fields, true));
            file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($fieldNames, true));
            file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($template, true));
            file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($data, true));
            */
           
            $file_name = translit(str_replace(' ','_','patients_export'));
            $form_path = uniqid().'_'.$file_name.'.'.$docType;   
            
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
            
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
                    
            return $form_path; 
    }
        
        
        
        /** 
        * @param string $form_path
        * @return boolean
        */  
        public static function deleteFile($form_path)
        {
            return deleteFileUtils($form_path);
        }
    // export patients data *********** END **************
   
}

?>
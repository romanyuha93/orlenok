<?php
require_once "tbs/tbs_class.php";
require_once "tbs/tbs_plugin_opentbs.php"; 

require_once "Connection.php";
require_once "Utils.php";
require_once "F43OBJECT.php";
require_once "EmailService.php";
require_once "PrintDataModule.php";

//$ta=new TeethExamModule();
//print($ta->getF43XML());
class TeethExamModuleToothOut
{
    /**
   * @var string
   */
	var $examid;
   /**
   * @var string
   */
	var $exam;
   /**
   * @var string
   */
	var $examdate;  
   /**
   * @var string
   */
	var $checkup;   
   /**
   * @var string
   */
	var $complaints;   
   /**
   * @var string
   */
	var $doctor;//фамилия + имя доктора
   /**
   * @var string
   */
	var $assistant;//фамилия + имя ассистента
    /**
   * @var string
   */
	var $channelsexam;
    /**
   * @var int
   */
	var $bitetype;
    
   /**
   * @var int
   */
	var $afterflag;
   /**
   * @var string
   */
	var $course_fk;
   /**
   * @var string
   */
	var $course_date;
    
   /**
   * @var string
   */
	var $diseasehistory;
    
    /**
   * @var string
   */
	var $treatmentplan;
    
    /**
   * @var string
   */
	var $examplan;
}

class TeethExamModuleToothIn
{
   /**
   * @var string
   */
   var $examid;
   /**
   * @var int
   */
	var $pat_id;
   /**
   * @var string
   */
	var $doc_id;
   /**
   * @var string
   */
	var $assistant_id;
   /**
   * @var string
   */
	var $exam;
   /**
   * @var string
   */
   var $examdate;
    /**
   * @var string
   */
	var $channelsexam;
   /**
   * @var string
   */
	var $checkup;   
   /**
   * @var string
   */
	var $complaints; 
    /**
   * @var int
   */
	var $bitetype; 
    
   /**
   * @var int
   */
	var $afterflag;
   /**
   * @var string
   */
	var $course_fk;
    
   /**
   * @var string
   */
	var $diseasehistory;
    
        /**
   * @var string
   */
	var $treatmentplan;
    
    /**
   * @var string
   */
	var $examplan;
}

class TeethExamModuleToothSOut
{
   /**
   * @var int
   **/
    var $birthyears;
   /**
   * @var string
   */
	var $fname;
   /**
   * @var string
   */
	var $lname;
   /**
   * @var string
   */
	var $kartnum; 
    /**
   * @var TeethExamModuleDocForTooth[]
   */
	var $docList; 
    /**
   * @var TeethExamModuleAssistantForTooth[]
   */
	var $assistantsList;
   /**
   * @var TeethExamModuleToothOut[]
   */
	var $ToothOut;  
}

class TeethExamModuleDocForTooth
{
   /**
   * @var string
   */
	var $name;
   /**
   * @var int
   */
	var $docid;  
 
    /**
   * @var string
   */
	var $spec; 
}

class TeethExamModuleAssistantForTooth
{
   /**
   * @var string
   */
	var $name;
   /**
   * @var int
   */
	var $assid;  
}

class TeethExamModuleF43
{
    /**
   * @var int
   */
	var $ID;
    
   /**
   * @var int
   */
	var $F43;
    
    /**
   * @var int
   */
	var $SEQNUMBER;
    
    /**
   * @var boolean
   */
	var $ISQUICK;
    
    /**
   * @var string
   */
	var $NAME;
    
       /**
   * @var string
   */
	var $COMPLAINTS;
    
    /**
   * @var string
   */
	var $SIGN;
    
    /**
   * @var boolean
   */
	var $FLAGTOOTH;
    
    /**
   * @var boolean
   */
	var $FLAGSURFACE;
    
    /**
   * @var boolean
   */
	var $FLAGROOT;

    
}

class TeethExamModule
{
  
  /**
  *	@param TeethExamModuleToothOut $p1
  *	@param TeethExamModuleToothIn $p2
  *	@param TeethExamModuleToothSOut $p3
  *	@param TeethExamModuleDocForTooth $p4
  * @param TeethExamModuleAssistantForTooth $p5
  * @param TeethExamModuleF43 $p6
  * @return void
  */
	public function registertypes($p1, $p2, $p3, $p4, $p5, $p6) {}
   
  
    /**
  * @param resource $trans
  * @return TeethExamModuleF43[]
  */
  
  public function getF43($trans=null)//sqlite
  {
         if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
        
        	$QueryText = "
			select 
                ID,
                F43,
                NAME,
                COMPLAINTS,
                SIGN,
                FLAGTOOTH,
                FLAGSURFACE,
                FLAGROOT,
                SEQNUMBER,
                ISQUICK
			from F43 order by SEQNUMBER";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$f43_array=array();
			while($row = ibase_fetch_row($result))
			{		 
                $f43=new TeethExamModuleF43();
                $f43->ID=$row[0]; 
                $f43->F43=$row[1]; 
                $f43->NAME=$row[2]; 
                $f43->COMPLAINTS=$row[3]; 
                $f43->SIGN=$row[4]; 
                $f43->FLAGTOOTH=$row[5]==1; 
                $f43->FLAGSURFACE=$row[6]==1; 
                $f43->FLAGROOT=$row[7]==1; 
                $f43->SEQNUMBER=$row[8]; 
                $f43->ISQUICK=$row[9]; 
                
                $f43_array[]=$f43;
			}
			ibase_free_query($query);
			ibase_free_result($result);
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $f43_array;
  }
  
  
   /**
  * @return string
  */
  public function getF43XML()//sqlite
  {
     $f43_arr=$this->getF43();
     $xml='<diagdb>';
     foreach($f43_arr as $f43)
     {
        $buf='
        <label>'.(nonull($f43->SIGN)?($f43->SIGN.' - '):'').$f43->NAME.'</label>
        <data>'.$f43->NAME.'</data>
        <num>'.$f43->F43.'</num>
        <complaints>'.$f43->COMPLAINTS.'</complaints>
    ';
        if($f43->FLAGSURFACE)
        {
            $xml.='
    <Obj id="'.$f43->F43.'">'.$buf.'</Obj>';
        }
        if($f43->FLAGTOOTH)
        {
            $xml.='
    <ObjBack id="'.$f43->F43.'">'.$buf.'</ObjBack>';
        }
        if($f43->FLAGROOT)
        {
            $xml.='
    <ObjR id="'.$f43->F43.'">'.$buf.'</ObjR>';
        }
     }
     $xml.='
  </diagdb>';
     return $xml;
  }
  
      /**
 	* @param TeethExamModuleF43 $ovo
    * @return boolean
 	*/
    public function updateF43 ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
            $QueryText = "select SEQNUMBER from F43 where (ID=".$ovo->ID." AND SEQNUMBER<>".$ovo->SEQNUMBER.")";
            $query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if($row = ibase_fetch_row($result))
			{	
                $old_seqnumber=$row[0];
                if($old_seqnumber>$ovo->SEQNUMBER)
                {
                    $QueryText = "update F43
                     set SEQNUMBER =SEQNUMBER+1
					     where (SEQNUMBER>=".$ovo->SEQNUMBER." and SEQNUMBER<".$old_seqnumber.")"; 
                }
                else
                {
                    $QueryText = "update F43
                     set SEQNUMBER =SEQNUMBER-1
					     where (SEQNUMBER<=".$ovo->SEQNUMBER." and SEQNUMBER>".$old_seqnumber.")"; 
                }

        		$query = ibase_prepare($trans, $QueryText);
        		ibase_execute($query);
			}

        
        
        $QueryText = "update F43
					 set NAME ='".safequery($ovo->NAME)."',
                     SEQNUMBER =".$ovo->SEQNUMBER.",
                     SIGN ='".safequery($ovo->SIGN)."',
                     COMPLAINTS ='".safequery($ovo->COMPLAINTS)."',
                     ISQUICK =".($ovo->ISQUICK?"1":"0")." 
					     where ID = $ovo->ID";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
  /**
  * @param TeethExamModuleToothIn $item
  * @param boolean $isUpdate
  * @param string $treatmentcourseId
  * @param boolean $addAfterExam
  * @return boolean
  */
  public function saveExam($item, $isUpdate, $treatmentcourseId, $addAfterExam)
  {
    $connection = new Connection();
	$connection->EstablishDBConnection();
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    
    if($isUpdate == false)
    {
        //create treatmencourse 
        $QueryText = "insert into treatmentcourse(id, doctor_fk, patient_fk) values (null, ".$item->doc_id.", ".$item->pat_id.") RETURNING id";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        if($row = ibase_fetch_row($result))
        {
            $tcid=$row[0];
        }
         
        ibase_free_query($query);
        ibase_free_result($result);
        //create diagnos2 for general procedures
        $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,GARANTDATE,STACKNUMBER,PROTOCOL_FK) 
        values (null, $tcid,".$item->doc_id.",null,null,null,-1,null)";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
    	ibase_free_query($query);
        
        //create toothexam with afterflag = 0
    	$QueryText = "insert into TOOTHEXAM(ID, DOCTORID, ASSISTANTID, EXAM, EXAMDATE, CHANNELSEXAM, AFTERFLAG, COMPLAINTS, CHECKUP, BITETYPE, TREATMENTCOURSE_FK, DISEASEHISTORY, TREATMENTPLAN, EXAMPLAN)
                 values($item->pat_id, $item->doc_id, ";
        if(($item->assistant_id != null)&&($item->assistant_id != ''))
        {
            $QueryText .= $item->assistant_id.", ";
        }
        else
        {
            $QueryText .= "null, ";
        }
        $QueryText .= " '$item->exam', '$item->examdate', '$item->channelsexam', 0, '".safequery($item->complaints)."', '".safequery($item->checkup)."', $item->bitetype, $tcid, '".safequery($item->diseasehistory)."', '".safequery($item->treatmentplan)."', '".safequery($item->examplan)."')"; 
    	$query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
    	//$result = ibase_execute($query);
        //$ToothExamID = ibase_fetch_row($result);
        ibase_free_query($query);
        
        if($addAfterExam)
        {
            //create toothexam with afterflag = 1
        	$QueryText = "insert into TOOTHEXAM(ID, DOCTORID, ASSISTANTID, EXAM, EXAMDATE, CHANNELSEXAM, AFTERFLAG, COMPLAINTS, CHECKUP, BITETYPE, TREATMENTCOURSE_FK, DISEASEHISTORY, TREATMENTPLAN, EXAMPLAN)
                     values($item->pat_id, $item->doc_id, ";
            if(($item->assistant_id != null)&&($item->assistant_id != ''))
            {
                $QueryText .= $item->assistant_id.", ";
            }
            else
            {
                $QueryText .= "null, ";
            }
            $QueryText .= " '$item->exam', '$item->examdate', '$item->channelsexam', 1, '', '', $item->bitetype, $tcid, '', '', '')"; 
        	$query = ibase_prepare($trans, $QueryText);
        	ibase_execute($query);
            //$ToothExamID = ibase_fetch_row($result);
            ibase_free_query($query);
        }
    }
    else if(($item->course_fk != null)&&($item->course_fk != ''))
    {
        if(($item->examid != null)&&($item->examid != ''))
        {
           $QueryText = "update toothexam set
                    ID = $item->pat_id,
                    DOCTORID = $item->doc_id, ";
                    
            if(($item->assistant_id != null)&&($item->assistant_id != ''))
            {
                $QueryText .= "ASSISTANTID = ".$item->assistant_id.", ";
            }
            
            $QueryText .= "EXAM = '$item->exam', 
                        EXAMDATE = '$item->examdate', 
                        CHANNELSEXAM = '$item->channelsexam', 
                        AFTERFLAG = 1, 
                        COMPLAINTS = '".safequery($item->complaints)."', 
                        CHECKUP = '".safequery($item->checkup)."', 
                        BITETYPE = $item->bitetype, 
                        TREATMENTCOURSE_FK = $item->course_fk,
                        DISEASEHISTORY = '".safequery($item->diseasehistory)."', 
                        TREATMENTPLAN = '".safequery($item->treatmentplan)."', 
                        EXAMPLAN = '".safequery($item->examplan)."'
                        where examid = $item->examid";
                       
        	$query = ibase_prepare($trans, $QueryText);
        	ibase_execute($query);
            //$ToothExamID = ibase_fetch_row($result);
            ibase_free_query($query); 
        }
        else
        {
            //create toothexam with afterflag = 1
        	$QueryText = "insert into TOOTHEXAM(ID, DOCTORID, ASSISTANTID, EXAM, EXAMDATE, CHANNELSEXAM, AFTERFLAG, COMPLAINTS, CHECKUP, BITETYPE, TREATMENTCOURSE_FK, DISEASEHISTORY, TREATMENTPLAN, EXAMPLAN)
                     values($item->pat_id, $item->doc_id, ";
            if(($item->assistant_id != null)&&($item->assistant_id != ''))
            {
                $QueryText .= $item->assistant_id.", ";
            }
            else
            {
                $QueryText .= "null, ";
            }
            $QueryText .= " '$item->exam', '$item->examdate', '$item->channelsexam', 1, '".safequery($item->complaints)."', '".safequery($item->checkup)."', $item->bitetype, $item->course_fk, '".safequery($item->diseasehistory)."', '".safequery($item->treatmentplan)."', '".safequery($item->examplan)."')"; 
        	$query = ibase_prepare($trans, $QueryText);
        	ibase_execute($query);
            //$ToothExamID = ibase_fetch_row($result);
            ibase_free_query($query);
        }
    }

    
    
	$success = ibase_commit($trans);	
	$connection->CloseDBConnection();
	return $success;
  }
  
    
  /**
  * @param int $item
  * @param string $passForDelete
  * @return Object
  */
  public function deleteExam($item, $passForDelete)
  { 
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $successObj = new stdClass();
        $success = 0;
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
                $sqlDelILL = "delete from ILLHISTORY where TOOTHEXAM = '$item'";
                $queryDelILL = ibase_prepare($trans, $sqlDelILL);
               	ibase_execute($queryDelILL);		
            	ibase_free_query($queryDelILL);
                
                
                //check count of toothexams for deleted exams course
                $sqlCheck = "select count(id) from toothexam where TREATMENTCOURSE_FK = (select TREATMENTCOURSE_FK from toothexam where examid = $item)";
                $queryCheck = ibase_prepare($trans, $sqlCheck);
                $resultCheck = ibase_execute($queryCheck);
                $rowCheck = ibase_fetch_row($resultCheck);
                
                if($rowCheck[0] > 1)
                {
            	    $success = 1;//DELETE ONLY TOOTHEXAM
              	    $sqlExamDel = "delete from TOOTHEXAM where EXAMID = '$item'";
                	$queryExamDel = ibase_prepare($trans, $sqlExamDel);
                	ibase_execute($queryExamDel);		
                	ibase_free_query($queryExamDel);
                }
                else
                {
                    //check is exist PROCEDURES WITH INVOICES or ONLY PROCEDURES
            	    $success = 2;//ONLY PROCEDURES WITHOUT INVOICES
                    $sqlProcCheck = "select
                            (select invoices.invoicenumber from invoices where invoices.id = treatmentproc.invoiceid),  
                            (select sum(accountflow.summ) from accountflow where accountflow.oninvoice_fk = treatmentproc.invoiceid),
                            (select invoices.createdate from invoices where invoices.id = treatmentproc.invoiceid),
                            treatmentproc.name
                            
                            from treatmentproc
                            left join diagnos2 on diagnos2.id = treatmentproc.diagnos2
                            
                            where diagnos2.treatmentcourse = (select TREATMENTCOURSE_FK from toothexam where examid = $item)";
                    $queryProcCheck = ibase_prepare($trans, $sqlProcCheck);
                    $resultProcCheck = ibase_execute($queryProcCheck);
                    $isInvoice = false;
                    while($rowProcCheck = ibase_fetch_row($resultProcCheck))
                    {
                        if($rowProcCheck[0] != null) //PROCEDURES WITH INVOICES
                        {
                            
                            $invoice = new stdClass();
                            $invoice->invNumber = $rowProcCheck[0];
                            $invoice->invDate = $rowProcCheck[2];
                            $invoice->trtName = $rowProcCheck[3];
                            $invoice->invCash = false;
                            
                            if($success != 4)
                            {
                                $success = 3;
                            }
                            if($rowProcCheck[1] != null && $rowProcCheck[1] > 0) //PROCEDURES WITH INVOICES AND CASH
                            {
                                $success = 4;
                                $invoice->invCash = true;
                            }
                            
                            $successObj->invoices[] = $invoice;
                        }
                    }
                }
			}
		}
        $successObj->success = $success;
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
        return $successObj;
  }
  
  
    /**
  * @param int $item
  * @param string $passForDelete
  * @param int $securityCode
  * @return int
  */
  public function deleteLastExam($item, $passForDelete, $securityCode)
  { 
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        /*
            $securityCode
            2 - pass for treatment
            3 - pass for account
            4 - pass for account
            
        */
        if($securityCode == 2)
        {
            $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
        }
        else if($securityCode == 3)
        {
            $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
        }
        else if($securityCode == 4)
        {
            $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
        }
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
        
        
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
           	    $success = 1;
                
                
           	    $sqlTreatDel = "delete from treatmentcourse where treatmentcourse.id =
                                (select first 1 toothexam.treatmentcourse_fk from toothexam where toothexam.examid = '$item')";
               	$queryTreatDel = ibase_prepare($trans, $sqlTreatDel);
               	ibase_execute($queryTreatDel);		
               	ibase_free_query($queryTreatDel);
                
                
           	    $sqlExamDel = "delete from TOOTHEXAM where EXAMID = '$item'";
               	$queryExamDel = ibase_prepare($trans, $sqlExamDel);
               	ibase_execute($queryExamDel);		
               	ibase_free_query($queryExamDel);
			}
            else
            {
           	    $success = 0;
            }
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
        return $success;
  }
  
  
  
  
  
  
  
  
  
 
    /**
    * @param int $id
    * @param string $nowdate
    * @param boolean $isLite
    * @return TeethExamModuleToothSOut
    */    
    public function getExam($id, $nowdate, $isLite) //sqlite
    { 
    	$connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText = "select 
                        PATIENTS.FIRSTNAME, 
                        PATIENTS.LASTNAME, 
                        PATIENTS.CARDNUMBER, 
                        PATIENTS.CARDBEFORNUMBER,
                        PATIENTS.CARDAFTERNUMBER, 
                        TOOTHEXAM.EXAM, 
                        TOOTHEXAM.EXAMDATE, 
                        TOOTHEXAM.EXAMID,
                        DOCTORS.LASTNAME || ' ' || DOCTORS.FIRSTNAME, 
                        TOOTHEXAM.CHANNELSEXAM, 
                        TOOTHEXAM.COMPLAINTS, 
                        TOOTHEXAM.CHECKUP, 
                        assistants.LASTNAME || ' ' || assistants.FIRSTNAME, 
                        TOOTHEXAM.bitetype,
                        datediff(year, patients.birthday, date 'now'),
                        toothexam.afterflag,
                        toothexam.treatmentcourse_fk,
                        treatmentcourse.createdate,
                        
                        TOOTHEXAM.DISEASEHISTORY,
                        TOOTHEXAM.TREATMENTPLAN,
                        TOOTHEXAM.EXAMPLAN
                   from PATIENTS 
                   left join TOOTHEXAM
                   on TOOTHEXAM.ID = PATIENTS.ID
                   left join treatmentcourse
                   on treatmentcourse.id = toothexam.treatmentcourse_fk
                   left join DOCTORS
                   on DOCTORS.ID = TOOTHEXAM.DOCTORID
                   left join assistants
                   on assistants.id = toothexam.assistantid
                  where PATIENTS.ID = $id
                  order by treatmentcourse.createdate desc, toothexam.treatmentcourse_fk desc,  TOOTHEXAM.EXAMID desc";
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
    	$rows = array();
        $retdata = new TeethExamModuleToothSOut;
        while ($row = ibase_fetch_row($result))
    	{
    	    if($row[7] != null)
            {
        	    $obj = new TeethExamModuleToothOut;
                $obj->exam = $row[5];
                $obj->examdate = $row[6];
                $obj->examid = $row[7];
                $obj->doctor = $row[8];
                $txtBlob = null;
                    $blob_info = ibase_blob_info($row[9]);
                    if ($blob_info[0]!=0)
                    {
                        $blob_hndl = ibase_blob_open($row[9]);
                        $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                        ibase_blob_close($blob_hndl);
                        
                    }
                $obj->complaints = $row[10];
                $obj->checkup = $row[11];
                $obj->assistant = $row[12];
                $obj->bitetype = $row[13];
                $obj->channelsexam=$txtBlob;
                $obj->afterflag = $row[15];
                $obj->course_fk = $row[16];
                $obj->course_date = $row[17];
                $obj->diseasehistory = $row[18];
                $obj->treatmentplan = $row[19];
                $obj->examplan = $row[20];
 		        $retdata->ToothOut[] = $obj;
            }
            
            $retdata->fname = $row[0];
            $retdata->lname = $row[1];
            $retdata->kartnum = "{$row[3]}{$row[2]}{$row[4]}";
            $retdata->birthyears = $row[14];
    	}	
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
        if($isLite==false)
        {
           	//**************** DOCTORS LIST START ****************
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        	$QueryText = "select ID, LASTNAME || ' ' || FIRSTNAME, SPECIALITY 
                            FROM DOCTORS 
                            WHERE doctors.isfired is null or doctors.isfired != 1";
        	$query = ibase_prepare($trans, $QueryText);
        	$result=ibase_execute($query);
        	$rows = array();
        
            while ($row = ibase_fetch_row($result))
        	{
        	    $obj = new TeethExamModuleDocForTooth;
                $obj->docid = $row[0];
                $obj->name = $row[1];
                $obj->spec=$row[2];
        		$retdata->docList[] = $obj;
        	}	
        	ibase_commit($trans);
        	ibase_free_query($query);
        	ibase_free_result($result);
           	//**************** DOCTORS LIST END ****************
            
            //**************** ASSISTANT LIST START ************************
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        	$QueryText = "select ID, LASTNAME || ' ' || FIRSTNAME FROM ASSISTANTS 
                            WHERE ASSISTANTS.isfired is null or ASSISTANTS.isfired != 1";
        	$query = ibase_prepare($trans, $QueryText);
        	$result=ibase_execute($query);
        	$rows = array();
        
            while ($row = ibase_fetch_row($result))
        	{
        	    $obj = new TeethExamModuleAssistantForTooth;
                $obj->assid = $row[0];
                $obj->name = $row[1];
        		$retdata->assistantsList[] = $obj;
        	}	
        	ibase_commit($trans);
        	ibase_free_query($query);
        	ibase_free_result($result);
        }
        //**************** ASSISTANT LIST END ************************
    	$connection->CloseDBConnection();		
    	return $retdata;       
    } 
 

    
    /*
    reports
    */
    
    /**
    * @param string $examId
    * @param string $examByteArray
    * @param boolean $isEmailSend
    * @param string $docType
    * @return string
    */    
    public function createReport($examId, $examByteArray, $isEmailSend, $docType ) 
    {
        $res=dirname(__FILE__).'/tbs/res/';
        
        if(($docType == 'docx') && (!file_exists($res.'ToothExam.docx')) )
        {
            return "TEMPLATE_NOTFOUND";
        }
        else if(($docType == 'odt') && (!file_exists($res.'ToothExam.odt') ) )
        {
            return "TEMPLATE_NOTFOUND";
        }
        
        if( ($docType != 'docx' && $docType != 'odt' )  ){
            return "TEMPLATE_NOTFOUND";  // введен неизвестный формат
        }
        
        
        // получение $patientId через переменную examId
        $connection = new Connection();
        $connection->EstablishDBConnection();
        
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = "select ID from toothexam where examId = $examId";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        
        if ($row = ibase_fetch_row($result))
        {
            $patientId= $row[0];
        }
        ibase_free_query($query);
        ibase_free_result($result);
        
        //=========================================
        
        
        $toothid=unserialize(toothid);
        
        
        
        $ToothExam = new ToothExam($examId, $trans);
        $exam=$ToothExam->EXAM;
        $bitetype = $ToothExam->BITETYPE;
        $diag_43_array=getSortTeethF43($exam, $bitetype, unserialize(toothid), unserialize(dentdiswordall));
        
            
        $ClinicRegistrationData = new PrintDataModule_ClinicRegistrationData(false, $trans);
        $Patient = new PrintDataModule_PatientData($patientId, $trans);
        $Data = new stdclass();
        $Data->ImagePath = dirname(__FILE__).'/docs/exam.png';
        
        // print("<div style='height:400px; overflow:auto;'><pre>");
        // print_r($f43);
        // print("</pre></div>");
            
        file_put_contents(dirname(__FILE__).'/docs/exam.png', base64_decode($examByteArray) );

        //=================================================
        $TBS = new clsTinyButStrong();
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        
        $TBS->LoadTemplate($res.'ToothExam.'.$docType, OPENTBS_ALREADY_XML);
        
        
        // для предотвращения возможной ошибки генерации в модуле tcpdf 
        // когда зуб обследован, но не выявлено никаких болезней - тогда таблица 
        // будет пустой и будет создаватся тег <tr> в котором не будет ни одного тега <td>
        if(count($diag_43_array)==0 ){
            $diag_43_array[] = array("ID"=>"", "DIAG"=>"");
        }

        $TBS->MergeField("ClinicRegistrationData", $ClinicRegistrationData);
        $TBS->MergeField("Patient", $Patient);
        $TBS->MergeBlock("Diagnoses", $diag_43_array);
        $TBS->MergeField("ToothExam", $ToothExam);
        $TBS->MergeField("Data", $Data);


        $patient_name_file= translit(str_replace(' ','_',$Patient->SHORTNAME) );
        $patient_name_file= preg_replace('/[^A-Za-z0-9_]/', '', $patient_name_file);
        
        $file_name = uniqid().'_eid_'.$examId.'_ToothExam_'. $patient_name_file.'.'.$docType; 
        $file_name = translit(str_replace(' ','_',$file_name) );

        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$file_name );
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        
        
        if($isEmailSend == false) //print
        {
            return $file_name;
        }
        else //$isEmailSend == false -> email
        {
              /*
              проверемя емал пациента 
                если его нет -> возвращаем в флекс PATIENT_EMAIL_EMPTY
                а если неправильный (невалидный) тогда PATIENT_EMAIL_WRONG
                если есть емаил
                  генерируем документ
                  сохраняем на стороне сервера
                  отправляем на емаил пациента
                  удаляем документ
                  возвращаем PATIENT_EMAIL_SENDED
                  если взникает ошибка при отправке письма, то 
                  возвращаем строку SMTP_ERROR
              */
            
            $email = $Patient->EMAIL;
          
          
            if( $email!=null && $email!=""  )
            {
              
                if( $this->validate_email($email)==0 )
                {
                    deleteFileUtils($file_name);
                    return "PATIENT_EMAIL_WRONG";
                }

                //require_once (dirname(__FILE__).'/../../ZendFramework/library/Zend/Mail.php');
                //require_once (dirname(__FILE__).'/../../ZendFramework/library/Zend/Mail/Transport/Smtp.php');
                require_once 'Zend/Mail.php';
                require_once 'Zend/Mail/Transport/Smtp.php';
                
                
                // получение настроек smpt соединения с БД и проверка их на пустые значения
                // если есть хоть одно пустое значения возврвщаем строку SMTP_NOTCONFIGURATED
                $emailService = new EmailService();
                $settings = $emailService->getSettings();
                foreach($settings as $key => $value){
                    if($value==null || $value == '' ){
                        deleteFileUtils($file_name);
                        return 'SMTP_NOTCONFIGURATED';
                    }
                }
              
                $smtpOptions = array(
                    'auth'      => 'login',
                    'username'  => $settings->EmailService_email,
                    'password'  => $settings->EmailService_emailPassword,
                    'ssl'       => 'ssl',
                    'port'      => $settings->EmailService_smtpPortServer
                );
                
                $mailTransport = new Zend_Mail_Transport_Smtp($settings->EmailService_smtpServer, $smtpOptions);
                
                $mail = new Zend_Mail('utf-8');
                
                try{
                    $mail->addTo($email, $Patient->SHORTNAME);
                    $mail->setFrom($settings->EmailService_email, $ClinicRegistrationData->NAME);
                    $mail->setSubject('Карта зубів');
                    $mail->addHeader('Importance' , 'high');
                    
                    // обрабатываем html шаблон
                    
                    $htmlPattern = file_get_contents($res.'ToothExamHtml.html');
                    
                    $htmlPatternLoop=$this->find_loop($htmlPattern);
                    $htmlPatternLoopRes='';
                    
                    
                    
                    
                    foreach($diag_43_array as $diag_43_cell) 
                    {
                       $dc = new stdclass(); 
                       $dc->ID = $diag_43_cell["ID"] ; 
                       $dc->DIAG = $diag_43_cell["DIAG"] ; 
                       
                       
                       $htmlPatternLoopRes.=$this->page_build($htmlPatternLoop, $dc, '$Diagnos');
                    }
                    $htmlPattern=str_replace("<!--(-->$htmlPatternLoop<!--)-->", $htmlPatternLoopRes, $htmlPattern);
                    
                    
                    $htmlPattern=$this->page_build($htmlPattern, $ClinicRegistrationData, '$ClinicRegistrationData');
                    $htmlPattern=$this->page_build($htmlPattern, $Patient, '$Patient');
                    $htmlPattern=$this->page_build($htmlPattern, $ToothExam, '$ToothExam');
                    
                    
                    
                    $mail->setBodyHtml($htmlPattern, 'UTF-8', Zend_Mime::ENCODING_8BIT);
                    $mail->setBodyText('Данный документ был сгенерирован по запросу из программы "Зубная Фея"');
                    
                    //вложение картинки для отображения в HTML документе
                    $mail->setType(Zend_Mime::MULTIPART_RELATED);
                    $img = $mail->createAttachment(file_get_contents(dirname(__FILE__).'/docs/exam.png'));
                    $img->type = 'image/png';  

                    $img->disposition = Zend_Mime::DISPOSITION_INLINE;  
                    $img->encoding = Zend_Mime::ENCODING_BASE64;  
                    $img->id = 'exam.png';

                    // вложение документа
                    $document = new Zend_Mime_Part(file_get_contents(dirname(__FILE__).'/docs/'.$file_name));
                    if($docType == 'docx'){
                        $document->type = 'application/docx';
                    }elseif($docType == 'odt'){
                        $document->type = 'application/odt';
                    }
                    
                    $document->disposition = Zend_Mime::DISPOSITION_INLINE;
                    $document->encoding = Zend_Mime::ENCODING_BASE64;
                    $document->filename =  substr( $file_name, 14);  // только в имени письма удаляем Uid
                    //$document->id = md5(time());
                    $mail->addAttachment($document);
              
                    $mail->send($mailTransport);
                }catch(Exception $exc){
                    deleteFileUtils($file_name);
                    return 'SMTP_ERROR';
                }
              
                deleteFileUtils($file_name);
                return "PATIENT_EMAIL_SENDED";
              
            }
            else
            {
                deleteFileUtils($file_name);
                return "PATIENT_EMAIL_EMPTY";
            }
        }
    }
    /** 
    * @param string $file_name
    * @return boolean
    */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);
    }
    
    
    function validate_email($email)
    {

       // Create the syntactical validation regular expression
       $regexp = "^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$";

       // Presume that the email is invalid
       $valid = 0;

       // Validate the syntax
       if (eregi($regexp, $email))
       {
          list($username,$domaintld) = split("@",$email);
          // Validate the domain
          if (getmxrr($domaintld,$mxrecords))
             $valid = 1;
       } else {
          $valid = 0;
       }

       return $valid;

    }
    
    
    
    private function page_build($page,$std,$stdName)
    {
        foreach (array_keys(get_object_vars($std))as $el) {

            if(strpos(strval($page),strval($stdName.'.'.$el.'<'))){
                $page = str_replace($stdName.'.'.$el.'<',$std->$el.'<',$page);
            }
            if(strpos(strval($page),strval($stdName.'.'.$el.' '))){
                $page = str_replace($stdName.'.'.$el.' ',$std->$el.' ',$page);
            }
        }
        
        return $page;
    }

    /*
   public static const TYPE_COMPLAINTS:int=0;
   public static const TYPE_OBJECTIVE_INVESTIGATION:int=1;
   public static const TYPE_CURRENT_DISEASE:int=2;
    */
    /**
  * @param string $toothexamId
  * @param string $text
  * @param string $type
  * @return boolean
  */
  public function saveAdditionalForToothExam($toothexamId, $text, $type)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$update_sql = "update toothexam set
                toothexam.".$type." = '".safequery($text)."'
                where toothexam.examid = $toothexamId";  
        $update_query = ibase_prepare($trans, $update_sql);
		ibase_execute($update_query);
		ibase_free_query($update_query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  }
    
    //**********************************************************************    UTILITIES   *********
    
    private function find_loop($page)
    {
        preg_match_all("/<\!--\(-->(.|\n?)*<\!--\)-->/",$page,$matches);    
        $loop='';
        foreach($matches[0] as $match){
            if(strlen($loop)<strlen($match)){
                $loop = $match;
            }
        }
        
        return substr($loop,8,strlen($loop)-16);
    }


    private function var_clear($page)
    {
        $page = preg_replace('/\\$[A-Za-z0-9_\.]+/', '', $page); 
        
        return $page;
    }
    
    
}


?>
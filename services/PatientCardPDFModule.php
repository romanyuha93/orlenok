<?php

require_once "Connection.php";

class PatientCardPDFModulePatientCardData
{
    //èíôî î êëèíèê ñòàðò
   /**
   * @var string
   */
	var $CLINICNAME;
   /**
   * @var string
   */
	var $CLINICLEGALNAME;
   /**
   * @var string
   */
	var $CLINICADDRESS; 
    //èíôî î êëèíèê êîíåö
    
    //èíôî î ïàöèåíòå ñòàðò
    /**
    * @var string
    */
    var $PATIENTCARDNUMBER;
    
    /**
    * @var string
    */
    var $PATIENTCARDDATE;
    
    /**
    * @var string
    */
    var $PATIENTFIRSTNAME;
     
    /**
    * @var string
    */
    var $PATIENTMIDDLENAME;
     
    /**
    * @var string
    */
    var $PATIENTLASTNAME;
    
    /**
    * @var string
    */
    var $PATIENTBIRTHDAY;    
   
    /**
    * @var string
    */
    var $PATIENTSEX;
         
    /**
    * @var string
    */
    var $PATIENTPOSTALCODE;
    
    /**
    * @var string
    */
    var $PATIENTCOUNTRY;
    
    /**
    * @var string
    */
    var $PATIENTSTATE;
    
    /**
    * @var string
    */
    var $PATIENTCITY;
    
    /**
    * @var string
    */
    var $PATIENTRAGION;
    
    /**
    * @var string
    */
    var $PATIENTADDRESS;
    
    /**
    * @var string
    */
    var $PATIENTTELEPHONENUMBER;
     
    /**
    * @var string
    */
    var $PATIENTMOBILENUMBER;  
    
    /**
    * @var string
    */
    var $PATIENTEMPLOYMENTPLACE;
    
    /**
    * @var string
    */
    var $PATIENTJOB;
     
    /**
    * @var string
    */
    var $PATIENTWORKPHONE;
    //èíôî î ïàöèåíòå êîíåö
    
    //èíôî î òåñòå ñòàðò
    /**
    * @var string
    */
    var $PATIENTTESTTESTDATE;
    
    /**
    * @var string
    */
    var $PATIENTTESTCARDIACDISEASE;
    
    /**
    * @var string
    */
    var $PATIENTTESTKIDNEYDISEASE;
    
    /**
    * @var string
    */
    var $PATIENTTESTLIVERDISEASE;
    
    /**
    * @var string
    */
    var $PATIENTTESTOTHERDISEASE;
    
    /**
    * @var string
    */
    var $PATIENTTESTARTERIALPRESSURE;
    
    /**
    * @var string
    */
    var $PATIENTTESTRHEUMATISM;
    
    /**
    * @var string
    */
    var $PATIENTTESTHEPATITIS;
    
    /**
    * @var string
    */
    var $PATIENTTESTDIABETES;
    
    /**
    * @var string
    */
    var $PATIENTTESTFIT;
    
    /**
    * @var string
    */
    var $PATIENTTESTBLEEDING;    

    /**
    * @var string
    */    
    var $PATIENTTESTALLERGY;
    
    /**
    * @var string
    */
    var $PATIENTTESTPREGNANCY;
    
    /**
    * @var string
    */
    var $PATIENTTESTMEDICATIONS;
    
    /**
    * @var string
    */
    var $PATIENTTESTBLOODGROUP;
    
    /**
    * @var string
    */
    var $PATIENTTESTRHESUSFACTOR;
    
    /**
    * @var string
    */
    var $PATIENTTESTMOUTHULCER;
    
    /**
    * @var string
    */
    var $PATIENTTESTFUNGUS;
    
    /**
    * @var string
    */
    var $PATIENTTESTFEVER;
    
    /**
    * @var string
    */
    var $PATIENTTESTTHROATACHE;
    
    /**
    * @var string
    */
    var $PATIENTTESTLYMPHNODES;
    
    /**
    * @var string
    */
    var $PATIENTTESTREDSKINAREA;
    
    /**
    * @var string
    */
    var $PATIENTTESTHYPERHIDROSIS;
    
    /**
    * @var string
    */
    var $PATIENTTESTDIARRHEA;
    
    /**
    * @var string
    */
    var $PATIENTTESTAMNESIA;
    
    /**
    * @var string
    */
    var $PATIENTTESTWEIGHTLOSS;
    
    /**
    * @var string
    */
    var $PATIENTTESTHEADACHE;
    
    /**
    * @var string
    */
    var $PATIENTTESTAIDS;
    
    /**
    * @var string
    */
    var $PATIENTTESTWORKWITHBLOOD;
    
    /**
    * @var string
    */
    var $PATIENTTESTNARCOMANIA;   
    //èíôî î òåñòå êîíåö
    
    //PatientCardPDFModuleExam
    //PatientCardPDFModuleAmbcard
    //PatientCardPDFModuleHygcontrol
    //PatientCardPDFModuleIllHistoryRecord
    
   /**
   * @var PatientCardPDFModuleExam[]
   */
	var $TOOTHEXAM;
   /**
   * @var PatientCardPDFModuleAmbcard[]
   */
	var $AMBCARD;
   /**
   * @var PatientCardPDFModuleHygcontrol[]
   */
	var $HYGCONTROL;
   /**
   * @var PatientCardPDFModuleIllHistoryRecord[]
   */
	var $ILLHISTORY;  
}

class PatientCardPDFModuleExam
{
    /**
    * @var string
    */
    var $DOCTORSHORTNAME;
    
    /**
    * @var string
    */
    var $EXAM;

    /**
    * @var string
    */
    var $EXAMDATE;
}

class PatientCardPDFModuleAmbcard
{
     /**
    * @var string
    */
    var $OCCLUSION;
    
    /**
    * @var string
    */
    var $HYGIENE;

     /**
    * @var string
    */
    var $ANALYSIS;

    /**
    * @var string
    */
    var $VITASCALE;
      
}

class PatientCardPDFModuleHygcontrol
{
    /**
    * @var string
    */
    var $CONTROLDATE;
    
    /**
    * @var string
    */
    var $SHORTNAME;

     /**
    * @var string
    */
    var $ISCONTROL;
}
 
class PatientCardPDFModuleIllHistoryRecord
{
    /**
    * @var string
    */
    var $EXAMDATE;

    /**
    * @var string
    */
    var $DATEOFRECORD;   

    /**
    * @var string
    */
    var $RECORDTYPE;

    /**
    * @var string
    */
    var $HISTORYTEXT;   

    /**
    * @var string
    */
    var $DOCTORSHORTNAME;  
}


class PatientCardPDFModule
{	
    /**  
    * @param PatientCardPDFModulePatientCardData $p1
    * @param PatientCardPDFModuleExam $p2
    * @param PatientCardPDFModuleAmbcard $p3
    * @param PatientCardPDFModuleHygcontrol $p4
    * @param PatientCardPDFModuleIllHistoryRecord $p5
    * 
    * @return void
    */    
	public function registertypes($p1, $p2, $p3, $p4, $p5) 
	{} 
    
 	/** 
    * @param int $PatID
 	* @return PatientCardPDFModulePatientCardData[]
	*/ 
     public function getPatientCardData($PatID) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
/////////////////////////////////////////////////////////////////////
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select NAME, LEGALNAME, ADDRESS from CLINICREGISTRATIONDATA";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$outObj = new PatientCardPDFModulePatientCardData;
            $row = ibase_fetch_row($result);
            $outObj->CLINICNAME = $row[0];
            $outObj->CLINICLEGALNAME = $row[1];
            $outObj->CLINICADDRESS = $row[2];
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select 
                    CARDNUMBER,
                    CARDBEFORNUMBER,
                    CARDAFTERNUMBER,
                    CARDDATE,
                    FIRSTNAME,
                    MIDDLENAME,
                    LASTNAME,
                    BIRTHDAY,
                    SEX,
                    POSTALCODE,
                    COUNTRY,
                    STATE,
                    CITY,
                    RAGION,
                    ADDRESS,
                    TELEPHONENUMBER,
                    MOBILENUMBER,
                    EMPLOYMENTPLACE,
                    JOB,
                    WORKPHONE 
                from PATIENTS where ID='$PatID'";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        
        $row = ibase_fetch_row($result);
        $outObj->PATIENTCARDNUMBER = "{$row[1]}{$row[0]}{$row[2]}";
        $outObj->PATIENTCARDDATE = $row[3];
        $outObj->PATIENTFIRSTNAME = $row[4];
        $outObj->PATIENTMIDDLENAME = $row[5];
        $outObj->PATIENTLASTNAME = $row[6];
        $outObj->PATIENTBIRTHDAY = $row[7];    
        $outObj->PATIENTSEX = $row[8];
        $outObj->PATIENTPOSTALCODE = $row[9];
        $outObj->PATIENTCOUNTRY = $row[10];
        $outObj->PATIENTSTATE = $row[11];
        $outObj->PATIENTCITY = $row[12];
        $outObj->PATIENTRAGION = $row[13];
        $outObj->PATIENTADDRESS = $row[14];
        $outObj->PATIENTTELEPHONENUMBER = $row[15];
        $outObj->PATIENTMOBILENUMBER = $row[16];  
        $outObj->PATIENTEMPLOYMENTPLACE = $row[17];
        $outObj->PATIENTJOB = $row[18];
        $outObj->PATIENTWORKPHONE = $row[19];

		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////

        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText ="select 
                    TESTDATE,
                    CARDIACDISEASE,
                    KIDNEYDISEASE,
                    LIVERDISEASE,
                    OTHERDISEASE,
                    ARTERIALPRESSURE,
                    RHEUMATISM,
                    HEPATITIS,
                    DIABETES,
                    FIT,
                    BLEEDING,
                    ALLERGY,
                    PREGNANCY,
                    MEDICATIONS,
                    BLOODGROUP,
                    RHESUSFACTOR,
                    MOUTHULCER,
                    FUNGUS,
                    FEVER,
                    THROATACHE,
                    LYMPHNODES,
                    REDSKINAREA,
                    HYPERHIDROSIS,
                    DIARRHEA,
                    AMNESIA,
                    WEIGHTLOSS,
                    HEADACHE,
                    AIDS,
                    WORKWITHBLOOD,
                    NARCOMANIA  
                from PATIENTSTESTCARDS where PATIENTID='$PatID'";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        $row = ibase_fetch_row($result);
        $outObj->PATIENTTESTTESTDATE = $row[0];
        $outObj->PATIENTTESTCARDIACDISEASE = $row[1];
        $outObj->PATIENTTESTKIDNEYDISEASE = $row[2];
        $outObj->PATIENTTESTLIVERDISEASE = $row[3];
        $outObj->PATIENTTESTOTHERDISEASE = $row[4];
        $outObj->PATIENTTESTARTERIALPRESSURE = $row[5];
        $outObj->PATIENTTESTRHEUMATISM = $row[6];
        $outObj->PATIENTTESTHEPATITIS = $row[7];
        $outObj->PATIENTTESTDIABETES = $row[8];
        $outObj->PATIENTTESTFIT = $row[9];
        $outObj->PATIENTTESTBLEEDING = $row[10];
            $txtBlob = null;
            $blob_info = ibase_blob_info($row[11]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[11]);
                $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
            }
        $outObj->PATIENTTESTALLERGY=$txtBlob;
        $outObj->PATIENTTESTPREGNANCY = $row[12];
            $txtBlob = null;
            $blob_info = ibase_blob_info($row[13]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[13]);
                $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
            }
        $outObj->PATIENTTESTMEDICATIONS=$txtBlob;
        $outObj->PATIENTTESTBLOODGROUP = $row[14];
        $outObj->PATIENTTESTRHESUSFACTOR = $row[15];
        $outObj->PATIENTTESTMOUTHULCER = $row[16];
        $outObj->PATIENTTESTFUNGUS = $row[17];
        $outObj->PATIENTTESTFEVER = $row[18];
        $outObj->PATIENTTESTTHROATACHE = $row[19];
        $outObj->PATIENTTESTLYMPHNODES = $row[20];
        $outObj->PATIENTTESTREDSKINAREA = $row[21];
        $outObj->PATIENTTESTHYPERHIDROSIS = $row[22];
        $outObj->PATIENTTESTDIARRHEA = $row[23];
        $outObj->PATIENTTESTAMNESIA = $row[24];
        $outObj->PATIENTTESTWEIGHTLOSS = $row[25];
        $outObj->PATIENTTESTHEADACHE = $row[26];
        $outObj->PATIENTTESTAIDS = $row[27];
        $outObj->PATIENTTESTWORKWITHBLOOD = $row[28];
        $outObj->PATIENTTESTNARCOMANIA = $row[29];

		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText ="select
                        EXAM,
                        EXAMDATE
                    from TOOTHEXAM
                    where ID = $PatID
                    order by EXAMID desc";
 		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
        while ($row = ibase_fetch_row($result))
        {
    	    $toothobj = new PatientCardPDFModuleExam;
            $toothobj->EXAM = $row[0];
            $toothobj->EXAMDATE = $row[1];
            $outObj->TOOTHEXAM[] = $toothobj;
        }
        
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select 
                OCCLUSION, 
                HYGIENE, 
                ANALYSIS, 
                VITASCALE  
                    from AMBCARD 
                    where PATIENTID = '$PatID'";
 		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        
        while ($row = ibase_fetch_row($result))
        {
            $ambobj = new PatientCardPDFModuleAmbcard;
            $ambobj->OCCLUSION = $row[0]; 
            $ambobj->HYGIENE = $row[1];
            $ambobj->ANALYSIS = $row[2];
            $ambobj->VITASCALE = $row[3];
            $outObj->AMBCARD[] = $ambobj;
        }
        
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////

        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select 
                HYGCONTROL.CONTROLDATE, 
                HYGCONTROL.DOCTORID, 
                DOCTORS.SHORTNAME, 
                HYGCONTROL.ISCONTROL 
                     from HYGCONTROL 
                     INNER JOIN DOCTORS
                     ON HYGCONTROL.DOCTORID = DOCTORS.ID
                     WHERE HYGCONTROL.PATIENTID='$PatID' order by CONTROLDATE";
 		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        
        while ($row = ibase_fetch_row($result))
        {
            $hygobj = new PatientCardPDFModuleHygcontrol;
            $hygobj->CONTROLDATE = $row[0]; 
            $hygobj->SHORTNAME = $row[2];
            $hygobj->ISCONTROL = $row[3];
            $outObj->HYGCONTROL[] = $hygobj;
        }
        
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////
        
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                
		$QueryText =  "select 
                    illhistory.doctorid, 
                    illhistory.examdate,
                    illhistory.dateofrecord, 
                    illhistory.recordtype, 
                    illhistory.historytext,
                    doctors.shortname
                        from doctors
   						inner join illhistory on (doctors.id = illhistory.doctorid)
						where (illhistory.patientid = '$PatID')";
                        
 		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        
        while ($row = ibase_fetch_row($result))
        {
            $illobj = new PatientCardPDFModuleIllHistoryRecord;
            $illobj->EXAMDATE = $row[1]; 
            $illobj->DATEOFRECORD = $row[2];
            $illobj->RECORDTYPE = $row[3];
            $illobj->HISTORYTEXT = $row[4];
            $illobj->DOCTORSHORTNAME = $row[5];
            $outObj->ILLHISTORY[] = $illobj;
        }
        
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
/////////////////////////////////////////////////////////////////////
        
		$connection->CloseDBConnection();
		return $outObj;  
    }
    
}

?>

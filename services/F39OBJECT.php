<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
//$f37=new F37OBJECT(1,"2013-06-19");
//print_r($f37);
class F39OBJECT
{	
   /**
	* @var PrintDataModule_DoctorData
	*/
   var $DOCTOR_DATA;
   
   /**
	* @var PrintDataModule_DateUA
	*/
   var $STARTDATE;
   
      /**
	* @var PrintDataModule_DateUA
	*/
   var $ENDDATE;
   
    /**
    * @var PrintDataModule_ClinicRegistrationData
    */
    var $CLINIC_DATA;
    
    /**
    * @var string[][]
    */
    var $TABLE_DATA; 
    
    function __construct($doctor_id, $startDate, $endDate, $form, $trans=null)     
    { 
        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        

        $this->DOCTOR_DATA=new PrintDataModule_DoctorData($doctor_id,$trans);
        $this->STARTDATE=new PrintDataModule_DateUA($startDate);
        $this->ENDDATE=new PrintDataModule_DateUA($endDate);
        $this->CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(false,$trans);
        $this->TABLE_DATA=array();        
        $startTime=strtotime($startDate); 
        $endTime=strtotime($endDate); 
        $currTime=$startTime;
        while($currTime<=$endTime)
        {
        
        $rec=F39OBJECT::getForm039Record($doctor_id, date("Y-m-d",$currTime)." 0:00:00", date("Y-m-d",$currTime)." 23:59:59", $form, $trans);
         if($rec['3']>0||$rec['2']>0)
         {
             $this->TABLE_DATA[]=$rec;
         }

         $currTime+=3600*24;
            
        }
        if(count($rec)>0)
        {
            $rec=F39OBJECT::getForm039Record($doctor_id, date("Y-m-d",$startTime)." 0:00:00", date("Y-m-d",$endTime)." 23:59:59", $form, $trans);
            $rec["1"]='';
            $this->TABLE_DATA[]=$rec;
        }

        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }  
    }
    
    public static function getForm039Record($doctor_id, $startDate, $endDate, $form, $trans=null)
    {
        $res=array();
         if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
            $QueryText = "select 24*sum(cast(t.taskdate||' '||t.endoftheinterval as timestamp)-cast(t.taskdate||' '||t.beginoftheinterval as timestamp)) from tasks t
                            where
                            cast(t.taskdate||' '||t.beginoftheinterval as timestamp)>='$startDate'
                            and cast(t.taskdate||' '||t.endoftheinterval as timestamp)<='$endDate'
                            and t.doctorid=$doctor_id";

            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);
        
            if($row = ibase_fetch_row($result))
        	{	
                $res['2']=round($row[0],1); 

        	}  
           
        	ibase_free_query($query);
        	ibase_free_result($result);

            
            $QueryText = "select f.number,sum(tp.proc_count) from f39 f
                            left join f39tproc ftp on (ftp.f39id=f.id)
                            left join treatmentproc tp on (tp.id=ftp.treatmentprocid)
                            where f.form=$form
                            and f.number is not null
                            and ftp.unsigned=0
                            and (f.TYPE=0 or f.TYPE=1)
                            and tp.dateclose>='$startDate'
                            and tp.dateclose<='$endDate'
                            and tp.doctor=$doctor_id
                            group by f.number";
            //echo $QueryText;
            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);
        	
            while($row = ibase_fetch_row($result))
        	{	
                $res[$row[0]]=$row[1]; 
        
        	}  
        
        	ibase_free_query($query);
        	ibase_free_result($result);

             
            
            $QueryText = "select f.number,sum(tp.proc_count) from f39 f
                            left join f39tproc ftp on (ftp.f39id=f.parrent)
                            left join treatmentproc tp on (tp.id=ftp.treatmentprocid)
                            left join patients p on (p.id=tp.patient_fk)
                            where f.form=$form
                            and f.number is not null
                            and ftp.unsigned=0
                            and f.TYPE=-1
                            and tp.dateclose>='$startDate'
                            and tp.dateclose<='$endDate'
                            and tp.doctor=$doctor_id
                            and (Select * from AGE(p.birthday,tp.dateclose))<18
                            group by f.number";
            //echo $QueryText;
            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);
        	
            while($row = ibase_fetch_row($result))
        	{	
                $res[$row[0]]=$row[1]; 
        
        	}  
        
        	ibase_free_query($query);
        	ibase_free_result($result);
             
            
            
            $QueryText = "select f.number,sum(tp.proc_count) from f39 f
                            left join f39tproc ftp on (ftp.f39id=f.id)
                            left join treatmentproc tp on (tp.id=ftp.treatmentprocid)
                            left join patients p on (p.id=tp.patient_fk)
                            where f.form=$form
                            and f.number is not null
                            and ftp.unsigned=0
                            and f.TYPE=2
                            and tp.dateclose>='$startDate'
                            and tp.dateclose<='$endDate'
                            and tp.doctor=$doctor_id
                            and (Select * from AGE(p.birthday,tp.dateclose))>17
                            group by f.number";
            //echo $QueryText;
            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);
        	
            while($row = ibase_fetch_row($result))
        	{	
                $res[$row[0]]=$row[1]; 
        
        	}  
        
        	ibase_free_query($query);
        	ibase_free_result($result);
            
            $QueryText = "select f.number,sum(tp.proc_count) from f39 f
                            left join f39tproc ftp on (ftp.f39id=f.parrent)
                            left join treatmentproc tp on (tp.id=ftp.treatmentprocid)
                            left join patients p on (p.id=tp.patient_fk)
                            where f.form=$form
                            and f.number is not null
                            and ftp.unsigned=0
                            and f.TYPE=-2
                            and tp.dateclose>='$startDate'
                            and tp.dateclose<='$endDate'
                            and tp.doctor=$doctor_id
                            and p.villageorcity=0
                            group by f.number";
            //echo $QueryText;
            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);
        	
            while($row = ibase_fetch_row($result))
        	{	
                $res[$row[0]]=$row[1]; 
        
        	}  
        
        	ibase_free_query($query);
        	ibase_free_result($result);
            
            switch($form)
            {
                case 392:
                    $uop='63';
                    break;
                case 393:
                    $uop='37';
                    break;
                case 394:
                    $uop='80';
                    $uop;
                default:
                    $fpath='63';
                    $uop;
            }
            $QueryText = "select 3,count(distinct(cast(tp.dateclose as Date)||'_'||p.id)) from treatmentproc tp
                                            left join patients p on (p.id=tp.patient_fk)
                                            where
                                            tp.dateclose>='$startDate'
                                            and tp.dateclose<='$endDate'
                                            and tp.doctor=$doctor_id
                union
                select 4,count(distinct(cast(tp.dateclose as Date)||'_'||p.id)) from treatmentproc tp
                                            left join patients p on (p.id=tp.patient_fk)
                                            where
                                            (select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)=1
                                            and tp.dateclose>'$startDate'
                                            and tp.dateclose<'$endDate'
                                            and tp.doctor=$doctor_id
                                            
                union
                select $uop,sum(tp.uop) from treatmentproc tp
                                            left join patients p on (p.id=tp.patient_fk)
                                            where
                                            tp.doctor=$doctor_id
                                            and tp.dateclose>='$startDate'
                                            and tp.dateclose<='$endDate'";
                if($form==394)
                {
                    $QueryText.= "union
                                        select 6,count(distinct(cast(tp.dateclose as Date)||'_'||p.id)) from treatmentproc tp
                                                                    left join patients p on (p.id=tp.patient_fk)
                                                                    where
                                                                    (select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)=1
                                                                    and p.villageorcity=0
                                                                    and tp.dateclose>='$startDate'
                                                                    and tp.dateclose<='$endDate'
                                        
                                                                    and tp.doctor=$doctor_id
                                        union
                                            select 5,sum(tp.proc_count) from f39 f
                                                                        left join f39tproc ftp on (ftp.f39id=f.id)
                                                                        left join treatmentproc tp on (tp.id=ftp.treatmentprocid)
                                                                        where f.form=$form
                                                                        and f.number=5
                                                                        and ftp.unsigned=0
                                                                        and tp.dateclose>='$startDate'
                                                                        and tp.dateclose<='$endDate'
                                                                        and tp.doctor=$doctor_id
                                                                        and (select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)=1
                                        union
                                            select 7,sum(tp.proc_count) from f39 f
                                                                        left join f39tproc ftp on (ftp.f39id=f.id)
                                                                        left join treatmentproc tp on (tp.id=ftp.treatmentprocid)
                                                                        left join patients p on (p.id=tp.patient_fk)
                                                                        where f.form=$form
                                                                        and f.number=5
                                                                        and ftp.unsigned=0
                                                                        and tp.dateclose>='$startDate'
                                                                        and tp.dateclose<='$endDate'
                                                                        and tp.doctor=$doctor_id
                                                                        and p.villageorcity=0
                                                                        and (select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)=1";
                //echo $QueryText."\n"."\n";
                }
                else
                {
                    $QueryText.= "union
                select 401,count(distinct(cast(tp.dateclose as Date)||'_'||p.id)) from treatmentproc tp
                                            left join patients p on (p.id=tp.patient_fk)
                                            where
                                            (select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)=1
                                            and p.villageorcity=0
                                            and tp.dateclose>='$startDate'
                                            and tp.dateclose<='$endDate'
                
                                            and tp.doctor=$doctor_id
                union
                select 5,count(distinct(cast(tp.dateclose as Date)||'_'||p.id)) from treatmentproc tp
                                            left join patients p on (p.id=tp.patient_fk)
                                            where
                                            (select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)=1
                                            and (Select * from AGE(p.birthday,tp.dateclose))<18
                                            and tp.dateclose>='$startDate'
                                            and tp.dateclose<='$endDate'
                                            and tp.doctor=$doctor_id";
                }
               

            //echo $QueryText;
            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);
        
            while($row = ibase_fetch_row($result))
        	{	
                $res[$row[0]]=$row[0]==$uop?$row[1]:floor($row[1]); 
                
        
        	}  
        
        	ibase_free_query($query);
        	ibase_free_result($result);
            
            $res['1']= date("d.m.Y",strtotime($endDate)); 
        
                
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res; 
    }     
}
?>

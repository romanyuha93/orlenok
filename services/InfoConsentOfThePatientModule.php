<?php
require_once "Connection.php";
require_once "Utils.php";

class PrintInfoRecord{
    /**
    * @var string
    */
    var $FORMNAME;
	/**
    * @var string
    */
    var $DOCTORFULLNAME;
    /**
    * @var string
    */
    var $BIRTHDATE;
    /**
    * @var string
    */
    var $PATIENTFULLNAME;
    /**
    * @var string
    */
    var $LEGALNAME;
    /**
    * @var string
    */
    var $CLINICNAME;
    /**
    * @var string
    */
    var $ADDRESS;
    /**
    * @var string
    */
    var $PHONE;
    /**
    * @var string
    */
     var $FAX;
    /**
    * @var string
    */
    var $PATIENTSHORTNAME;
    /**
    * @var string
    */
    var $DOCTORSHORTNAME;
    /**
    * @var string
    */
    var $DATECREATE;
}

class ProcedureRecord{

	/**
    * @var string
    */
    var $NAME;
	
	/**
    * @var int
    */
    var $PROCTYPE;
	
	/**
    * @var float
    */
    var $PRICE;
}

class ExamRecord{
	/**
    * @var string
    */
    var $EXAM;
}

class DiagnosAndTreatmentPlan{
	/**
    * @var string
    */
    var $DIAGNOSES;
    
    /**
    * @var string
    */
    var $TREATMENTPLAN;
    
    /**
    * @var float
    */
    var $PRICE;
    
    /**
    * @var int
    */
    var $EXAMID;
    
    /**
    * @var string
    */
    var $CREATEDATEANDTIME;
}

class InfoConsentOfThePatientModule{
	/**
     * @param ProcedureRecord $p1
     * @param ExamRecord $p2
     * @param DiagnosAndTreatmentPlan $p3
     * @param PrintInfoRecord $p4
     * @return void
     */

    public function registertypes($p1, $p2, $p3, $p4)
    {
    }
    
    /**  
	* @param int $examId
	* @param string $doctorId
	* @return PrintInfoRecord
	*/ 
    public function getPrintInformation($examId, $doctorId){
     
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'currentInfoConsentFormName'";
	  	$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$row = ibase_fetch_row($result);
        $obj = new PrintInfoRecord;
        $obj->FORMNAME = $row[0];
		$QueryText = "";
		if($doctorId == 'NaN'){
			$QueryText = "select infoconsent.createdateandtime, patients.fullname, patients.shortname, patients.birthday,
       							doctors.fullname,doctors.shortname
					  from infoconsent
						        left join toothexam on (toothexam.examid = infoconsent.examid)
						        left join patients on (toothexam.id = patients.id)
						        left join doctors on (toothexam.doctorid = doctors.id)
					  where infoconsent.examid = $examId";
	  	}else{
	  		$QueryText = "select infoconsent.createdateandtime, patients.fullname, patients.shortname, patients.birthday,
       							doctors.fullname,doctors.shortname
					  from infoconsent
						        left join toothexam on (toothexam.examid = infoconsent.examid)
						        left join patients on (toothexam.id = patients.id)
						        left join doctors on ($doctorId = doctors.id)
					  where infoconsent.examid = $examId";
	  	}
					  
	  	$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$row = ibase_fetch_row($result);
		$obj->DATECREATE = $row[0];
		$obj->PATIENTFULLNAME = $row[1];
		$obj->PATIENTSHORTNAME = $row[2];
		$obj->BIRTHDATE = $row[3];
		$obj->DOCTORFULLNAME = $row[4];
		$obj->DOCTORSHORTNAME = $row[5];
		$QueryText = 'select clinicregistrationdata.legalname, clinicregistrationdata.name,
			       	  clinicregistrationdata.telephonenumber, clinicregistrationdata.fax, clinicregistrationdata.address
					  from clinicregistrationdata';
	  	$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$row = ibase_fetch_row($result);
		$obj->LEGALNAME = $row[0];
		$obj->CLINICNAME = $row[1];
		$obj->PHONE = $row[2];
		$obj->FAX = $row[3];
		$obj->ADDRESS = $row[4];
		ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $obj;
	}
	
	/**  
    * @return ProcedureRecord[]
	*/
	public function getProcedures(){
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = 'select healthproc.name "NAME", 
			    healthproc.healthtype "HEALTHTYPE",
			     healthproc.price "PRICE"
			      from healthproc where healthproc.nodetype = 1 
                  order by healthproc.healthtype';
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
			$rows = array();
			while ($row = ibase_fetch_row($result))
			{
				$obj = new ProcedureRecord;
				$obj->NAME = $row[0];
				$obj->PROCTYPE = $row[1];
				$obj->PRICE = $row[2];
				$rows[] = $obj;
			}
			
			ibase_commit($trans);
			ibase_free_query($query);
			ibase_free_result($result);
			$connection->CloseDBConnection();	
			return $rows;
	}
	
	/**  
	* @param int $examId
	*/ 
	public function getDiagnosesByExamId($examId){
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "select count(id) from infoconsent where infoconsent.examid = $examId";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $row = ibase_fetch_row($result);
			ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
			 if($row[0] == 0){
				$connection = new Connection();
				$connection->EstablishDBConnection();
				$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
				$QueryText = "select toothexam.exam from toothexam where toothexam.examid = $examId";
				$query = ibase_prepare($trans, $QueryText);
				$result=ibase_execute($query);
				$obj = new ExamRecord;
				while ($row = ibase_fetch_row($result))
				{
					$obj->EXAM = $row[0];
				}
				ibase_commit($trans);
				ibase_free_query($query);
				ibase_free_result($result);
				$connection->CloseDBConnection();				
				return $obj;
			}else{
				$connection = new Connection();
				$connection->EstablishDBConnection();
				$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
				$QueryText = "select infoconsent.diagnoses, infoconsent.treatmentplan, infoconsent.price from infoconsent where examid = $examId";
				
				$query = ibase_prepare($trans, $QueryText);
				$result=ibase_execute($query);
				$obj = new DiagnosAndTreatmentPlan;
				while ($row = ibase_fetch_row($result))
				{
					$obj->DIAGNOSES = $row[0];
					$obj->TREATMENTPLAN = $row[1];
					$obj->PRICE = $row[2];
				}
				
				ibase_commit($trans);
				ibase_free_query($query);
				ibase_free_result($result);
				$connection->CloseDBConnection();
				return $obj;
			}
	}
	
	/**  
	* @param DiagnosAndTreatmentPlan $daigAndTreatPlan
    * @return boolean
	*/ 
	public function addIntoInfoconsentNewRecord($daigAndTreatPlan){
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = format_for_query("update or insert into INFOCONSENT (diagnoses, treatmentplan, price, examid, createdateandtime)
									   values
									   ('{0}', '{1}', '{2}', '{3}', '{4}') matching (examid)",
									   $daigAndTreatPlan->DIAGNOSES, $daigAndTreatPlan->TREATMENTPLAN, $daigAndTreatPlan->PRICE, 
									   $daigAndTreatPlan->EXAMID, $daigAndTreatPlan->CREATEDATEANDTIME);
	   
	   	$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		ibase_free_query($query);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
	}
	
	
}

?>
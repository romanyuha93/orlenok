<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

class AccountModuleInvoice
{				
	/**
	* @var string
	*/
	var $ID;    
	
	/**
	* @var string
	*/
	var $PATIENTID;

	/**
	* @var string
	*/
	var $PATIENTFULLNAME;          

	/**
	* @var string
	*/
	var $AMOUNT;

	/**
	* @var string
	*/
	var $PAIDINCASH;
	 
	/**
	* @var string
	*/
	var $CREATEDATE;  
	
	/**
	* @var string
	*/
	var $DOCTORID;

	/**
	* @var string
	*/
	var $DOCTORSHORTNAME;

	/**
	* @var string
	*/
	var $INVOICENUMBER;	
}

class AccountModule_mobilephone
{
    /**
    * @var string
    */
    var $id; 
    /**
    * @var string
    */
    var $parent_fk; 
    /**
    * @var string
    */
    var $phone; 
    /**
    * @var int
    */
    var $isActive; 
    /**
    * @var string
    */
    var $comments; 
}

//---------------------------- Groups START ------------------------------
class AccountModule_Group
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $group_type;
    /**
     * @var int
     */
    var $rules;
    /**
     * @var int
     */
    var $is_cashpayment;
    /**
     * @var int
     */
    var $monthly_is;
    /**
     * @var number
     */
    var $monthly_defaultsumm;
    /**
     * @var int
     */
    var $group_color;
    /**
     * @var int
     */
    var $group_sequince;
    /**
     * @var string
     */
    var $group_residue;	
    
    /**
     * @var int
     */
    var $isEmployee;
}

class AccountModule_Doctor
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var string
     */
    var $lname;
    /**
     * @var string
     */
    var $fname;
    /**
     * @var string
     */
    var $sname;
    /**
     * @var string
     */
    var $fullname;
    /**
     * @var string
     */
    var $shortname;
}

class AccountModuleClinicRegistrationData
{
    /**
    * @var string
    */
    var $ESTABLISHMENTNAME;
		
		/**
    * @var string
    */
    var $ADDRESS;
		
		/**
    * @var string
    */
    var $CODE;		
		
		/**
    * @var string
    */
    var $TELEPHONENUMBER;		
}

//---------------------------- Groups END ------------------------------

//---------------------------- Expenses Groups START ------------------------------
class AccountModule_ExpensesGroup
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $is_cashpayment;
    /**
     * @var int
     */
    var $group_color;
    /**
     * @var int
     */
    var $group_sequince;
}
//---------------------------- Expenses Groups END ------------------------------

//---------------------------- Receipt Objects START ---------------------
class AccountModuleReceiptProcedurData
{
    /**
    * @var string
    */
    var $name;
		
    /**
    * @var string
    */
    var $shifr;
		
    /**
    * @var int
    */
    var $is_discount;	
    /**
    * @var number
    */
    var $price;		
    /**
    * @var number
    */
    var $discount_summ_price;	
    /**
    * @var number
    */
    var $proc_count;
}

class AccountModuleReceiptData
{
        /**
    * @var string
    */
    var $company;
		
    /**
    * @var string
    */
    var $companyaddress;
		
    /**
    * @var string
    */
    var $companyname;
    
    /**
    * @var string
    */
    var $companyin;	
    
    /**
    * @var object
    */
    var $companylogo;
    
    /**
    * @var string
    */
    var $companyphone;
    
    /**
    * @var string
    */
    var $companysite;
    /**
    * @var string
    */
    var $director;
    /**
    * @var string
    */
    var $fax;
    /**
    * @var string
    */
    var $email;
    /**
    * @var string
    */
    var $chiefaccountant;
    /**
    * @var string
    */
    var $legaladdress;
    /**
    * @var string
    */
    var $bank_name;
    /**
    * @var string
    */
    var $bank_mfo;
    /**
    * @var string
    */
    var $bank_currentaccount;
    /**
    * @var string
    */
    var $license_number;
    /**
    * @var string
    */
    var $license_series;
    /**
    * @var string
    */
    var $license_startdate;
    /**
    * @var string
    */
    var $license_enddate;
    /**
    * @var string
    */
    var $license_issued;
                            
    
    /**
    * @var string
    */
    var $receiptnumber;
    
    /**
    * @var number
    */
    var $discount_amount;
	
	/**
    * @var number
    */
    var $paid_sum;
    
    
    /**
    * @var string
    */
    var $doctorSname;
    /**
    * @var string
    */
    var $patientSname;
    /**
    * @var string
    */
    var $creatdate;
    
    /**
    * @var AccountModuleReceiptProcedurData[]
    */
    var $ReceiptProcedurData;
}

class AccountModuleProcedureObject
{
   /**
   * @var string
   */
	var $proc_id;
   /**
   * @var string
   */
	var $proc_nameforplan;
   /**
   * @var string
   */
	var $proc_price;
   /**
   * @var number
   */
	var $proc_count;
   /**
   * @var string
   */
	var $dateclose;
   /**
   * @var string
   */
	var $doctor_fname; 
   /**
   * @var string
   */
	var $doctor_lname; 
   /**
   * @var string
   */
	var $doctor_pname;
   /**
   * @var number
   */
	var $discount_amount;
   /**
   * @var int
   */
	var $discount_flag;//0 - не распространяется скидка, 1 - распространяется
    /**
     * @var string
     */
    var $proc_shifr;
   /**
   * @var string
   */
	var $healthproc_fk;
}
//---------------------------- Receipt Objects END ---------------------

// -------------- ACCOUNT FLOW OBJECTS START ----------------------------
class AccountModule_FlowObject
{
   /**
   * @var boolean
   */
	var $is_invoice;
   /**
   * @var string
   */
	var $invoice_id;
   /**
   * @var string
   */
	var $invoice_timestamp;
   /**
   * @var string
   */
	var $invoice_number;
   /**
   * @var number
   */
	var $invoice_sum;
   /**
   * @var number
   */
	var $invoice_discountsum;
   /**
   * @var number
   */
	var $invoice_paidincash;
   /**
   * @var number
   */
	var $invoice_discount;
   /**
   * @var int
   */
	var $invoice_isCashpayment;
   /**
   * @var string
   */
	var $patient_id;
   /**
   * @var string
   */
	var $patient_insuranceId;
   /**
   * @var string
   */
	var $patient_insuranceNumber;
   /**
   * @var string
   */
	var $patient_insuranceCompanyName;
    /**
   * @var string
   */
	var $patient_insuranceCompanyId;
   /**
   * @var string
   */
	var $patient_fname;
   /**
   * @var string
   */
	var $patient_lname;
   /**
   * @var string
   */
	var $patient_sname;
   /**
   * @var string
   */
	var $patient_cardnumber;
   /**
   * @var number
   */
	var $patient_discount;
    
    /**
    * @var number
    */
    var $discount_amount_auto;
    
    /**
    * @var boolean
    */
    var $INDDISCOUNT;
    
   /**
   * @var number
   */
	var $patient_balance;
   /**
   * @var number
   */
	var $patient_paidincash;
    
    
   /**
   * @var number
   */
	var $patient_balanceNoncash;
   /**
   * @var number
   */
	var $patient_paidincashNoncash;
    
    
   /**
   * @var number
   */
	var $patient_balanceAll;
   /**
   * @var number
   */
	var $patient_paidincashAll;
    
   /**
   * @var string
   */
	var $patient_label;
    
   /**
   * @var string
   */
	var $procedures_MinDate;
   /**
   * @var string
   */
	var $procedures_MaxDate;
       /**
   * @var string
   */
	var $author_id;
       /**
   * @var string
   */
	var $author_shortname;

    /**
   * @var string
   */
	var $subdivision_id;
        
    /**
   * @var string
   */
	var $subdivision_name;
}

class AccountModule_ProcedureObject
{
   /**
   * @var string
   */
	var $proc_id;
   /**
   * @var string
   */
	var $proc_nameforplan;
   /**
   * @var number
   */
	var $proc_price;
   /**
   * @var number
   */
	var $proc_discountprice;
   /**
   * @var number
   */
	var $proc_count;
   /**
    * @var string
    */
    var $proc_shifr;
   /**
   * @var number
   */
	var $patient_discount;
    
    /**
    * @var number
    */
    var $discount_amount_auto;
    
    /**
    * @var boolean
    */
    var $INDDISCOUNT;
    
   /**
   * @var int
   */
	var $discount_flag;//0 - не распространяется скидка, 1 - распространяется
   /**
   * @var string
   */
	var $dateclose;
   /**
   * @var string
   */
	var $doctor_id; 
   /**
   * @var string
   */
	var $doctor_fname; 
   /**
   * @var string
   */
	var $doctor_lname; 
   /**
   * @var string
   */
	var $doctor_pname;
   /**
   * @var boolean
   */
	var $is_includeininvoice = true;
   /**
   * @var string
   */
	var $healthproc_fk;
    
    
   /**
   * @var string
   */
	var $regCode;
}

class AccountModule_InvoiceCashObject
{
   /**
   * @var string
   */
   var $id;
   /**
   * @var string
   */
   var $summ;
   /**
   * @var string
   */
   var $operationdate;
   
}
// -------------- ACCOUNT FLOW OBJECTS END ----------------------------


// ------- DEBTOR MODULE OBJECTS START ----------------------
class AccountModuleDebtorObject
{
   /**
   * @var string
   */
	var $patient_id;
   /**
   * @var string
   */
	var $patient_fname; 
   /**
   * @var string
   */
	var $patient_lname; 
   /**
   * @var string
   */
	var $patient_sname;
   /**
   * @var string
   */
	var $patient_mobile;
   /**
   * @var string
   */
	var $patient_phone;
   /**
   * @var number
   */
	var $work_sum;
   /**
   * @var number
   */
	var $balance_sum;
   /**
   * @var string
   */
	var $lastPaid;
    
    /**
    * @var AccountModule_mobilephone[]
    */
    var $mobiles; 
    
    
   /**
   * @var boolean
   */
	var $attentionFlag;
}
// ------- DEBTOR MODULE OBJECTS END ----------------------


// ------- ACCOUNT MODULE OBJECTS START ----------------------

class AccountModule_AccountObject
{
   /**
   * @var string
   */
	var $flow_id;
   /**
   * @var string
   */
	var $patient_id;
   /**
   * @var string
   */
	var $patient_fname; 
   /**
   * @var string
   */
	var $patient_lname; 
   /**
   * @var string
   */
	var $patient_sname;
   /**
   * @var string
   */
	var $patient_cardnumber;
   /**
   * @var number
   */
	var $flow_sum;
   /**
   * @var int
   */
	var $flow_operationtype;
   /**
   * @var string
   */
	var $flow_operationnote;
   /**
   * @var string
   */
	var $flow_createdate;
 /**
   * @var int
   */
	var $flow_ordernumber;
   /**
   * @var string
   */
	var $flow_oninvoice_fk;
    
   /**
   * @var string
   */
	var $invoice_number;
   /**
   * @var string
   */
	var $invoice_createdate;
    
   /**
   * @var int
   */
	var $flow_isCashpayment;
    
   /**
   * @var string
   */
	var $flow_groupName;
    
    
   /**
   * @var string
   */
	var $flow_award_personId;
   /**
   * @var string
   */
	var $flow_award_personFio;   
   /**
   * @var string
   */
	var $flow_award_foundDate;
    
         
   /**
   * @var string
   */
	var $exp_group_id;
   /**
   * @var string
   */
	var $exp_group_name;   
   /**
   * @var int
   */
	var $exp_group_color;
   /**
   * @var string
   */
	var $exp_proc_id; 
   /**
   * @var string
   */
	var $exp_proc_dateclose;
   /**
   * @var string
   */
	var $exp_proc_name;   
   /**
   * @var string
   */
	var $exp_proc_shifr; 
   /**
   * @var string
   */
	var $exp_doctor_fname;
   /**
   * @var string
   */
	var $exp_doctor_lname;
   /**
   * @var string
   */
	var $exp_doctor_sname;   
   /**
   * @var string
   */
	var $exp_patient_fname;
   /**
   * @var string
   */
	var $exp_patient_lname;   
   /**
   * @var string
   */
	var $exp_patient_sname;
 
    /**
   * @var string
   */
	var $author_id;
       /**
   * @var string
   */
	var $author_shortname;

    /**
   * @var string
   */
	var $subdivision_id;
        
    /**
   * @var string
   */
	var $subdivision_name;


    /**
   * @var string
   */
	var $certificate_id;
    /**
   * @var string
   */
	var $certificate_number;
    
    
    

    /**
   * @var string
   */
	var $storage_docId;
    /**
   * @var int
   */
	var $storage_docType;
    /**
   * @var string
   */
	var $storage_docNumber;
    /**
   * @var string
   */
	var $storage_docDate;
    /**
   * @var string
   */
	var $storage_patientId;
    /**
   * @var string
   */
	var $storage_patientShortname;
}

class AccountModule_AccountData
{
   /**
   * @var number
   */
	var $account_sum;
   /**
   * @var AccountModule_AccountObject[]
   */
	var $flowData;
}


class AccountModule_PatientObject
{
      /**
   * @var string
   */
	var $patient_id;
   /**
   * @var string
   */
	var $patient_fname; 
   /**
   * @var string
   */
	var $patient_lname; 
   /**
   * @var string
   */
	var $patient_sname;
   /**
   * @var string
   */
	var $patient_fio;
   /**
   * @var string
   */
	var $patient_label;
}

// ------- ACCOUNT MODULE OBJECTS START ----------------------


class AccountModule_PatientInsurances
{
   /**
   * @var string
   */
	var $insurance_id;
   /**
   * @var string
   */
	var $insurance_number;
   /**
   * @var string
   */
	var $insurance_fromDate;
   /**
   * @var string
   */
	var $insurance_toDate;
   /**
   * @var string
   */
	var $company_id;
   /**
   * @var string
   */
	var $company_name;
   /**
   * @var string
   */
	var $company_phone;
}

class AccountModule
{		
  /** 
  * @param AccountModuleReceiptProcedurData $p1
  * @param AccountModuleReceiptData $p2
  * @param AccountModuleProcedureObject $p3
  * @param AccountModule_FlowObject $p4
  * @param AccountModule_ProcedureObject $p5
  * @param AccountModule_PrintAccountInfo $p6
  * @param AccountModuleDebtorObject $p7
  * @param AccountModule_AccountObject $p8
  * @param AccountModule_AccountData $p9
  * @param AccountModule_PatientObject $p10
  * @param AccountModule_Group $p11
  * @param AccountModule_InvoiceCashObject $p12
  * @param AccountModule_PatientInsurances $p13
  * @param AccountModule_Doctor $p14
  * @param AccountModule_mobilephone $p15
  * @param AccountModule_ExpensesGroup $p16
  * @return void
  */    
	public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10, $p11, $p12, $p13, $p14, $p15, $p16) 
	{
	}   
    
    /** 
 	* @param string $patient_id
    * @return AccountModule_mobilephone[]
	*/ 
    private function getMobilePhones($patient_id, $trans)
    {
         $mobile_sql =
			"select patientsmobile.id,
                patientsmobile.parent_fk,
                patientsmobile.mobilenumber,
                patientsmobile.senderactive,
                patientsmobile.comments
            from patientsmobile
                where patientsmobile.patient_fk = $patient_id
            order by patientsmobile.senderactive desc, patientsmobile.id desc";
        			
        $mobile_query = ibase_prepare($trans, $mobile_sql);
        $mobile_result=ibase_execute($mobile_query);
        $rows = array();
		while ($row = ibase_fetch_row($mobile_result))
		{ 
            $obj = new AccountModule_mobilephone;
            $obj->id = $row[0];
            $obj->parent_fk = $row[1];
            $obj->phone = $row[2];
            if($row[3] == 1)
            {
                $obj->isActive = true;
            }
            else
            {
                $obj->isActive = false; 
            }
            $obj->comments = $row[4];
            $rows[] = $obj;
		}	
        ibase_free_query($mobile_query);
        ibase_free_result($mobile_result);
        return $rows;
    } 
    
  /** 
  * @param string $date
  * @param string $patientId
  * @param boolean $isEnabledInCash
  * @param int $flow_isCashpayment
  * @param string $accountId
  * @param int $onlyCurrentSubdivision
  * @param string $subdivision
  * @param string $userIDOnly
  * @return AccountModule_FlowObject[]
  */  
  public function getAccountFlow($date, $patientId, $isEnabledInCash, $flow_isCashpayment, $accountId, $onlyCurrentSubdivision, $subdivision, $userIDOnly)
  {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection); 
        $rows = array();
        
        $nowDate = new DateTime();
        $nowDate->setTime(0,0,0);
        $queryDate = new DateTime($date);
        $queryDate->setTime(0,0,0);
        //1 
        //select flow patients where invoice is not created
        if(($queryDate >= $nowDate)||(($patientId != 'null')&&($patientId != null)&&($patientId != '')&&($patientId != '-')))
        { 
    	    $notClosedFlowQueryText =
    			"select distinct( patients.id),
                    patients.firstname, 
                    patients.lastname, 
                    patients.middlename, 
                    patients.cardbefornumber, 
                    patients.cardnumber, 
                    patients.cardafternumber, 
                    patients.discount_amount,
                    patients.MOBILENUMBER,
                    (
                        select first 1 patientsinsurance.id
                        from patientsinsurance
                        where patientsinsurance.patientid = patients.id
                        and ((cast(treatmentproc.dateclose as Date) >= patientsinsurance.policefromdate)
                        and (cast(treatmentproc.dateclose as Date) < patientsinsurance.policetodate) )
                        order by patientsinsurance.policefromdate desc, patientsinsurance.id desc
                    ),
                    (select max(tp.dateclose) from treatmentproc tp where tp.patient_fk = patients.id and tp.invoiceid is null
                    and tp.dateclose is not null) as dc,
                    coalesce((select first 1 ds.percent from discount_schema ds where ds.summ<=patients.ACCUMULATION order by ds.summ desc),0),
                    (case (select aps.parametervalue from applicationsettings aps where aps.parametername='DISCOUNTSCHEMA') when 'true' then patients.inddiscount else 1 end)
                    from treatmentproc
                    inner join diagnos2
                    on diagnos2.id = treatmentproc.diagnos2
                    inner join treatmentcourse
                    on treatmentcourse.id = diagnos2.treatmentcourse
                    inner join patients
                    on treatmentcourse.patient_fk = patients.id
                    
                    where treatmentproc.invoiceid is null
                    and treatmentproc.dateclose is not null";
            if(($patientId != 'null')&&($patientId != null)&&($patientId != '')&&($patientId != '-'))
            {
                $notClosedFlowQueryText .= " and patients.id = $patientId ";
            }
            $notClosedFlowQueryText .= " order by dc desc";
            
            $notClosedFlowQuery = ibase_prepare($trans, $notClosedFlowQueryText);
            $notClosedFlowResult=ibase_execute($notClosedFlowQuery);
    		while ($row = ibase_fetch_row($notClosedFlowResult))
    		{ 
    		  if(($row[0]!='')&&($row[0]!=null))
              {
                $obj = new AccountModule_FlowObject;
                $obj->is_invoice = false;
                $obj->patient_id = $row[0];
                $obj->patient_fname = $row[1];
                $obj->patient_lname = $row[2];
                $obj->patient_sname = $row[3];
                $obj->patient_cardnumber = $row[4].$row[5].$row[6];
                $obj->patient_discount = $row[7];
                $obj->patient_label = $obj->patient_lname.' '.$obj->patient_fname.' '.$obj->patient_sname.' ('.$obj->patient_cardnumber.') '.$row[8];
                $obj->patient_insuranceId = $row[9];
                $obj->discount_amount_auto = $row[11];
                $obj->INDDISCOUNT = $row[12];
                 if($row[9] == null)//for insurance
                 {
                    $invoice_sum_query_txt = "select 
                                    coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0),
                                    
                                    coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                                     coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*".($obj->INDDISCOUNT?$obj->patient_discount:$obj->discount_amount_auto)."/100.00) as decimal(15,2))),0),
                                   max(treatmentproc.dateclose),
                                   min(treatmentproc.dateclose)
                  
                       from treatmentproc
                       
                       inner join diagnos2
                       on diagnos2.id = treatmentproc.diagnos2
                       inner join treatmentcourse
                       on treatmentcourse.id = diagnos2.treatmentcourse
                    
                       where treatmentcourse.patient_fk = $obj->patient_id
                       and treatmentproc.invoiceid is null
                       and treatmentproc.dateclose is not null";
                       
                    $insuranseDates_sql = "select patientsinsurance.policefromdate,
                                            patientsinsurance.policetodate
                                        from patientsinsurance
                                            where patientsinsurance.patientid = $obj->patient_id";  
                    $insuranseDates_query = ibase_prepare($trans, $insuranseDates_sql);
                    $insuranseDates_result = ibase_execute($insuranseDates_query);
  		            while ($datesRow = ibase_fetch_row($insuranseDates_result))
                    {
                        $invoice_sum_query_txt .= " and ((cast(treatmentproc.dateclose as Date) < '$datesRow[0]')
                        or (cast(treatmentproc.dateclose as Date) >= '$datesRow[1]')) ";
                    }
                    
                 }
                 else//for not insurance
                 {
                     $invoice_sum_query_txt = "select 
                                        coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0),
                                        
                                        coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                                         coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*".($obj->INDDISCOUNT?$obj->patient_discount:$obj->discount_amount_auto)."/100.00) as decimal(15,2))),0),
                                       max(treatmentproc.dateclose),
                                       min(treatmentproc.dateclose)
                  
                                    from treatmentproc
                       
                                       inner join diagnos2
                                       on diagnos2.id = treatmentproc.diagnos2
                                       inner join treatmentcourse
                                       on treatmentcourse.id = diagnos2.treatmentcourse
                                       inner join patientsinsurance
                                       on patientsinsurance.id = $row[9]
                                    
                                       where treatmentcourse.patient_fk = $obj->patient_id
                                       and treatmentproc.invoiceid is null
                                       and treatmentproc.dateclose is not null
                
                
                                       and (cast(treatmentproc.dateclose as Date) >= patientsinsurance.policefromdate)
                                       and (cast(treatmentproc.dateclose as Date) < patientsinsurance.policetodate)"; 
                 }
                        
                $invoice_sum_query = ibase_prepare($trans, $invoice_sum_query_txt);
                $invoice_sum_result = ibase_execute($invoice_sum_query);
                $invoice_sum_row = ibase_fetch_row($invoice_sum_result);
                $obj->invoice_sum = $invoice_sum_row[0];
                $obj->invoice_discountsum = $invoice_sum_row[1];
                $obj->procedures_MaxDate = $invoice_sum_row[2];
                $obj->procedures_MinDate = $invoice_sum_row[3];
                            
                            
                $patient_paidincash_text = "select

                                (select coalesce(cast(sum(invoices.summ) as decimal(15,2)),0)
                                        from invoices
                                    where invoices.patientid = $obj->patient_id
                                    and invoices.is_cashpayment = 0),
                                
                                (select coalesce(cast(sum(accountflow.summ) as decimal(15,2)),0) 
                                        from accountflow 
                                    where accountflow.patientid = $obj->patient_id
                                    and accountflow.is_cashpayment = 0),
                                    
                                    
                                (select coalesce(cast(sum(invoices.summ) as decimal(15,2)),0)
                                        from invoices
                                    where invoices.patientid = $obj->patient_id
                                    and invoices.is_cashpayment = 1),
                                
                                (select coalesce(cast(sum(accountflow.summ) as decimal(15,2)),0) 
                                        from accountflow 
                                    where accountflow.patientid = $obj->patient_id
                                    and accountflow.is_cashpayment = 1)     "; 
                                    
                $patient_paidincash_text .= 'from RDB$DATABASE';       
                $patient_paidincash_query = ibase_prepare($trans, $patient_paidincash_text);
                $patient_paidincash_result = ibase_execute($patient_paidincash_query);
                $patient_paidincash_row = ibase_fetch_row($patient_paidincash_result);
                               
                                                             
                $obj->patient_paidincash = $patient_paidincash_row[1];       
                $obj->patient_balance = $patient_paidincash_row[1]-$patient_paidincash_row[0];
                
                $obj->patient_paidincashNoncash = $patient_paidincash_row[3];       
                $obj->patient_balanceNoncash = $patient_paidincash_row[3]-$patient_paidincash_row[2];
                
                
                $obj->patient_paidincashAll = $obj->patient_paidincash+$obj->patient_paidincashNoncash;       
                $obj->patient_balanceAll = $obj->patient_balance+$obj->patient_balanceNoncash;   
                /* avatar $row[10] */
                $rows[] = $obj;
              }
    		}
            ibase_free_query($notClosedFlowQuery);
            ibase_free_result($notClosedFlowResult);
            
             
        }
        //2 for invoices
        $temp_patient_id = '';
        $ClosedFlowQueryText =
			"select
                patients.id,
                patients.firstname,
                patients.lastname,
                patients.middlename, 
                patients.cardbefornumber,
                patients.cardnumber,
                patients.cardafternumber,
                patients.discount_amount,
                
                invoices.id,
                invoices.createdate,
                invoices.paidincash,
                invoices.invoicenumber,
                invoices.discount_amount,
                invoices.is_cashpayment,
                patients.MOBILENUMBER,
                invoices.patieninsurance_fk,
                patientsinsurance.policenumber,
                insurancecompanys.id,
                insurancecompanys.name,
                
                invoices.summ,
                
                account.id,
                account.lastname,
                account.firstname,
                account.middlename,
                coalesce((select first 1 ds.percent from discount_schema ds where ds.summ<=patients.ACCUMULATION order by ds.summ desc),0),
                (case (select aps.parametervalue from applicationsettings aps where aps.parametername='DISCOUNTSCHEMA') when 'true' then patients.inddiscount else 1 end),
                subdivision.id,
                subdivision.name
                
                
                from invoices
                
                inner join patients
                on patients.id = invoices.patientid
                left join account 
                on account.id=invoices.authorid
                left join patientsinsurance
                on patientsinsurance.id = invoices.patieninsurance_fk
                left join insurancecompanys
                on insurancecompanys.id = patientsinsurance.companyid
                left join subdivision
                on subdivision.id = coalesce(invoices.subdivisionid,(select first 1 id from subdivision))";
        if(($patientId != 'null')&&($patientId != null)&&($patientId != '')&&($patientId != '-'))
        {
            $ClosedFlowQueryText .= " where invoices.patientid = $patientId ";
        }
        else
        {
            $ClosedFlowQueryText .= " where invoices.createdate >= '$date 00:00:00' and  invoices.createdate <= '$date 23:59:59' ";
        }
        
        if($flow_isCashpayment != -1)
        {
            $ClosedFlowQueryText .= " and invoices.is_cashpayment = $flow_isCashpayment";
        }
        
        if($userIDOnly != null && $userIDOnly != '' && $userIDOnly != '0')
        {
                $ClosedFlowQueryText.= " and invoices.authorid = $userIDOnly";
        }
        else if(nonull($accountId))
        {
                $ClosedFlowQueryText.= " and invoices.authorid = $accountId";
        }
        
        if($onlyCurrentSubdivision==1)
        {
            $ClosedFlowQueryText.= " and (invoices.subdivisionid is null or invoices.subdivisionid=(select id from current_subdivision))";
        } 
        else if($subdivision != null && $subdivision != "" && $subdivision != "0")
        {
            $ClosedFlowQueryText.= " and subdivision.id = $subdivision ";
        }    
        $ClosedFlowQueryText .= " order by invoices.invoicenumber desc";
        $ClosedFlowQuery = ibase_prepare($trans, $ClosedFlowQueryText);
        $ClosedFlowResult=ibase_execute($ClosedFlowQuery);
		while ($row = ibase_fetch_row($ClosedFlowResult))
		{ 
            $obj = new AccountModule_FlowObject;
            $obj->is_invoice = true;
            $obj->patient_id = $row[0];
            $obj->patient_fname = $row[1];
            $obj->patient_lname = $row[2];
            $obj->patient_sname = $row[3];
            $obj->patient_cardnumber = $row[4].$row[5].$row[6];
            $obj->patient_discount = $row[7];
            
            $obj->invoice_id = $row[8];
            $obj->invoice_timestamp = $row[9];
            $obj->invoice_paidincash = $row[10];
            $obj->invoice_number = $row[11];
            $obj->invoice_discount = $row[12];
            $obj->invoice_isCashpayment = $row[13];
            $obj->patient_label = $obj->patient_lname.' '.$obj->patient_fname.' '.$obj->patient_sname.' ('.$obj->patient_cardnumber.') '.$row[14];
            $obj->patient_insuranceId = $row[15];
            $obj->patient_insuranceNumber = $row[16];
            $obj->patient_insuranceCompanyId = $row[17];
            $obj->patient_insuranceCompanyName = $row[18];
            $obj->author_id= $row[20];
            $obj->author_shortname = shortname($row[21],$row[22],$row[23]); 
            $obj->discount_amount_auto = $row[24];
            $obj->INDDISCOUNT = $row[25];
            $obj->subdivision_id = $row[26];
            $obj->subdivision_name = $row[27];
            $invoice_sum_query_txt = "select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0),
            
                    max(treatmentproc.dateclose),
                    min(treatmentproc.dateclose)
                  
            from treatmentproc
            
                   inner join diagnos2
                   on diagnos2.id = treatmentproc.diagnos2
                   inner join treatmentcourse
                   on treatmentcourse.id = diagnos2.treatmentcourse
                
                   where treatmentcourse.patient_fk = $obj->patient_id
                   and treatmentproc.invoiceid = $obj->invoice_id
                   and treatmentproc.dateclose is not null";
                    /*
                        coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                        coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*$obj->invoice_discount/100.00) as decimal(15,2))),0),
                    */
                $invoice_sum_query = ibase_prepare($trans, $invoice_sum_query_txt);
                $invoice_sum_result = ibase_execute($invoice_sum_query);
                $invoice_sum_row = ibase_fetch_row($invoice_sum_result);
            
            $obj->invoice_discountsum = $row[19];//$invoice_sum_row[1];
            $obj->invoice_sum = $invoice_sum_row[0];//$invoice_sum_row[0];
            $obj->procedures_MaxDate = $invoice_sum_row[1];//$invoice_sum_row[2];
            $obj->procedures_MinDate = $invoice_sum_row[2];//$invoice_sum_row[3];
            if($temp_patient_id != $obj->patient_id)
            {
                $temp_patient_id = $obj->patient_id;
                /*$patient_paidincash_text = "select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                                                coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0)
                                from invoices 
                            inner join treatmentproc
                            on treatmentproc.invoiceid = invoices.id
                            where invoices.patientid = $obj->patient_id";
                $patient_paidincash_query = ibase_prepare($trans, $patient_paidincash_text);
                $patient_paidincash_result = ibase_execute($patient_paidincash_query);
                $patient_paidincash_row = ibase_fetch_row($patient_paidincash_result);*/
                       
                
                $patient_paidincash_text = "select

                                (select coalesce(cast(sum(invoices.summ) as decimal(15,2)),0)
                                        from invoices
                                    where invoices.patientid = $obj->patient_id
                                    and invoices.is_cashpayment = 0),
                                
                                (select coalesce(sum(accountflow.summ),0) 
                                        from accountflow 
                                    where accountflow.patientid = $obj->patient_id
                                    and accountflow.is_cashpayment = 0),
                                    
                                    
                                (select coalesce(cast(sum(invoices.summ) as decimal(15,2)),0)
                                        from invoices
                                    where invoices.patientid = $obj->patient_id
                                    and invoices.is_cashpayment = 1),
                                
                                (select coalesce(cast(sum(accountflow.summ) as decimal(15,2)),0) 
                                        from accountflow 
                                    where accountflow.patientid = $obj->patient_id
                                    and accountflow.is_cashpayment = 1)     "; 
                                    
                $patient_paidincash_text .= 'from RDB$DATABASE';       
                $patient_paidincash_query = ibase_prepare($trans, $patient_paidincash_text);
                $patient_paidincash_result = ibase_execute($patient_paidincash_query);
                $patient_paidincash_row = ibase_fetch_row($patient_paidincash_result);
                                
            }         
                 
                 
                                                             
            $obj->patient_paidincash = $patient_paidincash_row[1];       
            $obj->patient_balance = $patient_paidincash_row[1]-$patient_paidincash_row[0];
            
            $obj->patient_paidincashNoncash = $patient_paidincash_row[3];       
            $obj->patient_balanceNoncash = $patient_paidincash_row[3]-$patient_paidincash_row[2];
            
            $obj->patient_paidincashAll = $obj->patient_paidincash+$obj->patient_paidincashNoncash;       
            $obj->patient_balanceAll = $obj->patient_balance+$obj->patient_balanceNoncash;                                           
            
            /// avatar $row[13] 
            $rows[] = $obj;
		}
        ibase_free_query($ClosedFlowQuery);
        ibase_free_result($ClosedFlowResult);
        
        
        //select account flow datas ************ START ************
        if($isEnabledInCash == true)
        {
             $FlowQueryText =
    			"select
                    accountflow.id,
                    accountflow.patientid,
                    patients.cardbefornumber,
                    patients.cardnumber,
                    patients.cardafternumber,
                    patients.lastname,
                    patients.firstname,
                    patients.middlename,
                    accountflow.summ,
                    accountflow.operationtype,
                    accountflow.operationnote,
                    accountflow.operationtimestamp,
                    accountflow.oninvoice_fk,
                     invoices.invoicenumber,
                     invoices.createdate,
                     accountflow.is_cashpayment,
                     accountflow.ordernumber,
                                     
                        account.id,
                        account.lastname,
                        account.firstname,
                        account.middlename,
                        subdivision.id,
                        subdivision.name,
                        
                        certificate.id,
                        certificate.number
                        
                
                from accountflow
                    left join patients
                    on accountflow.patientid = patients.id 
                     left join account 
                     on account.id=accountflow.authorid
                    left join invoices
                    on accountflow.oninvoice_fk = invoices.id
                    left join subdivision
                    on subdivision.id = coalesce(accountflow.subdivisionid,(select first 1 id from subdivision))
                    left join certificate
                    on certificate.id = accountflow.certificateid
                where (accountflow.operationtype = 0 or accountflow.operationtype = 5)";//operation only for patients
             
            if(($patientId != 'null')&&($patientId != null)&&($patientId != '')&&($patientId != '-'))
            {
                $FlowQueryText .= " and patients.id = $patientId";
            } 
            else
            {
                $FlowQueryText .= "  and accountflow.operationtimestamp >= '$date 00:00:00'
                    and accountflow.operationtimestamp <= '$date 23:59:59'";       
            } 
                                   
            if($flow_isCashpayment != -1)
            {
                    $FlowQueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment";
            }
            
            if($userIDOnly != null && $userIDOnly != '' && $userIDOnly != '0')
            {
                    $FlowQueryText.= " and accountflow.authorid = $userIDOnly";
            }
            else if(nonull($accountId))
            {
                    $FlowQueryText.= " and accountflow.authorid = $accountId";
            }
            
            if($onlyCurrentSubdivision==1)
            {
                $FlowQueryText.= " and (accountflow.subdivisionid is null or accountflow.subdivisionid=(select id from current_subdivision))";
            }
            else if($subdivision != null && $subdivision != "" && $subdivision != "0")
            {
                $FlowQueryText.= " and subdivision.id = $subdivision ";
            } 
            $FlowQueryText .= " order by accountflow.operationtimestamp desc";
            $FlowQuery = ibase_prepare($trans, $FlowQueryText);
            $FlowResult=ibase_execute($FlowQuery);
    		while ($row = ibase_fetch_row($FlowResult))
    		{ 
                $objfl = new AccountModule_AccountObject;
                $objfl->flow_id = $row[0];
                $objfl->patient_id = $row[1];
                $objfl->patient_cardnumber = $row[2].$row[3].$row[4];
                $objfl->patient_lname = $row[5];
                $objfl->patient_fname = $row[6];
                $objfl->patient_sname = $row[7];
                $objfl->flow_sum = $row[8];
                $objfl->flow_operationtype = $row[9];
                $objfl->flow_operationnote = $row[10];
                $objfl->flow_createdate = $row[11];
                $objfl->flow_oninvoice_fk = $row[12];
                $objfl->invoice_number = $row[13];
                $objfl->invoice_createdate = $row[14];
                $objfl->flow_isCashpayment = $row[15];
                $objfl->flow_ordernumber = $row[16];
                $objfl->author_id= $row[17];
                $objfl->author_shortname = shortname($row[18],$row[19],$row[20]); 
                $objfl->subdivision_id = $row[21];
                $objfl->subdivision_name = $row[22];
                $objfl->certificate_id = $row[23];
                $objfl->certificate_number = $row[24];
                //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($obj,true));
                $rows[] = $objfl;
    		}
            ibase_free_query($FlowQuery);
            ibase_free_result($FlowResult);
                
        }
        //select account flow datas ************ END ************
        
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
//        file_put_contents("C:/tmp.txt",var_export($rows, true));		
        //usort($rows, "invoice_timestamp_cmp");
        //file_put_contents("C:/tmp.txt",var_export($rows, true));	
        return $rows;  
  }    
  
    /** 
	* @param string $patientID
	* @param string $insuranceID
    * @param boolean $isCoursename
	*
	* @return AccountModule_ProcedureObject[]
	*/ 
    public function getProceduresToInvoiceByPatientID($patientID, $insuranceID, $isCoursename) 
    { 
        //$COURSENAME = true;
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        //procedure query
        if($insuranceID != '-')
        {
            $ProcedureQueryText = "select 
                                    treatmentproc.id, 
                                    treatmentproc.nameforplan, 
                                    treatmentproc.price, 
                                    treatmentproc.dateclose,
                                    doctors.firstname, 
                                    doctors.lastname, 
                                    doctors.middlename, 
                                    treatmentproc.proc_count, 
                                    treatmentproc.discount_flag,
                                    patients.discount_amount, 
                                    doctors.id,
                                    treatmentproc.shifr, 
                                    treatmentproc.name,
                                    coalesce((select first 1 ds.percent from discount_schema ds where ds.summ<=patients.ACCUMULATION order by ds.summ desc),0),
                                    (case (select aps.parametervalue from applicationsettings aps where aps.parametername='DISCOUNTSCHEMA') when 'true' then patients.inddiscount else 1 end),
                                    healthproc.regcode
                    
                                from treatmentproc
                       
                                       inner join diagnos2
                                       on diagnos2.id = treatmentproc.diagnos2
                                       inner join treatmentcourse
                                       on treatmentcourse.id = diagnos2.treatmentcourse
                                       inner join patients
                                       on patients.id = treatmentcourse.patient_fk
                                       inner join patientsinsurance
                                       on patientsinsurance.id = $insuranceID
                                       left join doctors
                                       on doctors.id = treatmentproc.doctor
                                       left join healthproc
                                       on treatmentproc.healthproc_fk = healthproc.id

                                       where patients.id = $patientID
                                       and treatmentproc.invoiceid is null
                                       and treatmentproc.dateclose is not null
                
                
                                       and (cast(treatmentproc.dateclose as Date) >= patientsinsurance.policefromdate)
                                       and (cast(treatmentproc.dateclose as Date) < patientsinsurance.policetodate)";
        }
        else
        {
            $ProcedureQueryText = "select 
                                    treatmentproc.id, 
                                    treatmentproc.nameforplan, 
                                    treatmentproc.price, 
                                    treatmentproc.dateclose,
                                    doctors.firstname, 
                                    doctors.lastname, 
                                    doctors.middlename, 
                                    treatmentproc.proc_count, 
                                    treatmentproc.discount_flag,
                                    patients.discount_amount, 
                                    doctors.id,
                                    treatmentproc.shifr, 
                                    treatmentproc.name,
                                    coalesce((select first 1 ds.percent from discount_schema ds where ds.summ<=patients.ACCUMULATION order by ds.summ desc),0),
                                    (case (select aps.parametervalue from applicationsettings aps where aps.parametername='DISCOUNTSCHEMA') when 'true' then patients.inddiscount else 1 end),
                                    healthproc.regcode
                                    
                       from treatmentproc
                       
                       inner join diagnos2
                       on diagnos2.id = treatmentproc.diagnos2
                       inner join treatmentcourse
                       on treatmentcourse.id = diagnos2.treatmentcourse
                       inner join patients
                       on patients.id = treatmentcourse.patient_fk
                       left join doctors
                       on doctors.id = treatmentproc.doctor
                       left join healthproc
                       on treatmentproc.healthproc_fk = healthproc.id
                                       
                       where patients.id = $patientID
                       and treatmentproc.invoiceid is null
                       and treatmentproc.dateclose is not null";
                       
                    $insuranseDates_sql = "select patientsinsurance.policefromdate,
                                            patientsinsurance.policetodate
                                        from patientsinsurance
                                            where patientsinsurance.patientid = $patientID";  
                    $insuranseDates_query = ibase_prepare($trans, $insuranseDates_sql);
                    $insuranseDates_result = ibase_execute($insuranseDates_query);
  		            while ($datesRow = ibase_fetch_row($insuranseDates_result))
                    {
                        $ProcedureQueryText .= " and ((cast(treatmentproc.dateclose as Date) < '$datesRow[0]')
                        or (cast(treatmentproc.dateclose as Date) >= '$datesRow[1]')) ";
                    }
        }
	    
        $ProcedureQuery = ibase_prepare($trans, $ProcedureQueryText);
        $ProcedureResult=ibase_execute($ProcedureQuery);
        $rows = array();
		while ($row = ibase_fetch_row($ProcedureResult))
		{ 
            $obj = new AccountModule_ProcedureObject;
            $obj->proc_id = $row[0];
            if($isCoursename == true)
            {
                $obj->proc_nameforplan = $row[12];
            }
            else
            {
                $obj->proc_nameforplan = $row[1];
            }
            $obj->proc_price = $row[2];
            $obj->dateclose = $row[3];
            $obj->doctor_fname = $row[4];
            $obj->doctor_lname = $row[5];
            $obj->doctor_pname = $row[6];
            $obj->proc_count = $row[7];
            $obj->discount_flag = $row[8];
            $obj->proc_discountprice = $obj->proc_price*$obj->proc_count - round($obj->discount_flag*$obj->proc_price*$obj->proc_count*$row[9]/100, 2);
            $obj->patient_discount = $row[9];
            $obj->doctor_id = $row[10];
            $obj->proc_shifr = $row[11];
            $obj->discount_amount_auto = $row[13];
            $obj->INDDISCOUNT = $row[14];
            $obj->regCode = $row[15];
            $rows[] = $obj;
		}
        ibase_free_query($ProcedureQuery);
        ibase_free_result($ProcedureResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $rows;
    }
 
   /** 
  * @param string $procedureID
  * @param int $discount_flag
  * @return int
  */  
  public function updateProcedureDiscountFlag($procedureID, $discount_flag)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update treatmentproc set treatmentproc.discount_flag = '$discount_flag'
                        where treatmentproc.id = '$procedureID'";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $discount_flag;
  }
    
  /** 
  * @param string $patientID
  * @param number $discount
  * @param boolean $indDiscount
  * @return number
  */  
  public function setPatientDiscount($patientID, $discount, $indDiscount)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update patients set 
                            patients.discount_amount = '$discount',
                            patients.INDDISCOUNT = ".($indDiscount?'1':'0')."
                        where patients.id = '$patientID'";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $discount;
  }
  
  /**
   * @param string $invoiceNumber
   * @param string $invoiceDate
   * @param string $patient_id
   * @param number $discount_amount
   * @param Object[] $procedure_ids
   * @param number $paidincash
   * @param int $is_cashpayment
   * @param string $doctor_id
   * @param string $operationnote
   * @param string $payerId
   * @param string $cardId
   * @param string $certificateId
   * @param number $invoiceSumm
   * @param int $is_fiscal
   * @return Object
   */
  public function createInvoice($invoiceNumber, $invoiceDate, $patient_id, $discount_amount, $procedure_ids, $paidincash, $is_cashpayment, $doctor_id, $operationnote, $payerId, $cardId, $certificateId, $invoiceSumm, $is_fiscal)
  {
 	    $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if(nonull($invoiceDate))
        {
            $invoiceDate="'".$invoiceDate."'";
        }
        $invoiceID = 'null';
        $orderNextId = 'null';
        if(($is_cashpayment == 0)||($payerId == '-'))
        {
            $payerId = 'null';
        }
        else
        {
            $payerId = $payerId;
        }
        //SELECT NEXT INVOICEID START
//		$QueryInvoiceIdText = "SELECT GEN_ID(gen_invoices_id, 1) FROM RDB\$DATABASE";
//		$queryInvoiceId = ibase_prepare($trans, $QueryInvoiceIdText);
//		$resultInvoiceId = ibase_execute($queryInvoiceId);
//        $row=ibase_fetch_row($resultInvoiceId);
//        $invoiceNextId = $row[0];
//        ibase_free_query($queryInvoiceId);
//        ibase_free_result($resultInvoiceId);	
        //SELECT NEXT INVOICEID END
        
        //
        
        
        //INSERT INVOICE START
        if($doctor_id == null && $doctor_id == "")
        {
            $doctor_id = "null";
        }
		 $QueryInvoiceText = "insert into INVOICES(ID, PATIENTID, PAIDINCASH, DOCTORID, INVOICENUMBER, CREATEDATE, DISCOUNT_AMOUNT, IS_CASHPAYMENT, PATIENINSURANCE_FK, SUMM, is_fiscal) 
                    values (null, $patient_id, 0, $doctor_id, ".nullcheck($invoiceNumber).", ".nullcheck($invoiceDate).", '$discount_amount', $is_cashpayment, $payerId, '$invoiceSumm', $is_fiscal) returning id";
               //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryInvoiceText);
                $queryInvoice = ibase_prepare($trans, $QueryInvoiceText);
                $resultInvoiceId = ibase_execute($queryInvoice);
                $row=ibase_fetch_row($resultInvoiceId);
                $invoiceNextId = $row[0];
                ibase_free_query($queryInvoice);
                ibase_free_result($resultInvoiceId);	
        //INSERT INVOICE END
        
        //SET INVOICEID IN TREATMETPROCS START
        
        $foreach_flag = true;
        /*
        foreach ($procedure_ids as $procedure_id)
        {
            if($foreach_flag == true)
            {
                $QueryProceduresUpdateText = "update treatmentproc set treatmentproc.invoiceid = $invoiceID where treatmentproc.id = $procedure_id";
                $foreach_flag = false;
            }
            else
            {
                $QueryProceduresUpdateText .= " or treatmentproc.id = $procedure_id";
            }
        }
        */
        
        foreach ($procedure_ids as $procedure)
        {
            //if($foreach_flag == true)
            //{
                $QueryProceduresUpdateText = "update treatmentproc set 
                            treatmentproc.invoiceid = $invoiceNextId,
                            treatmentproc.price = '$procedure->proc_price'
                            
                            where treatmentproc.id = $procedure->proc_id";
                            
                $QueryProceduresUpdate = ibase_prepare($trans, $QueryProceduresUpdateText);
                ibase_execute($QueryProceduresUpdate);
         		ibase_free_query($QueryProceduresUpdate);
            //    $foreach_flag = false;
            //}
            //else
            //{
            //    $QueryProceduresUpdateText .= " or treatmentproc.id = $procedure->proc_id";
            //}
        }
        
        //SET INVOICEID IN TREATMETPROCS END
        
        
        //REFRESH MONEY FLOW START
        $QueryText = "execute procedure REFRESHMONEYFLOW(".$invoiceNextId.", ".$patient_id.", '".$discount_amount."')";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        //REFRESH MONEY FLOW START
        
        //SET PAIDINCASH INTO ACCOUNT START
        if(!is_nan($paidincash)&&($paidincash != 0)&&($paidincash != ""))
        {
            //SELECT NEXT ORDERNUMBER START
            //if($paidincash > 0)//приходной кассовый ордер
            //{
    		//  $QueryOrderNumberText = "select coalesce(MAX(accountflow.ORDERNUMBER),0) + 1 from accountflow 
            //                        where accountflow.summ > 0 and accountflow.operationtype = 0";
            //}
            //else//расходный кассовый ордер
            //{
    		//  $QueryOrderNumberText = "select coalesce(MAX(accountflow.ORDERNUMBER),0) + 1 from accountflow 
            //                        where accountflow.summ < 0 and accountflow.operationtype = 0";  
            //}
    		//$queryOrderNumber = ibase_prepare($trans, $QueryOrderNumberText);
    		//$resultOrderNumber = ibase_execute($queryOrderNumber);
            //$rowOrder=ibase_fetch_row($resultOrderNumber);
            //$OrderNextNumber = $rowOrder[0];
            //ibase_free_query($queryOrderNumber);
            //ibase_free_result($resultOrderNumber);	
            //SELECT NEXT ORDERNUMBER END
            
            //SELECT NEXT ORDERID START
    		$QueryOrderIdText = "SELECT GEN_ID(gen_accountflow_id, 1) FROM RDB\$DATABASE";
    		$queryOrderId = ibase_prepare($trans, $QueryOrderIdText);
    		$resultOrderId = ibase_execute($queryOrderId);
            $row=ibase_fetch_row($resultOrderId);
            $orderNextId = $row[0];
            ibase_free_query($queryOrderId);
            ibase_free_result($resultOrderId);	
            //SELECT NEXT ORDERID END
            
            $QueryProceduresUpdateText = "insert into accountflow(
                                            accountflow.id, 
                                            accountflow.patientid, 
                                            accountflow.summ, 
                                            accountflow.operationtype, 
                                            accountflow.operationnote, 
                                            accountflow.oninvoice_fk, 
                                            accountflow.ordernumber, 
                                            accountflow.is_cashpayment, 
                                            accountflow.discountcardid, 
                                            accountflow.certificateid)
                                        values (
                                            $orderNextId, 
                                            $patient_id, 
                                            '$paidincash', 
                                            0, 
                                            '".safequery($operationnote)."', 
                                            $invoiceNextId, 
                                            null, 
                                            $is_cashpayment, 
                                            ".nullcheck($cardId).", 
                                            ".nullcheck($certificateId).")";
 //           file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryProceduresUpdateText);
            $QueryProceduresUpdate = ibase_prepare($trans, $QueryProceduresUpdateText);
            $resultProceduresUpdate = ibase_execute($QueryProceduresUpdate);
            ibase_free_query($QueryProceduresUpdate);
            
        }
        //SET PAIDINCASH INTO ACCOUNT END
        
        	
	    ibase_commit($trans);
		$connection->CloseDBConnection();
        
		$returnObj = new stdclass();
        $returnObj->invoiceID = $invoiceNextId;
        $returnObj->orderID = $orderNextId;
        
        return $returnObj;
  }
	
    
    //----------------------------------------------------- RECEIPT START ---------------------------------------------------------------
   	 /** 
	* @param string $invoiceID
    * @param boolean $isCoursename
	*
	* @return AccountModuleReceiptData
	*/ 
	public function getProcedursForReceipt($invoiceID, $isCoursename) 
	{
	   //$COURSENAME = true;
			$connection = new Connection();
			$connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $receiptData = new AccountModuleReceiptData;
            
            $receiptData->receiptnumber = 1;
			//CLINIC DATA
            $QueryText = "select 
                            clinicregistrationdata.name,
                            clinicregistrationdata.legalname,
                            clinicregistrationdata.address,
                            clinicregistrationdata.code,
                            clinicregistrationdata.telephonenumber,
                            clinicregistrationdata.sitename,
                            clinicregistrationdata.logo,

                            clinicregistrationdata.director,
                            clinicregistrationdata.fax,
                            clinicregistrationdata.email,
                            clinicregistrationdata.chiefaccountant,
                            clinicregistrationdata.legaladdress,
                            clinicregistrationdata.bank_name,
                            clinicregistrationdata.bank_mfo,
                            clinicregistrationdata.bank_currentaccount,
                            clinicregistrationdata.license_number,
                            clinicregistrationdata.license_series,
                            clinicregistrationdata.license_startdate,
                            clinicregistrationdata.license_enddate,
                            clinicregistrationdata.license_issued
                        from clinicregistrationdata";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            if($row = ibase_fetch_row($result))
            {
                $receiptData->company = $row[0];
                $receiptData->companyname = $row[1];
                $receiptData->companyaddress = $row[2];
                $receiptData->companyin = $row[3];
                $receiptData->companyphone = $row[4];
                $receiptData->companysite = $row[5];
        
                $imageBlob = null;
                $blob_info = ibase_blob_info($row[6]);
                if ($blob_info[0]!=0)
                {
                    $blob_hndl = ibase_blob_open($row[6]);
                    $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                    ibase_blob_close($blob_hndl);
                }
                $receiptData->companylogo = base64_encode($imageBlob);
                
                $receiptData->director = $row[7];
                $receiptData->fax = $row[8];
                $receiptData->email = $row[9];
                $receiptData->chiefaccountant = $row[10];
                $receiptData->legaladdress = $row[11];
                $receiptData->bank_name = $row[12];
                $receiptData->bank_mfo = $row[13];
                $receiptData->bank_currentaccount = $row[14];
                $receiptData->license_number = $row[15];
                $receiptData->license_series = $row[16];
                $receiptData->license_startdate = $row[17];
                $receiptData->license_enddate = $row[18];
                $receiptData->license_issued = $row[19];
			}	
			ibase_free_query($query);
			ibase_free_result($result);
            
			//PROCEDURES
			$QueryText = "select 
                            treatmentproc.nameforplan, 
                            treatmentproc.shifr, 
                            treatmentproc.price, 
                            treatmentproc.proc_count, 
                            treatmentproc.discount_flag, 
                            invoices.discount_amount,
                            treatmentproc.name,
                            (select SHORTNAME from DOCTORS where ID = treatmentproc.doctor),
                            (select SHORTNAME from PATIENTS where ID = treatmentproc.patient_fk),
                            invoices.createdate
                           
                            from treatmentproc
                            left join invoices
                            on invoices.id = treatmentproc.invoiceid
                            where treatmentproc.invoiceid = '$invoiceID'";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			while ($row = ibase_fetch_row($result))
			{
                $item = new AccountModuleReceiptProcedurData;
                if($isCoursename == true)
                {
                    $item->name = $row[6];
                }
                else
                {
                    $item->name = $row[0];
                }
                $item->shifr = $row[1];
                $item->price = $row[2];
                $item->proc_count = $row[3];
                $item->is_discount = $row[4];
                $item->discount_summ_price = $item->price*$item->proc_count - round($item->price*$item->proc_count*$item->is_discount*$receiptData->discount_amount/100,2);
				$receiptData->ReceiptProcedurData[] = $item;
                
                $receiptData->discount_amount = $row[5];
                
                $receiptData->doctorSname = $row[7];
                $receiptData->patientSname = $row[8];
                $receiptData->creatdate = $row[9];
			}	
			ibase_free_query($query);
			ibase_free_result($result);
			
			$QueryText = "
				select
					(select coalesce(sum(accountflow.summ),0) from accountflow where accountflow.oninvoice_fk = '$invoiceID')
				from  invoices
				inner join patients on (patients.id = invoices.patientid) where invoices.id = '$invoiceID'
			";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            if($row = ibase_fetch_row($result))
            {
				$receiptData->paid_sum = $row[0];
			}
			
			ibase_free_query($query);
			ibase_free_result($result);
            
			ibase_commit($trans);
			$connection->CloseDBConnection();
			return $receiptData;
	}	
    //----------------------------------------------------- RECEIPT END ---------------------------------------------------------------
    
    
    //----------------------------------------------------- INVOICE PROCEDURES START ---------------------------------------------------------------
   	/**
	* @param string $invoiceId
	* @param string $passForDelete
 	* @param string[] $accountflowsids
	* @return boolean
 	*/ 		 		
	public function deleteInvoiceById($invoiceId, $passForDelete, $accountflowsids) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               ibase_free_result($result);
        	   ibase_free_query($query);
               
        	    $QueryText = "delete from INVOICES where ID = $invoiceId";
                $QueryFlowsQuery = "delete from accountflow where (id is null)";
           	    for ($i=0; isset($accountflowsids[$i]); $i++)
            	{ 
            	   $QueryFlowsQuery = $QueryFlowsQuery." or (id=$accountflowsids[$i])"; 
                }		
               
        	   $query = ibase_prepare($trans, $QueryText);
        	   ibase_execute($query);
        	   ibase_free_query($query);
 		       $queryFlows = ibase_prepare($trans, $QueryFlowsQuery);
 		       ibase_execute($queryFlows);
        	   ibase_free_query($queryFlows);
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
	}
    
    
    
    /** 
	* @param string $invoiceID
    * @param boolean $isCoursename
	*
	* @return AccountModuleProcedureObject[]
	*/ 
    public function getInvoiceProceduresByInvoiceID($invoiceID, $isCoursename) 
    { 
	   //$COURSENAME = true;
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        //procedure query
	    $ProcedureQueryText =
			"select 
            treatmentproc.id, 
            treatmentproc.nameforplan, 
            treatmentproc.price, 
            treatmentproc.dateclose,
            doctors.firstname, 
            doctors.lastname, 
            doctors.middlename, 
            treatmentproc.proc_count, 
            treatmentproc.discount_flag, 
            invoices.discount_amount,
            treatmentproc.shifr, 
            treatmentproc.healthproc_fk, 
            treatmentproc.name
                from treatmentproc
                left join invoices
                on invoices.id = treatmentproc.invoiceid
                left join doctors
                on doctors.id = treatmentproc.doctor
                where treatmentproc.invoiceid = '$invoiceID'";
        $ProcedureQuery = ibase_prepare($trans, $ProcedureQueryText);
        $ProcedureResult=ibase_execute($ProcedureQuery);
        $rows = array();
		while ($row = ibase_fetch_row($ProcedureResult))
		{ 
            $obj = new AccountModuleProcedureObject;
            $obj->proc_id = $row[0];
            if($isCoursename == true)
            {
                $obj->proc_nameforplan = $row[12];
            }
            else
            {  
                $obj->proc_nameforplan = $row[1];
            }
            $obj->proc_price = $row[2];
            $obj->dateclose = $row[3];
            $obj->doctor_fname = $row[4];
            $obj->doctor_lname = $row[5];
            $obj->doctor_pname = $row[6];
            $obj->proc_count = $row[7];
            $obj->discount_flag = $row[8];
            $obj->discount_amount = $row[9];
            $obj->proc_shifr = $row[10];
            $obj->healthproc_fk = $row[11];
            $rows[] = $obj;
		}
        ibase_free_query($ProcedureQuery);
        ibase_free_result($ProcedureResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;
    }
    //----------------------------------------------------- INVOICE PROCEDURES END ---------------------------------------------------------------
    
    
    
// ------- DEBTOR & LENDERS MODULE PROCEDURES START ----------------------
    /** 
	* @param string $patientId
    * @param int $flow_isCashpayment
    * @param float $disregardDebtValue
    * @param float $disregardMaxDebtValue
    * @param int $errorsCheck
	* @param string $subdivId
    * 
	* @return AccountModuleDebtorObject[]
	*/ 
    public function getDebtors($patientId, $flow_isCashpayment, $disregardDebtValue, $disregardMaxDebtValue, $errorsCheck, $subdivId) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        //procedure query
	    /*
        $ProcedureQueryText =
			"select          
                patients.id,
                patients.lastname,
                patients.firstname, 
                patients.middlename,
                patients.mobilenumber,
                patients.telephonenumber,
                
             coalesce(cast((select sum(invoices.summ) from invoices
                            where invoices.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and invoices.is_cashpayment = $flow_isCashpayment ";
                            $ProcedureQueryText .= ") as decimal(15,2)),0),
             coalesce(cast((select sum(accountflow.summ) from accountflow
                            where accountflow.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and accountflow.is_cashpayment =  $flow_isCashpayment ";
                            $ProcedureQueryText .= ") as decimal(15,2)),0),

                (select first 1 accountflow.summ||'|'||accountflow.operationtimestamp from accountflow
                    where accountflow.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment ";
                            $ProcedureQueryText .= "order by accountflow.operationtimestamp desc),
        
             (select count(*)
                            from invoices
                            
                            inner  join accountflow
                                on accountflow.summ = invoices.summ
                                and accountflow.patientid = invoices.patientid
                            
                            where ( (datediff (day, invoices.createdate, accountflow.operationtimestamp) < 1) or 
                                (datediff (day, invoices.createdate, accountflow.operationtimestamp) > -1) )
                            and invoices.is_cashpayment != accountflow.is_cashpayment
                            and invoices.patientid = patients.id)

                from invoices
                left join patients on patients.id = invoices.patientid
                where patients.id is not null ";
            
        if(($patientId != null)&&($patientId != '')&&($patientId != 'null'))
        {
            $ProcedureQueryText .= "and patients.id = $patientId ";
        }
        
        
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= "and invoices.is_cashpayment = $flow_isCashpayment ";
        }
        
        $ProcedureQueryText .= "
                
                   
                group by patients.id,
                         patients.lastname,
                         patients.firstname,
                         patients.middlename,
                         patients.mobilenumber,
                         patients.telephonenumber
                   
                   having
                        coalesce(cast((select sum(invoices.summ) from invoices
                            where invoices.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and invoices.is_cashpayment = $flow_isCashpayment ";
                            $ProcedureQueryText .= ") as decimal(15,2)),0)
                        -
                        coalesce(cast((select sum(accountflow.summ) from accountflow
                            where accountflow.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment ";
                            $ProcedureQueryText .= ") as decimal(15,2)),0) > $disregardDebtValue

         order by patients.lastname 
            ";
         */
         
         $ProcedureQueryText =  "select

                patients.id,
                patients.lastname,
                patients.firstname,
                patients.middlename,
                patients.mobilenumber,
                patients.telephonenumber,
                b.inv_summ,
                coalesce(a.acc_summ, 0) ";
         if($errorsCheck == 1) 
            $ProcedureQueryText .= ", (select count(*)
                            from invoices
                            
                            inner  join accountflow
                                on accountflow.summ = invoices.summ
                                and accountflow.patientid = invoices.patientid
                            
                            where   ( 
                                      (datediff (day, invoices.createdate, accountflow.operationtimestamp) < 1) or 
                                      (datediff (day, invoices.createdate, accountflow.operationtimestamp) > -1) 
                                    )
                            and invoices.is_cashpayment != accountflow.is_cashpayment
                            and invoices.patientid = patients.id)";
                
        $ProcedureQueryText .= "from 
                
                (select invoices.patientid, sum(invoices.summ) as inv_summ from invoices
                where invoices.patientid is not NULL ";
        if($flow_isCashpayment != -1)
            $ProcedureQueryText .= " and invoices.is_cashpayment = $flow_isCashpayment ";
            
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $ProcedureQueryText .= " and invoices.SUBDIVISIONID = $subdivId ";
        
        $ProcedureQueryText .= " group by invoices.patientid) as b
                
                left join
                
                (select accountflow.patientid";
                
        if($flow_isCashpayment != -1)
        {    
                $ProcedureQueryText .= ", accountflow.is_cashpayment";
        }
                
        $ProcedureQueryText .= ", sum(accountflow.summ) as acc_summ from accountflow
                where accountflow.patientid is not NULL ";
                
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $ProcedureQueryText .= " and accountflow.SUBDIVISIONID = $subdivId ";
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment ";
        }
        $ProcedureQueryText .= " group by accountflow.patientid";
        
        if($flow_isCashpayment != -1)
        {    
                $ProcedureQueryText .= ", accountflow.is_cashpayment";
        }
        
        $ProcedureQueryText .= ") as a
                
                on b.patientid = a.patientid
                
                left join patients on patients.id = b.patientid
                
                
                where b.inv_summ - coalesce(a.acc_summ, 0) > $disregardDebtValue ";
        if($disregardMaxDebtValue != -1)
        {
            $ProcedureQueryText .= " and b.inv_summ - coalesce(a.acc_summ, 0) < $disregardMaxDebtValue ";   
        }
         if(($patientId != null)&&($patientId != '')&&($patientId != 'null'))
        {
            $ProcedureQueryText .= " and patients.id = $patientId ";
        }
        
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= " and a.is_cashpayment = $flow_isCashpayment ";
        }
        
        $ProcedureQueryText .= " order by patients.lastname";
        

        //file_put_contents("C:/tmp.txt", $ProcedureQueryText);
                
        $ProcedureQuery = ibase_prepare($trans, $ProcedureQueryText);
        $ProcedureResult=ibase_execute($ProcedureQuery);
        $rows = array();
		while ($row = ibase_fetch_row($ProcedureResult))
		{ 
            $obj = new AccountModuleDebtorObject;
            $obj->patient_id = $row[0];
            $obj->patient_lname = $row[1];
            $obj->patient_fname = $row[2];
            $obj->patient_sname = $row[3];
            $obj->patient_mobile = $row[4];
            $obj->patient_phone = $row[5];
            $obj->work_sum = $row[6];
            $obj->balance_sum = $row[7];
            $obj->lastPaid = "-1";//$row[8];
            //$obj->mobiles = array();
            $obj->mobiles = $this->getMobilePhones($obj->patient_id, $trans);
            //if($row[9] != 0)
            if($errorsCheck == 1)
            {
                if($row[8] != 0)
                {
                    $obj->attentionFlag = true; 
                }
                else
                {
                    $obj->attentionFlag = false; 
                }
            }
            else
            {
                $obj->attentionFlag = false;
            }
            $rows[] = $obj;
		}
        ibase_free_query($ProcedureQuery);
        ibase_free_result($ProcedureResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;
    }
     /** 
	* @param string $patientId
    * @param int $flow_isCashpayment
    * @param float $disregardDebtValue
    * @param float $disregardMaxDebtValue
    * @param int $errorsCheck
	* @param string $subdivId
    * 
	* @return AccountModuleDebtorObject[]
	*/  
    public function getLenders($patientId, $flow_isCashpayment, $disregardDebtValue, $disregardMaxDebtValue, $errorsCheck, $subdivId)   
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
        //procedure query
	    /*$ProcedureQueryText =
			"select          
                patients.id,
                patients.lastname,
                patients.firstname, 
                patients.middlename,
                patients.mobilenumber,
                patients.telephonenumber,
                
             coalesce(cast((select sum(invoices.summ) from invoices
                            where invoices.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and invoices.is_cashpayment = $flow_isCashpayment ";
                            $ProcedureQueryText .= ") as decimal(15,2)),0),
             coalesce(cast(sum(accountflow.summ) as decimal(15,2)),0),

                (select first 1 accountflow.summ||'|'||accountflow.operationtimestamp from accountflow
                    where accountflow.patientid = patients.id ";            
                            if($flow_isCashpayment != -1)
                                $ProcedureQueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment ";
                            $ProcedureQueryText .= "
                    order by accountflow.operationtimestamp desc),
        
             (select count(*)
                            from invoices
                            
                            inner  join accountflow
                                on accountflow.summ = invoices.summ
                                and accountflow.patientid = invoices.patientid
                            
                            where ( (datediff (day, invoices.createdate, accountflow.operationtimestamp) < 1) or 
                                (datediff (day, invoices.createdate, accountflow.operationtimestamp) > -1) )
                            and invoices.is_cashpayment != accountflow.is_cashpayment
                            and invoices.patientid = patients.id)

                from accountflow
                left join patients on patients.id = accountflow.patientid
                where patients.id is not null ";
            
        if(($patientId != null)&&($patientId != '')&&($patientId != 'null'))
        {
            $ProcedureQueryText .= "and patients.id = $patientId ";
        }
        
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= "and accountflow.is_cashpayment = $flow_isCashpayment ";
        }
        
        $ProcedureQueryText .= "
                
                   group by patients.id,
                         patients.lastname,
                         patients.firstname,
                         patients.middlename,
                         patients.mobilenumber,
                         patients.telephonenumber
                   
                   having

                        coalesce(cast(sum(accountflow.summ) as decimal(15,2)),0)
                            - ";
        if($flow_isCashpayment != -1){
            $ProcedureQueryText .= "coalesce(cast((select sum(invoices.summ) from invoices
                            where invoices.patientid = patients.id and invoices.is_cashpayment = $flow_isCashpayment) 
                            as decimal(15,2)),0) > $disregardDebtValue

            order by patients.lastname";
        }else {
            $ProcedureQueryText .= "coalesce(cast((select sum(invoices.summ) from invoices
                            where invoices.patientid = patients.id) as decimal(15,2)),0) > $disregardDebtValue
            order by patients.lastname";
        }*/
        
         $ProcedureQueryText =  "select

                patients.id,
                patients.lastname,
                patients.firstname,
                patients.middlename,
                patients.mobilenumber,
                patients.telephonenumber,
                b.inv_summ,
                coalesce(a.acc_summ, 0)";
         if($errorsCheck == 1) 
            $ProcedureQueryText .= ", (select count(*)
                            from invoices
                            
                            inner  join accountflow
                                on accountflow.summ = invoices.summ
                                and accountflow.patientid = invoices.patientid
                            
                            where ( (datediff (day, invoices.createdate, accountflow.operationtimestamp) < 1) or 
                                (datediff (day, invoices.createdate, accountflow.operationtimestamp) > -1) )
                            and invoices.is_cashpayment != accountflow.is_cashpayment
                            and invoices.patientid = patients.id)";
                
        $ProcedureQueryText .= "from 
                
                (select invoices.patientid, sum(invoices.summ) as inv_summ from invoices
                where invoices.patientid is not NULL ";
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= " and invoices.is_cashpayment = $flow_isCashpayment ";
        }
        
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $ProcedureQueryText .= " and invoices.SUBDIVISIONID = $subdivId ";
            
        $ProcedureQueryText .= " group by invoices.patientid) as b
                
                left join
                
                (select accountflow.patientid";
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= ", accountflow.is_cashpayment";
        }        
                
        $ProcedureQueryText .= ", sum(accountflow.summ) as acc_summ from accountflow
                where accountflow.patientid is not NULL ";
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment ";
        }
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $ProcedureQueryText .= " and accountflow.SUBDIVISIONID = $subdivId ";
            
        $ProcedureQueryText .= "group by accountflow.patientid";
        
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= ", accountflow.is_cashpayment ";
        }
        $ProcedureQueryText .= ") as a
                
                on b.patientid = a.patientid
                
                left join patients on patients.id = b.patientid
                
                
                where b.inv_summ - coalesce(a.acc_summ, 0) < -$disregardDebtValue ";
        if($disregardMaxDebtValue != -1)
        {
            $ProcedureQueryText .= " and b.inv_summ - coalesce(a.acc_summ, 0) > -$disregardMaxDebtValue ";   
        }
        
        if(($patientId != null)&&($patientId != '')&&($patientId != 'null'))
        {
            $ProcedureQueryText .= " and patients.id = $patientId ";
        }
        
        if($flow_isCashpayment != -1)
        {
            $ProcedureQueryText .= " and a.is_cashpayment = $flow_isCashpayment ";
        }
        
        $ProcedureQueryText .= " order by patients.lastname";
                        
        
        $ProcedureQuery = ibase_prepare($trans, $ProcedureQueryText);
        $ProcedureResult=ibase_execute($ProcedureQuery);
        $rows = array();
		while ($row = ibase_fetch_row($ProcedureResult))
		{ 
            $obj = new AccountModuleDebtorObject;
            $obj->patient_id = $row[0];
            $obj->patient_lname = $row[1];
            $obj->patient_fname = $row[2];
            $obj->patient_sname = $row[3];
            $obj->patient_mobile = $row[4];
            $obj->patient_phone = $row[5];
            $obj->work_sum = $row[6];
            $obj->balance_sum = $row[7];
            $obj->lastPaid = "-1";//$row[8];
            //$obj->mobiles = array();
            $obj->mobiles = $this->getMobilePhones($obj->patient_id, $trans);
            
            //if($row[9] != 0)
            if($errorsCheck == 1)
            {
                if($row[8] != 0)
                {
                    $obj->attentionFlag = true; 
                }
                else
                {
                    $obj->attentionFlag = false; 
                }
            }
            else
            {
                $obj->attentionFlag = false;
            }
            $rows[] = $obj;
		}
        ibase_free_query($ProcedureQuery);
        ibase_free_result($ProcedureResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;
    }
// ------- DEBTOR & LENDERS MODULE PROCEDURES END ----------------------

// ------- ACCOUNT MODULE PROCEDURES START ----------------------

    /** 
    * @param string $startDate
    * @param string $endDate
    * @param string $patientId
    * @param int $flow_isCashpayment
    * @param int $groupRules
    * @param boolean $isDirector
    * @param string $filterGroupId
    * @param string $accountId
    * @param int $onlyCurrentSubdivision
    * @param string $subdivision
    * @param string $userIDOnly
	* @return AccountModule_AccountData
	*/ 
    public function getAccountData($startDate, $endDate, $patientId, $flow_isCashpayment, $groupRules, $isDirector, $filterGroupId, $accountId, $onlyCurrentSubdivision, $subdivision, $userIDOnly) 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $accountData = new AccountModule_AccountData;
        //account summ
        if($isDirector == true)
        {
            $QueryText = "select coalesce(sum(accountflow.summ),0) from accountflow ";
            if($subdivision != 0)
            {
                $QueryText .= "inner join subdivision
                                on subdivision.id = accountflow.subdivisionid and subdivision.id = $subdivision ";
            }
            $QueryText .= " where accountflow.id is not null and accountflow.operationtype != 5";
            if($flow_isCashpayment != -1)
            {
                $QueryText .= " and accountflow.is_cashpayment = $flow_isCashpayment ";
            }
            if(($endDate != 'null')&&($endDate != null)&&($endDate != '')&&($endDate != '-'))
            {
                $QueryText .= "  and accountflow.operationtimestamp >= '$startDate 00:00:00'
                    and accountflow.operationtimestamp <= '$endDate 23:59:59'";  
            }     
            
    		$query = ibase_prepare($trans, $QueryText);
    		$result=ibase_execute($query);
            $resultArray=ibase_fetch_row($result);
            $accountData->account_sum = $resultArray[0];
        }
        else
        {
            $accountData->account_sum = 0;
        }
        //flow objects query
	    $FlowQueryText =
			"select
                accountflow.id,
                accountflow.patientid,
                
                af_patient.cardbefornumber,
                af_patient.cardnumber,
                af_patient.cardafternumber,
                af_patient.lastname,
                af_patient.firstname,
                af_patient.middlename,
                
                accountflow.summ,
                accountflow.operationtype,
                accountflow.operationnote,    
                accountflow.operationtimestamp,
                accountflow.oninvoice_fk,
                invoices.invoicenumber,
                invoices.createdate,
                accountflow.is_cashpayment,
                
                accountflow_groups.rules,
                accountflow_groups.name,
                
                doctors.id,
                doctors.lastname,
                doctors.firstname,
                doctors.middlename,
                doctorsawards.notice,
                doctorsawards.datefoundaward,

                accountflow_texp_groups.id,
                accountflow_texp_groups.name,
                accountflow_texp_groups.color,
                treatmentproc.id,  
                treatmentproc.dateclose,  
                treatmentproc.name,
                treatmentproc.shifr,  
                tp_doctors.firstname,   
                tp_doctors.lastname,
                tp_doctors.middlename,
                tp_patient.firstname,
                tp_patient.lastname,
                tp_patient.middlename,
                accountflow.ordernumber,
                                     
                account.id,
                account.lastname,
                account.firstname,
                account.middlename,
                subdivision.id,
                subdivision.name,
                
                certificate.id,
                certificate.number,
                
                farmdoc.id,
                farmdoc.doctype,
                farmdoc.docnumber,
                farmdoc.docdate,
                farmdoc.patient_fk,
                (select patients.shortname from patients where patients.id = farmdoc.patient_fk)
                
                
                from accountflow

                    left join patients as af_patient
                    on accountflow.patientid = af_patient.id
                    left join invoices
                    on accountflow.oninvoice_fk = invoices.id
                    
                    left join accountflow_groups
                    on accountflow.group_fk = accountflow_groups.id

                    left join account 
                     on account.id=accountflow.authorid
                     
                    left join doctorsawards
                    on accountflow.award_fk = doctorsawards.id
                    left join doctors
                    on doctorsawards.doctorfc = doctors.id
                    

                
                    left join accountflow_texp_groups
                    on accountflow.texp_group_fk = accountflow_texp_groups.id
                    left join treatmentproc
                    on accountflow.texp_treatmentproc_fk = treatmentproc.id
                    left join doctors as tp_doctors
                    on treatmentproc.doctor = tp_doctors.id
                    left join patients as tp_patient
                    on treatmentproc.patient_fk = tp_patient.id
                    left join subdivision
                    on subdivision.id = coalesce(accountflow.subdivisionid,(select first 1 id from subdivision))
                    left join certificate
                    on certificate.id = accountflow.certificateid
                    left join farmdoc
                    on farmdoc.accountflow_fk = accountflow.id

            where accountflow.id is not null ";
        
        if(($endDate != 'null')&&($endDate != null)&&($endDate != '')&&($endDate != '-'))
        {
            $FlowQueryText .= "and accountflow.operationtimestamp >= '$startDate 00:00:00'
                and accountflow.operationtimestamp <= '$endDate 23:59:59' ";
        }  
        
        if(($patientId != 'null')&&($patientId != null)&&($patientId != '')&&($patientId != '-'))
        {
            $FlowQueryText .= "and af_patient.id = $patientId ";
        }   
        
        if($flow_isCashpayment != -1)
        {
            $FlowQueryText .= "and accountflow.is_cashpayment = $flow_isCashpayment ";
        } 
        
        if($groupRules != -1)//if all -> general, director, salary
        {
            $FlowQueryText .= "and ((accountflow_groups.rules is null) or (accountflow_groups.rules = $groupRules)) ";
                               
            if($groupRules != 1) // if not director -> exclude salary
            {
                $FlowQueryText .= "and accountflow.operationtype != 2 ";
            }
        }
        if(($filterGroupId != "-")&&($filterGroupId != null)&&($filterGroupId != "null"))
        {
            if($filterGroupId == "salary")
            {
                $FlowQueryText .= "and accountflow.operationtype = 2 ";
            }
            else if($filterGroupId == "storage")
            {
                $FlowQueryText .= "and accountflow.operationtype = 4 ";
            }
            else
            {
                $FlowQueryText .= "and accountflow.group_fk = $filterGroupId ";
            }
        }
        
        if($userIDOnly != null && $userIDOnly != '' && $userIDOnly != '0')
        {
                $FlowQueryText.= " and accountflow.authorid = $userIDOnly";
        }
        else if(nonull($accountId))
        {
                $FlowQueryText.= " and accountflow.authorid = $accountId";
        }
        
        if($onlyCurrentSubdivision==1)
        {
            $FlowQueryText.= " and (accountflow.subdivisionid is null or accountflow.subdivisionid=(select id from current_subdivision)) ";
        }  
        else if($subdivision != null && $subdivision != "" && $subdivision != "0")
        {
            $FlowQueryText.= " and subdivision.id = $subdivision ";
        }      
        $FlowQueryText .= "order by accountflow.operationtimestamp desc";
        $FlowQuery = ibase_prepare($trans, $FlowQueryText);
        $FlowResult=ibase_execute($FlowQuery);
        $rows = array();
		while ($row = ibase_fetch_row($FlowResult))
		{ 
            $obj = new AccountModule_AccountObject;
            $obj->flow_id = $row[0];
            $obj->patient_id = $row[1];
            $obj->patient_cardnumber = $row[2].$row[3].$row[4];
            $obj->patient_lname = $row[5];
            $obj->patient_fname = $row[6];
            $obj->patient_sname = $row[7];
            $obj->flow_sum = $row[8];
            $obj->flow_operationtype = $row[9];
            $obj->flow_operationnote = $row[10];
            $obj->flow_createdate = $row[11];
            $obj->flow_oninvoice_fk = $row[12];
            $obj->invoice_number = $row[13];
            $obj->invoice_createdate = $row[14];
            $obj->flow_isCashpayment = $row[15];
            $obj->flow_ordernumber = $row[37];
            $obj->flow_groupName = $row[17];
            
            if($row[18])
            {
                $obj->flow_award_personId = $row[18];
                $obj->flow_award_personFio = $row[19].' '.$row[20];
                
                $obj->flow_operationnote = $row[22];
                $obj->flow_award_foundDate = $row[23];
            }
            
            if( ($row[9] == 3)||($row[9] == '3') )
            {
                $obj->exp_group_id = $row[24];
                $obj->exp_group_name = $row[25];
                $obj->exp_group_color = $row[26];
                $obj->exp_proc_id = $row[27];
                $obj->exp_proc_dateclose = $row[28];
                $obj->exp_proc_name = $row[29];
                $obj->exp_proc_shifr = $row[30];
                $obj->exp_doctor_fname = $row[31];
                $obj->exp_doctor_lname = $row[32];
                $obj->exp_doctor_sname = $row[33];
                $obj->exp_patient_fname = $row[34];
                $obj->exp_patient_lname = $row[35];
                $obj->exp_patient_sname = $row[36];
             
            }
            
            $obj->author_id= $row[38];
            $obj->author_shortname = shortname($row[39],$row[40],$row[41]); 
            $obj->subdivision_id = $row[42];
            $obj->subdivision_name = $row[43];
            $obj->certificate_id = $row[44];
            $obj->certificate_number = $row[45];
            
            //storgae
            $obj->storage_docId = $row[46];
            $obj->storage_docType = $row[47];
            $obj->storage_docNumber = $row[48];
            $obj->storage_docDate = $row[49];
            $obj->storage_patientId = $row[50];
            $obj->storage_patientShortname = $row[51];
            
            $accountData->flowData[] = $obj;
		}
        ibase_free_query($FlowQuery);
        ibase_free_result($FlowResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $accountData;
    }
  
  /** 
  * @param number $summ
  * @param string $operationnote
  * @param string $timestamp
  * @param int $is_cashpayment
  * @param string $group_id
  * @param string $cardId
  * @param string $certificateId
  * @return boolean
  */  
  public function addMoneyToAccount($summ, $operationnote, $timestamp, $is_cashpayment, $group_id, $cardId, $certificateId)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into accountflow (accountflow.id, 
                                accountflow.summ, accountflow.operationtype, 
                                accountflow.operationnote, accountflow.operationtimestamp, accountflow.is_cashpayment, accountflow.group_fk, accountflow.discountcardid,accountflow.certificateid)
                    values(null, '$summ', 1, '".safequery($operationnote)."', ";
        if(($timestamp != null)&&($timestamp != 'null')&&($timestamp != '')&&($timestamp != '-'))
        {
             $QueryText .= "'".$timestamp."', ";
        }
        else
        {
            $QueryText .= "null, ";
        }           
        $QueryText .= "$is_cashpayment, $group_id, ".nullcheck($cardId).", ".nullcheck($certificateId).")";         
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  }
  
  /** 
  * @param string $patientid
  * @param number $summ
  * @param string $operationnote
  * @param string $timestamp
  * @param int $is_cashpayment
  * @return boolean
  */  
  public function cancelPatientDebtAccount($patientid, $summ, $operationnote, $timestamp, $is_cashpayment)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into accountflow (
                                        
                                        accountflow.id, 
                                        accountflow.patientid,
                                        accountflow.summ, 
                                        accountflow.operationtype, 
                                        accountflow.operationnote, 
                                        accountflow.operationtimestamp, 
                                        accountflow.is_cashpayment )
                    values(
                                        null, 
                                        $patientid,
                                        '$summ', 
                                        5, 
                                        '".safequery($operationnote)."', ";
        
        if(($timestamp != null)&&($timestamp != 'null')&&($timestamp != '')&&($timestamp != '-'))
        {
             $QueryText .= "'".$timestamp."', ";
        }
        else
        {
            $QueryText .= "null, ";
        }           
        
        $QueryText .= "$is_cashpayment)";  
               
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  }
  
  /** 
  * @param string $patientID
  * @param number $summ
  * @param string $operationnote
  * @param string $timestamp
  * @param string $invoiceID
  * @param int $is_cashpayment
  * @param string $cardId
  * @param string $certificateId
  * @return number
  */  
  public function addPatientMoney($patientID, $summ, $operationnote, $timestamp, $invoiceID, $is_cashpayment, $cardId, $certificateId)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if(($invoiceID == '')||($invoiceID == 'null')||($invoiceID == null))
        {
            $invoiceID = 'null';
        }
            
		$QueryText = "insert into accountflow (accountflow.id, accountflow.patientid, accountflow.summ, 
                                            accountflow.operationtype, accountflow.operationnote, accountflow.operationtimestamp, 
                                            accountflow.oninvoice_fk, accountflow.ordernumber, accountflow.is_cashpayment, accountflow.discountcardid,accountflow.certificateid)
                    values(null, $patientID, '$summ', 0, '".safequery($operationnote)."', ";
        if(($timestamp != null)&&($timestamp != 'null')&&($timestamp != '')&&($timestamp != '-'))
        {
             $QueryText .= "'".$timestamp."', ";
        }
        else
        {
            $QueryText .= "null, ";
        }                 
        $QueryText .= "$invoiceID, null, $is_cashpayment, ".nullcheck($cardId).", ".nullcheck($certificateId).") returning id";
            
        $query = ibase_prepare($trans, $QueryText);
        $resultQuery = ibase_execute($query);
        $rowIdOrder = ibase_fetch_row($resultQuery);
        $orderID = $rowIdOrder[0];
        ibase_free_query($query);
        ibase_free_result($resultQuery);
           
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $orderID;
  }
    
  
  /**
   * @param string $fio
   * @param boolean $includePhoneSearch
   * @return AccountModule_PatientObject[]
   */
   public function getPatientLazyList($fio, $includePhoneSearch)
   {
    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select patients.ID, 
                    patients.FIRSTNAME, patients.MIDDLENAME, patients.LASTNAME, patients.fullname,
                    patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER, patients.MOBILENUMBER
                 from patients ";
        if(($includePhoneSearch == true)&&(is_numeric($fio)))
        {
            $QueryText .= "inner join patientsmobile
                    on patients.id = patientsmobile.patient_fk ";
        }
        $QueryText .= "where 
                    ( lower( patients.LASTNAME||' '||patients.FIRSTNAME||' '||patients.MIDDLENAME) starting lower('$fio')
                    or lower(patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER) containing lower('$fio') ";
        if(($includePhoneSearch == true)&&(is_numeric($fio)))
        {
            $QueryText .= "or lower(patientsmobile.mobilenumber) containing lower('$fio') ) ";
        }
        else
        {
            $QueryText .= " ) ";
        }
        $QueryText .= "and PRIMARYFLAG = 1 and IS_ARCHIVED = 0 order by patients.LASTNAME";
                //patients.MOBILENUMBER
        $Query = ibase_prepare($trans, $QueryText);
        $QueryResult=ibase_execute($Query);
        $rows = array();
		while ($row = ibase_fetch_row($QueryResult))
		{ 
            $obj = new AccountModule_PatientObject;
            $obj->patient_id = $row[0];
            $obj->patient_lname = $row[1];
            $obj->patient_fname = $row[2];
            $obj->patient_sname = $row[3];
            $obj->patient_fio = $row[4];
            $obj->patient_label = $row[4].' ('.$row[5].') ';
            if($includePhoneSearch == true)
            {
                //$obj->patient_label .= $row[6];
            }
            $rows[] = $obj;
		}
        ibase_free_query($Query);
        ibase_free_result($QueryResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
   }
   /**
   * @param string $patientId
   * @return AccountModule_PatientObject
   */
   public function getPatientObjectById($patientId)
   {
    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select patients.ID, 
                    patients.FIRSTNAME, patients.MIDDLENAME, patients.LASTNAME, patients.fullname,
                    patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER, patients.MOBILENUMBER
                 from patients
                 where patients.id = $patientId";
        $Query = ibase_prepare($trans, $QueryText);
        $QueryResult=ibase_execute($Query);
		$row = ibase_fetch_row($QueryResult);
		$obj = new AccountModule_PatientObject;
        $obj->patient_id = $row[0];
        $obj->patient_lname = $row[1];
        $obj->patient_fname = $row[2];
        $obj->patient_sname = $row[3];
        $obj->patient_fio = $row[4];
        $obj->patient_label = $obj->patient_fio.' ('.$row[5].') '.$row[6];
        ibase_free_query($Query);
        ibase_free_result($QueryResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $obj;             
   }
   
  	/**
	* @param string $accountObjectId
	* @param string $passForDelete
	* @return boolean
 	*/ 		 		
	public function deleteAccountObjectById($accountObjectId, $passForDelete) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               ibase_free_result($result);
        	   ibase_free_query($query);
        	   $QueryText = "delete from accountflow where accountflow.id = '$accountObjectId'";
        	   $query = ibase_prepare($trans, $QueryText);
        	   ibase_execute($query);
        	   ibase_free_query($query);
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
	}
    
    
       
    ///************************************************************************************************///
    ///                         SEPARATE PROCEDURE FUNCTIONS                                                  ///
    ///                                START                                                             ///
    ///************************************************************************************************///
      /**
     * @param string $procId
     * @param int $separatedCount
     * @return boolean
     */
    public function separateProcedureCount($procId, $separatedCount)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "execute procedure separate_procedures($procId, $separatedCount)";
        $Query = ibase_prepare($trans, $QueryText);
        ibase_execute($Query);
        $result = ibase_commit($trans);
        ibase_free_query($Query);
        
        $connection->CloseDBConnection();
        return $result;
    }
       /**
     * @param string $first_Id
     * @param string $second_Id
     * @return boolean
     */
    public function uniteProcedureCount($first_Id, $second_Id)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "execute procedure unite_procedures($second_Id, $first_Id)";
        $Query = ibase_prepare($trans, $QueryText);
        ibase_execute($Query);
        $result = ibase_commit($trans);
        ibase_free_query($Query);
        
        $connection->CloseDBConnection();
        return $result;
    }
    
    
    /*
     * @param string $proc_Id
     * @param number $price
     * @return boolean
     *
    public function changeProcedurePrice($proc_Id, $price)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update treatmentproc set treatmentproc.price = '$price' where treatmentproc.id = '$proc_Id'";
        $Query = ibase_prepare($trans, $QueryText);
        ibase_execute($Query);
        $result = ibase_commit($trans);
        ibase_free_query($Query);
        
        $connection->CloseDBConnection();
        return $result;
    }*/
    ///************************************************************************************************///
    ///                         SEPARATE PROCEDURE FUNCTIONS                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    
// ------- ACCOUNT MODULE PROCEDURES END ----------------------

    ///************************************************************************************************///
    ///                         FLOW GROUP                                                  ///
    ///                                START                                                             ///
    ///************************************************************************************************///

    /** 
    * @param int $groupRules
    * @param int $groupTypes
    * @param int $isCashpayment
    * @param boolean $isSalary
    * @param boolean $isEmptyGroup
    * @return AccountModule_Group[]
    */  
    public function getGroups($groupRules, $groupTypes, $isCashpayment, $isSalary, $isEmptyGroup) //sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select
                        accountflow_groups.id,
                        accountflow_groups.name,
                        accountflow_groups.group_type,
                        accountflow_groups.rules,
                        accountflow_groups.is_cashpayment,
                        accountflow_groups.monthly_is,
                        accountflow_groups.monthly_defaultsumm,
                        accountflow_groups.color,
                        accountflow_groups.gridsequence,
                        accountflow_groups.residue,
                        accountflow_groups.isemployee
                    from accountflow_groups ";
        
        if($groupRules != -1)
        {
            $QueryText .= " where accountflow_groups.rules = $groupRules ";
        }
        else
        {
            $QueryText .= " where accountflow_groups.id is not null ";
        }       
         
        if($isCashpayment != -1)
        {
            $QueryText .= " and accountflow_groups.is_cashpayment = $isCashpayment";
        }
        
        if($groupTypes != -1)
        {
            $QueryText .= " and accountflow_groups.group_type = $groupTypes";
        }           
        $QueryText .= " order by accountflow_groups.is_cashpayment asc, accountflow_groups.name asc";                                        
        
        $Query = ibase_prepare($trans, $QueryText);
        $QueryResult=ibase_execute($Query);
        $rows = array();
        
        if($isEmptyGroup == true)
        {
            $emptyGroup = new AccountModule_Group;
            $emptyGroup->id = '-';
            $emptyGroup->name = 'emptyGroup';
            $rows[] = $emptyGroup; 
        }
        
        if($isSalary == true)
        {
            $salaryGroup = new AccountModule_Group;
            $salaryGroup->id = 'salary';
            $salaryGroup->name = 'salaryGroup';
            $rows[] = $salaryGroup; 
        }
        
        /*
        //if($isSalary == true)
        //{
            $storageGroup = new AccountModule_Group;
            $storageGroup->id = 'storage';
            $storageGroup->name = 'storageGroup';
            $rows[] = $storageGroup; 
        //}
        */
		while ($row = ibase_fetch_row($QueryResult))
		{ 
            $obj = new AccountModule_Group;
            $obj->id = $row[0];
            $obj->name = $row[1];
            $obj->group_type = $row[2];
            $obj->rules = $row[3];
            $obj->is_cashpayment = $row[4];
            $obj->monthly_is = $row[5];
            $obj->monthly_defaultsumm = $row[6];
            $obj->group_color = $row[7];
            $obj->group_sequince = $row[8];
            $obj->group_residue = $row[9];
            $obj->isEmployee = $row[10];
            
            $rows[] = $obj;
		}
        ibase_free_query($Query);
        ibase_free_result($QueryResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
    }
    
    /**
     * @return AccountModule_Doctor[]
     */
    public function getDoctors() //sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $QueryText = "select
                            doctors.id,
                            doctors.lastname,
                            doctors.firstname,
                            doctors.middlename
                        from doctors";
            // WHERE (DATEWORKEND is null) or (DATEWORKEND >= '$nowDate')";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $doctors = array();
            while ($row = ibase_fetch_row($result))
            {
                $doc = new AccountModule_Doctor();
                $doc->id = $row[0];
                $doc->lname = $row[1];
                $doc->fname = $row[2];
                $doc->sname = $row[3];
                $doc->shortname = $row[1].' '.$row[2];
                $doctors[] = $doc;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $doctors;
    }
    ///************************************************************************************************///
    ///                         FLOW GROUP                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    
    
    ///************************************************************************************************///
    ///                         TRANSACTION                                                  ///
    ///                                START                                                             ///
    ///************************************************************************************************///
  /** 
  * @param string $summ_from
  * @param string $summ_to
  * @param string $summ_residue
  * 
  * @param string $groupId_transaction
  * @param string $groupId_residue
  * 
  * @param string $operationnote
  * @param string $timestamp
  * 
  * @param int $is_cashpayment
  * @return boolean
  */  
  public function transactionAccount($summ_from, $summ_to, $summ_residue, $groupId_transaction, $groupId_residue,
                                        $operationnote, $timestamp, $is_cashpayment)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        // from query start
		$from_sql = "insert into accountflow (
                                accountflow.id, 
                                accountflow.summ, 
                                accountflow.operationtype, 
                                accountflow.operationnote, 
                                accountflow.operationtimestamp, 
                                accountflow.is_cashpayment, 
                                accountflow.group_fk)
                    values(
                    null, 
                    '-$summ_from', 
                    1, 
                    '".safequery($operationnote)."', ";
        if(($timestamp != null)&&($timestamp != 'null')&&($timestamp != '')&&($timestamp != '-'))
        {
             $from_sql .= "'".$timestamp."', ";
        }
        else
        {
            $from_sql .= "null, ";
        }           
        $from_sql .= " $is_cashpayment, 
                        $groupId_transaction)";         
        $from_query = ibase_prepare($trans, $from_sql);
		ibase_execute($from_query);
		ibase_free_query($from_query);
        // from query end
        
        // to query start
		$to_sql = "insert into accountflow (
                                accountflow.id, 
                                accountflow.summ, 
                                accountflow.operationtype, 
                                accountflow.operationnote, 
                                accountflow.operationtimestamp, 
                                accountflow.is_cashpayment, 
                                accountflow.group_fk)
                    values(
                    null, 
                    '$summ_to', 
                    1, 
                    '".safequery($operationnote)."', ";
        if(($timestamp != null)&&($timestamp != 'null')&&($timestamp != '')&&($timestamp != '-'))
        {
             $to_sql .= "'".$timestamp."', ";
        }
        else
        {
            $to_sql .= "null, ";
        }  
        $to_sql .= (1 - $is_cashpayment).", 
                        $groupId_transaction)";         
        $to_query = ibase_prepare($trans, $to_sql);
		ibase_execute($to_query);
		ibase_free_query($to_query);
        // to query end
        
        // residue query start
        if( ($groupId_residue != null)&&($groupId_residue != 'null')&&($groupId_residue != '') )
        { 
    		$residue_sql = "insert into accountflow (
                                    accountflow.id, 
                                    accountflow.summ, 
                                    accountflow.operationtype, 
                                    accountflow.operationnote, 
                                    accountflow.operationtimestamp, 
                                    accountflow.is_cashpayment, 
                                    accountflow.group_fk)
                        values(
                        null, 
                        '-$summ_residue', 
                        1, 
                        '".safequery($operationnote)."', ";
            if(($timestamp != null)&&($timestamp != 'null')&&($timestamp != '')&&($timestamp != '-'))
            {
                 $residue_sql .= "'".$timestamp."', ";
            }
            else
            {
                $residue_sql .= "null, ";
            }  
            $residue_sql .= " $is_cashpayment, 
                            $groupId_residue)";         
            $residue_query = ibase_prepare($trans, $residue_sql);
    		ibase_execute($residue_query);
    		ibase_free_query($residue_query);
        }
        // residue query end
        
        
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  }
    ///************************************************************************************************///
    ///                         TRANSACTION                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    
    /**
    * @param string $invoiceId
    * @return AccountModule_InvoiceCashObject[]
    */    
    public function getInvoicesCashes($invoiceId) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select accountflow.id,
                        accountflow.summ,
                        accountflow.operationtimestamp
                        from accountflow
                        where accountflow.oninvoice_fk = $invoiceId
                        order by accountflow.operationtimestamp desc";			
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new AccountModule_InvoiceCashObject;
			$obj->id = $row[0];
			$obj->summ = $row[1];
			$obj->operationdate = $row[2];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
     /**
    * @param string $patientId
    * @param boolean $isActual
    * @return AccountModule_PatientInsurances[]
    */    
    public function getPatientsInsurances($patientId, $isActual) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select
                        patientsinsurance.id,
                        patientsinsurance.policenumber,
                        patientsinsurance.policefromdate,
                        patientsinsurance.policetodate,
                        insurancecompanys.id, 
                        insurancecompanys.name,
                        insurancecompanys.phone
                        
                    from patientsinsurance
                        inner join insurancecompanys
                        on insurancecompanys.id = patientsinsurance.companyid
                    where patientsinsurance.patientid = $patientId ";
        if($isActual == true)
        {
            //$QueryText .= "and patientsinsurance.policetodate < 'now' ";
        }
        $QueryText .= "order by patientsinsurance.policenumber";
        			
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new AccountModule_PatientInsurances;
			$obj->insurance_id = $row[0];
			$obj->insurance_number = $row[1];
			$obj->insurance_fromDate = $row[2];
			$obj->insurance_toDate = $row[3];
			$obj->company_id = $row[4];
			$obj->company_name = $row[5];
			$obj->company_phone = $row[6];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /** 
	* @param string $flowID
	*
	* @return boolean
	*/ 
    public function changeAccountFlowCashType($flowID) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$flow_sql = "update accountflow set accountflow.is_cashpayment = (1-accountflow.is_cashpayment)
                    where accountflow.id = $flowID";
        			
		$flow_query = ibase_prepare($trans, $flow_sql);
		ibase_execute($flow_query);
		ibase_free_query($flow_query);
		
		$success = ibase_commit($trans);
        
		$connection->CloseDBConnection();		
		return $success; 
    }
    
    
  /** 
  * @param AccountModule_AccountObject $accountObject
  * @return boolean
  */  
  public function updateAccountObject($accountObject)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$update_sql = "update ACCOUNTFLOW set
                            ACCOUNTFLOW.OPERATIONNOTE = '".safequery($accountObject->flow_operationnote)."'
                        where ACCOUNTFLOW.ID = ".$accountObject->flow_id;
             
        $update_query = ibase_prepare($trans, $update_sql);
		ibase_execute($update_query);
		ibase_free_query($update_query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  }
  
  
  
  //---------------------------- Expenses Groups START ------------------------------

    /** 
    * @param boolean $isEmptyGroup
    * @return AccountModule_ExpensesGroup[]
    */  
    public function getExpensesGroups($isEmptyGroup)//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$group_sql = "select 
                            ACCOUNTFLOW_TEXP_GROUPS.ID, 
                            ACCOUNTFLOW_TEXP_GROUPS.NAME, 
                            ACCOUNTFLOW_TEXP_GROUPS.IS_CASHPAYMENT, 
                            ACCOUNTFLOW_TEXP_GROUPS.COLOR, 
                            ACCOUNTFLOW_TEXP_GROUPS.GRIDSEQUENCE
                        from ACCOUNTFLOW_TEXP_GROUPS 
                       
                        order by ACCOUNTFLOW_TEXP_GROUPS.GRIDSEQUENCE asc, ACCOUNTFLOW_TEXP_GROUPS.NAME asc";                                        
        
        $group_query = ibase_prepare($trans, $group_sql);
        $group_result=ibase_execute($group_query);
        $rows = array();
        
        if($isEmptyGroup == true)
        {
            $emptyGroup = new AccountModule_ExpensesGroup;
            $emptyGroup->id = 'null';
            $emptyGroup->name = 'emptyGroup';
            $rows[] = $emptyGroup; 
        }
		while ($row = ibase_fetch_row($group_result))
		{ 
            $obj = new AccountModule_ExpensesGroup;
            $obj->id = $row[0];
            $obj->name = $row[1];
            $obj->is_cashpayment = $row[2];
            $obj->group_color = $row[3];
            $obj->group_sequince = $row[4];
            
            $rows[] = $obj;
		}
        ibase_free_query($group_query);
        ibase_free_result($group_result);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
    }
//---------------------------- Expenses Groups END ------------------------------
      /**
    * @param  string $invoice_id
    * @param  string $invoice_number
     * @param string $invoice_date
     * @return boolean
     */
    public function updateInvoiceDateNumber($invoice_id, $invoice_number, $invoice_date)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update invoices set invoicenumber = $invoice_number, 
                                            createdate = '$invoice_date' 
                                            where ID=$invoice_id";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
    
    
          /**
    * @param  string $flow_id
     * @param string $flow_date
     * @return boolean
     */
    public function updateFlowDate($flow_id, $flow_date)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update accountflow set OPERATIONTIMESTAMP = '$flow_date' where ID=$flow_id";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
    
    /**
    * @param  string $flow_id
     * @param int $flow_ordernumber
     * @return boolean
     */
    public function updateFlowOrderNumber($flow_id, $flow_ordernumber)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update accountflow set ORDERNUMBER = '$flow_ordernumber' where ID=$flow_id";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
	
	
	  /**
     * @param string $paramName  
     * @param string $paramValue  
     * @return boolean
     */
    public static function setApplicationSetting($paramName, $paramValue)
    {
        return setApplicationSetting($paramName, $paramValue);
    } 
    
        /**
     * @param string[] $paramNames  
     * @return string[]
     */
    public static function getApplicationSettings($paramNames)
	{
	   $res=array();
       foreach($paramNames as $paramName)
       {
            $res[]=getApplicationSetting($paramName);
       }
	   
	   return $res;
	} 
}
?>
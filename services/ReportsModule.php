<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "Form037Module.php";
require_once "Form039Module.php";
class ReportsModule_Doctor
{
    /**
	* @var string
	*/
	var $id;
    
    /**
	* @var string
	*/
	var $fname;
    /**
	* @var string
	*/
	var $lname;
    /**
	* @var string
	*/
	var $sname;
    /**
	* @var string
	*/
	var $healthtype;
}


class ReportsModule
{		
	/**
	* @param ReportsModule_Doctor $p1
	*/
	public function registertypes($p1) 
	{
	}
   /** 
 * @param string $docType
 * @param string $doctor_id
 * @param string $startDate
 * @param string $endDate
 * @param int $form
 * @return string
 */  
 public static function createForm($docType, $doctor_id, $startDate, $endDate, $form)
 {
    
    switch($form)
        {   
            case 370:
            case 371:
            case 372:
                return Form037Module::create037($docType, $doctor_id, $startDate, $form);
            case 392:
            case 393:
            case 394:
                return Form039Module::create039($docType, $doctor_id, $startDate, $endDate, $form);
            default:
                return null;
        }
 }

    /**
     * @param string $doctorId
     * @return ReportsModule_Doctor[]
     */
	public function getDoctors($doctorId)//sqlite
	{
		$connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            //doctors
        $QueryText = "select
                        doctors.id,
                        doctors.firstname,
                        doctors.lastname,
                        doctors.middlename,
                        doctors.healthtype
                      from doctors
                      where 
                        (doctors.isfired is null or doctors.isfired != 1)";
        if(nonull($doctorId))
        {
            $QueryText.=' and doctors.id='.$doctorId;
        }
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
            
        $doctors = array();
                
        while ($row = ibase_fetch_row($result))
        {
            $doc = new ReportsModule_Doctor();
            $doc->id = $row[0];
            $doc->fname = $row[1];
            $doc->lname = $row[2];
            $doc->sname = $row[3];
            $doc->healthtype = $row[4];
            $doctors[] = $doc;
        }
        ibase_free_query($query);
        ibase_free_result($result);
        ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $doctors;
	}
        /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteForm($form_path)
    {
        return deleteFileUtils($form_path);
    }
}
?>
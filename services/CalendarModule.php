<?php
require_once "Connection.php";
require_once "Settings.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
//$cm=new CalendarModule;
//$res=$cm->getScheduleOnDoctorsWizard(array('6'),'20.12.2012','20.1.2013');
//print_r($res);
class CalendarModuleTaskItem
{   
    /**
    * @var string
    */
    var $DOCTORID;  
    
    /**
    * @var date
    */
    var $DATE;    
    
    /**
    * @var CalendarModuleInterval[]
    */
    var $INTERVALS;     
    
}
class CalendarModuleIntervalItem
{   
    /**
    * @var Boolean
    */
    var $ISENABLED;
    
    /**
    * @var string[]
    */
    var $DOCTORID;  
    
    /**
    * @var date
    */
    var $STARTDATE;    
    
    /**
    * @var date
    */
    var $ENDDATE;
    
    /**
    * @var date[]
    */
    var $MINDATE;    
    
    /**
    * @var date[]
    */
    var $MAXDATE;
    
    /**
    * @var string
    */
    var $ROOMID;    
    
    /**
    * @var int
    */
    var $CURDOC;    

  
}
class CalendarModuleTableTask
{				
    /**
    * @var string
    */
    var $TASKID;    
    
    /**
    * @var string
    */
    var $TASKTASKDATE;
     
    /**
    * @var string
    */
    var $TASKBEGINOFTHEINTERVAL;
     
    /**
    * @var string
    */
    var $TASKENDOFTHEINTERVAL;
    
    /**
    * @var string
    */
    var $TASKPATIENTID;
     
    /**
    * @var string
    */
    var $TASKDOCTORID;
    
    /**
    * @var string
    */
    var $TASKROOMID;
     
    /**
    * @var string
    */
    var $TASKWORKPLACEID;
    
    /**
    * @var string
    */
    var $TASKNOTICED;
     
    /**
    * @var string
    */
    var $TASKFACTOFVISIT;
    
    /**
    * @var string
    */
    var $TASKWORKDESCRIPTION;
     
    /**
    * @var string
    */
    var $TASKCOMMENT;

    /**
    * @var string
    */
    var $PATIENTSHORTNAME;
	
	/**
    * @var string
    */
    var $PATIENTFULLNAME;
	
	/**
    * @var string
    */
    var $PATIENTTELEPHONENUMBER;
     
    /**
    * @var string
    */
    var $PATIENTMOBILENUMBER; 
 
	/**
    * @var string
    */
    var $DOCTORSHORTNAME;
	
     /**
    * @var string
    */
    var $ROOMNUMBER;
    
    /**
    * @var string
    */
    var $ROOMNAME;
    
     /**
    * @var string
    */
    var $WORKPLACENUMBER; 	
    
        
     /**
    * @var int
    */
    var $PRIMARYPATIENT; 	    
     /**
    * @var string
    */
    var $FNAME;     
     /**
    * @var string
    */
    var $LNAME;     
     /**
    * @var string
    */
    var $MNAME;     
      /**
    * @var string
    */
    var $CARDNUMBER;    
      /**
    * @var string
    */
    var $PSHORTNAME; 
    
    /**
    * @var CalendarModuleInsurancePolisObject
    */
    var $insurance;
    
      /**
    * @var string
    */
    var $AUTHORID; 
     
     /**
    * @var string
    */
    var $AUTHOR_SHORTANME; 
     
    /**
    * @var int
    */
    var $TASKTYPE;
    /**
    * @var int
    */
    var $SMSSTATUS; 
    
    /**
    * @var int
    */
    var $SMS_SEND_STATUS; 
    
    /**
     * @var string
     */
     var $xml_item;		
}

/////////////////////////////////////////////////INSURANCE OBJECTS START//////////////////////////////////////////////////////////
class CalendarModuleInsurancePolisObject
{
    /**
    * @var string
    */
    var $Id;
    
    /**
    * @var string
    */
    var $policyNumber;
     
    /**
    * @var string
    */
    var $toDate;
     
    /**
    * @var string
    */
    var $fromDate;
     
    /**
    * @var string
    */
    var $Comments;
    
    /**
    * @var CalendarModuleInsuranceCompanyObject
    */
    var $company;
    
    /**
    * @var string
    */
    var $patientId;
}

class CalendarModuleInsuranceCompanyObject
{
    /**
    * @var string
    */
    var $Id;
    
    /**
    * @var string
    */
    var $Name;
    
    /**
    * @var string
    */
    var $Phone;
    
    /**
    * @var string
    */
    var $Address;
}
/////////////////////////////////////////////////INSURANCE OBJECTS END//////////////////////////////////////////////////////////

class CalendarModulePatient
{
    /**
    * @var string
    */
    var $ID;
  
    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var string
    */
    var $CARDNUMBER;
    
    /**
    * @var string
    */
    var $doctors;
    
    /**
    * @var string
    */
    var $agreementTimestamp;
    
    /**
    * @var string
    */
    var $dob;
    /**
    * @var string
    */
    var $doctor_lead;
    /**
    * @var int
    */
    var $GROUP_COLOR;
    /**
    * @var string
    */
    var $GROUP_DESCR;
}

class CalendarModulePatientDOB
{
    /**
    * @var string
    */
    var $ID;
	
	/**
    * @var string
    */
	var $SHORTNAME;
	
    /**
    * @var string
    */
    var $FULLNAME;
    
    /**
    * @var string
    */
    var $BIRTHDAY;
	
	/**
    * @var string
    */
    var $MOBILE;
	
	/**
    * @var string
    */
    var $PHONE;
	
	/**
    * @var string
    */
    var $EMAIL;
}

class CalendarModuleDoctor
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var int
    */
    var $ROLE;
}

class CalendarModuleRoom
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $NAME;  
    
    /**
    * @var int
    */
    var $ROOMCOLOR;     
}

class CalendarModuleWorkPlace
{
	/**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $ROOMID;
    
    /**
    * @var string
    */
    var $NUMBER;    
}

/*=======================================================*/
/*================SCHEDULE===============================*/
/*=======================================================*/

class CalendarModuleScheduleRule
{
    /**
     * @var int
     */ 
    var $id;
    /**
     * @var String
     */ 
    var $name;
    /**
     * @var String
     */ 
    var $daysRange;
    /**
     * @var bool
     */ 
    var $isWorking;
    /**
     * @var int
     */
    var $repeatType;
    /**
     * @var int
     */ 
    var $skipDaysCount;
    /**
     * @var int
     */ 
    var $cabinetid;
    /**
     * @var String
     */ 
    var $startTime;
    /**
     * @var String
     */ 
    var $endTime;
    
     /**
     * @var String
     */ 
     var $cabinetNumber;
     /**
     * @var String
     */ 
     var $cabinetName;
    
    /**
     * CalendarModuleScheduleRule::processing()
     * применение правила к дню
     * 
     * @param CalendarModuleDaySchedule $day
     * @return CalendarModuleDaySchedule
     */
    public function processing($day)
    {
        if ($day->timeIntervals == null) $day->timeIntervals = array();
                
        //временной интервал правила
        $ruleTI = new CalendarModuleInterval($this->startTime, $this->endTime, $this->cabinetid);
        
        $ruleTI->cabinetNumber = $this->cabinetNumber;
        $ruleTI->cabinetName = $this->cabinetName;
        
        //продолжительность интервала
        $dayRangeLength = $this->getDayRangeLength();
        
        $xml = new SimpleXMLElement($this->daysRange);
        foreach ($xml->children() as $item)
        {
            $itemStart = strtotime((string)$item->rangeStart);
            $itemEnd = strtotime((string)$item->rangeEnd);
                        

        
            //условие для запрета повторения в обратном порядке
            if ($itemStart - strtotime($day->day) > 0) continue;

            switch($this->repeatType)
            {
                case 0: // - кожен день
                    if ($this->isWorking) //добавляем интервал если это рабочее время
                    {
                        $day->addInterval($ruleTI);
                    }
                    else
                    {
                        //отнимаем нерабочее
                        $day->subInterval($ruleTI);
                    }
                    break;
                case 1: // - з перервою в N днів
                case 2: // - кожен тиждень
                    //длина интервала, для недели 7
                    $length = $this->repeatType == 2 ? 7 : ($dayRangeLength + $this->skipDaysCount);
                    //количество дней между текущим из daysRange и днем из интервала $day
                    $count = round(abs($itemStart - (strtotime($day->day))) / 86400);
                          //если остаток от деления 0, значит $day подпадает под правило
                    if ($count % $length == 0)
                    {
                        if ($this->isWorking) //добавляем интервал если это рабочее время
                        {
                            $day->addInterval($ruleTI);
                        }
                        else
                        {
                            //отнимаем нерабочее
                            $day->subInterval($ruleTI);
                        }
                    }
                    break;
                case 3: // - кожен місяць
                    if (date("d", strtotime($day->day)) == date("d", $itemStart))
                    {
                        if ($this->isWorking) //добавляем интервал если это рабочее время
                        {
                            $day->addInterval($ruleTI);
                        }
                        else
                        {
                            //отнимаем нерабочее
                            $day->subInterval($ruleTI);
                        }
                    }
                    break;
                case -1: //- не повторювати
                    //если текущий день $day равен дню из правила
                    if (date("d.m.y", strtotime($day->day)) == date("d.m.y", $itemStart))
                    {
                        if ($this->isWorking) //добавляем интервал если это рабочее время
                        {
                            $day->addInterval($ruleTI);
                        }
                        else
                        {
                            //отнимаем нерабочее
                            $day->subInterval($ruleTI);
                        }                            
                    }
                    break;
                default:
                    break;
            }

        }
        
        return $day;
    }
    
    /**
     * Длина интервала в днях
     * CalendarModuleScheduleRule::getDayRangeLength()
     * 
     * @return int
     */
    public function getDayRangeLength()
    {
        $xml = new SimpleXMLElement($this->daysRange);
        $minDate = null;
        $maxDate = null;
        foreach ($xml->children() as $item)
        {
            $itemStart = strtotime((string)$item->rangeStart);
            $itemEnd = strtotime((string)$item->rangeEnd);
            if ($minDate == null || $minDate > $itemStart)
                $minDate = $itemStart;
            if ($maxDate == null || $maxDate < $itemEnd)
                $maxDate = $itemEnd;
        } 
        return (int)($minDate && $maxDate) ? (abs($minDate - $maxDate) / 86400 + 1) : 0; 
    }
    
}
    
class CalendarModuleSchedule
{
    /**
     * @var int
     */ 
    var $id;
    /**
     * @var String
     */ 
    var $name;
    /**
     * @var CalendarModuleScheduleRule[]
     */ 
    var $Rules;
    /**
     * @var String
     */ 
    var $startDate;
    /**
     * @var String
     */ 
    var $endDate;
    /**
     * @var CalendarModuleDoctor
     */ 
    var $Doctor;
    /**
     * @var bool
     */ 
    var $isCurrent;
    /**
     * @var CalendarModuleDaySchedule[]
     */  
    var $daySchedule;
    
    
    
    
    /**
     * CalendarModuleSchedule::getRules()
     * 
     * @param resource $trans
     * @param int $cabID
     * @return void
     */
    public function getRules($trans, $cabID = null)
    {
        $QueryText = "select
            operatingschedulerules.id,
            operatingschedulerules.name,
            operatingschedulerules.isworking,
            operatingschedulerules.daysrange,
            operatingschedulerules.repeattype,
            operatingschedulerules.skipdayscount,
            operatingschedulerules.starttime,
            operatingschedulerules.endtime,
            operatingschedulerules.cabinetid,
            
            (select rooms.number from rooms where rooms.id = operatingschedulerules.cabinetid), 
            (select rooms.name from rooms where rooms.id = operatingschedulerules.cabinetid)
            
            
        from operatingschedulerules
        where operatingschedulerules.operatingscheduleid = " . $this->id;
        if ($cabID != null)
            $QueryText .= " and (operatingschedulerules.cabinetid = $cabID or operatingschedulerules.isworking = 0)";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $this->Rules = array();
        while ($row = ibase_fetch_row($result))
        {
            $rule = new CalendarModuleScheduleRule;
            $rule->id = $row[0];
            $rule->name = $row[1];
            $rule->isWorking = $row[2];
            $blob_info = ibase_blob_info($row[3]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[3]);
                $rule->daysRange = $this->xmlForEveryDay(ibase_blob_get($blob_hndl, $blob_info[0]));
                ibase_blob_close($blob_hndl);
            }
            $rule->repeatType = $row[4];
            $rule->skipDaysCount = $row[5];
            $rule->startTime = $row[6];
            $rule->endTime = $row[7];
            $rule->cabinetid = $row[8];
            $rule->cabinetNumber = $row[9];
            $rule->cabinetName = $row[10];
            $this->Rules[] = $rule;            
        }
        ibase_free_query($query);
        ibase_free_result($result);
    }
    
    

    
    /**
      * CalendarModuleSchedule::xmlForEveryDay()
      * 
      * @param string $str
      * @return string
      */
     public static function xmlForEveryDay($str)
     {     
        $xml = new SimpleXMLElement($str);
        foreach ($xml->children() as $item)
        {
            
            $s = strtotime($item->rangeStart);
            $e = strtotime($item->rangeEnd);

            if ($s != $e)
            {
                $days = abs($s - $e) / 86400 + 1; 
                $item->rangeEnd = $item->rangeStart;
                $arrayXML = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Rule/>');
                for ($i=0; $i<$days; $i++)
                {
                    $itemXML = $arrayXML->addChild('item');
                    $itemXML->addChild('rangeStart', date("d.m.Y", $s + 86400*$i));
                    $itemXML->addChild('rangeEnd', date("d.m.Y", $s + 86400*$i));
                }
                return $arrayXML->asXML();
            }
            else
            {
                return $str;
            }
        }
        
        
     }
     
}

class CalendarModuleInterval
{
    /**
     * Time h:m:s
     * @var string
     */
     var $startTime;
     /**
     * Time h:m:s
     * @var string
     */
     var $endTime;
     /**
      * @var int
      */ 
     var $cabinetID;
     /**
      * @var string
      */ 
     var $cabinetNumber;
     /**
      * @var string
      */ 
     var $cabinetName;
     
     
     
     /**
      * CalendarModuleInterval::cmp()
      * 
      * @param CalendarModuleInterval $a
      * @param CalendarModuleInterval $b
      * @return int
      */
     public static function cmp($a, $b)
     {
        if ($a->startTime == $b->startTime)
        {
            if ($a->endTime == $b->endTime)
                return 0;
            else
                return strtotime($a->endTime) > strtotime($b->endTime) ? 1 : -1;
        } 
        return strtotime($a->startTime) > strtotime($b->startTime) ? 1 : -1;
     }
     
     public function __construct($startTime = null, $endTime = null, $cabinetID = null)
     {
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->cabinetID = $cabinetID;
     }
     
     /**
      * CalendarModuleInterval::isCross()
      * 
      * @param CalendarModuleInterval $interval
      * @return bool
      */
     public function isCross($interval)
     {
        return ($interval->cabinetID == $this->cabinetID || $interval->cabinetID == null || $this->cabinetID == null) && !(strtotime($interval->startTime) > strtotime($this->endTime) || strtotime($interval->endTime) < strtotime($this->startTime));
     }
}

class CalendarModuleDaySchedule
{
    /**
     * Date YYYY-MM-DD
     * @var string
     */
     var $day;
     /**
      * @var CalendarModuleInterval[]
      */ 
     var $timeIntervals;
     
     /**
      * CalendarModuleDaySchedule::isCross()
      * 
      * @param CalendarModuleInterval $interval
      * @return bool
      */
     public function isCross($interval)
     {
        if ($this->timeIntervals == null) return false;
        foreach($this->timeIntervals as $ti)
        {
            if ($interval->isCross($ti))
            {
                return true;
            }
        }
        return false;
     }
     
     /**
      * CalendarModuleDaySchedule::addInterval()
      * 
      * @param CalendarModuleInterval $interval
      * @return void
      */
     public function addInterval($interval)
     {
        if ($this->isCross($interval))
        {
            foreach($this->timeIntervals as $key => $ti)
            {
                if ($interval->isCross($ti))
                {
                    //Новый интервал, который делается из текущего $ti и $interval
                    $newInterval = new CalendarModuleInterval();
                    $newInterval->startTime = strtotime($ti->startTime) > strtotime($interval->startTime) ? $interval->startTime : $ti->startTime;
                    $newInterval->endTime = strtotime($ti->endTime) < strtotime($interval->endTime) ? $interval->endTime : $ti->endTime;
                    $newInterval->cabinetID = $interval->cabinetID;
                    $newInterval->cabinetNumber = $interval->cabinetNumber;
                    $newInterval->cabinetName = $interval->cabinetName;
                    
                    //удаляем $ti
                    unset($this->timeIntervals[$key]);
                    
                    //добавляем новый интервал
                    $this->addInterval($newInterval);
                    break;
                }
            }
        }
        else
        {
            //если не пересекаются интервалы, то просто добавляем
            $this->timeIntervals[] = $interval;
        }
     }
     
     /**
      * CalendarModuleDaySchedule::subInterval()
      * 
      * @param CalendarModuleInterval $interval
      * @return void
      */
     public function subInterval($interval)
     {
        foreach($this->timeIntervals as $key => $ti)
        {
            //если интервалы пересекаются, тогда вычетаем время
            if ($interval->isCross($ti))
            {
                //в случае вычитания интервала могут появиться 2 интервала: до и после
                $before = null;
                $after = null;
                
                //интервал "до"
                if (strtotime($ti->startTime) < strtotime($interval->startTime))
                {
                    $before = new CalendarModuleInterval($ti->startTime, $interval->startTime);
                    $before->cabinetID = $ti->cabinetID; 
                }
                //интервал "после"
                if (strtotime($ti->endTime) > strtotime($interval->endTime))
                {
                    $after = new CalendarModuleInterval($interval->endTime, $ti->endTime);
                    $after->cabinetID = $ti->cabinetID;
                }
                
                //удаляем $ti
                unset($this->timeIntervals[$key]);
                
                //добавляем новые интервалы, если они существуют
                if ($before != null)
                    $this->addInterval($before);
                if ($after != null)
                    $this->addInterval($after);
            }
        }
     }
}

class CalendarModuleRequestData
{
    /**
     * @var string[]
     */ 
    var $types;
    /**
     * @var Number[]
     */ 
    var $ids;
    /**
     * @var string
     */
     var $taskdatestart;
     /**
     * @var string
     */
     var $taskdateend;
     /**
     * @var string
     */
    var $patientid; 
    /**
     * @var string
     */
    var $noticed;
    /**
     * @var string
     */
    var $factofvisit;
}

class CalendarModuleResponseData
{
    /**
     * @var CalendarModuleTableTask[]
     */ 
    var $tasks;
    /**
     * @var CalendarModuleSchedule[]
     */ 
    var $schedules;
    
    /**
     * @var string
     */
     var $XMLcollection;
}


class CalendarModule
{	 
     /**
     * @param CalendarModuleWorkPlace $p1
     * @param CalendarModuleRoom $p2
     * @param CalendarModuleDoctor $p3
     * @param CalendarModulePatientDOB $p4
     * @param CalendarModulePatient $p5
     * @param CalendarModuleTableTask $p6
     * @param CalendarModuleInsuranceCompanyObject $p7
     * @param CalendarModuleInsurancePolisObject $p8
     * @param CalendarModuleScheduleRule $p9
     * @param CalendarModuleDaySchedule $p10
     * @param CalendarModuleSchedule $p11
     * @param CalendarModuleRequestData $p12
     * @param CalendarModuleResponseData $p13
     * @param CalendarModuleInterval $p14
     * @param CalendarModuleSchedule $p15
     * @param CalendarModuleTaskItem $p16
     * @param CalendarModuleIntervalItem $p17
     * @return void
     */

    public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10, $p11, $p12, $p13, $p14, $p15, $p16, $p17)
    {
    }
    
    
	/** 
 	* @param string $taskdatestart
 	* @param string $taskdateend
 	* @param string $patientid
 	* @param string $doctorid
 	* @param string $roomid
 	* @param string $workplaceid
 	* @param string $noticed
 	* @param string $factofvisit
 	* @param string $workdescription
 	* @param string $comment
 	* @param object $trans
 	* 
 	* @return CalendarModuleTableTask[]
	**/      
    	 		
	public function getTableTasksFilteredMultiDate($taskdatestart, $taskdateend, $patientid,
     									   $doctorid, $roomid, $workplaceid, $noticed, $factofvisit,
     									   $workdescription, $comment, $trans=null)
	{   
    	   if($trans==null)
            {
                $connection = new Connection();
                $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
			
            $rez = $this->getTasksFilteredMultiDate($taskdatestart, $taskdateend, $patientid,
     									   $doctorid, $roomid, $workplaceid, $noticed, $factofvisit,
     									   $workdescription, $comment, $trans);

			if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }	
			
			if (ibase_errmsg() != false)
			{
				trigger_error(ibase_errmsg(), E_USER_ERROR);				
			}		
			return $rez;		
	}
    
    /** 
 	* @param string $taskdatestart
 	* @param string $taskdateend
 	* @param string $patientid
 	* @param string $doctorid
 	* @param string $roomid
 	* @param string $workplaceid
 	* @param string $noticed
 	* @param string $factofvisit
 	* @param string $workdescription	
 	* @param string $comment
    * @param resource $trans
 	* 
 	* @return CalendarModuleTableTask[]
	**/ 
    private function getTasksFilteredMultiDate($taskdatestart, $taskdateend, $patientid,
     									   $doctorid, $roomid, $workplaceid, $noticed, $factofvisit,
     									   $workdescription, $comment, $trans) 
     { 
		$QueryText =
		'select 
				tasks.id "TASKID",
			    tasks.taskdate "TASKTASKDATE",
			    tasks.beginoftheinterval "TASKBEGINOFTHEINTERVAL",
			    tasks.endoftheinterval "TASKENDOFTHEINTERVAL",
			    tasks.patientid "TASKPATIENTID",
			    tasks.doctorid "TASKDOCTORID",
			    tasks.roomid "TASKROOMID",
			    tasks.workplaceid "TASKWORKPLACEID",
			    tasks.noticed "TASKNOTICED",
			    tasks.factofvisit "TASKFACTOFVISIT",
			    tasks.workdescription "TASKWORKDESCRIPTION",
			    tasks.comment "TASKCOMMENT",
			    patients.fullname "PATIENTFULLNAME",
			    patients.shortname "PATIENTSHORTNAME",
			    patients.TELEPHONENUMBER "PATIENTTELEPHONENUMBER",
			    patients.MOBILENUMBER "PATIENTMOBILENUMBER",
			    doctors.shortname "DOCTORSHORTNAME",
			    rooms.NUMBER "ROOMNUMBER",
			    rooms.NAME "ROOMNAME",
			    workplaces.NUMBER "WORKPLACENUMBER",
			    patients.PRIMARYFLAG,
			    patients.FIRSTNAME,
			    patients.LASTNAME,
			    patients.MIDDLENAME,
                tasks.TASKTYPE,
                tasks.SMSSTATUS,
                smssender.responsecode,
                account.id,
                account.lastname,
                account.firstname,
                account.middlename,
                coalesce(patients.CARDBEFORNUMBER,\'\')||coalesce(patients.CARDNUMBER,\'\')||coalesce(patients.CARDAFTERNUMBER,\'\')                
                
			from tasks				
			    left join patients on (patients.id = tasks.patientid)
			    left join doctors on (tasks.doctorid = doctors.id)
			    left join workplaces on (tasks.workplaceid = workplaces.id)
			    left join rooms on (tasks.roomid = rooms.id)
                left join smssender on (tasks.id = smssender.taskid)
                left join account on account.id=tasks.authorid
			where (tasks.id is not null)';
		if ($taskdatestart!="")	
		{
			$QueryText=$QueryText." and (tasks.taskdate>='$taskdatestart')"; 
		}
		if ($taskdateend!="")	
		{
			$QueryText=$QueryText." and (tasks.taskdate<='$taskdateend')"; 
		}
		if ($patientid!="")	
		{
			$QueryText=$QueryText." and (tasks.patientid=$patientid)"; 
		}
		if ($doctorid!="")	
		{
			$QueryText=$QueryText." and (tasks.doctorid=$doctorid)"; 
		}
		if ($roomid!="")	
		{
			$QueryText=$QueryText." and (tasks.roomid=$roomid)"; 
		}
		if ($workplaceid!="")	
		{
			$QueryText=$QueryText." and (tasks.workplaceid=$workplaceid)"; 
		}
		if ($noticed!="-1")	
		{
			$QueryText=$QueryText." and (tasks.noticed=$noticed)"; 
		}
		if ($factofvisit!="-1")	
		{
			$QueryText=$QueryText." and (tasks.factofvisit=$factofvisit)"; 
		}
		if ($workdescription!="")	
		{
			$QueryText=$QueryText." and (tasks.workdescription containing '$workdescription')"; 
		}
		if ($comment!="")	
		{
			$QueryText=$QueryText." and (tasks.comment containing '$comment')"; 
		}
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new CalendarModuleTableTask;
			$obj->TASKID = $row[0];
			$obj->TASKTASKDATE = $row[1];
			$obj->TASKBEGINOFTHEINTERVAL = $row[2];
			$obj->TASKENDOFTHEINTERVAL = $row[3];
			$obj->TASKPATIENTID = $row[4];
			$obj->TASKDOCTORID = $row[5];
			$obj->TASKROOMID = $row[6];
			$obj->TASKWORKPLACEID = $row[7];
			$obj->TASKNOTICED = $row[8];
			$obj->TASKFACTOFVISIT = $row[9];
			$obj->TASKWORKDESCRIPTION = $row[10];
			$obj->TASKCOMMENT = $row[11];
			$obj->PATIENTFULLNAME = $row[12];
			$obj->PATIENTSHORTNAME = $row[13];
			$obj->PATIENTTELEPHONENUMBER = $row[14];
			$obj->PATIENTMOBILENUMBER = $row[15];
			$obj->DOCTORSHORTNAME = $row[16];
			$obj->ROOMNUMBER = $row[17];
			$obj->ROOMNAME = $row[18];
			$obj->WORKPLACENUMBER = $row[19];
            $obj->PRIMARYPATIENT = $row[20];
            $obj->FNAME = $row[21];
            $obj->LNAME = $row[22];
            $obj->MNAME = $row[23];
            $obj->TASKTYPE = $row[24];
            $obj->SMSSTATUS = (bool)$row[25];
            $obj->SMS_SEND_STATUS = $row[26] == null ? 0 : $row[26];
            $obj->AUTHORID = $row[27];
            $obj->AUTHOR_SHORTANME = shortname($row[28],$row[29],$row[30]); 
            $obj->CARDNUMBER = $row[31];
            $begin_date = new DateTime($obj->TASKTASKDATE.' '.$obj->TASKBEGINOFTHEINTERVAL);
            $end_date = new DateTime($obj->TASKTASKDATE.' '.$obj->TASKENDOFTHEINTERVAL);
            
            $begin_date_flex = $begin_date->format("D M j G:i:s").' '.Settings::GMT_STRING.' '.$begin_date->format("Y");
            $end_date = $end_date->format("D M j G:i:s").' '.Settings::GMT_STRING.' '.$end_date->format("Y");
            
            $obj->xml_item = '<event
                id="'.$obj->TASKID.'" 
                startTime="'.$begin_date_flex.'" 
                endTime="'.$end_date.'" 
                patientid="'.$obj->TASKPATIENTID.'" 
                doctorid="'.$obj->TASKDOCTORID.'" 
                workplaceid="'.$obj->TASKWORKPLACEID.'" 
                cabinetid="'.$obj->TASKROOMID.'" 
                primaryflag="'.$obj->PRIMARYPATIENT.'" 
                tasktype="'.$obj->TASKTYPE.'" 
                smsstatus="'.$obj->SMSSTATUS.'" 
                smssendstatus="'.$obj->SMS_SEND_STATUS.'"
                notice="'.$obj->TASKNOTICED.'" 
                visit="'.$obj->TASKFACTOFVISIT.'"
                summary="P: '.str_replace('"', "", $obj->PATIENTSHORTNAME);//.';
               // str_replace($vowels, "", "Hello World of PHP")
                //<summary><![CDATA[P: '.$obj->PATIENTSHORTNAME;//.'"/>';
                //&#xA;
                if(($obj->TASKDOCTORID!=null)&&($obj->TASKDOCTORID!=''))
                {
                   $obj->xml_item .=  '&#xA;D: '.str_replace('"', "", $obj->DOCTORSHORTNAME);
                }
                if(($obj->TASKCOMMENT!=null)&&($obj->TASKCOMMENT!=''))
                {
                   $obj->xml_item .=  '&#xA;CM: '.str_replace('"', "", $obj->TASKCOMMENT);
                }
                if(($obj->TASKWORKDESCRIPTION!=null)&&($obj->TASKWORKDESCRIPTION!=''))
                {
                   $obj->xml_item .=  '&#xA;WD: '.str_replace('"', "", $obj->TASKWORKDESCRIPTION);
                }
                $obj->xml_item .= '">';
                
                
                $obj->xml_item .= '<patient><![CDATA['.$obj->PATIENTSHORTNAME.']]></patient>
                <fname><![CDATA['.$obj->FNAME.']]></fname>
                <lname><![CDATA['.$obj->LNAME.']]></lname>
                <mname><![CDATA['.$obj->MNAME.']]></mname> 
                <card><![CDATA['.$obj->CARDNUMBER.']]></card> 
                <patientfio><![CDATA['.$obj->LNAME.' '.$obj->FNAME.' '.$obj->MNAME.']]></patientfio> 
                <phone><![CDATA['.$obj->PATIENTTELEPHONENUMBER.']]></phone> 
                <doctor><![CDATA['.$obj->DOCTORSHORTNAME.']]></doctor> 
                <cabinet><![CDATA['.$obj->ROOMNUMBER.']]></cabinet> 
                <cabinetname><![CDATA['.$obj->ROOMNAME.']]></cabinetname> 
                <workplace><![CDATA['.$obj->WORKPLACENUMBER.']]></workplace>
                <description><![CDATA['.$obj->TASKWORKDESCRIPTION.']]></description> 
                <mobile><![CDATA['.$obj->PATIENTMOBILENUMBER.']]></mobile> 
                <comment><![CDATA['.$obj->TASKCOMMENT.']]></comment>'; 
                
          if(nonull($row[4]))
          {
                        $QueryText2 ="select patientsinsurance.id,
                    patientsinsurance.policenumber,
                    patientsinsurance.policetodate,
                    patientsinsurance.policefromdate,
                    patientsinsurance.companyid,
                    patientsinsurance.comments,
                
                    insurancecompanys.id, 
                    insurancecompanys.name,
                    insurancecompanys.phone,
                    insurancecompanys.address
                
                    from patientsinsurance
                    left join insurancecompanys
                    on insurancecompanys.id = patientsinsurance.companyid
                
                    where patientsinsurance.patientid =  $row[4]";
    		$query2 = ibase_prepare($trans, $QueryText2);
    		$result2=ibase_execute($query2);
            $obj2 = new CalendarModuleInsurancePolisObject;
    		while ($row2 = ibase_fetch_row($result2))
    		{
              $obj2->Id = $row2[0];
              $obj2->policyNumber = $row2[1];
              $obj2->toDate = $row2[2];
              $obj2->fromDate = $row2[3];
              $obj2->patientId = $row[4];
              $obj2->Comments = $row2[5];
              
                  $comp_obj = new CalendarModuleInsuranceCompanyObject;
                  $comp_obj->Id = $row2[6];
                  $comp_obj->Name = $row2[7];
                  $comp_obj->Phone = $row2[8];
                  $comp_obj->Address = $row2[9];
                  
              $obj2->company = $comp_obj;
              $obj->insurance = $obj2;
              
    		  $obj->xml_item .= '<insurance>
                                    <policyNumber>'.$row2[1].'</policyNumber>
                                    <policyEndDate>'.$row2[2].'</policyEndDate> 
                                    <policyStartDate>'.$row2[3].'</policyStartDate>
                                    <policyComment><![CDATA['.$row2[5].']]></policyComment> 
                                    <policyCompanyName><![CDATA['.$row2[7].']]></policyCompanyName> 
                                    <policyCompanyPhone>'.$row2[8].'</policyCompanyPhone>
                                </insurance>';
    		}
          }

            $obj->xml_item .= '</event>';
			$rows[] = $obj;
		}
		ibase_free_query($query);
		ibase_free_result($result);
		return $rows; 
    }
    
    //XMLDataProvider START
    
    /** 
 	* @param string $taskdatestart
 	* @param string $taskdateend
 	* @param string $patientid
 	* @param string $doctorid
 	* @param string $roomid
 	* @param string $workplaceid
 	* @param string $noticed
 	* @param string $factofvisit
 	* @param string $workdescription	
 	* @param string $comment
    * @param resource $trans
 	* 
 	* @return string
	**/ 
    private function getTasksFilteredMultiDate2($taskdatestart, $taskdateend, $patientid,
     									   $doctorid, $roomid, $workplaceid, $noticed, $factofvisit,
     									   $workdescription, $comment, $trans) 
     { 
		$QueryText =
		'select 
				tasks.id "TASKID",
			    tasks.taskdate "TASKTASKDATE",
			    tasks.beginoftheinterval "TASKBEGINOFTHEINTERVAL",
			    tasks.endoftheinterval "TASKENDOFTHEINTERVAL",
			    tasks.patientid "TASKPATIENTID",
			    tasks.doctorid "TASKDOCTORID",
			    tasks.roomid "TASKROOMID",
			    tasks.workplaceid "TASKWORKPLACEID",
			    tasks.noticed "TASKNOTICED",
			    tasks.factofvisit "TASKFACTOFVISIT",
			    tasks.workdescription "TASKWORKDESCRIPTION",
			    tasks.comment "TASKCOMMENT",
			    patients.fullname "PATIENTFULLNAME",
			    patients.shortname "PATIENTSHORTNAME",
			    patients.TELEPHONENUMBER "PATIENTTELEPHONENUMBER",
			    patients.MOBILENUMBER "PATIENTMOBILENUMBER",
				(select doctors.shortname from doctors where doctors.id = tasks.doctorid) "DOCTORSHORTNAME",
			    rooms.NUMBER "ROOMNUMBER",
			    rooms.NAME "ROOMNAME",
                (select workplaces.number from workplaces where workplaces.id = tasks.workplaceid) "WORKPLACENUMBER",
			    patients.PRIMARYFLAG,
			    patients.FIRSTNAME,
			    patients.LASTNAME,
			    patients.MIDDLENAME,
                tasks.TASKTYPE,
                tasks.SMSSTATUS,
                null,
                account.id,
                account.lastname,
                account.firstname,
                account.middlename,
                coalesce(patients.CARDBEFORNUMBER,\'\')||coalesce(patients.CARDNUMBER,\'\')||coalesce(patients.CARDAFTERNUMBER,\'\'),
                
                tasks.FACTOFOUT,
                tasks.STARTTIMESTAMP,
                tasks.ENDTTIMESTAMP
                
			from tasks				
			    left join patients on (patients.id = tasks.patientid)
			    left join rooms on (tasks.roomid = rooms.id)
                left join account on account.id=tasks.authorid
			where (tasks.id is not null)';
		if ($taskdatestart!="")	
		{
			$QueryText=$QueryText." and (tasks.taskdate>='$taskdatestart')"; 
		}
		if ($taskdateend!="")	
		{
			$QueryText=$QueryText." and (tasks.taskdate<='$taskdateend')"; 
		}
		if ($patientid!="")	
		{
			$QueryText=$QueryText." and (tasks.patientid=$patientid)"; 
		}
		if ($doctorid!="")	
		{
			$QueryText=$QueryText." and (tasks.doctorid=$doctorid)"; 
		}
		if ($roomid!="")	
		{
			$QueryText=$QueryText." and (tasks.roomid=$roomid)"; 
		}
		if ($workplaceid!="")	
		{
			$QueryText=$QueryText." and (tasks.workplaceid=$workplaceid)"; 
		}
		if ($noticed!="-1")	
		{
			$QueryText=$QueryText." and (tasks.noticed=$noticed)"; 
		}
		if ($factofvisit!="-1")	
		{
			$QueryText=$QueryText." and (tasks.factofvisit=$factofvisit)"; 
		}
		if ($workdescription!="")	
		{
			$QueryText=$QueryText." and (tasks.workdescription containing '$workdescription')"; 
		}
		if ($comment!="")	
		{
			$QueryText=$QueryText." and (tasks.comment containing '$comment')"; 
		}
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		//$resultXML = '<events>';
        $resultXML = '';
		while ($row = ibase_fetch_row($result))
		{
            $begin_date = new DateTime($row[1].' '.$row[2]);
            $end_date = new DateTime($row[1].' '.$row[3]);
            
            $begin_date_flex = $begin_date->format("D M j G:i:s").' GMT'.$begin_date->format("O Y");
            $end_date = $end_date->format("D M j G:i:s").' GMT'.$end_date->format("O Y");
            if($row[26] == null)
            {
                $sms_send_status = 0;
            }
            else
            {
                $sms_send_status = $row[26];
            }
            $vowels = array('"', '<', '>');
            $resultXML .= '<event
                id="'.$row[0].'" 
                startTime="'.$begin_date_flex.'" 
                endTime="'.$end_date.'" 
                patientid="'.$row[4].'" 
                doctorid="'.$row[5].'" 
                workplaceid="'.$row[7].'" 
                cabinetid="'.$row[6].'" 
                primaryflag="'.$row[20].'" 
                tasktype="'.$row[24].'" 
                smsstatus="'.(bool)$row[25].'" 
                smssendstatus="'.$sms_send_status.'"
                notice="'.$row[8].'" 
                visit="'.$row[9].'" 
                visitout="'.$row[32].'" 
                visitfactstart="'.$row[33].'"
                visitfactend="'.$row[34].'"  
                authorid="'.$row[27].'"
                author_shortname="'.shortname($row[28],$row[29],$row[30]).'"                 
                summary="';
                
                
                if($row[24] == 3){
                    if(($row[5]!=null)&&($row[5]!=''))
                    {
                       $resultXML .=  'D: '.str_replace($vowels, "", $row[16]).'&#xA;';
                    }
                    if(($row[11]!=null)&&($row[11]!=''))
                    {
                       $resultXML .=  'CM: '.str_replace($vowels, "", $row[11]);
                    }
                }
                else{
                    $resultXML .= 'P: '.str_replace($vowels, "", $row[13]);//.';
                    if(($row[5]!=null)&&($row[5]!=''))
                    {
                       $resultXML .=  '&#xA;D: '.str_replace($vowels, "", $row[16]);
                    }
                    if(($row[11]!=null)&&($row[11]!=''))
                    {
                       $resultXML .=  '&#xA;CM: '.str_replace($vowels, "", $row[11]);
                    }
                    if(($row[10]!=null)&&($row[10]!=''))
                    {
                       $resultXML .=  '&#xA;WD: '.str_replace($vowels, "", $row[10]);
                    }
                }
                $resultXML .= '">';
                
                
                $resultXML .= '<patient><![CDATA['.$row[13].']]></patient>
                <fname><![CDATA['.$row[21].']]></fname>
                <lname><![CDATA['.$row[22].']]></lname>
                <mname><![CDATA['.$row[23].']]></mname> 
                <card><![CDATA['.$row[31].']]></card>                 
                <patientfio><![CDATA['.$row[21].' '.$row[22].' '.$row[23].']]></patientfio> 
                <phone><![CDATA['.$row[14].']]></phone> 
                <doctor><![CDATA['.$row[16].']]></doctor> 
                <cabinet><![CDATA['.$row[17].']]></cabinet> 
                <cabinetname><![CDATA['.$row[18].']]></cabinetname> 
                <workplace><![CDATA['.$row[19].']]></workplace>
                <description><![CDATA['.$row[10].']]></description> 
                <mobile><![CDATA['.$row[15].']]></mobile> 
                <comment><![CDATA['.$row[11].']]></comment>'; 
                
            if($row[24] != 3){
                $QueryText2 ="select patientsinsurance.id,
                        patientsinsurance.policenumber,
                        patientsinsurance.policetodate,
                        patientsinsurance.policefromdate,
                        patientsinsurance.companyid,
                        patientsinsurance.comments,
                    
                        insurancecompanys.id, 
                        insurancecompanys.name,
                        insurancecompanys.phone,
                        insurancecompanys.address
                    
                        from patientsinsurance
                        left join insurancecompanys
                        on insurancecompanys.id = patientsinsurance.companyid
                    
                        where patientsinsurance.patientid =  $row[4]
                        and patientsinsurance.policetodate >= '$row[1]'
                        and patientsinsurance.policefromdate < '$row[1]'";
        		$query2 = ibase_prepare($trans, $QueryText2);
        		$result2=ibase_execute($query2);
        		while ($row2 = ibase_fetch_row($result2))
        		{
        		  $resultXML .= '<insurance>
                                        <policyNumber>'.$row2[1].'</policyNumber>
                                        <policyEndDate>'.$row2[2].'</policyEndDate> 
                                        <policyStartDate>'.$row2[3].'</policyStartDate>
                                        <policyComment><![CDATA['.$row2[5].']]></policyComment> 
                                        <policyCompanyName><![CDATA['.$row2[7].']]></policyCompanyName> 
                                        <policyCompanyPhone>'.$row2[8].'</policyCompanyPhone>
                                 </insurance>';
        		}
                
                //get mobiles start
                $mobiles_sql ="select patientsmobile.id,
                                    patientsmobile.parent_fk,
                                    patientsmobile.mobilenumber,
                                    patientsmobile.senderactive,
                                    patientsmobile.comments,
                                    (select first 1 smssender.responsecode from smssender where smssender.taskid = $row[0] and smssender.phone = patientsmobile.mobilenumber)
                                from patientsmobile
                                    where patientsmobile.patient_fk = $row[4]
                                order by patientsmobile.senderactive desc, patientsmobile.id desc";
        		$mobiles_query = ibase_prepare($trans, $mobiles_sql);
        		$mobiles_result = ibase_execute($mobiles_query);
                $resultXML .= '<mobiles>';
        		while ($rowMobiles = ibase_fetch_row($mobiles_result))
        		{
        		  $resultXML .= '<mobile>
                                        <id>'.$rowMobiles[0].'</id>
                                        <parent_fk>'.$rowMobiles[1].'</parent_fk>
                                        <mobilenumber>'.$rowMobiles[2].'</mobilenumber> 
                                        <senderactive>'.$rowMobiles[3].'</senderactive>
                                        <comments><![CDATA['.$rowMobiles[4].']]></comments>
                                        <responsecode>'.$rowMobiles[5].'</responsecode>
                                </mobile>';
        		}
        		ibase_free_query($mobiles_query);
        		ibase_free_result($mobiles_result);
                $resultXML .= '</mobiles>';
            }
            //get mobiles end
            $resultXML .= '</event>';
		}
		//$resultXML .= '</events>';
		ibase_free_query($query);
		ibase_free_result($result);
		return $resultXML; 
    }
    //XMLDataProvider END
    
    

	
    /**
 	* @param CalendarModuleTableTask $task
    * @return string
 	*/
    public function addTableTask($task) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        //SELECT NEXT INVOICEID START
		$task_id_sql = "SELECT GEN_ID(GEN_TASKS_ID, 1) FROM RDB\$DATABASE";
		$task_id_query = ibase_prepare($trans, $task_id_sql);
		$task_id_result = ibase_execute($task_id_query);
        $row=ibase_fetch_row($task_id_result);
        $task_id = $row[0];
        ibase_free_query($task_id_query);
        ibase_free_result($task_id_result);	
        //SELECT NEXT INVOICEID END
        
        if($task->TASKTYPE == 3)
        {
             $QueryText = "insert into TASKS (
                                                        id, 
                                                        taskdate, 
                                                        beginoftheinterval, 
                                                        endoftheinterval, 
                                                         
                                                        doctorid, 
                                                        roomid, 
                                                        workplaceid, 
                                                         
                                                        comment, 
                                                        smsstatus,
                                                        tasktype)
                                            values($task_id, 
                                            '$task->TASKTASKDATE', 
                                            '$task->TASKBEGINOFTHEINTERVAL', 
                                            '$task->TASKENDOFTHEINTERVAL', 
                                            
                                            ";
               if($task->TASKDOCTORID!=null && $task->TASKDOCTORID!='') {
                    $QueryText .= "$task->TASKDOCTORID, ";  
               }
               else {
                    $QueryText .= "null, "; 
               }
               $QueryText .= "              $task->TASKROOMID, 
                                            $task->TASKWORKPLACEID,
                                            
                                            '".safequery($task->TASKCOMMENT)."', 
                                            
                                            0,
                                            3)";
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		//$success = ibase_commit($trans);
    		ibase_free_query($query);
        }
        else if($task->PRIMARYPATIENT == 1)
        {
            $QueryText = "insert into TASKS (
                                                        id, 
                                                        taskdate, 
                                                        beginoftheinterval, 
                                                        endoftheinterval, 
                                                        patientid, 
                                                        doctorid, 
                                                        roomid, 
                                                        workplaceid, 
                                                        noticed, 
                                                        factofvisit, 
                                                        dateofvisit, 
                                                        timeofvisit, 
                                                        workdescription, 
                                                        comment, 
                                                        smsstatus)
                                            values($task_id, 
                                            '$task->TASKTASKDATE', 
                                            '$task->TASKBEGINOFTHEINTERVAL', 
                                            '$task->TASKENDOFTHEINTERVAL', 
                                            $task->TASKPATIENTID, 
                                            $task->TASKDOCTORID, 
                                            $task->TASKROOMID, 
                                            $task->TASKWORKPLACEID, 
                                            $task->TASKNOTICED, 
                                            $task->TASKFACTOFVISIT, 
                                            null, 
                                            null, 
                                            '".safequery($task->TASKWORKDESCRIPTION)."', 
                                            '".safequery($task->TASKCOMMENT)."', 
                                            $task->SMSSTATUS)";
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		//$success = ibase_commit($trans);
    		ibase_free_query($query);
        }
        else
        {
            $QueryText = "INSERT into PATIENTS (id,
                                                firstname, 
                                                middlename, 
                                                lastname,
                                                shortname,
                                                fullname,
                                                mobilenumber,
                                                telephonenumber,
                                                primaryflag) 
                                    values (null, 
                                            '".safequery($task->FNAME)."', 
                                            '".safequery($task->MNAME)."', 
                                            '".safequery($task->LNAME)."', 
                                            '".safequery($task->PSHORTNAME)."', 
                                            '".safequery($task->LNAME." ".$task->FNAME." ".$task->MNAME)."', 
                                            '".safequery($task->PATIENTTELEPHONENUMBER)."', 
                                            '', 
                                            0) returning ID";
                                            
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
            $row = ibase_fetch_row($result);
            $PRIMPATID = $row[0];
    		ibase_free_query($query);
    		ibase_free_result($result);
            
            //add mphone start
            $mphone_sql = "insert into 
                                    patientsmobile(patient_fk, parent_fk, mobilenumber, senderactive, comments) 
                                    values ($PRIMPATID, null, '$task->PATIENTTELEPHONENUMBER', 1, '')";
                                            
    		$mphone_query = ibase_prepare($trans, $mphone_sql);
    		ibase_execute($mphone_query);
    		ibase_free_query($mphone_query);
            //add mphone end
            
            
    		$QueryText = 		
        		"insert into TASKS (id, taskdate, beginoftheinterval, endoftheinterval, patientid, doctorid, roomid, workplaceid, noticed, factofvisit, dateofvisit, timeofvisit, workdescription, comment, smsstatus)
        		values($task_id, '$task->TASKTASKDATE', '$task->TASKBEGINOFTHEINTERVAL', '$task->TASKENDOFTHEINTERVAL',
        		       $PRIMPATID, $task->TASKDOCTORID, $task->TASKROOMID, $task->TASKWORKPLACEID,
        		       $task->TASKNOTICED, $task->TASKFACTOFVISIT, null, null, '".safequery($task->TASKWORKDESCRIPTION)."', '".safequery($task->TASKCOMMENT)."', $task->SMSSTATUS)";
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		//$success = ibase_commit($trans);
    		ibase_free_query($query);
        }
        
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $task_id;		
    }    
    
     /**
 	* @param CalendarModuleTableTask $task
    * @return string
 	*/
    public function addTableTaskWizard($task) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if(!nonull($task->TASKWORKPLACEID))
        {
            $QueryText="select first 1
                                ID, 
                                ROOMID 
                                from WORKPLACES 
                                where id not in (
                                    select 
                                        workplaceid 
                                        from tasks where 
                                        taskdate='$task->TASKTASKDATE' and 
                                        beginoftheinterval>='$task->TASKBEGINOFTHEINTERVAL' and 
                                        endoftheinterval<='$task->TASKENDOFTHEINTERVAL'
                                        ) order by ";
            if(nonull($task->TASKROOMID)&&$task->TASKROOMID!=0)
            {
            $QueryText.="(case ROOMID when $task->TASKROOMID then 0 else 1 end)";
            }
            else
            {
                $QueryText.=" ROOMID";
            }
            
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
            
            if($row = ibase_fetch_row($result))      
            {                                
                $task->TASKWORKPLACEID=$row[0];
                $task->TASKROOMID=$row[1];
            }
            else return false;
         }   
            $task->TASKNOTICED=0;
            $task->TASKFACTOFVISIT=0;
               
        if($task->PRIMARYPATIENT == 0)
        {
            $QueryText = "INSERT into PATIENTS (id,
                                                            firstname, 
                                                            middlename, 
                                                            lastname,
                                                            shortname,
                                                            fullname,
                                                            mobilenumber,
                                                            telephonenumber,
                                                            primaryflag) 
                                                values (null, 
                                                        '".safequery($task->FNAME)."', 
                                                        '".safequery($task->MNAME)."', 
                                                        '".safequery($task->LNAME)."', 
                                                        '".safequery($task->PSHORTNAME)."', 
                                                        '".safequery($task->LNAME." ".$task->FNAME." ".$task->MNAME)."',
                                                        '".safequery($task->PATIENTTELEPHONENUMBER)."', 
                                                        '', 
                                                        0) returning id";
                        // file_put_contents('c:\Users\Roma\Desktop\new  2.txt',$QueryText);                            
                		$query = ibase_prepare($trans, $QueryText);
                		$result = ibase_execute($query);
                        $row = ibase_fetch_row($result);
                        $task->TASKPATIENTID = $row[0];
                        //file_put_contents('c:\Users\Roma\Desktop\new  2.txt', $task->TASKPATIENTID);
                		ibase_free_query($query);
                		ibase_free_result($result);
                        
            //add mphone start
                  if(nonull($task->PATIENTTELEPHONENUMBER))
                  {
                    $mphone_sql = "insert into 
                                            patientsmobile(patient_fk, parent_fk, mobilenumber, senderactive, comments) 
                                            values ($task->TASKPATIENTID, null, '$task->PATIENTTELEPHONENUMBER', 1, '')";
                                                    
            		$mphone_query = ibase_prepare($trans, $mphone_sql);
            		ibase_execute($mphone_query);
            		ibase_free_query($mphone_query);
                  }
                        
            //add mphone end
         }   
         
        //SELECT NEXT INVOICEID START
		$task_id_sql = "SELECT GEN_ID(GEN_TASKS_ID, 1) FROM RDB\$DATABASE";
		$task_id_query = ibase_prepare($trans, $task_id_sql);
		$task_id_result = ibase_execute($task_id_query);
        $row=ibase_fetch_row($task_id_result);
        $task_id = $row[0];
        ibase_free_query($task_id_query);
        ibase_free_result($task_id_result);	
        //SELECT NEXT INVOICEID END
        
            $QueryText = "insert into TASKS (
                                                        id, 
                                                        taskdate, 
                                                        beginoftheinterval, 
                                                        endoftheinterval, 
                                                        patientid, 
                                                        doctorid, 
                                                        roomid, 
                                                        workplaceid, 
                                                        noticed, 
                                                        factofvisit, 
                                                        dateofvisit, 
                                                        timeofvisit, 
                                                        workdescription, 
                                                        comment, 
                                                        smsstatus)
                                            values($task_id, 
                                            '$task->TASKTASKDATE', 
                                            '$task->TASKBEGINOFTHEINTERVAL', 
                                            '$task->TASKENDOFTHEINTERVAL', 
                                            $task->TASKPATIENTID, 
                                            $task->TASKDOCTORID, 
                                            $task->TASKROOMID, 
                                            $task->TASKWORKPLACEID, 
                                            $task->TASKNOTICED, 
                                            $task->TASKFACTOFVISIT, 
                                            null, 
                                            null, 
                                            '".safequery($task->TASKWORKDESCRIPTION)."', 
                                            '".safequery($task->TASKCOMMENT)."', 
                                            $task->SMSSTATUS)";
                                            
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		//$success = ibase_commit($trans);
    		ibase_free_query($query);
        
        
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $task_id;		
    }
            
    /**
 	* @param CalendarModuleTableTask $task
    * @return boolean
 	*/ 	 	
    public function updateTableTask($task) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if($task->TASKTYPE == 3)
        {
             $QueryText = 			
    		"update TASKS
    		set TASKDATE='$task->TASKTASKDATE', 
                BEGINOFTHEINTERVAL='$task->TASKBEGINOFTHEINTERVAL',
    		    ENDOFTHEINTERVAL='$task->TASKENDOFTHEINTERVAL',
                ROOMID=$task->TASKROOMID";
                if($task->TASKWORKPLACEID!=null)
                {
                    $QueryText .= ", WORKPLACEID=$task->TASKWORKPLACEID";
                }
                if($task->TASKDOCTORID!=null)
                {
                    $QueryText .= ", DOCTORID=$task->TASKDOCTORID";
                }
                $QueryText .= ", COMMENT='".safequery($task->TASKCOMMENT)."'";
            $QueryText .= ", SMSSTATUS=0";
    		$QueryText .= " where ID=$task->TASKID";
    		$query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
            ibase_free_query($query);
        }
        else if($task->PRIMARYPATIENT == 1)
        {
    		$QueryText = 			
    		"update TASKS
    		set TASKDATE='$task->TASKTASKDATE', 
                BEGINOFTHEINTERVAL='$task->TASKBEGINOFTHEINTERVAL',
    		    ENDOFTHEINTERVAL='$task->TASKENDOFTHEINTERVAL', 
                PATIENTID=$task->TASKPATIENTID,
                ROOMID=$task->TASKROOMID";
                if($task->TASKWORKPLACEID!=null)
                {
                    $QueryText .= ", WORKPLACEID=$task->TASKWORKPLACEID";
                }
                $QueryText .= ", NOTICED=$task->TASKNOTICED,
                                 FACTOFVISIT=$task->TASKFACTOFVISIT";
                if($task->TASKDOCTORID!=null)
                {
                    $QueryText .= ", DOCTORID=$task->TASKDOCTORID";
                }
                $QueryText .= ", WORKDESCRIPTION='".safequery($task->TASKWORKDESCRIPTION)."', 
                                 COMMENT='".safequery($task->TASKCOMMENT)."'";
            $QueryText .= ", SMSSTATUS=$task->SMSSTATUS";
    		$QueryText .= " where ID=$task->TASKID";
    		$query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
            ibase_free_query($query);
        }
        else
        {
            $QueryText =  "update PATIENTS 
                set firstname='".safequery($task->FNAME)."', 
                    middlename='".safequery($task->MNAME)."', 
                    lastname='".safequery($task->LNAME)."',
                    shortname='".safequery($task->PSHORTNAME)."',
                    fullname='".safequery($task->LNAME)." ".safequery($task->FNAME)." ".safequery($task->MNAME)."',
                    mobilenumber='".safequery($task->PATIENTTELEPHONENUMBER)."'
                where ID = (select PATIENTS.ID from PATIENTS
                        left join TASKS on
                        PATIENTS.ID =  TASKS.PATIENTID
                        where TASKS.ID = $task->TASKID)";
            
    		$query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		ibase_free_query($query);
            
             //update mphone start
            $mphone_sql = "update patientsmobile set 
                                    mobilenumber='$task->PATIENTTELEPHONENUMBER'
                                    where  patientsmobile.patient_fk = (select PATIENTS.ID from PATIENTS
                                                                        left join TASKS on
                                                                        PATIENTS.ID =  TASKS.PATIENTID
                                                                        where TASKS.ID = $task->TASKID)";
    		$mphone_query = ibase_prepare($trans, $mphone_sql);
    		ibase_execute($mphone_query);
    		ibase_free_query($mphone_query);
            //update mphone end
            
    		$QueryText = 			
    		"update TASKS
    		  set TASKDATE='$task->TASKTASKDATE', 
                  BEGINOFTHEINTERVAL='$task->TASKBEGINOFTHEINTERVAL',
                  ENDOFTHEINTERVAL='$task->TASKENDOFTHEINTERVAL', 
                  PATIENTID=$task->TASKPATIENTID,
                  ROOMID=$task->TASKROOMID";
                if($task->TASKWORKPLACEID!=null)
                {
                    $QueryText .= ", WORKPLACEID=$task->TASKWORKPLACEID";
                }
                    
                $QueryText .= ", NOTICED=$task->TASKNOTICED,
                                 FACTOFVISIT=$task->TASKFACTOFVISIT";
                if($task->TASKDOCTORID!=null)
                {
                    $QueryText .= ", DOCTORID=$task->TASKDOCTORID";
                    $QueryText .= ", TASKTYPE=0";
                }
                $QueryText .= ", WORKDESCRIPTION='".safequery($task->TASKWORKDESCRIPTION)."', 
                COMMENT='".safequery($task->TASKCOMMENT)."'";
                
            $QueryText .= ", SMSSTATUS=$task->SMSSTATUS";
    		$QueryText .= " where ID=$task->TASKID";
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		//$success = ibase_commit($trans);
    		ibase_free_query($query);
        }
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
        return $success;	
    }
    
    /**
 	* @param string $taskId
    * @return boolean
 	*/ 	 	
    public function clearSMSsenderForTask($taskId) 
    { 
        $connection = new Connection();   
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText =  "update SMSSENDER set SMSSENDER.TASKID = null where SMSSENDER.TASKID = $taskId";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteTableTasks($Arguments) 
    {
     	$connection = new Connection();   
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = 			
		"delete from TASKS
		where (ID is null)";	
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }    
    
    /** 
    * @param bool $isFullName - загружать ли полные имена
    * @param bool $isLazyLoading - find type
    * @param string $findCharacters - first characters for search
 	* @return CalendarModulePatient[]
	*/ 
    public function getPatients($isFullName, $isLazyLoading, $findCharacters)  
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "SELECT ";
        if($isLazyLoading)
        {
            $QueryText .= "first 10 ";
        }
        $QueryText .= " p.ID, 
                        coalesce(p.CARDBEFORNUMBER,'')||coalesce(p.CARDNUMBER,'')||coalesce(p.CARDAFTERNUMBER,''), 
                        p.leaddoctorfio,
                        p.birthday, 
                        p.agreementtimestamp,
                        p.tretdoctors, 
                        p.GROUP_COLOR,
                        p.GROUP_DESCR,
                        ";
        if($isFullName)
        {
            $QueryText .= "p.lastname, p.firstname, p.middlename ";
        } 
        else
        {
            $QueryText .= "p.SHORTNAME ";
        }
        $QueryText .= "
                 from patients_grid_all as p ";
        if(is_numeric($findCharacters))
        {
            $QueryText .= "inner join patientsmobile
                    on p.id = patientsmobile.patient_fk ";
        }
        $QueryText .= "where ";
        if($isLazyLoading)
        {
            $QueryText .= "
                 ( lower( p.LASTNAME||' '||p.FIRSTNAME||' '||p.MIDDLENAME) starting lower('$findCharacters')
                    or lower(coalesce(p.CARDBEFORNUMBER,'')||coalesce(p.CARDNUMBER,'')||coalesce(p.CARDAFTERNUMBER,'')) containing lower('$findCharacters') ";
                if(is_numeric($findCharacters))
                {
                    $QueryText .= "or lower(patientsmobile.mobilenumber) containing lower('$findCharacters') )";
                }
                else
                {
                    $QueryText .= ")";
                } 
        }
		
		
		 
        //$QueryText .= " order by patients.LASTNAME";
		
        $query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		
		while ($row = ibase_fetch_row($result))
		{		
			$obj = new CalendarModulePatient;
			$obj->ID = $row[0];
            $obj->CARDNUMBER = $row[1];
            $obj->doctor_lead = $row[2];
			$obj->dob = $row[3];
            $obj->agreementTimestamp = $row[4];
            
            $obj->GROUP_COLOR = $row[6];
            $obj->GROUP_DESCR = $row[7];
            
            $obj->SHORTNAME = $row[8];
            
            if($isFullName)
            {
                if ($row[8] != null && $row[8] != "")
                {
                    if ($row[9] != null && $row[9] != "")
                    {
                        $obj->SHORTNAME .= " " . $row[9];
                    }
                    if ($row[10] != null && $row[10] != "")
                    {
                        $obj->SHORTNAME .= " " . $row[10];
                    }
                }
            }
            
            $obj->doctors = $row[5];
			$rows[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $rows; 
    }
    
    /** 
    * @param bool $isFullName
    * @param int $patientId
 	* @return CalendarModulePatient[]
	*/ 
    public function getPatientById($isFullName, $patientId)
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "SELECT patients.ID, 
                            coalesce(patients.CARDBEFORNUMBER,'')||coalesce(patients.CARDNUMBER,'')||coalesce(patients.CARDAFTERNUMBER,''),  
                            (select doctors.shortname from doctors where doctors.id = patients.leaddoctor_fk),
                            patients.agreementtimestamp,
                            CAST( (select LIST(distinct(doctors.shortname)) from doctors inner join toothexam on toothexam.doctorid = doctors.id where toothexam.id = patients.id) as VARCHAR(1000) ),
                            patients.birthday, " . ($isFullName ? "patients.lastname, patients.firstname, patients.middlename" : "patients.SHORTNAME");
        $QueryText .= " FROM PATIENTS
        
                  where  patients.ID =  $patientId order by patients.LASTNAME";
		
        $query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);		
		$rows = array();
		$row = ibase_fetch_row($result);
			$obj = new CalendarModulePatient;
			$obj->ID = $row[0];
            $obj->CARDNUMBER = $row[1];
            $obj->doctor_lead = $row[2];
            $obj->agreementTimestamp = $row[3];
			$obj->dob = $row[4];
            
			$obj->SHORTNAME = $row[6];
            
            
            if ($isFullName)
            {
                if ($row[7] != null && $row[7] != "")
                {
                    $obj->SHORTNAME .= " " . $row[7];
                    if ($row[8] != null && $row[8] != "")
                    {
                        $obj->SHORTNAME .= " " . $row[8];
                    }
                }
            }
               /*
                $QueryDoctorText =
    			"select distinct(doctors.id), doctors.shortname
                    from doctors
                    
                    left join toothexam
                    on toothexam.doctorid = doctors.id
                
                where toothexam.id = '$obj->ID'";
                //and (doctors.dateworkend is null or doctors.dateworkend < '".$nowTimestamp->format("D M j G:i:s")."')";
                $queryDoctor = ibase_prepare($trans, $QueryDoctorText);
                $resultDoctor=ibase_execute($queryDoctor);
                $doctorsString = '';
        		while($rowDoctor = ibase_fetch_row($resultDoctor))
        		{ 
        		  $doctorsString .= $rowDoctor[1].' ';
        		}	
        		ibase_free_query($queryDoctor);
        		ibase_free_result($resultDoctor);
            $obj->doctors = $doctorsString;
            */
            $obj->doctors = $row[5];
        
        $rows[] = $obj;
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $rows; 
    }        
    
        /** 
    * @param bool $isFullName
    * @param int $patientId
 	* @return CalendarModulePatient[]
	*/ 
    public function getPatientByIdWizard($isFullName, $patientId)
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        
        
		$QueryText = "SELECT 
        
        patients.ID, 
        coalesce(patients.CARDBEFORNUMBER,'')||coalesce(patients.CARDNUMBER,'')||coalesce(patients.CARDAFTERNUMBER,''),    
        (select doctors.shortname from doctors where doctors.id = patients.leaddoctor_fk),
        patients.birthday,
        CAST( (select LIST(distinct(doctors.shortname)) from doctors inner join toothexam on toothexam.doctorid = doctors.id where toothexam.id = patients.id) as VARCHAR(1000) ),
        " . ($isFullName ? "patients.lastname, patients.firstname, patients.middlename" : "patients.SHORTNAME");
        
        
        $QueryText .= " FROM PATIENTS
        
                  where patients.PRIMARYFLAG = 1 and patients.ID = $patientId 
                  order by patients.LASTNAME";
		
        $query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);		
		$rows = array();
		$row = ibase_fetch_row($result);
			$obj = new CalendarModulePatient;
			$obj->ID = $row[0];
            $obj->CARDNUMBER = $row[1];
            $obj->doctor_lead = $row[2];
			$obj->dob = $row[3];
            
			$obj->SHORTNAME = $row[5];
            
            
            if ($isFullName)
            {
                if ($row[6] != null && $row[6] != "")
                {
                    $obj->SHORTNAME .= " " . $row[6];
                    if ($row[7] != null && $row[7] != "")
                    {
                        $obj->SHORTNAME .= " " . $row[7];
                    }
                }
            }
               /*
                $QueryDoctorText =
    			"select distinct(doctors.id), doctors.shortname
                    from doctors
                    
                    left join toothexam
                    on toothexam.doctorid = doctors.id
                
                where toothexam.id = '$obj->ID'";
                //and (doctors.dateworkend is null or doctors.dateworkend < '".$nowTimestamp->format("D M j G:i:s")."')";
                $queryDoctor = ibase_prepare($trans, $QueryDoctorText);
                $resultDoctor=ibase_execute($queryDoctor);
                $doctorsString = '';
        		while($rowDoctor = ibase_fetch_row($resultDoctor))
        		{ 
        		  $doctorsString .= $rowDoctor[1].' ';
        		}	
        		ibase_free_query($queryDoctor);
        		ibase_free_result($resultDoctor);
            $obj->doctors = $doctorsString;
            */
            $obj->doctors = $row[4];
        $rows[] = $obj;
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $rows; 
    } 
    
    /** 
 	* @param string $nowdate
 	* @return CalendarModuleDoctor[]
	*/ 
    public function getDoctors($nowdate) //sqlite
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, SHORTNAME from DOCTORS 
                     where 
                        doctors.isfired != 1";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new CalendarModuleDoctor;
			$obj->ID = $row[0];
			$obj->SHORTNAME = $row[1];
			$rows[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /** 
 	* @param string[] $unvisibleRoomIds
 	* @return CalendarModuleRoom[]
	*/ 
    public function getRooms($unvisibleRoomIds) //sqlite
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select rooms.ID, rooms.NUMBER, rooms.NAME, rooms.ROOMCOLOR from ROOMS where rooms.id is not null ";
        foreach($unvisibleRoomIds as $room_id) {
            $QueryText .= "and rooms.id <> $room_id ";
        }
        $QueryText .= "order by rooms.sequincenumber, rooms.id";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new CalendarModuleRoom;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->NAME = $row[2];
            $obj->ROOMCOLOR = $row[3];
			$rows[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /**
 	* @param string $RoomID
    * @return CalendarModuleWorkPlace[]
 	*/  	  
    public function getWorkPlaces($RoomID)     //sqlite
     {      
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, ROOMID, NUMBER from WORKPLACES
				      where ROOMID=$RoomID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new CalendarModuleWorkPlace;
			$obj->ID = $row[0];
			$obj->ROOMID = $row[1];
			$obj->NUMBER = $row[2];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
	
	/** 
 	* @param string $startDate
    * @param string $endDate
    * @param resource $trans
 	* @return CalendarModulePatientDOB[]
	*/  
	public function getPatientsDOB($startDate, $endDate, $trans = null)
	{
	   
        $startDate = new DateTime($startDate);
        $endDate = new DateTime($endDate);
            
    	if(!$trans)
        {
            $connection = new Connection();
	        $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
            
		$QueryText = "select
                    p.id,
                    p.shortname, 
                    p.fullname,
                    p.telephonenumber,
                    (select
                        cast(LIST(patientsmobile.mobilenumber, '\n') as varchar(1000))
                        from patientsmobile
                        where patientsmobile.patient_fk = p.id
                         and patientsmobile.mobilenumber is not null
                         and patientsmobile.mobilenumber != ''
                         and patientsmobile.senderactive = 1),
                    p.email,
                    p.birthday
                    
        from patients p
        where p.birthday is not null
            and
            (cast(substring(100+extract(month from p.birthday) from 2 for 2) ||'.'|| substring(100+extract(day from p.birthday) from 2 for 2) as decimal(2,2) )) >= ".$startDate->format('n').".".$startDate->format('d')." 
            and
            (cast(substring(100+extract(month from p.birthday) from 2 for 2) ||'.'|| substring(100+extract(day from p.birthday) from 2 for 2) as decimal(2,2) )) <= ".$endDate->format('n').".".$endDate->format('d')." 
        order by (cast(substring(100+extract(month from p.birthday) from 2 for 2) ||'.'|| substring(100+extract(day from p.birthday) from 2 for 2) as decimal(2,2) )), p.fullname";
        
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
		while ($row = ibase_fetch_row($result))						// пїЅпїЅпїЅпїЅ пїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
		{
			$obj = new CalendarModulePatientDOB;
			$obj->ID = $row[0];
			$obj->SHORTNAME = $row[1];
			$obj->FULLNAME = $row[2];
			$obj->PHONE = $row[3];
			$obj->MOBILE = $row[4];
			$obj->EMAIL = $row[5];
            $obj->BIRTHDAY = $row[6];
			$rows[] = $obj;
		}	
		ibase_free_query($query);
		ibase_free_result($result);
        
		if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }	
        
		return $rows; 
	}
    
    //             Functions using in MultiCalendar
    
    /**
     * CalendarModule::getDoctor()
     * 
     * @param int $id
     * @return CalendarModuleDoctor
     */
    public function getDoctor($id)
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, SHORTNAME from DOCTORS where ID = $id";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$doc = new CalendarModuleDoctor;	
		if ($row = ibase_fetch_row($result))
		{
			$doc->ID = $row[0];
			$doc->SHORTNAME = $row[1];
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $doc;
    }
    
    /** 
    * @param int $id
 	* @return CalendarModuleRoom[]
	*/ 
    public function getRoom($id) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, NUMBER, NAME from ROOMS where id = $id";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$obj = new CalendarModuleRoom;		
		if ($row = ibase_fetch_row($result))
		{
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->NAME = $row[2];
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $obj; 
    }

    /**
     * CalendarModule::getScheduleOnCabinet()
     * 
     * @param int $id
     * @param string $startDate
     * @param string $endDate
     * @param resource $trans
     * @return CalendarModuleSchedule[]
     */
    private function getScheduleOnCabinet($id, $startDate, $endDate, $trans)
    {
        $QueryText = "select distinct
                operatingschedule.id,
                operatingschedule.name,
                operatingschedule.startdate,
                operatingschedule.enddate,
                operatingschedule.iscurrent, 
                doctors.id,
                doctors.shortname
            from operatingschedule
            inner join operatingschedulerules on (operatingschedule.id = operatingschedulerules.operatingscheduleid)
            inner join doctors on (operatingschedule.doctorid = doctors.id)
            where (operatingschedulerules.cabinetid = '$id' or operatingschedulerules.isworking = 0)
                and operatingschedule.iscurrent = 1
                and doctors.isfired != 1
                and ((operatingschedule.startdate <= '$startDate' and operatingschedule.enddate >= '$startDate')
                or (operatingschedule.startdate <= '$endDate' and operatingschedule.enddate >= '$endDate')
                or (operatingschedule.startdate >= '$startDate' and operatingschedule.enddate <= '$endDate'))";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $schedules = array();
        while ($row = ibase_fetch_row($result))
        {
            $sch = new CalendarModuleSchedule;
            $sch->id = $row[0];
            $sch->name = $row[1];
            $sch->startDate = $row[2];
            $sch->endDate = $row[3];
            $sch->isCurrent = $row[4];
            $sch->Doctor = new CalendarModuleDoctor;
            $sch->Doctor->ID = $row[5];
            $sch->Doctor->SHORTNAME = $row[6];
            $sch->getRules($trans, $id);
            //массив дней с рабочими интервалами
            $days = $this->createDaysArray($startDate, $endDate);   
            
            foreach ($days as $key => $day)
            {
                //попадает ли заданый интервал в промежуток, на который распространяется график
                if (strtotime($day->day) >= strtotime($sch->startDate) && strtotime($day->day) <= strtotime($sch->endDate))
                {
                    foreach($sch->Rules as $rule)
                    {
                        if ($rule->isWorking)
                            $rule->processing($day); 
                    }
                    foreach($sch->Rules as $rule)
                    {
                        if (!$rule->isWorking)
                        {                          
                            $rule->processing($day); 
                        }                                                    
                    }   
                }
                
                //очистка всех пустых дней
                if ($day->timeIntervals == null || count($day->timeIntervals) == 0)
                {
                    unset($days[$key]);
                }
            }
            $sch->daySchedule = $days;
            
            //добавляем расписание врача, если в нем есть записи
            if (count($days) != 0)
                $schedules[] = $sch;
        }        
        ibase_free_query($query);
        ibase_free_result($result);

        return $schedules;
    }
    
    /**
     * CalendarModule::getScheduleOnDoctor()
     * 
     * @param int $id
     * @param string $startDate
     * @param string $endDate
     * @param resource $trans
     * @return CalendarModuleSchedule[]
     */
    private function getScheduleOnDoctor($id, $startDate, $endDate, $trans)
    {
        $QueryText = "select first 1
                operatingschedule.id,
                operatingschedule.name,
                operatingschedule.startdate,
                operatingschedule.enddate,
                operatingschedule.iscurrent, 
                (select doctors.id from doctors where doctors.id = operatingschedule.doctorid),
                (select doctors.shortname from doctors where doctors.id = operatingschedule.doctorid)

            from operatingschedule
            inner join operatingschedulerules on (operatingschedule.id = operatingschedulerules.operatingscheduleid)
                        
            where operatingschedule.doctorid = '$id' and operatingschedule.iscurrent = 1
                and ((operatingschedule.startdate <= '$startDate' and operatingschedule.enddate >= '$startDate')
                or (operatingschedule.startdate <= '$endDate' and operatingschedule.enddate >= '$endDate')
                or (operatingschedule.startdate >= '$startDate' and operatingschedule.enddate <= '$endDate'))";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $schedules = array();
        while ($row = ibase_fetch_row($result))
        {
            $sch = new CalendarModuleSchedule;
            $sch->id = $row[0];
            $sch->name = $row[1];
            $sch->startDate = $row[2];
            $sch->endDate = $row[3];
            $sch->isCurrent = $row[4];
            $sch->Doctor = new CalendarModuleDoctor;
            $sch->Doctor->ID = $row[5];
            $sch->Doctor->SHORTNAME = $row[6];
            $sch->getRules($trans);
            
            //массив дней с рабочими интервалами
            $days = $this->createDaysArray($startDate, $endDate);   
            
            foreach ($days as $key => $day)
            {
                //попадает ли заданый интервал в промежуток, на который распространяется график
                if (strtotime($day->day) >= strtotime($sch->startDate) && strtotime($day->day) <= strtotime($sch->endDate))
                {
                    foreach($sch->Rules as $rule)
                    {
                        if ($rule->isWorking)
                            $rule->processing($day); 
                    }
                    foreach($sch->Rules as $rule)
                    {
                        if (!$rule->isWorking)
                        {                          
                            $rule->processing($day); 
                        }                                                    
                    }   
                }
                
                //очистка всех пустых дней
                if ($day->timeIntervals == null || count($day->timeIntervals) == 0)
                {
                    unset($days[$key]);
                }
                
                //сортируем интервалы по дате начала
                usort($day->timeIntervals, "CalendarModuleInterval::cmp");
            }
            $sch->daySchedule = $days;
            
            //добавляем расписание врача, если в нем есть записи
            if (count($days) != 0)
                $schedules[] = $sch;
        }        
        ibase_free_query($query);
        ibase_free_result($result);

        return $schedules;
    }


/**
     * CalendarModule::getScheduleOnDoctorWizard()
     * 
     * @param int $id
     * @param string $startDate
     * @param string $endDate 
     * @param int $min_diap
     * @param CalendarModuleDaySchedule[] $def_days
     * @param resource $trans
     * @return CalendarModuleSchedule[]
     */
    private function getScheduleOnDoctorWizard($id, $startDate, $endDate, $min_diap, $def_days, $trans)
    {
        
        $doc_tasks=$this->getTableTasksFilteredMultiDate($startDate,$endDate,"",$id,"","","-1","-1","","", $trans);
        $QueryText = "select first 1
                operatingschedule.id,
                operatingschedule.startdate,
                operatingschedule.enddate
            from operatingschedule
            where operatingschedule.doctorid = '$id' and operatingschedule.iscurrent = 1
                and ((operatingschedule.startdate <= '$startDate' and operatingschedule.enddate >= '$startDate')
                or (operatingschedule.startdate <= '$endDate' and operatingschedule.enddate >= '$endDate')
                or (operatingschedule.startdate >= '$startDate' and operatingschedule.enddate <= '$endDate'))";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $schedules=array();                
        $sch = new CalendarModuleSchedule;
        $sch->Doctor = new CalendarModuleDoctor;
        $sch->Doctor->ID=$id;
                
        if ($row = ibase_fetch_row($result))
        {               
            $sch->id = $row[0];
            $sch->startDate = ($row[1]>$startDate)?$row[1]:$startDate;
            $sch->endDate = ($row[2]<$endDate)?$row[2]:$endDate;
            $sch->getRules($trans);
            
            //массив дней с рабочими интервалами
            $days = $this->createDaysArray($sch->startDate, $sch->endDate);   
            foreach ($days as $key => $day)
            {
                foreach($sch->Rules as $rule)
                {
                    if ($rule->isWorking)
                        $rule->processing($day); 
                }
                foreach($sch->Rules as $rule)
                {
                    if (!$rule->isWorking)
                    {                          
                        $rule->processing($day); 
                    }                                                    
                } 
                
                if ($day->timeIntervals == null || count($day->timeIntervals) == 0)
                {
                    unset($days[$key]);
                }
                
                //сортируем интервалы по дате начала
                usort($day->timeIntervals, "CalendarModuleInterval::cmp");
            }
        }
        else
        {
           $sch->startDate = $startDate;
           $sch->endDate = $endDate;
           $days=$def_days;
        }                  
        

          foreach ($days as $key => $day)
          {      
                foreach($doc_tasks as $task)
                {
                    
                   if(strtotime($day->day)==strtotime($task->TASKTASKDATE))
                   {    
                        //echo("\ntask=".$task->TASKTASKDATE." day=".$day->day."\n");
                        foreach($day->timeIntervals as $ii=>$interval)
                        {
                            //echo("task=".$task->TASKBEGINOFTHEINTERVAL."-".$task->TASKENDOFTHEINTERVAL." day=".$interval->startTime."-".$interval->endTime."\n");
                            $uns=false;
                            if(strtotime($task->TASKBEGINOFTHEINTERVAL)>strtotime($interval->startTime)&&strtotime($task->TASKBEGINOFTHEINTERVAL)<strtotime($interval->endTime))
                            {
                                //echo("begin=".(strtotime($task->TASKBEGINOFTHEINTERVAL)-strtotime($interval->startTime))."\n");
                                if((strtotime($task->TASKBEGINOFTHEINTERVAL)-strtotime($interval->startTime)>=$min_diap))
                                {
                                    $new_interval=new CalendarModuleInterval();
                                    $new_interval->startTime=$day->timeIntervals[$ii]->startTime;
                                    $new_interval->endTime=$task->TASKBEGINOFTHEINTERVAL;
                                    $new_interval->cabinetID=$interval->cabinetID;                                    
                                    $day->timeIntervals[]=$new_interval;
                                }
                                $uns=true;
                            }
                                                        
                            
                            if(strtotime($task->TASKENDOFTHEINTERVAL)>strtotime($interval->startTime)&&strtotime($task->TASKENDOFTHEINTERVAL)<strtotime($interval->endTime))
                            {
                                //echo("end=".(strtotime($interval->endTime)-strtotime($task->TASKENDOFTHEINTERVAL))."\n");
                                if(strtotime($interval->endTime)-strtotime($task->TASKENDOFTHEINTERVAL)>=$min_diap)
                                {   
                                    $new_interval=new CalendarModuleInterval();
                                    $new_interval->endTime=$day->timeIntervals[$ii]->endTime;
                                    $new_interval->startTime=$task->TASKENDOFTHEINTERVAL;
                                    $new_interval->cabinetID=$interval->cabinetID;                                    
                                    $day->timeIntervals[]=$new_interval;
                                }
                                $uns=true;
                            }
                            if((strtotime($task->TASKBEGINOFTHEINTERVAL)<=strtotime($interval->startTime))&&(strtotime($task->TASKENDOFTHEINTERVAL)>=strtotime($interval->endTime))) $uns=true;
                            //print_r($day->timeIntervals);
                            if($uns) unset($day->timeIntervals[$ii]); 
                            //print_r($day->timeIntervals);                                         
                        }
                        
                   }
                }
                
                //очистка всех пустых дней
                if ($day->timeIntervals == null || count($day->timeIntervals) == 0)
                {
                    unset($days[$key]);
                }
                
                //сортируем интервалы по дате начала
                usort($day->timeIntervals, "CalendarModuleInterval::cmp");
            }
            $sch->daySchedule = $days;
            
            //добавляем расписание врача, если в нем есть записи
            if (count($days) != 0)
                $schedules[] = $sch;     
        ibase_free_query($query);
        ibase_free_result($result);

        return $schedules;
    }
    
    /**
     * CalendarModule::getScheduleOnCabinets()
     * 
     * @param Number[] $cabinetIDs
     * @param string $startDate
     * @param string $endDate
     * @return CalendarModuleSchedule[]
     */
    public function getScheduleOnCabinets($cabinetIDs, $startDate, $endDate)
    {
            $connection = new Connection();
    		$connection->EstablishDBConnection();
    		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $rez = array();
            foreach ($cabinetIDs as $cabID)
            {
                $rez[] = $this->getScheduleOnCabinet($cabID, $startDate, $endDate, $trans);
            }
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $rez;
    }
    
    /**
     * CalendarModule::getScheduleOnDoctors()
     * 
     * @param Number[] $doctorIDs
     * @param string $startDate
     * @param string $endDate
     * @param object $trans
     * @return Object[]
     */
    public function getScheduleOnDoctors($doctorIDs, $startDate, $endDate, $trans = null)
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
        
            $rez = array();
            foreach ($doctorIDs as $docID)
            {
                $rez[] = $this->getScheduleOnDoctor($docID, $startDate, $endDate, $trans);
            }
            
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $rez;
    }
    
    /**
     * CalendarModule::getScheduleOnDoctorsWizard()
     * 
     * @param Number[] $doctorIDs
     * @param string $startDate
     * @param string $endDate
     * @param int $min_diap
     * @param Object[] $clinic_schedule
     * @return Object[]
     */
    public function getScheduleOnDoctorsWizard($doctorIDs, $startDate, $endDate, $min_diap, $clinic_schedule)
    {       
            $connection = new Connection();
    		$connection->EstablishDBConnection();
    		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
      
            $def_days = $this->getDefaultDays($clinic_schedule, $startDate, $endDate);

                 
            $rez = array();
            foreach ($doctorIDs as $docID)
            {
                $rez[] = $this->getScheduleOnDoctorWizard($docID, $startDate, $endDate, $min_diap, $def_days, $trans);
            }
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $rez;
    }
    
    private function getDefaultRules($clinic_schedule)
    {
        $Rules=array();
        foreach($clinic_schedule as $weekday)
        {
            $rule = new CalendarModuleScheduleRule;
            
            $rule->isWorking = 1;
            $rule->daysRange = CalendarModuleSchedule::xmlForEveryDay($weekday->xml);
            $rule->repeatType = 2;
            $rule->skipDaysCount = 0;
            $rule->startTime = $weekday->startTime;
            $rule->endTime = $weekday->endTime;
            $Rules[] = $rule; 
        } 
        return $Rules;          
    } 
    
    private function getDefaultDays($clinic_schedule, $startDate, $endDate)
    {
            $def_rules=$this->getDefaultRules($clinic_schedule);
            
            $def_days = $this->createDaysArray($startDate, $endDate);   
            foreach ($def_days as $key => $day)
            {
                foreach($def_rules as $rule)
                {
                        $rule->processing($day); 
                }
                
                if ($day->timeIntervals == null || count($day->timeIntervals) == 0)
                {
                    unset($def_days[$key]);
                }
                
                //сортируем интервалы по дате начала
                usort($day->timeIntervals, "CalendarModuleInterval::cmp");
            }
        return $def_days;
    }        
    
    /**
     * CalendarModule::getCalendarData()
     * 
     * @param CalendarModuleRequestData $request
     * @return CalendarModuleResponseData[]
     */
    public function getCalendarData($request)
    {
            $connection = new Connection();
    		$connection->EstablishDBConnection();
    		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $rez = array();
            foreach ($request->ids as $key => $id)
            {
                $calendar = new CalendarModuleResponseData;
                $docid = "";
                $cabid = "";
                if ($request->types[$key] == "TASKROOMID")//Cabinet
                {
                    $cabid = $id;
                    $calendar->schedules = $this->getScheduleOnCabinet($cabid, $request->taskdatestart, $request->taskdateend, $trans);
                }
                elseif ($request->types[$key] == "TASKDOCTORID")//Doctor
                {
                    $docid = $id;
                }
                //$calendar->tasks = $this->getTasksFilteredMultiDate($request->taskdatestart, $request->taskdateend, $request->patientid, $docid, $cabid, "", $request->noticed, $request->factofvisit, "","", $trans);
                $calendar->XMLcollection = $this->getTasksFilteredMultiDate2($request->taskdatestart, $request->taskdateend, $request->patientid, $docid, $cabid, "", $request->noticed, $request->factofvisit, "","", $trans);
                
                $rez[] = $calendar;
            }
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $rez;
    }
    

     /**
      * CalendarModule::createDaysArray()
      * 
      * @param string $startDate YYYY-MM-DD
      * @param string $endDate YYYY-MM-DD
      * @return CalendarModuleDaySchedule[]
      */
     private function createDaysArray($startDate, $endDate)
     {
        //откидывание лишних часов, секунд, минут
        $s = strtotime($startDate);
        $e = strtotime($endDate);

        $days = abs($s - $e) / 86400 + 1; 
        $intervals = array();
        for ($i=0; $i<$days; $i++)
        {
            $interv = new CalendarModuleDaySchedule;
            $interv->day = date("Y-m-d", $s + 86400*$i);
            $interv->timeIntervals = array();
            $intervals[] = $interv;
        }
        return $intervals;
     }
     
     /**
     * CalendarModule::sendBirthDayMessage()
     * 
     * @param Number[] $ids
     * @return int
     */
    public function sendBirthDayMessage($ids)
    {
        include_once('SMSSenderModule.php');
        if (count($ids) == 0) return 0;
        $smsModule = new SMSSenderModule;
        $settings = $smsModule->getSMSSettings();
        $filter = new SMSSenderModuleFilter;
        $filter->field = 'patients.id';
        $filter->type = SMSSenderModuleFilter::IN;
        $filter->value = '';
        foreach ($ids as $id)
        {
            $filter->value .= ($filter->value == '' ? $id : ", $id"); 
        }
        $filter->value = "($filter->value)";
        return $smsModule->sendMassMessage($settings->SMSDOBTemplate, array($filter));
    }
    
    /**
 	* @param string $task_id
 	* @param string $noticed
    * @return boolean
 	*/ 	 	
    public function updateTableTaskNoticed($task_id, $noticed) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update tasks set tasks.noticed = '$noticed' where tasks.id = '$task_id'";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param string $task_id
 	* @param string $visited
    * @return boolean
 	*/ 	 	
    public function updateTableTaskVisited($task_id, $visited) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update tasks set tasks.factofvisit = '$visited' where tasks.id = '$task_id'";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param string $task_id
 	* @param string $outcome
    * @return boolean
 	*/ 	 	
    public function updateTableTaskOutcome($task_id, $outcome) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update tasks set tasks.FACTOFOUT = '$outcome' where tasks.id = '$task_id'";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
     * @param string $doc_type
 	* @param string $startTime	
 	* @param string $endTime
     * @return string
     */
     public function printPatientsDOB($doc_type, $startTime, $endTime)
     {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                
        $ids=$this->getPatientsDOB($startTime, $endTime, $trans);
        $res=array();
        foreach($ids as $id)
        {
            $res[]=new PrintDataModule_PatientData($id->ID, $trans);
        }
        $TBS = new clsTinyButStrong;
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
       
       $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
       $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/PatientsDOB.'.$doc_type, OPENTBS_ALREADY_XML);

         

            //$TBS->NoErr=true;
            $TBS->MergeBlock('DATA',$res);
            //echo($TBS->Source);
            
            $file_name = 'patients_dob_'.$startTime.'_-_'.$endTime;
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().$file_name.'.'.$doc_type;  
              
        
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
            return $form_path; 
     }
   
            /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deletePatientsDOBPrint($form_path)
    {
        return deleteFileUtils($form_path);
    }
    
        /**
     * @param string $paramName  
     * @param string $paramValue  
     * @return boolean
     */
    public static function setApplicationSetting($paramName, $paramValue)
    {
        return setApplicationSetting($paramName, $paramValue);
    } 
    
        /**
     * @param string[] $paramNames  
     * @return string[]
     */
    public static function getApplicationSettings($paramNames)
	{
	   $res=array();
       foreach($paramNames as $paramName)
       {
            $res[]=getApplicationSetting($paramName);
       }
	   
	   return $res;
	} 
}

?>

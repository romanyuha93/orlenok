<?php

require_once "Connection.php";
require_once "Utils.php";

class EmailService_Settings
{
   /**
   * @var string
   */
	var $EmailService_email;
   /**
   * @var string
   */
	var $EmailService_emailPassword; 
   /**
   * @var string
   */
	var $EmailService_smtpServer; 
   /**
   * @var string
   */
	var $EmailService_smtpPortServer;  
}

class EmailService
{
	/**
	* @param EmailService_Settings $settings
	* @return boolean
	*/
	public function saveSettings($settings)
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        foreach(get_object_vars($settings) as $key => $value)
        {
            $settings_sql = "update or insert into 
                                applicationsettings(parametername, parametervalue) 
                            values('$key', '$value') 
                                matching(parametername)";
            $settings_query = ibase_prepare($trans, $settings_sql);
            ibase_execute($settings_query);
            ibase_free_query($settings_query);
        }
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $success;
	}
    
   	/**
	* @return EmailService_Settings
	*/
	public function getSettings()
	{
		$connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $settings = $this->_getSettings($trans);
       	ibase_commit($trans);
       	$connection->CloseDBConnection();
        return $settings;
	}
    
    /**
	* @param EmailService_Settings $settings
	* @return boolean
	*/
	public function testEmail($settings)
	{
        //require_once (dirname(__FILE__).'/../../ZendFramework/library/Zend/Mail.php');
        //require_once (dirname(__FILE__).'/../../ZendFramework/library/Zend/Mail/Transport/Smtp.php');
        require_once 'Zend/Mail.php';
        require_once 'Zend/Mail/Transport/Smtp.php';

        $smtpOptions = array(
            'auth'      => 'login',
            'username'  => $settings->EmailService_email,
            'password'  => $settings->EmailService_emailPassword,
            'ssl'       => 'ssl',
            'port'      => $settings->EmailService_smtpPortServer
        );

        try{
            $mailTransport = new Zend_Mail_Transport_Smtp($settings->EmailService_smtpServer, $smtpOptions);
            $mail = new Zend_Mail('utf-8');
            $mail->addTo($settings->EmailService_email, $settings->EmailService_email);
            $mail->setFrom($settings->EmailService_email, '');
            $mail->setSubject('Проверочное письмо');
            $mail->setBodyText('Данное письмо служит для проверки правильности настроек SMTP сервера для правильной работы программы "Зубная фея". ');

            $mail->send($mailTransport);
        }catch(Exception $exc){
            return false;
        }
        
	    return true;
	}
    
    /**
     * getSettings() 
     * 
     * @param resource $trans
     * @return array
     */
    private function _getSettings($trans)
    {
        $QueryText = "select PARAMETERNAME, PARAMETERVALUE from APPLICATIONSETTINGS where parametername like 'EmailService%'";
        $query = ibase_prepare($trans, $QueryText);
    	$result = ibase_execute($query);
        $settingsArray = array();
        while($row = ibase_fetch_row($result))
        {
            $settingsArray[$row[0]] = $row[1];
        }
        $settings = new EmailService_Settings;
        $settings->EmailService_email = @$settingsArray['EmailService_email'];
        $settings->EmailService_emailPassword = @$settingsArray['EmailService_emailPassword'];
        $settings->EmailService_smtpServer = @$settingsArray['EmailService_smtpServer'];
        $settings->EmailService_smtpPortServer = @$settingsArray['EmailService_smtpPortServer'];
        ibase_free_query($query);
        
        return $settings;
    }
}

?>
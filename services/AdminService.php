<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
//define('tfClientId','online');
//echo AdminService::checkAutorization('admin','1');
 //print_r(AdminService::getClientSettings());           

class AdminModuleDoctorAccessObject
{
    /**
    * @var string
    */
    var $DoctorID;
    
    /**
    * @var string
    */
    var $DoctorLname;
    
    /**
    * @var string
    */
    var $DoctorFname;
    
    /**
    * @var string
    */
    var $DoctorMname;
    
    /**
    * @var string
    */
    var $DoctorAdminID;
    
    /**
    * @var string
    */
    var $DoctorAdminLogin;
    
    /**
    * @var string
    */
    var $DoctorAdminPassword;
    
    /**
    * @var int
    */
    var $DoctorAdimAccessAllow;
}

class AdminModuleDoctorAccessObjectToSet
{
    /**
    * @var string
    */
    var $DoctorID;
    
    /**
    * @var string
    */
    var $DoctorAdminLogin;
    
    /**
    * @var string
    */
    var $DoctorAdminPassword;
    
    /**
    * @var int
    */
    var $DoctorAdimAccessAllow;
}

class AdminModuleJournalRecord
{
    /**
    * @var string
    */
    var $ID;
    
     /**
    * @var string
    */
    var $ACCOUNTID;
    
    /**
    * @var string
    */
    var $TABLENAME;
    
    /**
    * @var string
    */
    var $FIELDNAME;
    
    /**
    * @var string
    */
    var $RECORDID;
    
    /**
    * @var string
    */
    var $OLDVALUE;
    
    /**
    * @var string
    */
    var $NEWVALUE;
    
    /**
    * @var string
    */
    var $DATETIME;   
       
    /**
    * @var string
    */
    var $USER;
}
class AdminModuleClientSetting
{
    /**
    * @var string
    */
    var $SETTINGNAME;
    
    /**
    * @var string
    */
    var $SETTINGVALUE;
}
class AdminModuleClientColorSetting
{
    /**
    * @var string
    */
    var $SETTINGNAME;
    
    /**
    * @var int
    */
    var $SETTINGVALUE;
}
class AdminModuleSubdivision
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $NAME;
    /**
    * @var string
    */
    var $CITY;
    /**
    * @var string
    */
    var $ADDRESS;
    
    /**
    * @var int
    */
    var $ORDERNUMBER_CASH;
    /**
    * @var int
    */
    var $ORDERNUMBER_CLEARING;
    /**
    * @var int
    */
    var $INVOICENUMBER;
}
class AdminModuleAutorizeObject
{
    /**
    * @var string
    */
    var $ACCOUNT_ID;
    
    /**
    * @var boolean
    */
    var $ACCOUNT_ACCESS;
    
    /**
    * @var string
    */
    var $ACCOUNT_USER;
    /**
    * @var string
    */
    var $ACCOUNT_PASSWORD;
    /**
    * @var string
    */
    var $ACCOUNT_KEY;
    /**
    * @var int
    */
    var $ACCOUNT_CHECK;
    
    /**
    * @var string
    */
    var $ACCOUNT_LASTNAME;
    
        /**
    * @var string
    */
    var $ACCOUNT_FIRSTNAME;
    
        /**
    * @var string
    */
    var $ACCOUNT_MIDDLENAME;  
    
    /**
    * @var string
    */
    var $ACCOUNT_SHORTNAME;
        
    /**
    * @var string
    */
    var $DOCTOR_ID;
    
    /**
    * @var string
    */
    var $DOCTOR_SHORTNAME;
        
    /**
    * @var string
    */
    var $DOCTOR_SPECIALITY;
    
    /**
    * @var int
    */
    var $SPEC;
    
     /**
    * @var string
    */
    var $SUBDIVISIONID;
    
    /**
    * @var string
    */
    var $SUBDIVISIONNAME; 
    
    /**
    * @var string
    */
    var $SUBDIVISIONCITY;
    
    /**
    * @var string
    */
    var $LOGO;
    
    /**
    * @var string
    */
    var $ACCOUNT_CASHIERCODE;
    
    /**
     * @var string
     */
    var $INFO2;
    
    
    
    /**
     * @var string
     */
    var $ACCOUNTTYPE;
    /**
     * @var string
     */
    var $TAX_ID;
    /**
     * @var string
     */
    var $BIRTH_DATE;
    /**
     * @var string
     */
    var $BIRTH_PLACE;
    /**
     * @var int
     */
    var $GENDER;
    /**
     * @var string
     */
    var $EMAIL;
    /**
     * @var string
     */
    var $USER_POSITION;
    
    
    /**
     * @var string
     */
    var $PHONE_TYPE;
    /**
     * @var string
     */
    var $PHONE;
    /**
     * @var string
     */
    var $DOC_TYPE;
    /**
     * @var string
     */
    var $DOC_NUMBER;
    /**
     * @var boolean
     */
    var $IS_OWNER;
    
    //don query in get client and in sqllite
    /**
     * @var string
     */
    var $EH_ACCESS_TOKEN;
    /**
     * @var string
     */
    var $EH_ACCESS_TOKEN_EXPIRY;
    /**
     * @var string
     */
    var $EH_SECRET_KEY;
    /**
     * @var string
     */
    var $OA_DIR;
    //don query in get client and in sqllite
    
    
}
class AdminModuleJournalFilter
{
    /**
    * @var string
    */
    var $TABLENAME;
    
    /**
    * @var string[]
    */
    var $FIELDNAMES;
}
class AdminModuleAssistant
{
   /**
   * @var int
   */
	var $assid;
   /**
   * @var string
   */
	var $name; 
}
class AdminModuleHR
{
     /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $LASTNAME;
    
        /**
    * @var string
    */
    var $FIRSTNAME;
    
        /**
    * @var string
    */
    var $MIDDLENAME;  
    
    /**
    * @var string
    */
    var $SPECIALITY;
    
    /**
    * @var string
    */
    var $SHORTNAME;

    /**
    * @var string
    */
    var $ACCOUNTID;
    
    
    /**
    * @var int
    */
    var $type;
}
class AdminModuleCabinet
{
    /**
    * @var string
    */
    var $id;
    
    /**
    * @var string
    */
    var $name;
    /**
    * @var string
    */
    var $color;
}

class AdminModuleAccountPhone
{
    /**
    * @var string
    */
    var $id;
    
    /**
    * @var string
    */
    var $account_fk;
    /**
    * @var string
    */
    var $phone;
    /**
    * @var string
    */
    var $phone_type;
}

class AdminModuleAccountEducation
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $account_fk;
    /**
    * @var string
    */
    var $country;
    /**
    * @var string
    */
    var $city;
    /**
    * @var string
    */
    var $institution_name;
    /**
    * @var string
    */
    var $issued_date;
    /**
    * @var string
    */
    var $diploma_number;
    /**
    * @var string
    */
    var $degree;
    /**
    * @var string
    */
    var $speciality;
}


class AdminService
{
    /**
     * @param AdminModuleDoctorAccessObject $p1
     * @param AdminModuleDoctorAccessObjectToSet $p2
     * @param AdminModuleJournalRecord  $p3
     * @param AdminModuleClientSetting $p4
     * @param AdminModuleAutorizeObject $p5
     * @param AdminModuleClientColorSetting $p6
     * @param AdminModuleAssistant $p7
     * @param AdminModuleHR $p8
     * @param AdminModuleJournalFilter $p9
     * @param AdminModuleCabinet $p10
     * @return void
     */
	 
    public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10){}

    
    
	/**
	* @param string $oldpass
	* @param string $newpass
	* @param string $passType
	* @return boolean
	*/
	public function savePassword($oldpass, $newpass, $passType)
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "SELECT PASSWRD FROM USERS WHERE NAME=";
        $pass = "'passForDel'";
        if($passType == '0')
        {
       	    $QueryText .= "'passForDel'";
        }
        else if($passType == '1')
        {
            $QueryText .= "'passForAccountDel'";
            $pass = "'passForAccountDel'";
        }
        else if($passType == '2')
        {
            $QueryText .= "'passForTreatDel'";
            $pass = "'passForTreatDel'";
        }
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$success = true;
		if ($row = ibase_fetch_row($result))
		{
			$success = ($row[0] == $oldpass);
		}
		ibase_free_query($query);
		ibase_free_result($result);
		if ($success)
		{
			$QueryText = "UPDATE OR INSERT into USERS (NAME, PASSWRD) values (".$pass.", '".safequery($newpass)."') MATCHING (NAME)";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			ibase_free_query($query);
		}
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
	}
    
    
   	/**
	* @return string
	*/
	public function checkPasswords()
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$sqlCheck = "SELECT NAME, PASSWRD FROM USERS WHERE NAME containing 'pass' order by NAME";
        
        
		$queryCheck = ibase_prepare($trans, $sqlCheck);
		$resultCheck = ibase_execute($queryCheck);
        $result = '000';
        
        $mainPass = '0';
        $accountPass = '0';
        $treatPass = '0';
        
        $index = 0;
		while ($row = ibase_fetch_row($resultCheck))
		{
            $pass = $row[1];
			if($index == 0)
            {
                if($pass == '7777')
                {
                    $accountPass = 2;
                }
                else if(strlen($pass) < 3)
                {
                    $accountPass = 1;
                }
            }
            else if ($index == 1)
            {
                if($pass == '7777')
                {
                    $mainPass = 2;
                }
                else if(strlen($pass) < 3)
                {
                    $mainPass = 1;
                }
            }
            else
            {
                if($pass == '7777')
                {
                    $treatPass = 2;
                }
                else if(strlen($pass) < 3)
                {
                    $treatPass = 1;
                }
            }
            $index++;
		}
		$result = $mainPass.$accountPass.$treatPass;
		ibase_free_query($queryCheck);
		ibase_free_result($resultCheck);
		
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $result;
	}
    
   	/**
	* @return AdminModuleDoctorAccessObject[]
	*/
	public function getDoctorUsers()
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select DOCTORS.ID, DOCTORS.LASTNAME, DOCTORS.FIRSTNAME, DOCTORS.MIDDLENAME, USERS.ID, USERS.NAME, USERS.PASSWRD, USERS.ACCESSALLOW
                        from DOCTORS
                        left join USERS
                           on DOCTORS.ID = USERS.DOCTORID";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$obj = array();
		while ($row = ibase_fetch_row($result))
		{
            $item = new AdminModuleDoctorAccessObject;
            $item->DoctorID = $row[0];
            $item->DoctorLname = $row[1];
            $item->DoctorFname = $row[2];
            $item->DoctorMname = $row[3];
            $item->DoctorAdminID = $row[4];
            $item->DoctorAdminLogin = $row[5];
            $item->DoctorAdminPassword = $row[6];
            $item->DoctorAdimAccessAllow = $row[7];
            $obj[] = $item;
		}
		ibase_free_query($query);
		ibase_free_result($result);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $obj;
	}
       	/**
	* @return AdminModuleHR[]
	*/
	public function getHRs()
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select d.id, d.lastname, d.middlename, d.firstname, d.speciality, d.shortname, d.accountid, 1  from doctors d
                        where  isfired is null or isfired != 1
                        union
                      select '1_'||a.id, a.lastname, a.middlename, a.firstname, null, a.shortname, a.accountid, 0  from assistants a
                        where  isfired is null or isfired != 1";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$obj = array();
		while ($row = ibase_fetch_row($result))
		{
            $item = new AdminModuleHR;
            $item->ID = $row[0];
            $item->LASTNAME = $row[1];
            $item->MIDDLENAME = $row[2];
            $item->FIRSTNAME = $row[3];
            $item->SPECIALITY = $row[4];
            $item->SHORTNAME = $row[5];
            $item->ACCOUNTID = $row[6];
            $item->type = $row[7];
            
            $obj[] = $item;
		}
		ibase_free_query($query);
		ibase_free_result($result);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $obj;
	}
    
	/**
	* @param AdminModuleDoctorAccessObjectToSet[] $doctors
	* @return boolean
	*/
	public function setDoctorUsers($doctors)
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        foreach ($doctors as $item)
        {
            if(($item->DoctorAdminLogin != "")&&($item->DoctorAdminLogin != null))
            {
                $QueryText = "UPDATE OR INSERT into USERS (NAME, PASSWRD, ACCESSALLOW, DOCTORID) values ('".safequery($item->DoctorAdminLogin)."', '".safequery($item->DoctorAdminPassword)."', '".$item->DoctorAdimAccessAllow."', '".$item->DoctorID."') MATCHING (DOCTORID)";
                $query = ibase_prepare($trans, $QueryText);
                $result = ibase_execute($query);
            }
        }
		
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
	}
    
   	/**
	* @param string $doctorslogin
	* @param string $doctorspassw
	* @return string[]
	*/
	public function loginDoctorFunction($doctorslogin, $doctorspassw)
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = "select USERS.DOCTORID, USERS.ACCESSALLOW, DOCTORS.SHORTNAME,
                        doctors.lastname, doctors.firstname, doctors.middlename, doctors.speciality
                        from USERS
                        left join DOCTORS
                           on  USERS.DOCTORID = DOCTORS.ID
                        where USERS.NAME = '".safequery($doctorslogin)."' and USERS.PASSWRD = '".safequery($doctorspassw)."'";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $logininfo=ibase_fetch_row($result);
		ibase_free_query($query);
        ibase_free_result($result);
        ibase_commit($trans);
		$connection->CloseDBConnection();
        $out = Array();
        $out[0] = "x100";
            if($logininfo[0] == null)
            {
                $out[0] = "x101";
                return $out;   
            }
            if($logininfo[1] == 0)
            {
                $out[0] = "x102";
                return $out;  
            }
            if(($logininfo[0] != null)&&($logininfo[1] != 0))
            {
                $out[0] = $logininfo[0];
                $out[1] = $logininfo[2];
                $out[2] = $logininfo[3];
                $out[3] = $logininfo[4];
                $out[4] = $logininfo[5];
                $out[5] = $logininfo[6];
                return $out;
            }
		return $out;
	}
    
    /**
     *                 
     * @param resource $trans
     * @return boolean
     */
        public static function syncAndClearJornal($trans=null) 
        {
            $rs=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
            $res=AdminService::getJournal(null,null,null,null,null,null,$trans);			
    		AdminService::insertJournalRecords($res);
            
            $QueryText = "delete from journal";
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		ibase_free_query($query);                       
                   
            if($local)
            {
                $rs=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $rs;
     
    	}
     
     /**              
     * @param resource $trans
     * @return boolean
     */
        public static function exitAutorization($trans=null) 
        {
            
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection("main",false);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
            
           
            $ip=tfClientId;
            $QueryText="update or insert into CLIENTACCOUNT (CLIENT_ADDRESS, ACCOUNTID) values('$ip', null) matching(CLIENT_ADDRESS)";
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".date("Y-m-d H:i:s")." ".$QueryText);
            $query = ibase_prepare($trans, $QueryText);
		    ibase_execute($query);
            ibase_free_query($query); 
              
            $rs=true;           
            if($local)
            {
                $rs=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $rs;
     
    	}  
            /**
     * @param string $user
     * @param string $password                
     * @param resource $trans
     * @return AdminModuleAutorizeObject
     */
        public static function checkAutorization($user, $password, $trans=null) 
        {
            $rs=new AdminModuleAutorizeObject;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection("main",false);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
              $tfClientId=tfClientId;
            $QueryText = "select first 1 
                                a.id,
                                a.ACCOUNTKEY, 
                                a.lastname, 
                                a.firstname, 
                                a.middlename, 
                                d.id, 
                                d.shortname, 
                                d.speciality,
                                a.subdivisionid,
                                s.name,
                                a.cashiercode,
                                a.info2,
                                ca.client_address,
                                ca.lastactivity,
                                s.CITY,
                                a.EH_ACCESS_TOKEN,
                                a.EH_ACCESS_TOKEN_EXPIRY,
                                a.EH_SECRET_KEY,
                                a.OA_DIR
                                from account a 
                                left join doctors d on d.accountid=a.id
                                left join clientaccount ca on ca.accountid=a.id
                                left join subdivision s on s.id=a.subdivisionid
                             where a.access=1 
                             and (d.isfired is null or d.isfired != 1)
                             and a.ACCOUNTUSER=".nullcheck($user,true)." 
                             and a.ACCOUNTPASSWORD=".nullcheck($password,true); 
                             //"and (select count(*) from clientaccount ca2 left join account a2 on ca2.accountid=a2.id where a2.licensename=a.licensename and a2.id<>a.id)<a.lc and (ca.client_address is null or ca.client_address='$tfClientId' or (a.ACCOUNTUSER='DEMO_VIKISOFT_LOGIN' and a.ACCOUNTPASSWORD='DEMO_VIKISOFT_PASSWORD'))";
           	
            $query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);

            $rs->ACCOUNT_CHECK = 0;
    		if ($row = ibase_fetch_row($result))
    		{				
                
                $rs->ACCOUNT_ID=$row[0];
                $rs->ACCOUNT_KEY=$row[1];
                $rs->ACCOUNT_LASTNAME=$row[2];
                $rs->ACCOUNT_FIRSTNAME=$row[3];
                $rs->ACCOUNT_MIDDLENAME=$row[4];
                $rs->DOCTOR_ID=$row[5];
                $rs->DOCTOR_SHORTNAME=$row[6];
                $rs->DOCTOR_SPECIALITY=$row[7];
                $rs->SUBDIVISIONID=$row[8];  
                $rs->SUBDIVISIONNAME=$row[9]; 
                $rs->ACCOUNT_CASHIERCODE=$row[10]; 
                $rs->INFO2=$row[11]; 
                if($row[13] != '' || $row[13] != null)
                    $a_date = strtotime($row[13]);
                else
                    $a_date = strtotime('now');
                $c_date = strtotime('now');
                $d_date = $c_date - $a_date;
                if($row[12] == null || $row[11] == '' || $row[12] == $tfClientId || $d_date > 86400)
                { 
                    $rs->ACCOUNT_CHECK = 1;
                }
                $rs->SUBDIVISIONCITY=$row[14]; 
                $rs->EH_ACCESS_TOKEN = $row[15];
                $rs->EH_ACCESS_TOKEN_EXPIRY=$row[16];
                $rs->EH_SECRET_KEY=$row[17];
                $rs->OA_DIR=$row[18];
                
                if($rs->OA_DIR == null || $rs->OA_DIR == '')
                {
                    $rs->OA_DIR = uniqid();
                    
                    mkdir("services/oa/".$rs->OA_DIR);
                    copy("services/oa/init.php","services/oa/".$rs->OA_DIR."/index.php");
                    
                    $oa_dir_sql = "update account set OA_DIR = '$rs->OA_DIR' where ID = ".$rs->ACCOUNT_ID;
                    $oa_dir_query = ibase_prepare($trans, $oa_dir_sql);
        		    ibase_execute($oa_dir_query);
      		        ibase_free_query($oa_dir_query);
                }
                else
                {
                    if(!file_exists ("services/oa/".$rs->OA_DIR))
                    {
                        //directory not exist
                        mkdir("services/oa/".$rs->OA_DIR);
                        copy("services/oa/init.php","services/oa/".$rs->OA_DIR."/index.php");
                    }
                    else
                    {
                        if(!file_exists ("services/oa/".$rs->OA_DIR."/index.php"))
                        {
                            //file not exist
                            copy("services/oa/init.php","services/oa/".$rs->OA_DIR."/index.php");
                        }
                    }
                }
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result); 
            
            if($rs->ACCOUNT_CHECK == 1)
            {  
                if(nonull($rs->ACCOUNT_KEY)) 
                {
                    $QueryText="update or insert into CLIENTACCOUNT (CLIENT_ADDRESS, ACCOUNTID, LASTACTIVITY) values('$tfClientId', ".$rs->ACCOUNT_ID.", 'now') matching(CLIENT_ADDRESS)";
                   // file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
                    $query = ibase_prepare($trans, $QueryText);
        		    ibase_execute($query);
      		        ibase_free_query($query);
                } 
            }
                                    
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $rs;
     
    	}
        
    	/**
	* @param string $accountid
    * @param string $startDateTime
    * @param string $endDateTime
    * @param string $tablename
    * @param string $fieldname
    * @param string $fieldvalue
    * @param resource $trans
    * @param string $type
	* @return AdminModuleJournalRecord[]
	*/
        public static function getJournal($accountid, $startDateTime, $endDateTime, $tablename, $fieldname, $fieldvalue, $trans=null,$type="log") 
        {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection($type);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
        	$QueryText = 'select j.id,
                                     j.accountid,
                                     j.tablename,
                                     j.fieldname,
                                     j.recordid,
                                     j.oldvalue,
                                     j.newvalue,
                                     j.datetime
                                      from journal j';
    		if($type=="log")
            {
 
                    $where=array();       	
                    
                    if(nonull($startDateTime))
                    {
                        $where[]="j.datetime >= '$startDateTime'";  
                    }
                    if(nonull($endDateTime))
                    {
                        $where[]="j.datetime <= '$endDateTime'";  
                    }
                    
                    if(nonull($tablename))
                    {
                        $where[]="lower(j.tablename) = lower('$tablename')";  
                    }
                    
                    if(nonull($fieldname))
                    {
                        $where[]="lower(j.fieldname) = lower('$fieldname')";  
                    } 
                    
                    if(nonull($accountid))
                    {
                        $where[]="j.accountid = $accountid";  
                    }
                    else
                    {
                        $where[]="j.accountid is not null";  
                    }
                    
                    if(nonull($fieldvalue))
                    {
                        $where[]="(lower(j.oldvalue) like lower('%$fieldvalue%') or lower(j.newvalue) like lower('%$fieldvalue%'))";  
                    }
                    if(count($where)>0)
                    {
                        $QueryText.=' where '.implode(' and ',$where).' order by j.datetime desc';
                    }
            }
            else
            {
               $QueryText.=' where j.accountid is not null 
               order by j.datetime desc';
            }
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		$res=array();
    		while ($row = ibase_fetch_row($result))
    		{				
                $record=new AdminModuleJournalRecord;
                $record->ID=$row[0];
                $record->ACCOUNTID=$row[1];
                $record->TABLENAME=$row[2];
                $record->FIELDNAME=$row[3];
                $record->RECORDID=$row[4];
                $record->OLDVALUE=$row[5];
                $record->NEWVALUE=$row[6];
                $record->DATETIME=$row[7];
                $res[]=$record;
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
        }
        
     	/**
    * @param string $tablename
    * @param string $fieldid
    * @param resource $trans
	* @return string
	*/
        public static function getRecordString($tablename, $fieldid, $trans=null) 
        {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
        	$QueryText = "select * from $tablename where id=$fieldid";
            
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		$res='';
    		if ($obj = ibase_fetch_object($result))
    		{				
    		  foreach($obj as $key=>$value)
              {
                $res.=$key.' = '.nullcheck($value)."\n";
              }
                
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
        }
        
    /**
    * @param resource $trans
    * @param string $type
	* @return AdminModuleJournalFilter[]
	*/
        public static function getJournalFilters($trans=null,$type="log") 
        {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection($type);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
        	$QueryText = "select    j.tablename, cast(list(distinct j.fieldname, ' ') as varchar(1000))
                                      from journal j  group by j.tablename";
            
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		$res=array();
    		while ($row = ibase_fetch_row($result))
    		{				
                $filter=new AdminModuleJournalFilter;
                $filter->TABLENAME=$row[0];
                $filter->FIELDNAMES=explode(' ', $row[1]);
                $res[]=$filter;
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
        }
        
            /**
     * 
     * @param AdminModuleJournalRecord[] $records                 
     * @param resource $trans
     * @param string $type
     * @return boolean
     */
     
    public static function insertJournalRecords($records, $trans=null, $type="log")
	{
	   $res=true;
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection($type);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

			foreach($records as $record)
            {
                $QueryText = "insert into journal (id,
                                         accountid,
                                         tablename,
                                         fieldname,
                                         recordid,
                                         oldvalue,
                                         newvalue,
                                         datetime) values(null, "
                                                                    .nullcheck($record->ACCOUNTID).", 
                                                                    '".safequery($record->TABLENAME)."',
                                                                    '".safequery($record->FIELDNAME)."', "
                                                                    .nullcheck($record->RECORDID,true).", "
                                                                    .nullcheck($record->OLDVALUE,true).", "
                                                                    .nullcheck($record->NEWVALUE,true).", "
                                                                    .nullcheck($record->DATETIME,true).") ";
                
                $query = ibase_prepare($trans, $QueryText);
    			ibase_execute($query);
                ibase_free_query($query);
            }                      
            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	}
    
     /**
      *@param int[] $spec
     * @param resource $trans  
     * @return AdminModuleAutorizeObject[]
     */
     public static function getClients($spec,$trans=null)//sqlite
	{
       $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection("main",false);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
            
            $QueryText = 'select a.id, 
                                a.accountuser, 
                                a.accountpassword, 
                                a.lastname, 
                                a.firstname, 
                                a.middlename, 
                                coalesce(d.id,\'1_\'||ass.id), 
                                coalesce(d.shortname,ass.shortname), 
                                d.speciality,
                                a.access,
                                a.ACCOUNTKEY,
                                a.spec,
                                a.subdivisionid,
                                s.name,
                                a.cashiercode,
                                
                                a.ACCOUNTTYPE,
                                a.TAX_ID,
                                a.BIRTH_DATE,
                                a.BIRTH_PLACE,
                                a.GENDER,
                                a.EMAIL,
                                a.USER_POSITION,
                                a.PHONE_TYPE,
                                a.PHONE,
                                a.DOC_TYPE,
                                a.DOC_NUMBER,
                                a.IS_OWNER
                                from account a left join doctors d on d.accountid=a.id
                                left join assistants ass on ass.accountid=a.subdivisionid 
                                left join subdivision s on s.id=a.subdivisionid';
           	if($spec!=null&&count($spec)>0)
            {
                $QueryText.=' where a.spec in ('.implode(',',$spec).')';
            }
            $QueryText.=' order by  a.accountuser';
            $query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);

    		while ($row = ibase_fetch_row($result))
    		{				
                
                $rs=new AdminModuleAutorizeObject;
                $rs->ACCOUNT_ID=$row[0];
                $rs->ACCOUNT_USER=$row[1];
                $rs->ACCOUNT_PASSWORD=$row[2];
                $rs->ACCOUNT_LASTNAME=$row[3];
                $rs->ACCOUNT_FIRSTNAME=$row[4];
                $rs->ACCOUNT_MIDDLENAME=$row[5];
                $rs->ACCOUNT_SHORTNAME=shortname($row[3],$row[4],$row[5]);
                $rs->DOCTOR_ID=$row[6];
                $rs->DOCTOR_SHORTNAME=$row[7];
                $rs->DOCTOR_SPECIALITY=$row[8];
                $rs->ACCOUNT_ACCESS=$row[9]==1;
                $rs->ACCOUNT_KEY=$row[10];
                $rs->SPEC=$row[11]; 
                $rs->SUBDIVISIONID=$row[12];  
                $rs->SUBDIVISIONNAME=$row[13];       
                $rs->ACCOUNT_CASHIERCODE=$row[14];  
                $rs->ACCOUNTTYPE = $row[15];
                $rs->TAX_ID = $row[16];
                $rs->BIRTH_DATE = $row[17];
                $rs->BIRTH_PLACE = $row[18];
                $rs->GENDER = $row[19];
                $rs->EMAIL = $row[20];
                $rs->USER_POSITION = $row[21];  
                $rs->PHONE_TYPE = $row[22];
                $rs->PHONE = $row[23];
                $rs->DOC_TYPE = $row[24];
                $rs->DOC_NUMBER = $row[25];
                $rs->IS_OWNER = $row[26]==1;
                $res[]=$rs;
                
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);   
                         
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteSubdivision($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Subdivision where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    } 
    
     /**
    *  @param AdminModuleAutorizeObject $account  
    * @param AdminModuleClientSetting[] $settings  
    * @param boolean $saveHr   
     * @param resource $trans  
     * @return boolean
     */
     public static function saveClientAndSettings($account, $settings, $saveHr, $trans=null)
	{
            return AdminService::saveClient($account, $saveHr)&&AdminService::setAccountClientSettings($settings,$account->ACCOUNT_ID);
	} 
    
        /**
    *  @param AdminModuleAutorizeObject $account 
    *  @param boolean $saveHr   
     * @param resource $trans  
     * @return boolean
     */
     public static function saveClient($account, $saveHr, $trans=null)
	{
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection("main",false);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
            
            
                $values=array();
                $values[]=nullcheck($account->ACCOUNT_ID);
                $values[]="'".safequery($account->ACCOUNT_USER)."'";
                $values[]="'".safequery($account->ACCOUNT_PASSWORD)."'";
                $values[]="'".safequery($account->ACCOUNT_FIRSTNAME)."'";
                $values[]="'".safequery($account->ACCOUNT_MIDDLENAME)."'";
                $values[]="'".safequery($account->ACCOUNT_LASTNAME)."'";
                $values[]=nullcheck($account->SPEC);                
                $values[]=$account->ACCOUNT_ACCESS?"1":"0";
                $values[]=nullcheck($account->SUBDIVISIONID);
                /*if($account->ACCOUNT_CASHIERCODE == null || $account->ACCOUNT_CASHIERCODE == "")
                {
                    $values[]="null";
                }
                else
                {
                    $values[]=$account->ACCOUNT_CASHIERCODE;
                }*/
                
                $values[]=nullcheck($account->ACCOUNT_CASHIERCODE);
                $values[]="'".safequery($account->ACCOUNTTYPE)."'";
                $values[]="'".safequery($account->TAX_ID)."'";
                $values[]=nullcheck($account->BIRTH_DATE, true);
                $values[]="'".safequery($account->BIRTH_PLACE)."'";
                $values[]=nullcheck($account->GENDER);
                $values[]="'".safequery($account->EMAIL)."'";
                $values[]="'".safequery($account->USER_POSITION)."'";
                
                $values[]="'".safequery($account->PHONE_TYPE)."'";
                $values[]="'".safequery($account->PHONE)."'";
                $values[]="'".safequery($account->DOC_TYPE)."'";
                $values[]="'".safequery($account->DOC_NUMBER)."'";       
                $values[]=$account->IS_OWNER?"1":"0";
                
                    
                //$values[]=$account->ACCOUNT_CASHIERCODE != '' ? $account->ACCOUNT_CASHIERCODE:"";// nullcheck($account->ACCOUNT_CASHIERCODE);          

                $QueryText="update or insert into account (
                                                                        id,
                                                                        accountuser,
                                                                        accountpassword,
                                                                        firstname,
                                                                        middlename,
                                                                        lastname,
                                                                        spec,                                                                        
                                                                        access,
                                                                        subdivisionid,
                                                                        cashiercode,
                                                                        
                                                                        ACCOUNTTYPE,
                                                                        TAX_ID,
                                                                        BIRTH_DATE,
                                                                        BIRTH_PLACE,
                                                                        GENDER,
                                                                        EMAIL,
                                                                        USER_POSITION,
                                                                        PHONE_TYPE,
                                                                        PHONE,
                                                                        DOC_TYPE,
                                                                        DOC_NUMBER,
                                                                        IS_OWNER)
                                                                        
                                                                    values (".implode(",", $values).")";
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);	
           	
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);			
    		//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($account,true));	
    		ibase_free_query($query);
             if($saveHr)
             {
                if(strstr($account->DOCTOR_ID,'1_'))
                {
                    $QueryText="update assistants set accountid=".$account->ACCOUNT_ID." where id=".substr($account->DOCTOR_ID,2);
                }
                else
                {
                    $QueryText="update doctors set accountid=".$account->ACCOUNT_ID." where id=".$account->DOCTOR_ID;
                }
                
               	
                $query = ibase_prepare($trans, $QueryText);	
        		ibase_execute($query);
        		ibase_free_query($query);
             }           
            $res=true;             
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
     /**
     * @param resource $trans  
     * @return AdminModuleSubdivision[]
     */
     public static function getSubdivisions($trans=null)//sqlite
	{
       $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection("main",false);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
            
            $QueryText = 'select s.id, 
                                s.name,
                                s.ordernumber_cash,
                                s.ordernumber_clearing,
                                s.invoicenumber,
                                s.city,
                                s.address
                                from subdivision s';
            $query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);

    		while ($row = ibase_fetch_row($result))
    		{				
                
                $rs=new AdminModuleSubdivision;
                $rs->ID=$row[0];
                $rs->NAME=$row[1];  
                $rs->ORDERNUMBER_CASH=$row[2];
                $rs->ORDERNUMBER_CLEARING=$row[3];
                $rs->INVOICENUMBER=$row[4];      
                $rs->CITY=$row[5];  
                $rs->ADDRESS=$row[6];
                $res[]=$rs;
                
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);   
                         
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
    
        /**
    *  @param AdminModuleSubdivision $subdivision 
     * @param resource $trans  
     * @return boolean
     */
     public static function saveSubdivision($subdivision, $trans=null)
	{
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection("main",false);
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
            
                $position = null;
                if($subdivision->ADDRESS != null && $subdivision->CITY != null && $subdivision->ADDRESS != '' && $subdivision->CITY != '')
                {
                    $position = getLocation($subdivision->ADDRESS, $subdivision->CITY);
                }
                $values=array();
                if(nonull($subdivision->ID))
                {
                    $values[]=$subdivision->ID; 
                }
                else
                {
                    $values[]='null';
                }

                $values[]="'".safequery($subdivision->NAME)."'";  
                $values[]="'".safequery($subdivision->CITY)."'";  
                $values[]="'".safequery($subdivision->ADDRESS)."'"; 
                
                if($position != null)
                {
                    $values[]=$position[0]; 
                    $values[]=$position[1]; 
                }
                else
                {
                    $values[]="null"; 
                    $values[]="null"; 
                }        

                $QueryText="update or insert into subdivision (id, name, city, address, LONGTITUDE, LATITUDE)
                                                                    values (".implode(",", $values).")";
    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);		
           	
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
	
    		ibase_free_query($query);
 
            $res=true;             
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
    /**
     * @param resource $trans  
     * @return AdminModuleClientSetting[]
     */
     public static function getClientSettings($trans=null)
	{
        return AdminService::getAccountClientSettings('(select id from current_account)',false);
	} 
    
    /**
     * @param string $accountId 
     * @param boolean $adminOnly   
     * @param resource $trans  
     * @return AdminModuleClientSetting[]
     */
     public static function getAccountClientSettings($accountId, $adminOnly=true, $trans=null)
	{
        $res=''; 
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

			$QueryText = " select
             settingname,
             settingvalue from clientsettings where coalesce(accountid,'null')=coalesce(".nullcheck($accountId).",'null')";
             if($adminOnly)
             {
                $QueryText.=" and isadmin=1";
             }
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		      $res=array();
			while($row = ibase_fetch_row($result))
			{
			     $setting=new AdminModuleClientSetting;
                 $setting->SETTINGNAME=$row[0];
                 $setting->SETTINGVALUE=$row[1];
                 $res[]=$setting;
			}
            
            ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
        /**
     * @param AdminModuleClientSetting[] $settings  
     * @param resource $trans 
     * @return boolean
     */
     public static function setClientSettings($settings, $trans=null)
	{
	   return AdminService::setAccountClientSettings($settings, '(select id from current_account)',false);
	}
        /**
     * @param AdminModuleClientSetting[] $settings 
     * @param string $accountId 
     * @param boolean $adminOnly   
     * @param resource $trans 
     * @return boolean
     */
     public static function setAccountClientSettings($settings, $accountId, $adminOnly=true, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
     
            foreach($settings as $setting)
            {
                $values=array();
                $setting->SETTINGVALUE=str_replace("\n","",$setting->SETTINGVALUE);
                $values[]=nullcheck($accountId);
                $values[]="'".safequery($setting->SETTINGNAME)."'";
                $values[]="'".safequery($setting->SETTINGVALUE)."'";
                $values[]=$adminOnly?'1':'0';
                $QueryText="update or  insert into clientsettings (
                                                    accountid,
                                                    settingname,
                                                    settingvalue,
                                                    isadmin)
                                                  values (".implode(",", $values).") matching (accountid, settingname)";
                 $query = ibase_prepare($trans, $QueryText);
		      

                ibase_execute($query);          
                ibase_free_query($query);
            }
            
            
            $res=true;
            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	}
    
    /**
     * @param resource $trans  
     * @return AdminModuleClientColorSetting[]
     */
     public static function getClientColorSettings($trans=null)
	{
        $res=''; 
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

			$QueryText = " select
             settingname,
             settingvalue from clientcolorsettings where coalesce(accountid,'null')=(select coalesce(id,'null') from current_account)";
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		      $res=array();
			while($row = ibase_fetch_row($result))
			{
			     $setting=new AdminModuleClientColorSetting;
                 $setting->SETTINGNAME=$row[0];
                 $setting->SETTINGVALUE=$row[1];
                 $res[]=$setting;
			}
            
            ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
    
        /**
     * @param AdminModuleClientColorSetting[] $settings  
     * @param resource $trans 
     * @return boolean
     */
     public static function setClientColorSettings($settings, $trans=null)
	{
        $res='';
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
     
            foreach($settings as $setting)
            {
                $values=array();
                $setting->SETTINGVALUE=str_replace("\n","",$setting->SETTINGVALUE);
                $values[]='(select id from current_account)';
                $values[]="'".safequery($setting->SETTINGNAME)."'";
                $values[]=nullcheck($setting->SETTINGVALUE);
                $QueryText="update or  insert into clientcolorsettings (
                                                    accountid,
                                                    settingname,
                                                    settingvalue)
                                                  values (".implode(",", $values).") matching (accountid, settingname)";
                 $query = ibase_prepare($trans, $QueryText);
		      

                ibase_execute($query);          
                ibase_free_query($query);
            }
            
            
            $res=true;
            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	}
    
    // ********************************************* ASSISTANTS START *******************************************
    /**
    * @return AdminModuleAssistant[]
	*/    
    public function getAssistantsList() //sqlite
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText = "select ID, LASTNAME || ' ' || FIRSTNAME FROM ASSISTANTS  WHERE 
                             isfired is null or isfired != 1";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AdminModuleAssistant;
            $obj->assid = $row[0];
            $obj->name = $row[1];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    // ********************************************* ASSISTANTS END *******************************************
       /**
     * @param string $paramName  
     * @param string $paramValue  
     * @return boolean
     */
    public static function setApplicationSetting($paramName, $paramValue)
    {
        return setApplicationSetting($paramName, $paramValue);
    } 
    
    /**
     * @param AdminModuleClientSetting[] $settings 
     * @return boolean
     */
    public static function setApplicationSettings($settings)
    {
        $result = false;
        foreach($settings as $setting)
        {
            $values=array();
            $setting->SETTINGVALUE=str_replace("\n","",$setting->SETTINGVALUE);
            if(!setApplicationSetting(safequery($setting->SETTINGNAME), safequery($setting->SETTINGVALUE)))
            {
            $result = false;
            }
        }
        return $result;
    }
    
        /**
     * @param string[] $paramNames  
     * @return string[]
     */
    public static function getApplicationSettings($paramNames)
	{
	   $res=array();
       foreach($paramNames as $paramName)
       {
            $res[]=getApplicationSetting($paramName);
       }
	   
	   return $res;
	} 
    
    
    // ********** AccountFunctions ************
    	/**
	* @param string $subdivId
	* @param string $passForDelete
	* @return boolean
 	*/ 		 		
	public function resetSubdivInvoicesSeq($subdivId, $passForDelete) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               $QueryText2 = "update SUBDIVISION set ORDERNUMBER_CASH = 0, ORDERNUMBER_CLEARING = 0, ORDERNUMBER_MIN = 0, INVOICENUMBER = 0 where SUBDIVISION.ID = $subdivId";
        	   $query2 = ibase_prepare($trans, $QueryText2);
        	   ibase_execute($query2);
        	   ibase_free_query($query2);
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $result;
	}
    
    	/**
	* @param string $subdivId
	* @param int $ordernumberCash
	* @param int $ordernumberClearing
	* @param int $invoiceNumber
	* @param string $passForDelete
	* @return boolean
 	*/ 		 		
	public function setSubdivInvoicesSeq($subdivId, $ordernumberCash, $ordernumberClearing, $invoiceNumber, $passForDelete) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               $QueryText2 = "update SUBDIVISION set ORDERNUMBER_CASH = $ordernumberCash, ORDERNUMBER_CLEARING = $ordernumberClearing, INVOICENUMBER = $invoiceNumber 
                                where SUBDIVISION.ID = $subdivId";
        	   $query2 = ibase_prepare($trans, $QueryText2);
        	   ibase_execute($query2);
               
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $result;
	}
    
    
    
   	/**
	* @return AdminModuleCabinet[]
 	*/ 		 		
	public function getCabinetsList() 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = "select ID, NUMBER, NAME, ROOMCOLOR
                            from ROOMS order by SEQUINCENUMBER";
		
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AdminModuleCabinet;
            $obj->id = $row[0];
            $obj->name = $row[1];
            $obj->color = $row[2];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
	}
    
    public function getF43($trans=null)//sqlite
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
            
            	$QueryText = "select 
                                    F43,
                                    NAME
                    			from F43 
                                    order by F43";
    			
    			$query = ibase_prepare($trans, $QueryText);
    			$result = ibase_execute($query);
    			$f43_array=array();
    			while($row = ibase_fetch_row($result))
    			{		 
                    $f43 = new stdClass();
                    $f43->id = $row[0]; 
                    $f43->label = $row[1]; 
                    
                    $f43_array[]=$f43;
    			}
    			ibase_free_query($query);
    			ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $f43_array;
    }
    
    
    /**
	* @param string $pass
	* @param string $passType
	* @return boolean
	*/
	public function validatePassword($pass, $passType)
	{
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = '$passType'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $pass)
			{
               ibase_free_result($result);
        	   ibase_free_query($query);
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
	}
    
    
    // ************************ PHONES START ******************************
   	/** 
 	* @param string $account_id
 	* @return AdminModuleAccountPhone[]
	*/ 
     public function getPhones($account_id)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select 
                            ID, 
                            ACCOUNT_FK, 
                            PHONE, 
                            PHONE_TYPE
                        from ACCOUNTSMOBILE
                    where ACCOUNT_FK = $account_id";		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new AdminModuleAccountPhone;
			$obj->id = $row[0];
			$obj->account_fk = $row[1];
			$obj->phone = $row[2];
			$obj->phone_type = $row[3];
		
			$rows[] = $obj;
		}				
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
    
   	/** 
 	* @param AdminModuleAccountPhone $address
 	* @return AdminModuleAccountPhone
	*/ 
     public function savePhones($phone)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
        $save_sql = "update or insert into ACCOUNTSMOBILE (
                                                    ID, 
                                                    ACCOUNT_FK, 
                                                    PHONE, 
                                                    PHONE_TYPE  )
                                              values (".nullcheck($phone->id).", 
                                                      ".$phone->account_fk.", 
                                                      '".safequery($phone->phone)."', 
                                                      '".safequery($phone->phone_type)."')
                                         matching (ID) returning (ID)";
               		
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $phone->ID = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        return $phone;
    }
    
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deletePhoneByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from ACCOUNTSMOBILE where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
// ************************ PHONES END ******************************



 // ************************ EDUCATIONS START ******************************
   	/** 
 	* @param string $account_id
 	* @return AdminModuleAccountEducation[]
	*/ 
     public function getEducations($account_id)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select 
                        ID, 
                        COUNTRY, 
                        CITY, 
                        INSTITUTION_NAME, 
                        ISSUED_DATE, 
                        DIPLOMA_NUMBER, 
                        DEGREE, 
                        SPECIALITY
                        
                  from EH_EDUCATION
                  
                  where FK_ACCOUNT = $account_id";		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new AdminModuleAccountEducation;
			$obj->id = $row[0];
			$obj->country = $row[1];
			$obj->city = $row[2];
			$obj->institution_name = $row[3];
			$obj->issued_date = $row[4];
			$obj->diploma_number = $row[5];
			$obj->degree = $row[6];
			$obj->speciality = $row[7];
		
			$rows[] = $obj;
		}				
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
    
   	/** 
 	* @param AdminModuleAccountEducation $address
 	* @return AdminModuleAccountEducation
	*/ 
     public function saveEducation($education)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
        $save_sql = "update or insert into EH_EDUCATION (
                                                            ID, 
                                                            COUNTRY, 
                                                            CITY, 
                                                            INSTITUTION_NAME, 
                                                            ISSUED_DATE, 
                                                            DIPLOMA_NUMBER, 
                                                            DEGREE,
                                                            SPECIALITY, 
                                                            FK_ACCOUNT)
                                                values (
                                                ".nullcheck($education->id).", 
                                                '".$education->country."', 
                                                '".safequery($education->city)."', 
                                                '".safequery($education->institution_name)."', 
                                                '".safequery($education->issued_date)."', 
                                                '".safequery($education->diploma_number)."', 
                                                '".$education->degree."', 
                                                '".safequery($education->speciality)."',
                                                ".$education->account_fk.", )
                                                            matching (ID)  returning (ID)";
               		
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $education->ID = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        return $education;
    }
    
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deleteEducationByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from EH_EDUCATION where ID = $id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
// ************************ EDUCATIONS END ******************************

}

?>
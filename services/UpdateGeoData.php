<?php
require_once "Connection.php";

class getData
{
//функция получения долготы и широты по адрессу и городу 



function getPosition($address,$city)
{	
	$lat=0;
	$long=0;
	$params = array
					(
					'geocode' => "г. ".$city.", ".$address, // адрес
					'format'  => 'json',               // формат ответа
					'results' => 1,   				   // количество выводимых результатов
					);
	$response = json_decode(
					file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
					
	if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
	{
		
		$pos=$response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;	
		// в некоторых случаях невалидных адрессов, 
		// возвращается долгота и широта г. Киев, площадь независимости "30.523541 50.450418".
		if ($pos!="30.523541 50.450418")
		{
			//получаем долготу и широту	
			list($long, $lat) = explode(" ", $pos);
		}				
	}
	$position[]=$long;
	$position[]=$lat;
	return $position;
}
}



class addData
{

//Функция на проставление долготы и широты всем пациентам
public function updatePatients($patientCount)
    { 
		//подключение на получение адресов всех пациентов
		$_con = new Connection();
		$_con->EstablishDBConnection("main",false);
		$trans = ibase_trans(IBASE_DEFAULT, $_con->connection);
	$QueryText = "select first {$patientCount} t1.ADDRESS, t1.ID, t3.CITY,
							(select t2.CITY from SUBDIVISION t2 WHERE t2.ID = t1.SUBDIVISIONID)
							from  CLINICREGISTRATIONDATA t3, PATIENTS t1
									
							WHERE (t1.ID is not null) and (t1.ADDRESS!='')
							and (t1.ADDRESS is not null) and (  (t1.LONGTITUDE is null)
																     or (t1.LATITUDE is null) ) 
							order by t1.ID";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);		
		
		//подключение на обновление полей LATITUDE i LONGTITUDE
		$_con1 = new Connection();
		$_con1->EstablishDBConnection("main",false);
		
		//обработка всех пациентов
		$data=new getData();

		while ($row = ibase_fetch_row($result))
		{			
			$trns = ibase_trans(IBASE_DEFAULT, $_con1->connection);
			//проверка на PATIENTS.CITY!=null and PATIENTS.CITY!=""	
			if (($row[3]!=null) and ($row[3]!=""))
			{	
				$position=$data->getPosition($row[0],$row[3]);
			}
			else
			{
				$position=$data->getPosition($row[0],$row[2]);				
			}
					//подключение на обновление записей LATITUDE i LONGTITUDE
					//Обновление записи LONGTITUDE i LATITUDE в таблице PATIENTS
					$QueryTxt = "update PATIENTS set LONGTITUDE={$position[0]}, LATITUDE={$position[1]}
									WHERE ID={$row[1]}";
					$quer = ibase_prepare($trns, $QueryTxt);
					$reslt = ibase_execute($quer);
					ibase_commit($trns);								

		}

		//закрытие подключения		
		ibase_free_query($quer);
		$_con1->CloseDBConnection();

		//закрытие подключения
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$_con->CloseDBConnection();		
	}	
}




?>
﻿<?php

require_once "Connection.php";
require_once "Utils.php";

define('backup_folder','C:/ToothFairyServer/Firebird/backup/');
define('backup_outdated_time',604800);

//BackupModule::doBackup();
class BackupModule
{
    
    public static function doBackup()
    {
        
        $doBackupNow=true;
        $backupTodayName=date("Y-m-d").".zip";
        if ($handle = opendir(backup_folder)) 
        {
            while($file = readdir($handle))
            {
                if ($file != '.' and $file != '..')
                {
                    if((time()-filectime(backup_folder.$file))>backup_outdated_time)
                    {
                        if(file_exists(backup_folder.$file))
                        {
                            @unlink(backup_folder.$file);
                        }
                    }
                    if($doBackupNow&&(basename($file)==$backupTodayName))
                    {
                        $doBackupNow=false;
                    }
                }
            }
        }
        else
        {
            mkdir(backup_folder,0777);
        }       

        if($doBackupNow)
        {
            $con=new Connection;
            $pathToDB=$con->databasename['main'];
            $dbName=basename($pathToDB);
            $zip = new ZipArchive();
            $zip->open(backup_folder.$backupTodayName, ZIPARCHIVE::CREATE);
            $zip->addFile($pathToDB,$dbName);
            $zip->close();
        }
    }
    
}

?>
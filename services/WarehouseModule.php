<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');


//select t.rdb$trigger_inactive from rdb$triggers t where t.rdb$trigger_name='TREATMENTPROCFARM_AIU'
//update rdb$triggers t set t.rdb$trigger_inactive=1 where t.rdb$trigger_name='TREATMENTPROCFARM_AIU'
//print_r(WarehouseModule::getOLAP('',null,'2013-09-01','2013-09-30',null));
class WarehouseModule
{
    
    
    /**
     * @param WarehouseModuleFarmDocument $p1
     * @param WarehouseModuleFarmFlow $p2
     * @param WarehouseModuleDoctor $p3
     * @param WarehouseModuleSupplier $p4
     * @param WarehouseModuleFarmPrice $p5
     * @param WarehouseModuleFarm $p6
     * @param WarehouseModuleFarmCard $p7
     * @param WarehouseModuleOLAPRecord $p8
     * @return void
     */
    public function registerTypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8){}
    
    
    /**
     * @param string $paramName  
     * @param string $paramValue  
     * @return boolean
     */
    public static function setApplicationSetting($paramName, $paramValue)
    {
        return setApplicationSetting($paramName, $paramValue);
    } 
    
        /**
     * @param string[] $paramNames  
     * @return string[]
     */
    public static function getApplicationSettings($paramNames)
	{
	   $res=array();
       foreach($paramNames as $paramName)
       {
            $res[]=getApplicationSetting($paramName);
       }
	   
	   return $res;
	} 
     
    /**
     * WarehouseModule::getFarmDocuments()
     * 
     * @param string $stype
     * @param string $rtype
     * @param string $start_date
     * @param string $end_date
     * @param string $doctor_id   
     * @param string $supplier_id                 
     * @param resource $trans
     * @return WarehouseModuleFarmDocument[]
     */
     
    public static function getFarmDocuments($stype, $rtype, $start_date, $end_date, $doctor_id, $supplier_id, $trans=null)
	{		
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            if(nonull($start_date))
            {
                $start_date.=' 00:00:00';
            }
            if(nonull($end_date))
            {
                $end_date.=' 23:59:59';
            }
            
			$QueryText = "
                select
                    fd.id
                from farmdocument fd 
                where fd.treatmentprocid is null"; 
                $where="";
                if(nonull($rtype))
                {
                  $where.=" AND fd.rtype = $rtype";  
                }
                if(nonull($stype))
                {
                  $where.=" AND fd.stype = $stype";  
                }
                if(nonull($start_date))
                {
                  $where.=" AND fd.doctimestamp >= '$start_date'";  
                }
                if(nonull($end_date))
                {
                  $where.=" AND fd.doctimestamp <= '$end_date'";  
                }
                if($doctor_id!="")
                {
                    $where.=" AND (fd.sdoctorid = $doctor_id or fd.rdoctorid = $doctor_id )"; 
                }
                else if(isset($doctor_id)==false)
                {
                    $where.=" AND (fd.sdoctorid is null or fd.rdoctorid is null)"; 
                }
                
                if($supplier_id!="")
                {
                    $where.=" AND fd.supplierid = $supplier_id";  
                }
                else if(isset($supplier_id)==false)
                {
                    $where.=" AND fd.supplierid is null AND fd.stype = 1";  
                }
    			$QueryText.=$where;
                $QueryText.=" order by doctimestamp";	
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $doc=new WarehouseModuleFarmDocument($row[0], false, false, $trans);
                $res[]=$doc;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
        /**
     * WarehouseModule::getFarmFlows()
     * 
     * @param string $docid              
     * @param resource $trans
     * @return WarehouseModuleFarmFlow[]
     */
     
    public static function getFarmFlows($docid, $trans=null)
	{
	   return WarehouseModuleFarmDocument::getFarmFlows($docid,false);
    }
	   
    /**
     * WarehouseModule::upsertFarmDocument()
     * 
     * @param WarehouseModuleFarmDocument $farmDocument                 
     * @param resource $trans
     * @return boolean
     */
     
    public static function upsertFarmDocument($farmDocument, $trans=null)
	{
	   $res=true;
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

			$QueryText = "update or insert into farmdocument (id, stype, rtype, treatmentprocid, supplierid, doctimestamp, docnumber, sdoctorid, rdoctorid, comment, roomid) values("
                                                                .nullcheck($farmDocument->ID).", "
                                                                .nullcheck($farmDocument->STYPE).", "
                                                                .nullcheck($farmDocument->RTYPE).", "
                                                                .nullcheck($farmDocument->TREATMENTPROCID).", ";
            if($farmDocument->SUPPLIER!=null)
            {
                $QueryText.=nullcheck($farmDocument->SUPPLIER->ID).", '";
            }
            else
            {
                $QueryText.="NULL, '";
            }
            $QueryText.=nullcheck($farmDocument->DOCTIMESTAMP)."', "
                        .nullcheck($farmDocument->DOCNUMBER).", ";
            if($farmDocument->SDOCTOR!=null)
            {
                $QueryText.=nullcheck($farmDocument->SDOCTOR->ID).", ";
            }
            else
            {
                $QueryText.="NULL, ";
            }
            if($farmDocument->RDOCTOR!=null)
            {
                $QueryText.=nullcheck($farmDocument->RDOCTOR->ID).", ";
            }
            else
            {
                $QueryText.="NULL, ";
            }
            
            $QueryText.="'".$farmDocument->COMMENT."', ";            

            if($farmDocument->ROOM!=null)
            {
                $QueryText.=nullcheck($farmDocument->ROOM->ID).")";
            }
            else
            {
                $QueryText.="NULL)";
            }   
                     
            $QueryText.=" returning ID";
            $query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if ($row = ibase_fetch_row($result))
            {
                $farmDocument->ID=$row[0];
            }
            ibase_free_query($query);
            ibase_free_result($result);
            foreach($farmDocument->FARMFLOWS as $flow)
            {
                if($flow->STATUS=="DELETE")
                {
                    deleteRecordByField($flow->ID,"FARMFLOW",$trans);
                }
                elseif($flow->STATUS=="NEW")
                {
                    $flow->FARMDOCUMENTID=$farmDocument->ID;
                    WarehouseModule::insertFarmFlow($flow,$trans);                                                                                        
                }                
                            
            }                        
            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	}
    
    
    /**
     * WarehouseModule::insertFarmPrice()
     * 
     * @param WarehouseModuleFarmPrice $farmPrice                 
     * @param resource $trans
     * @return WarehouseModuleFarmPrice
     */
     
    public static function upsertFarmPrice($farmPrice, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   

			$QueryText = "update or insert into farmprice (price,farmid) values("
                                                                .$farmPrice->PRICE.","
                                                                .$farmPrice->FARM->ID.")
                                                                matching (price,farmid)
                                                                returning ID";
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if ($row = ibase_fetch_row($result))
            {
                $farmPrice->ID=$row[0];
            }
			ibase_free_query($query);
            
            
            
            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $farmPrice;
	} 
    
     /**
     * WarehouseModule::insertSupplier()
     * 
     * @param WarehouseModuleSupplier $supplier                 
     * @param resource $trans
     * @return WarehouseModuleSupplier
     */
     
    public static function insertSupplier($supplier, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   

			$QueryText = "insert into supplier (id, name) values(NULL, '".format_for_query($supplier->NAME)."')
                                                                returning ID";
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if ($row = ibase_fetch_row($result))
            {
                $supplier->ID=$row[0];
            }
			ibase_free_query($query);
            
            
            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $supplier;
	}
	       
     
     /**
     * WarehouseModule::insertFarmFlow()
     * 
     * @param WarehouseModuleFarmFlow $farmFlow                 
     * @param resource $trans
     * @return WarehouseModuleFarmFlow
     */
     
    public static function insertFarmFlow($farmFlow, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   
                $farmFlow->FARMPRICE=WarehouseModule::upsertFarmPrice($farmFlow->FARMPRICE, $trans);
            
            
           
            
			$QueryText = "insert into farmflow (id, quantity, farmdocumentid, treatmentprocfarmid, farmpriceid) 
                                                            values(NULL,"
                                                                .nullcheck($farmFlow->QUANTITY).","
                                                                .nullcheck($farmFlow->FARMDOCUMENTID).","
                                                                ."NULL,"
                                                                .nullcheck($farmFlow->FARMPRICE->ID)."                                                                 
                                                                ) returning ID";
                                                                
 //file_put_contents("C:/tmp.txt", $QueryText);
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if ($row = ibase_fetch_row($result))
            {
                $farmFlow->ID=$row[0];
            }
			ibase_free_query($query);
            
            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            

            
            return $farmFlow;
	} 
    
    /**
     * WarehouseModule::triggerTPFarms
     * 
     * @param string[] $ids                
     * @param resource $trans
     * @return boolean
     */
     
    public static function triggerTPFarms($ids, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   

        $QueryText="update treatmentprocfarm tpf set tpf.id=tpf.id where tpf.id in (".implode(',',$ids).")";
        
       	$query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        
        if($local)
        { 
            $res=ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res;
	} 
    
    /**
     * WarehouseModule::deleteFarmDocument
     * 
     * @param string $id                
     * @param resource $trans
     * @return boolean
     */
     
    public static function deleteFarmDocument($id, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   
           
        $res=deleteRecordByField($id, "FARMDOCUMENT", $trans) ;
        
        if($local)
        { 
            $res=ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res;
	} 
      /**
     * WarehouseModule::insertFarm()
     * 
     * @param WarehouseModuleFarm $farm                 
     * @param resource $trans
     * @return WarehouseModuleFarm
     */
     
    public static function insertFarm($farm, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   

			$QueryText = "insert into farm (id,
                                            parentid,
                                            nodetype,
                                            name,
                                            nomenclaturearticlenumber) values(NULL, NULL, 1, '"
                                                                .format_for_query($farm->NAME)."', '"
                                                                .format_for_query($farm->ARTICUL)
                                                                ."') returning ID";
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if ($row = ibase_fetch_row($result))
            {
                $farm->ID=$row[0];
            }
			ibase_free_query($query);
            
            
            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $farm;
	}   
        /**
     * WarehouseModule::insertFarmUnit()
     * 
     * @param WarehouseModuleFarmUnit $unit                 
     * @param resource $trans
     * @return WarehouseModuleFarmUnit
     */
     
    public static function insertFarmUnit($unit, $trans=null)
	{
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   

			$QueryText = "insert into unitofmeasure (id, farmid, name, isbase, coefficient) values(NULL, "
                                                                .$unit->FARMID.", '"
                                                                .format_for_query($unit->NAME)."',"
                                                                .($unit->ISBASIC?1:0).", "
                                                                .nullcheck($unit->COEFFICIENT).")
                                                                returning ID";
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			if ($row = ibase_fetch_row($result))
            {
                $unit->ID=$row[0];
            }
			ibase_free_query($query);
            
            
            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $unit;
	}
       
    /**
     * @param string $nowDate
     * @param boolean $withbalance
     * @param resource $trans
     * @return WarehouseModuleDoctor[]
     */
    public static function getDoctors($nowDate, $withbalance, $trans=null)//sqlite
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   
            if($withbalance)
            {
                $doc = new WarehouseModuleDoctor(null, $withbalance, $trans);
                {
                   $res[]=$doc; 
                }  
            }
            
            $QueryText = "select
                    d.id
                from doctors d ";
            if(nonull($nowDate))
            {
                $QueryText.=" where (d.dateworkend is null) or (d.dateworkend >= '$nowDate')";
            }
            $QueryText.=" order by d.shortname";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $doc = new WarehouseModuleDoctor($row[0], $withbalance, $trans);
                $res[]=$doc;

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
    }
    /**
     * @param string $id
     * @param boolean $withbalance
     * @param resource $trans
     * @return WarehouseModuleDoctor
     */
    public static function getDoctor($id, $withbalance, $trans=null)
    {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }              
            if(nonull($id))
            {
                $doc = new WarehouseModuleDoctor($id, $withbalance, $trans);
            }
            else
            {
                $doc = new WarehouseModuleDoctor(null, $withbalance, $trans);
            }
               

			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $doc;
    }
     /**
      * @param resource $trans
     * @return WarehouseModuleSupplier[]
     */
    public static function getSuppliers($trans=null)//sqlite
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   

            $QueryText = "select
                    s.id
                from supplier s order by LOWER(s.name)";
  
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $sup = new WarehouseModuleSupplier($row[0],$trans);
                $res[]=$sup;

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
    }
    /**
      * @param resource $trans
     * @return WarehouseModuleRoom[]
     */
    public static function getRooms($trans=null)//sqlite
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   

            $QueryText = "select
                    r.id
                from rooms r order by LOWER(r.name)";
  
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $room = new WarehouseModuleRoom($row[0],$trans);
                $res[]=$room;

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
    }
    /**
     * @param string $doctor_id
      * @param int $from_type
      * @param resource $trans
     * @return WarehouseModuleFarmFlow[]
     * $from_type 0-from current, 1-from common, -1-all
     */
    public static function getBalanceFlows($doctor_id,$from_type,$trans=null)
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            if($from_type==-1)
            {
                $QueryText = "select fcf.farmflowid, fcf.farmid from farmcardflow fcf
                left join farmflowcard ffc on ffc.id=fcf.farmflowcardid
                    where
                   coalesce(fcf.doctorid,'NULL')=coalesce(".nullcheck($doctor_id).",'NULL')
                    and (fcf.farmpriceid is null or ffc.quantity<0)";
            }
            else
            {
                if($from_type==0)
                {
                    $from=$doctor_id;
                }
                else
                {
                    $from=null;
                }
                $QueryText = "select fcf.farmflowid, sum(fc.quantity) from farmcardflow fcf
                    join farmcard fc on (fc.farmid=fcf.farmid and coalesce(fc.doctorid,'NULL')=coalesce(".nullcheck($from).",'NULL') and fc.farmpriceid is not null)
                    where
                    coalesce(fcf.doctorid,'NULL')=coalesce(".nullcheck($doctor_id).",'NULL')
                    and fcf.farmpriceid is null group by fcf.farmflowid";
            }   

//  file_put_contents("C:/tmp.txt", $QueryText);
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                if($from_type==-1 or (nonull($row[1]) and $row[1]>0))
                {
                    $flow = new WarehouseModuleFarmFlow($row[0],true,$from_type,$trans);
                    $res[]=$flow;
                }

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
    }
    /**
     * @param string $search
     * @param string $doctor_id
     * @param resource $trans
     * @return WarehouseModuleFarmCard[]
     */
    public static function searchFarmCards($search, $doctor_id, $trans=null)
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   
            
            $QueryText = "select ffc.farmpriceid 
                            from farmflowcard ffc 
                            left join farmprice fp on (fp.id=ffc.farmpriceid) 
                            left join farm f on (f.id=fp.farmid) 
                            where  ffc.quantity>0";
            if($doctor_id!="")
            {
                $QueryText.=" AND (ffc.doctorid = $doctor_id)";
                 
            }
            elseif(isset($doctor_id)==false)
            {
                $QueryText.=" AND (ffc.doctorid is null)";
            }            
            $QueryText.=" AND lower(f.name||' '||coalesce(f.nomenclaturearticlenumber, '')) like '%$search%' 
                            order by LOWER(f.name)";
  
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $sup = new WarehouseModuleFarmCard($row[0], $doctor_id, $trans, true);
                $res[]=$sup;

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
//			 file_put_contents("C:/tmp.txt",var_export($res, true));
			return $res;
    }	
    
    /**
     * @param string $search
     * @param resource $trans
     * @return WarehouseModuleFarm[]
     */
    public static function searchFarms($search, $trans=null)
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   

            $QueryText = "select f.id 
                            from farm f
                            where lower(f.name||' '||coalesce(f.nomenclaturearticlenumber, '')) like '%$search%' 
                            and f.nodetype=1
                            order by LOWER(f.name)";
  
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $sup = new WarehouseModuleFarm($row[0],$trans,true,-1);
                $res[]=$sup;

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
//			 file_put_contents("C:/tmp.txt",var_export($res, true));
			return $res;
    }
    
     /**
     * WarehouseModule::setFarmRequired()
     * 
     * @param string $farm_id
     * @param int $required                
     * @param resource $trans
     * @return boolean
     */
          
    public static function setFarmRequired($farm_id, $required, $trans=null)
	{
	   $res=true;
	   if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   

			$QueryText = "update farm set required=$required where id=$farm_id";
            
			$query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
			ibase_free_query($query);
            
            
            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	}
    
    /**
     * WarehouseModule::printOLAP()
     * 
     * @param string[] $fields
     * @param string[] $fieldNames
     * @param object[] $olapData
     * @param string $docType            
     * @return string
     */
          
    public static function printOLAP($fields, $fieldNames, $olapData, $docType)
	{

   // file_put_contents("C:/tmp.txt",var_export($olapData, true));                          
       $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/WarehouseOLAP.'.$docType, OPENTBS_ALREADY_UTF8);
        //$TBS->NoErr=true;
        
        $template=array();
        
        if($docType=='ods')
        {
            $rowBlock='table:table-row';
            $numOpe='odsNum';
            $dateOpe='odsDate'; 
        }
        else if($docType=='xlsx')
        {
            $rowBlock='row';
            $numOpe='xlsxNum';
            $dateOpe='xlsxDate'; 
        }
        else
        {
            $rowBlock='';
            $numOpe='';
            $dateOpe='';  
        }
                          
        foreach($fields as $key=>$field)
        {
            switch($field->type)
            {
                case 'numeric':
                {
                    $template[]='[OLAP.'.$field->name.';ope='.$numOpe.']';    
                    break;                    
                }
                case 'string':
                case 'date':
                default:
                {
                    $template[]='[OLAP.'.$field->name.']';
                    break;
                }                                                    
            }                                
                
         }
        if(count($template)>0)
        {
           $template[0]='[OLAP;block='.$rowBlock.']'.$template[0];                     
        } 
        $TBS->SetOption('protect',false);
        $TBS->MergeBlock('FIELDS',$fieldNames);            
        $TBS->MergeBlock('TEMPLATE',$template);
        $TBS->MergeBlock('OLAP',$olapData);
        
        $file_name = 'olap';
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;    
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        return $form_path; 
	}
 
 /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteOLAP($form_path)
    {
        return deleteFileUtils($form_path);
    }
    
     /**
     * WarehouseModule::getOLAP()
     * 
     * @param string $doctor_id
     * @param string $farmprice_id
     * @param string $start_date   
     * @param string $end_date
     * @param string $supplier_id   
     * @param int $type   
     * @param string $param                
     * @param resource $trans
     * @return WarehouseModuleOLAPRecord[]
     */
     
    public static function getOLAP($doctor_id, $farmprice_id, $start_date, $end_date, $supplier_id, $type, $param,  $trans=null)
	{		
            if(nonull($start_date))
            {
                $start_date.=' 00:00:00';
            }
            if(nonull($end_date))
            {
                $end_date.=' 23:59:59';
            }
            
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select $param 
                            ff.id,
                            coalesce(fcf.quantity,0),
                            (case when fcf.quantity>0 then ff.quantity else 0 end),
                            (case when fcf.quantity<0 then ff.quantity else 0 end),
                            fcf.beforequantity,
                                fcf.beforequantity+fcf.quantity,
                                fd.id,
                                fd.stype,
                                fd.rtype,
                                    s.id,
                                    s.name,
                                fd.doctimestamp,
                                fd.docnumber,
                    
                                tpf.id,
                                tpf.quantity,
                                    tp.id,
                                        hp.id,
                                        hp.name,
                                            hpf.quantity*tp.proc_count,
                                fp.id,
                                fp.price,
                                    f.id,
                                    f.name,
                                    f.nomenclaturearticlenumber,
                                    f.minimumquantity, 
                            d.id,
                            d.shortname,
                            
                            p.id,
                            p.shortname,
                            
                            null,null,null,null,
                            (case fd.stype when 0 then coalesce(sd.shortname, 'NA_Warehouse') when 1 then  coalesce(s.name,'NA_Residues') else null end),
                            (case fd.rtype when 0 then coalesce(rd.shortname, 'NA_Warehouse') when 1 then  coalesce(p.shortname,'NA_WriteOff') else null end),
                            null,
                            0
                        from farmcardflow fcf
                        left join farmflow ff on (ff.id=fcf.farmflowid)
                         left join farmdocument fd on (fd.id = ff.farmdocumentid)
                         left join doctors sd on (sd.id = fd.sdoctorid)
                         left join doctors rd on (rd.id = fd.rdoctorid)
                         left join doctors d on (d.id = fcf.doctorid)
                         left join farmprice fp on (fp.id = fcf.farmpriceid)
                         left join treatmentprocfarm tpf on (tpf.id = ff.treatmentprocfarmid)
                         left join treatmentproc tp on (tp.id = tpf.treatmentproc) 
                         left join farm f on (f.id = fp.farmid or f.id = tpf.farm)
                         left join healthproc hp on (hp.id = tp.healthproc_fk)
                         left join healthprocfarm hpf on (hpf.healthproc = hp.id and hpf.farm = tpf.farm) 
                         left join supplier s on (s.id = fd.supplierid) 
                         left join patients p on (p.id = tp.patient_fk) 
                         where fcf.doctimestamp >= '$start_date' and fcf.doctimestamp <= '$end_date'";  
                         
			     $QueryText2 = "select $param 
                            null,
                            0,
                            0,
                            0,
                             coalesce((select first 1 coalesce(lff.beforequantity+lff.quantity,0)
                                from farmcardflow lff where lff.farmflowcardid=fc.farmflowcardid
                                and (lff.doctimestamp < '$start_date')
                                order by lff.doctimestamp desc),0),
                            coalesce((select first 1 coalesce(lff.beforequantity+lff.quantity,0)
                            from farmcardflow lff where lff.farmflowcardid=fc.farmflowcardid
                            and (lff.doctimestamp <= '$end_date')
                            order by lff.doctimestamp desc),0),
                                null,
                                null,
                                null,
                                    null,
                                    null,
                                null,
                                null,
                    
                                null,
                                null,
                                    null,
                                        null,
                                        null,
                                            null,
                                fp.id,
                                fp.price,
                                    f.id,
                                    f.name,
                                    f.nomenclaturearticlenumber,
                                    f.minimumquantity, 
                            null,
                            null,
                            
                            null,
                            null,
                            
                            null,null,null,null,
                            null,
                            null,
                            f.required,
                            0
                        from farmcard fc
                         left join farmprice fp on (fp.id = fc.farmpriceid)
                         left join farm f on (f.id = fc.farmid)
                         where fc.quantity<>0 and fc.mindoctimestamp<='$end_date'";           
            $where="";
            if($doctor_id!="")
            {
                $QueryText.=" and fcf.doctorid = $doctor_id";
                $QueryText2.=" and fc.doctorid = $doctor_id";
            }
            else if(isset($doctor_id)==false)
            {

                $QueryText.=" and fcf.doctorid is null"; 
                $QueryText2.=" and fc.doctorid is null";
            }
            if(nonull($farmprice_id))
            {
              $QueryText.=" and fcf.farmpriceid = $farmprice_id";
              $QueryText2.=" and fc.farmpriceid = $farmprice_id";
            }
            if($supplier_id!="")
            {
                $QueryText.=" AND fd.supplierid = $supplier_id";  
            }
            else if(isset($supplier_id)==false)
            {
                $QueryText.=" AND fd.supplierid is null AND fd.stype = 1"; 
            }
            $QueryText.=' order by fd.doctimestamp';
            //$QueryText.=" union ".$QueryText2;
			//file_put_contents("C:/tmp.txt",$QueryText);
            
            if($type==0)
            {
                $query = ibase_prepare($trans, $QueryText);
            }
			else
            {
                $query = ibase_prepare($trans, $QueryText2);
    			
            }
            
			$result = ibase_execute($query);
                
                while ($row = ibase_fetch_row($result))
                {
                    $rec=new WarehouseModuleOLAPRecord($row);
                    $res[]=$rec;			     
                }
                
    			ibase_free_query($query);
    			ibase_free_result($result);
            
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
      /**
     * WarehouseModule::getOLAPCount()
     * 
     * @param string $doctor_id
     * @param string $farmprice_id
     * @param string $start_date   
     * @param string $end_date
     * @param string $supplier_id   
     * @param int $type                 
     * @param resource $trans
     * @return int
     */
     
    public static function getOLAPCount($doctor_id, $farmprice_id, $start_date, $end_date, $supplier_id, $type,  $trans=null)
	{		
            if(nonull($start_date))
            {
                $start_date.=' 00:00:00';
            }
            if(nonull($end_date))
            {
                $end_date.=' 23:59:59';
            }
            
            $res=0;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select count(*) 
                        from farmcardflow fcf
                        left join farmflow ff on (ff.id=fcf.farmflowid)
                         left join farmdocument fd on (fd.id = ff.farmdocumentid)
                         left join doctors sd on (sd.id = fd.sdoctorid)
                         left join doctors rd on (rd.id = fd.rdoctorid)
                         left join doctors d on (d.id = fcf.doctorid)
                         left join farmprice fp on (fp.id = fcf.farmpriceid)
                         left join treatmentprocfarm tpf on (tpf.id = ff.treatmentprocfarmid)
                         left join treatmentproc tp on (tp.id = tpf.treatmentproc) 
                         left join farm f on (f.id = fp.farmid or f.id = tpf.farm)
                         left join healthproc hp on (hp.id = tp.healthproc_fk)
                         left join healthprocfarm hpf on (hpf.healthproc = hp.id and hpf.farm = tpf.farm) 
                         left join supplier s on (s.id = fd.supplierid) 
                         left join patients p on (p.id = tp.patient_fk) 
                         where fcf.doctimestamp >= '$start_date' and fcf.doctimestamp <= '$end_date'";  
                         
			     $QueryText2 = "select count(*) 
                        from farmcard fc
                         left join farmprice fp on (fp.id = fc.farmpriceid)
                         left join farm f on (f.id = fc.farmid)
                         where fc.quantity<>0 and fc.mindoctimestamp<='$end_date'";           
            $where="";
            if($doctor_id!="")
            {
                $QueryText.=" and fcf.doctorid = $doctor_id";
                $QueryText2.=" and fc.doctorid = $doctor_id";
            }
            else if(isset($doctor_id)==false)
            {

                $QueryText.=" and fcf.doctorid is null"; 
                $QueryText2.=" and fc.doctorid is null";
            }
            if(nonull($farmprice_id))
            {
              $QueryText.=" and fcf.farmpriceid = $farmprice_id";
              $QueryText2.=" and fc.farmpriceid = $farmprice_id";
            }
            if($supplier_id!="")
            {
                $QueryText.=" AND fd.supplierid = $supplier_id";  
            }
            else if(isset($supplier_id)==false)
            {
                $QueryText.=" AND fd.supplierid is null AND fd.stype = 1"; 
            }
            //$QueryText.=" union ".$QueryText2;
			//file_put_contents("C:/tmp.txt",$QueryText);
            
            if($type==0)
            {
                $query = ibase_prepare($trans, $QueryText);
            }
            else
            {
                $query = ibase_prepare($trans, $QueryText2);
            }
    			$result = ibase_execute($query);
                
                if ($row = ibase_fetch_row($result))
                {
                    $res=$row[0];			     
                }
                
    			ibase_free_query($query);
    			ibase_free_result($result);
           
			
            
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
} 
class WarehouseModuleFarmDocument
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $RTYPE; 
    
   	/**
    * @var string
    */
    var $STYPE; 
    
    /**
    * @var string
    */
    var $TREATMENTPROCID;
    
    /**
    * @var WarehouseModuleSupplier
    */
    var $SUPPLIER;
    
    /**
    * @var WarehouseModuleDoctor
    */
    var $SDOCTOR;
    
    /**
    * @var WarehouseModuleDoctor
    */
    var $RDOCTOR;
        
    /**
    * @var string
    */
    var $DOCTIMESTAMP;
    
    /**
    * @var string
    */
    var $DOCNUMBER;
    
    /**
    * @var double
    */
    var $SUMM;
    
    /**
    * @var WarehouseModuleFarmFlow[]
    */
    var $FARMFLOWS;
    
        /**
    * @var string
    */
    var $COMMENT;
    
    /**
    * @var WarehouseModuleRoom
    */
    var $ROOM; 
    
    /**
    * @var string
    */
    var $STATUS;
    
    function __construct($id=null, $withproc=null, $withFlows=null, $trans=null) 
    {
        
        if($id)
        { 
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   
             
            	$QueryText = "
                select
                        fd.stype,
                        fd.rtype,
                        fd.supplierid,
                        fd.sdoctorid,
                        fd.rdoctorid,
                        fd.doctimestamp,
                        fd.docnumber,
                        fd.summ,
                        fd.comment,
                        fd.roomid";
                if($withproc)
                {
                    $QueryText.=", fd.treatmentprocid";
                }
                $QueryText.=" from farmdocument fd
                    where fd.id=$id";
    			
    			$query = ibase_prepare($trans, $QueryText);
    			$result = ibase_execute($query);
    			
    			if ($row = ibase_fetch_row($result))
    			{				
                    $this->ID=$id;
                    $this->STYPE=$row[0];
                    $this->RTYPE=$row[1];
                    $this->SUPPLIER=new WarehouseModuleSupplier($row[2],$trans);
                    $this->SDOCTOR=new WarehouseModuleDoctor($row[3],$trans);
                    $this->RDOCTOR=new WarehouseModuleDoctor($row[4],$trans);               
                    $this->DOCTIMESTAMP=$row[5];
                    $this->DOCNUMBER=$row[6];
                    $this->SUMM=$row[7];
                    $this->COMMENT=$row[8];
                    $this->ROOM=new WarehouseModuleRoom($row[9],$trans);
                    if($withproc)
                    {
                        $this->TREATMENTPROCID=$row[10];
                    }
                    if($withFlows)
                    {
                        $this->FARMFLOWS=WarehouseModuleFarmDocument::getFarmFlows($id,$withproc,$trans);
                    }
    
    			}			
    			
    			ibase_free_query($query);
    			ibase_free_result($result);               
                   
                if($local)
                {
                    ibase_commit($trans);
                    $connection->CloseDBConnection();
                }   
            }     
    }
    
    /**
     * WarehouseModuleFarmDocument::getFarmFlows()
     * 
     * @param string $docid
     * @param boolean $withproc                    
     * @param resource $trans
     * @return WarehouseModuleFarmFlow[]
     */
     
    public static function getFarmFlows($docid, $withproc, $trans=null)
	{		
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   
            
			$QueryText = "
                  select
                    ff.id
                from farmflow ff
                where ff.farmdocumentid=$docid";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $flow=new WarehouseModuleFarmFlow($row[0], $withproc,0, $trans);
                $res[]=$flow;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}    
} 

class WarehouseModuleFarmFlow
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $FARMDOCUMENTID; 
    
   	/**
    * @var WarehouseModuleFarmPrice
    */
    var $FARMPRICE;
            
   	/**
    * @var double
    */
    var $QUANTITY;
   	
    /**
    * @var double
    */
    var $CARDQUANTITY;
    
   	/**
    * @var WarehouseModuleTPFarm
    */
    var $TPFARM; 
    
    /**
    * @var string
    */
    var $STATUS;
          
    /**
    * @var double
    */
    var $SUMM;
        
    function __construct($id, $withproc, $from_type, $trans=null) 
    {
        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }      
        	$QueryText = "select
                            ff.quantity,
                            ff.farmdocumentid,
                            ff.farmpriceid,
                            fc.quantity";
            if($withproc)
            {
                $QueryText.=", ff.treatmentprocfarmid";
            }
            
            $QueryText.=" from farmflow ff
            left join farmdocument fd on (fd.id=ff.farmdocumentid)
            left join farmcard fc on (fc.farmpriceid=ff.farmpriceid and coalesce(fc.doctorid, 'NULL')=coalesce(fd.rdoctorid, 'NULL'))                       
                where ff.id=$id";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{				
                $this->ID=$id;
                $this->QUANTITY=$row[0];
                $this->FARMDOCUMENTID=$row[1];
                if(nonull($row[2]))
                {
                    $this->FARMPRICE=new WarehouseModuleFarmPrice($row[2],$trans);
                    $this->SUMM=$row[0]*$this->FARMPRICE->PRICE;
                }
                $this->CARDQUANTITY=$row[3];              
                if($withproc and nonull($row[4]))
                {
                    $this->TPFARM=new WarehouseModuleTPFarm($row[4],$from_type,$trans);
                }
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
}  
  
class WarehouseModuleDoctor
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $SHORTNAME;
    
   	/**
    * @var boolean
    */
    var $ISNEGATIVE;
    
    
    function __construct($id, $withbalance=false, $trans=null) 
    {
        $this->ID=$id;
        if($id!=null)
        {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }      
            
       	    $QueryText = "select d.shortname from doctors d
                        where d.id=$id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{				
                $this->SHORTNAME=$row[0];
                if($withbalance)
                {
                    $this->ISNEGATIVE=WarehouseModuleDoctor::checkDoctorBalance($id,$trans);
                }
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
        }
        else
        {
            $this->SHORTNAME='';
            if($withbalance)
            {
                $this->ISNEGATIVE=WarehouseModuleDoctor::checkDoctorBalance(null,$trans);
            }
        }
        
    } 
    
        /**
     * WarehouseModuleDoctor::checkDoctorBalance()
     * 
     * @param string $id                
     * @param resource $trans
     * @return boolean
     */
    public static function checkDoctorBalance($id, $trans=null)
    {
        $res=false;
        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
   	    $QueryText = "select count(ffc.id) 
                        from farmflowcard ffc where ffc.farmpriceid is null and ffc.doctorid";
        if($id!="")
        {
            $QueryText.=" = $id";
             
        }
        else
        {
            $QueryText.=" is null";
        }   
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        if ($row = ibase_fetch_row($result))
		{				
            $res=$row[0]>0;
		}    
        ibase_free_query($query);
		ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res;
    }
}



class WarehouseModuleSupplier
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $NAME;

    
    function __construct($id, $trans=null) 
    {
        $this->ID=$id;
        if($id!=null)
        {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }      
        	$QueryText = "select
                            s.name
                        from supplier s
                        where s.id=$id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{				
                $this->NAME=$row[0];
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
        }
        else
        {
            $this->NAME='';
        }
        
    } 
}
class WarehouseModuleFarmCard extends WarehouseModuleFarmPrice
{    
   	/**
    * @var number
    */
    var $QUANTITY; 
       
   	/**
    * @var WarehouseModuleDoctor
    */
    var $DOCTOR;
    function __construct($id, $doctor_id, $trans=null, $withunits=false) 
    {
        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   
    	$QueryText = "select first 1
                        fp.price,
                        fp.farmid,
                        ffc.quantity
                    from farmflowcard ffc 
                            left join farmprice fp on (fp.id=ffc.farmpriceid) 
                            left join farm f on (f.id=fp.farmid) 
                    where ffc.farmpriceid=$id and ffc.quantity>0 ";
            if($doctor_id!="")
            {
                $QueryText.=" AND (ffc.doctorid = $doctor_id)";
                 
            }
            elseif($doctor_id=null)
            {
                $QueryText.=" AND (ffc.doctorid is null)";
            }   
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		
		if ($row = ibase_fetch_row($result))
		{	
            $this->ID=$id;			
            $this->PRICE=$row[0];
            $this->FARM=new WarehouseModuleFarm($row[1],$trans,$withunits);
            $this->QUANTITY=$row[2];
		}			
		
		ibase_free_query($query);
		ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
    }
    
}
class WarehouseModuleFarmPrice
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var number
    */
    var $PRICE;
    
   	/**
    * @var WarehouseModuleFarm
    */
    var $FARM;
    
   	/**
    * @var double
    */
    var $QUANTITY;
    
   	/**
    * @var string
    */
    var $STATUS;         
    
    function __construct($id, $trans=null, $withunits=false) 
    {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   
        	$QueryText = "select
                            fp.price,
                            fp.farmid
                        from farmprice fp
                        where fp.id=$id";
    		
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		
    		if ($row = ibase_fetch_row($result))
    		{	
                $this->ID=$id;			
                $this->PRICE=$row[0];
                $this->FARM=new WarehouseModuleFarm($row[1],$trans,$withunits);
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }    
    } 
}

class WarehouseModuleFarm
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $NAME;
        
   	/**
    * @var string
    */
    var $ARTICUL;
   	
    /**
    * @var string
    */
    var $BASEUNITNAME;
    
   	/**
    * @var WarehouseModuleFarmUnit[]
    */
    var $UNITS;
    
   	/**
    * @var WarehouseModuleFarmPrice[]
    */
    var $PRICES;
    
    function __construct($id, $trans=null, $withunits=false, $from_type=0) 
    {

        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }   
    	$QueryText = "select
                        f.name,
                        f.nomenclaturearticlenumber,
                        u.name
                    from farm f
                    left join unitofmeasure u 
                    on (u.farmid=f.id and u.isbase=1)
                    where f.id=$id";
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		
		if ($row = ibase_fetch_row($result))
		{				
            $this->ID=$id;
            $this->NAME=$row[0];
            $this->ARTICUL=$row[1];
            $this->BASEUNITNAME=$row[2];
            if($withunits)
            {
               $this->UNITS=WarehouseModuleFarm::getFarmUnits($id,$trans); 
            }
            if($from_type!=0)
            {
               $this->PRICES=WarehouseModuleFarm::getFarmPrices($id,$withunits,$from_type,$trans); 
            }
		}			
		
		ibase_free_query($query);
		ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }

    }
     
     /**
     * WarehouseModuleFarmDocument::getFarmUnits()
     * 
     * @param string $farmid           
     * @param resource $trans
     * @return WarehouseModuleFarmUnit[]
     */
     
    public static function getFarmUnits($farmid, $trans=null)//sqlite
	{		
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "
                  select
                    u.id
                from unitofmeasure u
                where u.farmid=$farmid
                order by u.isbase desc";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $unit=new WarehouseModuleFarmUnit($row[0], $trans);
                $res[]=$unit;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}    
    
     /**
     * WarehouseModuleFarmDocument::getFarmPrices()
     * 
     * @param string $farmid 
     * @param boolean $withunits
     * @param boolean $from_type         
     * @param resource $trans
     * @return WarehouseModuleFarmPrice[]
     */
     
    public static function getFarmPrices($farmid, $withunits, $from_type, $trans=null)
	{		
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            $order=getApplicationSetting('WRITEOFFORDER',$trans);
            if(nonull($order)==false)
            {
                $order="farmpriceid asc";
            }
            if($from_type==-1)
            {
                $order=str_replace("farmprice","",$order);
                $QueryText = "
                              select
                                fp.id,
                                0
                            from farmprice fp
                            where fp.farmid=$farmid
                            order by fp.$order";
            }
            elseif($from_type==1)
            {
                $QueryText = "
                              select
                                fc.farmpriceid,
                                fc.quantity
                            from farmcard fc
                            where fc.farmid=$farmid
                            and fc.doctorid is null
                            order by fc.$order";
            }
			
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $farmPrice=new WarehouseModuleFarmPrice($row[0], $trans, $withunits);
                $farmPrice->QUANTITY=$row[1];
                $res[]=$farmPrice;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}    
}

class WarehouseModuleFarmUnit
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $FARMID;
    
   	/**
    * @var string
    */
    var $NAME;
        
   	/**
    * @var boolean
    */
    var $ISBASIC;
    
   	/**
    * @var double
    */
    var $COEFFICIENT;
    
    function __construct($id, $trans=null) 
    {

        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }  
    	$QueryText = "select
                        u.name,
                        u.coefficient,
                        u.isbase
                        from unitofmeasure u
                    where u.id=$id";
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		
		if ($row = ibase_fetch_row($result))
		{				
            $this->ID=$id;
            $this->NAME=$row[0];
            $this->COEFFICIENT=$row[1];
            $this->ISBASIC=($row[2]==1);
		}			
		
		ibase_free_query($query);
		ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }

    }    
}

class WarehouseModuleTPFarm
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var WarehouseModuleFarm
    */
    var $FARM;
    
   	/**
    * @var string
    */
    var $TPNAME;
    
   	/**
    * @var string
    */
    var $TPDATE;
    
   	/**
    * @var string
    */
    var $PATIENTSHORTNAME;
    
    function __construct($id, $from_type, $trans=null) 
    {

        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }  
    	$QueryText = "select tpf.farm, tp.name, tp.dateclose, p.shortname from treatmentprocfarm tpf
                    left join treatmentproc tp on (tp.id=tpf.treatmentproc)
                    left join patients p on (p.id=tp.patient_fk)
                    where tpf.id=$id";
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		
		if ($row = ibase_fetch_row($result))
		{				
            $this->ID=$id;
            $this->FARM=new WarehouseModuleFarm($row[0],$trans,true,$from_type);
            $this->TPNAME=$row[1];
            $this->TPDATE=$row[2];
            $this->PATIENTSHORTNAME=$row[3];
		}			
		
		ibase_free_query($query);
		ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }

    }    
}

class WarehouseModuleRoom
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $NAME;
    
    function __construct($id, $from_type, $trans=null) 
    {

        $this->ID=$id;
        if($id!=null)
        {
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }  
        	$QueryText = "select r.name from rooms r where r.id=$id";
    		
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		
    		if ($row = ibase_fetch_row($result))
    		{				
                $this->NAME=$row[0];
    		}			
    		
    		ibase_free_query($query);
    		ibase_free_result($result);                       
                   
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

      }
    }    
}
class WarehouseModuleOLAPRecord
{
       /**
    * @var string
    */
    var $FARMFLOW_ID;

    /**
    * @var double
    */
    var $FARMFLOW_QUANTITY;
    
    /**
    * @var double
    */
    var $FARMFLOW_INQUANTITY;
    
        
    /**
    * @var double
    */
    var $FARMFLOW_OUTQUANTITY;

    
    /**
    * @var double
    */
    var $FARMFLOW_BEFOREQUANTITY;
  
    /**
    * @var double
    */
    var $FARMFLOW_RESQUANTITY;
         
    /**
    * @var string
    */
    var $FARMDOCUMENT_ID;

    /**
    * @var string
    */
    var $FARMDOCUMENT_STYPE;

    /**
    * @var string
    */
    var $FARMDOCUMENT_RTYPE;

    /**
    * @var string
    */
    var $SUPPLIER_ID;

    /**
    * @var string
    */
    var $SUPPLIER_NAME;

    /**
    * @var string
    */
    var $FARMDOCUMENT_DOCTIMESTAMP;

    /**
    * @var string
    */
    var $FARMDOCUMENT_DOCNUMBER;

    /**
    * @var string
    */
    var $TREATMENTPROCFARM_ID;

    /**
    * @var double
    */
    var $TREATMENTPROCFARM_QUANTITY;

    /**
    * @var string
    */
    var $TREATMENTPROC_ID;

    /**
    * @var string
    */
    var $HEALTHPROC_ID;

    /**
    * @var string
    */
    var $HEALTHPROC_NAME;
    
    /**
    * @var double
    */
    var $HEALTHPROCFARM_QUANTITY;
    
    /**
    * @var string
    */
    var $FARMPRICE_ID;

    /**
    * @var string
    */
    var $FARMPRICE_PRICE;

    /**
    * @var string
    */
    var $FARM_ID;

    /**
    * @var string
    */
    var $FARM_NAME;

    /**
    * @var string
    */
    var $FARM_NOMENCLATUREARTICLENUMBER;

    /**
    * @var double
    */
    var $FARM_MINIMUMQUANTITY;

    /**
    * @var string
    */
    var $DOCTOR_ID;

    /**
    * @var string
    */
    var $DOCTOR_SHORTNAME;
        
        /**
    * @var string
    */
    var $PATIENT_ID;

    /**
    * @var string
    */
    var $PATIENT_SHORTNAME;
        
    /**
    * @var double
    */
    var $SUMMOUT;        
    
        /**
    * @var double
    */
    var $SUMMBEFORE;   
    
        /**
    * @var double
    */
    var $SUMMRES;   
    
        /**
    * @var double
    */
    var $SUMMIN;   
    /**
    * @var string
    */
    var $SENDER;
    
    /**
    * @var string
    */
    var $RECEIVER;
 
    /**
    * @var int
    */
    var $FARM_REQUIRED;
    
    /**
    * @var boolean
    */
    var $isOpen; 
               
    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars("WarehouseModuleOLAPRecord"));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }


}
?>
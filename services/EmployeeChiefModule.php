<?php

require_once "Connection.php";
require_once "Utils.php";
 
class EmployeeChiefModule_EmployeeSalary
{
    /**
    * @var string
    */
    var $id;
    
    /**
    * @var string
    */
    var $fname;
     
    /**
    * @var string
    */
    var $sname;
     
    /**
    * @var string
    */
    var $lname;
    
    /**
    * @var string
    */
    var $fullname;
     
    /**
    * @var string
    */
    var $shortname;
    
    /**
    * @var string
    */
    var $birthday; 

    /**
    * @var int
    */
    var $sex;     

    /**
    * @var string
    */
    var $phone1;  
    
    /**
    * @var string
    */
    var $phone2; 
    
    /**
    * @var string
    */
    var $specialty; 
	
    /**
    * @var EmployeeChiefModule_EmployeeFlow[]
    */
    var $flowAdd; 
    /**
    * @var EmployeeChiefModule_EmployeeFlow[]
    */
    var $flowRemove; 
    
    
    /**
    * @var number
    */
    var $flowSumm; 
	
}

class EmployeeChiefModule_EmployeeFlow
{
    /**
    * @var string
    */
    var $id;
	
    /**
    * @var number
    */
    var $summ;
    
    /**
    * @var string
    */
    var $comment;
     
    /**
    * @var string
    */
    var $operationTimestamp;
    
    /**
    * @var int
    */
    var $isCashpayment;
     
    /**
    * @var string
    */
    var $group_fk;
     
    /**
    * @var string
    */
    var $employee_fk;
    
    /**
    * @var string
    */
    var $authorid;
    
}

class EmployeeChiefModule_SalGroup
{
    /**
    * @var string
    */
    var $id;
	
    /**
    * @var string
    */
    var $name;
    
    /**
    * @var int
    */
    var $color;
}


class EmployeeChiefModule
{	
    /**
     * @param EmployeeChiefModule_EmployeeSalary $p1
	 * @param EmployeeChiefModule_EmployeeFlow $p2
     * @param EmployeeChiefModule_SalGroup $p3
     */
    public function registertypes($p1, $p2, $p3)
    {}
    
    /** 
    * @return EmployeeChiefModule_SalGroup
    */  
    public function getSalaryGroup()
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryEmpText = "select ID, NAME, COLOR
                from ACCOUNTFLOW_GROUPS
                where ISEMPLOYEE = 1"; 
        $queryEmp = ibase_prepare($trans, $QueryEmpText);
		$resultEmp = ibase_execute($queryEmp);
            
        $objEmp = new EmployeeChiefModule_SalGroup;
        $objEmp->name = 'none';
		if($rowEmp = ibase_fetch_row($resultEmp))
		{ 
          $objEmp->id = $rowEmp[0];
          $objEmp->name = $rowEmp[1];
          $objEmp->color = $rowEmp[2];
		}
        ibase_free_result($resultEmp);
		ibase_free_query($queryEmp);
        ibase_commit($trans);
        
		$connection->CloseDBConnection();
		return $objEmp;
    }
    
    /** 
 	* @param string $startDate
 	* @param string $endDate
 	* @param string $lastname
	* @return EmployeeChiefModule_EmployeeSalary[]
	*/    
    public function getEmployeeSalary($startDate, $endDate, $lastname)
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		
		$QueryText = "select ID, FNAME, LNAME, SNAME, PHONE1, PHONE2, BIRTHDAY, SPECIALTY, SEX
                      from EMPLOYEE where (ID is not null)";
		if ($lastname != "")	
		{
			$QueryText .= " and lower(LNAME) starting lower('$lastname')";
		}				
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new EmployeeChiefModule_EmployeeSalary;
			$obj->id = $row[0];
			$obj->fname = $row[1];
			$obj->lname = $row[2];
			$obj->sname = $row[3];
			$obj->fullname = $row[2].' '.$row[1].' '.$row[3];
			$obj->shortname = shortname($row[2],$row[1],$row[3]);
			$obj->phone1 = $row[4];
			$obj->phone2 = $row[5];
			$obj->birthday = $row[6];
			$obj->specialty = $row[7];
			$obj->sex = $row[8];
            
            $obj->flowSumm = 0;
            $obj->flowAdd = array();
            $obj->flowRemove = array();
            
            $QuerySalaryTxt = "select 
                                     ID,
                                     SUMM,
                                     OPERATIONNOTE,
                                     OPERATIONTIMESTAMP,
                                     IS_CASHPAYMENT,
                                     EMPLOYEE_FK,
                                     AUTHORID
                        from ACCOUNTFLOW
                    where OPERATIONTIMESTAMP >= '$startDate 00:00:00'
                    and OPERATIONTIMESTAMP <= '$endDate 23:59:59'
                    and EMPLOYEE_FK = ".$obj->id;
                    
            $querySalary = ibase_prepare($trans, $QuerySalaryTxt);
            $resultSalary = ibase_execute($querySalary);
            while ($rowSalary = ibase_fetch_row($resultSalary))
            {
                $objFlow = new EmployeeChiefModule_EmployeeFlow;
    			$objFlow->id = $rowSalary[0];
    			$objFlow->summ = abs($rowSalary[1]);
    			$objFlow->comment = $rowSalary[2];
    			$objFlow->operationTimestamp = $rowSalary[3];
    			$objFlow->isCashpayment = $rowSalary[4];
    			$objFlow->employee_fk = $rowSalary[5];
                $objFlow->authorid = $rowSalary[6];
                $obj->flowSumm -= $rowSalary[1];
                if($rowSalary[1] < 0)
                {
                    $obj->flowAdd[] = $objFlow;
                }
                else
                {
                    $obj->flowRemove[] = $objFlow;
                }
            }
    		ibase_free_query($querySalary);
    		ibase_free_result($resultSalary);
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /**
 	* @param EmployeeChiefModule_EmployeeFlow $salary
	* @return boolean
 	*/
    public function addSalary($salary) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = 		
		"insert into ACCOUNTFLOW (
                                    SUMM, 
                                    OPERATIONTYPE, 
                                    OPERATIONNOTE, 
                                    OPERATIONTIMESTAMP,
                                    IS_CASHPAYMENT, 
                                    GROUP_FK,
                                    EMPLOYEE_FK,
                                    AUTHORID)
                    values (
                                    ".$salary->summ.", 
                                    1, 
                                    '".safequery($salary->comment)."',
                                    '".$salary->operationTimestamp."',
                                    ".$salary->isCashpayment.", 
                                    ".$salary->group_fk.",
                                    ".$salary->employee_fk.",
                                    ".$salary->authorid.")";
        
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;		
    }      
         
    
    /**
 	* @param string $id,
	* @return boolean
 	*/ 		 		
    public function deleteSalary($id) 
     {           
     	$connection = new Connection();   	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$QueryText = "delete from ACCOUNTFLOW
                            where (ID = ".$id.")";
    	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);
        
		$connection->CloseDBConnection();
		return $success;	
    }     
  
}

?>
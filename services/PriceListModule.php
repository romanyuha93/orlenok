<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "Form039Module.php";

class PriceListModuleReportingRecord
{
    /**
	* @var string
	*/
	var $F39;
    
    /**
	* @var string
	*/
	var $DESCRIPTION;
}
class PriceListModuleRecord
{
    	/**
	* @var string
	*/
	var $label;
    
    
	/**
	* @var boolean
	*/
	var $isOpen;
	/**
	* @var boolean
	*/
	var $isBranch;
	/**
	* @var string
	*/
	var $ID;

	/**
	* @var string
	*/
	var $PARENTID;
	
	/**
	* @var string
	*/
	var $NODETYPE;
	
	/**
	* @var string
	*/
	var $SHIFR;

	/**
	* @var string
	*/
	var $NAME;
	 
	/**
	* @var string
	*/
	var $PLANNAME;  
	
	/**
	* @var string
	*/
	var $UOP; 
	
	/**
	* @var string
	*/
	var $PROCTYPE ; 
	
	/**
	* @var string
	*/
	var $TOOTHUSE; 

	/**
	* @var string
	*/
	var $PRICE;
	/**
	* @var string
	*/
	var $PRICE2;
	/**
	* @var string
	*/
	var $PRICE3;
	
	/**
	* @var string
	*/
	var $HEALTHTYPE;
	
	/**
	* @var string
	*/
	var $PROCTIME;
	
	/**
	* @var number
	*/
	var $EXPENSES;
    
	/**
	* @var int
	*/
	var $discount_flag;
    
    /**
    * @var PriceListModuleRecordFarm[]
    */
    var $farms;
	
	/**
	* @var string
	*/
	var $CAPTIONLEVEL;
    
	/**
	* @var string
	*/
	var $SEQUINCENUMBER;
    
    
	/**
	* @var int
	*/
	var $FARMSCOUNT;
    
	/**
	* @var PriceListModuleRecord[]
	*/
	var $children;
    
    
	/**
	* @var int
	*/
	var $salary_settings;
    
	/**
	* @var string
	*/
	var $complete_examdiag;
    
    
    
	/**
	* @var string
	*/
	var $HEALTHTYPES;
    
   	/**
	* @var Form039Module_F39_Val[]
	*/
	var $F39;
    
    
    	/**
	* @var string
	*/
	var $NAME48;
    
   	/**
	* @var int
	*/
	var $regCode;
} 

class PriceListModuleRecordFarm
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $farmID;
		
    /**
    * @var string
    */
    var $farmName;
		
    /**
    * @var string
    */
    var $farmGroup;		
		
    /**
    * @var number
    */
    var $farmQuantity;
    
    /**
    * @var number
    */
    var $farmPrice;		
    
    /**
    * @var string
    */
    var $farmUnitofmeasure;
    
    
    /**
    * @var string
    */
    var $farmHealthproc;
}

class PriceListModuleRecordRegistrator
{
   	/**
	* @var string
	*/
	var $NAME48;
    
   	/**
	* @var int
	*/
	var $regCode;
} 


class PriceListModule
{		
	/**
	* @param PriceListModuleRecord $p1
    * @param PriceListModuleRecordRegistrator $p2
	*/
	public function registertypes($p1, $p2) 
	{
	}

	public function getPriceListRecordsRecursive($trans, $parentID, $captionLevel, $whereStr)//sqlite
	{
		$QueryText = "
		select              p.ID,
            p.PARENTID,
            p.NODETYPE,    
            p.SHIFR,       
            p.NAME,        
            p.PLANNAME,    
            p.UOP,         
            p.PROCTYPE,    
            p.TOOTHUSE,    
            p.PRICE,       
            p.HEALTHTYPE,  
            p.PROCTIME,
            p.SEQUINCENUMBER,
            p.EXPENSES,
            p.FARMSCOUNT,
            p.DISCOUNT_FLAG,
            p.SALARY_SETTINGS,
            p.COMPLETEEXAMDIAG,
            p.HEALTHTYPES,
            p.name48,
            p.regcode,    
            p.PRICE2,    
            p.PRICE3
        from HEALTHPROC p";
		
		if (($parentID == "null") || ($parentID == ""))
		{
			$QueryText .= " where p.PARENTID is null";                  
		}			
		else 
		{
		    
			$QueryText .= " where p.PARENTID = $parentID";
		}
        $QueryText.=$whereStr." order by p.SEQUINCENUMBER";
        
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();	
	
		while ($row = ibase_fetch_row($result))
		{
			$obj = new PriceListModuleRecord;
			$obj->ID = $row[0]; 
			$obj->PARENTID = $row[1];
			$obj->NODETYPE = $row[2];
			$obj->SHIFR = $row[3];
			$obj->NAME = $row[4];
			$obj->PLANNAME = $row[5];
            $obj->label = $row[5];
			$obj->UOP = $row[6];
			$obj->PROCTYPE = $row[7];
			$obj->TOOTHUSE = $row[8];
			$obj->PRICE = $row[9];
			$obj->HEALTHTYPE = $row[10];
			$obj->PROCTIME = $row[11];
            $obj->SEQUINCENUMBER = $row[12];
            $obj->EXPENSES = $row[13];
            $obj->FARMSCOUNT = $row[14];
			$obj->discount_flag = $row[15];	
            $obj->salary_settings = $row[16];
            $obj->complete_examdiag = $row[17];
            $obj->HEALTHTYPES = $row[18];
            $obj->NAME48 = $row[19];
            $obj->regCode = $row[20];
            $obj->PRICE2 = $row[21];
            $obj->PRICE3 = $row[22];
            $obj->F39 = Form039Module::get039ProcValues(0,$obj->ID,$trans);
			$parentID = $obj->ID;
			
			if ($obj->NODETYPE == "0")
			{
				$obj->CAPTIONLEVEL = $captionLevel;
				$obj->children = $this->getPriceListRecordsRecursive($trans, $parentID, $captionLevel + 1, $whereStr);	
			}
			else
			{			
				$obj->CAPTIONLEVEL = 0;
                if(($obj->FARMSCOUNT != null)&&($obj->FARMSCOUNT > 0))
                {
                    $farmQueryText = "select
                                                HEALTHPROCFARM.ID,
                                                FARM.ID,
                                                FARM.NAME, 
                                                FARMGROUP.CODE, 
                                                HEALTHPROCFARM.QUANTITY,  
                                                0,
                                                UNITOFMEASURE.NAME
                                                
                                                from HEALTHPROCFARM 
                                                
                                                left join FARM
                                                    on HEALTHPROCFARM.FARM = FARM.ID
    
                                                left join UNITOFMEASURE
                                                    on FARM.ID = UNITOFMEASURE.FARMID
                                                
                                                left join FARMGROUP
                                                    on FARM.FARMGROUPID = FARMGROUP.ID
                                                
                                                where HEALTHPROCFARM.HEALTHPROC='$row[0]' and UNITOFMEASURE.isbase = 1";

            		$farmquery = ibase_prepare($trans, $farmQueryText);
            		$farmresult = ibase_execute($farmquery);
                    while ($farmRow = ibase_fetch_row($farmresult))
                    {
                        $farmobj = new PriceListModuleRecordFarm;
                        $farmobj->ID = $farmRow[0];
                        $farmobj->farmID = $farmRow[1];
                        $farmobj->farmName = $farmRow[2];
                        $farmobj->farmGroup = $farmRow[3];
                        $farmobj->farmQuantity = $farmRow[4];
                        $farmobj->farmPrice = $farmRow[5];
                        $farmobj->farmUnitofmeasure = $farmRow[6];
                        $obj->farms[] = $farmobj;
                    }                            
    			}			
			}
			$rows[] = $obj;
		}	
		
		ibase_free_query($query);
		ibase_free_result($result);	
			
		return $rows; 
	}
    
    // **************************************** FARM PROCEDURES START ************************************
    /**
 	* @param PriceListModuleRecordFarm $priceListRecordFarm
 	* @param string $healthprocID
    * @return PriceListModuleRecordFarm
 	*/
	public function saveFarm($priceListRecordFarm, $healthprocID) 
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "insert into HEALTHPROCFARM(ID, HEALTHPROC, FARM, QUANTITY)
						values(";
        if(($priceListRecordFarm->ID == "")||($priceListRecordFarm->ID == "null"))
        {
	        $QueryText .= "null"; 
    	}          
        else
        {
            $QueryText .= "$priceListRecordFarm->ID"; 
        }      
        $QueryText .= ", $healthprocID, $priceListRecordFarm->farmID, $priceListRecordFarm->farmQuantity) returning (ID)";

        $query = ibase_prepare($trans, $QueryText);            
        $result = ibase_execute($query);
        $newfarmID = ibase_fetch_row($result);
        ibase_free_query($query);
        ibase_commit($trans);
		$connection->CloseDBConnection();
        $priceListRecordFarm->farmHealthproc = $healthprocID;
        $priceListRecordFarm->ID = $newfarmID;
        return $priceListRecordFarm;
    }
    /**
 	* @param PriceListModuleRecordFarm $priceListRecordFarm
    * @return boolean
 	*/
	public function updateFarm($priceListRecordFarm) 
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update HEALTHPROCFARM set
                        QUANTITY = $priceListRecordFarm->farmQuantity
                        where ID = $priceListRecordFarm->ID";
        $query = ibase_prepare($trans, $QueryText);            
        $result = ibase_execute($query);
        ibase_free_query($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        return $success;
    }
    /**
 	* @param string $healthprocfarmID
    * @return boolean
 	*/
	public function deleteFarm($healthprocfarmID) 
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "delete from HEALTHPROCFARM where ID = $healthprocfarmID";
        $query = ibase_prepare($trans, $QueryText);            
        ibase_execute($query);
        ibase_free_query($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        return $success;
    }
    
    /**
     * @return PriceListModuleRecordFarm[]
     */
    public function getFarmsForProcDictionary()//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);

        $QueryFarmText = "select
                                        FARM.ID,
                                        FARM.NAME, 
                                        FARMGROUP.CODE,
                                        UNITOFMEASURE.NAME,  
                                        0
                                        
                                        from FARM
                                        left join UNITOFMEASURE
                                            on FARM.ID = UNITOFMEASURE.FARMID
                                   
                                        left join FARMGROUP
                                            on FARM.FARMGROUPID = FARMGROUP.ID

                                        where FARM.nodetype = 1 and UNITOFMEASURE.isbase = 1";

        $FarmQuery = ibase_prepare($trans, $QueryFarmText);
        $FarmResult = ibase_execute($FarmQuery);

        $outItems = array();

        while ($farmRow = ibase_fetch_row($FarmResult))
        {
            $item = new PriceListModuleRecordFarm;
            $item->farmID = $farmRow[0];
            $item->farmName = $farmRow[1];
            $item->farmGroup = $farmRow[2];
            $item->farmUnitofmeasure = $farmRow[3];
            $item->farmPrice = $farmRow[4];
            $item->farmQuantity = 1;
            $outItems[] = $item;
        }
        ibase_commit($trans);
        ibase_free_query($FarmQuery);
        ibase_free_result($FarmResult);
        $connection->CloseDBConnection();
        return $outItems;
    }
    // **************************************** FARM PROCEDURES END ************************************
    
    
	/** 									
 	* @param string $parentID 
    * @param string $searchFields 
    * @param string $searchText
    * @param int $searchType
    * @param int[] $searchSpecialities
	* @return PriceListModuleRecord[]
	*/ 
    public function getPriceListRecords($parentID, $searchFields, $searchText, $searchType, $searchSpecialities) //sqlite
    {
	   $connection = new Connection();
	   $connection->EstablishDBConnection();
	   $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $where=array();
        if(nonull($searchText))
        {
              $pFields="p.".str_replace("||' '||","||' '||p.",$searchFields);
              $where[]= " lower($pFields) like lower('%$searchText%')"; 
        }
		if ($searchType>-1)
		{
			$where[]= " p.PROCTYPE=$searchType";                  
		}
        if (count($searchSpecialities)>0)
		{
          $QuerySpec='';
		  foreach($searchSpecialities as $spec)
          {
            	if($QuerySpec!='')
                {
                    $QuerySpec.=' or ';
                }
                $QuerySpec .= "'|'||p.HEALTHTYPES||'|'||p.HEALTHTYPE||'|' like '%|$spec->id|%'";   
          }
		   	$where[]= " (".$QuerySpec." )";               
		}
        $whereStr="";
        if(count($where)>0)
        {
            $whereStr=implode(' and ',$where);
	
        	   $parentIDs= $this->getParrentIdsRecoursive($whereStr, $trans);
               
                if(count($parentIDs)>0)
                {
                     $whereStr =" and (p.id in (".implode(',',$parentIDs).") or (".$whereStr."))";
                }
                else
                {
                    $whereStr=" and ".$whereStr;
                }
           
        }
	   $functionResult = $this->getPriceListRecordsRecursive($trans, $parentID, 1, $whereStr);
	   ibase_commit($trans);
	   $connection->CloseDBConnection();
	   return $functionResult;
    }
    
    public function getParrentIdsRecoursive($whereStr, $trans) //sqlite
    {
        $QueryText = "
        		select  distinct
                    p.PARENTID
                from HEALTHPROC p where p.PARENTID is not null and ".$whereStr;
                
                
        		
                $query = ibase_prepare($trans, $QueryText);
        		$result = ibase_execute($query);
	
        	   $parentIDs=array();
        		while ($row = ibase_fetch_row($result))
        		{
        		    $parentIDs[]=$row[0];  
                    $parentIDs=array_merge($parentIDs, $this->getParrentIdsRecoursive(" p.id=$row[0]", $trans));
        		}
                
                ibase_free_query($query);
                ibase_free_result($result);
                
                return $parentIDs;
    }
    
    
   /**
 	* @param PriceListModuleRecord $priceListItem
    * @return object
 	*/
	public function updatePriceListRecord($priceListItem) 
	{
			$connection = new Connection();
			$connection->EstablishDBConnection();		
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            if (($priceListItem->SEQUINCENUMBER == "") || ($priceListItem->SEQUINCENUMBER == "null"))
			{
    			$maxsequenNumberQueryText = "select max(SEQUINCENUMBER) from HEALTHPROC where PARENTID";
                if (($priceListItem->PARENTID == "") || ($priceListItem->PARENTID == "null"))
    			{
    				$maxsequenNumberQueryText .= " is null"; 
    			}
                else
                {
                    $maxsequenNumberQueryText .= " = '$priceListItem->PARENTID'"; 
                }
                $maxsequenNumberQuery = ibase_prepare($trans, $maxsequenNumberQueryText);
    			$maxsequenNumberResult = ibase_execute($maxsequenNumberQuery);
    			if ($maxsequenNumberRow = ibase_fetch_row($maxsequenNumberResult))
    			{
    				$maxsequenNumber = $maxsequenNumberRow[0]+1;
    			}
    			ibase_free_query($maxsequenNumberQuery);	
    			ibase_free_result($maxsequenNumberResult);
            }
            else
            {
                $maxsequenNumber = $priceListItem->SEQUINCENUMBER;
            }	
            
            
			$QueryText = "
			update or insert into HEALTHPROC (
				ID,          
				PARENTID,    
				NODETYPE,    
				SHIFR,       
				NAME,        
				PLANNAME,    
				UOP,         
				PROCTYPE,    
				TOOTHUSE,    
				PRICE,       
				HEALTHTYPE,  
				PROCTIME,
                SEQUINCENUMBER,
                EXPENSES,
                DISCOUNT_FLAG,
                SALARY_SETTINGS,
                COMPLETEEXAMDIAG,
                HEALTHTYPES,
                name48,
                regcode,
                PRICE2,    
                PRICE3     
			) 
			values(";
			
			if (($priceListItem->ID == "") || ($priceListItem->ID == "null"))
			{
				$QueryText = $QueryText."null"; 
			}
			else 
			{ 
				$QueryText = $QueryText."$priceListItem->ID"; 
			}
			
			if (($priceListItem->PARENTID == "") || ($priceListItem->PARENTID == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->PARENTID"; 
			}
			
			if (($priceListItem->NODETYPE == "") || ($priceListItem->NODETYPE == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->NODETYPE"; 
			}
			
			$QueryText = $QueryText.", '".safequery($priceListItem->SHIFR)."'";
			
			if (($priceListItem->NAME == "") || ($priceListItem->NAME == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", '".safequery($priceListItem->NAME)."'"; 
			}
			
			if (($priceListItem->PLANNAME == "") || ($priceListItem->PLANNAME == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", '".safequery($priceListItem->PLANNAME)."'"; 
			}
			
			if (($priceListItem->UOP == "") || ($priceListItem->UOP == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->UOP"; 
			}
			
			if (($priceListItem->PROCTYPE == "") || ($priceListItem->PROCTYPE == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->PROCTYPE"; 
			}
			
			if (($priceListItem->TOOTHUSE == "") || ($priceListItem->TOOTHUSE == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->TOOTHUSE"; 
			}
			
			if (($priceListItem->PRICE == "") || ($priceListItem->PRICE == "null"))
			{
				$QueryText = $QueryText.", 0"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->PRICE"; 
			}
			
			if (($priceListItem->HEALTHTYPE == "") || ($priceListItem->HEALTHTYPE == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->HEALTHTYPE"; 
			}			
			
			if (($priceListItem->PROCTIME == "") || ($priceListItem->PROCTIME == "null"))
			{
				$QueryText = $QueryText.", null"; 
			}
			else 
			{ 
				$QueryText = $QueryText.", $priceListItem->PROCTIME"; 
			}
            
            $QueryText .= ", $maxsequenNumber";
            if(is_nan($priceListItem->EXPENSES))
			{ 
				$QueryText = $QueryText.", null"; 
			}
			else
			{ 
				$QueryText = $QueryText.", $priceListItem->EXPENSES";
			} 
            
            $QueryText .= ", $priceListItem->discount_flag";
            if(is_nan($priceListItem->salary_settings))
			{ 
				$QueryText = $QueryText.", null"; 
			}
			else
			{
				$QueryText = $QueryText.", $priceListItem->salary_settings";
			}
            
            if(($priceListItem->complete_examdiag != null) && ($priceListItem->complete_examdiag != ''))
			{ 
				$QueryText = $QueryText.", $priceListItem->complete_examdiag";
			}
			else
			{
				$QueryText = $QueryText.", null"; 
			}
            
            if(($priceListItem->HEALTHTYPES != null) && ($priceListItem->HEALTHTYPES != ''))
			{ 
				$QueryText = $QueryText.", '$priceListItem->HEALTHTYPES'";
			}
			else
			{
				$QueryText = $QueryText.", null"; 
			}
            if(($priceListItem->NAME48 != null) && ($priceListItem->NAME48 != ''))
			{ 
				$QueryText = $QueryText.", '".safequery($priceListItem->NAME48)."'";
			}
			else
			{
				$QueryText = $QueryText.", '".safequery(setTo48($priceListItem->NAME))."' "; 
			}
            
            if(($priceListItem->regCode != null) && ($priceListItem->regCode != ''))
			{ 
				$QueryText = $QueryText.", '$priceListItem->regCode'";
			}
			else
			{
				$QueryText = $QueryText.", null"; 
			}
            
            if(($priceListItem->PRICE2 != null) && ($priceListItem->PRICE2 != ''))
			{ 
				$QueryText = $QueryText.", '$priceListItem->PRICE2'";
			}
			else
			{
				$QueryText = $QueryText.", 0"; 
			}
            if(($priceListItem->PRICE3 != null) && ($priceListItem->PRICE3 != ''))
			{ 
				$QueryText = $QueryText.", '$priceListItem->PRICE3'";
			}
			else
			{
				$QueryText = $QueryText.", 0"; 
			}
            
            
			$QueryText .= ") RETURNING ID";	
            				
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$resultObj=new stdClass();
            $resultObj->ID=$priceListItem->ID;
			if ($row = ibase_fetch_row($result))
			{
				$resultObj->ID = $row[0];
			}
			ibase_free_query($query);	
			ibase_free_result($result);
            
            
            //insert farms when ADD prosedur
            if((($priceListItem->ID == "")||($priceListItem->ID == "null"))&&($priceListItem->NODETYPE == 1)&&($priceListItem->farms != null))//when adding new procedur
            {
                foreach ($priceListItem->farms as $itemFarm)
                {
                    $QueryText = "insert into HEALTHPROCFARM(ID, HEALTHPROC, FARM, QUANTITY)
						values(null, $resultObj->ID, $itemFarm->farmID, $itemFarm->farmQuantity)";
                    $query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
                    ibase_free_query($query);
                }
            }
            if($priceListItem->F39!=null)
            {
              $resultObj->F39=Form039Module::save039ProcValues(0,$resultObj->ID,$priceListItem->F39,$trans);  
            }
            
            	
            //$functionResult = $this->getPriceListRecordsRecursive($trans, "null", 1);		
			ibase_commit($trans);
			$connection->CloseDBConnection();	
			//return $functionResult;
			return $resultObj;
	}    
      
	/**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
	public function deletePriceListRecords($Arguments) 
	{
			$connection = new Connection(); 
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			for ($i=0; isset($Arguments[$i]); $i++)
			{ 
				$QueryText = "
				SELECT COUNT(*) FROM (select ID from PROTOCOLPROC where HEALTHPROC = $Arguments[$i])";
				$query = ibase_prepare($trans, $QueryText);	
				$result = ibase_execute($query);
				$row = ibase_fetch_row($result);
			}			
			
			$QueryText = "delete from HEALTHPROC where (ID is null)";
			for ($i=0; isset($Arguments[$i]); $i++)
			{ 
				$QueryText = $QueryText." or (ID=$Arguments[$i])"; 
			}
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			$success = ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
			return $success;		
	}
	
	
  
  
  
  	/**
	* @param PriceListModuleRecord[]
    * @return boolean
	*/ 
    public function updatePriceListRecords2($input)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $this->getQueryRecordsRecursive($input, $trans, null);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  
    }
    
    private function getQueryRecordsRecursive($subinput, $trans, $patID)
    {
        foreach ($subinput as $item) 
        {
            $QueryText = "update HEALTHPROC set SEQUINCENUMBER=$item->SEQUINCENUMBER, PARENTID ";
            if(($patID == "")||($patID=="null"))
            {
                $QueryText .= " = null";
            }
            else
            {
                $QueryText .= " = $patID";
            }
            $QueryText .= " WHERE id=$item->ID";
            $query = ibase_prepare($trans, $QueryText);
    	    $result = ibase_execute($query);
    		ibase_free_query($query);
            if($item->children != null)
            {
                $this->getQueryRecordsRecursive($item->children, $trans, $item->ID);
            }
        }
    }
    
    /**
	* @param string $healthproc_id
	* @param string $salary_settings
    * @return boolean
	*/ 
    public function updateSalarySettingsInTreatmentProcs($healthproc_id, $salary_settings)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update treatmentproc set treatmentproc.salary_settings = $salary_settings
                        where treatmentproc.healthproc_fk = $healthproc_id";
        $query = ibase_prepare($trans, $QueryText);
   	    $result = ibase_execute($query);
		ibase_free_query($query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  
    }
    
    /**
    * @return PriceListModuleRecordRegistrator[]
	*/ 
    public function GetRecordsForRegistrator()
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select healthproc.regcode,
                healthproc.name48
                from healthproc
                where healthproc.nodetype = 1";
        $RecordQuery = ibase_prepare($trans, $QueryText);
		$RecordResult = ibase_execute($RecordQuery);
                    
        $outItems = array();

        while ($recordRow = ibase_fetch_row($RecordResult))
        {
            $item = new PriceListModuleRecordRegistrator;
            $item->regCode = $recordRow[0];
            $item->NAME48 = $recordRow[1];
            $outItems[] = $item;
        }
        ibase_commit($trans);
        ibase_free_query($RecordQuery);
        ibase_free_result($RecordResult);
        $connection->CloseDBConnection();
        return $outItems;
  
    }
    
 
}
?>
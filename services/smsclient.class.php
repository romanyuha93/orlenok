<?php
/*
Copyright (c) 2009-2011 by Dmitry Skachko, AlphaSMS.com.ua
All rights reserved.

History:

Version 1.0 (10.06.2009)
 - First Release
 
Version 1.1 (24.09.2009)
 - Added send_date parameter
 - Added wap-push link
 - Added flash message 
 
Version 1.2 (31.10.2009)
- send_date format changed to DATE_ISO8601 - ISO-8601 (example: 2005-08-15T15:52:01+0000)
- added function hasErrors() to check number of errors
- added ability to auto-check for new version of PHP class

Version 1.3 (25.11.2009)
- fixed wrong time format, if no time set
- API moved to alphasms.com.ua
- class renamed

Version 1.4 (02.02.2010)
- Added choice to use HTTPS POST instead of HTTP GET (via variable $mode)

Version 1.5 (08.02.2010)
- Added function to convert text to translit

Version 1.6 (10.03.2010)
- Added function getResponse 
- Now server returs more data that can be accessed as array via new function

Version 1.7 (11.05.2010)
- Send datetime parameter for sendSMS function now can accept timestamp and date in text format 

Version 1.8 (5.06.2011)
- Added auth by API key 


*/
class SMSClient
{
    protected $_type='fly';
    //protected $_type='alpha';
    //protected $_type='prostor';
    //protected $_type='letsads';
    //protected $_type='devinotelecom';
    
    public $client;
    public function __construct($login = '', $password = '', $key = '')
	{
        if($this->_type=='fly')
        {
            $this->client=new SMSFlyClient($login, $password, $key);
        }
        else if($this->_type=='alpha')
        {
            $this->client=new AlphaSMSClient($login, $password, $key);
        }
        else if($this->_type=='letsads')
        {
            $this->client=new LetsAdsClient($login, $password, $key);
        }
        else if($this->_type=='prostor')
        {
            $this->client=new ProstorSMSClient($login, $password, $key);
        }
        else if($this->_type=='devinotelecom')
        {
            $this->client=new DevinoTelecomClient($login, $password, $key);
        }
	}
    
    public function sendSMS($recipients, $body, $source, $desc='', $rate = 120, $livetime = 24, $start_time = 'AUTO', $end_time = 'AUTO')
	{
        if($this->_type=='fly')
        {
            $res=$this->client->sendSMS($recipients, $body, $source, $desc, $rate, $livetime, $start_time, $end_time);
            return  $res['states'][0]['campaignID'];
        }
        else if($this->_type=='alpha')
        {
            return $this->client->sendSMS($source, $recipients[0], $body);
        }
        else if($this->_type=='letsads')
        {
            $res=$this->client->sendSMS($recipients, $body, $source, $desc, $rate, $livetime, $start_time, $end_time);
            return  $res[0];
        }
        else if($this->_type=='prostor')
        {
           return $this->client->sendSMS($source, $recipients[0], $body);
        }
        else if($this->_type=='devinotelecom')
        {
           return $this->client->sendSMS($source, $recipients[0], $body);
        }
	}
	
    public function receiveSMS($recipient, $campaignID)
	{
        if($this->_type=='fly')
        {
            $res=$this->client->receiveSMS($recipient, $campaignID);
            return $res['states'][0]['status'];
        }
        else if($this->_type=='alpha'||$this->_type=='letsads'||$this->_type=='prostor'||$this->_type=='devinotelecom')
        {
            return $this->client->receiveSMS($campaignID);
        }
	}
    
   	public function getBalance()
	{
        return doubleval($this->client->getBalance());//*($this->_type=='fly'?2.5:1);
	}
	
	//OUT:	returns number of errors
	public function hasErrors()
	{
		return $this->client->hasErrors();
	}
	
	//OUT:	returns array of errors
	public function getErrors()
	{
		return $this->client->getErrors();
	}
    
    public function getErrorString()
    {
        return $this->client->getErrorString();
    }

	public function getResponse()
	{
		return $this->client->getResponse();
	}
    
    public static function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}	
}

class AlphaSMSClient
{
	public $mode = 'HTTPS'; //HTTP or HTTPS
	protected $_server = '://alphasms.com.ua/api/http.php';
	protected $_errors = array();
	protected $_last_response = array();
	private $_version = '1.8';
	
	
	//IN: login and password or key on platform (AlphaSMS)
	public function __construct($login = '', $password = '', $key = '')
	{
		$this->_login = $login;
		$this->_password = $password;
		$this->_key = $key;
	}

	//IN: 	sender name, phone of receiver, text message in UTF-8 - if long - will be auto split
	//		send_dt - date-time of sms sending, wap - url for Wap-Push link, flash - for Flash sms.
	//OUT: 	message_id to track delivery status, if empty message_id - check errors via $this->getErrors()
	public function sendSMS($from, $to, $message, $send_dt = 0, $wap = '', $flash = 0)
	{
		if(!$send_dt)
			$send_dt = date('Y-m-d H:i:s');
		$d = is_numeric($send_dt) ? $send_dt : strtotime($send_dt);
		$data = array(	'from'=>$from,
						'to'=>$to,
						'message'=>$message,
						'ask_date'=>date(DATE_ISO8601, $d),
						'wap'=>$wap,
						'flash'=>$flash,
						'class_version'=>$this->_version);
		$result = $this->execute('send', $data);
		if(count(@$result['errors']))
			$this->_errors = $result['errors'];
		return @$result['id'];
	}
	
	//IN: 	message_id to track delivery status
	//OUT: 	text name of status
	public function receiveSMS($sms_id)
	{
		$data = array('id'=>$sms_id);
		$result = $this->execute('receive', $data);
		if(count(@$result['errors']))
			$this->_errors = $result['errors'];
             
		return @$result['code'];		
	}

	//OUT:	amount in UAH, if no return - check errors
	public function getBalance()
	{
		$result = $this->execute('balance');
		if(count(@$result['errors']))
			$this->_errors = $result['errors'];
		return @$result['balance'];		
	}
	
	//OUT:	returns number of errors
	public function hasErrors()
	{
		return count($this->_errors);
	}
	
	//OUT:	returns array of errors
	public function getErrors()
	{
		return $this->_errors;
	}
    
    public function getErrorString()
    {
        $str = "";
        foreach($this->_errors as $er)
            $str .= $er . "\n";
        return trim($str);
    }

	public function getResponse()
	{
		return $this->_last_response;
	}

	public function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}	

	protected function execute($command, $params = array())
	{
		$this->_errors = array();

		//HTTP GET
		if(strtolower($this->mode) == 'http')
		{
			$response = @file_get_contents($this->generateUrl($command, $params));
			return @unserialize($this->base64_url_decode($response));
		}
		else
		{
			$params['login'] = $this->_login;
			$params['password'] = $this->_password;
			$params['key'] = $this->_key;
			$params['command'] = $command;
			$params_url = '';
			foreach($params as $key=>$value)
		 		$params_url .= '&' . $key . '=' . $this->base64_url_encode($value);
		
			//cURL HTTPS POST
			$ch = curl_init(strtolower($this->mode) . $this->_server);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params_url);			
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);			
			$response = @curl_exec($ch);
			curl_close($ch);

			$this->_last_response = @unserialize($this->base64_url_decode($response));
            if ($this->_last_response xor $response)
                $this->_errors[] = $response;
			return $this->_last_response;		
		}
	}
	
	protected function generateUrl($command, $params = array())
	{
		$params_url = '';
		if(count($params))
			foreach($params as $key=>$value)
		 		$params_url .= '&' . $key . '=' . $this->base64_url_encode($value);
		if(!$this->_key) { 		
			$auth = '?login=' . $this->base64_url_encode($this->_login) . '&password=' . $this->base64_url_encode($this->_password);
		} else {
			$auth = '?key=' . $this->base64_url_encode($this->_key);
		}
		$command = '&command=' . $this->base64_url_encode($command);
		return strtolower($this->mode) . $this->_server . $auth . $command . $params_url;
	}

	public function base64_url_encode($input)
	{
		return strtr(base64_encode($input), '+/=', '-_,');
	}
	
	public function base64_url_decode($input)
	{
		return base64_decode(strtr($input, '-_,', '+/='));
	}

}

class SMSFlyClient
{
	public $mode = 'HTTP'; //HTTP or HTTPS
	protected $_server = '://sms-fly.com/api/api.php';
	protected $_errors = array();
	protected $_last_response = '';
	private $_version = '1.8';
	
	
	//IN: login and password or key on platform (AlphaSMS)
	public function __construct($login = '', $password = '', $key = '')
	{
		$this->_login = $login;
		$this->_password = $password;
		$this->_key = $key;
	}

   /** 
 * @param string[] $recipients
 * номера получателей в международном формате без разделителей (пример: 380631234567)
 * @param string $body
 * текст смс-сообщения в кодировке utf-8
 * @param string $source
 * отправитель. Задает альфанумерическое имя (альфаимя). 
 * Допускаются только альфанумерические имена, зарегистрированные для пользователя
 * @param string $desc
 * описание рассылки (отображается в веб интерфейсе) в кодировке utf-8
 * @param int $rate
 * скорость отправки сообщения(й) в количестве сообщений в минуту. 
 * Допускаются только целые значения в диапазоне от 1 до 120.
 * @param int $livetime
 * срок жизни сообщения(й) в часах.  
 * Допускаются только целые значения в диапазоне от 1 до 24
 * @param string $start_time 
 * время начала отправки сообщения(й) ,  
 * в формате YYYY-MM-DD HH:MM:SS и не может быть временем в прошлом. 
 * Система допускает поправку времени в 5 минут.  
 * (формат для PHP “Y-m-d H:i:s”). 
 * Можно использовать значение AUTO для отправки немедленно.
 * @param string $end_time
 * время окончания отправки сообщения(й),  
 * в формате YYYY-MM-DD HH:MM:SS и не может быть раньше времени начала отправки. 
 * (формат для PHP “Y-m-d H:i:s”). 
 * Можно использовать значение AUTO для автоматического расчета времени системой.
 * @return array
 */ 
	public function sendSMS($recipients, $body, $source, $desc='', $rate = 120, $livetime = 24, $start_time = 'AUTO', $end_time = 'AUTO')
	{
	   $recipientStr='';
	   foreach($recipients as $recipient)
       {
        $recipientStr.='<recipient>'.$recipient.'</recipient>';
       }
        $xmlStr='<?xml version="1.0" encoding="utf-8"?>
                    <request>
                        <operation>SENDSMS</operation>
                        <message start_time="'.$start_time.'" end_time="'.$end_time.'" livetime="'.$livetime.'" rate="'.$rate.'" desc="'.$this->translit($desc).'" source="'.$source.'">
                        <body>'.$body.'</body> 
                       '.$recipientStr.'
                    </message>
                </request>';
		$result = $this->execute($xmlStr);
        $xml= new SimpleXMLElement($result);
        $res=array();
        foreach($xml->state as $state)
        {
            switch($state->attributes()->code)
            {
                case 'ACCEPT':
                    $res['states'][]=array('campaignID'=>$state->attributes()->campaignID,'date'=>$state->attributes()->date, 'text'=>$state);
                    break;
                default:
                    $res['errors'][]=array('code'=>$state->attributes()->code,'date'=>$state->attributes()->date, 'text'=>$state);
                    break;
            }
        }
        
        foreach($xml->to as $to)
        {
            $res['to'][]=array('recipient'=>$to->attributes()->recipient,'status'=>$to->attributes()->status);
        }
		
        $this->_errors = $res['errors'];
		return $res;
	}
    
	 /** 
 * @param string $recipient
 * номер получателя в международном формате без разделителей (пример: 380631234567)
 * @param string $campaignID
 * идентификатор рассылки в системе
 * @return array
 */ 
	public function receiveSMS($recipient, $campaignID)
	{
	   $recipientStr='';
        $recipientStr.='<recipient>'.$recipient.'</recipient>';
       
        $xmlStr='<?xml version="1.0" encoding="utf-8"?>
                    <request>
                        <operation>GETMESSAGESTATUS</operation>
                        <message campaignID="'.$campaignID.'" recipient="'.$recipient.'" />
                    </message>
                </request>';
		$result = $this->execute($xmlStr);
        $xml= new SimpleXMLElement($result);
        $res=array();
        foreach($xml->state as $state)
        {
            $res['states'][]=array('campaignID'=>$state->attributes()->campaignID,'recipient'=>$state->attributes()->recipient,'date'=>$state->attributes()->date, 'status'=>$state->attributes()->status);
        }
        
		return $res;
	}

 /** 
 * @return string
 */ 
	public function getBalance()
	{
        $xmlStr='<?xml version="1.0" encoding="utf-8"?>
                    <request>
                        <operation>GETBALANCE</operation>
                    </message>
                </request>';
        $result = $this->execute($xmlStr);
        
        $xml= new SimpleXMLElement($result);

        $res=$xml->balance;
		return $res;
	}
	
	//OUT:	returns number of errors
	public function hasErrors()
	{
		return count($this->_errors);
	}
	
	//OUT:	returns array of errors
	public function getErrors()
	{
		return $this->_errors;
	}
    
    public function getErrorString()
    {
        $str = "";
        foreach($this->_errors as $er)
        {
            $str .= $er['date'].' - '.$er['code'].' : '.$er['text']. "\n";
        }
            
        return trim($str);
    }

	public function getResponse()
	{
		return $this->_last_response;
	}

	public function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}	

	protected function execute($xml)
	{
		$this->_errors = array();

		  $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERPWD , 	$this->_login.':'.$this->_password);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_URL, strtolower($this->mode) . $this->_server);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Accept: text/xml"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            $response = curl_exec($ch);
            curl_close($ch);

			$this->_last_response = $response;
			return $this->_last_response;
	}

}

class LetsAdsClient
{
	public $mode = 'HTTP'; //HTTP or HTTPS
	protected $_server = '://letsads.com/api';
	protected $_errors = array();
	protected $_last_response = '';
	private $_version = '1.8';
	
	
	//IN: login and password or key on platform (AlphaSMS)
	public function __construct($login = '', $password = '', $key = '')
	{
		$this->_login = $login;
		$this->_password = $password;
		$this->_key = $key;
	}

   /** 
 * @param string[] $recipients
 * номера получателей в международном формате без разделителей (пример: 380631234567)
 * @param string $body
 * текст смс-сообщения в кодировке utf-8
 * @param string $source
 * отправитель. Задает альфанумерическое имя (альфаимя). 
 * Допускаются только альфанумерические имена, зарегистрированные для пользователя
 * @param string $desc
 * описание рассылки (отображается в веб интерфейсе) в кодировке utf-8
 * @param int $rate
 * скорость отправки сообщения(й) в количестве сообщений в минуту. 
 * Допускаются только целые значения в диапазоне от 1 до 120.
 * @param int $livetime
 * срок жизни сообщения(й) в часах.  
 * Допускаются только целые значения в диапазоне от 1 до 24
 * @param string $start_time 
 * время начала отправки сообщения(й) ,  
 * в формате YYYY-MM-DD HH:MM:SS и не может быть временем в прошлом. 
 * Система допускает поправку времени в 5 минут.  
 * (формат для PHP “Y-m-d H:i:s”). 
 * Можно использовать значение AUTO для отправки немедленно.
 * @param string $end_time
 * время окончания отправки сообщения(й),  
 * в формате YYYY-MM-DD HH:MM:SS и не может быть раньше времени начала отправки. 
 * (формат для PHP “Y-m-d H:i:s”). 
 * Можно использовать значение AUTO для автоматического расчета времени системой.
 * @return array
 */ 
	public function sendSMS($recipients, $body, $source, $desc='', $rate = 120, $livetime = 24, $start_time = 'AUTO', $end_time = 'AUTO')
	{
	   $recipientStr='';
	   foreach($recipients as $recipient)
       {
        $recipientStr.='<recipient>'.$recipient.'</recipient>';
       }
        $xmlStr='<?xml version="1.0" encoding="utf-8"?>
            <request>
                <auth>
                    <login>'.$this->_login.'</login>
                    <password>'.$this->_password.'</password>
                </auth>
                <message>
                    <from>'.$source.'</from>
                    <text>'.$body.'</text>
                    '.$recipientStr.'
                </message>   
        </request>';
//         file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($xmlStr, true));
		$result = $this->execute($xmlStr);
        $xml= new SimpleXMLElement($result);
        $res=$xml->sms_id;
        if($xml->name!='Complete')
        {
           $this->_errors = $res->description; 
        }		
        
		return $res;
	}
    
	 /** 
 * @param string $recipient
 * номер получателя в международном формате без разделителей (пример: 380631234567)
 * @param string $campaignID
 * идентификатор рассылки в системе
 * @return array
 */ 
	public function receiveSMS($campaignID)
	{
	   $recipientStr='';
              
       $xmlStr='<?xml version="1.0" encoding="utf-8"?>
            <request>
                <auth>
                    <login>'.$this->_login.'</login>
                    <password>'.$this->_password.'</password>
                </auth>
              <sms_id>'.$campaignID.'</sms_id>
        </request>';
        
		$result = $this->execute($xmlStr);
        $xml= new SimpleXMLElement($result);

        $res=$xml->description;
        
		return $res;
	}

	 /** 
 * @return string
 */ 
	public function getBalance()
	{
        $xmlStr='<?xml version="1.0" encoding="utf-8"?>
                    <request>
                        <auth>
                            <login>'.$this->_login.'</login>
                            <password>'.$this->_password.'</password>
                        </auth>
                     <balance />
                </request>';
        $result = $this->execute($xmlStr);

        $xml= new SimpleXMLElement($result);
        $res=$xml->description;
		return $res;
	}
	
	//OUT:	returns number of errors
	public function hasErrors()
	{
		return count($this->_errors);
	}
	
	//OUT:	returns array of errors
	public function getErrors()
	{
		return $this->_errors;
	}
    
    public function getErrorString()
    {
        $str = "";
        foreach($this->_errors as $er)
        {
            $str .= $er."\n";
        }
            
        return trim($str);
    }

	public function getResponse()
	{
		return $this->_last_response;
	}

	public function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}	

	protected function execute($xml)
	{
		$this->_errors = array();

		  $ch = curl_init(strtolower($this->mode).$this->_server);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            $response = curl_exec($ch);
            curl_close($ch);
// file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($xml, true)."\n".var_export($response, true));
			$this->_last_response = $response;
			return $this->_last_response;
	}

}
class ProstorSMSClient
{
	public $mode = 'HTTP'; //HTTP or HTTPS
	protected $_server = '://api.prostor-sms.ru/messages/v2/';
	protected $_errors = array();
	protected $_last_response = array();
	private $_version = '2';
	
	
	//IN: login and password or key on platform (AlphaSMS)
	public function __construct($login = '', $password = '', $key = '')
	{
		$this->_login = $login;
		$this->_password = $password;
		$this->_key = $key;
	}

	//IN: 	sender name, phone of receiver, text message in UTF-8 - if long - will be auto split
	//		send_dt - date-time of sms sending, wap - url for Wap-Push link, flash - for Flash sms.
	//OUT: 	message_id to track delivery status, if empty message_id - check errors via $this->getErrors()
	public function sendSMS($from, $to, $message, $send_dt = 0, $wap = '', $flash = 0)
	{
		if(!$send_dt)
			$send_dt = date('Y-m-d H:i:s');
		$d = is_numeric($send_dt) ? $send_dt : strtotime($send_dt);
        $dt=date(DATE_ISO8601, $d);
        $dt=substr($dt,0,strlen($dt)-5).'Z';                
		$data = array(	'phone'=>$to,
						'text'=>$message
                        //,'scheduleTime'=>$dt
                        );
        if($from!='')
        {
            $data['sender']=$from;
        }
		$result = $this->execute('send', $data);
        $arr=explode(';',$result);

        if($arr[0]=='error')
        {
       	   $this->_errors[]=$arr[1];
        }
        return $arr[1];
	
	}
	
	//IN: 	message_id to track delivery status
	//OUT: 	text name of status
	public function receiveSMS($sms_id)
	{
		$data = array('id'=>$sms_id);
		$result = $this->execute('status', $data);
        $arr=explode(';',$result);
        
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($arr, true));
        if($arr[0]=='error')
        {
       	   $this->_errors[]=$arr[1];
        }
        return $arr[1];		
	}

	//OUT:	amount in UAH, if no return - check errors
	public function getBalance()
	{
		$result = $this->execute('balance');
        $arr=explode(';',$result);
        if($arr[0]=='error')
        {
       	   $this->_errors[]=$arr[1];
        }
//        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($arr, true));
		return $arr[1];		
	}
	
	//OUT:	returns number of errors
	public function hasErrors()
	{
		return count($this->_errors);
	}
	
	//OUT:	returns array of errors
	public function getErrors()
	{
		return $this->_errors;
	}
    
    public function getErrorString()
    {
        $str = "";
        foreach($this->_errors as $er)
            $str .= $er . "\n";
        return trim($str);
    }

	public function getResponse()
	{
		return $this->_last_response;
	}

	public function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}	

	protected function execute($command, $params = array())
	{
		$this->_errors = array();

		//HTTP GET
			$params['login'] = $this->_login;
			$params['password'] = $this->_password;
			$params_url = '';
			foreach($params as $key=>$value)
		 		$params_url .= '&' . $key . '=' . $value;


			$url=strtolower($this->mode).$this->_server.$command.'?'.$params_url;	

			$this->_last_response = file_get_contents($url);
           
			return $this->_last_response;		
		
	}
}

class DevinoTelecomClient
{
	public $mode = 'HTTPS'; //HTTP or HTTPS
	protected $_server = '://integrationapi.net/rest';
	protected $_errors = array();
	protected $_last_response = '';
	private $_version = '1.1';
	
	//IN: login and password or key on platform (AlphaSMS)
	public function __construct($login = '', $password = '', $key = '')
	{
		$this->_login = $login;
		$this->_password = $password;
		$this->_key = $key;
 	      $this->_session_id = "none";
	}

	//IN: 	sender name, phone of receiver, text message in UTF-8 - if long - will be auto split
	//		send_dt - date-time of sms sending, wap - url for Wap-Push link, flash - for Flash sms.
	//OUT: 	message_id to track delivery status, if empty message_id - check errors via $this->getErrors()
	public function sendSMS($from, $to, $message, $send_dt = 0, $wap = '', $flash = 0)
	{
     
		$data = array(	'DestinationAddress'=>$to,
						'Data'=>$message
                        );
        if($from!='')
        {
            $data['SourceAddress']=$from;
        }
		$result = $this->execute('/Sms/Send', $data,"POST");
        
        $obj=json_decode($result);
        if(is_array($obj)==false)
        {
            $this->_errors[]=$result;
            return $obj;
        }
        return $obj[0];
	
	}
	
	//IN: 	message_id to track delivery status
	//OUT: 	text name of status
	public function receiveSMS($sms_id)
	{
		$data = array('messageId'=>urlencode($sms_id));
		$result = $this->execute('/Sms/State', $data);
        
        $obj=json_decode($result);
        if(isset($obj->State)==false)
        {
            $this->_errors[]=$result;
            return null;
        }
        return DevinoTelecomClient::convertState($obj->State);		
	}

    public static function convertState($state)
    {
         switch ($state)
        {
            case -1: return "2"; //Отправлено:;
            case 0: return "3";//Доставлено
            case -2: return "0";//В очереди
            case -98: return "1";//Остановлено
            case 46: return "5";//Просрочено
            case 69: return "50";//Отклонено
            case 48: return "96";;//Отклонено платформой
            case 11: return "99";//Неверный адрес получателя
            default: return "-99";
        }
    }
	//OUT:	amount in UAH, if no return - check errors
	public function getBalance()
	{
		$result = $this->execute('/User/Balance');
        $arr=$result;
  
//        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($arr, true));
		return $arr;		
	}
	
	//OUT:	returns number of errors
	public function hasErrors()
	{
		return count($this->_errors);
	}
	
	//OUT:	returns array of errors
	public function getErrors()
	{
		return $this->_errors;
	}
    
    public function getErrorString()
    {
        $str = "";
        foreach($this->_errors as $er)
            $str .= $er . "\n";
        return trim($str);
    }

	public function getResponse()
	{
		return $this->_last_response;
	}

	public function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}	
    private $_session_id = "none";
	protected function execute($command, $params = array(), $method='GET')
	{
            if($this->_session_id=="none")
            {
               $paramsLogin = array();
               $paramsLogin['login'] = $this->_login;
               $paramsLogin['password'] = $this->_password;
               $this->_session_id="";
               $res=$this->execute('/user/sessionid',$paramsLogin);
               $this->_session_id=substr($res,1,strlen($res)-2);
               return  $this->execute($command, $params, $method);        
            }
            else
            {
               if($this->_session_id!="")
               {
                    $params['SessionID'] = $this->_session_id;
                    
               }

    		  $this->_errors = array();
    		
    			$params_url = '';
    			foreach($params as $key=>$value)
                {
                    if($params_url!='')
                    {
                        $params_url .= '&' ;
                    }
                    $params_url .= $key . '=' . $value;
                }
                if($method=="GET")
                {
                   $url=strtolower($this->mode).$this->_server.$command.'?'.$params_url;	
                    //file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt")."\n url=".$url);
    	           $this->_last_response = file_get_contents($url);
                   // file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt")."\n resp=".$this->_last_response); 
                }
                else
                {
                    $ch = curl_init(strtolower($this->mode) . $this->_server.$command);
        			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        			curl_setopt($ch, CURLOPT_POST, count($params));
        			curl_setopt($ch, CURLOPT_POSTFIELDS, $params_url);
          	         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          	         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);			
                    curl_setopt($ch, CURLOPT_MAXREDIRS, $params_url);													
        			$this->_last_response = @curl_exec($ch);
        			curl_close($ch);
                }
                
			     
    		//	
    			return $this->_last_response;	
            }	
		
	}

}
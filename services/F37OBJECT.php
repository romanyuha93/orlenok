<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
//$f37=new F37OBJECT(1,"2013-06-19");
//print_r($f37);
class F37OBJECT
{	
   /**
	* @var PrintDataModule_DoctorData
	*/
   var $DOCTOR_DATA;
   
   /**
	* @var PrintDataModule_DateUA
	*/
   var $DATE;
   
      /**
	* @var F37OBJECT_Worktime
	*/
   var $WORKTIME;
   
    /**
    * @var PrintDataModule_ClinicRegistrationData
    */
    var $CLINIC_DATA;
    
      /**
    * @var F37OBJECT_TableDataRecord
    */
    var $TABLE_DATA; 
    
    function __construct($doctor_id, $date, $trans=null)     
    { 
        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        $work_time=0;
        $this->DOCTOR_DATA=new PrintDataModule_DoctorData($doctor_id,$trans);
        $this->DATE=new PrintDataModule_DateUA($date);
        $this->CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(false,$trans);
        $this->TABLE_DATA=array();        
        $QueryText="select
                     t.beginoftheinterval,
                     t.endoftheinterval,
                     tp.patient_fk,
                     min((select count(distinct(cast(tp2.dateclose as Date)))+1 from treatmentproc tp2 where cast(tp2.dateclose as Date)<cast(tp.dateclose as Date) and tp2.patient_fk=tp.patient_fk)),
                     sum(tp.uop),
                     min((select cast(list(distinct d2.id,';|;') AS varchar(8000)) from diagnos2 d2
                        left join treatmentcourse tc on (tc.id=d2.treatmentcourse)
                        where d2.doctor=tp.doctor and
                        tc.patient_fk=tp.patient_fk and
                        cast(d2.diagnosdate as Date)=cast(tp.dateclose as Date))),
                     min((select cast(list(ta.additiontxt,'; ') AS varchar(8000)) from treatmentadditions ta
                        left join treatmentcourse tc on (tc.id=ta.treatmentcourse)
                        where ta.doctor=tp.doctor and
                        ta.additiontype=3 and 
                        tc.patient_fk=tp.patient_fk and
                        cast(ta.signdate as Date)=cast(tp.dateclose as Date)))
                     from treatmentproc tp
                        left join tasks t on (t.taskdate=cast(tp.dateclose as Date) and t.patientid=tp.patient_fk and t.doctorid=tp.doctor)
                     where cast(tp.dateclose as Date)='$date'
                     and tp.doctor=$doctor_id
                     group by
                            tp.patient_fk,
                            t.beginoftheinterval,
                            t.endoftheinterval
                     order by t.beginoftheinterval";
 			
            $query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		
			while($row = ibase_fetch_row($result))
            {
                $rec=new F37OBJECT_TableDataRecord;
                $rec->TASK_TIME=substr($row[0],0,5).'-'.substr($row[1],0,5);
                $work_time+=(strtotime($date.' '.$row[1])-strtotime($date.' '.$row[0]));
                $rec->PATIENT_DATA=new PrintDataModule_PatientData($row[2],$trans);
                $rec->PATIENT_DATA->AGE=bcdiv(bcdiv(strtotime($date)-strtotime($rec->PATIENT_DATA->BIRTHDAYDATE), 2628000), 12);
                $rec->PATIENT_CURSEORDER=$row[3];
                $rec->DOCTOR_UOP=$row[4];
                $diags=explode(';|;',$row[5]);
                $d2s=array();
                foreach($diags as $diag)
                {
                    if(nonull($diag))
                    {
                        $d2=new F37OBJECT_Diagnos2($diag,$trans);
                        // file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($d2,true));
                        $d2s[]=nonull($d2->F43SIGN)?($d2->TREATMENT_OBJECTS.' - '.$d2->F43SIGN):$d2->PROTOCOL_NAME;
                    }      
                }
                $rec->DIAGNOS_TEXT=implode('; ',$d2s);
                $rec->COMPLEX_TEXT=$row[6];
                $rec->ANALGESIC=F37OBJECT::getAnalgesic($doctor_id,$row[2],$date,$trans);
                $this->TABLE_DATA[]=$rec;                
            }
           	ibase_free_query($query);
        	ibase_free_result($result);
            
            $this->WORKTIME=new F37OBJECT_Worktime($work_time);


        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }  
    }
    
    public static function getAnalgesic($doctor_id, $patient_id, $date, $trans=null)
    {
                if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        

        $QueryText="select f.description, sum(tp.proc_count) from f39 f
                                left join f39tproc ftp on (ftp.f39id=f.id and ftp.unsigned=0)
                                left join f39 fp on (fp.id=f.parrent)
                                left join treatmentproc tp on (tp.id=ftp.treatmentprocid and tp.doctor=$doctor_id 
                                                                and cast(tp.dateclose as Date)='$date' and tp.patient_fk=$patient_id)
                                where
                                fp.number=11
                                and f.form=370
                                and f.id is not null
                                group by f.id,f.description";
 			
            $query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
            $res=array();	
            
			while($row = ibase_fetch_row($result))
            {
                $res[]=(nonull($row[1])&&$row[1]>0)?$row[0]:' ';
            }
           	ibase_free_query($query);
        	ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res;
    }  
}

class F37OBJECT_TableDataRecord
{
    /**
	* @var string
	*/
   var $TASK_TIME;
   
   /**
	* @var PrintDataModule_PatientData
	*/
   var $PATIENT_DATA;
    
   /**
	* @var string
	*/
   var $DIAGNOS_TEXT;   
   
   /**
	* @var string
	*/
   var $COMPLEX_TEXT;
   
   /**
	* @var string[]
	*/
   var $ANALGESIC;
   
   /**
	* @var string
	*/
   var $PATIENT_CURSEORDER;
   
   /**
	* @var double
	*/
   var $DOCTOR_UOP;
 
}

class F37OBJECT_Worktime
{
    /**
	* @var int
	*/
   var $TIME;
   
    /**
	* @var string
	*/
   var $HOURS;
   
   /**
	* @var string
	*/
   var $MINUTES;
   
    
    function __construct($time) 
    {
        $this->TIME = $time;
        $this->HOURS = (int) $time/3600;
        $this->MINUTES = round(($time-($this->HOURS*3600))/60);
    }
}
class F37OBJECT_Diagnos2
{
    /**
	* @var string
	*/
   var $ID;
   
    /**
	* @var string
	*/
   var $F43SIGN;
   
   /**
	* @var string
	*/
   var $PROTOCOL_NAME;
   
      /**
	* @var string
	*/
   var $TREATMENT_OBJECTS;
   
    
    function __construct($id, $trans) 
    {
        if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        $this->ID=$id;        
        $this->TREATMENT_OBJECTS=getTreatmentObjects($id,$trans);
        $QueryText="select f.sign, p.name from diagnos2 d2
                        left join protocols p on (p.id=d2.protocol_fk)
                        left join f43 f on (f.f43=d2.f43 and f.f43<>0)
                        where d2.id=$id";
 			
            $query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
					
			if($row = ibase_fetch_row($result))
            {
                $this->F43SIGN=$row[0];
                $this->PROTOCOL_NAME=$row[1];
            }
            
           	ibase_free_query($query);
        	ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        } 
    }
}
?>

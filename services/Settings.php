<?php
	class Settings
	{	
		const GMT_STRING = 'GMT+0300';
		const SERVICES_VERSION_STRING = '983';
		
		const SMS_COST = 0;//deprecated
		const LOCATION = "uk_UA";
		const SYNC_URL = "http://server2.vikisoft.com.ua:8082/toothfairysync";//-1 off
        
        const FILE_STORAGE='../../../storage';//LOCAL
        //const FILE_STORAGE='storage';//SERVER
        
        const DICOM_PACS_URL='82.117.240.50:8042';
        
        
        //exports
        const EXPORT_DOCTORSTREAT_FILE="C:/ToothFairyServer/att_log/doctors.csv";
        const EXPORT_ACCOUNT_FILE="C:/ToothFairyServer/att_log/account.csv";
        
        
        const GEOCODING_URL='http://server2.vikisoft.com.ua/toothfairyhost';
        const EHEALTH_URL='http://demo.ehealth.world';//http://demo.ehealth.world';
        const EHEALTH_REDIRECT_URL='http://193.106.59.101';//http://demo.ehealth.world';
	}
?>
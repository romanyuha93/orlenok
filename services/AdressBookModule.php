<?php

require_once 'Connection.php';
require_once "Utils.php";

class AdressBookModuleCountrys
{				
	   
	/**
    * @var string
    */
    var $ID;
    
   	/**
    * @var string
    */
    var $NUM;
    
   	/**
    * @var string
    */
    var $CHARCODE;
    
	/**
    * @var string
    */
    var $NAME;
    
    /**
    * @var bool
    */
    var $DELCOUNTRY;    
 }

class AdressBookModuleStates
{
   	/**
    * @var string
    */
    var $COUNTRY;
    
	/**
    * @var string
    */
    var $STATE;
    
	/**
    * @var string
    */
    var $NAME;
    /**
    * @var bool
    */
    var $DELSTATE;        
}

class AdressBookModuleRegions
{
	/**
    * @var string
    */
    var $NUM;
    
	/**
    * @var string
    */
    var $NAME;
    
	/**
    * @var string
    */
    var $STATE;
    
	/**
    * @var string
    */
    var $COUNTRY;
    
    /**
    * @var bool
    */
    var $DELREGION;                
}

class AdressBookModuleCitys
{
	/**
    * @var string
    */
    var $ID;
    
	/**
    * @var string
    */
    var $NAME;
       
    /**
    * @var string
    */
    var $COUNTRY; 
    
	/**
    * @var string
    */
    var $STATE;
    
    /**
    * @var string
    */
    var $REGION;
    
    /**
    * @var bool
    */
    var $DELCITY;
}

class AdressBookModule
{		
     /** 
 	*  @return AdressBookModuleCountrys[]
	*/ 	
     public function getCountrys() 
     { 
	$connection = new Connection();
    	$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText = 
            "select ID, NUM, CHARCODE, NAME from COUNTRYS order by ID";
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
    	$rows = array();//ñîçäàåì ìàññèâ ðåçóëüòàòîâ
        while ($row = ibase_fetch_row($result))//ïðîõîäèì ïî âñåì àññîöèèðîâàííûì ðÿäàì ðåçóëüòàòà èç ÁÄ
    	{
    	    $obj = new AdressBookModuleCountrys;//ñîçäàåì íîâûé îäèí îáúåêò PeopleExampleModuleOutput
            
            $obj->ID = $row[0];// òàê êàê â íàøåì çàïðîñå "select FNAME, LNAME, ID from PEOPLE" 
                                      //ñíà÷àëà â âûáîðêå ïîëó÷àåì FNAME òî â $row[0] áóäóò äàííûå ïîëÿ FNAME
                                       
            $obj->NUM = $row[1];// òàê êàê â íàøåì çàïðîñå "select FNAME, LNAME, ID from PEOPLE" 
                                     //ñíà÷àëà â âûáîðêå ïîëó÷àåì LNAME òî â $row[1] áóäóò äàííûå ïîëÿ LNAME
                                      
    		$obj->CHARCODE = $row[2];// òàê êàê â íàøåì çàïðîñå "select FNAME, LNAME, ID from PEOPLE" 
                               //ñíà÷àëà â âûáîðêå ïîëó÷àåì ID òî â $row[2] áóäóò äàííûå ïîëÿ ID
     	    
            $obj->NAME = $row[3];// òàê êàê â íàøåì çàïðîñå "select FNAME, LNAME, ID from PEOPLE" 
                               //ñíà÷àëà â âûáîðêå ïîëó÷àåì ID òî â $row[2] áóäóò äàííûå ïîëÿ ID
    	    
            $rows[] = $obj;//çàïèñûâàåì íàø îáúåêò $obj òèïà Output â ìàññèâ ðåçóëüòàòîâ
        }	
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
    	$connection->CloseDBConnection();		
    	return $rows;//âîçâðàùàåì ìàññèâ òèïà PeopleExampleModuleOutput[] ñ îáúåêòàìè òèïà PeopleExampleModuleOutput 
    }
        
    /**
    * @param AdressBookModuleCountrys $item, 
    * @return boolean
    */
    public function updCountrys($item)//ôóíêöèÿ ìîäèôèêàöèè îáúåêòîâ ñ òàáëèöè ÁÄ, 
                                     //êîòîðàÿ âîçâðàùàåò ôëàã óñïåøíîñòè îïåðàöèè
    {
    	$success = "False";
        $connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if ($item->DELCOUNTRY == true)
		{
			$QueryText = "delete from COUNTRYS where ID = '$item->ID'";
		}		
		else
        {
            $QueryText = format_for_query("execute procedure UPDCOUNTRYS ('{0}','{1}', '{2}', '{3}')", $item->ID, $item->NUM, $item->CHARCODE, $item->NAME);
        }
    	
       	$query = ibase_prepare($trans, $QueryText);
    	ibase_execute($query);	
    	$success = ibase_commit($trans);
    	ibase_free_query($query);	
    	$connection->CloseDBConnection();
    	return $success;
    }
    
     /**
    *  @param string $id,  
 	*  @return AdressBookModuleStates[]
	*/ 	
     public function getStates($id) 
     { 
	$connection = new Connection();
    	$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText = 
            "select STATE, NAME from STATES where COUNTRY='$id' order by STATE";
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
    	$rows = array();//ñîçäàåì ìàññèâ ðåçóëüòàòîâ
        while ($row = ibase_fetch_row($result))//ïðîõîäèì ïî âñåì àññîöèèðîâàííûì ðÿäàì ðåçóëüòàòà èç ÁÄ
    	{
    	    $obj = new AdressBookModuleStates;
            
            $obj->STATE = $row[0];
                                       
            $obj->NAME = $row[1];
                                      
    		$rows[] = $obj;
        }	
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
    	$connection->CloseDBConnection();		
    	return $rows;//âîçâðàùàåì ìàññèâ 
    }
     
    /**
    * @param AdressBookModuleStates $item, 
    * @return boolean
    */
    public function updStates($item)//ôóíêöèÿ ìîäèôèêàöèè îáúåêòîâ ñ òàáëèöè ÁÄ, 
                                     //êîòîðàÿ âîçâðàùàåò ôëàã óñïåøíîñòè îïåðàöèè
    {
    	$success = "False";
        $connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if ($item->DELSTATE == true)
		{
			$QueryText = "delete from States where COUNTRY = '$item->COUNTRY' and STATE ='$item->STATE'";
		}		
		else
        {
            $QueryText = format_for_query("execute procedure UPDSTATES ('{0}','{1}','{2}')", $item->COUNTRY, $item->STATE, $item->NAME);
        }
       	$query = ibase_prepare($trans, $QueryText);
    	ibase_execute($query);	
    	$success = ibase_commit($trans);
    	ibase_free_query($query);	
    	$connection->CloseDBConnection();
       	return $success;
    }
    
     /**
    *  @param string $cn, 
    *  @param string $st, 
 	*  @return AdressBookModuleRegions[]
	*/ 	
     public function getRegions($cn, $st) 
     { 
	$connection = new Connection();
    	$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText = 
            "select NUM, NAME, STATE, COUNTRY from REGIONS where COUNTRY='$cn' and STATE ='$st' order by NUM";
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
    	$rows = array();//ñîçäàåì ìàññèâ ðåçóëüòàòîâ
        while ($row = ibase_fetch_row($result))//ïðîõîäèì ïî âñåì àññîöèèðîâàííûì ðÿäàì ðåçóëüòàòà èç ÁÄ
    	{
    	    $obj = new AdressBookModuleRegions;
            
            $obj->NUM = $row[0];
                                       
            $obj->NAME = $row[1];
                                      
    		$rows[] = $obj;
        }	
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
    	$connection->CloseDBConnection();		
    	return $rows;//âîçâðàùàåì ìàññèâ 
    }
     
    /**
    * @param AdressBookModuleRegions $item, 
    * @return boolean
    */
    public function updRegions($item)//ôóíêöèÿ ìîäèôèêàöèè îáúåêòîâ ñ òàáëèöè ÁÄ, 
                                     //êîòîðàÿ âîçâðàùàåò ôëàã óñïåøíîñòè îïåðàöèè
    {
    	$success = "False";
        $connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if ($item->DELREGION == true)
		{
			$QueryText = "delete from Regions where COUNTRY = '$item->COUNTRY' and STATE ='$item->STATE' and NUM='$item->NUM'";
		}		
		else
        {
            $QueryText = format_for_query("execute procedure UPDREGIONS ('{0}','{1}','{2}','{3}')", $item->NUM, $item->NAME, $item->STATE, $item->COUNTRY);
        }
    	$query = ibase_prepare($trans, $QueryText);
    	ibase_execute($query);	
    	$success = ibase_commit($trans);
    	ibase_free_query($query);	
    	$connection->CloseDBConnection();
       	return $success;
    }
   
    /**
    *  @param string $cn, 
    *  @param string $st,
    *  @param string $rg,
 	*  @return AdressBookModuleCitys[]
	*/ 	
     public function getCitys($cn, $st, $rg) 
     { 
	$connection = new Connection();
    	$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select distinct CITYID, CITYNAME from ADDRESS where COUNTRYID='$cn' "; 
        if ($st!="N" && $rg!="N")	
		{
		    	$QueryText=$QueryText."and STATEID='$st' and REGIONID='$rg'"; 
		}
        
        elseif ($st!="N" && $rg="N")
		{
		    	$QueryText=$QueryText."and STATEID='$st' and REGIONID IS NULL";            
		}
       
        elseif ($st="N" && $rg="N")	
        {
       	    	$QueryText=$QueryText."and STATEID IS NULL and REGIONID IS NULL";
        } 
       
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
    	$rows = array();//ñîçäàåì ìàññèâ ðåçóëüòàòîâ
        while ($row = ibase_fetch_row($result))//ïðîõîäèì ïî âñåì àññîöèèðîâàííûì ðÿäàì ðåçóëüòàòà èç ÁÄ
    	{
    	    $obj = new AdressBookModuleCitys;
            
            $obj->ID = $row[0];
            
            $obj->NAME = $row[1];
                                       
    		$rows[] = $obj;
        }	
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
    	$connection->CloseDBConnection();		
    	return $rows;//âîçâðàùàåì ìàññèâ 
    }
     
    /**
    * @param AdressBookModuleCitys $item, 
    * @return boolean
    */
    public function updCitys($item)//ôóíêöèÿ ìîäèôèêàöèè îáúåêòîâ ñ òàáëèöè ÁÄ, 
                                     //êîòîðàÿ âîçâðàùàåò ôëàã óñïåøíîñòè îïåðàöèè
    {
    	$success = "False";
        $connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        // ôîðìóºìî òåêñò çàïèòó
	    if ($item->DELCITY == true)
		{
			$QueryText = "delete from CITYS	where ID='$item->ID'";
		}		
		else
        {
            $QueryText = format_for_query("execute procedure UPDCITYS ('{0}','{1}','{2}','{3}','{4}')", $item->ID, $item->NAME, $item->COUNTRY, $item->STATE, $item->REGION);
        }		
		// ãîòóºìî ñåðâåð äî çàïèòó
		$query = ibase_prepare($trans, $QueryText);
		// âèêîíóºìî çàïèò
		ibase_execute($query);
       	$success = ibase_commit($trans);
    	ibase_free_query($query);	
    	$connection->CloseDBConnection();
    	return $success;
    }
 }
?>
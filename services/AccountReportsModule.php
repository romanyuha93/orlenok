<?php
//define('tfClientId','online');

require_once 'tbs/tbs_class.php';
require_once 'tbs/tbs_plugin_opentbs.php';
require_once "F25OBJECT.php";

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";


//$form = new AccountReportsModule();
//$form->createReport("5", "NaN", "docx", false, false);
//$form->printAccountFlowPatient('odt', '10001', 'ru_RU');


	function num2str($num) {
		$nul='нуль';
		$ten=array(
			array('','одна','дві','три','чотири',"п'ять",'шість','сім', 'вісім',"дев'ять"),
			array('','одна','дві','три','чотири',"п'ять",'шість','сім', 'вісім',"дев'ять"),
		);
		$a20=array('десять','одинадцять','дванадцять','тринадцять','чотирнадцять' ,"п'ятнадцать",'шістнадцать','сімнадцять','вісімнадцать',"дев'ятнадцать");
		$tens=array(2=>'двадцять','тридцять','сорок',"п'ятдесят",'шістдесят','сімдесят' ,'вісімдесят',"дев'яносто");
		$hundred=array('','сто','двісті','триста','чотириста',"п'ятьсот",'шістсот', 'сімсот','вісімсот',"дев'ятсот");
		$unit=array(
		// Units
			array('копійка' ,'копійки' ,'копійок',1),
			array('гривня'   ,'гривні'   ,'гривень'    ,0),
			array('тисяча'  ,'тисячі'  ,'тисяч'     ,1),
			array('мільйон' ,'мільйона','мільйонів' ,0),
			array('мільярд','міліарда','мільярдів',0),
		);
		//
		list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
		$out = array();
		if (intval($rub)>0) {
			foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
				if (!intval($v)) continue;
				$uk = sizeof($unit)-$uk-1; // unit key
				$gender = $unit[$uk][3];
				list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
				// mega-logic
				$out[] = $hundred[$i1]; # 1xx-9xx
				if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
				else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
				// units without rub & kop
				if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
			} //foreach
		}
		else $out[] = $nul;
		$out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
		$out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
		return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
	}

	function morph($n, $f1, $f2, $f5) 
	{
		$n = abs(intval($n)) % 100;
		if ($n>10 && $n<20) return $f5;
		$n = $n % 10;
		if ($n>1 && $n<5) return $f2;
		if ($n==1) return $f1;
		return $f5;
	}
	
    
    
    
class AccountReportsModule_AccountFlowBookObject
{
       /**
    	* @var PrintDataModule_DateUA
    	*/
       var $DATE;
        
        /**
        * @var AccountReportsModule_AccountFlowBookTableRecord[]
        */
        var $TABLE_DATA; 
        
            /**
    	* @var double
    	*/
       var $SUMMSTART;
       
       /**
    	* @var double
    	*/
       var $SUMMEND;
       
        /**
    	* @var double
    	*/
       var $SUMMIN;
       
       /**
    	* @var double
    	*/
       var $SUMMOUT;
       
       /**
    	* @var string
    	*/
       var $SUMMINSTR;
       
       /**
    	* @var string
    	*/
       var $SUMMOUTSTR;
       
        function __construct($date, $trans=null)     
        { 
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
            $QueryText="select sum(af.ordernumber) from accountflow af
                            where af.operationtimestamp<'$date'";
     			
                $query = ibase_prepare($trans, $QueryText);
    			$result = ibase_execute($query);
    		
    			if($row = ibase_fetch_row($result))
                {
                    $this->SUMMSTART=$row[0];           
                }
    
               	ibase_free_query($query);
            	ibase_free_result($result);
    
            if(nonull($this->SUMMSTART)==false)
            {
                $this->SUMMSTART=0;
            }
            //$this->CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(false,$trans);
            $this->DATE=new PrintDataModule_DateUA($date);
            $this->TABLE_DATA=array();    
            $this->SUMMIN=0.00;    
            $this->SUMMOUT=0.00;        
            $QueryText="select af.ordernumber,
                                p.shortname, 
                                (case when af.summ>0 then af.summ else null end), 
                                (case when af.summ<0 then -af.summ else null end) 
                            from accountflow af
                                left join patients p on (p.id=af.patientid) 
                            where cast(af.operationtimestamp as date)='$date' and
                            af.is_cashpayment=0
                            order by af.ordernumber";
     			
                $query = ibase_prepare($trans, $QueryText);
    			$result = ibase_execute($query);
    		
    			while($row = ibase_fetch_row($result))
                {
                    $rec=new AccountReportsModule_AccountFlowBookTableRecord;
                    $rec->ORDERNUMBER=$row[0];
                    $rec->PATIENT_SHORTNAME=$row[1];
                    $rec->SUMMIN=$row[2];
                    $rec->SUMMOUT=$row[3];
                    $this->SUMMIN+=$row[2];  
                    $this->SUMMOUT+=$row[3]; 
                    $this->TABLE_DATA[]=$rec;                
                }
                
                $this->SUMMEND=$this->SUMMSTART+$this->SUMMIN-$this->SUMMOUT;
                $this->SUMMINSTR=money2str_ua($this->SUMMIN, 1);
                $this->SUMMOUTSTR=money2str_ua($this->SUMMOUT, 1);
               	ibase_free_query($query);
            	ibase_free_result($result);
    
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }  
        }
}

class AccountReportsModule_AccountFlowBookTableRecord
{
    /**
	* @var string
	*/
   var $ORDERNUMBER;
   
   /**
	* @var string
	*/
   var $PATIENT_SHORTNAME;
     
   /**
	* @var double
	*/
   var $SUMMIN;
   
      /**
	* @var double
	*/
   var $SUMMOUT;   
}
    
    
    
    
    
    
    
    
    
class AccountReportsModule
{

    public static $isDebugLog = false;
    
  /** 
  * @param string $file_name
  */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);        
    }


    /**
    * @param string $docType
    * @param string $invoiceID
    * @return string
    */    
    public function createInsuranceReport($docType,$invoiceID)
    {
    	$res=(dirname(__FILE__).'/tbs/res/');
    		
    	if(($docType == 'docx') && (!file_exists($res.'Insurance.docx')) )
            {
                return "TEMPLATE_NOTFOUND";
            }
    		if(($docType == 'odt') && (!file_exists($res.'Insurance.odt')) )
            {
                return "TEMPLATE_NOTFOUND";
    	}
    	//////////////////////////////////////////////////////////////
    	//////////////////////////// DOCX ////////////////////////////
    	//////////////////////////////////////////////////////////////
    	if($docType == 'docx')
    	{				
    	$connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
    	$QueryText = 
    		"select 
    		INVOICES.DOCTORID,
    		DOCTORS.SHORTNAME,
    		(select INVOICES.INVOICENUMBER from INVOICES where INVOICES.ID={$invoiceID}),
    		PATIENTS.FIRSTNAME, PATIENTS.MIDDLENAME, PATIENTS.LASTNAME, PATIENTS.CITY, PATIENTS.ADDRESS, PATIENTS.MOBILENUMBER,
    		(select CLINICREGISTRATIONDATA.LEGALNAME from CLINICREGISTRATIONDATA where CLINICREGISTRATIONDATA.ID=1),
    		(select CLINICREGISTRATIONDATA.ADDRESS from CLINICREGISTRATIONDATA where CLINICREGISTRATIONDATA.ID=1),
    		(select CLINICREGISTRATIONDATA.TELEPHONENUMBER from CLINICREGISTRATIONDATA where CLINICREGISTRATIONDATA.ID=1)
    		from 
    		PATIENTS, INVOICES
    		LEFT JOIN DOCTORS ON DOCTORS.ID=INVOICES.DOCTORID
    		where PATIENTS.ID=(select INVOICES.PATIENTID from INVOICES where INVOICES.ID=$invoiceID)";			
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $INVOICE_DATA = new stdclass;
    	$INSURANCE_DATA = new stdclass;
    		//array(FIRSTNAME,MIDDLENAME,LASTNAME,CITY,ADDRESS,MOBILENUMBER,
    		//CLINIC_LEGALNAME,CLINIC_ADDRESS,CLINIC_TELEPHONENUMBER,PRICE,DOCTOR_SHORTNAME,POLICENUMBER);
    	if ($row = ibase_fetch_row($result))
            {
    		$INSURANCE_DATA->DOCTOR_SHORTNAME=$row[1];
    		$INVOICE_DATA->INVOICENUMBER=$row[2];
    		$INSURANCE_DATA->FIRSTNAME=$row[3];
    		$INSURANCE_DATA->MIDDLENAME=$row[4];
    		$INSURANCE_DATA->LASTNAME=$row[5];
    		$INSURANCE_DATA->CITY=$row[6];
    		$INSURANCE_DATA->ADDRESS=$row[7];
    		$INSURANCE_DATA->MOBILENUMBER=$row[8];
    		$INSURANCE_DATA->CLINIC_LEGALNAME=$row[9];
    		$INSURANCE_DATA->CLINIC_ADDRESS=$row[10];
    		$INSURANCE_DATA->CLINIC_TELEPHONENUMBER=$row[11];			
    	}
    		$date=date('d.m.Y');
    		$QueryText = "select 
    			  tp.proc_count, 
    			  tp.price, 
                              coalesce(cast(tp.price as decimal(15,2)),0) -
                              coalesce(cast(tp.price*(tp.discount_flag*invoices.discount_amount/100.00) as decimal(15,2)),0),
                              tp.name,
                              tp.diagnos2,
    			  PROTOCOLS.NAME,
    			  invoices.discount_amount,
    			  (select first 1 PATIENTSINSURANCE.POLICENUMBER from PATIENTSINSURANCE where 
    			  (PATIENTSINSURANCE.POLICETODATE>='$date' and PATIENTSINSURANCE.POLICEFROMDATE<='$date' and  
    			  PATIENTSINSURANCE.PATIENTID=(select INVOICES.PATIENTID from INVOICES where INVOICES.ID=$invoiceID))
    			order by PATIENTSINSURANCE.POLICETODATE desc )
    		        from treatmentproc tp
    			left join PROTOCOLS on PROTOCOLS.ID = (select DIAGNOS2.PROTOCOL_FK from DIAGNOS2 where DIAGNOS2.ID=tp.diagnos2)
    			inner join invoices on invoices.id = tp.invoiceid 
    			where tp.invoiceid = $invoiceID
    			order by tp.diagnos2";
                    $query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
    		$ALL_SUMM = new stdclass();
    		/**
    		* @param string $prev_diag
    		*/
            $prev_diag="";
    		/**
    		* @param int $j
    		*/
    		$j=0;
    		$ALL_SUMM->SUMM=0;
    		$ALL_SUMM->ALL=0;
    		while ($row = ibase_fetch_row($result))
                    {						
    			$Procedure = new stdclass();
    			$Procedure->FULL_NAME=$INSURANCE_DATA->FIRSTNAME." ".$INSURANCE_DATA->MIDDLENAME." ".$INSURANCE_DATA->LASTNAME;
    			$Procedure->NAME = $row[3];
                            $Procedure->SUMM=$row[1];
                            $Procedure->SUMMD=$row[2];
    			$Procedure->POLICENUMBER=$row[7];
    			$j++;
    			$Procedure->DIAG = getTreatmentObjects($row[4],$trans)." ".$row[5];
    			$ALL_SUMM->DISCOUNT = $row[6];
    			$ALL_SUMM->SUMM+=$row[1]*$row[0];
    			$ALL_SUMM->ALL+=$row[2]*$row[0];
    			for ($i=1; $i<=$row[0]; $i++)
    			{
    				$ProceduresArray[]=$Procedure;	
    			}
    		}
    
    	ibase_free_query($query);
            ibase_free_result($result);
    
            $TBS = new clsTinyButStrong;
    
    	$INSURANCE_DATA->PRICE=num2str($ALL_SUMM->ALL);
    
            $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
            if(file_exists(dirname(__FILE__).'/tbs/res/Insurance.'.$docType))
            {
                $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/Insurance.'.$docType, OPENTBS_ALREADY_UTF8);  
            }
            $TBS->NoErr=true;
    	
    	$DATE_CURRENT= new stdclass;
    	$date_current1 = time();
            $DATE_CURRENT->DAY=ua_date("d", $date_current1);
    		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
            $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
    		
    	$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
    				
    
    	$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
    	$TBS->MergeField("INVOICE_DATA", $INVOICE_DATA);
    	$TBS->MergeField("INSURANCE_DATA", $INSURANCE_DATA);
    	$TBS->MergeBlock("Procedures", $ProceduresArray);
    	$TBS->MergeField("ALL_SUMM", $ALL_SUMM);
    
    	$GLOBALS['header1']=tflocale::$medcard.' №';
    	$file_name = translit(str_replace(' ','_',$INSURANCE_DATA->LASTNAME.'_'.$INSURANCE_DATA->FIRSTNAME.'_'.$INSURANCE_DATA->MIDDLENAME));
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().'_'.$file_name.'.'.$docType;
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
    	return $form_path;
    	}	
    	//////////////////////////////////////////////////////////////
    	//////////////////////////// ODT /////////////////////////////
    	//////////////////////////////////////////////////////////////
    	if($docType == 'odt')
            {
    		$connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
    		$QueryText = 
    			"select 
    			INVOICES.DOCTORID,
    			DOCTORS.SHORTNAME,
    			(select INVOICES.INVOICENUMBER from INVOICES where INVOICES.ID={$invoiceID}),
    			PATIENTS.FIRSTNAME, PATIENTS.MIDDLENAME, PATIENTS.LASTNAME, PATIENTS.CITY, PATIENTS.ADDRESS, PATIENTS.MOBILENUMBER,
    			(select CLINICREGISTRATIONDATA.LEGALNAME from CLINICREGISTRATIONDATA where CLINICREGISTRATIONDATA.ID=1),
    			(select CLINICREGISTRATIONDATA.ADDRESS from CLINICREGISTRATIONDATA where CLINICREGISTRATIONDATA.ID=1),
    			(select CLINICREGISTRATIONDATA.TELEPHONENUMBER from CLINICREGISTRATIONDATA where CLINICREGISTRATIONDATA.ID=1)
    			
    			from 
    			PATIENTS, INVOICES
    			LEFT JOIN DOCTORS ON DOCTORS.ID=INVOICES.DOCTORID
    			where PATIENTS.ID=(select INVOICES.PATIENTID from INVOICES where INVOICES.ID=$invoiceID)";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $INVOICE_DATA = new stdclass;
    		$INSURANCE_DATA = new stdclass;
    	if ($row = ibase_fetch_row($result))
            {
    
    		$INSURANCE_DATA->DOCTOR_SHORTNAME=$row[1];
    		$INVOICE_DATA->INVOICENUMBER=$row[2];
    		$INSURANCE_DATA->FIRSTNAME=$row[3];
    		
    		$INSURANCE_DATA->MIDDLENAME=$row[4];
    		$INSURANCE_DATA->LASTNAME=$row[5];
    		$INSURANCE_DATA->CITY=$row[6];
    		$INSURANCE_DATA->ADDRESS=$row[7];
    		$INSURANCE_DATA->MOBILENUMBER=$row[8];
    		$INSURANCE_DATA->CLINIC_LEGALNAME=$row[9];
    		$INSURANCE_DATA->CLINIC_ADDRESS=$row[10];
    		$INSURANCE_DATA->CLINIC_TELEPHONENUMBER=$row[11];	
    		
    	}
    
    		$date=date('d.m.Y');
    		$QueryText = "select 
    		  tp.proc_count, 
    		  tp.price, 
                      coalesce(cast(tp.price as decimal(15,2)),0) -
                      coalesce(cast(tp.price*(tp.discount_flag*invoices.discount_amount/100.00) as decimal(15,2)),0),
                      tp.name,
                      tp.diagnos2,
    		  PROTOCOLS.NAME,
    		  invoices.discount_amount,
    		  (select first 1 PATIENTSINSURANCE.POLICENUMBER from PATIENTSINSURANCE where 
    		  (PATIENTSINSURANCE.POLICETODATE>='$date' and PATIENTSINSURANCE.POLICEFROMDATE<='$date' and  
    		   PATIENTSINSURANCE.PATIENTID=(select INVOICES.PATIENTID from INVOICES where INVOICES.ID=$invoiceID))
    		   order by PATIENTSINSURANCE.POLICETODATE desc )
    		from treatmentproc tp
    		left join PROTOCOLS on PROTOCOLS.ID = (select DIAGNOS2.PROTOCOL_FK from DIAGNOS2 where DIAGNOS2.ID=tp.diagnos2)
    		inner join invoices on invoices.id = tp.invoiceid 
    		where tp.invoiceid = $invoiceID
    		order by tp.diagnos2";
    		$query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
                    $ALL_SUMM = new stdclass();
    		$ALL_SUMM->SUMM=0;
    		$ALL_SUMM->ALL=0;
    		while ($row = ibase_fetch_row($result))
                    {
    			$Procedure = new stdclass();
    			$Procedure->NAME = $row[3];
    			$Procedure->SUMM = $row[1];
    			$Procedure->SUMMDISCOUNT = $row[2];
                    	$Procedure->DIAG=getTreatmentObjects($row[4],$trans)." ".$row[5];						
    			$ALL_SUMM->DISCOUNT = $row[6];
    			$ALL_SUMM->SUMM+=$row[1]*$row[0];
    			$ALL_SUMM->ALL+=$row[2]*$row[0];
    			$Procedure->POLICENUMBER = $row[7];
    			for ($i=1; $i<=$row[0]; $i++)
    			{
    				$ProceduresArray[]=$Procedure;
    			}
    		}
    
    	ibase_free_query($query);
            ibase_free_result($result);
    
            $TBS = new clsTinyButStrong;
    	
    	$INSURANCE_DATA->PRICE=num2str($ALL_SUMM->ALL);
    
            $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
            if(file_exists(dirname(__FILE__).'/tbs/res/Insurance.'.$docType))
            {
                $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/Insurance.'.$docType, OPENTBS_ALREADY_UTF8);  
            }
            $TBS->NoErr=true;
    
    	$DATE_CURRENT= new stdclass;
    	$date_current1 = time();
            $DATE_CURRENT->DAY=ua_date("d", $date_current1);
    		$DATE_CURRENT->MONTH=ua_date("M", $date_current1);
            $DATE_CURRENT->YEAR=ua_date("y", $date_current1);		
    		
    	$DATE_CURRENT->MONTH = dateDeclineOnCases($DATE_CURRENT->MONTH);
    
    									
    		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT);
    		$TBS->MergeField("INVOICE_DATA", $INVOICE_DATA);
    		$TBS->MergeField("INSURANCE_DATA", $INSURANCE_DATA);
    		$TBS->MergeBlock("Procedures", $ProceduresArray);
    		$TBS->MergeField("ALL_SUMM",$ALL_SUMM);
    
    		$GLOBALS['header1']=tflocale::$medcard.' №';	
    		$file_name = translit(str_replace(' ','_',$INSURANCE_DATA->LASTNAME.'_'.$INSURANCE_DATA->FIRSTNAME.'_'.$INSURANCE_DATA->MIDDLENAME));
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().'_'.$file_name.'.'.$docType;
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
    		return $form_path;
            }
    }
    
    
        
        /**
        * @param string $id_invoice
        * @param string $id_order
        * @param string $docType
        * @param boolean $isCoursename
        * @param boolean $isNullProceduresInCheck
        * @param string $orderGeneralLabel;
        * @return string
        */
    public function createReport( $id_invoice, $id_order, $docType, $isCoursename, $isNullProceduresInCheck, $orderGeneralLabel)
        {
             //file_put_contents("C:/tmp.txt",'"'.$id_invoice.'", "'.$id_order.'", "'.$docType.'"');
            $res=(dirname(__FILE__).'/tbs/res/orders/');
            
            
            if(($docType == 'docx') && (!file_exists($res.'CashOrder.docx') | !file_exists($res.'CashlessInvoice.docx') | !file_exists($res.'ReturningCashOrder.docx')) )
            {
                return "TEMPLATE_NOTFOUND";
            }
            else if(($docType == 'odt') && (!file_exists($res.'CashOrder.odt') | !file_exists($res.'CashlessInvoice.odt') | !file_exists($res.'ReturningCashOrder.odt')) )
            {
                return "TEMPLATE_NOTFOUND";
            }
            
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
           
            $ClinicRegistrationData = new stdclass();
            $Invoices = new stdclass();
            $PrintData = new stdclass();
            $Accountflow = new stdclass();
            $Insurance = new stdclass();
            
            
            
                $QueryText = "
                select 
                    NAME,
                    LEGALNAME,
                    ADDRESS,
                    ORGANIZATIONTYPE,
                    CODE,
                    DIRECTOR,
                    TELEPHONENUMBER,
                    FAX,
                    SITENAME,
                    EMAIL,
                    ICQ,
                    SKYPE,
                    CHIEFACCOUNTANT,
                    LEGALADDRESS,
                    BANK_NAME,
                    BANK_MFO,
                    BANK_CURRENTACCOUNT,
                    LICENSE_NUMBER,
                    LICENSE_SERIES,
                    LICENSE_STARTDATE,
                    LICENSE_ENDDATE,
                    LICENSE_ISSUED,
                    LOGO
                from CLINICREGISTRATIONDATA";
                
                $query = ibase_prepare($trans, $QueryText);
                $result = ibase_execute($query);
                
                if ($row = ibase_fetch_row($result))
                {
                    $ClinicRegistrationData->NAME = $row[0];
                    $ClinicRegistrationData->CLINICNAME = $row[1];
                    $ClinicRegistrationData->CLINICADDRESS = $row[2];
                    $ClinicRegistrationData->ORGANIZATIONTYPE = $row[3];
                    $ClinicRegistrationData->CODE = $row[4];
                    $ClinicRegistrationData->DIRECTOR = $row[5];
                    $ClinicRegistrationData->TELEPHONENUMBER = $row[6];
                    $ClinicRegistrationData->FAX = $row[7];
                    $ClinicRegistrationData->SITENAME = $row[8];
                    $ClinicRegistrationData->EMAIL = $row[9];
                    $ClinicRegistrationData->ICQ = $row[10];
                    $ClinicRegistrationData->SKYPE = $row[11];
                    $ClinicRegistrationData->CHIEFACCOUNTANT = $row[12];
                    $ClinicRegistrationData->LEGALADDRESS = $row[13];
                    $ClinicRegistrationData->BANK_NAME = $row[14];
                    $ClinicRegistrationData->BANK_MFO = $row[15];
                    $ClinicRegistrationData->BANK_CURRENTACCOUNT = $row[16];
                    $ClinicRegistrationData->LICENSE_NUMBER = $row[17];
                    $ClinicRegistrationData->LICENSE_SERIES = $row[18];
                    $ClinicRegistrationData->LICENSE_STARTDATE = $row[19];
                    $ClinicRegistrationData->LICENSE_ENDDATE = $row[20];
                    $ClinicRegistrationData->LICENSE_ISSUED = $row[21];
                    
                    $imageBlob = null;
                    $blob_info = ibase_blob_info($row[22]);
                    if ($blob_info[0]!=0){
                        $blob_hndl = ibase_blob_open($row[22]);
                        $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                        ibase_blob_close($blob_hndl);
                    }
                    $ClinicRegistrationData->LOGO = base64_encode($imageBlob);
                    
                }
                
                ibase_free_query($query);
                ibase_free_result($result);
                
                if($ClinicRegistrationData->CODE!=null&&strlen($ClinicRegistrationData->CODE)>7)
                {
                    $ClinicRegistrationData->CODE1 = $ClinicRegistrationData->CODE[0];
                    $ClinicRegistrationData->CODE2 = $ClinicRegistrationData->CODE[1];
                    $ClinicRegistrationData->CODE3 = $ClinicRegistrationData->CODE[2];
                    $ClinicRegistrationData->CODE4 = $ClinicRegistrationData->CODE[3];
                    $ClinicRegistrationData->CODE5 = $ClinicRegistrationData->CODE[4];
                    $ClinicRegistrationData->CODE6 = $ClinicRegistrationData->CODE[5];
                    $ClinicRegistrationData->CODE7 = $ClinicRegistrationData->CODE[6];
                    $ClinicRegistrationData->CODE8 = $ClinicRegistrationData->CODE[7];
                }
                $Accountflow->paid_sum=0;
                $PrintData->display_check = 'none';
                
                $ProceduresArray = array();
    
                if($id_invoice != null && $id_invoice != 'NaN')
                {
                   /* $QueryText = "
                             select 
                            invoices.summ,
                            patients.fullname,
                            (select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0)
                                         from invoices
                                        inner join treatmentproc
                                        on treatmentproc.invoiceid = invoices.id
                                        where invoices.id = $id_invoice),
                            invoices.is_cashpayment,
                            (select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0)
                                        from invoices
                                        inner join treatmentproc
                                        on treatmentproc.invoiceid = invoices.id
                                        where invoices.id = $id_invoice),
                            invoices.createdate,
                            (select doctors.shortname from doctors where id=invoices.doctorid),
                            (select applicationsettings.parametervalue from applicationsettings where applicationsettings.parametername='CashOrderFormName'),
                            (select applicationsettings.parametervalue from applicationsettings where applicationsettings.parametername='InvoiceFormName'),
                            invoices.invoicenumber,
                            invoices.paidincash, --------------------------*
                            invoices.discount_amount,
                            invoices.PATIENINSURANCE_FK,
    						patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER,
    						patients.CARDDATE,
                            invoices.summ-(select coalesce(sum(af.summ),0) from accountflow af where af.oninvoice_fk=invoices.id)
                            from  invoices
                            inner join patients on (patients.id = invoices.patientid) where invoices.id = $id_invoice"; */
                            
                            
                    $QueryText = "
                             select 
                            invoices.summ,
                            patients.fullname,
                            (select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0)
                                         from invoices
                                        inner join treatmentproc
                                        on treatmentproc.invoiceid = invoices.id
                                        where invoices.id = $id_invoice),
                            invoices.is_cashpayment,
                            (select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0)
                                        from invoices
                                        inner join treatmentproc
                                        on treatmentproc.invoiceid = invoices.id
                                        where invoices.id = $id_invoice),
                            invoices.createdate,
                            (select doctors.shortname from doctors where id=invoices.doctorid),
                            (select applicationsettings.parametervalue from applicationsettings where applicationsettings.parametername='CashOrderFormName'),
                            (select applicationsettings.parametervalue from applicationsettings where applicationsettings.parametername='InvoiceFormName'),
                            invoices.invoicenumber,
                            (select coalesce(sum(accountflow.summ),0) from accountflow where accountflow.oninvoice_fk = $id_invoice),
                            invoices.discount_amount,
                            invoices.PATIENINSURANCE_FK,
    						patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER,
    						patients.CARDDATE,
                            invoices.summ-(select coalesce(sum(af.summ),0) from accountflow af where af.oninvoice_fk=invoices.id),
                            patients.PASSPORT_SERIES,
                            patients.PASSPORT_NUMBER,
                            patients.PASSPORT_ISSUING,
                            patients.PASSPORT_DATE,
                            patients.ADDRESS,
                            patients.TELEPHONENUMBER
                            
                            from  invoices
                            inner join patients on (patients.id = invoices.patientid) where invoices.id = $id_invoice";
                            
                            
                            
                        $query = ibase_prepare($trans, $QueryText);
                        $result = ibase_execute($query);
                        
                        
                        
                        if ($row = ibase_fetch_row($result))
                        {
                            $Invoices->sum = $row[0];
                            $Invoices->patient_fullname = $row[1];
                            $Invoices->payer = $Invoices->patient_fullname;
                            
                            $Invoices->discount_amount = $row[2];
                            $Invoices->is_cashpayment = $row[3];
                            $Invoices->sumoutdiscount = $row[4]; 
                            
                            $Invoices->only_invoice_date = $row[5];
                            $Invoices->invoice_date = $row[5]!=null ? date("d/m/Y", strtotime($row[5])) : "";
                            $Invoices->doctor_shortname = $row[6];
                            $Invoices->cash_order_form_name = $row[7];
                            $Invoices->invoice_form_name = $row[8];
                            $Invoices->invoice_number = $row[9];
                            $Invoices->invoice_number_ext = '('.tflocale::$invoiceShort.' № '.$row[9].')';
                            //$Invoices->invoice_number_full = tflocale::$invoice.' № '.$row[9];
                            $Invoices->paidincash = $row[10];
                            $Invoices->discount_amount_percent = $row[11] == 0 ? "" : $row[11].'%'; //TODO %
                            $Invoices->word_paidincash = money2str_ua( $Invoices->sum , 0x01);
                            $Invoices->patientinsurance_fk = $row[12];
                            
                            $Invoices->additional_patient = "";
                            $Invoices->additional_police = "";
                            $Accountflow->passport_series = $row[16];
                            $Accountflow->passport_number = $row[17];
                            $Accountflow->passport_issuing = $row[18];
                            $Accountflow->passport_date = $row[19];
                            $Accountflow->patient_address = $row[20];
                            $Accountflow->patient_phone = $row[21];
                            
                            
                            $PrintData->display_check = 'block';
                            
                            if(($id_order==null) || ($id_order=="NaN"))
                            {
                                $Accountflow->paid_sum = $row[10];
                                $Accountflow->timestamp = ua_date("d M Y", strtotime($row[5]));
                                $Accountflow->timestampstr = date("d.m.Y", strtotime($row[5]));
    							$Accountflow->CARDNUMBER = $row[13];
    							$Accountflow->CARDDATE = $row[14]!=null?date("d.m.Y", strtotime($row[14])):"";
    							$Accountflow->INVOICE_BALANCE = $row[15];
                            }
                        
                        }
                        
                        ibase_free_query($query);
                        ibase_free_result($result);
                        
                        
                        // Если пациент страховой (PATIENINSURANCE_FK = number)
                        // то выбираем данные из таблици PATIENTSINSURANCE и присоединяем к нему название страховой компании
                        
                        //echo("fk ". $Invoices->patientinsurance_fk);
                        if($Invoices->patientinsurance_fk != null)
                        {
                            $patientinsurance_fk = $Invoices->patientinsurance_fk;
                            $QueryText = "
                                select 
                                    p.id,
                                    p.policenumber,
                                    p.policetodate,
                                    p.policefromdate,
                                    i.namelegal
                                from patientsinsurance p
                                left join insurancecompanys i
                                    on p.companyid = i.id
                                where p.id = $patientinsurance_fk; 
                            ";
                            
                            $query = ibase_prepare($trans, $QueryText);
                            $result = ibase_execute($query);
                            
                            if ($row = ibase_fetch_row($result))
                            {
                                $Insurance->id = $row[0];
                                $Insurance->policenumber = $row[1];
                                $Insurance->policetodate = $row[2];
                                $Insurance->policefromdate = $row[3];
                                $Insurance->legalname = $row[4];
                                
                                $Invoices->payer = $Insurance->legalname;
                                $Invoices->additional_patient = "Пацієнт: ".$Invoices->patient_fullname ;
                                $Invoices->additional_police ="Поліс № ".$Insurance->policenumber . ' від '. $Insurance->policefromdate;
                            }
                            
                            
                            
                        }
                        
                        
                        
                        $QueryText = "select tp.shifr, tp.nameforplan, tp.proc_count, tp.price, tp.proc_count * tp.price,
                                coalesce(cast(tp.price*tp.proc_count as decimal(15,2)),0) -
                                coalesce(cast(tp.price*tp.proc_count*(tp.discount_flag*invoices.discount_amount/100.00) as decimal(15,2)),0),
                                tp.name,
                                tp.diagnos2
                            from treatmentproc tp inner join invoices on invoices.id = tp.invoiceid where tp.invoiceid = $id_invoice";
                            if($isNullProceduresInCheck==false)
                            {
                                $QueryText.=" and tp.price>0";
                            }
                        
                        $query = ibase_prepare($trans, $QueryText);
                        $result = ibase_execute($query);
                        
                        while ($row = ibase_fetch_row($result))
                        {
                            $Procedure = new stdclass();
                            $Procedure->SHIFR = $row[0];
                            if($isCoursename == true)
                            {
                                $Procedure->NAMEFORPLAN = $row[6];
                            }
                            else
                            {
                                $Procedure->NAMEFORPLAN = $row[1]; 
                            }
                            $Procedure->PROCCOUNT = $row[2];
                            $Procedure->PRICEPERUNIT = $row[3];
                            $Procedure->SUMM = $row[4];
                            $Procedure->SUMMWITHDISCOUNT = $row[5];
                            $Procedure->TREATMENTOBJECTS = getTreatmentObjects($row[7],$trans);
                            $ProceduresArray[]=$Procedure;   
                        }
                        
                        
                        
                        
                        ibase_free_query($query);
                        ibase_free_result($result);
                    
                }
                
                $PrintData->display_order = 'none';
                
                $IS_RETURNING = false;
                
                if($id_order != null && $id_order != "NaN" )
                {
                    $QueryText = "
                    select 
                        accountflow.summ,
                        accountflow.operationtimestamp,
                        patients.fullname,
                        accountflow.ordernumber,
                        accountflow.is_cashpayment,
                        accountflow.operationtype,
    					patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER,
    					patients.CARDDATE,
    				    invoices.summ-(select coalesce(sum(af.summ),0) from accountflow af where af.oninvoice_fk=invoices.id),
                        patients.PASSPORT_SERIES,
                        patients.PASSPORT_NUMBER,
                        patients.PASSPORT_ISSUING,
                        patients.PASSPORT_DATE,
                        patients.ADDRESS,
                        patients.TELEPHONENUMBER
                        
                    from ACCOUNTFLOW 
                    left join patients on (patients.id = ACCOUNTFLOW.PATIENTID) 
    				left join invoices on (invoices.id = ACCOUNTFLOW.oninvoice_fk)
                    where accountflow.id=$id_order";
                    //file_put_contents("C:/tmp.txt",$QueryText);
                    $query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
                    
                    if ($row = ibase_fetch_row($result))
                    {
                        $Accountflow->paid_sum = $row[0];
                        $Accountflow->word_sum = mb_ucfirst(money2str_ua($Accountflow->paid_sum, 0x01 ));
                        $Accountflow->order_number =$row[3];
                        $Accountflow->only_timestamp = $row[1];
                        $Accountflow->timestampstr = date("d.m.Y", strtotime($row[1]));
                        $Accountflow->timestamp = $row[1]!=null? ua_date("d M Y", strtotime($row[1])):"";
                        $Accountflow->patient_fullname = nonull($row[2])?$row[2]:"";
                        $Accountflow->is_cashpayment = $row[4];
                        $Accountflow->operationtype = $row[5];
                        $PrintData->display_order = 'block';
    					$Accountflow->CARDNUMBER = $row[6];
    					$Accountflow->CARDDATE = $row[7]!=null?date("d.m.Y", strtotime($row[7])):"";
    					$Accountflow->INVOICE_BALANCE = $row[8];
                        
                        $Accountflow->passport_series = $row[9];
                        $Accountflow->passport_number = $row[10];
                        $Accountflow->passport_issuing = $row[11];
                        $Accountflow->passport_date = $row[12];
                        $Accountflow->patient_address = $row[13];
                        $Accountflow->patient_phone = $row[14];
                    }
                    // если сумма отрицательная, тогда пациент снимает деньги со счета 
                    // и в этом случае нужно печатать видатковий кассовый ордер
                    if($Accountflow->paid_sum < 0  &&  ($Accountflow->operationtype == 0 || $Accountflow->operationtype == 1))
                    {
                        // Отрицательное число меняем на положительное
                        // и обновляем суму прописью
                        $Accountflow->paid_sum = abs($Accountflow->paid_sum);
                        $Accountflow->word_sum = mb_ucfirst(money2str_ua($Accountflow->paid_sum, 0x01 ));
                        $IS_RETURNING = true;
                    }
                    
                     if($orderGeneralLabel == "storage")
                     {
                        $Accountflow->GENERALLABEL = "Стоматологічні товари";
                     }
                     else if ($orderGeneralLabel == "nhealth")
                     {
                        $Accountflow->GENERALLABEL = "Медичні послуги";
                     }
                     else
                     {
                        $Accountflow->GENERALLABEL = "Стоматологічні послуги";
                     }
                    
                    
                    ibase_free_query($query);
                    ibase_free_result($result);
                    
                    // ------------------------------------ cashless
                    
                    
                    if($id_invoice == "NaN")
                    {
                        // Создаем одну процедуру, которая будет содержать дефолтные значения
                        // в сумах везде  будет  $Accountflow->paid_sum
                        $Procedure = new stdclass();
                            $Procedure->SHIFR = "";
                            if($orderGeneralLabel == "storage")
                            {
                                $Procedure->NAMEFORPLAN = "Стоматологічні товари";
                            }
                            else if ($orderGeneralLabel == "nhealth")
                            {
                                $Procedure->NAMEFORPLAN = "Медичні послуги";
                            }
                            else
                            {
                                $Procedure->NAMEFORPLAN = "Стоматологічні послуги";
                            }
                            $Procedure->PROCCOUNT = 1;
                            $Procedure->PRICEPERUNIT = $Accountflow->paid_sum ;
                            $Procedure->SUMM = $Accountflow->paid_sum ;
                            $Procedure->SUMMWITHDISCOUNT = $Accountflow->paid_sum ;
                            
                            $ProceduresArray[0] = $Procedure;
                    
                    
                        $Invoices->invoice_number = $Accountflow->order_number;
                        $Invoices->sumoutdiscount = $Accountflow->paid_sum;
                        $Invoices->discount_amount = 0;
                        $Invoices->paidincash = $Accountflow->paid_sum;
                        $Invoices->sum = $Accountflow->paid_sum;
                        $Invoices->invoice_date = ua_date("d M Y", strtotime($Accountflow->timestamp));
                        $Invoices->word_paidincash = money2str_ua( $Accountflow->paid_sum , 0x01);
                        
                        // invisible values (needed to work with openTBS);
                        $Invoices->invoice_number_ext="";
                        $Invoices->doctor_shortname="";
                        $Invoices->patient_fullname=$Accountflow->patient_fullname;
                        $Invoices->payer=$Accountflow->patient_fullname;
                        $Invoices->discount_amount_percent="";
                        $Invoices->additional_patient = "";
                        $Invoices->additional_police = "";
                    }
                
                }
                
                ibase_commit($trans);
                $connection->CloseDBConnection();
                
                
                
                 $IS_CASHLESS = false;  // true - безналичний; false - наличний
                 // определение переменной $IS_CASHLESS
                 if($id_invoice != "NaN" && $id_invoice != null){ $IS_CASHLESS = $Invoices->is_cashpayment == 1 ? true : false; }
                 if($id_order   != "NaN" && $id_order != null){ $IS_CASHLESS = $Accountflow->is_cashpayment == 1 ? true : false; }
                
                // need for displaying blocks in openTBS
                $GLOBALS["display_order"] = $PrintData->display_order;
                $GLOBALS["display_check"] = $PrintData->display_check;
                
                
                $TBS = new clsTinyButStrong();
                //$TBS->NoErr = true;
                
                // если открываемый файл не zip архив, то данный модуль нельзя подключать
                $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
                
                
                //======================== загрузка шаблонов
              
                if($docType == 'odt')
                {
                    if( ! $IS_CASHLESS){
                        if( ! $IS_RETURNING){
                            $TBS->LoadTemplate($res.'CashOrder.odt', OPENTBS_ALREADY_XML);
                        }else{
                            $TBS->LoadTemplate($res.'ReturningCashOrder.odt', OPENTBS_ALREADY_XML);
                        }
                    }else{
                        $TBS->LoadTemplate($res.'CashlessInvoice.odt', OPENTBS_ALREADY_XML);
                    }
                }
                else
                {
                    if( ! $IS_CASHLESS)
                    {
                        if( ! $IS_RETURNING){
                            $TBS->LoadTemplate($res.'CashOrder.docx', OPENTBS_ALREADY_XML);
                        }else{
                            $TBS->LoadTemplate($res.'ReturningCashOrder.docx', OPENTBS_ALREADY_XML);
                        }
                    }else{
                        $TBS->LoadTemplate($res.'CashlessInvoice.docx', OPENTBS_ALREADY_XML);
                    }
                }
                //-------------
                
                $TBS->MergeField("ClinicRegistrationData", $ClinicRegistrationData);
                $TBS->MergeField("Invoices", $Invoices);
                $TBS->MergeField("Accountflow", $Accountflow);
                $TBS->MergeBlock("Procedures", $ProceduresArray);
                
                $TBS->MergeBlock("Procedures2", $ProceduresArray);
                
            
            
                if(($PrintData->display_order == 'none')&&($PrintData->display_check == 'block'))//only invoice
                {
                    $filename = $Invoices->patient_fullname;
                    $filename.='_i'.$Invoices->invoice_number;
                    if($Invoices->only_invoice_date != null)
                    {
                        $filename.='_'.date("d-m-Y", strtotime($Invoices->only_invoice_date));
                    }
                }
                else if(($PrintData->display_order == 'block')&&($PrintData->display_check == 'none'))//only order
                {
                    $filename = $Accountflow->patient_fullname;
                    $filename.='_i'.$Accountflow->order_number;
                    if($Accountflow->only_timestamp!=null)
                    {
                        $filename.='_'.date("d-m-Y", strtotime($Accountflow->only_timestamp));
                    }
                }
                else//invoice && order
                {
                    $filename = $Invoices->patient_fullname;
                    $filename .= '_i'.$Invoices->invoice_number.'_o'.$Accountflow->order_number;
                    if($Invoices->only_invoice_date != null)
                    {
                        $filename .= '_'.date("d-m-Y", strtotime($Invoices->only_invoice_date));
                    }
                }
                
                $form_path = translit(str_replace(' ','_',$filename));
                $form_path = preg_replace('/[^A-Za-z0-9_]/', '', $form_path);
                
                
                $file_name = uniqid().'_'.$form_path .'.'.$docType;
                // OPENTBS_FILE+TBS_EXIT
                
                if($docType == 'docx' || $docType == 'odt'){
                     $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$file_name);
                }
                if($IS_RETURNING)
                {
                  $size="A5"; 
                  $format="L"; 
                }
                else
                {
                    $size="A4";
                    $format="P"; 
                }
                
                
                return $file_name;
            
            
            
    }
        
    /** 
    * @param string $docType
    * @param string $date
    * @return string
    */  
    public static function printAccountFlowBook($docType, $date)
     {      
        $TBS = new clsTinyButStrong;
        
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/AccountFlowBook.'.$docType, OPENTBS_ALREADY_UTF8);
        
            
        $BOOK=new AccountReportsModule_AccountFlowBookObject($date);
        $TBS->MergeBlock('TABLE_DATA',$BOOK->TABLE_DATA);            
        $TBS->MergeField('BOOK',$BOOK);    
        $file_name = translit(str_replace(' ','_','Касова книга '.$date));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
      
        
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);   
    
        return $form_path;   
     }
     
     
     
     
     
     
     
     
    // ********************* PRINT ACCOUNTFLOW PATIENT START ********************************
    /** 
    * @param string $docType
    * @param string $patientID
    * @param string $locale
    * @param string $onlyWithInvoice
    * @return string
    */ 
    public static function printAccountFlowPatient($docType, $patientID, $locale, $onlyWithInvoice=true)
    {
            $docType = 'docx';
        
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $CLINIC_DATA = new PrintDataModule_ClinicRegistrationData(true, $trans);
            $PATIENT_DATA = new PrintDataModule_PatientData($patientID, $trans);
            
            $ACCOUNT_DATA = array();
            
            
            
            //GET ACCOUNT DATA START
            
            $sql = "select

                        invoices.id,
                        invoices.invoicenumber,
                        invoices.createdate,
                        (select account.lastname||' '||account.firstname from account where account.id = invoices.authorid),
                        coalesce(invoices.discount_amount, 0),  
                        invoices.summ,
                        
                        (select sum(accountflow.summ) from accountflow where accountflow.oninvoice_fk = invoices.id) as paid_summ,
                        (select max(accountflow.operationtimestamp) from accountflow where accountflow.oninvoice_fk = invoices.id) as paid_lastdate,
                        
                        (select cast (list(treatmentobjects.tooth,'|') as varchar(100)) from treatmentobjects where treatmentobjects.diagnos2 = treatmentproc.diagnos2),
                        (select protocols.name from protocols where protocols.id = (select diagnos2.protocol_fk from diagnos2 where diagnos2.id = treatmentproc.diagnos2)),
                        
                        
                        treatmentproc.name,
                        treatmentproc.price,
                        treatmentproc.proc_count,
                        (select doctors.shortname from doctors where doctors.id = treatmentproc.doctor) as treatmentproc_doctor,
                        treatmentproc.discount_flag
                        
                        
                        from treatmentproc ";
            if($onlyWithInvoice)
            {
                $sql .= " inner ";
            }
            else
            {
                $sql .= " left ";
            }         
            $sql .= " join invoices on invoices.id = treatmentproc.invoiceid
                        
                        where treatmentproc.patient_fk = $patientID
                            and treatmentproc.dateclose is not null
                        order by invoices.id desc";
                                			
    		$query = ibase_prepare($trans, $sql);
    		$result = ibase_execute($query);
            	
            $sum = 0;
            $sum_paid = 0;
            $inv_id = -1000;
    		while ($row = ibase_fetch_row($result))
    		{
              
                $invObj = new stdClass;
                if(is_null($row[0]))
                {
                    $invObj->INV_ID = -1;
                }
                else
                {
                    $invObj->INV_ID = $row[0];
                }
                if(is_null($row[1]))
                {
                    if($locale == 'uk_UA')
                    {
    			         $invObj->INV_NUM = 'Без рахунка';
                    }
                    else
                    {
    			         $invObj->INV_NUM = 'Без счета';
                    }
			        $invObj->INV_DATE = '-';
   			        $invObj->INV_AUTHOR = '-';
                    $invObj->INV_DISCOUNT = '-';
                    $invObj->INV_SUM = '-';
                    $invObj->PAID_SUM = '-';
                    $invObj->PAID_DATE = '-';
                }
                else
                {
    			     $invObj->INV_NUM = $row[1];
    			     $invObj->INV_DATE = $row[2];
    			     $invObj->INV_AUTHOR = $row[3];
                     $invObj->INV_DISCOUNT = $row[4];
                     $invObj->INV_SUM = $row[5];
                     if(is_null($row[6]))
                     {
                        $invObj->PAID_SUM = '-';
                        $invObj->PAID_DATE = '-';
                     }
                     else
                     {
                        $invObj->PAID_SUM = $row[6];
                        $invObj->PAID_DATE = $row[7];
                     }
                }
                
                $invObj->TOOTH = $row[8];
                $invObj->DIAGNOS = $row[9];
                $invObj->PRC_NAME = $row[10];
                $invObj->PRC_PRICE = $row[11];
                $invObj->PRC_COUNT = $row[12];
                $invObj->PRC_DOC = $row[13];
                $invObj->PRC_DISCOUNTFLAG = $row[14];
                $invObj->PRC_DISCOUNT = (float)($invObj->INV_DISCOUNT) * (float)($invObj->PRC_DISCOUNTFLAG);
                
                $invObj->PRC_SUM = ($invObj->PRC_PRICE*$invObj->PRC_COUNT)*(1-($invObj->PRC_DISCOUNT)/100);
                
                if($invObj->PRC_DISCOUNT == 0)
                {
                     $invObj->PRC_DISCOUNT = '-';
                }
                
                
                $ACCOUNT_DATA[] = $invObj;
                
                
                
    		      if($inv_id == -1000)
                  {
                      $inv_id = $invObj->INV_ID;
                      $sum += (float)$invObj->INV_SUM;
                      $sum_paid += (float)$invObj->PAID_SUM;
                  }
                  
                  if($invObj->INV_ID == -1)
                  {
                     $sum += $invObj->PRC_SUM;
                  }
                  else if($invObj->INV_ID != $inv_id)
                  {
                     $inv_id = $invObj->INV_ID;
                     $sum += (float)$invObj->INV_SUM;
                     $sum_paid += (float)$invObj->PAID_SUM;
                  }
                
    		}	
            
            $SUMMARY = new stdClass;
            $SUMMARY->SUM = $sum;
            $SUMMARY->PAID_SUM = $sum_paid;
            $SUMMARY->RESULT = $sum - $sum_paid;         
            
    		ibase_free_query($query);
    		ibase_free_result($result);
            
            
            //GET ACCOUNT DATA END
            
    		if(AccountReportsModule::$isDebugLog)
    		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($ACCOUNT_DATA,true));
    		}
             
                
            $TBS = new clsTinyButStrong;
            $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
            
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/PatientAccountReport.'.$docType, OPENTBS_ALREADY_UTF8);  
            
            
            //$TBS->NoErr=true;
                  
            $TBS->MergeField('PATIENT_DATA', $PATIENT_DATA);
            $TBS->MergeField('CLINIC_DATA', $CLINIC_DATA);
            $TBS->MergeField('SUMMARY', $SUMMARY);
            
            $TBS->MergeBlock('ACCOUNT_DATA', $ACCOUNT_DATA);
                    
            $file_name = translit(str_replace(' ','_','PatientAccountReport'));
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().'_'.$file_name.'.'.$docType;   
           
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
                    
            return $form_path;
    }
    // ********************* PRINT ACCOUNTFLOW PATIENT  END  ********************************
     
     
     
     
     
}
    

?>
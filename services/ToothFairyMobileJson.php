<?php

define('tfClientId', 'online');
require_once "Connection.php";
require_once "ServiceModule.php";
require_once "Utils.php";



const PHONE_COUNTRY = '38';

    //***********************************************PRIVATE FUNCTIONS START***********************************************
     /**
     * ToothFairyMobileService::getAges()
     * 
     * @param string $birthdate
     * @return int
     */
    function getAges($birthdate)
    {
        if(($birthdate != null)&&($birthdate != ''))
        {
            $date = new DateTime($birthdate);
            $now = new DateTime();
            $interval = $now->diff($date);
            return $interval->y;
        }
        else
        {
            return -1;
        }
    }
    
    //***********************************************PRIVATE FUNCTIONS END*************************************************

    if($_GET["func"]) 
    {
		//http://localhost/toothfairy/services/ToothFairyMobileJson.php?func=getTasks&doctorId=-2147483603&filterDate=2014-02-04&startWorkTime=09:00&endWorkTime=21:00
        //http://server2.vikisoft.com.ua/toothfairydemo/salus/toothfairy/services/ToothFairyMobileJson.php?func=getTasks&doctorId=-2147483603&filterDate=2014-04-02&startWorkTime=09:00&endWorkTime=21:00
        if($_GET["func"] == "getTasks" && $_GET["doctorId"] && $_GET["filterDate"] && $_GET["startWorkTime"] && $_GET["endWorkTime"])
        {
            $_startWorkTime = $_GET["startWorkTime"];
            $_filterDate = $_GET["filterDate"];
            $_endWorkTime = $_GET["endWorkTime"];
            $_doctorId = $_GET["doctorId"];
            
            /*echo $_startWorkTime;
            echo $_filterDate;
            echo $_endWorkTime;
            echo $_doctorId;
            */
            
            $endAddTaskTime = $_startWorkTime;
            $nowDate = new DateTime();
            $nowDate->setTime(0,0,0);
            
            $filtrDate = new DateTime($_filterDate);
            $startDate = new DateTime($_filterDate.' '.$_startWorkTime);
            $endDate = new DateTime($_filterDate.' '.$_endWorkTime);
            
            $connection = new Connection();
    	    $connection->EstablishDBConnection("main", false);
    	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
    	    $QueryText =
    			"select
                    TASKS.ID,
                    TASKS.BEGINOFTHEINTERVAL,
                    TASKS.ENDOFTHEINTERVAL,
                    TASKS.WORKDESCRIPTION,
                    TASKS.COMMENT,
                    PATIENTS.ID,
                    PATIENTS.LASTNAME,
                    PATIENTS.FIRSTNAME,
                    PATIENTS.MIDDLENAME,
                    PATIENTS.CARDBEFORNUMBER,
                    PATIENTS.CARDNUMBER,
                    PATIENTS.CARDAFTERNUMBER,
                    PATIENTS.PRIMARYFLAG,
                    PATIENTS.TELEPHONENUMBER,
                    (select first 1
                            patientsmobile.mobilenumber
                        from patientsmobile
                            where patientsmobile.patient_fk = PATIENTS.ID
                            and patientsmobile.senderactive = 1),
                    PATIENTS.BIRTHDAY,
                    PATIENTS.SEX, 
                    patients.hobby
                
                from TASKS
                    left join PATIENTS on (TASKS.PATIENTID = PATIENTS.ID)
                
                where TASKS.DOCTORID = '".$_doctorId."'
                    and TASKS.taskdate = '".$_filterDate."'
                order by TASKS.BEGINOFTHEINTERVAL asc;";
                
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
            
            $rows = array();
            $firstAddObjectFlag = false;
            $prev_objectEndDate = null;
            $prev_objectEndTime;
            
    		while ($row = ibase_fetch_row($result))
    		{ 
    		  //echo $row;
    		  $objectStartDate = new DateTime($_filterDate.' '.$row[1]);
    		  $objectEndDate = new DateTime($_filterDate.' '.$row[2]);
              
              if(($startDate < $objectStartDate)&&($firstAddObjectFlag == false)&&($nowDate <= $filtrDate))
              {
               	$objAddTask = new stdClass;
                $objAddTask->is_plus_object = 0;
                $objAddTask->task_startTime = $_startWorkTime;
                $objAddTask->task_endTime = substr($row[1], 0, 5);//$row[1]
                $rows[] = $objAddTask;
              }
                $firstAddObjectFlag = true;
              
                $obj = new stdClass;
                $obj->task_id = $row[0];
                $obj->task_startTime = substr($row[1], 0, 5);
                $obj->task_endTime = substr($row[2], 0, 5);
                $obj->task_workdescription = $row[3];
                $obj->task_comment = $row[4];
                $obj->task_range = $obj->task_startTime.' - '.$obj->task_endTime;
                $obj->patient_id = $row[5];
                $obj->patient_lname = $row[6];
                $obj->patient_fname = $row[7];
                $obj->patient_sname = $row[8];
                $obj->patient_fullcardnumber = "{$row[9]}{$row[10]}{$row[11]}";
                $obj->patient_primaryflag = $row[12];
                $obj->patient_phone = $row[13];
                $obj->patient_mobile = $row[14];
                $obj->patient_birthday = $row[15];
                $obj->patient_sex = $row[15];
                $obj->patient_notes = $row[16];
                $obj->patient_fio = $obj->patient_lname.' '.$obj->patient_fname.' '.$obj->patient_sname;
                $patient_ages = getAges($obj->patient_birthday);
                if($patient_ages != -1)
                {
                    $obj->patient_ages = getAges($obj->patient_birthday);
                }
                else
                {
                    $obj->patient_ages = '-'; 
                }
                $obj->patient_notes = $row[17];
                $obj->task_interval_minutes = $objectEndDate->diff($objectStartDate)->d*24 + $objectEndDate->diff($objectStartDate)->h*60 + $objectEndDate->diff($objectStartDate)->i;
                $obj->is_plus_object = 1;
              //add additem start  
              if(($objectStartDate > $prev_objectEndDate)&&($prev_objectEndDate != null)&&($nowDate <= $filtrDate))
              {
               	$objAddTask = new stdClass;
                $objAddTask->is_plus_object = 0;
                $objAddTask->task_startTime = $prev_objectEndTime;
                $objAddTask->task_endTime = $obj->task_startTime;
                $rows[] = $objAddTask;
              }
              $prev_objectEndDate = $objectEndDate;
              $prev_objectEndTime = $obj->task_endTime;
              //add additem end  
              
              $rows[] = $obj;
              
              $endAddTaskTime = $obj->task_endTime;
    		};
            //add plus object
            if($nowDate <= $filtrDate)
            {
               	$objAddTask = new stdClass;
                $objAddTask->is_plus_object = 0;
                $objAddTask->task_startTime = $endAddTaskTime;
                $rows[] = $objAddTask;
            }
            
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
            echo json_encode($rows);
            
        }
        
        //http://localhost/toothfairy/services/ToothFairyMobileJson.php?func=getLazyPatients&searchString=Пе
        if($_GET["func"] == "getLazyPatients" && $_GET["searchString"])
        {
            $searchText = $_GET["searchString"];
            $connection = new Connection();
    	    $connection->EstablishDBConnection("main", false);
    	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
    	    $QueryText =
    			"select patients.ID, patients.LASTNAME, patients.FIRSTNAME, patients.MIDDLENAME
                     from patients
                     where lower(patients.LASTNAME) starting lower('$searchText') 
                     and PRIMARYFLAG = 1 order by patients.LASTNAME";
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
            $rows = array();
    		while ($row = ibase_fetch_row($result))
    		{ 
                $obj = new stdClass;
                $obj->patient_id = $row[0];
                $obj->patient_fio = $row[1].' '.$row[2].' '.$row[3];
                $rows[] = $obj;
    		}	
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();		
            echo json_encode($rows);
        }
            
       
        //http://localhost/toothfairy/services/ToothFairyMobileJson.php?func=getRooms
        if($_GET["func"] == "getRooms")
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection("main", false);
    	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
    	    $QueryText = "select ROOMS.ID, ROOMS.NUMBER, ROOMS.NAME from ROOMS";
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
            $rows = array();
            
    		while($row = ibase_fetch_row($result))
    		{
                $obj = new stdClass;
                $obj->room_id = $row[0];
                $obj->room_number = $row[1];
                $obj->room_name = $row[2];
                 $TemplQuery = "select WORKPLACES.ID, WORKPLACES.NUMBER, WORKPLACES.DESCRIPTION from WORKPLACES where WORKPLACES.roomid = '$row[0]'";
                 $templquery = ibase_prepare($trans, $TemplQuery);
                 $templresult = ibase_execute($templquery);
                 while($templrow = ibase_fetch_row($templresult))
                 {
                    $templ = new stdClass;
                    $templ->workplace_id = $templrow[0];
                    $templ->workplace_number = $templrow[1];
                    $templ->workplace_name = $templrow[2];
                    $obj->workplaces[] = $templ;
                 }
                 ibase_free_query($templquery);
                 ibase_free_result($templresult);
                
                $rows[] = $obj;
    		}	
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();		
            echo json_encode($rows);
        }

        //http://localhost/toothfairy/services/ToothFairyMobileJson.php?func=addTask&&task={JSON}
        if($_GET["func"] == "addTask")// && $_POST["task"])
        {
			$post = file_get_contents('php://input');
            $task = json_decode($post);
            if($task != null)
            {
                $connection = new Connection();
        		$connection->EstablishDBConnection("main", false);
        		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                if(($task->patient_id != '')&&($task->patient_id != null))
                {
                    $QueryText = "insert into TASKS (
                                                id, 
                                                taskdate, 
                                                beginoftheinterval, 
                                                endoftheinterval, 
                                                patientid, 
                                                doctorid, 
                                                roomid, 
                                                workplaceid, 
                                                noticed, 
                                                factofvisit, 
                                                dateofvisit, 
                                                timeofvisit, 
                                                workdescription, 
                                                comment)
                		values(null, 
                        '$task->task_date', 
                        '$task->task_begin', 
                        '$task->task_end', 
                        $task->patient_id, 
                        $task->doctor_id, 
                        $task->task_room_id, 
                        '$task->task_workplace_id', 
                        0, 0, null, null, 
                        '".safequery($task->task_wd)."', 
                        '".safequery($task->task_cm)."')";
                    $query = ibase_prepare($trans, $QueryText);
            		ibase_execute($query);
            		ibase_free_query($query);
                }
                else
                {
                    $shortname = $task->patient_lname;
                    $fullname = $task->patient_lname;   
                    if(mb_strlen($task->patient_fname) > 0)
                    {
                        $shortname .= ' '.mb_substr($task->patient_fname, 0, 1).'.';
                        $fullname .= ' '.$task->patient_fname;                
                        if(mb_strlen($task->patient_sname) > 0)
                        {
                            $shortname .= ' '.mb_substr($task->patient_sname, 0, 1).'.';
                            $fullname .= ' '.$task->patient_sname;
                        }
                    }
                    /*$QueryText = "INSERT into PATIENTS 
                                (id,
                                firstname,  
                                lastname,
                                middlename,
                                shortname,
                                fullname,
                                telephonenumber,
                                mobilenumber,
                                primaryflag) 
                                values 
                                (null, 
                                '".safequery($task->patient_fname)."', 
                                '".safequery($task->patient_lname)."', 
                                '".safequery($task->patient_sname)."',
                                '".safequery($shortname)."',
                                '".safequery($fullname)."',
                                '".safequery($task->patient_mobile)."',
                                '".safequery($task->patient_mobile)."', 
                                0) returning ID";*/
                    $QueryText = "INSERT into PATIENTS 
                                (id,
                                firstname,  
                                lastname,
                                middlename,
                                shortname,
                                fullname,
                                primaryflag) 
                                values 
                                (null, 
                                '".safequery($task->patient_fname)."', 
                                '".safequery($task->patient_lname)."', 
                                '".safequery($task->patient_sname)."',
                                '".safequery($shortname)."',
                                '".safequery($fullname)."',
                                0) returning ID";
            		$query = ibase_prepare($trans, $QueryText);
            		$result = ibase_execute($query);
                    $row = ibase_fetch_row($result);
                    $PRIMPATID = $row[0];
            		ibase_free_query($query);
            		ibase_free_result($result);
                    
                    
                    if(($PRIMPATID != null)&&($PRIMPATID != ''))
                    {
                        if(($task->patient_mobile != null)&&($task->patient_mobile != ''))
                        {
                            //add phone
                            $QueryTextMobile = "insert into patientsmobile(
                                            patient_fk, 
                                            parent_fk, 
                                            mobilenumber, 
                                            senderactive, 
                                            comments) 
                                        values (
                                            ".$PRIMPATID.", 
                                            null, 
                                            '".PHONE_COUNTRY.$task->patient_mobile."',
                                            1, 
                                            '')";
                            $queryMobile = ibase_prepare($trans, $QueryTextMobile);
                    		ibase_execute($queryMobile);
                            //add phone
                        }
                        
                		$QueryText = "insert into TASKS (
                                                    id, 
                                                    taskdate, 
                                                    beginoftheinterval, 
                                                    endoftheinterval, 
                                                    patientid, 
                                                    doctorid, 
                                                    roomid, 
                                                    workplaceid, 
                                                    noticed, 
                                                    factofvisit, 
                                                    dateofvisit, 
                                                    timeofvisit, 
                                                    workdescription, 
                                                    comment)
                    		values(null, 
                            '$task->task_date', 
                            '$task->task_begin', 
                            '$task->task_end', 
                            $PRIMPATID, 
                            $task->doctor_id, 
                            $task->task_room_id, 
                            '$task->task_workplace_id', 
                            0, 0, null, null, 
                            '".safequery($task->task_wd)."', 
                            '".safequery($task->task_cm)."')";
                        $query = ibase_prepare($trans, $QueryText);
                		ibase_execute($query);
                		//$success = ibase_commit($trans);
                		ibase_free_query($query);
                        
                    }
                }
                $success = ibase_commit($trans);
        		$connection->CloseDBConnection();
        		return "ok";
            }
        }
        
        
        if($_GET["func"] == "deleteTask" && $_GET["taskId"])
        {	
            $task_id = $_GET["taskId"];	
            
            $connection = new Connection();
    		$connection->EstablishDBConnection("main",false);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    		$QueryText = "delete from TASKS where ID = $task_id";
    		$query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		ibase_free_query($query);
    		$success = ibase_commit($trans);
    		$connection->CloseDBConnection();
    		return "ok";
        }
        
        
        if($_GET["func"] == "check" && $_GET["login"] && $_GET["pass"])
        {
			$doctorslogin = $_GET["login"];	
			$doctorspassw = $_GET["pass"];	
            $connection = new Connection();
			$connection->EstablishDBConnection("main",false);
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			/*$QueryText = "select USERS.DOCTORID, USERS.ACCESSALLOW, DOCTORS.SHORTNAME,
							doctors.lastname, doctors.firstname, doctors.middlename
							from USERS
							left join DOCTORS
							   on  USERS.DOCTORID = DOCTORS.ID
							where USERS.NAME = '".safequery($doctorslogin)."' and USERS.PASSWRD = '".safequery($doctorspassw)."'";*/
			$QueryText = "select DOCTORS.ID, 1, DOCTORS.SHORTNAME,
                            doctors.lastname, doctors.firstname, doctors.middlename
                            from account
                            left join DOCTORS
                               on  account.id = DOCTORS.accountid
                            where account.accountuser = '".safequery($doctorslogin)."' 
							and account.accountpassword = '".safequery($doctorspassw)."'";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$logininfo=ibase_fetch_row($result);
			ibase_free_query($query);
			ibase_free_result($result);
			ibase_commit($trans);
			$connection->CloseDBConnection();
			
			$resultObject = new stdClass;
			$resultObject->err_msg = "x100";
				if($logininfo[0] == null)
				{
					$resultObject->err_msg = "x101";
				}
				else if($logininfo[1] == 0)
				{
					$resultObject->err_msg = "x102";
				}
				else if(($logininfo[0] != null)&&($logininfo[1] != 0))
				{
					$ServiceInfo = new ServiceModule;
					$resultObject->user_id = $logininfo[0];
					$resultObject->user_shortname = $logininfo[2];
					$resultObject->user_lname = $logininfo[3];
					$resultObject->user_fname = $logininfo[4];
					$resultObject->user_mname = $logininfo[5];
					
					$_keys = $ServiceInfo->check_hd();
					for($i=4 ; $i <= count($_keys)-1 ; $i++ )
					{
						$key = new stdClass;
						$key->key = $_keys[$i];
						$resultObject->checkItems[] = $key;
					}
				}
			echo json_encode($resultObject);	
        }
        
        if($_GET["func"] == "f2" && $_GET["par"])
        {
            
        }
        
        
    }
    
    
?>

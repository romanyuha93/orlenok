<?php
require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once "Form039Module.php";
//$mkb=new HealthPlanMKB10Module;
//$mkb->setAutomaticalDiagnos43(10,3);

class HealthPlanMKB10MKBProtocol
{
    /**
     * @var int
     */
    var $ID;
    /**
     * @var string 
     */
    var $NAME;
    /**
     * @var number
     */
    var $healthtype;
    /**
     * @var HealthPlanMKB10MKB
     */
    var $MKB10;
    /**
     * @var int
     */
    var $F43;
    /**
     * @var HealthPlanMKB10F43[]
     */
     var $F43list;
    /**
     * @var string 
     */
    var $PROTOCOLTXT;
    /**
     * @var HealthPlanMKB10Template[]
     */
    var $Templates;
    /**
     * @var string 
     */
    var $ANAMNESTXT;
    /**
     * @var string 
     */
    var $STATUSTXT;
    /**
     * @var string 
     */
    var $RECOMENDTXT;
    
    /**
     * @var string 
     */
    var $RENTGENTXT;
    /**
     * @var string 
     */
    var $EPICRISISTXT;
}
class HealthPlanMKB10F43
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $f43_id;
    /**
    * @var string
    */
    var $protocol_id;
    /**
    * @var int
    */
    var $f43;
}
class HealthPlanMKB10QuickF43
{
    /**
   * @var int
   */
	var $ID;
    
   /**
   * @var int
   */
	var $F43;
    
    /**
   * @var string
   */
	var $NAME;
}
class HealthPlanMKB10MKB
{
    /**
     * @var int 
     */
    var $ID;
    /**
     * @var int 
     */
    var $PARENTID;
    /**
     * @var string 
     */
    var $SHIFR;
    /**
     * @var string 
     */
    var $NAME;
}

class HealthPlanMKB10ToothExam
{
    /**
     * @var int 
     */
    var $ID;
    /**
     * @var string
     */
    var $exam;
    /**
     * @var string
     */
    var $examdate;
    /**
     * @var int
     */
    var $bittype;
    /**
     * @var HealthPlanMKB10Doctor
     */
    var $Doctor;
}


class HealthPlanMKB10Patient
{
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $id;
}

class HealthPlanMKB10Template
{
    /**
     * @var int
     */
    var $ID;
    /**
     * @var string
     */
    var $NAME;
    /**
     * @var string
     */
    var $CREATEDATE;
    /**
     * @var int
     */
    var $DOCTOR;
    /**
     * @var HealthPlanMKB10MKBProtocol
     */
    var $Protocol;
    /**
     * @var float
     */ 
    var $cost;
    /**
     * @var string
     */ 
    var $toolTip;
}

class HealthPlanMKB10Comment
{
    /**
     * @var int
     */
    var $ID;
    /**
     * @var int
     */
    var $OWNERID;
    /**
     * @var int
     */
    var $TYPE;
    /**
     * @var string
     */
    var $COMMENTTXT;
    /**
     * @var string
     */
    var $DATECOMMENT;
    /**
     * @var boolean
     */
    var $SIGNFLAG;
    /**
     * @var HealthPlanMKB10Doctor
     */
    var $Doctor;
    /**
     * @var bool
     */
    var $deltype;
}

class HealthPlanMKB10TreatmentAdditions
{
    /**
     * @var int
     */
    var $ID;
    /**
     * @var int
     */
    var $TYPE;
    /**
     * @var string
     */
    var $ADDITINSTXT;
    /**
     * @var boolean
     */
    var $SIGNFLAG;
    /**
     * @var HealthPlanMKB10Doctor
     */
    var $Doctor;
    /**
     * @var string
     */
    var $SIGNDATE;
    /**
     * @var int
     */
    var $TrCourse;
    
    /**
     * @var int
     */
    var $visit;
}

class HealthPlanMKB10Diagnos2
{
    /**
     * @var int
     */
    var $ID;
    /**
     * @var int
     */
    var $f43;
    /**
     * @var int
     */
    var $EXAMID;
    /**
     * @var HealthPlanMKB10MKBProtocol
     */
    var $protocol;
    /**
     * @var HealthPlanMKB10Doctor
     */
    var $Doctor;
    /**
     * @var string
     */
    var $DIAGNOSDATE;
    /**
     * @var string
     */
    var $EXAMDATE;
    /**
     * @var HealthPlanMKB10Template
     */
    var $Template;
    /**
     * @var string
     */
    var $GARANTDATE;
    /**
     * @var HealthPlanMKB10MKBProtocol[]
     */
    var $Protocols;
    /**
     * @var HealthPlanMKB10Comment[]
     */
    var $DiagnosComments;
    /**
     * @var int
     */
    var $STACKNUMBER;
    /**
     * @var bool
     */
    var $selected;
    /**
     * @var HealthPlanMKB10TreatmentObject[]
     */
    var $TreatmentObjects;
    
    /**
     * @var int
     */
     var $TREATMENTCOURSE;
     
    /**
     * @var string
     */
     var $TEETH;
     
    /**
     * @var int
     */
    var $TOOTH_MIN;
     
    /**
     * HealthPlanMKB10Diagnos2::isExist()
     * 
     * @param HealthPlanMKB10Diagnos2 $diags
     * @return bool
     */
    public function isExist($diags)
    {
        foreach ($diags as $d)
        {
            if ($this->f43 == $d->f43)
            {
                foreach ($this->TreatmentObjects as $to)
                {
                    if ($to->isExist($d->TreatmentObjects))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * HealthPlanMKB10Diagnos2::addTreatmentObjects()
     * 
     * @param HealthPlanMKB10TreatmentObject[] $objs
     * @return void
     */
    public function addTreatmentObjects($objs)
    {
        //echo "<br><br>".count($this->TreatmentObjects)."<br>";
        foreach ($objs as $newto)
        {
            //echo "<br>new ".$newto->TOOTH."side ".$newto->TOOTHSIDE;
            $exist = false;
            foreach ($this->TreatmentObjects as $to)
            {
                //echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;this ".$to->TOOTH."side ".$to->TOOTHSIDE;
                if ($newto->TOOTH == $to->TOOTH)
                {
                    //echo '<br>Before: '.$to->TOOTHSIDE;
                    $to->addSide($newto->TOOTHSIDE);
                    $exist = true;
                    break;
                    //echo '<br>After : '.$to->TOOTHSIDE;
                }
            }
            if (!$exist)
                $this->TreatmentObjects[] = $newto;
        }
    }
}

class HealthPlanMKB10TreatmentCourse
{
    /**
     * @var int
     */
    var $ID;
    
    
    
    
    /**
     * @var HealthPlanMKB10TreatmentAdditions[]
     */
    var $addiotions_Anamnes;
    /**
     * @var HealthPlanMKB10TreatmentAdditions[]
     */
    var $addiotions_Status;
    /**
     * @var HealthPlanMKB10TreatmentAdditions[]
     */
    var $addiotions_Recomend;
    /**
     * @var HealthPlanMKB10TreatmentAdditions[]
     */
    var $addiotions_Epicrisis;
    
    
    /**
     * @var int
     */
    var $VISIT;
    /**
     * @var HealthPlanMKB10ToothExam
     */
    var $Befor;
    /**
     * @var HealthPlanMKB10ToothExam
     */
    var $After;
    /**
     * @var HealthPlanMKB10Diagnos2[]
     */
    var $Diagnos;
    /**
     * @var string
     */
    var $PatientName;
    /**
     * @var HealthPlanMKB10TreatmentObject[]
     */
    var $TreatmentObjects;
    /**
     *@var string
     */
    var $ProtocolAnamnes;
    /**
     *@var string
     */
    var $ProtocolStatus;
    /**
     *@var string
     */
    var $ProtocolRecomend;
    /**
     *@var string
     */
    var $ProtocolRentgen;
    /**
     *@var string
     */
    var $ProtocolEpicrisis;
}
class HealthPlanMKB10TreatmentObject
{
    /**
     * @var int
     */
    var $ID;
    /**
     * @var HealthPlanMKB10Diagnos2
     */
    var $Diagnos;
    /**
     * @var int
     */
    var $TOOTH;
    /**
     * @var string
     */
    var $TOOTHSIDE;
    /**
     * @var string
     */
    var $TOOTHROOT;
    /**
     * @var string
     */
    var $TOOTHCHANNEL;

    /**
     * HealthPlanMKB10TreatmentObject::isExist()
     * 
     * @param HealthPlanMKB10TreatmentObject[] $TreatObjects
     * @return bool
     */
    public function isExist($TreatObjects)
    {
        if ($this->TOOTHSIDE == null)
            $this->TOOTHSIDE = "";
        foreach ($TreatObjects as $to)
        {
            if ($to->TOOTHSIDE == null)
                $to->TOOTHSIDE = "";
            if ($to->TOOTH == $this->TOOTH && $to->TOOTHSIDE == $this->TOOTHSIDE)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * HealthPlanMKB10TreatmentObject::addSide()
     * 
     * @param string $side
     * @return void
     */
    public function addSide($side)
    {
        if ($side == null || $side == "")
            return;
        if ($this->TOOTHSIDE == null || $this->TOOTHSIDE == "")
        {
            $this->TOOTHSIDE = $side;
            return;
        }
        foreach (explode(',', $side) as $s)
        {
            $exists = false;
            foreach (explode(',', $this->TOOTHSIDE) as $ts)
            {
                if ($s == $ts)
                    $exists = true;
            }
            if (!$exists)
            {
                if (count($this->TOOTHSIDE) != 0)
                    $this->TOOTHSIDE = $this->TOOTHSIDE . ',';
                $this->TOOTHSIDE = $this->TOOTHSIDE . $s;
            }
        }
    }

    /**
     * HealthPlanMKB10TreatmentObject::delExistSide()
     * 
     * ���������� true ���� ������ �������� �� ������, false - ���� ������ �������� ��������.
     * ��� �� true, ���� ���������� ������ ��� ������
     * 
     * @param string $side
     * @return bool
     */
    public function delExistSide($side)
    {
        if ($this->TOOTHSIDE == null || $this->TOOTHSIDE == "")
            return true;
        $thisSides = explode(",", $this->TOOTHSIDE);
        for ($i = 0; $i < count($thisSides); $i++)
        {
            foreach (explode(",", $side) as $s)
            {
                //echo "<br>$s & " . $thisSides[$i];
                if ($thisSides[$i] == $s)
                {
                    //echo "asdf";
                    unset($thisSides[$i]);
                    break;
                }
            }
        }
        if (count($thisSides) == 0)
            return false;
        foreach ($thisSides as $key => $s)
        {
            if ($key == 0)
                $this->TOOTHSIDE = $s;
            else
                $this->TOOTHSIDE = $this->TOOTHSIDE . "," . $s;
        }
        return true;
    }
}
class HealthPlanMKB10TreatmentDiagnos extends HealthPlanMKB10Diagnos2
{
    /**
     *@var int
     */
    var $Status;
    /**
     *@var float
     */
    var $PlanSumma;
    /**
     *@var float
     */
    var $FactSumma;
    /**
     *@var float
     */
    var $BalanceSumma;
    /**
     *@var int
     */
    var $procedures_count;
    /**
     *@var HealthPlanMKB10Template[]
     */
    var $templates;
    
    /**
     * @var int
     */
    var $TOOTH_MIN;
    
}

///************************************************************************************************///
///                       Assistant-Doctor CLASSES                                                    ///
///                                START                                                           ///
///************************************************************************************************///
class HealthPlanMKB10ClinicPersonal
{
   /**
   * @var HealthPlanMKB10Doctor[]
   */
	var $assistants;
   /**
   * @var HealthPlanMKB10Assistant[]
   */
	var $doctors;  
}
class HealthPlanMKB10Assistant
{
   /**
   * @var int
   */
	var $assid;
   /**
   * @var string
   */
	var $name;  
}

class HealthPlanMKB10Doctor
{
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $docid;
    /**
     * @var string
     */
    var $healthtype;
}
///************************************************************************************************///
///                       Assistant-Doctor CLASSES                                                    ///
///                                END                                                           ///
///************************************************************************************************///

///************************************************************************************************///
///                       TREATMENTPROC CLASSES                                                    ///
///                                START                                                           ///
///************************************************************************************************///
class HealthPlanMKB10ModuleTreatmentProc
{
    /**
     * @var string
     */
    var $procID;

    /**
     * @var int
     */
    var $visit;

    /**
     * @var string
     */
    var $procShifr;

    /**
     * @var string
     */
    var $planName;
    
    /**
     * @var string
     */
    var $label;
    
    /**
     * @var string
     */
    var $courseName;

    /**
     * @var number
     */
    var $procPrice;

    /**
     * @var int
     */
    var $procTime;

    /**
     * @var number
     */
    var $procUOP;

    /**
     * @var HealthPlanMKB10ModuleTreatmentFarmsProc[]
     */
    var $farms;

    /**
     * @var string
     */
    var $assistantID;
    
    /**
     * @var string
     */
    var $assistantShortName;
    
    /**
     * @var string
     */
    var $doctorID;

    /**
     * @var string
     */
    var $doctorShortName;

    /**
     * @var string
     */
    var $dateClose;

    /**
     * @var int
     */
    var $toothUse;
    
    /**
     * @var int
     */
    var $procHealthtype;
    
    /**
     * @var number
     */
    var $procExpenses;
    
    /**
     * @var number
     */
    var $proc_count;
    
    /**
     * @var boolean
     */
    var $isSaved;
    /**
     * @var boolean
     */
    var $isComplete;
    /**
     * @var boolean
     */
    var $isChanged = false;
    
    /**
     * @var number
     */
    var $invoice_id;
    /**
     * @var string
     */
    var $invoice_number;
    /**
     * @var string
     */
    var $invoice_createdate;
	/**
	* @var int
	*/
	var $discount_flag;
	/**
	* @var int
	*/
	var $salary_settings;
	/**
	* @var string
	*/
	var $healthproc_fk;
	/**
	* @var string
	*/
	var $order_fk;
	/**
	* @var boolean
	*/
	var $order_fixed;
	/**
	* @var string
	*/
	var $order_fixed_date;
	/**
	* @var string
	*/
	var $complete_examdiag;
    
	/**
	* @var string
	*/
	var $procHealthtypes;
    
    	/**
	* @var string
	*/
	var $NODETYPE;
    
    
    /**
    * @var Form039Module_F39_Val[]
    */
    var $F39;
    
     /**
    * @var Form039Module_F39_Val[]
    */
    var $F39OUT;
    
        /**
     * @var boolean
     */
    var $isOpen;
    
   	/**
	* @var HealthPlanMKB10ModuleTreatmentProc[]
	*/
	var $children;
    
    
    
    
    
	/**
	* @var int
	*/
	var $diagnos_proceduresCount;
    
    /**
     * @var number
     */
    var $diagnos_factSumm;
    /**
     * @var number
     */
    var $diagnos_planSumm;
}

class HealthPlanMKB10ModuleTreatmentFarmsProc
{
    /**
     * @var string
     */
    var $ID;

    /**
     * @var string
     */
    var $farmID;

    /**
     * @var string
     */
    var $farmName;

    /**
     * @var string
     */
    var $farmGroup;

    /**
     * @var number
     */
    var $farmQuantity;

    /**
     * @var string
     */
    var $farmUnitofmeasure;

    /**
     * @var number
     */
    var $farmPrice;
    
    
    /**
     * @var string
     */
     var $cabinet_fk;
}
///************************************************************************************************///
///                       TREATMENTPROC CLASSES                                                    ///
///                                END                                                             ///
///************************************************************************************************///


class HealthPlanMKB10Module
{
    /**
     * @param HealthPlanMKB10MKB $p1
     * @param HealthPlanMKB10TreatmentDiagnos $p2
     * @param HealthPlanMKB10Diagnos2 $p3
     * @param HealthPlanMKB10Doctor $p4
     * @param HealthPlanMKB10Patient $p5
     * @param HealthPlanMKB10ToothExam $p6
     * @param HealthPlanMKB10Comment $p7
     * @param HealthPlanMKB10MKBProtocol $p8
     * @param HealthPlanMKB10TreatmentAdditions $p9
     * @param HealthPlanMKB10Template $p10
     * @param HealthPlanMKB10TreatmentCourse $p11
     * @param HealthPlanMKB10TreatmentObject $p12
     * @param HealthPlanMKB10ModuleTreatmentFarmsProc $p13
     * @param HealthPlanMKB10ModuleTreatmentProc $p14
     * @param HealthPlanMKB10ClinicPersonal $p15
     * @param HealthPlanMKB10Assistant $p16
     * @param HealthPlanMKB10F43 $p17
     * @param HealthPlanMKB10QuickF43 $p18
     * @return void
     */

    public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10,
        $p11, $p12, $p13, $p14, $p15, $p16, $p17, $p18){}


    private function getExamPrivate($id, $trans)
    {
            $QueryText = "select 
				toothexam.examid,
				toothexam.exam,
				toothexam.examdate,
				toothexam.doctorid,
				doctors.shortname,
				doctors.healthtype,
                toothexam.bitetype
			from toothexam inner join doctors on (toothexam.doctorid = doctors.id)
			where (toothexam.examid = $id)";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $retdata = new HealthPlanMKB10ToothExam;
            if ($row = ibase_fetch_row($result))
            {
                $retdata->ID = $row[0];
                $retdata->exam = $row[1];
                $retdata->examdate = $row[2];
                $retdata->Doctor = new HealthPlanMKB10Doctor;
                $retdata->Doctor->docid = $row[3];
                $retdata->Doctor->name = $row[4];
                $retdata->Doctor->healthtype = $row[5];
                $retdata->bittype = $row[6];
            }
            ibase_free_query($query);
            ibase_free_result($result);
        return $retdata;
    }

    /**
     * @param int $id
     * @return HealthPlanMKB10ToothExam
     */
    public function getExam($id)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select 
				toothexam.examid,
				toothexam.exam,
				toothexam.examdate,
				toothexam.doctorid,
				doctors.shortname,
				doctors.healthtype,
                toothexam.bitetype
			from toothexam inner join doctors on (toothexam.doctorid = doctors.id)
			where (toothexam.examid = $id)";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $retdata = new HealthPlanMKB10ToothExam;
            if ($row = ibase_fetch_row($result))
            {
                $retdata->ID = $row[0];
                $retdata->exam = $row[1];
                $retdata->examdate = $row[2];
                $retdata->Doctor = new HealthPlanMKB10Doctor;
                $retdata->Doctor->docid = $row[3];
                $retdata->Doctor->name = $row[4];
                $retdata->Doctor->healthtype = $row[5];
                $retdata->bittype = $row[6];
            }
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
        return $retdata;
    }
    /**
     * @return HealthPlanMKB10Patient[]
     */
    public function getPatients()
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select ID, SHORTNAME from PATIENTS where PRIMARYFLAG = 1";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10Patient;
                $obj->id = $row[0];
                $obj->name = $row[1];
                $rows[] = $obj;
            }
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
        return $rows;
    }

    /**
     * ������� ���������� ���������
     * 
     * @param int $F43ID, ���� 0, �� ���������� ��� ���������
     * @return HealthPlanMKB10MKBProtocol[]
     */
    public function getProtocols($F43ID, $specialities)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            /* *************** WITH OLD TREATMENTCONSTRUCTOR *******************************
            $QueryText = "select 
                    protocols.id,
                    protocols.name,
                    protocols.protocoltxt,
                    mkb10.id,
                    mkb10.parentid,
                    mkb10.shifr,
                    mkb10.name,
                    protocols.f43
                from protocols inner join mkb10 on (mkb10.id = protocols.mkb10)";
            if ($F43ID != 0)
                $QueryText = $QueryText . "where (protocols.f43 = $F43ID)";
			$QueryText.=" order by protocols.name";
             *************** WITH OLD TREATMENTCONSTRUCTOR *******************************/
            $QueryText = "select 
                    protocols.id,
                    protocols.name,
                    protocols.protocoltxt,
                    mkb10.id,
                    mkb10.parentid,
                    mkb10.shifr,
                    mkb10.name,
                    protocols.f43,
                    protocols.healthtype                    
                from protocols 
                left join mkb10 
                on (mkb10.id = protocols.mkb10)
                where (protocols.f43 = $F43ID)";
            
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10MKBProtocol;
                $obj->ID = $row[0];
                $obj->NAME = $row[1];
                $txtBlob = null;
                $blob_info = ibase_blob_info($row[2]);
                if ($blob_info[0] != 0)
                {
                    $blob_hndl = ibase_blob_open($row[2]);
                    $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                    ibase_blob_close($blob_hndl);
                }
                $obj->PROTOCOLTXT = $txtBlob;
                $obj->MKB10 = new HealthPlanMKB10MKB;
                $obj->MKB10->ID = $row[3];
                $obj->MKB10->PARENTID = $row[4];
                $obj->MKB10->SHIFR = $row[5];
                $obj->MKB10->NAME = $row[6];
                $obj->F43 = $row[7];
                $obj->healthtype = $row[8];
                $rows[] = $obj;
            }
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
        return $rows;
    }

    /**
     * @param int $trcourse
     * @param bool $trsumma
     * @param bool $istoothsort
     * @param int $priceIndex
     * @return HealthPlanMKB10TreatmentDiagnos[]
     */
    public function getDiagnosTreatment($trcourse, $trsumma, $istoothsort, $priceIndex)
    {
            //			$debugdata = array();
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            if ($trsumma == true)
            {
                $QueryText = "select
                            diagnos2.diagnoslabel,
                            template.id,
                            template.name,
                            protocols.id,
                            protocols.name,
                            protocols.healthtype,
                            diagnos2.id,
                            diagnos2.diagnosdate,
                            diagnos2.stacknumber,
                            protocols.anamnestxt,
                            protocols.statustxt,
                            protocols.recomendtxt,
                            
                            protocols.rentgentxt,
                            protocols.epicrisistxt,
                            
                            (select count(*) from  treatmentproc where treatmentproc.diagnos2 = diagnos2.id),	
                            													
                            case
                                 when (select count(*) from treatmentproc where dateclose is not null and treatmentproc.diagnos2 = diagnos2.id)=0
                                 then 0
                                 else case
                                 when ((select count(*) from treatmentproc where treatmentproc.diagnos2 = diagnos2.id)-
                                 (select count(*) from treatmentproc where dateclose is not null and treatmentproc.diagnos2 = diagnos2.id))=0
                                    then 2
                                    else 1
                                    end
                            end status,
                            
                            coalesce((select sum(price*proc_count) from  treatmentproc where treatmentproc.diagnos2 = diagnos2.id),0) 
                            as plansumma,
                            
                            coalesce((select sum(price*proc_count) from  treatmentproc where dateclose is not null and treatmentproc.diagnos2 = diagnos2.id),0) 
                            as factsumma,
                            
                            coalesce((select sum(price*proc_count) from  treatmentproc where dateclose is null and treatmentproc.diagnos2 = diagnos2.id),0) 
                            as balancesumma,
                            
							doctors.shortname,
							doctors.id,														
							doctors.healthtype,
                            diagnos2.teeth   
                                                    
                        from diagnos2
                           left join template on (diagnos2.template = template.id)
                           left join protocols on (diagnos2.protocol_fk = protocols.id)
													 left join doctors on (doctors.id = diagnos2.doctor)
                           where diagnos2.treatmentcourse = $trcourse";
                           if($istoothsort == false)//sort by tooth number
                           {
                                $QueryText .= " order by stacknumber";
                           }
            } else
            {
                $QueryText = "select
                                diagnos2.diagnoslabel,
                                template.id,
                                template.name,
                                protocols.id,
                                protocols.name,
                                protocols.healthtype,
                                diagnos2.id,
                                diagnos2.diagnosdate,
                                diagnos2.stacknumber,
                                protocols.anamnestxt,
                                protocols.statustxt,
                                protocols.recomendtxt,
                                
                                protocols.rentgentxt,
                                protocols.epicrisistxt,
                                
                                (select count(*) from  treatmentproc where treatmentproc.diagnos2 = diagnos2.id)                              
                             from diagnos2
                               left join template on (diagnos2.template = template.id)
                               left join protocols on (diagnos2.protocol_fk = protocols.id)
                               where diagnos2.treatmentcourse = $trcourse";
                               if($istoothsort == false)//sort by tooth number
                               {
                                    $QueryText .= " order by stacknumber";
                               }
            }
            // return $QueryText;
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10TreatmentDiagnos;
                $obj->Template = new HealthPlanMKB10Template;
                $obj->Template->ID = $row[1];
                $obj->Template->NAME = $row[2];
                $obj->protocol = new HealthPlanMKB10MKBProtocol;
                $obj->protocol->ID = $row[3];
                if($row[4] == null || $row[4] == "")
                {
                    $obj->protocol->NAME = $row[0];
                }
                else
                {
                    $obj->protocol->NAME = $row[4];
                }
                $obj->protocol->healthtype = $row[5];
                $obj->ID = $row[6];
                $obj->DIAGNOSDATE = $row[7];
                $obj->STACKNUMBER = $row[8];
                $obj->TreatmentObjects = $this->getTreatmentObjects($row[6], $trans);
                $obj->protocol->ANAMNESTXT = text_blob_encode($row[9]);
                $obj->protocol->STATUSTXT = text_blob_encode($row[10]);
                $obj->protocol->RECOMENDTXT = text_blob_encode($row[11]);
                
                $obj->protocol->RENTGENTXT = text_blob_encode($row[12]);
                $obj->protocol->EPICRISISTXT = text_blob_encode($row[13]);
                
                
                $obj->procedures_count = $row[14];
                
                $obj->Status = $row[15];
                if ($trsumma == true)
                {
                    $obj->PlanSumma = $row[16];
                    $obj->FactSumma = $row[17];
                    $obj->BalanceSumma = $row[18];
                }
                $obj->TreatmentObjects = $this->getTreatmentObjects($row[6], $trans);
								
								$obj->Doctor = new HealthPlanMKB10Doctor();
								$obj->Doctor->name = $row[19];
								$obj->Doctor->docid = $row[20];
								$obj->Doctor->healthtype = $row[21];						
								$obj->TEETH = $row[22];
                //$obj->DiagnosComments = $this->getDiagnosComments($row[4],$trans);
                
                //get templates
                if($row[3] != null)
                {
            		$sql_templates = "select id, name, 
                                (select sum(healthproc.price".sql_checkPriceIndex($priceIndex).") 
                                from healthproc
                                inner join templateproc on (healthproc.id = templateproc.healthproc)
                                where templateproc.template = template.id)
                                from template where template.protocol = $row[3]";  
                    $query_templates = ibase_prepare($trans, $sql_templates);
                    $result_templates = ibase_execute($query_templates);
                    
            		while ($rowt = ibase_fetch_row($result_templates))
            		{ 
                        $objt = new HealthPlanMKB10Template;
                        $objt->ID = $rowt[0];
                        $objt->NAME = $rowt[1];
                        $objt->cost = $rowt[2];
                        $obj->templates[] = $objt;
                    }
                    ibase_free_query($query_templates);
                    ibase_free_result($result_templates);
                }
                
                $rows[] = $obj;
            }
            if($istoothsort == true)//sort by tooth number
            {
                if(count($rows) > 1)
                {
                    for ($i = 0; $i < count($rows); $i++)
                    {
                        $tooth_min = 1000;
                        for ($j = 0; $j < count($rows[$i]->TreatmentObjects); $j++)
                        {
                            if($tooth_min > $rows[$i]->TreatmentObjects[$j]->TOOTH)
                            {
                                $tooth_min = $rows[$i]->TreatmentObjects[$j]->TOOTH;
                            }    
                        }
                        if($rows[$i]->STACKNUMBER == -1)
                        {
                            $rows[$i]->TOOTH_MIN = -1; 
                        }
                        else
                        {
                            $rows[$i]->TOOTH_MIN = $tooth_min;
                        }
                    }
                }
                usort($rows,'cmp_tooth');
            }

            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
            return $rows;
    }
    
    /**************************************************************************************************
    *************************************          DIAGNOS2            ********************************
    **************************************************************************************************/

    /**
     * @param string $nowDate
     * @return HealthPlanMKB10ClinicPersonal
     */
    public function getDoctors($nowDate)//sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $personal = new HealthPlanMKB10ClinicPersonal();
            //doctors
            $QueryText = "SELECT ID, SHORTNAME, HEALTHTYPE FROM DOCTORS 
            WHERE  doctors.isfired is null or doctors.isfired != 1";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $doc = new HealthPlanMKB10Doctor();
                $doc->docid = $row[0];
                $doc->name = $row[1];
                $doc->healthtype = $row[2];
                $personal->doctors[] = $doc;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            //assistatnts
            $QueryText = "SELECT ID, SHORTNAME FROM ASSISTANTS 
            WHERE  ASSISTANTS.isfired is null or ASSISTANTS.isfired != 1";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $asst = new HealthPlanMKB10Assistant();
                $asst->assid = $row[0];
                $asst->name = $row[1];
                $personal->assistants[] = $asst;
            }
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
            return $personal;
    }

    /* *
     * @param int $examId
     * @return HealthPlanMKB10Diagnos2[]
     *
    private function getDiagnos2($examId, $trans)
    {
            $QueryText = "select 
                diagnos2.id,
                diagnos2.diagnosdate,
                diagnos2.garantdate,
                diagnos2.stacknumber,
                diagnos2.treatmentcourse,
                diagnos2.doctor,
                doctors.shortname,
                diagnos2.template,
                template.name,
                protocols.id,
                protocols.name,
                protocols.f43,
                protocols.healthtype,
                mkb10.id,
                mkb10.shifr,
                mkb10.name,
                diagnos2.f43
            from diagnos2
               inner join template on (template.id = diagnos2.template)
               inner join treatmentcourse on (diagnos2.treatmentcourse = treatmentcourse.id)
               inner join protocols on (diagnos2.protocol_fk = protocols.id)
               left join mkb10 on (protocols.mkb10 = mkb10.id)
               inner join doctors on (doctors.id = diagnos2.doctor)
            where treatmentcourse.toothexambefore = $examId";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $diags = array();
        while ($row = ibase_fetch_row($result))
        {
            $diag = new HealthPlanMKB10Diagnos2;
            $diag->ID = $row[0];
            $diag->DIAGNOSDATE = $row[1];
            $diag->EXAMID = $examId;
            $diag->GARANTDATE = $row[2];
            $diag->STACKNUMBER = $row[3];
            $diag->TREATMENTCOURSE = $row[4];
            $diag->Doctor = new HealthPlanMKB10Doctor;
            $diag->Doctor->docid = $row[5];
            $diag->Doctor->name = $row[6];
            $diag->Template = new HealthPlanMKB10Template;
            $diag->Template->ID = $row[7];
            $diag->Template->NAME = $row[8];
            $diag->DiagnosComments = $this->getDiagnosComments($diag->ID, $trans);
            $diag->TreatmentObjects = $this->getTreatmentObjects($diag->ID, $trans);
            $protocol = new HealthPlanMKB10MKBProtocol;
            $protocol->ID = $row[9];
            $protocol->NAME = $row[10];
            ///$protocol->F43 = $row[11];
            if(($row[9] != null)&&($row[9] != 'null'))
            {
          		$this->getProtocolF43list($row[9], $trans);
            }
            $protocol->healthtype = $row[12];
            $protocol->MKB10 = new HealthPlanMKB10MKB;
            $protocol->MKB10->ID = $row[13];
            $protocol->MKB10->SHIFR = $row[14];
            $protocol->MKB10->NAME = $row[15];
            $diag->protocol = $protocol;
            $diag->f43 = $row[16];
            $diags[] = $diag;
        }
        ibase_free_query($query);
        ibase_free_result($result);
        return $diags;
    }*/
    /**
     * @param int $examId
     * @param int $priceIndex
     * @return HealthPlanMKB10Diagnos2[]
     */
    private function getDiagnos3($examId, $priceIndex, $trans)
    {
            /*$QueryText = "select 
                diagnos2.id,
                diagnos2.diagnosdate,
                diagnos2.garantdate,
                diagnos2.stacknumber,
                diagnos2.treatmentcourse,
                diagnos2.doctor,
                doctors.shortname,
                diagnos2.template,
                template.name,
                protocols.id,
                protocols.name,
                protocols.f43,
                protocols.healthtype,
                mkb10.id,
                mkb10.shifr,
                mkb10.name,
                diagnos2.f43,
                (select sum(healthproc.price) 
                            from healthproc
                            inner join templateproc on (healthproc.id = templateproc.healthproc)
                            where templateproc.template = template.id),
                diagnos2.teeth          
            from diagnos2
               inner join template on (template.id = diagnos2.template)
               inner join treatmentcourse on (diagnos2.treatmentcourse = treatmentcourse.id)
               inner join toothexam on (toothexam.treatmentcourse_fk = treatmentcourse.id)
               inner join protocols on (diagnos2.protocol_fk = protocols.id)
               left join mkb10 on (protocols.mkb10 = mkb10.id)
               inner join doctors on (doctors.id = diagnos2.doctor)
            where toothexam.examid = $examId";*/
            $QueryText = "select 
                diagnos2.id,
                diagnos2.diagnosdate,
                diagnos2.garantdate,
                diagnos2.stacknumber,
                diagnos2.treatmentcourse,
                diagnos2.doctor,
                (select doctors.shortname from doctors where doctors.id = diagnos2.doctor),
                diagnos2.template,
                (select template.name from template where template.id = diagnos2.template),
                protocols.id,
                protocols.name,
                protocols.f43,
                protocols.healthtype,
                'mkb10.id',
                'mkb10.shifr',
                'mkb10.name',
                diagnos2.f43,
                (select sum(healthproc.price".sql_checkPriceIndex($priceIndex).") 
                            from healthproc
                            inner join templateproc on (healthproc.id = templateproc.healthproc)
                            where templateproc.template = diagnos2.template),
                diagnos2.teeth,
                toothexam.examdate         
            from diagnos2
               inner join toothexam on (toothexam.treatmentcourse_fk = diagnos2.treatmentcourse )
               inner join protocols on (diagnos2.protocol_fk = protocols.id)
            where toothexam.examid = $examId";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $diags = array();
        while ($row = ibase_fetch_row($result))
        {
            $diag = new HealthPlanMKB10Diagnos2;
            $diag->ID = $row[0];
            $diag->DIAGNOSDATE = $row[1];
            $diag->EXAMID = $examId;
            $diag->GARANTDATE = $row[2];
            $diag->STACKNUMBER = $row[3];
            $diag->TREATMENTCOURSE = $row[4];
            $diag->Doctor = new HealthPlanMKB10Doctor;
            $diag->Doctor->docid = $row[5];
            $diag->Doctor->name = $row[6];
            $diag->Template = new HealthPlanMKB10Template;
            $diag->Template->ID = $row[7];
            $diag->Template->NAME = $row[8];
            $diag->Template->cost = $row[17];            
            $diag->DiagnosComments = $this->getDiagnosComments($diag->ID, $trans);
            $diag->TreatmentObjects = $this->getTreatmentObjects($diag->ID, $trans);
            
            for ($i = 0; $i < count($diags); $i++)
            {
                $tooth_min = 1000;
                for ($j = 0; $j < count($diag->TreatmentObjects); $j++)
                {
                    if($tooth_min > $diag->TreatmentObjects[$j]->TOOTH)
                    {
                        $tooth_min = $diag->TreatmentObjects[$j]->TOOTH;
                    }    
                }
                $diag->TOOTH_MIN = $tooth_min;
            }
            
            $protocol = new HealthPlanMKB10MKBProtocol;
            $protocol->ID = $row[9];
            $protocol->NAME = $row[10];
            
            //if(($row[9] != null)&&($row[9] != 'null'))
            //{
          	//	$this->getProtocolF43list($row[9], $trans);
            //}
            $protocol->healthtype = $row[12];
            $protocol->MKB10 = new HealthPlanMKB10MKB;
            $protocol->MKB10->ID = $row[13];
            $protocol->MKB10->SHIFR = $row[14];
            $protocol->MKB10->NAME = $row[15];
            $diag->protocol = $protocol;
            $diag->f43 = $row[16];
            $diag->TEETH = $row[18];
            $diag->EXAMDATE = $row[19];
            $diags[] = $diag;
        }
        ibase_free_query($query);
        ibase_free_result($result);
        return $diags;
    }
    
    /**
     * @param int $protocolId
     * @return HealthPlanMKB10F43[]
     */
    private function getProtocolF43list($protocolId, $trans)
    {
        $QueryF43Text = "select f43.f43, 
                                f43.id, 
                                protocols_f43.id, 
                                protocols_f43.protocol_fk
                                        from f43
                                        inner join protocols_f43 on f43.id = protocols_f43.f43_fk
                                        where protocols_f43.protocol_fk = $protocolId";				
            		$queryF43 = ibase_prepare($trans, $QueryF43Text);
            		$resultF43 = ibase_execute($queryF43);
        $f43list = array();	
            		while ($rowF43 = ibase_fetch_row($resultF43))
            		{
            		  $f43_obj = new HealthPlanMKB10F43;
                      $f43_obj->f43 = $rowF43[0];
                      $f43_obj->f43_id = $rowF43[1];
                      $f43_obj->id = $rowF43[2];
                      $f43_obj->protocol_id = $rowF43[3];
            		  $f43list[] = $f43_obj;
  		            }
        ibase_free_query($queryF43);
        ibase_free_result($resultF43);
        return $f43list; 
    }

    /**
     * @param int $examId
     * @param boolean $istoothsort
     * @param int $priceIndex
     * @return HealthPlanMKB10Diagnos2[]
     */
    public function createDiagnos2($examId, $specialities, $istoothsort, $priceIndex)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $exam = $this->getExamPrivate($examId, $trans);
            $xml = new SimpleXMLElement("<teeth>" . $exam->exam . "</teeth>");
            $diags = array();
            $MAXINT = 2147483647;
            foreach ($xml as $tooth)
            {
                switch ($tooth->getName())
                {
                    case "zub":
                        foreach ($tooth->children() as $t)
                        {
                            //if ($t == "1" || $t == "7" || $t == "8" || $t == "9" || $t == "19" || $t == "24" ||
                            //    $t == "27")
                            //{
                                $diag2 = new HealthPlanMKB10Diagnos2;
                                $diag2->EXAMID = $exam->ID;
                                $diag2->ID = $MAXINT;
                                $diag2->TREATMENTCOURSE = $MAXINT;
                                //$diag2->Template = new HealthPlanMKB10Template();
                                //$diag2->Template->ID = $MAXINT;
                                //$diag2->protocol = new HealthPlanMKB10MKBProtocol();
                                //$diag2->protocol->ID = $MAXINT;
                                //$diag2->protocol->MKB10 = new HealthPlanMKB10MKB();
                                //$diag2->protocol->MKB10->ID = $MAXINT;
                                //$diag2->protocol->F43 = (int)$t;
                                $diag2->f43 = (int)$t;
                                $to = new HealthPlanMKB10TreatmentObject;
                                $to->ID = $MAXINT;
                                $to->TOOTH = (int)$tooth['id'];
                                //$to->TOOTHSIDE = ($t->getName() == "Z") ? "" : $t->getName();
                                $to->TOOTHSIDE = $t->getName();
                                $diag2->TreatmentObjects = array($to);
                                $diag2->EXAMDATE = $exam->examdate;
                                $diags[] = $diag2;
                            //}
                        }
                        break;
                    case "GP1":
                    case "GP2":
                        $diag2 = new HealthPlanMKB10Diagnos2;
                        $diag2->EXAMID = $exam->ID;
                        $diag2->ID = $MAXINT;
                        $diag2->TREATMENTCOURSE = $MAXINT;
                        //$diag2->Template = new HealthPlanMKB10Template();
                        //$diag2->Template->ID = $MAXINT;
                        //$diag2->protocol = new HealthPlanMKB10MKBProtocol();
                        //$diag2->protocol->ID = $MAXINT;
                        //$diag2->protocol->MKB10 = new HealthPlanMKB10MKB();
                        //$diag2->protocol->MKB10->ID = $MAXINT;
                        //$diag2->protocol->F43 = ($tooth == '20' || $tooth == '21') ? 9:24;
                        if($tooth == '20' || $tooth == '21')
                        {
                           $diag2->f43 = 9; 
                        }
                        else if($tooth == '22' || $tooth == '23')
                        {
                           $diag2->f43 = 24;  
                        }
                        //$diag2->f43 = ($tooth == '20' || $tooth == '21') ? 9:24;
                        if ($tooth == '20' || $tooth == '22')
                        { //general for 4 quarters
                            if ($tooth->getName() == 'GP1')//top teethes
                            {
                                if($exam->bittype == 1)//milk bite
                                {  
                                    for ($i = 5; $i >= 1; $i--)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 50 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    }
                                    for ($i = 1; $i <= 5; $i++)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 60 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    }
                                }
                                else//static bite
                                { 
                                    for ($i = 8; $i >= 1; $i--)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 10 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    }
                                    for ($i = 1; $i <= 8; $i++)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 20 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    }
                                }
                            } else//bottom teethes
                            {
                                if($exam->bittype == 1)//milk bite
                                {   
                                    for ($i = 5; $i >= 1; $i--)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 70 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    }
                                    for ($i = 1; $i <= 5; $i++)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 80 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    } 
                                }
                                else//static bite
                                {
                                    for ($i = 8; $i >= 1; $i--)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 30 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    }
                                    for ($i = 1; $i <= 8; $i++)
                                    {
                                        $to = new HealthPlanMKB10TreatmentObject;
                                        $to->ID = $MAXINT;
                                        $to->TOOTH = 40 + $i;
                                        $to->TOOTHSIDE = "D1";
                                        $diag2->TreatmentObjects[] = $to;
                                    } 
                                }
                            }
                        } else
                        { //frontal for 6 front teethes
                        
                            if($exam->bittype == 1)//milk bite
                            {
                                for ($i = 1; $i <= 4; $i++)
                                {
                                    $to = new HealthPlanMKB10TreatmentObject;
                                    $to->ID = $MAXINT;
                                    if ($tooth->getName() == 'GP1')
                                    {
                                        $snd = ($i <= 2) ? $i : ($i - 2);
                                        $fst = ($i <= 2) ? 50 : 60;
                                        $to->TOOTH = $fst + $snd;
                                        $to->TOOTHSIDE = "D1";
                                    }
                                    else
                                    {
                                        $snd = ($i <= 2) ? $i : ($i - 2);
                                        $fst = ($i <= 2) ? 80 : 70;
                                        $to->TOOTH = $fst + $snd;
                                        $to->TOOTHSIDE = "D1";
                                    }
                                    $diag2->TreatmentObjects[] = $to;
                                }       
                            }
                            else
                            {
                                for ($i = 1; $i <= 6; $i++)
                                {
                                    $to = new HealthPlanMKB10TreatmentObject;
                                    $to->ID = $MAXINT;
                                    $fst = ($i <= 3) ? 1 : 2;
                                    if ($tooth->getName() == 'GP2')
                                        $fst = $fst + 2;
                                    $snd = ($i <= 3) ? $i : ($i - 3);
                                    $to->TOOTH = $fst * 10 + $snd;
                                    $to->TOOTHSIDE = "D1";
                                    $diag2->TreatmentObjects[] = $to;
                                }   
                            }
                        }
                        
                        $diag2->EXAMDATE = $exam->examdate;
                        $diags[] = $diag2;
                        break;
                    default:
                        continue;
                }
            }
            
           
            
            $diags = $this->defaultDiagnosGrouping($diags);
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($diags,true));
                        
            $diags = $this->specDiagnos($diags, false, $trans, $examId, $specialities, $priceIndex);
            
            if($istoothsort == true)//sort by tooth number
            {
                if(count($diags) > 1)
                {
                    for ($i = 0; $i < count($diags); $i++)
                    {
                        if(isset($diags[$i]))
                        {
                            $tooth_min = 1000;
                            for ($j = 0; $j < count($diags[$i]->TreatmentObjects); $j++)
                            {
                                if($tooth_min > $diags[$i]->TreatmentObjects[$j]->TOOTH)
                                {
                                    $tooth_min = $diags[$i]->TreatmentObjects[$j]->TOOTH;
                                }    
                            }
                            $diags[$i]->TOOTH_MIN = $tooth_min;
                        }
                    }
                }
                usort($diags,'cmp_tooth');
            }
            else
            {
                usort($diags,'cmp_d2_diagnosdate');
            }
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
                       
            return $diags;
    }

    
    
    /**
     * HealthPlanMKB10Module::defaultDiagnosGrouping()
     * 
     * @param HealthPlanMKB10Diagnos2[] $diags
     * @return HealthPlanMKB10Diagnos2
     */
    private function defaultDiagnosGrouping($diags)
    {
        $n = count($diags);
        foreach ($diags as $i => $d)
        {
            if(!isset($diags[$i]))
            {
                continue;
            }
            if($diags[$i]->f43 == 19 || $diags[$i]->f43 == 27 || $diags[$i]->f43 == 24 || $diags[$i]->f43 == 20 || $diags[$i]->f43 == 22 || 
                    $diags[$i]->f43 == 21 || $diags[$i]->f43 == 23)
            {
                for ($j = $i + 1; $j < $n; $j++)
                {
                    if (!isset($diags[$j]))
                        continue;
                    if ($diags[$i]->f43 == $diags[$j]->f43)
                    {
                        $diags[$i]->addTreatmentObjects($diags[$j]->TreatmentObjects);
                        unset($diags[$j]);
                    }
                } 
            }
            elseif ($diags[$i]->f43 == 1 || $diags[$i]->f43 == 2 || $diags[$i]->f43 == 3 || $diags[$i]->f43 == 4 ||
                    $diags[$i]->f43 == 6 || $diags[$i]->f43 == 14 || $diags[$i]->f43 == 26 || $diags[$i]->f43 == 30 || $diags[$i]->f43 == 32 || 
                    $diags[$i]->f43 == 33 || $diags[$i]->f43 == 34)
            {
                    for ($j = $i + 1; $j < $n; $j++)
                    {
                        if (!isset($diags[$j]))
                            continue;
                        if ($diags[$i]->f43 == $diags[$j]->f43)
                        {
                            $diff = true;
                            foreach ($diags[$i]->TreatmentObjects as $toi)
                            {
                                foreach ($diags[$j]->TreatmentObjects as $toj)
                                {
                                    if ($toi->TOOTH == $toj->TOOTH)
                                        $diff = false;
                                }
                            }
                            if (!$diff)
                            {
                                $diags[$i]->addTreatmentObjects($diags[$j]->TreatmentObjects);
                                unset($diags[$j]);
                            }
                        }
                    }
            }
        }
        return $diags;
    }

    /**
     * @param HealthPlanMKB10Diagnos2[] $diags
     * @param bool $all
     * @param int $priceIndex
     * @return HealthPlanMKB10Diagnos2[]
     */
    public function specificationDiagnos($diags, $all, $priceIndex)
    {
        if ($diags != null && count($diags) != 0)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $diags = $this->specDiagnos($diags, $all, $trans, $diags[0]->EXAMID, null, $priceIndex);
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $diags;
    }

    private function specDiagnos($diags, $all, $trans, $examid, $specialities, $priceIndex)
    {
       
        //if ($diags == null || count($diags) == 0)
        //{
        //    return $diags;
        //}
        //else
        //{
        $signDiags = $all ? array() : $this->getDiagnos3($examid, $priceIndex, $trans);
        
        if (!$all)
        {
            $diags = $this->delExistsDiagnoses($signDiags, $diags);
        }
        if ($diags != null && count($diags) != 0)
        {
            /*
            // create temp dictionary by unique F43 START
            usort($diags,'cmp_f43');
            $diags_unique_f43 = array();
            $tmp_f43 = -100;
            foreach ($diags as $diag)
            {
                if($tmp_f43 != $diag->f43)
                {
                    $tmp_f43 = $diag->f43;
                    $diags_unique_f43[] = $diag->f43;
                    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$diag->f43); 
                }
            }
            // create temp dictionary by unique F43 END
            
            
            $protocols_array = array();
            //$arr["x"] = 42;
            $QueryText = "select 
                                    protocols.id,
                                    protocols.name,
                                    protocols.healthtype,
                                    mkb10.id,
                                    mkb10.shifr,
                                    mkb10.name
                                from protocols
            
                                inner join protocols_f43
                                on (protocols.id = protocols_f43.protocol_fk)
                                    
                                inner join f43
                                on (f43.id = protocols_f43.f43_fk)
            
                                left join mkb10
                                on (mkb10.id = protocols.mkb10)
            
                                 where (";
               
                for ($i = 0; $i < count($diags_unique_f43); $i++) 
                {
                    $QueryText.="f43.f43 = ".$diags_unique_f43[$i]." ";
                    if($i < count($diags_unique_f43)-1)
                    {
                        $QueryText.= " or ";
                    }
                }
                $QueryText.= ") ";
                                 
                if(nonull($specialities)&&count($specialities)>0)
                {
                   $QuerySpec="";
                   foreach($specialities as $spec)
                   {
                    	if($QuerySpec!='')
                        {
                            $QuerySpec.=' or ';
                        }
                        $QuerySpec .= "protocols.healthtype=$spec->id"; 
                   }
                   $QueryText.=" and (".$QuerySpec.")";
                }
				$QueryText.="order by protocols.name";
                //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);     
                $query = ibase_prepare($trans, $QueryText);
                $result = ibase_execute($query);
            */
            
            $tmp_f43 = -100;
            $protocols = array();// = new HealthPlanMKB10MKBProtocol;
            usort($diags,'cmp_f43');
            foreach ($diags as $d)
            {   
                $d->Protocols = array();
                if($tmp_f43 != $d->f43)
                {
                    $tmp_f43 = $d->f43;
                    /*$QueryText = "select 
                                    protocols.id,
                                    protocols.name,
                                    protocols.healthtype,
                                    mkb10.id,
                                    mkb10.shifr,
                                    mkb10.name
                                from protocols
            
                                inner join protocols_f43
                                on (protocols.id = protocols_f43.protocol_fk)
                                    
                                inner join f43
                                on (f43.id = protocols_f43.f43_fk)
            
                                left join mkb10
                                on (mkb10.id = protocols.mkb10)
            
                                 where (f43.f43 = ". $d->f43 . ")";*/
                    $QueryText = "select 
                                    protocols.id,
                                    protocols.name,
                                    protocols.healthtype,
                                    'mkb10.id',
                                    'mkb10.shifr',
                                    'mkb10.name'
                                from protocols
            
                                inner join protocols_f43
                                on (protocols.id = protocols_f43.protocol_fk)
                                    
                                inner join f43
                                on (f43.id = protocols_f43.f43_fk)
            
                                 where (f43.f43 = ". $d->f43 . ")";             
                    if(nonull($specialities)&&count($specialities)>0)
                    {
                       $QuerySpec="";
                       foreach($specialities as $spec)
                       {
                        	if($QuerySpec!='')
                            {
                                $QuerySpec.=' or ';
                            }
                            $QuerySpec .= "protocols.healthtype=$spec->id"; 
                       }
                       $QueryText.=" and (".$QuerySpec.")";
                    }
    				$QueryText.="order by protocols.name";    
                    $query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
                    
                    while ($row = ibase_fetch_row($result))
                    {
                        $prot = new HealthPlanMKB10MKBProtocol;
                        $prot->ID = $row[0];
                        $prot->NAME = $row[1];
                        //$prot->F43 = $d->protocol->F43;
                        $prot->healthtype = $row[2];
                        $prot->MKB10 = new HealthPlanMKB10MKB;
                        $prot->MKB10->ID = $row[3];
                        $prot->MKB10->SHIFR = $row[4];
                        $prot->MKB10->NAME = $row[5];
                        
                        $prot->Templates = array();
    
                        
                        //$prot->F43list = $this->getProtocolF43list($row[0], $trans);
                        
                        $TemplQuery = "select 
                                id, 
                                name, 
                                (select sum(healthproc.price".sql_checkPriceIndex($priceIndex).") from healthproc 
                                inner join templateproc 
                                on (healthproc.id = templateproc.healthproc) where templateproc.template = template.id) 
                                
                                from template where protocol = ".$prot->ID;
                        $templquery = ibase_prepare($trans, $TemplQuery);
                        $templresult = ibase_execute($templquery);
                        while ($templrow = ibase_fetch_row($templresult))
                        {
                            $templ = new HealthPlanMKB10Template;
                            $templ->ID = $templrow[0];
                            $templ->NAME = $templrow[1];
                            $templ->cost = $templrow[2];
                            $templ->Protocol = $prot;
                            
                            $ToolTipSQL = "select 
                            
                                            templateproc.visit, 
                                            (select healthproc.planname from healthproc where templateproc.healthproc = healthproc.id)
                            
                                    from templateproc
                                    where templateproc.template = ".$templ->ID;
                            $ToolTipQuery = ibase_prepare($trans, $ToolTipSQL);
                            $ToolTipResult = ibase_execute($ToolTipQuery);
                            $tooltip = '';
                            while ($tooltiprow = ibase_fetch_row($ToolTipResult))
                            {
                               $tooltip .=  '<font size="13"><b>'.$tooltiprow[0].'</b></font> : '.$tooltiprow[1].'<br/>';
                            }
                            if(($templ->cost != 0)&&($templ->cost != null))
                            {
                                $tooltip .= '<font size="15"><b>∑'.$templ->cost.'</b></font>';
                            }
                            $templ->toolTip = $tooltip;
                            ibase_free_query($ToolTipQuery);
                            ibase_free_result($ToolTipResult);
                            
                            
                            $prot->Templates[] = $templ;
                        }
                        ibase_free_query($templquery);
                        ibase_free_result($templresult);
                        
    
                        $d->Protocols[] = $prot;
                        $protocols = $d->Protocols;
                    }
                    ibase_free_query($query);
                    ibase_free_result($result);
                }
                else
                {
                    $d->Protocols = $protocols;
                }
                
                
                

                    
                    
            }//foreach ($diags as $d) !END!
            
        }
        
        
        foreach ($signDiags as $d)
        {
            $diags[] = $d;
        }
        return $diags;
    }

    /**
     * HealthPlanMKB10Module::delExistsDiagnoses()
     * 
     * @param HealthPlanMKB10Diagnos2[] $signed
     * @param HealthPlanMKB10Diagnos2[] $newDiags
     * @return HealthPlanMKB10Diagnos2[] ���������� ������ ����� �������
     */
    private function delExistsDiagnoses($signed, $newDiags)
    {
        foreach ($newDiags as $key => $d)
        {
            foreach ($signed as $sd)
            {
                if ($d->f43 == $sd->f43)
                {
                    foreach ($d->TreatmentObjects as $key_to => $to)
                    {
                        if ($to->isExist($sd->TreatmentObjects))
                        {
                            unset($d->TreatmentObjects[$key_to]);
                        } else
                        {
                            foreach ($sd->TreatmentObjects as $sto)
                            {
                                if ($to->TOOTH == $sto->TOOTH)
                                {
                                    //echo $to->TOOTH . "    ".$to->TOOTHSIDE . " - ".$sto->TOOTHSIDE."<br />";
                                    if (!$to->delExistSide($sto->TOOTHSIDE))
                                    {
                                        unset($d->TreatmentObjects[$key_to]);
                                    }
                                    //echo $to->TOOTH . "    ".$to->TOOTHSIDE . " - ".$sto->TOOTHSIDE."<br />";
                                }
                            }
                        }
                    }
                }
            }
            if ($d->TreatmentObjects == null || count($d->TreatmentObjects) == 0)
            {
                unset($newDiags[$key]);
            }
        }
        return $newDiags;
    }
    
    /**
     * HealthPlanMKB10Module::getDiagnos2Comments()
     * 
     * @param int $diagnosid
     * @return HealthPlanMKB10Comment[]
     */
    public function getDiagnos2Comments($diagnosid)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $comments = $this->getDiagnosComments($diagnosid, $trans);
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $comments;
    }

    private function getDiagnosComments($diagnos, $trans)
    {  
            $QueryText = "select 
                            diagnos2comment.id,
                            diagnos2comment.commenttxt,
                            diagnos2comment.datecomment,
                            diagnos2comment.doctor,
                            doctors.shortname
                            from diagnos2comment
                        inner join doctors on (diagnos2comment.doctor = doctors.id)    
                        where diagnos2 = $diagnos";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10Comment;
                $obj->ID = $row[0];
                $obj->COMMENTTXT = $row[1];
                $obj->DATECOMMENT = $row[2];
                $obj->Doctor = new HealthPlanMKB10Doctor;
                $obj->Doctor->docid = $row[3];
                $obj->Doctor->name = $row[4];
                $obj->OWNERID = $diagnos;
                $obj->SIGNFLAG = true;
                $rows[] = $obj;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            return $rows;
    }
    
    /**
     * HealthPlanMKB10Module::saveDiagnosComments()
     * 
     * @param HealthPlanMKB10Comment[] $comments
     * @return bool
     */
    public function saveDiagnosComments($comments)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $this->saveDiagComments($comments, $trans);
            $success = ibase_commit($trans);
            $connection->CloseDBConnection();
            return $success;
    }
    
    private function saveDiagComments($comments, $trans)
    {
        if ($comments != null)
            foreach ($comments as $comm)
            {
                $QueryText = "";
                if ($comm->deltype)
                {
                    $QueryText = "delete from DIAGNOS2COMMENT where id = " . $comm->ID;
                } else
                {
                    $QueryText = "update or insert into DIAGNOS2COMMENT values (";
                    $QueryText .= (($comm->ID == 2147483647) ?
                        "(SELECT GEN_ID(gen_diagnos2comment_id, 1) FROM RDB\$DATABASE)" : $comm->ID);
                    $QueryText .= "," . $comm->OWNERID . ",'" .safequery($comm->COMMENTTXT). "'," . ($comm->Doctor == null ? "null" : $comm->Doctor->docid) . ",'" . $comm->DATECOMMENT . "')";
                }
                $debugdata[] = $QueryText;
                $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
                ibase_free_query($query);
            }
    }

    /**
     * @param HealthPlanMKB10Diagnos2[] $diagnoses
     * @return string
     */
   public function saveDiagnos2($diagnoses)
    {
        //$debugdata = array();
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $course = 2147483647;
            $lastDiagId;
            
            
            foreach ($diagnoses as $d)
            {
                if ($course == 2147483647 && $d->TREATMENTCOURSE == 2147483647)
                {
                    //$QueryText = "select treatmentcourse.id from toothexam
                    //    inner join treatmentcourse 
                    //    on (toothexam.examid = treatmentcourse.toothexambefore) and (toothexam.examid = treatmentcourse.toothexamafter)
                    //    where toothexam.examid = " . $d->EXAMID;
                    
                    $QueryText = "select toothexam.treatmentcourse_fk from toothexam
                        where toothexam.examid = " . $d->EXAMID;
                        
                    //$debugdata[] = $QueryText;
                    $query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
                    $row = ibase_fetch_row($result);
                    ibase_free_query($query);
                    ibase_free_result($result);
                    if($row)
                    {
                        $course = $row[0];
                    } 
                    else
                    {
                        //$QueryText = "insert into treatmentcourse(id, toothexambefore, toothexamafter) values ($course,$d->EXAMID,$d->EXAMID)";
                        $QueryText = "insert into treatmentcourse(id) values (null) returning id";
                        //$debugdata[] = $QueryText;
                        $query = ibase_prepare($trans, $QueryText);
                        $result = ibase_execute($query);
                        if($row = ibase_fetch_row($result))
                        {
                            $course = $row[0];   
                        }
                        ibase_free_query($query);
                        ibase_free_result($result);

                        //������� ��� ����� ��������
                        $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,GARANTDATE,STACKNUMBER,PROTOCOL_FK,F43,TEETH) 
                        values (null, $course," . $d->Doctor->docid . ",null,null,null,-1,null,".$d->f43.",".nulcheck($d->TEETH,true).")";
                        //$debugdata[] = $QueryText;
                        $query = ibase_prepare($trans, $QueryText);
                        ibase_execute($query);
                        ibase_free_query($query);
                    }
                }
                if ($d->TREATMENTCOURSE == 2147483647)
                {
                    $d->TREATMENTCOURSE = $course;
                }
                    
                $isNew = $d->ID == 2147483647;
                if ($isNew)
                {
                    $d->ID = 'null';
                }
                $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,GARANTDATE,STACKNUMBER,PROTOCOL_FK,F43,TEETH) 
                        values (".$d->ID.",".$d->TREATMENTCOURSE.",".$d->Doctor->docid.",".
                        (($d->DIAGNOSDATE == "" || $d->DIAGNOSDATE == null) ? "null, " : "'$d->DIAGNOSDATE', ");
                 if( ($d->Template->NAME != '-')) 
                 {
                     $QueryText .= $d->Template->ID.", ";
                 }  
                 else
                 {
                     $QueryText .= "null, ";
                 }     
                        
                $QueryText .= (($d->GARANTDATE == "" || $d->GARANTDATE == null) ? "null" : "'$d->GARANTDATE'") . ",$d->STACKNUMBER,".$d->protocol->ID.",".$d->f43.",".nullcheck($d->TEETH,true).") 
                            returning id";
                //$debugdata[] = $QueryText;
                $query = ibase_prepare($trans, $QueryText);
                 $result = ibase_execute($query);
                 if ($row = ibase_fetch_row($result));
                 {
                    $d->ID = $row[0];
                 }
                 $lastDiagId = $d->ID;
                    
                ibase_free_query($query);
                ibase_free_result($result);
                if ($d->TreatmentObjects != null)
                {
                    foreach ($d->TreatmentObjects as $obj)
                    {
                        $QueryText = "update or insert into TREATMENTOBJECTS values (";
                        $QueryText .= (($obj->ID == 2147483647) ? "null" : $obj->ID);
                        $QueryText .= ",".$d->ID.",".$obj->TOOTH.",'".$obj->TOOTHSIDE.
                            "','".$obj->TOOTHROOT."','".$obj->TOOTHCHANNEL."')";
                        //$debugdata[] = $QueryText;
                         //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
                        $query = ibase_prepare($trans, $QueryText);
                        ibase_execute($query);
                        ibase_free_query($query);
                    }
                }
                if ($d->DiagnosComments != null)
                    foreach ($d->DiagnosComments as $comm)
                    {
                        $comm->OWNERID = $d->ID;
                    }   
                $this->saveDiagComments($d->DiagnosComments, $trans);    
            }
            ibase_commit($trans);
            //$debugdata[] = $success;
            $connection->CloseDBConnection();
            //return $debugdata;
            return $lastDiagId;
    }

  

    
    private function getTreatmentAdditions2($trcourse, $type, $trans)
    {
            $QueryText = "select
                            treatmentadditions.additiontxt,
                            treatmentadditions.signflag,
                            treatmentadditions.doctor,
                            doctors.shortname,
                            treatmentadditions.signdate,
                            treatmentadditions.id,
                            treatmentadditions.additiontype,
                            treatmentadditions.treatmentcourse,
                            treatmentadditions.visit
                        from treatmentadditions
                        inner join doctors on (treatmentadditions.doctor = doctors.id)
                        where treatmentadditions.treatmentcourse = $trcourse
                        and treatmentadditions.additiontype = $type
                        order by treatmentadditions.signdate asc";
            //  return $QueryText;
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10TreatmentAdditions;
                $obj->ADDITINSTXT = text_blob_encode($row[0]);
                if($row[1] == 1)
                {
                    $obj->SIGNFLAG = true;
                }
                else
                {
                    $obj->SIGNFLAG = false;
                }
                //$obj->SIGNFLAG = $row[1];
                $obj->Doctor = new HealthPlanMKB10Doctor;
                $obj->Doctor->docid = $row[2];
                $obj->Doctor->name = $row[3];
                $obj->SIGNDATE = $row[4];
                $obj->ID = $row[5];
                $obj->TYPE = $row[6];
                $obj->TrCourse = $row[7];
                $obj->visit = $row[8];
                $rows[] = $obj;
                //$obj->Comments = $this->getAdditionsComments($row[5], $trans);
            }
            ibase_free_query($query);
            ibase_free_result($result);
            return $rows;
    }


    /**
     * @param int $patient
     * @return HealthPlanMKB10TreatmentCourse[]
     */
    public function getTreatmentCourse($patient)
    {
            //			$debugdata = array();
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            /*$QueryText = "select

                                t.id, 0
                                
                                toothexam.examid, 1 
                                t.examdate, 2
                                t.doctorid, 3
                                t.shortname, 4
                                
                                null, 5
                                toothexam.examdate, 6
                                toothexam.doctorid, 7
                                doctors.shortname, 8
                                
                                patients.shortname 9  
                                
                                from
                            (select
                                treatmentcourse.id,
                                toothexam.examdate,
                                toothexam.doctorid,
                                doctors.shortname,
                                toothexam.examid
                            from treatmentcourse
                               inner join toothexam on (treatmentcourse.id = toothexam.treatmentcourse_fk)
                               inner join doctors on (toothexam.doctorid = doctors.id)
                                 where toothexam.id = $patient and toothexam.afterflag=0)t
                                inner join treatmentcourse on (t.id = treatmentcourse.id)
                                inner join toothexam on (treatmentcourse.id = toothexam.treatmentcourse_fk)
                                inner join doctors on (toothexam.doctorid = doctors.id)
                                inner join patients on (toothexam.id = patients.id)
                            where toothexam.afterflag = 0
                                order by t.examdate desc, t.id desc";  
                             */   
                                
              $QueryText = "select
                                distinct(toothexam.treatmentcourse_fk),
                                (select min(tx.examid) from toothexam tx where tx.treatmentcourse_fk =  toothexam.treatmentcourse_fk), 
                                (select min(tx2.examdate) from toothexam tx2 where tx2.treatmentcourse_fk =  toothexam.treatmentcourse_fk) treatdate,
                                toothexam.doctorid,
                                (select doctors.shortname from doctors where doctors.id = toothexam.doctorid),
                                (select patients.shortname from patients where patients.id = toothexam.id)


                                from  toothexam 
                                    where toothexam.id = $patient
                                    and toothexam.treatmentcourse_fk is not null
                            order by treatdate desc, toothexam.treatmentcourse_fk desc";                  
                                
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10TreatmentCourse;
                $obj->ID = $row[0];
                
                $obj->Befor = new HealthPlanMKB10ToothExam;
                $obj->Befor->ID = $row[1];
                $obj->Befor->examdate = $row[2];
                $obj->Befor->Doctor = new HealthPlanMKB10Doctor;
                $obj->Befor->Doctor->docid = $row[3];
                $obj->Befor->Doctor->name = $row[4];
                
                /*
                $obj->After = new HealthPlanMKB10ToothExam;
                $obj->After->ID = $row[5];
                $obj->After->examdate = $row[6];
                $obj->After->Doctor = new HealthPlanMKB10Doctor;
                $obj->After->Doctor->docid = $row[7];
                $obj->After->Doctor->name = $row[8];
                
                
                $obj->PatientName = $row[9];*/
                $obj->PatientName = $row[5];
                
                
                $obj->addiotions_Anamnes = $this->getTreatmentAdditions2($row[0], 0, $trans);
                $obj->addiotions_Status = $this->getTreatmentAdditions2($row[0], 1, $trans);
                $obj->addiotions_Recomend = $this->getTreatmentAdditions2($row[0], 2, $trans);
                $obj->addiotions_Epicrisis = $this->getTreatmentAdditions2($row[0], 3, $trans);
                
                //$obj->TreatmentObjects = $this->getTreatmentObjects($row[3], $trans);
                
                    $obj->ProtocolAnamnes = $this->getProtocolsASR($row[0], 0, $trans);
                    $obj->ProtocolStatus = $this->getProtocolsASR($row[0], 1, $trans);
                    $obj->ProtocolRecomend = $this->getProtocolsASR($row[0], 2, $trans);
                    $obj->ProtocolRentgen = $this->getProtocolsASR($row[0], 3, $trans);
                    $obj->ProtocolEpicrisis = $this->getProtocolsASR($row[0], 4, $trans);
                
                $rows[] = $obj;
            }
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
            return $rows;
    }

    private function getTreatmentObjects($diagnos, $trans)
    {
            $QueryText = "select 
                        treatmentobjects.id,
                        treatmentobjects.tooth,
                        treatmentobjects.toothside
                    from treatmentobjects
                    where treatmentobjects.diagnos2 = $diagnos";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10TreatmentObject;
                $obj->ID = $row[0];
                $obj->TOOTH = $row[1];
                $obj->TOOTHSIDE = $row[2];
                $rows[] = $obj;
            }
            return $rows;
    }
    
private function getTreatmentObjectsCut($diagnos, $trans)
    {
            $QueryText = "select 
                        treatmentobjects.tooth,
                        treatmentobjects.toothside
                    from treatmentobjects
                    where treatmentobjects.diagnos2 = $diagnos";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array('TOOTH'=>array(),'TOOTHSIDE'=>array());
            while ($row = ibase_fetch_row($result))
            {
                
                
                if(strstr($row[1],','))
                {
                     $sides=explode(',',$row[1]); 
                     foreach($sides as $side)
                     {
                        $rows['TOOTH'][]=$row[0];
                        $rows['TOOTHSIDE'][]=$side;   
                     }  
                }
                else
                {
                  $rows['TOOTH'][]=$row[0];
                  $rows['TOOTHSIDE'][]=$row[1];    
                }
                              
            }
            return $rows;
    }
    
    /**
     * @param Number[] $diag
     * @return boolean
     */
    public function updDiagnos2($diag)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            for ($i = 1; $i <= count($diag); $i++)
            {
                $QueryText = "update DIAGNOS2 set STACKNUMBER = $i
                where diagnos2.id = " . $diag[$i - 1];
                $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
            }
            
            $success = ibase_commit($trans);
            $connection->CloseDBConnection();
            return $success;
    }
    
    /**
     * @param string $diagId
     * @return boolean
     */
    public function delDiagnos2($diagId)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
                $QueryText = "delete from DIAGNOS2
                where id = " . $diagId;
                $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
                
            $success = ibase_commit($trans);
            $connection->CloseDBConnection();
            return $success;
    }
     /**
     * @param string $templateID
     * @param string $diagID
     * @param int $priceIndex
     * @return HealthPlanMKB10ModuleTreatmentProc[]
     */
    public function updDiagnos2Template($templateID, $diagID, $priceIndex)
    {
        $debugdata = array();
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
                $QueryText = "update DIAGNOS2 set TEMPLATE = $templateID
                where diagnos2.id = $diagID";
                
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            
            $result = $this->getTreatmentProc($diagID ,1, $priceIndex, 0, 0, $trans);
             
            $success = ibase_commit($trans);
            $connection->CloseDBConnection();
            return $result;
    }
    
    private function getProtocolsASR($trcourse, $type, $trans)
    {
            //return "qwer";
            $QueryText = "select distinct ";
            if ($type == 0)
            {
                $QueryText = $QueryText . "protocols.anamnestxt";
            }
            if ($type == 1)
            {
                $QueryText = $QueryText . "protocols.statustxt";
            }
            
            if ($type == 3)
            {
                $QueryText = $QueryText . "protocols.RENTGENTXT";
            }
            if ($type == 4)
            {
                $QueryText = $QueryText . "protocols.EPICRISISTXT";
            }
            
            if ($type == 2)
            {
                $QueryText = $QueryText . "protocols.recomendtxt";
            }
            else
            {
                $QueryText .= ",diagnos2.id";
            }
            $QueryText = $QueryText . " from protocols
                       inner join template on (protocols.id = template.protocol)
                       inner join diagnos2 on (template.id = diagnos2.template)
                    where diagnos2.treatmentcourse = $trcourse";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            ibase_free_query($query);
            $asr = null;
            while ($row = ibase_fetch_row($result))
            {
                $asr = $asr == null ? "" : "$asr; ";
                $addit = text_blob_encode($row[0]);
                if ($type != 2)
                {
                    $diagid = $row[1];
                    $tooth = getTreatmentObjects($diagid,$trans)." - ";
                    $asr .= $tooth;
                }
                $asr .= $addit;
                
            }
            ibase_free_result($result);
            return $asr;
    }

    
    
    /**
     * @param HealthPlanMKB10TreatmentAdditions $addition
     * @return boolean
     */
    public function addAddition($addition)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "update or insert into 
                treatmentadditions(     
                            treatmentadditions.id,
                            treatmentadditions.treatmentcourse,
                            treatmentadditions.additiontype,
                            treatmentadditions.additiontxt,
                            treatmentadditions.signflag,
                            treatmentadditions.doctor,
                            treatmentadditions.visit) values (".$addition->ID.", ".$addition->TrCourse.
                                ",".$addition->TYPE.
                                ", '".safequery($addition->ADDITINSTXT).
                                "',".(($addition->SIGNFLAG) ? 1 : 0).
                                //", ".(($addition->SIGNDATE == "" || $addition->SIGNDATE == null) ? "null" : "'$addition->SIGNDATE'").
                                ",".$addition->Doctor->docid.
                                ", ".$addition->visit.") returning ID";
            $query = ibase_prepare($trans, $QueryText);
            	$result = ibase_execute($query);
            $res="";
            if($row = ibase_fetch_row($result))
    		{
    		  $res=$row[0];
  		    }
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $res;
    }
    /**
     * @param HealthPlanMKB10TreatmentAdditions $addition
	 * @param string $passForDelete
     * @return boolean
     */
    public function removeAddition($addition, $passForDelete)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		if($row = ibase_fetch_row($result))
    		{
    			if($row[0] == $passForDelete)
    			{
                    $QueryText = "delete from treatmentadditions where treatmentadditions.id  = $addition->ID";
                    $query = ibase_prepare($trans, $QueryText);
                    ibase_execute($query);
                    $success = ibase_commit($trans);
                    $connection->CloseDBConnection();
            	    return $success;
                }
    		}
    		ibase_free_result($result);
    		ibase_free_query($query);
    		ibase_commit($trans);
    		$connection->CloseDBConnection();
    		return false;
    }
    
    /**
     * HealthPlanMKB10Module::getProtocolText()
     * 
     * ������ ������ ���������
     * 
     * @param int $id
     * @return string
     */
    public function getProtocolText($id)//sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $txtBlob = null;
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "SELECT protocoltxt FROM protocols WHERE id = $id";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            if ($row = ibase_fetch_row($result))
            {
                $blob_info = ibase_blob_info($row[0]);
                if ($blob_info[0] != 0)
                {
                    $blob_hndl = ibase_blob_open($row[0]);
                    $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                    ibase_blob_close($blob_hndl);
                }
            }
            ibase_free_query($query);
            ibase_free_result($result);
            ibase_commit($trans);
            $connection->CloseDBConnection();
            return $txtBlob;
    }


    ///************************************************************************************************///
    ///                       TREATMENTPROC FUNCTIONS                                                  ///
    ///                                START                                                           ///
    ///************************************************************************************************///
    /**
     * @param int $diagnos2id
     * @param int $toothuse
     * @param int $priceIndex
 	 * @param int $skipCount
 	 * @param int $getCount
     * @param object $trans
     * @return HealthPlanMKB10ModuleTreatmentProc[]
     */
    public function getTreatmentProc($diagnos2id, $toothuse, $priceIndex, $skipCount, $getCount, $trans = null)
    {
        if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

        $QueryCountTreatmenProc = "select count(*) from TREATMENTPROC where DIAGNOS2 = '$diagnos2id'";
        $CountTreatmenProcQuery = ibase_prepare($trans, $QueryCountTreatmenProc);
        $CountTreatmenProcResult = ibase_execute($CountTreatmenProcQuery);
        $count = ibase_fetch_row($CountTreatmenProcResult);
        ibase_free_query($CountTreatmenProcQuery);
        ibase_free_result($CountTreatmenProcResult);
        if ($count[0] != 0) //if in treatment course enable procedurs

        {
            if($skipCount == 0 && $getCount == 0)
            {
                $QueryTreatmentProcText = "select ";
            }
            else
            {
                $QueryTreatmentProcText = "select first $getCount skip $skipCount ";
            }
            $QueryTreatmentProcText .= "
                        TREATMENTPROC.ID, 
                        TREATMENTPROC.VISIT, 
                        TREATMENTPROC.SHIFR, 
                        TREATMENTPROC.NAME, 
                        TREATMENTPROC.NAMEFORPLAN,
                        TREATMENTPROC.UOP, 
                        TREATMENTPROC.PROCTIME, 
                        TREATMENTPROC.PRICE, 
                        TREATMENTPROC.DOCTOR,  
                        (select DOCTORS.SHORTNAME from doctors where doctors.id = TREATMENTPROC.DOCTOR),
                        TREATMENTPROC.DATECLOSE,
                        TREATMENTPROC.HEALTHTYPE,
                        TREATMENTPROC.EXPENSES,
                        TREATMENTPROC.ASSISTANT,
                        (select ASSISTANTS.SHORTNAME from ASSISTANTS where ASSISTANTS.id = TREATMENTPROC.ASSISTANT),
                        TREATMENTPROC.PROC_COUNT,
                        invoices.id, 
                        invoices.invoicenumber,
                        invoices.createdate,
                        TREATMENTPROC.discount_flag,
                        TREATMENTPROC.SALARY_SETTINGS,
                        TREATMENTPROC.HEALTHPROC_FK,
                        treatmentproc.order_fk,
                        orders.paidflag,
                        orders.paidfixdate,
                        treatmentproc.completeexamdiag,
                        
                        (select coalesce(sum(TREATMENTPROC.PRICE * TREATMENTPROC.PROC_COUNT),0) from TREATMENTPROC where TREATMENTPROC.doctor is not null and TREATMENTPROC.DIAGNOS2='$diagnos2id'),
                        (select coalesce(sum(TREATMENTPROC.PRICE * TREATMENTPROC.PROC_COUNT),0) from TREATMENTPROC where TREATMENTPROC.doctor is null and TREATMENTPROC.DIAGNOS2='$diagnos2id')
                        
                        
                        from TREATMENTPROC 


                        left join invoices
                            on invoices.id = treatmentproc.invoiceid
                            
                        left join orders
                            on orders.id = treatmentproc.order_fk
                        
                        where TREATMENTPROC.DIAGNOS2='$diagnos2id' order by TREATMENTPROC.STACKNUMBER asc";

            $TreatmentProcQuery = ibase_prepare($trans, $QueryTreatmentProcText);
            $TreatmentProcResult = ibase_execute($TreatmentProcQuery);

            $outItems = array();

            while ($procRow = ibase_fetch_row($TreatmentProcResult))
            {
                $item = new HealthPlanMKB10ModuleTreatmentProc;
                $item->procID = $procRow[0];
                $item->visit = $procRow[1];
                $item->procShifr = $procRow[2];
                $item->courseName = $procRow[3];
                $item->planName = $procRow[4];
                $item->procUOP = $procRow[5];
                $item->procTime = $procRow[6];
                $item->procPrice = $procRow[7];
                $item->doctorID = $procRow[8];
                $item->doctorShortName = $procRow[9];
                $item->dateClose = $procRow[10];
                $item->procHealthtype = $procRow[11];
                $item->procExpenses = $procRow[12];
                $item->assistantID = $procRow[13];
                $item->assistantShortName = $procRow[14];
                $item->proc_count = $procRow[15];
                $item->isSaved = true;
                $item->invoice_id = $procRow[16];
                $item->invoice_number = $procRow[17];
                $item->invoice_createdate = $procRow[18];
                $item->discount_flag = $procRow[19];
                $item->salary_settings = $procRow[20];
                $item->healthproc_fk = $procRow[21];
                $item->order_fk = $procRow[22];
                
                $item->F39 = Form039Module::get039ProcValues(1, $item->procID, $trans);
                
                if($procRow[23] == 1)
                {
                    $item->order_fixed = true;  
                }
                else
                {
                    $item->order_fixed = false; 
                }
                $item->order_fixed_date = $procRow[24];
                if(($procRow[8] != '')&&($procRow[8] != null))
                {
                    $item->isComplete = true;
                }
                else
                {
                    $item->isComplete = false;  
                }
                $item->complete_examdiag = $procRow[25];
                
                
                $item->diagnos_proceduresCount = $count[0];
                $item->diagnos_factSumm = $procRow[26];
                $item->diagnos_planSumm = $procRow[27];

                 // '' - material farmgroup code *** deprecated ***
                $FarmQueryText = "select
                                        TREATMENTPROCFARM.FARM, 
                                        (select FARM.NAME from FARM where TREATMENTPROCFARM.FARM = FARM.ID),
                                        
                                        '',
                                        
                                        
                                        TREATMENTPROCFARM.QUANTITY, 
                                        (select UNITOFMEASURE.NAME from UNITOFMEASURE where UNITOFMEASURE.FARMID = TREATMENTPROCFARM.FARM and UNITOFMEASURE.isbase = 1),
                                        TREATMENTPROCFARM.FARMPRICE,
                                        TREATMENTPROCFARM.ID
                                        
                                        from TREATMENTPROCFARM 
                                        
                                        where TREATMENTPROCFARM.TREATMENTPROC='$procRow[0]'";

                $FarmQuery = ibase_prepare($trans, $FarmQueryText);
                $FarmResult = ibase_execute($FarmQuery);
                while ($farmRow = ibase_fetch_row($FarmResult))
                {
                    $farmobj = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                    $farmobj->farmID = $farmRow[0];
                    $farmobj->farmName = $farmRow[1];
                    $farmobj->farmGroup = $farmRow[2]; // '' - material farmgroup code *** deprecated ***
                    $farmobj->farmQuantity = $farmRow[3];
                    $farmobj->farmUnitofmeasure = $farmRow[4];
                    $farmobj->farmPrice = $farmRow[5];
                    $farmobj->ID = $farmRow[6];
                    $item->farms[] = $farmobj;
                }
                $farmobj = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                $item->farms[] = $farmobj;

                ibase_free_query($FarmQuery);
                ibase_free_result($FarmResult);
                $outItems[] = $item;
            }
            ibase_free_query($TreatmentProcQuery);
            ibase_free_result($TreatmentProcResult);
            
               
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            
            
            return $outItems;
        } else //if in treatment course exist procedurs

        {
            $QueryTreatmentProcText = "select
                                                TEMPLATEPROC.VISIT,
                                                HEALTHPROC.SHIFR,
                                                HEALTHPROC.NAME,
                                                HEALTHPROC.PLANNAME,
                                                HEALTHPROC.UOP,
                                                HEALTHPROC.PROCTIME,
                                                HEALTHPROC.PRICE".sql_checkPriceIndex($priceIndex).",
                                                healthproc.ID,
                                                HEALTHPROC.HEALTHTYPES,
                                                HEALTHPROC.EXPENSES,
                                                HEALTHPROC.DISCOUNT_FLAG,
                                                HEALTHPROC.SALARY_SETTINGS,
                                                HEALTHPROC.COMPLETEEXAMDIAG                                                
                        
                                        from TEMPLATEPROC 
                                        
                                        inner join healthproc
                                            on TEMPLATEPROC.healthproc = healthproc.ID

                                        inner join  DIAGNOS2
                                            on TEMPLATEPROC.TEMPLATE = DIAGNOS2.template

                                        where  DIAGNOS2.id = '$diagnos2id'
                                        order by templateproc.visit, templateproc.stacknumber";

            $TreatmentProcQuery = ibase_prepare($trans, $QueryTreatmentProcText);
            $TreatmentProcResult = ibase_execute($TreatmentProcQuery);

            $outItems = array();

            while ($procRow = ibase_fetch_row($TreatmentProcResult))
            {
                $item = new HealthPlanMKB10ModuleTreatmentProc;
                $item->visit = $procRow[0];
                $item->procShifr = $procRow[1];
                $item->courseName = $procRow[2];
                $item->planName = $procRow[3];
                $item->procUOP = $procRow[4];
                $item->procTime = $procRow[5];
                $item->procPrice = $procRow[6];
                
                if($procRow[8] != '' && $procRow[8] != null)
                {
                    $item->procHealthtype = substr($procRow[8], 0, 1);
                }
                
                $item->procExpenses = $procRow[9];
                $item->proc_count = 1;
                $item->isSaved = false;
                $item->isComplete = false;
                $item->discount_flag = $procRow[10];
                $item->salary_settings = $procRow[11];
                $item->healthproc_fk = $procRow[7];
                $item->complete_examdiag = $procRow[12];
                $item->farms=array();
           
           
           // 0 - material costs *** tmp deprecated ***
           // '' - material farmgroup code *** deprecated ***
             $FarmQueryText = "select
                                        (select FARM.NAME from FARM where healthprocfarm.FARM = FARM.ID),
                                        '',
                                        healthprocfarm.quantity,
                                        (select UNITOFMEASURE.NAME from UNITOFMEASURE where UNITOFMEASURE.FARMID = healthprocfarm.FARM and UNITOFMEASURE.isbase = 1),
                                        0,
                                        healthprocfarm.FARM
                                         
                                        
                                        from healthprocfarm
                                        
                                        
                                        where healthprocfarm.healthproc='$procRow[7]'";

                $FarmQuery = ibase_prepare($trans, $FarmQueryText);
                $FarmResult = ibase_execute($FarmQuery);
                while ($farmRow = ibase_fetch_row($FarmResult))
                {
                    $farmobj = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                    $farmobj->farmName = $farmRow[0];
                    $farmobj->farmGroup = $farmRow[1]; // '' - material farmgroup code *** deprecated ***
                    $farmobj->farmQuantity = $farmRow[2];
                    $farmobj->farmUnitofmeasure = $farmRow[3];
                    $farmobj->farmPrice = $farmRow[4]; // 0 - material costs *** tmp deprecated ***
                    $farmobj->farmID = $farmRow[5];
                    $item->farms[] = $farmobj;
                }
                $farmobj = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                $item->farms[] = $farmobj;
                
                $item->F39 = Form039Module::get039ProcValues(0, $item->healthproc_fk, $trans);
                
                ibase_free_query($FarmQuery);
                ibase_free_result($FarmResult);
                $outItems[] = $item;
            }
            ibase_free_query($TreatmentProcQuery);
            ibase_free_result($TreatmentProcResult);
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $outItems;
        }
    }
        
    /**
     * @param int $diagnos2id
     * @param string $querymode //'template', 'protocol', 'general' - toothuse = 0, '1,2,3,4..' by health type, 'all'
     *  @param string $searchText 
     * @param boolean $isGrouping
     * @param int $priceIndex
     * @return HealthPlanMKB10ModuleTreatmentProc[]
     */
    public function getTreatmentProcArrayForDictionary($diagnos2id, $querymode, $searchText, $isGrouping, $priceIndex)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
         $whereStr="";

                $where=array();
                if(nonull($searchText))
                {
                      $where[]= " lower(p.planname||' '||p.shifr) like lower('%$searchText%')"; 
                }

                if($querymode == 'protocol')//protocols procedures
                {
                    $where[]= "p.nodetype = 1";
                    $where[]= "d2.id = $diagnos2id";
                }
                else if($querymode == 'template')
                {
                    $where[]= "p.nodetype = 1";
                    $where[]= "d2.id = $diagnos2id";
                }
                else if($querymode == 'general')
                {
                    $where[]= "p.nodetype = 1";
                    $where[]= "p.TOOTHUSE = 0";
                }
                else if(is_numeric($querymode))//procedures by healthtype
                {
                      $where[]= "'|'||p.HEALTHTYPES||'|'||p.HEALTHTYPE||'|' like '%|$querymode|%'";  
                        if($isGrouping==false)
                        {
                            $where[]="p.nodetype = 1";
                        }
                }
                else if($isGrouping==false)
                {
                    $where[]="p.nodetype = 1";
                }

            
                if(count($where)>0)
                {
                    $whereStr=implode(' and ',$where);
                
                    if($isGrouping&&$querymode != 'protocol'&&$querymode != 'template'&&$querymode!= 'general')
                    {
                        
                	 $parentIDs= $this->getParrentIdsRecoursive($whereStr, $trans);
               
                        if(count($parentIDs)>0)
                        {
                             $whereStr ="(p.id in (".implode(',',$parentIDs).") or (".$whereStr."))";
                        }
                       
                       }
                   }
	       
         
        $functionResult = $this->getTreatmentProcArrayForDictionaryRecursive($trans, "", $whereStr, $diagnos2id, $querymode, $isGrouping, $priceIndex);
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $functionResult;

    }
    
     public function getParrentIdsRecoursive($whereStr, $trans) 
    {
        $QueryText = "
        		select  distinct
                    p.PARENTID
                from HEALTHPROC p where p.PARENTID is not null and ".$whereStr;
                
                
        		
                $query = ibase_prepare($trans, $QueryText);
        		$result = ibase_execute($query);
	
        	   $parentIDs=array();
        		while ($row = ibase_fetch_row($result))
        		{
        		    $parentIDs[]=$row[0];  
                    $parentIDs=array_merge($parentIDs, $this->getParrentIdsRecoursive(" p.id=$row[0]", $trans));
        		}
                
                ibase_free_query($query);
                ibase_free_result($result);
                
                return $parentIDs;
    }
    
	public function getTreatmentProcArrayForDictionaryRecursive($trans, $parentID, $whereStr, $diagnos2id, $querymode, $isGrouping, $priceIndex)//sqlite
	{

		$QueryText = "
		select
           distinct(p.ID),
            
            p.SHIFR,
            p.NAME,
            p.PLANNAME,
            p.UOP,
            p.PROCTIME,
            p.PRICE".sql_checkPriceIndex($priceIndex).",
            ".($querymode == 'template'?" tp.visit":"1").",
            p.TOOTHUSE,
            p.HEALTHTYPE,
            p.EXPENSES,
            p.DISCOUNT_FLAG,
            p.SALARY_SETTINGS,
            ".($querymode == 'template'?" tp.stacknumber":"0").",
            p.COMPLETEEXAMDIAG,
            p.HEALTHTYPES,
            p.nodetype
                from healthproc p";
		

        
        if($querymode == 'protocol')//protocols procedures
        {
            $QueryText .= "                           
                            inner join templateproc tp
                                on tp.healthproc = p.id

                            inner join template t
                                on t.id = tp.template

                            inner join protocols pr
                                on pr.id = t.protocol

                            
                            inner join diagnos2 d2
                                on d2.protocol_fk = pr.id";

            }
            else if($querymode == 'template')
            {
               $QueryText.= "          
                                        inner join templateproc tp
                                            on tp.healthproc = p.id

                                        inner join template t
                                            on t.id = tp.template

                                        inner join diagnos2 d2
                                            on d2.template = t.id";
            }
            
      		
            
            if($querymode == 'protocol')//protocols procedures
            { 
                if(nonull($whereStr))
                {
                    $QueryText.=" where ";
                }
                
                $QueryText.=$whereStr." order by p.PARENTID, p.SEQUINCENUMBER";
            }
            else if($querymode == 'template')
            {
                if(nonull($whereStr))
                {
                    $QueryText.=" where ";
                }
                $QueryText.=$whereStr." order by tp.visit, tp.stacknumber";
            }
            else if($querymode == 'general')
            {
                if(nonull($whereStr))
                {
                    $QueryText.=" where ";
                }
                $QueryText.=$whereStr." order by p.PARENTID, p.SEQUINCENUMBER";
            }
            else//procedures by healthtype and all
            {
               if($isGrouping)
               {
                    if (($parentID == "null") || ($parentID == ""))
            		{
            			$QueryText .= " where p.PARENTID is null";                  
            		}			
            		else 
            		{
            		    
            			$QueryText .= " where p.PARENTID = $parentID";
            		}
                    if(nonull($whereStr))
                    {
                        $QueryText.=" and ";
                    }
                }
                else if(nonull($whereStr))
                {
                    $QueryText.=" where ";
                }
                $QueryText.=$whereStr." order by p.SEQUINCENUMBER";
            }


        
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
		
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$res = array();	
	
		while ($row = ibase_fetch_row($result))
		{
            $item = new HealthPlanMKB10ModuleTreatmentProc;
                $item->procShifr = $row[1];
                $item->courseName = $row[2];
                $item->planName = $row[3];
                $item->label = $row[3];
                $item->procUOP = (float)$row[4];
                $item->procTime = (int)$row[5];
                $item->procPrice = $row[6];
                $item->visit = $row[7];
                $item->toothUse = $row[8];
                $item->procHealthtype = $row[9];
                $item->procExpenses = $row[10];
                $item->proc_count = 1;
                $item->isSaved = false;
                $item->isComplete = false;
                $item->discount_flag = $row[11];
                $item->salary_settings = $row[12];
                $item->healthproc_fk = $row[0];
                $item->complete_examdiag = $row[14];
                $item->procHealthtypes = $row[15];
                $item->NODETYPE=$row[16];
                $parentID = $row[0];
			
			if ($item->NODETYPE == "0"&&$isGrouping)
			{
				$item->children = $this->getTreatmentProcArrayForDictionaryRecursive($trans, $parentID, $whereStr, $diagnos2id, $querymode, $isGrouping, $priceIndex);	
			}
			else
			{			
                    $FarmQueryText = "select
                                                FARM.NAME, 
                                                FARMGROUP.CODE, 
                                                HEALTHPROCFARM.QUANTITY,
                                                UNITOFMEASURE.NAME, 
                                                0,
                                                FARM.ID
                                                
                                                from HEALTHPROCFARM 
                                                
                                                left join FARM
                                                    on HEALTHPROCFARM.FARM = FARM.ID
    
                                                left join UNITOFMEASURE
                                                    on FARM.ID = UNITOFMEASURE.FARMID
                                                
                                                left join FARMGROUP
                                                    on FARM.FARMGROUPID = FARMGROUP.ID
                                                
                                                where HEALTHPROCFARM.HEALTHPROC='$row[0]' and UNITOFMEASURE.isbase = 1";

                $FarmQuery = ibase_prepare($trans, $FarmQueryText);
                $FarmResult = ibase_execute($FarmQuery);
                $item->farms = array();
                while ($farmRow = ibase_fetch_row($FarmResult))
                {
                    $farmobj = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                    $farmobj->farmName = $farmRow[0];
                    $farmobj->farmGroup = $farmRow[1];
                    $farmobj->farmQuantity = $farmRow[2];
                    $farmobj->farmUnitofmeasure = $farmRow[3];
                    $farmobj->farmPrice = $farmRow[4];
                    $farmobj->farmID = $farmRow[5];
                    $item->farms[] = $farmobj;
                }
                //$farmobj = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                //$item->farms[] = $farmobj;  
                                        
    			 $item->F39 = Form039Module::get039ProcValues(0, $row[0], $trans);			
			}
			$res[] = $item;
		}	
		
		ibase_free_query($query);
		ibase_free_result($result);	
			
		return $res; 
	}
    /**
     * @return HealthPlanMKB10ModuleTreatmentFarmsProc[]
     */
    public function getFarmsForProcDictionary()//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);

        $QueryFarmText = "select
                                        FARM.ID,
                                        FARM.NAME, 
                                        FARMGROUP.CODE,
                                        UNITOFMEASURE.NAME,  
                                        0
                                        
                                        from FARM
                                        left join UNITOFMEASURE
                                            on FARM.ID = UNITOFMEASURE.FARMID
                                        
                                        left join FARMGROUP
                                            on FARM.FARMGROUPID = FARMGROUP.ID

                                        where FARM.nodetype = 1 and UNITOFMEASURE.isbase = 1";

        $FarmQuery = ibase_prepare($trans, $QueryFarmText);
        $FarmResult = ibase_execute($FarmQuery);

        $outItems = array();

        while ($farmRow = ibase_fetch_row($FarmResult))
        {
            $item = new HealthPlanMKB10ModuleTreatmentFarmsProc;
            $item->farmID = $farmRow[0];
            $item->farmName = $farmRow[1];
            $item->farmGroup = $farmRow[2];
            $item->farmUnitofmeasure = $farmRow[3];
            $item->farmPrice = $farmRow[4];
            $item->farmQuantity = 1;
            $outItems[] = $item;
        }
        ibase_commit($trans);
        ibase_free_query($FarmQuery);
        ibase_free_result($FarmResult);
        $connection->CloseDBConnection();
        return $outItems;
    }


    //*******************************************************************************************************************
    //*******************************************************************************************************************
    //*******************************************************************************************************************
    /**
     * @param int $diagnos2id
     * @param HealthPlanMKB10ModuleTreatmentProc[] $treatmentprocs
     * @param HealthPlanMKB10ModuleTreatmentProc[] $treatprocsDel
     * @param string $patientId
     * @param boolean $isAfterExamsEnabled
     * @param int $priceIndex
     * @param string $cabinetid
     * @return boolean
     */
    public function saveTreatmentProc($diagnos2id, $treatmentprocs, $treatprocsDel, $patientId, $isAfterExamsEnabled, $priceIndex, $cabinetid)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $return = false;
        
        if(($patientId == null)||($patientId == 'null')||($patientId == ''))
        {
            $patientId = 'null';
        }
        if($cabinetid == null || $cabinetid == '' || $cabinetid == 'empty')
        {
            $cabinetid = 'null';
        }


        //$QueryDelProcsText = "delete from TREATMENTPROC where TREATMENTPROC.DIAGNOS2 = '$diagnos2id'";
        
        
        
        if(count($treatprocsDel) > 0)
        {
            $QueryDelProcsText = "delete from TREATMENTPROC where TREATMENTPROC.ID = '".$treatprocsDel[0]->procID."'";
            for($i = 1; $i < count($treatprocsDel); ++$i) 
            {
		          $QueryDelProcsText .= " or TREATMENTPROC.ID = '".$treatprocsDel[$i]->procID."'";
            }
            $QueryDelProcs = ibase_prepare($trans, $QueryDelProcsText);
            $ResultDelProcs = ibase_execute($QueryDelProcs);
            ibase_free_query($QueryDelProcs);
        }
        
        
        
        

        $stacknumber = 0;
        
        // means that all procedures is completed
        $isAllComplete = true;

        foreach ($treatmentprocs as $item)
        {
            $doctorId = 'null';
            if ($item->isComplete == true)	
            {
                $doctorId = $item->doctorID;
            }
            
            $dateClose = 'null';
            if($item->dateClose != '') 
            {
                $dateClose = "'".$item->dateClose."'";
            }
                    
            if ($item->procID != null) // Update TREATMENTPROCEDURE
            {
                //$QueryDelProcsText .= " and TREATMENTPROC.ID <> '$item->procID'";
                
                $QueryUpdProcsText = "update TREATMENTPROC set 
																VISIT = ".(($item->visit == '') ? "null" : "'$item->visit'").",
																NAME = ".(($item->courseName == '') ? "null" : "'".safequery($item->courseName)."'").", 
																NAMEFORPLAN = ".(($item->planName == '') ? "null" : "'".safequery($item->planName)."'").",
																UOP = ".(($item->procUOP == '') ? "0" : "'$item->procUOP'").", 
																PRICE = ".(($item->procPrice == '') ? "0" : "'$item->procPrice'").", 
																PROCTIME = ".(($item->procTime == '') ? "0" : "'$item->procTime'").",
                                                                ASSISTANT = ".(($item->assistantID == '') ? "null" : "'$item->assistantID'").",
																DOCTOR = $doctorId, 
																DATECLOSE = $dateClose, 
                    											HEALTHTYPE = '$item->procHealthtype',
                                                                EXPENSES = '$item->procExpenses',
																TASKS = null, 
																STACKNUMBER = '$stacknumber',
                                                                PROC_COUNT = '$item->proc_count',
                                                                DISCOUNT_FLAG = $item->discount_flag,
                                                                SALARY_SETTINGS = $item->salary_settings,
                                                                order_fk = ".(($item->order_fk == '') ? "null" : "'$item->order_fk'").",
                                                                patient_fk = $patientId,
                                                                COMPLETEEXAMDIAG = ".(($item->complete_examdiag == '') ? "null" : "'$item->complete_examdiag'")."
															where  TREATMENTPROC.ID = '$item->procID'"; //update: with procID, dateClose is NULL!!!!
                     
               
                $QueryUpdProcs = ibase_prepare($trans, $QueryUpdProcsText);
                $ResultUpdProcs = ibase_execute($QueryUpdProcs);
                ibase_free_query($QueryUpdProcs);
                
                //if($item->F39 != null)
                //{
                    //Form039Module::save039ProcValues(1, $item->procID, $item->F39, $trans);
                    Form039Module::save039ProcValues(1, $item->procID, $item->F39OUT, $trans);
                //}
                
                //set auto diagnios
                if($isAfterExamsEnabled)
                {
                    if(($item->doctorID != '')&&($item->doctorID != null)&&($item->dateClose != '')&&($item->dateClose != null)
                            &&($item->complete_examdiag != '')&&($item->complete_examdiag != null)&&($item->complete_examdiag != -1))
                    {
                        if($this->setAutomaticalDiagnos43($diagnos2id, $item->complete_examdiag, $trans))
                        {
                            $return = true;
                        }
                    }
                }

                if(($item->doctorID == '')||($item->doctorID == null)||($item->dateClose == '')||($item->dateClose == null))
                {
                    $isAllComplete = false;
                }

                //Updating and saving treatmentprcfarms
                $QueryDelProcFarmsText =
                    "delete from TREATMENTPROCFARM where TREATMENTPROCFARM.TREATMENTPROC = '$item->procID'";

                foreach ($item->farms as $itemFarm)
                {
                    if ($itemFarm->ID != null)//procfarms that have IDs and need only updates 
                    {
                        $QueryDelProcFarmsText .= " and TREATMENTPROCFARM.ID <> '$itemFarm->ID'";
                        
                        /*
                        $QueryUpdProcsFarmsText = "update TREATMENTPROCFARM set
                                                                        QUANTITY = '$itemFarm->farmQuantity',
																		FARMPRICE = coalesce((select MAINSTORE.PRICE from MAINSTORE where MAINSTORE.FARMID = '$itemFarm->farmID'),0)																							
                                                    where TREATMENTPROCFARM.ID = '$itemFarm->ID' and 
                                                    (QUANTITY <> '$itemFarm->farmQuantity' or 
                                                    FARMPRICE <> coalesce((select MAINSTORE.PRICE from MAINSTORE where MAINSTORE.FARMID = '$itemFarm->farmID'),0))";
                        */
                        $QueryUpdProcsFarmsText = "update TREATMENTPROCFARM set 
                                                                QUANTITY = '$itemFarm->farmQuantity', 
                                                                QUANTITYFULL = '".($itemFarm->farmQuantity * $item->proc_count)."',
                                                                DOCTOR_FK = $doctorId,
                                                                DATECLOSE = $dateClose
                                                    where TREATMENTPROCFARM.ID = '$itemFarm->ID'";

                        $QueryUpdProcsFarms = ibase_prepare($trans, $QueryUpdProcsFarmsText);
                        $ResultUpdProcsFarms = ibase_execute($QueryUpdProcsFarms);
                        ibase_free_query($QueryUpdProcsFarms);
                        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryUpdProcsFarmsText);
                    } 
                    else //procfarms that have null IDs
                    {
                        $QueryInsProcsFarmsText =
                            "insert into TREATMENTPROCFARM( ID, 
                                                            TREATMENTPROC, 
                                                            FARM, 
                                                            QUANTITY, 
                                                            QUANTITYFULL, 
                                                            FARMPRICE, 
                                                            DOCTOR_FK, 
                                                            DATECLOSE,
                                                            CABINET_FK)
                                                    values (
                                                                 null, 
                                                                 '$item->procID', 
                                                                 '$itemFarm->farmID', 
                                                                 '$itemFarm->farmQuantity', 
                                                                 '".($itemFarm->farmQuantity * $item->proc_count)."', 
                                                                 0, 
                                                                 $doctorId, 
                                                                 $dateClose,
                                                                 ".$cabinetid.") 
                                                    returning (ID)";
                        
                        $QueryInsProcsFarms = ibase_prepare($trans, $QueryInsProcsFarmsText);
                        $ResultInsProcsFarms = ibase_execute($QueryInsProcsFarms);
                        $InsFarmRowID = ibase_fetch_row($ResultInsProcsFarms);

                        ibase_free_query($QueryInsProcsFarms);
                        $QueryDelProcFarmsText .= " and TREATMENTPROCFARM.ID <> '$InsFarmRowID[0]'"; //returning_ID
                    }
                }

                $QueryDelProcFarms = ibase_prepare($trans, $QueryDelProcFarmsText);
                $ResultDelProcFarms = ibase_execute($QueryDelProcFarms);
                ibase_free_query($QueryDelProcFarms);
            } 
            else // Insert TREATEMNTPROC
            {
                $QueryInsProcsText = "insert into TREATMENTPROC(
																ID, 
                                                                VISIT, 
                                                                DIAGNOS2, 
                                                                SHIFR, 
																NAME, 
                                                                NAMEFORPLAN, 
                                                                UOP, 
                                                                PRICE, 
																PROCTIME, 
                                                                DOCTOR, 
                                                                DATECLOSE, 
																TASKS, 
                                                                STACKNUMBER, 
                                                                HEALTHTYPE, 
                                                                EXPENSES,
                                                                ASSISTANT,
                                                                PROC_COUNT,
                                                                DISCOUNT_FLAG,
                                                                SALARY_SETTINGS,
                                                                healthproc_fk,
                                                                order_fk,
                                                                patient_fk,
                                                                COMPLETEEXAMDIAG,
                                                                PRICEINDEX,
                                                                CABINET_FK) 
															values ( 
                                                            null,  
                                                            ".(($item->visit == '') ? "null" : "'$item->visit'").", 
                                                            '".$diagnos2id."', 
                                                            ".(($item->procShifr == '') ? "''" : "'".safequery($item->procShifr)."'").",
                                                            ".(($item->courseName == '') ? "null" : "'".safequery($item->courseName)."'").", 
                                                            ".(($item-> planName == '') ? "null" : "'".safequery($item->planName)."'").", 
                                                            ".(($item->procUOP == '') ? "0" : "'$item->procUOP'").", 
                                                            ".(($item->procPrice == '') ? "0" : "'$item->procPrice'").", 
                                                            ".(($item->procTime == '') ? "0" : "'$item->procTime'").", 
                                                            $doctorId, 
                                                            $dateClose, 
                                                            null, 
                                                            '".$stacknumber."', 
                                                            '".$item->procHealthtype."', 
                                                            '".$item->procExpenses."',
                                                            ".(($item->assistantID == '') ? "null" : "'$item->assistantID'").",
                                                            '$item->proc_count',
                                                            $item->discount_flag,
                                                            $item->salary_settings,
                                                            $item->healthproc_fk,
                                                            ".(($item->order_fk == '') ? "null" : "'$item->order_fk'").",
                                                            $patientId,
                                                            ".(($item->complete_examdiag == '') ? "null" : "'$item->complete_examdiag'").",
                                                            $priceIndex,
                                                            ".$cabinetid.")   
                                                            returning (ID)"; //insert: without procID!!!!

                $QueryInsProcs = ibase_prepare($trans, $QueryInsProcsText);
                $ResultInsProcs = ibase_execute($QueryInsProcs);
                $InsProcRowID = ibase_fetch_row($ResultInsProcs);

                //if($item->F39 != null)
                //{
                    //Form039Module::save039ProcValues(1, $InsProcRowID[0], $item->F39, $trans);
                    Form039Module::save039ProcValues(1, $InsProcRowID[0], $item->F39OUT, $trans);
                //}
                
                //set auto diagnios
                if($isAfterExamsEnabled)
                {
                    if(($item->doctorID != '')&&($item->doctorID != null)&&($item->dateClose != '')&&($item->dateClose != null)
                            &&($item->complete_examdiag != '')&&($item->complete_examdiag != null)&&($item->complete_examdiag != -1))
                    {
                        if($this->setAutomaticalDiagnos43($diagnos2id, $item->complete_examdiag, $trans))
                        {
                            $return = true;
                        };
                    }
                }
                if(($item->doctorID == '')||($item->doctorID == null)||($item->dateClose == '')||($item->dateClose == null))
                {
                    $isAllComplete = false;
                }
                
                //$QueryDelProcsText .= " and TREATMENTPROC.ID <> '$InsProcRowID[0]'"; //returning_ID

                foreach ($item->farms as $itemFarm)
                {
                    $QueryInsProcsFarmsText = "insert into TREATMENTPROCFARM (  ID,   
                                                                                TREATMENTPROC, 
                                                                                FARM, 
                                                                                QUANTITY, 
                                                                                QUANTITYFULL,                                       
                                                                                FARMPRICE, 
                                                                                DOCTOR_FK,  
                                                                                DATECLOSE,
                                                                                CABINET_FK)
												                      values (
                                                                                null, 
                                                                                '$InsProcRowID[0]', 
                                                                                '$itemFarm->farmID', 
                                                                                '$itemFarm->farmQuantity',
                                                                                '".($itemFarm->farmQuantity * $item->proc_count)."', 
                                                                                0, 
                                                                                $doctorId, 
                                                                                $dateClose,
                                                                                ".$cabinetid.")";

//                    file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryInsProcsFarmsText);
                    $QueryInsProcsFarms = ibase_prepare($trans, $QueryInsProcsFarmsText);
                    $ResultInsProcsFarms = ibase_execute($QueryInsProcsFarms);

                    ibase_free_query($QueryInsProcsFarms);

                }
            }

            $stacknumber++;
        }

        //$QueryDelProcs = ibase_prepare($trans, $QueryDelProcsText);
        //$ResultDelProcs = ibase_execute($QueryDelProcs);
        
        //set auto template diagnios
        if($isAfterExamsEnabled)
        {
              if($isAllComplete == true)
              {
                    if($this->setAutomaticalDiagnos43($diagnos2id, -11, $trans));
                    {
                        $return = true;
                    };
              } 
        }
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $return;
    }
    
    /**
     * @param int $diagnos2id
     * @param string $after43
     * @return boolean
     */
    private function setAutomaticalDiagnos43($diagnos2id, $after43, $trans=null)
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        $QueryText = "select STACKNUMBER,TEMPLATE,TREATMENTCOURSE from DIAGNOS2 where ID = $diagnos2id";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        
        if ($row = ibase_fetch_row($result))
        {
            if(nonull($row[0])&&nonull($row[1])&&$row[0]!=-1)
            {
                if ($after43 == -11) 
                {
                    $QueryText0 = "select F43AFTER from TEMPLATE where ID = ".$row[1];
                    $query0 = ibase_prepare($trans, $QueryText0);
                    $result0 = ibase_execute($query0);
                    if ($row0 = ibase_fetch_row($result0))
                    {
                        $after43=$row0[0];  
                    }
                    else 
                    {   
                       if($local)
                        {
                            ibase_commit($trans);
                            $connection->CloseDBConnection();
                        }     
                        return false; 
                    } 
                }

                
                $treatment_objects=$this->getTreatmentObjectsCut($diagnos2id,$trans);
                //print_r($treatment_objects);
               
                
                $QueryText2 = "select EXAM,EXAMID from TOOTHEXAM where EXAMID=(select MAX(EXAMID) from TOOTHEXAM where TREATMENTCOURSE_fk=".$row[2].")";
                $query2 = ibase_prepare($trans, $QueryText2);
                $result2 = ibase_execute($query2);
                if ($row2 = ibase_fetch_row($result2))
                    {
                     
                    $exam=$row2[0];
                    $exam_id=$row2[1];  
                    //echo("\n".$exam."\n");

                    if(strstr($exam,"<GP")&&$after43!=9&&$after43!=24)
                    {
                        if(!gp_array_dif($treatment_objects,1,"D1",false))
                        {
                            $treatment_objects=gp_array_dif_array($treatment_objects,1,"D1",false);
                            $exam=preg_replace("/<GP1>[0-9]+<\/GP1>/","",$exam);
                            
                        }
                        if(!gp_array_dif($treatment_objects,1,"D1",true))
                        {
                            $treatment_objects=gp_array_dif_array($treatment_objects,1,"D1",true);
                            $exam=preg_replace("/<GP1>[0-9]+<\/GP1>/","",$exam);
                        }

                        if(!gp_array_dif($treatment_objects,2,"D1",false))
                        {
                            $treatment_objects=gp_array_dif_array($treatment_objects,2,"D1",false);
                            $exam=preg_replace("/<GP2>[0-9]+<\/GP2>/","",$exam);
                            
                        }
                        if(!gp_array_dif($treatment_objects,2,"D1",true))
                        {
                            $treatment_objects=gp_array_dif_array($treatment_objects,2,"D1",true);
                            $exam=str_replace("/<GP2>[0-9]+<\/GP2>/","",$exam);
                        }
                    } 
//                        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n after43Total=".$after43);
                        $z=0;
                        $xml = simplexml_load_string('<xml>'.$exam.'</xml>');  
                        foreach($xml->zub as $zub)
                        {   
                            $zub_id=$zub->attributes()->id;
                            //echo("@id=".$zub_id."\n");
                            if (in_array($zub_id,$treatment_objects['TOOTH']))
                            {   
                                $teeth_keys=array_search_all($zub_id,$treatment_objects['TOOTH']);
                           // print_r($teeth_keys);
                                foreach($teeth_keys as $tooth_key)
                                {
//                                    file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\nbefore=".var_export($xml->zub[$z],true));
                                    
                                    $ts=$treatment_objects['TOOTHSIDE'][$tooth_key];
//                                    file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\nts=".var_export($ts,true));
//                                    file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\nzub=".var_export($zub,true));                                    
                                    //   echo("@side=".$ts."\n");
                                    //if (array_key_exists($ts, $zub)) 
                                   // {
                                       
                                        switch($after43)
                                        {
                                            case 0: 
                                                
                                                if($ts=="Z")
                                                {
                                                    foreach(get_tooth_surfaces($zub_id) as $surfaces)
                                                    {
                                                        unset($xml->zub[$z]->$surfaces);
                                                    }
                                                    foreach(get_tooth_roots($zub_id) as $root)
                                                    {
                                                        unset($xml->zub[$z]->$root);
                                                    }
                                                }
                                                else
                                                {
                                                    unset($xml->zub[$z]->$ts);
                                                }
                                                break;
                                                
                                            case 1: 
                                            case 3: 
                                            case 4: 
                                            case 26: 
                                            case 29: 
                                            case 30:
                                                if(in_array($ts,array("D","M","V","O","OK")))$xml->zub[$z]->$ts=$after43;
                                                else
                                                {
                                                    unset($xml->zub[$z]->$ts);
                                                    foreach(get_tooth_surfaces($zub_id) as $surfaces)
                                                    {
                                                        $xml->zub[$z]->$surfaces=$after43;
                                                    }
                                                    
                                                }
                                                break;
                                            
    
                                            case 15: 
                                            case 16:
                                                if(in_array($ts,array("R","MR","CR","DR")))$xml->zub[$z]->$ts=$after43;
                                                else
                                                {
                                                    unset($xml->zub[$z]->$ts);
                                                    foreach(get_tooth_roots($zub_id) as $root)
                                                    {
                                                        $xml->zub[$z]->$root=$after43;
                                                    }
                                                    
                                                }
                                                break;
                                                
                                            case 6: 
                                            case 14: 
                                                if(in_array($ts,array("D","M","V","O","OK","R","MR","CR","DR")))
                                                {
                                                  $xml->zub[$z]->$ts=$after43;  
                                                }
                                                else
                                                {
                                                    unset($xml->zub[$z]->$ts);
                                                    
                                                    foreach(get_tooth_roots($zub_id) as $root)
                                                    {
                                                        $xml->zub[$z]->$root=$after43;
                                                    }
                                                    
                                                    foreach(get_tooth_surfaces($zub_id) as $surfaces)
                                                    {
                                                        $xml->zub[$z]->$surfaces=$after43;
                                                    }

                                                    
                                                }
                                                break;
                                                
                                            case 9: 
                                            case 24: 
                                            case 27: 
                                                if($ts=="D1")
                                                {
                                                  $xml->zub[$z]->$ts=$after43;  
                                                }
                                                else
                                                {
                                                    unset($xml->zub[$z]->$ts);
                                                    $xml->zub[$z]->D1=$after43;
                                                }
                                                break;
                                                
                                            case 19: 
                                                if($ts=="D2")
                                                {
                                                  $xml->zub[$z]->$ts=$after43;  
                                                }
                                                else
                                                {
                                                    foreach(get_tooth_surfaces($zub_id) as $surfaces)
                                                    {
                                                        unset($xml->zub[$z]->$surfaces);
                                                    }
                                                    foreach(get_tooth_roots($zub_id) as $root)
                                                    {
                                                        unset($xml->zub[$z]->$root);
                                                    }
                                                    $xml->zub[$z]->D2=$after43;
                                                    
                                                }
                                                break;
                                                
                                            case 2: 
                                            case 5: 
                                            case 7: 
                                            case 8: 
                                            case 11: 
                                            case 12: 
                                            case 13: 
                                            case 17: 
                                            case 18: 
                                            case 25:
                                            case 28: 
                                            case 31:  
                                                if($ts=="Z")
                                                {
                                                  $xml->zub[$z]->$ts=$after43;  
                                                }
                                                else
                                                {
                                                    unset($xml->zub[$z]->$ts);
                                                    $xml->zub[$z]->Z=$after43;
                                                    
                                                }
                                                break;
                                            }
                                        }
                                       
                                    //}
//                                     file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\nafter=".var_export($xml->zub[$z],true));        
                            }
                            $z++;

                        }
                       $exam=substr($xml->asXML(),27,strlen($xml->asXML())-34);
                       $exam=preg_replace("/<zub id=\"[0-9]+\" milkflag=\"0\"\/>/","",$exam);
                        // echo($exam."\n"); 
//                       file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\nexam=".var_export($exam, true));
                       $QueryText3 = "update Toothexam set EXAM='$exam' where EXAMID=$exam_id";
                       $query3 = ibase_prepare($trans, $QueryText3);
                       ibase_execute($query3);
                       ibase_free_query($query3);
                        if($local)
                        {
                            ibase_commit($trans);
                            $connection->CloseDBConnection();
                        }     
                        return true; 

                }
                else 
                {   
                    if($local)
                    {
                        ibase_commit($trans);
                        $connection->CloseDBConnection();
                    }     
                    return false; 
                }       
            }
            else 
            {   
                    if($local)
                    {
                        ibase_commit($trans);
                        $connection->CloseDBConnection();
                    }     
                    return false; 
            }
        } 
                        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }     
        return false; 
    }
    
    


    ///************************************************************************************************///
    ///                       TREATMENTPROC FUNCTIONS                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    
    
    ///************************************************************************************************///
    ///                         ORDERS FUNCTIONS                                                       ///
    ///                                START                                                           ///
    ///************************************************************************************************///
    
    /**
     * @param int $diagnos_id
     * @param int $doctorid
     * @param int $patientid
     * @param HealthPlanMKB10ModuleTreatmentProc[] $treatmentprocs
     * @param int $assistantid
     * @return boolean
     */
    public function saveOrder($diagnos_id, $doctorid, $patientid, $treatmentprocs, $assistantid)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update or insert into orders (DIAGNOS2_FK, ORDERDATE, DOCTORID, PATIENTID, ASSISTANTID) values ($diagnos_id, CURRENT_TIMESTAMP, $doctorid, $patientid, $assistantid) matching (DIAGNOS2_FK) returning id";
        $query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$id;
		while ($row = ibase_fetch_row($result))
		{
			$id = $row[0];
		}
        $QueryText = "update treatmentproc set treatmentproc.order_fk = $id";
        $isFirst = true;
        foreach ($treatmentprocs as $item)
        {
            if($item->isSaved == true)
            {
                if($isFirst == true)
                {
                    $QueryText .= "where treatmentproc.id = $item->procID "; 
                    $isFirst = false;
                }
                else
                {
                    $QueryText .= " or treatmentproc.id = $item->procID"; 
                }
            }
        }
        if($isFirst == false)
        {
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
        }
        
        ibase_free_query($query);
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
    
    /**
	 * @param string $passForDelete
     * @return boolean
     */
    public function confirmationUpdate($passForDelete)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
    		if($row = ibase_fetch_row($result))
    		{
    			if($row[0] == $passForDelete)
    			{
            	    return true;
                }
    		}
    		ibase_free_result($result);
    		ibase_free_query($query);
    		ibase_commit($trans);
    		$connection->CloseDBConnection();
    		return false;
    }
    
    ///************************************************************************************************///
    ///                         ORDERS FUNCTIONS                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    

    /**
     * @param int $patId
     * @return int
     */
    public function checkDiagnos2($patId)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);

        $QueryText = "select count(diagnos2.id) from toothexam
                               inner join treatmentcourse on (toothexam.treatmentcourse_fk = treatmentcourse.id)
                               inner join diagnos2 on (treatmentcourse.id = diagnos2.treatmentcourse)
                            where toothexam.id = '$patId'";

        $Query = ibase_prepare($trans, $QueryText);
        $Result = ibase_execute($Query);
        $Row = ibase_fetch_row($Result);

        ibase_commit($trans);
        ibase_free_query($Query);
        ibase_free_result($Result);
        $connection->CloseDBConnection();
        return $Row[0];
    }
    
    /**
     * @param string $patId
     * @param string $docId
     * @param string $assistId
     * @return boolean
     */
    public function nhealth_addTreatmentcourse($patId, $docId, $assistId)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);

        //create treatmencourse 
        $QueryText = "insert into treatmentcourse(id, doctor_fk, patient_fk) values (null, ".$docId.", ".$patId.") RETURNING id";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        if($row = ibase_fetch_row($result))
        {
            $tcid=$row[0];
        }
         
        ibase_free_query($query);
        ibase_free_result($result);
        //create diagnos2 for general procedures
        $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,GARANTDATE,STACKNUMBER,PROTOCOL_FK) 
        values (null, $tcid, ".$docId.", null, null, null, -1, null)";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
    	ibase_free_query($query);
        
        //create diagnos2 for general procedures
        $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,GARANTDATE,STACKNUMBER,PROTOCOL_FK) 
        values (null, $tcid, ".$docId.", null, null, null, -2, null)";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
    	ibase_free_query($query);
        
        //create diagnos2 for general procedures
        $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,GARANTDATE,STACKNUMBER,PROTOCOL_FK) 
        values (null, $tcid, ".$docId.", null, null, null, -3, null)";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
    	ibase_free_query($query);
        
        //create toothexam with afterflag = 0
    	$QueryText = "insert into TOOTHEXAM(ID, DOCTORID, ASSISTANTID, AFTERFLAG, TREATMENTCOURSE_FK, EXAMDATE)
                 values($patId, $docId, ";
        if(($assistId != null)&&($assistId != ""))
        {
            $QueryText .= $assistId.", ";
        }
        else
        {
            $QueryText .= "null, ";
        }
        $QueryText .= "0, $tcid, 'now')";
         
    	$query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        
        
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
    
    
      /**
    * @param  int $addition_id
     * @param string $addition_date
     * @return boolean
     */
    public function updateAdditionDate($addition_id, $addition_date)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update TREATMENTADDITIONS set SIGNDATE = '$addition_date' where ID=$addition_id";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
 
 /**
     * @param string $search
     * @param string[] $specialities
     * @return HealthPlanMKB10MKBProtocol[]
     */ 
     public function searchProtocols($search, $specialities)//sqlite
     {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select 
                                        protocols.id,
                                        protocols.name,
                                        protocols.healthtype,
                                        mkb10.id,
                                        mkb10.shifr,
                                        mkb10.name
                                    from protocols
                
                                    left join protocols_f43
                                        on (protocols.id = protocols_f43.protocol_fk)
                                                        
                                    left join mkb10
                                        on (mkb10.id = protocols.mkb10)
                
                                    where protocols_f43.protocol_fk is null 
                                        and lower(protocols.name) like lower('%$search%')";
          if(nonull($specialities)&&count($specialities)>0)
            {
               $QuerySpec="";
               foreach($specialities as $spec)
               {
                	if($QuerySpec!='')
                    {
                        $QuerySpec.=' or ';
                    }
                    $QuerySpec .= "(protocols.healthtype=$spec->id or protocols.healthtype is null)"; 
               }
               $QueryText.=" and (".$QuerySpec.")";
            }               
//            file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
         $query = ibase_prepare($trans, $QueryText);
         $result = ibase_execute($query);
         $protocols = array();
         while ($row = ibase_fetch_row($result))
         {
            $prot = new HealthPlanMKB10MKBProtocol;
            $prot->ID = $row[0];
            $prot->NAME = $row[1];
                        //$prot->F43 = $d->protocol->F43;
            $prot->F43list = $this->getProtocolF43list($row[0], $trans);
            $prot->healthtype = $row[2];
            $prot->MKB10 = new HealthPlanMKB10MKB;
            $prot->MKB10->ID = $row[3];
            $prot->MKB10->SHIFR = $row[4];
            $prot->MKB10->NAME = $row[5];
            $prot->Templates = array();
    
            $TemplQuery = "select id, name, 
                (select sum(healthproc.price) 
                from healthproc
                inner join templateproc on (healthproc.id = templateproc.healthproc)
                where templateproc.template = template.id) 
                                
                from template where protocol = ".$prot->ID;
            $templquery = ibase_prepare($trans, $TemplQuery);
            $templresult = ibase_execute($templquery);
            while ($templrow = ibase_fetch_row($templresult))
            {
                $templ = new HealthPlanMKB10Template;
                $templ->ID = $templrow[0];
                $templ->NAME = $templrow[1];
                $templ->cost = $templrow[2];
                $templ->Protocol = $prot;
                            /* NEWTODO */
                $ToolTipSQL = "select templateproc.visit, healthproc.planname
                            from templateproc
                            inner join healthproc
                            on templateproc.healthproc = healthproc.id
                            where templateproc.template = ".$templ->ID;
                $ToolTipQuery = ibase_prepare($trans, $ToolTipSQL);
                $ToolTipResult = ibase_execute($ToolTipQuery);
                $tooltip = '';
                while ($tooltiprow = ibase_fetch_row($ToolTipResult))
                {
                    $tooltip .=  '<font size="13"><b>'.$tooltiprow[0].'</b></font> : '.$tooltiprow[1].'<br/>';
                }
                if(($templ->cost != 0)&&($templ->cost != null))
                {
                    $tooltip .= '<font size="15"><b>∑'.$templ->cost.'</b></font>';
                }
                $templ->toolTip = $tooltip;
                ibase_free_query($ToolTipQuery);
                ibase_free_result($ToolTipResult);
                            
                            
                $prot->Templates[] = $templ;
            }
                        
            ibase_free_query($templquery);
            ibase_free_result($templresult);
            $protocols[] = $prot;
        }
        return $protocols;
    }
    
       /**
  * @param boolean $isQuick
  * @param resource $trans
  * @return HealthPlanMKB10QuickF43[]
  */
  
  public function getF43($isQuick, $trans=null)//sqlite
  {
         if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
        
        	$QueryText = "
			select 
                ID,
                F43,
                SIGN||' - '||NAME
			from F43 ".($isQuick?"where ISQUICK=1 ":"")."order by SEQNUMBER";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$f43_array=array();
			while($row = ibase_fetch_row($result))
			{		 
                $f43=new HealthPlanMKB10QuickF43();
                $f43->ID=$row[0]; 
                $f43->F43=$row[1]; 
                $f43->NAME=$row[2]; 
                
                $f43_array[]=$f43;
			}
			ibase_free_query($query);
			ibase_free_result($result);
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $f43_array;
  }
  
    /**
     * @param int $f43Id
     * @return HealthPlanMKB10MKBProtocol[]
     */ 
     public function getQuickProtocols($f43Id)//sqlite
     {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select 
                                        p.id,
                                        p.name
                                  from protocols_f43 pf
                                  left join protocols p on p.id=pf.protocol_fk
                                  where pf.f43_fk=$f43Id  and p.isquick=1";
        
         $query = ibase_prepare($trans, $QueryText);
         $result = ibase_execute($query);
         $protocols = array();
         while ($row = ibase_fetch_row($result))
         {
            $prot = new HealthPlanMKB10MKBProtocol;
            $prot->ID = $row[0];
            $prot->NAME = $row[1];
            $protocols[] = $prot;
        }
        
        ibase_free_query($query);
		ibase_free_result($result);

        ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $protocols;
    }
    
    /**
     * @param int $protocolId
     * @return HealthPlanMKB10Template[]
     */ 
     public function getQuickTemplates($protocolId)//sqlite
     {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select t.id, t.name from template t where t.protocol=$protocolId ";
        
         $query = ibase_prepare($trans, $QueryText);
         $result = ibase_execute($query);
         $protocols = array();
         while ($row = ibase_fetch_row($result))
         {
            $prot = new HealthPlanMKB10Template;
            $prot->ID = $row[0];
            $prot->NAME = $row[1];
            $protocols[] = $prot;
        }
        
        ibase_free_query($query);
		ibase_free_result($result);
         ibase_commit($trans);
         $connection->CloseDBConnection();

        return $protocols;
    }
    

    /**
     * @param HealthPlanMKB10Diagnos2 $diagnos2
     * @param string $patientId
     * @param string $assistantId
     * @param boolean $isAfterExamsEnabled
     * @param int $priceIndex
     * @return boolean
     */
    public function saveQuickTreatmentCourse($diagnos2, $patientId, $assistantId, $isAfterExamsEnabled, $priceIndex)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);

        $values=array();
        $values[]=$diagnos2->TREATMENTCOURSE;
        $values[]=$diagnos2->Doctor->docid;
        $values[]=nullcheck($diagnos2->DIAGNOSDATE,true);
        $values[]=$diagnos2->Template->ID;
        $values[]=$diagnos2->STACKNUMBER;
        $values[]=$diagnos2->protocol->ID;
        $values[]='null';
        $values[]=nullcheck($diagnos2->TEETH,true);;
        $QueryText = "insert into DIAGNOS2(TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,TEMPLATE,STACKNUMBER,PROTOCOL_FK,F43,TEETH) 
                        values (".implode(',',$values).") returning id";

        $query = ibase_prepare($trans, $QueryText);
         $result = ibase_execute($query);
         if ($row = ibase_fetch_row($result))
         {
            $diagnos2->ID=$row[0];
        }
        
        
        ibase_free_query($query);
		ibase_free_result($result);
         
        $QueryText = "select 
                        tp.visit,
                        hp.shifr,
                        hp.planname,
                        hp.uop,
                        hp.price".sql_checkPriceIndex($priceIndex).",
                        hp.proctime,
                        tp.stacknumber,
                        hp.healthtype,
                        hp.healthtypes,
                        hp.expenses,
                        hp.discount_flag, 
                        hp.salary_settings,
                        hp.id, 
                        hp.completeexamdiag,
                        hp.name
                        from templateproc tp
                        left join healthproc hp on hp.id=tp.healthproc 
                        where tp.template=".$diagnos2->Template->ID;
        
        $query = ibase_prepare($trans, $QueryText);
         $result = ibase_execute($query);
         $procedures = array();
      //   file_put_contents("C:/tmp.txt", $assistantId."\n".nullcheck($assistantId)."\n".$patientId."\n".var_export($diagnos2,true));
         while ($row = ibase_fetch_row($result))
         {
            $proc = new HealthPlanMKB10ModuleTreatmentProc;
            $proc->visit = $row[0];
            $proc->procShifr = $row[1];
            $proc->planName = $row[2];
            $proc->courseName = $row[14];
            $proc->procUOP = $row[3];
            $proc->procPrice = $row[4];
            $proc->procTime = $row[5];
            $proc->doctorID = $diagnos2->Doctor->docid;
            $proc->dateClose = $diagnos2->DIAGNOSDATE;
            $proc->stacknumber = $row[6];
            $proc->procHealthtype = $row[7];
            $proc->procHealthtypes = $row[8];
            $proc->procExpenses = $row[9];
            $proc->assistantID = $assistantId;
            $proc->proc_count = 1;
            $proc->discount_flag = $row[10];
            $proc->salary_settings = $row[11];
            $proc->healthproc_fk = $row[12];
            $proc->complete_examdiag = $row[13];
            $proc->F39OUT = Form039Module::get039ProcValues(0,$proc->healthproc_fk);
            $proc->isComplete=true;
            $proc->farms=$this->getQuickTreatmentCourseFarms($proc->healthproc_fk,$trans);
            $procedures[] = $proc;
        }
        
      // file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($procedures, true));
        ibase_free_query($query);
		ibase_free_result($result);
  
        ibase_commit($trans);
        $connection->CloseDBConnection();

        return $this->saveTreatmentProc($diagnos2->ID, $procedures, $patientId, $isAfterExamsEnabled, $priceIndex);
    }
    
    /**
  * @param string $healthproc_fk
  * @param resource $trans
  * @return HealthPlanMKB10QuickF43[]
  */
  
  public function getQuickTreatmentCourseFarms($healthproc_fk, $trans=null)//sqlite
  {
         if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
        
            $QueryText = "select hpf.farm,
                            hpf.quantity from healthprocfarm hpf where hpf.healthproc=".$healthproc_fk;

            $query = ibase_prepare($trans, $QueryText);
             $result = ibase_execute($query);
             $farms = array();
             while ($row = ibase_fetch_row($result))
             {
                $farm = new HealthPlanMKB10ModuleTreatmentFarmsProc;
                $farm->farmID = $row[0];
                $farm->farmQuantity = $row[1];
             
                $farms[] = $farm;
            }
            ibase_free_query($query);
    		ibase_free_result($result);
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $farms;
  }
  
  
    /**
     * 
     * @return HealthPlanMKB10MKBProtocol[]
     */
    public function getProtocolWithASR()
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $QueryText = "select 
                    protocols.id,
                    protocols.name,
                    protocols.anamnestxt,
                    protocols.statustxt,
                    protocols.recomendtxt,
                    protocols.epicrisistxt
                from protocols
                where (select count(*) from protocols_f43 where protocols.id = protocols_f43.protocol_fk) > 0   
                order by protocols.name";
            
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($result))
            {
                $obj = new HealthPlanMKB10MKBProtocol;
                $obj->ID = $row[0];
                $obj->NAME = $row[1];
                $obj->ANAMNESTXT = text_blob_encode($row[2]);
                $obj->STATUSTXT = text_blob_encode($row[3]);
                $obj->RECOMENDTXT = text_blob_encode($row[4]);
                $obj->EPICRISISTXT = text_blob_encode($row[5]);
                $rows[] = $obj;
            }
            ibase_commit($trans);
            ibase_free_query($query);
            ibase_free_result($result);
            $connection->CloseDBConnection();
            return $rows;
    }
    
    
    
    
    
    
  
  /**
  * @param int $item
  * @param string $passForDelete
  * @return Object
  */
  public function deleteDiagnos2($item, $passForDelete)
  { 
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $successObj = new stdClass();
        $success = 0;
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
                
                //check count of treatmentproces for deleted diagnos2
                $sqlCheck = "select count(treatmentproc.id)
                        from treatmentproc
                        where treatmentproc.diagnos2 = $item  and treatmentproc.dateclose is not null";
                $queryCheck = ibase_prepare($trans, $sqlCheck);
                $resultCheck = ibase_execute($queryCheck);
                $rowCheck = ibase_fetch_row($resultCheck);
                
                if($rowCheck[0] < 1)
                {
            	    $success = 1;//DELETE ONLY DIAGNOS2
                }
                else
                {
                    //check is exist PROCEDURES WITH INVOICES or ONLY PROCEDURES
            	    $success = 2;//ONLY PROCEDURES WITHOUT INVOICES
                    $sqlProcCheck = "select
                            (select invoices.invoicenumber from invoices where invoices.id = treatmentproc.invoiceid),  
                            (select sum(accountflow.summ) from accountflow where accountflow.oninvoice_fk = treatmentproc.invoiceid),
                            (select invoices.createdate from invoices where invoices.id = treatmentproc.invoiceid),
                            treatmentproc.name
                            
                            from treatmentproc
                            
                            where treatmentproc.diagnos2 = $item";
                    $queryProcCheck = ibase_prepare($trans, $sqlProcCheck);
                    $resultProcCheck = ibase_execute($queryProcCheck);
                    $isInvoice = false;
                    while($rowProcCheck = ibase_fetch_row($resultProcCheck))
                    {
                        if($rowProcCheck[0] != null) //PROCEDURES WITH INVOICES
                        {
                            
                            $invoice = new stdClass();
                            $invoice->invNumber = $rowProcCheck[0];
                            $invoice->invDate = $rowProcCheck[2];
                            $invoice->trtName = $rowProcCheck[3];
                            $invoice->invCash = false;
                            
                            if($success != 4)
                            {
                                $success = 3;
                            }
                            if($rowProcCheck[1] != null && $rowProcCheck[1] > 0) //PROCEDURES WITH INVOICES AND CASH
                            {
                                $success = 4;
                                $invoice->invCash = true;
                            }
                            
                            $successObj->invoices[] = $invoice;
                        }
                    }
                }
			}
		}
        $successObj->success = $success;
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
        return $successObj;
  }
  
      /**
  * @param int $item
  * @param string $passForDelete
  * @param int $securityCode
  * @return int
  */
  public function deleteConfirmDIagnos2($item, $passForDelete, $securityCode)
  { 
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        /*
            $securityCode
            2 - pass for treatment
            3 - pass for account
            4 - pass for account
            
        */
        if($securityCode == 2)
        {
            $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
        }
        else if($securityCode == 3)
        {
            $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
        }
        else if($securityCode == 4)
        {
            $QueryText = "select users.passwrd from users where users.name = 'passForAccountDel'";
        }
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
        
        
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
           	    $success = 1;
			}
            else
            {
           	    $success = 0;
            }
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
        return $success;
  }
  
  
  
  
  
  
    
    
    
}

?>
<?php

require_once "Connection.php";
require_once "Utils.php";

class StoreModule_Farm
{				
	/**
	* @var string
	*/
	var $ID;         

	/**
	* @var string
	*/
	var $NAME;
    
	/**
	* @var string
	*/
	var $label;
    
	/**
	* @var string
	*/
	var $BASEUNITOFMEASUREID;  
	
	/**
	* @var string
	*/
	var $BASEUNITOFMEASURENAME;
 
	/**
	* @var string
	*/
	var $NOMENCLATUREARTICLENUMBER;	
	
	/**
	* @var string
	*/
	var $MINIMUMQUANTITY;
    
	/**
	* @var string
	*/
	var $OPTIMALQUANTITY;
    
	/**
	* @var StoreModule_UnitOfMeasure[]
	*/
	var $unitsOfMeasure;
    
    /**
     * @var number
     */
     var $rest;
     
    /**
     * @var number
     */
     var $orderCount;
    /**
     * @var bool
     */
     var $isValid;
     
   	/**
	* @var boolean
	*/
	var $isOpen;
}

class StoreModule_Order
{				
	/**
	* @var string
	*/
	var $ID;  

	/**
	* @var string
	*/
	var $NUMBER;
    
	/**
	* @var string
	*/
	var $label;
    
	/**
	* @var string
	*/
	var $TIMESTAMP;
    
    	/**
	* @var string
	*/
	var $STATUS;
    
	/**
	* @var string
	*/
	var $STATUS_TIMESTAMP;
    
	/**
	* @var StoreModule_Farm[]]
	*/
	var $children;
    
   	/**
	* @var boolean
	*/
	var $isOpen;		
}
class StoreModule_UnitOfMeasure
{				
	/**
	* @var string
	*/
	var $ID;  

	/**
	* @var string
	*/
	var $FARMID;  
	
	/**
	* @var string
	*/
	var $NAME;

	/**
	* @var string
	*/
	var $COEFFICIENT; 	
	
	/**
	* @var string
	*/
	var $ISBASE; 
	
	/**
	* @var string
	*/
	var $MODIFIED;		
}

class StoreModule
{		
    /**
    * @param StoreModule_Farm $p1 
    * @param StoreModule_UnitOfMeasure $p2
    * @param StoreModule_Order $p3
    * @return void 
    */    
	public function registertypes($p1, $p2, $p3) {}
    

    /**
    * @param resource $trans
	* @return StoreModule_Order[]
	*/ 
	public function getStroreOrders($trans=null) 
	{
	   $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select  so.id,
                            so.storeordernumber,
                            so.storeordertimestamp,
                            so.storeorderstatus,
                            so.statustimestamp
                        from storeorders so
                        order by so.storeordertimestamp";	
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $order=new StoreModule_Order;
                $order->ID=$row[0];
                $order->NUMBER=$row[1];
                $order->label=$row[1];
                $order->TIMESTAMP=$row[2];
                $order->STATUS=$row[3];
                $order->STATUS_TIMESTAMP=$row[4];
                $order->children=StoreModule::getStoreOrderItems($row[0],$trans);
                $res[]=$order;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
     	/** 						
    * @param StoreModule_Order[] $orders			
    * @param resource $trans
    * 
	* @return boolean
	*/ 
	public function updateStroreOrdersStatus($orders, $trans=null) 
	{
	   $res=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
            foreach($orders as $order)
            {
        			$QueryText = "update storeorders
                    set storeorderstatus=$order->STATUS,
                        statustimestamp='$order->STATUS_TIMESTAMP'
                        where storeordernumber=$order->NUMBER";
                         //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);	
    			$query = ibase_prepare($trans, $QueryText);
    			ibase_execute($query);
    			ibase_free_query($query);
            }


			
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
    /**
     *@param string $orderId
     *  @param resource $trans
	* @return StoreModule_Farm[]
	*/ 
	public static function getStoreOrderItems($orderId, $trans=null) 
	{
	    $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
    			$QueryText = "select  sot.id,
                                        sot.farmname,
                                        sot.storeordercount,
                                        sot.unitofmeasurename
                                    from storeorderitems sot
                                    where sot.fk_storeorder=$orderId
                                    order by sot.farmname";	
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $orderItem=new StoreModule_Farm;
                $orderItem->ID=$row[0];
                $orderItem->NAME=$row[1];
                $orderItem->label=$row[1];
                $orderItem->orderCount=$row[2];
                $orderItem->BASEUNITOFMEASURENAME=$row[3];
                $res[]=$orderItem;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
	/** 									
 	* @param string[] $farmsIds
    *
	* @return StoreModule_Farm[]
	*/ 
	public function getFarmsToOrder($farmsIds) 
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = 'select 		
        				farm.id,
        				farm.parentid,
        				farm.nodetype,
        				farm.name,
        				unitofmeasure2.id "BASEUNITOFMEASUREID",
        				unitofmeasure2.name "BASEUNITOFMEASURENAME",				
        				farm.nomenclaturearticlenumber,
        				farm.remark,				
        				farm.farmgroupid,
        				farmgroup.code,
        				farmgroup.name,
        				farm.minimumquantity,
                        farm.optimalquantity
            		from farm			 
            			 left join farmgroup on (farmgroup.id = farm.farmgroupid)
            			 left join
                         (
                            select
                                unitofmeasure.id,
                                unitofmeasure.farmid,
                                unitofmeasure.name
                            from unitofmeasure
                            where (unitofmeasure.isbase = 1)
                         )
                         unitofmeasure2 on (unitofmeasure2.farmid = farm.id)
            		where farm.id is null ';
        foreach ($farmsIds as $ids)
        {
            $QueryText .= " or farm.id = ".$ids;
        }
        
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);

		$rows = array();
        while ($row = ibase_fetch_row($result))
		{
			$obj = new StoreModule_Farm();
			
			$obj->ID = $row[0]; 
			//$obj->PARENTID = $row[1];
			//$obj->NODETYPE = $row[2];
			$obj->NAME = $row[3];
			$obj->BASEUNITOFMEASUREID = $row[4];
			$obj->BASEUNITOFMEASURENAME = $row[5];			
			$obj->NOMENCLATUREARTICLENUMBER = $row[6];
			//$obj->REMARK = $row[7];		
			//$obj->FARMGROUPID = $row[8];
			//$obj->FARMGROUPCODE = $row[9];
			//$obj->FARMGROUPNAME = $row[10];
			$obj->MINIMUMQUANTITY = $row[11];
			$obj->OPTIMALQUANTITY = $row[12];
			
                $QueryText2 = "select
					ID,			    
					NAME,
					COEFFICIENT,
					ISBASE
				from UNITOFMEASURE
				where FARMID = $obj->ID";
				$query2 = ibase_prepare($trans, $QueryText2);
				$result2 = ibase_execute($query2);
				$rows2 = array();	
				while ($row2 = ibase_fetch_row($result2))
				{ 
					$obj2 = new StoreModule_UnitOfMeasure;
					$obj2->ID = $row2[0];						
					$obj2->NAME = $row2[1];		
					$obj2->COEFFICIENT = $row2[2];						
					$obj2->ISBASE = $row2[3];
					$rows2[] = $obj2;
                }
				ibase_free_query($query2);
				ibase_free_result($result2);
                
			$obj->unitsOfMeasure = $rows2;
            $obj->orderCount = 1;
            $obj->isValid = true;
			$rows[] = $obj;
		}	
		
		ibase_free_query($query);
		ibase_free_result($result);		
		
		return $rows; 
	}
    
    	/** 						
    * @param string $clientId			
 	* @param StoreModule_Farm[] $farmsToOrder					
    * @param string $orderNumber	
	* @return string
	*/ 
	public function createOrder($clientId, $farmsToOrder, $orderNumber) 
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        //INSERT ORDER START
		 $sqlOrder = "insert into storeorders (id, storeordernumber, clientid) values (null, $orderNumber, '$clientId') returning id";
         $queryOrder = ibase_prepare($trans, $sqlOrder);
         $resultOrder = ibase_execute($queryOrder);
        $row = ibase_fetch_row($resultOrder);
        $orderId = $row[0];
        ibase_free_query($queryOrder);
        ibase_free_result($resultOrder);	
        //INSERT ORDER END
        
        //INSERT ORDERITEMS END
        foreach ($farmsToOrder as $farm)
        {
            $sqlFarm = "insert into storeorderitems (farmname, unitofmeasurename, storeordercount, fk_storeorder)
                            values ('".safequery($farm->NAME)."', '".safequery($farm->BASEUNITOFMEASURENAME)."', 
                                     '$farm->orderCount', $orderId)";
            $queryFarm = ibase_prepare($trans, $sqlFarm);
            ibase_execute($queryFarm);
            ibase_free_query($queryFarm);
        }
        //INSERT ORDERITEMS END
        
        //$this->sendOrderToEmail($clientId, $farmsToOrder, $orderNumber);
		
        ibase_commit($trans);
        $connection->CloseDBConnection();
        
		return $orderNumber; 
	}
}
?>
<?php

require_once "Connection.php";
require_once "Utils.php";

class ApplicationSettingRecord
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $PARAMETERNAME;

    /**
    * @var string
    */
    var $PARAMETERVALUE;  
}  


class ApplicationSetting
{
    
    /**
     * @param ApplicationSettingRecord $p1
     * @return void
     */
    public function registerTypes($p1){}
    
	/**
	* @param string $parameterName
	* @return ApplicationSettingRecord
	*/
	public function getApplicationSettings($parameterName)
	{
			$connection = new Connection();
		 
			// âñòàíîâëþºìî ç'ºäíàííÿ ç ÁÄ
			$connection->EstablishDBConnection();		
			
			// ñòàðòóºìî òðàíçàêö³þ
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			// ôîðìóºìî òåêñò çàïèòó
			$QueryText = "
			select 
				ID, 
				PARAMETERVALUE 
			from APPLICATIONSETTINGS
			where PARAMETERNAME = '$parameterName'";
			
			// ãîòóºìî ñåðâåð äî çàïèòó
			$query = ibase_prepare($trans, $QueryText);
			
			// âèêîíóºìî çàïèò
			$result = ibase_execute($query);
				
			$returnObj = new ApplicationSettingRecord();
			$returnObj->PARAMETERNAME = $parameterName;			
				
			if ($row = ibase_fetch_row($result))
			{
				$returnObj->ID = $row[0];				
				$returnObj->PARAMETERVALUE = $row[1];
			}			
			// ï³äòâåðäæóºìî òðàíçàêö³þ
			ibase_commit($trans);
			
			// âèäàëÿºìî çàïèò
			ibase_free_query($query);
			
			// âèäàëºìî ðåçóëüòàò çàïèòó
			ibase_free_result($result);

			// çàêðèâàºìî ç'ºäíàííÿ ç ÁÄ
			$connection->CloseDBConnection();	
			
			// ïîâåðòàºìî ðåçóëüòàò âèêîíàííÿ ôóíêö³¿
			return $returnObj;
	}
	
	/**
	* @param ApplicationSettingRecord[] $newApplicationSettingRecords
    * @return boolean
	*/
	public function setApplicationSettings($newApplicationSettingRecords) 
	{ 	
			$connection = new Connection();
		 
			// âñòàíîâëþºìî ç'ºäíàííÿ ç ÁÄ
			$connection->EstablishDBConnection();		
			
			// ñòàðòóºìî òðàíçàêö³þ
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			for ($i=0; isset($newApplicationSettingRecords[$i]); $i++)
			{ 
				$newApplicationSettingRecord = $newApplicationSettingRecords[$i];
			
				if (($newApplicationSettingRecord->ID == "") || ($newApplicationSettingRecord->ID == "null"))
				{
					// ôîðìóºìî òåêñò çàïèòó
					$QueryText = "
					select ID from APPLICATIONSETTINGS
					where (PARAMETERNAME = '$newApplicationSettingRecord->PARAMETERNAME')";
					
					// ãîòóºìî ñåðâåð äî çàïèòó
					$query = ibase_prepare($trans, $QueryText);
				
					// âèêîíóºìî çàïèò
					$result = ibase_execute($query);
					
					if ($row = ibase_fetch_row($result))
					{
						$newApplicationSettingRecord->ID = $row[0];
					}						
				}			
			
				// ôîðìóºìî òåêñò çàïèòó
				$QueryText = "
				update or insert into APPLICATIONSETTINGS
				(
					ID, 
					PARAMETERNAME,
					PARAMETERVALUE
				)
				values (";
				
				if (($newApplicationSettingRecord->ID == "") || ($newApplicationSettingRecord->ID == "null"))
				{
					$QueryText = $QueryText."null, ";
				}
				else
				{
					$QueryText = $QueryText."$newApplicationSettingRecord->ID, ";
				}	
					
				if (($newApplicationSettingRecord->PARAMETERNAME == "") || ($newApplicationSettingRecord->PARAMETERNAME == "null"))
				{
					$QueryText = $QueryText."null, ";
				}
				else
				{
					$QueryText = $QueryText."'".safequery($newApplicationSettingRecord->PARAMETERNAME)."', ";
				}
				
				if (($newApplicationSettingRecord->PARAMETERVALUE == "") || ($newApplicationSettingRecord->PARAMETERVALUE == "null"))
				{
					$QueryText = $QueryText."null)";
				}
				else
				{
					$QueryText = $QueryText."'".safequery($newApplicationSettingRecord->PARAMETERVALUE)."')";
				}
				
				// ãîòóºìî ñåðâåð äî çàïèòó
				$query = ibase_prepare($trans, $QueryText);
			
				// âèêîíóºìî çàïèò
				ibase_execute($query);				
			}
			
			// ï³äòâåðäæóºìî òðàíçàêö³þ
			$success = ibase_commit($trans);
			
			// âèäàëÿºìî çàïèò
			ibase_free_query($query);	

			// çàêðèâàºìî ç'ºäíàííÿ ç ÁÄ
			$connection->CloseDBConnection();	
			
			// ïîâåðòàºìî ðåçóëüòàò âèêîíàííÿ ôóíêö³¿
			return $success;		
	}
}
?>
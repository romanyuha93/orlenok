<?php

require_once "Connection.php";
require_once "Utils.php";

class SuppliersDictionaryModuleSupplier
{				
	/**
	* @var string
	*/
	var $ID;    
	
	/**
	* @var string
	*/
	var $NAME;

	/**
	* @var string
	*/
	var $LEGALNAME;          

	/**
	* @var string
	*/
	var $ADDRESS;
	 
	/**
	* @var string
	*/
	var $ORGANIZATIONTYPE;  
	
	/**
	* @var string
	*/
	var $CODE;
	
	/**
	* @var string
	*/
	var $DIRECTOR;

	/**
	* @var string
	*/
	var $TELEPHONENUMBER;    
	
	/**
	* @var string
	*/
	var $FAX;
	
	/**
	* @var string
	*/
	var $SITENAME;

	/**
	* @var string
	*/
	var $EMAIL; 		
	
	/**
	* @var string
	*/
	var $ICQ;
	
	/**
	* @var string
	*/
	var $SKYPE;
}




class SuppliersDictionaryModule
{		
	/**
	*	@param SuppliersDictionaryModuleSupplier $p1	
	*/
	public function registertypes($p1) 
	{
	}

	/**  
     * @param $searchtext
	* @return SuppliersDictionaryModuleSupplier[]
	*/ 
	public function getSuppliersDictionaryModuleData($searchtext) //sqlite
	{
			$connection = new Connection();
			 
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			
			$QueryText = "
			select
					ID,
					NAME,
					LEGALNAME,
					ADDRESS,
					ORGANIZATIONTYPE,
					CODE,
					DIRECTOR,
					TELEPHONENUMBER,
					FAX,
					SITENAME,
					EMAIL,
					ICQ,
					SKYPE
			from SUPPLIER
            
            where lower(NAME) containing lower('".$searchtext."')
            
			order by NAME";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$rows = array();		
			
			while ($row = ibase_fetch_row($result))				    // öèêë ïî çàïèñàì ðåçóëüòàòó çàïèòó
			{ 
				$obj = new SuppliersDictionaryModuleSupplier;		// ñòâîðþºìî íîâèé îá'ºêò SuppliersDictionaryModuleSupplier
							
				$obj->ID = $row[0];					
				$obj->NAME = $row[1];
				$obj->LEGALNAME = $row[2];
				$obj->ADDRESS = $row[3];
				$obj->ORGANIZATIONTYPE = $row[4];
				$obj->CODE = $row[5];
				$obj->DIRECTOR = $row[6];
				$obj->TELEPHONENUMBER = $row[7];
				$obj->FAX = $row[8];
				$obj->SITENAME = $row[9];
				$obj->EMAIL = $row[10];
				$obj->ICQ = $row[11];
				$obj->SKYPE = $row[12];
				$rows[] = $obj;	
			}	
			ibase_commit($trans);
			ibase_free_query($query);	
			ibase_free_result($result);
			$connection->CloseDBConnection();		
			return $rows;
	}		
	
	/**
 	* @param string $supplierId
    * @return boolean
 	*/ 		 		
	public function deleteSuppliersDictionaryModuleRecords($supplierId) 
	{ 
			$connection = new Connection();  
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
				
			
			$QueryText = "delete from SUPPLIER where ID = $supplierId";
				
		
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);		
			$success = ibase_commit($trans);
			ibase_free_query($query);	
			$connection->CloseDBConnection();	
			
			if (ibase_errmsg() != false)
			{
				trigger_error(ibase_errmsg(), E_USER_ERROR);				
			}		
			
			return $success;	
	}
	
	
	/**
 	* @param SuppliersDictionaryModuleSupplier $newSuppliersDictionaryModuleSupplier
	* @return SuppliersDictionaryModuleSupplier
 	*/
  public function updateSuppliersDictionaryModuleSupplier($newSuppliersDictionaryModuleSupplier) 
  {
			$connection = new Connection();
			$connection->EstablishDBConnection();	
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			if (($newSuppliersDictionaryModuleSupplier->ID == "") || ($newSuppliersDictionaryModuleSupplier->ID == "null"))
			{
				$newSuppliersDictionaryModuleSupplier->ID = ibase_gen_id("GEN_SUPPLIER_ID");
			}

			$QueryText = 		  
			"update or insert into SUPPLIER
			(
				ID,                
				NAME,              
				LEGALNAME,         
				ADDRESS,           
				ORGANIZATIONTYPE,  
				CODE,              
				DIRECTOR,          
				TELEPHONENUMBER,   
				FAX,               
				SITENAME,          
				EMAIL,             
				ICQ,               
				SKYPE   
			)
			values($newSuppliersDictionaryModuleSupplier->ID, '".safequery($newSuppliersDictionaryModuleSupplier->NAME)."', ";	
			
			if (($newSuppliersDictionaryModuleSupplier->LEGALNAME == "") || ($newSuppliersDictionaryModuleSupplier->LEGALNAME == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->LEGALNAME)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->ADDRESS == "") || ($newSuppliersDictionaryModuleSupplier->ADDRESS == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->ADDRESS)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->ORGANIZATIONTYPE == "") || ($newSuppliersDictionaryModuleSupplier->ORGANIZATIONTYPE == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."$newSuppliersDictionaryModuleSupplier->ORGANIZATIONTYPE, ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->CODE == "") || ($newSuppliersDictionaryModuleSupplier->CODE == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->CODE)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->DIRECTOR == "") || ($newSuppliersDictionaryModuleSupplier->DIRECTOR == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->DIRECTOR)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->TELEPHONENUMBER == "") || ($newSuppliersDictionaryModuleSupplier->TELEPHONENUMBER == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->TELEPHONENUMBER)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->FAX == "") || ($newSuppliersDictionaryModuleSupplier->FAX == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->FAX)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->SITENAME == "") || ($newSuppliersDictionaryModuleSupplier->SITENAME == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->SITENAME)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->EMAIL == "") || ($newSuppliersDictionaryModuleSupplier->EMAIL == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->EMAIL)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->ICQ == "") || ($newSuppliersDictionaryModuleSupplier->ICQ == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->ICQ)."', ";
			}
			
			if (($newSuppliersDictionaryModuleSupplier->SKYPE == "") || ($newSuppliersDictionaryModuleSupplier->SKYPE == "null"))
			{
				$QueryText = $QueryText."null)";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newSuppliersDictionaryModuleSupplier->SKYPE)."')";
			}

			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			ibase_free_query($query);
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();	
			return $newSuppliersDictionaryModuleSupplier;
  }
}
?>
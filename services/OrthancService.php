<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "Settings.php";
require_once "ServerFileModule.php";

class OrthancService 
{
	//private static $serverAddress;
	//private static $patientsListUrl;		
	
	/*public function __construct() {
		OrthancService::$serverAddress = $_SERVER['SERVER_ADDR'].':8042';
	}*/
	
	/**
	* @param string $dicomServerPatientId
	*/
	public function getPatientInstances($dicomServerPatientId)
	{				
		if ($curl = curl_init()) 
        {
			curl_setopt($curl, CURLOPT_URL, Settings::DICOM_PACS_URL."/patients/$dicomServerPatientId/instances");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$out = curl_exec($curl);	
			curl_close($curl);
			
			return json_decode($out);
		}
	}	
	
	/**
	* @param string $path
	* @param string $extension
	* @param string $serverName
	* @param integer $patientId	
    * @param resource $trans
	*/
	private function savePatientInstanceToDB($path, $extension, $serverName, $name, $patientId, $trans = null)
	{
	   $isThumbnailable = true;
       		
		try 
		{
			$thumb = PhpThumbFactory::create($path);  
		} 
		catch (Exception $e) 
		{
			$isThumbnailable = false;
		}
 
		if ($isThumbnailable) 
		{
			$thumb->resize(160, 160)->save($path.'_thumb');
			chmod($path.'_thumb',0777);
		}
	
		$sf = new ServerFileModule_ServerFile();
		
		$sf->name = $name.".".$extension;
		$sf->extention = $extension;
		$sf->serverName = $serverName;
		$sf->patientId = $patientId;
		$sf->isThumbnailable = $isThumbnailable;
        $sf->type = -333;
		$sf->comment = '';
		
		ServerFileModule::addServerFile($sf, $trans);
	}
	
	/**
	* @param string $path
	* @param string $content
	*/
	private function savePatientInstanceToFileSystem($path, $content)
	{
		$fp = fopen($path, 'w');
		fwrite($fp, $content);				
		fclose($fp);	

		chmod($path, 0777);
	}	
	
	public function getPatientsList()
	{				
		if ($curl = curl_init()) 
        {
			curl_setopt($curl, CURLOPT_URL, Settings::DICOM_PACS_URL.'/patients?expand');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$out = curl_exec($curl);	
			curl_close($curl);
			
			return json_decode($out);
		}
	}
	
	/**
	* @param string $patientId
	* @param string[] $dicomServerPatientIds
	*
	* @return boolean
	*/
	public function savePatientInstanceImagesToFileManagerDirectory($patientId, $dicomServerPatientIds)
	{	
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
       
        
        //Get servernames of files that already in fm *** START ***
	    $sql = "select SERVERNAME
                        from SERVERFILE
                where PATIENTID = ".$patientId." and SERVERFILE.TYPE = -333";
                
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."***addServerFile***".$QueryText);
                
		$query = ibase_prepare($trans, $sql);
		$result = ibase_execute($query);
		$fm_files = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $fm_files[] = $row[0];
		}	
		ibase_free_query($query);
		ibase_free_result($result);
        //Get servernames of files that already in fm *** END ***
		
		$result = false;
        
		foreach ($dicomServerPatientIds as $dicomServerPatientId)
        {
    	    //Get orthanc patient instance
    		$instances = $this->getPatientInstances($dicomServerPatientId);
			//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt").var_export($instances, true));
    		if($instances != null)
			{
				$result = true;
				foreach ($instances as $instance)
				{		
					$instanceId = $instance->ID;
					if(!in_array($instanceId, $fm_files))
					{
						$url = Settings::DICOM_PACS_URL."/instances/$instanceId/preview";
				
						if ($curl = curl_init()) {
							curl_setopt($curl, CURLOPT_URL, $url);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							$out = curl_exec($curl);
							curl_close($curl);		
			
							$serverName = $instanceId;	
							$name = $instance->MainDicomTags->InstanceCreationDate."_".$instance->IndexInSeries;			
							//$extension = 'png';
							//$name = $serverName.'.'.$extension;
							//$path = Settings::FILE_STORAGE.'/'.$name;
							
							$path = dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$serverName;
			
							$this->savePatientInstanceToFileSystem($path, $out);
							$this->savePatientInstanceToDB($path, 'png', $serverName, $name, $patientId, $trans);
						}
					}
				}
			}
        }
        
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $result;
	}
    
    /**
	* @param string $patientId
	* @param string $orthancPatientId
	* @param string $orthancPatientName
	*
	* @return boolean
	*/
	public function linkPatientWithOrthanc($patientId, $orthancPatientId, $orthancPatientName)
	{
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $del_sql = "delete from SERVERFILE where PATIENTID = ".$patientId." and SERVERFILE.TYPE = -333";	
		$del_query = ibase_prepare($trans, $del_sql);
		ibase_execute($del_query);
		ibase_free_query($del_query);
        
        if($orthancPatientId != 'null')
        {
            $orthancPatientId = "'".$orthancPatientId."'";            
        }
        if($orthancPatientName != 'null')
        {
            $orthancPatientName = "'".safequery($orthancPatientName)."'";
        }
        
		$sql = "update PATIENTS set
                        ORTHANCID = ".$orthancPatientId.",
                        ORTHANCPNAME = ".$orthancPatientName."
                    where ID = ".$patientId; 
		$query = ibase_prepare($trans, $sql);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
	}
}
/*
$orthancService = new OrthancService();
//print_r($orthancService->getPatientsList());



//$patientId = -2147483500;
$dicomServerPatientId = 'f50a2119-feb07087-5c9feecf-ac7fd7aa-498fd9e7';
print_r($orthancService->getPatientInstances($dicomServerPatientId));
//print_r($orthancService->savePatientInstanceImagesToFileManagerDirectory($patientId, $dicomServerPatientId));	
	*/
?>
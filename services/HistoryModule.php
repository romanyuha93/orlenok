<?php

require_once "Connection.php";
require_once "Utils.php";

class HistoryModuleDictionaryObject
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;
 }

class HistoryModuleDateDoctors
{				
    /**
    * @var string
    */
    var $EXAMDATE;

   /**
    * @var string
    */
    var $DOCTORID;

   /**
    * @var string
    */
    var $SHORTNAME;
     
    /**
    * @var string
    */
    var $EXAMID;
}

class HistoryModuleExamTooth
{
    /**
    * @var string
    */
    var $zub;

    /**
    * @var string
    */
    var $DESCRIPTION;
}
 
class HistoryModuleExam
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $DOCTORID;

    /**
    * @var string
    */
    var $EXAM;

    /**
    * @var string
    */
    var $EXAMDATE;
    
     /**
    * @var string
    */
    var $EXAMID;
 }

class HistoryModuleIllHistory
{
   /**
    * @var string
    */
    var $PATIENTID;

    /**
    * @var string
    */
    var $DATEOFRECORD;

    /**
    * @var string
    */
    var $EXAMDATE;

    /**
    * @var string
    */
    var $RECORDTYPE;

    /**
    * @var string
    */
    var $HISTORYTEXT;

    /**
    * @var string
    */
    var $DOCTORID;

    /**
    * @var int
    */
    var $NN;
    /**
    * @var string
    */
    var $EXAMID;

}
 
class HistoryModuleDoctor
{
    /**
    * @var string
    */
    var $ID;
     
    /**
    * @var string
    */
    var $SHORTNAME;
}

class HistoryModuleClinicRegistrationData
{
	/**
	* @var string
	*/
	var $NAME;
	
	/**
	* @var string
	*/
	var $ADDRESS;
	
	/**
	* @var string
	*/
	var $TELEPHONENUMBER;	
	
	/**
	* @var string
	*/
	var $FIRSTNAME;
	 
	/**
	* @var string
	*/
	var $MIDDLENAME;
	 
	/**
	* @var string
	*/
	var $LASTNAME;
	
	/**
	* @var string
	*/
	var $BIRTHDAY;
	
	/**
	* @var string
	*/
	var $PATIENTADDRESS;	
}
 
 
class HistoryModule
{		
    
     /**
     * @param HistoryModuleDictionaryObject $p1
     * @param HistoryModuleDateDoctors $p2
     * @param HistoryModuleExamTooth $p3
     * @param HistoryModuleExam $p4
     * @param HistoryModuleIllHistory $p5
     * @param HistoryModuleDoctor $p6
     * @param HistoryModuleClinicRegistrationData $p7
     * 
     * @return void
     */
    public function registerTypes($p1, $p2, $p3, $p4, $p5, $p6, $p7){}
  
    /** 
 	* @param string $nowdate
 	* @return HistoryModuleDoctor[]
	*/ 
    public function getDoctors($nowdate) 
     { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	    $QueryText ="select ID, SHORTNAME from DOCTORS 
                     where dateworkend is null or dateworkend>='$nowdate'";		
		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDoctor;
          $obj->ID = $row[0];
          $obj->SHORTNAME = $row[1];
		  $objs[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
     
    /** 
 	* @param string $examID
    * @return HistoryModuleExam[]
	*/
     public function getToothExam($examID) 
     {
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select exam, examdate, examid from TOOTHEXAM where EXAMID = '$examID'";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleExam;
          $obj->EXAM = $row[0];
          $obj->EXAMDATE = $row[1];
          $obj->EXAMID = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /** 
 	* @param int $ID
    * @return HistoryModuleDateDoctors[]
	*/ 
     public function getDateDoctors($ID)
     {
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select TOOTHEXAM.EXAMDATE, TOOTHEXAM.DOCTORID, DOCTORS.SHORTNAME, EXAMID  from TOOTHEXAM, DOCTORS  where TOOTHEXAM.ID = $ID
					 AND TOOTHEXAM.DOCTORID = DOCTORS.ID
					 ORDER BY EXAMID DESC";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDateDoctors;
          $obj->EXAMDATE = $row[0];
          $obj->DOCTORID = $row[1];
          $obj->SHORTNAME = $row[2];
          $obj->EXAMID = $row[3];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /** 
 	* @param string $examID 
    * @return HistoryModuleIllHistory[]
	*/ 
     public function getIllHistory($examID) 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select DOCTORID, DATEOFRECORD, RECORDTYPE, HISTORYTEXT, TOOTHEXAM FROM ILLHISTORY 
                    WHERE TOOTHEXAM = '$examID'";		
		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleIllHistory;
          $obj->DOCTORID = $row[0];
          $obj->DATEOFRECORD = $row[1];
          $obj->RECORDTYPE = $row[2];
          $obj->HISTORYTEXT = $row[3];
          $obj->TOOTHEXAM = $row[4];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
          
    /**
 	* @param HistoryModuleIllHistory $IllVO
    * @return boolean
 	*/
    public function addIllHistory($IllVO) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into ILLHISTORY (patientid, doctorid, examdate, dateofrecord, recordtype, historytext, toothexam) 
        values ('$IllVO->PATIENTID','$IllVO->DOCTORID','$IllVO->EXAMDATE','$IllVO->DATEOFRECORD','$IllVO->RECORDTYPE','".safequery($IllVO->HISTORYTEXT)."','$IllVO->EXAMID')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
    * @param HistoryModuleIllHistory $IllVO
    * @return boolean
 	*/
    public function updIllHistory ($IllVO) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update ILLHISTORY set DOCTORID = '$IllVO->DOCTORID',
											DATEOFRECORD ='$IllVO->DATEOFRECORD',
											RECORDTYPE = '$IllVO->RECORDTYPE',
    										HISTORYTEXT = '".safequery($IllVO->HISTORYTEXT)."'
					     where TOOTHEXAM ='$IllVO->EXAMID'
					     and RECORDTYPE = '$IllVO->RECORDTYPE'"; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    } 

    /** 
 	* @return HistoryModuleDictionaryObject[]
	*/ 
    public function getAnamnes() 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from ANAMNES ORDER BY NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDictionaryObject;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->DESCRIPTION = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param HistoryModuleDictionaryObject $Anm 
    * @return boolean
 	*/
    public function addAnamnes($Anm) 
     { 
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into ANAMNES (id, number, description) values (null,'$Anm->NUMBER','".safequery($Anm->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }     
    
    /** 
 	*  @return HistoryModuleDictionaryObject[]
	*/ 
    public function getStatus() 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from STATUS ORDER BY NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDictionaryObject;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->DESCRIPTION = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
     
    /**
 	* @param HistoryModuleDictionaryObject $Sts
    * @return boolean
 	*/
    public function addStatus($Sts) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into STATUS (id, number, description) values (null,'$Sts->NUMBER','".safequery($Sts->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    } 
    
    /** 
 	*  @return HistoryModuleDictionaryObject[]
	*/ 
    public function getDiagnos() 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from DIAGNOS ORDER BY NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDictionaryObject;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->DESCRIPTION = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param HistoryModuleDictionaryObject $Ds
    * @return boolean
 	*/
    public function addDiagnos($Ds) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into DIAGNOS (ID, NUMBER, DESCRIPTION) values (null,'$Ds->NUMBER','".safequery($Ds->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    } 
    
    /** 
 	*  @return HistoryModuleDictionaryObject[]
	*/ 
    public function getPlan() 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from HEALTH ORDER BY NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDictionaryObject;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->DESCRIPTION = $row[2];
		  $objs[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 
    
    /**
 	* @param HistoryModuleDictionaryObject $Pl
    * @return boolean
 	*/
    public function addPlan ($Pl) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into HEALTH (ID, NUMBER, DESCRIPTION) values (null,'$Pl->NUMBER','".safequery($Pl->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }      
    
    /** 
 	*  @return HistoryModuleDictionaryObject[]
	*/ 
    public function getConsult() 
    { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from CONSULT ORDER BY NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDictionaryObject;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->DESCRIPTION = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 
    
    /**
 	* @param HistoryModuleDictionaryObject $Cons
    * @return boolean
 	*/
    public function addConsult ($Cons) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into CONSULT (ID, NUMBER, DESCRIPTION) values (null,'$Cons->NUMBER','".safequery($Cons->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    } 
    
    /** 
 	*  @return HistoryModuleDictionaryObject[]
	*/ 
    public function getHealth() 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from HEALTHCOURSE ORDER BY NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new HistoryModuleDictionaryObject;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->DESCRIPTION = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 
    
    /**
 	* @param HistoryModuleDictionaryObject $Hl
    * @return boolean
 	*/
    public function addHealth ($Hl) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into HEALTHCOURSE (ID, NUMBER, DESCRIPTION) values (null,'$Hl->NUMBER','".safequery($Hl->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }   

	/** 
 	* @param int $patientID
 	* @return HistoryModuleClinicRegistrationData
	*/ 
  public function getClinicRegistrationData($patientID)  
  {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			
			$QueryText = 
			"select 
				NAME,
				ADDRESS,				
				TELEPHONENUMBER
			from CLINICREGISTRATIONDATA";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{
				$obj = new HistoryModuleClinicRegistrationData;	
				
				$obj->NAME = $row[0];	
				$obj->ADDRESS = $row[1];			
				$obj->TELEPHONENUMBER = $row[2];
			}	
			
			ibase_free_query($query);
      $QueryText = 
			"select 
				FIRSTNAME,
				MIDDLENAME,				
				LASTNAME,
								BIRTHDAY,
								ADDRESS
			from PATIENTS where ID = '$patientID'";
					
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
      if ($row = ibase_fetch_row($result))
			{				
				$obj->FIRSTNAME = $row[0];	
				$obj->MIDDLENAME = $row[1];			
				$obj->LASTNAME = $row[2];			
				$obj->BIRTHDAY = $row[3];			
				$obj->PATIENTADDRESS = $row[4];
			}	
			
			ibase_free_query($query);
			ibase_free_result($result);
			
			$success = ibase_commit($trans);
			
			$connection->CloseDBConnection();
			
			if (ibase_errmsg() != false)
			{
				trigger_error(ibase_errmsg(), E_USER_ERROR);				
			}
			
			return $obj;
  }
}
?>
<?php
//define('tfClientId','online');
    
require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once "Form039Module.php";


require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');

//$Form=new NHUltra;
//$Form->printForm('docx', 1, '20160422', 10001);

class NHColposcope_Object
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $docDate;
    /**
    * @var string
    */
    var $createTimestamp;
    /**
    * @var string
    */
    var $conclusion;
    /**
    * @var string
    */
    var $notes;
    /**
    * @var string
    */
    var $author_id;
    /**
    * @var string
    */
    var $author_name;
    /**
    * @var string
    */
    var $subdivision_id;
    /**
    * @var string
    */
    var $subdivision_name;
    /**
    * @var string
    */
    var $doctor_id;
    /**
    * @var string
    */
    var $doctor_name;
    /**
    * @var string
    */
    var $patient_id;
}

class NHColposcope_Doctor
{
    /**
     * @var string
     */
    var $doctor_id;
    /**
     * @var string
     */
    var $doctor_name;
}

class NHColposcope
{
    public static $isDebugLog = false;
       
    /**
     * @param NHColposcope_Object $p1
     * @param NHColposcope_Doctor $p2
     */
    public function registertypes($p1, $p2)
    {}
    
    /** 
 	* @param string $patientId
 	* @param int $skipCount
 	* @param int $getCount
	* @return NHColposcope_Object[]
	*/    
    public function getDataByPatientId($patientId, $skipCount, $getCount) 
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $resultArray = array();
        
		$sql = "select first $getCount skip $skipCount
                             ID,
                             CONCLUSION,
                             DOCDATE,
                             CREATETIMESTAMP,
                             AUTHOR_FK,
                             (select account.lastname ||' '|| account.firstname from account where account.id = AUTHOR_FK),
                             SUBDIVISION_FK,   
                             (select subdivision.name from subdivision where subdivision.id = SUBDIVISION_FK),
                             DOCTOR_FK,
                             (select doctors.shortname from doctors where doctors.id = DOCTOR_FK),
                             NOTES
                             
                    from NH_COLPOSCOPE
                where PATIENT_FK = $patientId
                    order by DOCDATE desc";
                                			
		$query = ibase_prepare($trans, $sql);
		$result = ibase_execute($query);
        	
		while ($row = ibase_fetch_row($result))
		{
			$obj = new NHColposcope_Object;
			$obj->id = $row[0];
			$obj->conclusion = text_blob_encode($row[1]);
			$obj->docDate = $row[2];
			$obj->createTimestamp = $row[3];
            $obj->author_id = $row[4];
            $obj->author_name = $row[5];
            $obj->subdivision_id = $row[6];
            $obj->subdivision_name = $row[7];
            $obj->doctor_id = $row[8];
            $obj->doctor_name = $row[9];
            $obj->notes = text_blob_encode($row[10]);
			$resultArray[] = $obj;
		}	
		ibase_free_query($query);
		ibase_free_result($result);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();		
		return $resultArray; 
    }
    
    /** 
 	* @param string $ultraId
    * @param object $trans
	* @return NHColposcope_Object
	*/    
    public static function getDataById($ultraId, $trans=null) 
    { 
        if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        
		$sql = "select
                             ID,
                             CONCLUSION,
                             DOCDATE,
                             CREATETIMESTAMP,
                             AUTHOR_FK,
                             (select account.lastname ||' '|| account.firstname from account where account.id = AUTHOR_FK),
                             SUBDIVISION_FK,   
                             (select subdivision.name from subdivision where subdivision.id = SUBDIVISION_FK),
                             DOCTOR_FK,
                             (select doctors.shortname from doctors where doctors.id = DOCTOR_FK),
                             NOTES
                             
                    from NH_COLPOSCOPE
                where ID = $ultraId";
                                			
		$query = ibase_prepare($trans, $sql);
		$result = ibase_execute($query);
        	
		if ($row = ibase_fetch_row($result))
		{
			$obj = new NHColposcope_Object;
			$obj->id = $row[0];
			$obj->conclusion = text_blob_encode($row[1]);
			$obj->docDate = $row[2];
			$obj->createTimestamp = $row[3];
            $obj->author_id = $row[4];
            $obj->author_name = $row[5];
            $obj->subdivision_id = $row[6];
            $obj->subdivision_name = $row[7];
            $obj->doctor_id = $row[8];
            $obj->doctor_name = $row[9];
            $obj->notes = text_blob_encode($row[10]);
		}	
		ibase_free_query($query);
		ibase_free_result($result);
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }	
        
		return $obj; 
    }
    
    
    /** 
 	* @param NHColposcope_Object $data
	* @return NHColposcope_Object
	*/    
    public function saveUltra($data) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if($data->id == null || $data->id == '')
        {
            $data->id = "null";
        }
        if($data->docDate != null && $data->docDate != '')
        {
            $data->docDate = "'".$data->docDate."'";
        }
        else
        {
            $data->docDate = "null";
        }
        if($data->doctor_id == null && $data->doctor_id == '')
        {
            $data->doctor_id = "null";
        }
        
        $save_sql = "update or insert into NH_COLPOSCOPE (
                                ID, 
                                CONCLUSION, 
                                DOCDATE, 
                                DOCTOR_FK, 
                                PATIENT_FK,
                                NOTES )
               values (
                            ".$data->id.", 
                            '".safequery($data->conclusion)."', 
                            ".$data->docDate.", 
                            ".$data->doctor_id.",
                            ".$data->patient_id.",
                            '".safequery($data->notes)."' )
               matching (ID) returning (ID)";
        
		if(NHColposcope::$isDebugLog)
		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($save_sql,true));
		}
            
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $data->id = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
         
            
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();
        return $data;
    }
    
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deleteDataByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from NH_COLPOSCOPE where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
    
    /**
     * @return NHColposcope_Doctor[]
     */
    public function getDoctors()//sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $doctors = array();
            //doctors
            $QueryText = "SELECT ID, SHORTNAME FROM DOCTORS 
            WHERE  doctors.isfired is null or doctors.isfired != 1";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $doc = new NHColposcope_Doctor();
                $doc->doctor_id = $row[0];
                $doc->doctor_name = $row[1];
                $doctors[] = $doc;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            
            ibase_commit($trans);
            
            $connection->CloseDBConnection();
            
            return $doctors;
    }
    
    
    
    // ********************* PRINT START ********************************
    /** 
    * @param string $docType
    * @param string $colpsID
    * @param string $colpsDate
    * @param string $patientID
    * @return string
    */ 
    public static function printForm($docType, $colpsID, $colpsDate, $patientID)
    {   
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $CLINIC_DATA = new PrintDataModule_ClinicRegistrationData(true, $trans);
            $PATIENT_DATA = new PrintDataModule_PatientData($patientID, $trans);
            $IMAGE_ARRAY = PrintDataModule::getColposcopeServerFilesArray($patientID, $colpsDate, $trans);
            $COLP_DATA = NHColposcope::getDataById($colpsID, $trans);
            
    		if(NHColposcope::$isDebugLog)
    		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($IMAGE_ARRAY,true));
    		}
                
            $TBS = new clsTinyButStrong;
            $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
            
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/ColposcopeConclusion.'.$docType, OPENTBS_ALREADY_UTF8);  
            
            
            // $TBS->NoErr=true;
                  
            $TBS->MergeBlock('imgblock', $IMAGE_ARRAY);
            $TBS->MergeField('CLINIC_DATA', $CLINIC_DATA);
            $TBS->MergeField('PATIENT_DATA', $PATIENT_DATA);
            $TBS->MergeField('COLP_DATA', $COLP_DATA);
                    
            $file_name = translit(str_replace(' ','_','ColposcopeConclusion'));
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().'_'.$file_name.'.'.$docType;   
           
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
                    
            return $form_path;
    }
        
    /** 
    * @param string $form_path
    * @return boolean
    */  
    public static function deletePrintedForm($form_path)
    {
        return deleteFileUtils($form_path);
    }
    // ********************* PRINT  END  ********************************
    
}

?>
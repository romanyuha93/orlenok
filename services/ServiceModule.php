<?php
	require_once "Connection.php";
	require_once "Settings.php";
	require_once "Utils.php";
	require_once "AdminService.php";
	class ServiceModule
	{
		const GLIC = '3026343527';
		/**
		* @param string $login
		* @param string $pass
		* @return string[]
		*/
		public function check($login, $pass)
		{
			$successLogin = false;
			$successPass = false;
			$authobj=AdminService::checkAutorization($login, $pass);
			if(nonull($authobj->ACCOUNT_KEY))
			{
				$successLogin = true;
				$successPass = true;
			}
			$rows = array();
			if($successLogin && $successPass)
				{
				AdminService::syncAndClearJornal();
				$_con = new Connection();
				$_con->EstablishDBConnection("main",false);
				$_trans = ibase_trans(IBASE_DEFAULT, $_con->connection);
				$logo_sql ="select clinicregistrationdata.logo from clinicregistrationdata";
				$logo_query = ibase_prepare($_trans, $logo_sql);
				$logo_result = ibase_execute($logo_query);
				$row_logo = ibase_fetch_row($logo_result);
				$imageBlob = null;
				$blob_info = ibase_blob_info($row_logo[0]);
				if ($blob_info[0] != 0)
				{
					$blob_hndl = ibase_blob_open($row_logo[0]);
					$imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
					ibase_blob_close($blob_hndl);
				}
				ibase_commit($_trans);
				ibase_free_query($logo_query);
				ibase_free_result($logo_result);
				$_con->CloseDBConnection();
				$authobj->LOGO=base64_encode($imageBlob);
			}
			else if($successLogin)
			{
				$authobj->LOGO = "LOGIN_FAULT";
			}
			else if($successPass)
			{
				$authobj->LOGO = "PASS_FAULT";
			}
			else
			{
				$authobj->LOGO = "LOGINPASS_FAULT";
			}
			return $authobj;
		}
		/**
		* @return string[]
		*/
		public function check_hd()
		{
			$_con = new Connection();
			$_con->EstablishDBConnection("main",false);
			$_trans = ibase_trans(IBASE_DEFAULT, $_con->connection);
			$db_version_sql = "select PARTVERSION from APPLICATIONVERSION where PARTNAME = 'DATABASE'";
			$db_version_sql2 = "execute procedure xxyyzz('97.114.109.124.49.52.124.100.105.114.124.49.124.114.101.103.124.56.124.35.97.108.108.101.104.118.120.114.113.106.102.119.110.112.115.110.114.119.114.114.119.".
				"118.122.102.118.65.87.53.55.56.120.84.97.110.54.53.49.85.65.108.103.108.81.120.54.52.56.54.122.55.53.45.51.48.101.100.48.52.54.48.56.52.".
				"56.54.48.97.102.54.101.51.50.53.99.53.102.57.54.52.56.99.57.50.50.50.124.49.52.124.97.114.109.126.117.98.100.97.99.98.118.97.106.102.100.".
				"104.103.108.97.105.122.110.113.121.88.53.55.54.71.102.68.88.52.57.57.114.104.101.74.111.78.73.49.48.54.49.122.55.53.45.54.57.99.50.101.49.".
				"99.48.52.99.55.101.48.48.56.52.100.50.57.100.57.99.56.98.100.99.50.51.101.102.102.102.124.49.124.100.105.114.126.114.107.101.102.100.100.110.100.".
				"119.107.99.107.102.98.111.120.119.110.115.120.118.114.122.67.69.86.87.53.55.55.116.83.80.76.53.54.50.110.112.99.85.67.72.82.57.48.54.54.122.".
				"55.53.45.57.56.97.48.48.98.97.52.101.52.53.53.50.54.97.102.98.52.98.54.49.48.57.48.102.101.52.52.56.48.99.50.124.56.124.114.101.103')";
			$db_version_query2 = ibase_prepare($_trans, $db_version_sql2);
			ibase_execute($db_version_query2);
			$db_version_query = ibase_prepare($_trans, $db_version_sql);
			$db_version_result = ibase_execute($db_version_query);
			$db_version_row = ibase_fetch_row($db_version_result);
			ibase_commit($_trans);
			ibase_free_query($db_version_query);
			ibase_free_result($db_version_result);
			$_con->CloseDBConnection();
			$rows = array();
			$rows[] = $db_version_row[0];
			$rows[] = Settings::SERVICES_VERSION_STRING;
			$rows[] = Settings::SMS_COST;
			$rows[] = Settings::LOCATION;
			$rows[] = Settings::SYNC_URL;
			$rows[] = "161610109109161016169zHLj576mrj500rOFoTju1384z75-29308fe5a81aca6f3bab905857b9dcac";//admin_m1
			$rows[] = "1681788161681617991616zojK578MWo045LODtcnP2918z75-fc3a04a367854e8679befc60d22612ae";//android2
			$rows[] = "1788161681617991616zYWV577sNX995frnvKHg0286z75-cde17a568b563591cc25ed59041634de";//android3
			$rows[] = "888888888888zQeV583oCm646NisLOOs2906z75-a5dadfaea04b89d12eff3bba5f4b08a5";//android4_new
			$rows[] = "816101717171691616178zedF587fMN118hTWvMVj7289z75-f3cc65e102c7f7a9d9bc315c8923c819";//android5
			$rows[] = "98881681089171716zFNJ576cTs499IRWIEiv1047z75-8667c0637b732ba55d65c29b535923ed";//android_m1
			$rows[] = "161610109109161091610zbGy576QXp500eowmuxd1400z75-8c32624e4fef7cde95b6584245ce94f4";//arm1_m1
			$rows[] = "816991016108108179zrcQ576Gnm500tKpFWVG1402z75-7f88a91bd62760d75fc975ddc198e689";//arm2_m1
			$rows[] = "161610109109161091610zlaK576gtH500BucwJCL1721z75-03c7f423199ac4710188a7da8ffc43a0";//comp_m1
			$rows[] = "161610109109161091616zRkl576Bpl500lBtMOGq1722z75-61cd33254d798f9d25d7d2344e7ef369";//comp_m3
			$rows[] = "101081616161616178169zpVk576PPn500HMqbKAc1723z75-95eeb09e261b6bd286fc41320b284b29";//comp_m4
			$rows[] = "1710161616108161616916zcVQ588AGV507tGLsXRO4526z75-fe85bc457b20844ca349128754a05825";//home_m1_new
			$rows[] = "1681681088988178zoOR588nXa498tWQOcJO8130z75-b2864a03b37a222a9f49d8d9a2fd2fd9";//home_m2_new
			$rows[] = "8825881088988178zriC588mGF507oLXRpSx4545z75-0875203acd86c2988f5298d63d177dd2";//home_m3_new
			$rows[] = "881681088988178zPvG588qHl507qhDjTGe4741z75-b697666cdef6d56522565af708aed212";//home_m4_new
			$rows[] = "1681681088988178zrCk588eub507Hvuybys4744z75-809f72596badc34cd647c8b22c530167";//home_m5_new
			$rows[] = "1610910161616169161710zwTk586BUP659tcTJGex1429z75-3b8a3316769538f459e30c494597c0e5";//iphone3
			$rows[] = "16917998816101689zebu580TuT888tcKsPYe4218z75-834519e310402ab37651c6ec2de2879a";//iphoneMariya_m1
			$rows[] = "81616161799817988zdeH586dNA702YnYBUEm7424z75-6531ddc38301bb391a7af587be7a80a7";//iphone_yana_m1
			$rows[] = "88981791016161689znjF156IdJ740GKGoWvk6083z75-00b46585b762f406adbb7333df8edfb5";//new_reg1_m1
			$rows[] = "899171016816168916zlEE156hyl740OplPXcP6090z75-8f6dd8cd8cd5589ef2faec2cafc82ba7";//new_reg1_m2
			$rows[] = "89917101681616898zlIj156HuF740XDuJJdi6097z75-a7352a401de64fc4a9adadcc243e07fb";//new_reg1_m3
			$rows[] = "8109991691716171017zghx156WVk740YyMAsUb6106z75-dbc4ce15219442d218b63f83e9eeff32";//newclinic_pc_m1
			$rows[] = "16161091617169810917zLih156Maa691gbgteLm7291z75-9861a51aa560fb65b53b199874020ff3";//pc_irina_m1
			$rows[] = "101089161717888916zpAt156hFu691YLfeXpv7292z75-1457e020b26e8dbedb885b74a353a8cb";//pc_irina_m2
			$rows[] = "8891616169910161010zpRh156lpW740YriqWHM6065z75-62f40e772572f347178388d0d52f7bf3";//pcnew
			$rows[] = "1010916881016101789zTMo580djW501rTrJdrI5184z75-026b50a55c827ee03c2caa208b14b684";//phone6_m1
			$rows[] = "910910881688161617zfXv156uSB740FvfcTRP6369z75-d4c8eab79ffaa63cc15917fadacf3999";//sony_m1
			return $rows;
		}
	}
?>
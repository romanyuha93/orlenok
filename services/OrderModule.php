<?php

require_once "Connection.php";

class OrderProcedureRecord
{
    /**
    * @var string
    */
    var $shifr;
    
    /**
    * @var string
    */
    var $name;
    
    /**
    * @var number
    */
    var $price;
    
    /**
    * @var number
    */
    var $proc_count;
    
    /**
    * @var number
    */
    var $uop;
    
    /**
    * @var string
    */
    var $doctor_fname;
    
    /**
    * @var string
    */
    var $doctor_sname;
    
    /**
    * @var string
    */
    var $doctor_lname;
    
    /**
    * @var string
    */
    var $dateclose;
    
    /**
    * @var int
    */
    var $visit;
    
    /**
    * @var int
    */
    var $healthtype;
    
    /**
    * @var int
    */
    var $discount_flag;
}

class OrderRecord
{
    /**
    * @var int
    */
    var $id;
       
    /**
    * @var string[]
    */
    var $tooth;   
       
    /**
    * @var string[]
    */
    var $toothside;
    
    /**
    * @var string
    */
    var $garantdate;
    
    /**
    * @var string
    */
    var $diagnos;
    
	/**
    * @var OrderProcedureRecord[]
    */
    var $procedures;
    
    /**
    * @var bool
    */
    var $paiding_flag;
    
    /**
    * @var string
    */
    var $order_shifr;
    
    /**
    * @var string
    */
    var $paid_date;
    
    /**
    * @var string
    */
    var $paidfix_date;
    
    /**
    * @var string
    */
    var $patient_fname;
    
    /**
    * @var string
    */
    var $patient_sname;
    
    /**
    * @var string
    */
    var $patient_lname;
    
    /**
    * @var string
    */
    var $patient_mobile;
    
    /**
    * @var string
    */
    var $patient_address;
    
    /**
    * @var string
    */
    var $doctor_fname;
    
    /**
    * @var string
    */
    var $doctor_sname;
    
    /**
    * @var string
    */
    var $doctor_lname;
    
    /**
    * @var string
    */
    var $template;
    
    /**
    * @var string
    */
    var $orderdate;
    
     /**
    * @var string
    */
    var $assistent_fname;
    
    /**
    * @var string
    */
    var $assistent_sname;
    
    /**
    * @var string
    */
    var $assistent_lname;
}
class OrderModulePatient
{
    /**
   * @var string
   */
	var $patient_id;
   /**
   * @var string
   */
	var $patient_fname; 
   /**
   * @var string
   */
	var $patient_lname; 
   /**
   * @var string
   */
	var $patient_sname;
   /**
   * @var string
   */
	var $patient_fio;
       
}

class OrderModule
{
     /**
     * @param OrderRecord $p1
     * @param OrderProcedureRecord $p2
     * @param OrderModulePatient $p3
     * @return void
     */
    public function registertypes($p1, $p2, $p3)
    {}
    
    /**
   * @param string $fio
   * @return OrderModulePatient[]
   */
   public function getPatientLazyList($fio)
   {
    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select patients.ID, 
                    patients.FIRSTNAME, patients.MIDDLENAME, patients.LASTNAME, patients.fullname
                 from patients
                 where lower(patients.fullname) starting lower('$fio')
                 and PRIMARYFLAG = 1 order by patients.LASTNAME";
        $Query = ibase_prepare($trans, $QueryText);
        $QueryResult=ibase_execute($Query);
        $rows = array();
		while ($row = ibase_fetch_row($QueryResult))
		{ 
            $obj = new OrderModulePatient;
            $obj->patient_id = $row[0];
            $obj->patient_lname = $row[1];
            $obj->patient_fname = $row[2];
            $obj->patient_sname = $row[3];
            $obj->patient_fio = $row[4];
            $rows[] = $obj;
		}
        ibase_free_query($Query);
        ibase_free_result($QueryResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
   }
    
	/** 
    * @param string $startDate
    * @param string $endDate
    * @param string $patientId
    * @param boolean $isCoursename
    * @return OrderRecord[]
	*/ 
    public function getOrdersWithProcedures($startDate, $endDate, $patientId, $isCoursename) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select o.id,
                            p.name,
                            o.shifr,
                            o.paidflag,
                            o.paiddate,
                            pat.firstname,
                            pat.middlename,
                            pat.lastname,
                            d.firstname,
                            d.middlename,
                            d.lastname,
                            o.paidfixdate,
                            t.name,
                            pat.address,
                            pat.mobilenumber,
                            o.diagnos2_fk,
                            d2.garantdate,
                            o.orderdate,
                            assis.firstname,
                            assis.middlename,
                            assis.lastname
                        from orders o
                        inner join diagnos2 d2 on (d2.id = o.diagnos2_fk)
                        inner join template t on (t.id = d2.template)
                        inner join protocols p on (p.id = t.protocol)
                        inner join patients pat on (pat.id = o.patientid)
                        inner join doctors d on (d.id = o.doctorid)
                        left join assistants assis on (assis.id = o.assistantid)";
        $isDate = false;
        if(($endDate != 'null')&&($endDate != null)&&($endDate != '')&&($endDate != '-'))
        {
            $isDate = true;
            $QueryText .= "  where o.orderdate >= '$startDate 00:00:00'
                and o.orderdate <= '$endDate 23:59:59' or o.paidflag = 0";
        }
        if(($patientId != 'null')&&($patientId != null)&&($patientId != '')&&($patientId != '-'))
        {
            if($isDate){
                $QueryText = substr($QueryText, 0, -18);
            }
            $QueryText .= " and pat.id = $patientId";
        }     
        $QueryText .= " order by o.paidflag, o.orderdate asc";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $rows = array();
        $res = array();
        while ($row = ibase_fetch_row($result))
        {
            $orderRec = new OrderRecord();
            $orderRec->id = $row[0];
            $orderRec->diagnos = $row[1];
            $orderRec->order_shifr = $row[2] == null ? '' : $row[2];
            $orderRec->paiding_flag = ((bool)$row[3]);
            $orderRec->paid_date = $row[4] == null ? '' : $row[4];
            $orderRec->patient_fname = $row[5] == null ? '' : $row[5];
            $orderRec->patient_sname = $row[6] == null ? '' : $row[6];
            $orderRec->patient_lname = $row[7] == null ? '' : $row[7];
            $orderRec->doctor_fname = $row[8] == null ? '' : $row[8];
            $orderRec->doctor_sname = $row[9] == null ? '' : $row[9];
            $orderRec->doctor_lname = $row[10] == null ? '' : $row[10];
            $orderRec->paidfix_date = $row[11] == null ? '' : $row[11];
            $orderRec->template = $row[12];
            $orderRec->patient_address = $row[13] == null ? '' : $row[13];
            $orderRec->patient_mobile = $row[14] == null ? '' : $row[14];
            $diagnos2_fk = $row[15];
            $orderRec->garantdate = $row[16] == null ? '' : $row[16];
            $orderRec->orderdate = $row[17] == null ? '' : $row[17];
            $orderRec->assistent_fname = $row[18] == null ? '' : $row[18];
            $orderRec->assistent_sname = $row[19] == null ? '' : $row[19];
            $orderRec->assistent_lname = $row[20] == null ? '' : $row[20];
            $QueryTextProc = "select tp.shifr, tp.name, tp.price, tp.proc_count,
                               tp.uop, tp.dateclose, d.firstname, d.middlename, d.lastname,
                               tp.visit, tp.healthtype, tp.invoiceid, tp.acts_insurance,
                               tp.discount_flag, tp.healthproc_fk, tp.NAMEFORPLAN
                            from treatmentproc tp
                               left join doctors d on (d.id = tp.doctor)
                            where order_fk = $row[0]";
            $queryProc = ibase_prepare($trans, $QueryTextProc);
            $resultsProc = ibase_execute($queryProc);
            $rowsProc = array();
            while ($rowsProc = ibase_fetch_row($resultsProc))
            {
                $orderProcedureRec = new OrderProcedureRecord();
                $orderProcedureRec->shifr = $rowsProc[0];
                if($isCoursename == true)
                {
                    $orderProcedureRec->name = $rowsProc[1];
                }
                else
                {
                    $orderProcedureRec->name = $rowsProc[15];
                }
                $orderProcedureRec->price= $rowsProc[2];
                $orderProcedureRec->proc_count = $rowsProc[3];
                $orderProcedureRec->uop = $rowsProc[4] == null ? '' : $rowsProc[4];;
                $orderProcedureRec->dateclose = $rowsProc[5] == null ? '' : $rowsProc[5];
                $orderProcedureRec->doctor_fname = $rowsProc[6] == null ? '' : $rowsProc[6];
                $orderProcedureRec->doctor_sname =$rowsProc[7] == null ? '' : $rowsProc[7];
                $orderProcedureRec->doctor_lname = $rowsProc[8] == null ? '' : $rowsProc[8];
                
                $orderRec->procedures[] = $orderProcedureRec;
            }
            ibase_free_query($queryProc);
            ibase_free_result($resultsProc);
            
            $QueryTextTooth = "select tooth, toothside from treatmentobjects where diagnos2 = $diagnos2_fk";
            $queryTooth = ibase_prepare($trans, $QueryTextTooth);
            $resultsTooth = ibase_execute($queryTooth);
            $rowsTooth = array();
            while ($rowsTooth = ibase_fetch_row($resultsTooth))
            {
                $orderRec->tooth[] = $rowsTooth[0];
                $orderRec->toothside[] = $rowsTooth[1];
            }
            $res[] = $orderRec;
            ibase_free_query($queryTooth);
            ibase_free_result($resultsTooth);
        }
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();    
            
		return $res;
	}
    
     /**
     * @param int $orderId
     * @return boolean
     */
    public function deleteOrder($orderId)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update orders set shifr = '', paidflag = 0, paiddate = null, paidfixdate = null where id = $orderId";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        $success = ibase_commit($trans);
        ibase_free_query($query);
        $connection->CloseDBConnection();
        return $success;
    }

     /**
     * @param int $orderId
     * @param string $shifr
     * @param string $paiddate
     * @param string $paidfixdate
     * @return boolean
     */
    public function fixOrder($orderId, $shifr, $paiddate, $paidfixdate)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update orders set shifr = '$shifr', paidflag = 1, paiddate = '$paiddate', paidfixdate = '$paidfixdate' where id = $orderId";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        $success = ibase_commit($trans);
        ibase_free_query($query);
        $connection->CloseDBConnection();
        return $success;
    }
    
}
?>
<?php

require_once "Connection.php";

class InsuranceModule_CompanyObject
{
    /**
	 * @var string
	 */
	var $id;
    /**
	 * @var string
	 */
	var $name;
    /**
	 * @var string
	 */
	var $type = 'company';
	/**
	 * @var string
	 */
	var $company_name;
	/**
	 * @var string
	 */
	var $company_phone;
    
	/**
	 * @var float
	 */
	var $summ;
	/**
	 * @var float
	 */
	var $summClosed;
    
    /**
     * @var InsuranceModule_InsuranceObject[]
     */
     var $children;
}

class InsuranceModule_InsuranceObject
{    
	/**
	 * @var string
	 */
	var $id;
    /**
	 * @var string
	 */
	var $name;
    /**
	 * @var string
	 */
	var $type = 'insurance';
	/**
	 * @var string
	 */
	var $insurance_number;
	/**
	 * @var string
	 */
	var $insurance_fromDate;
	/**
	 * @var string
	 */
	var $insurance_toDate;
    
	/**
	 * @var string
	 */
	var $patient_id;
	/**
	 * @var string
	 */
	var $patient_lname;
	/**
	 * @var string
	 */
	var $patient_fname;
	/**
	 * @var string
	 */
	var $patient_sname;
	/**
	 * @var float
	 */
	var $summ;
	/**
	 * @var float
	 */
	var $summClosed;
    /**
     * @var InsuranceModule_InvoiceObject[]
     */
     var $children;
}

class InsuranceModule_InvoiceObject
{    
	/**
	 * @var string
	 */
	var $id;
    /**
	 * @var string
	 */
	var $name;
    /**
	 * @var string
	 */
	var $type = 'invoice';
	/**
	 * @var string
	 */
	var $invoice_number;
    /**
	 * @var string
	 */
	var $invoice_date;
	/**
	 * @var float
	 */
	var $summClosed;
	/**
	 * @var float
	 */
	var $summ;
	/**
	 * @var string
	 */
	var $doctor_id;
	/**
	 * @var string
	 */
	var $doctor_lname;
	/**
	 * @var string
	 */
	var $doctor_fname;
	/**
	 * @var string
	 */
	var $doctor_sname;
    
}

class InsuranceModule
{
    /**
     * @param InsuranceModule_CompanyObject $p1
     * @param InsuranceModule_InsuranceObject $p2
     * @param InsuranceModule_InvoiceObject $p3
     * @return void
     */
    public function registerTypes($p1, $p2, $p3){}
    
    /**
     * @param string $startDate
     * @param string $endDate
     * @param boolean $all
     * @return InsuranceModule_CompanyObject
     */
    public function getInsuranceOLAPdata($startDate, $endDate, $all)
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $Query = "select
                    	 invoices.id,
                    	 invoices.createdate,
                    	 invoices.invoicenumber,
                    	 invoices.discount_amount,
                    	 coalesce(invoices.summ,0),
                    	 (select coalesce(sum(accountflow.summ),0)
                    		from accountflow
                    		where accountflow.oninvoice_fk = invoices.id) as paidincash,
                    	 doctors.id,
                    	 doctors.lastname,
                    	 doctors.firstname,
                    	 doctors.middlename,
                    
                         patientsinsurance.id,
                    	 patientsinsurance.policenumber,
                    	 patientsinsurance.policefromdate,
                    	 patientsinsurance.policetodate,
                    	 patients.id,
                    	 patients.lastname,
                    	 patients.firstname,
                    	 patients.middlename,
                    
                    	 insurancecompanys.id, 
                    	 insurancecompanys.name,
                    	 insurancecompanys.phone
                    
                    from invoices
                    	left join doctors
                    	on doctors.id = invoices.doctorid
                    	inner join patients
                    	on patients.id = invoices.patientid
                    	inner join patientsinsurance
                    	on patientsinsurance.id = invoices.patieninsurance_fk
                    	inner join insurancecompanys
                    	on insurancecompanys.id = patientsinsurance.companyid";
        if($all == false)    
        { 
            $Query .= " where invoices.createdate >= '$startDate 00:00:00'
                    	and invoices.createdate <= '$endDate 23:59:59'";
        }
                     
        $Query .= " order by insurancecompanys.id, patientsinsurance.id, invoices.id";
        $query = ibase_prepare($trans, $Query);
        $result = ibase_execute($query);
        $root = new InsuranceModule_CompanyObject;
        $root->id = '-2';
        $root->name = 'root';
        $root->type = 'root';
        
        $companyId = '-';
        $company = new InsuranceModule_CompanyObject; 
        
        $insuranceId = '-';
        $insurance = new InsuranceModule_InsuranceObject;
        
        $invoiceId = '-';
        $invoice = new InsuranceModule_InvoiceObject;
        
        $firstFlag = false;
        while ($row = ibase_fetch_row($result))
        {
            if($row[18] != $companyId)
            {
                if($companyId != '-')
                {
                    $root->children[] = $company; 
                }
                $companyId = $row[18];
                $company = new InsuranceModule_CompanyObject; 
                $company->id = $row[18];
                $company->name = $row[19];
                $company->company_name = $row[19];
                $company->company_phone = $row[20];
                
                $company->summ = $row[4];
                $company->summClosed = $row[5];
                
               
                    $insuranceId = $row[10];
                    $insurance = new InsuranceModule_InsuranceObject;
                    $insurance->id = $row[10];
                    $insurance->name = $row[15].' '.$row[16];
                    $insurance->insurance_number = $row[11];
                    $insurance->insurance_fromDate = $row[12];
                    $insurance->insurance_toDate = $row[13];
                    $insurance->patient_id = $row[14];
                    $insurance->patient_lname = $row[15];
                    $insurance->patient_fname = $row[16];
                    $insurance->patient_sname = $row[17];
                    
                    $insurance->summ = $row[4];
                    $insurance->summClosed = $row[5];
                    
                    $company->children[] = $insurance; 
               
                    $invoiceId = $row[0];
                    $invoice = new InsuranceModule_InvoiceObject;
                    $invoice->id = $row[0];
                    $invoice->invoice_date = $row[1];
                    $invoice->name = '';//$row[2];
                    $invoice->invoice_number = $row[2];
                    $invoice->summ = $row[4];
                    $invoice->summClosed = $row[5];
                    $invoice->doctor_id = $row[6];
                    $invoice->doctor_lname = $row[7];
                    $invoice->doctor_fname = $row[8];
                    $invoice->doctor_sname = $row[9];
                    $insurance->children[] = $invoice;
            }
            else
            {
                $company->summ += $row[4];
                $company->summClosed += $row[5];
                if($row[10] != $insuranceId)
                { 
                    
                    $insuranceId = $row[10];
                    $insurance = new InsuranceModule_InsuranceObject;
                    $insurance->id = $row[10];
                    $insurance->name = $row[15].' '.$row[16];
                    $insurance->insurance_number = $row[11];
                    $insurance->insurance_fromDate = $row[12];
                    $insurance->insurance_toDate = $row[13];
                    $insurance->patient_id = $row[14];
                    $insurance->patient_lname = $row[15];
                    $insurance->patient_fname = $row[16];
                    $insurance->patient_sname = $row[17];
                            
                    $insurance->summ = $row[4];
                    $insurance->summClosed = $row[5];
                    $company->children[] = $insurance; 
                    
                    
                    $invoiceId = $row[0];
                    $invoice = new InsuranceModule_InvoiceObject;
                    $invoice->id = $row[0];
                    $invoice->invoice_date = $row[1];
                    $invoice->name = '';//$row[2];
                    $invoice->invoice_number = $row[2];
                    $invoice->summ = $row[4];
                    $invoice->summClosed = $row[5];
                    $invoice->doctor_id = $row[6];
                    $invoice->doctor_lname = $row[7];
                    $invoice->doctor_fname = $row[8];
                    $invoice->doctor_sname = $row[9];
                    $insurance->children[] = $invoice;
                        
                }
                else
                {
                    $insurance->summ += $row[4];
                    $insurance->summClosed += $row[5];
                    
                    $invoiceId = $row[0];
                    $invoice = new InsuranceModule_InvoiceObject;
                    $invoice->id = $row[0];
                    $invoice->invoice_date = $row[1];
                    $invoice->name = '';//$row[2];
                    $invoice->invoice_number = $row[2];
                    $invoice->summ = $row[4];
                    $invoice->summClosed = $row[5];
                    $invoice->doctor_id = $row[6];
                    $invoice->doctor_lname = $row[7];
                    $invoice->doctor_fname = $row[8];
                    $invoice->doctor_sname = $row[9];
                    $insurance->children[] = $invoice;
                } 
            }
        }
        $root->children[] = $company; 
        ibase_free_query($query);
        ibase_free_result($result);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $root;
    }
    
   
         
    
}

?>
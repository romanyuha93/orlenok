<?php
require_once "Connection.php";
require_once "Utils.php";
require_once "ServiceModule.php";
require_once "AdminService.php";

class ToothFairyMobileServiceModuleUserObject
{
    /**
    * @var string[]
    */
    var $checkItems;
    
    /**
    * @var string
    */
    var $err_msg;
    
    /**
    * @var string
    */
    var $user_id;
    
    /**
    * @var string
    */
    var $user_shortname;
    
    /**
    * @var string
    */
    var $user_lname;
    
    /**
    * @var string
    */
    var $user_fname;
    
    /**
    * @var string
    */
    var $user_mname;
    
}

class ToothFairyMobileServiceModule
{
    /**
	* @param string $doctorslogin
	* @param string $doctorspassw
	* @return ToothFairyMobileServiceModuleUserObject
	*/
    public function check($doctorslogin, $doctorspassw) 
    {
        $authobj=(AdminService::checkAutorization($doctorslogin, $doctorspassw));
         $resultObject = new ToothFairyMobileServiceModuleUserObject;;
        $resultObject->err_msg = "x100";
        $ServiceInfo = new ServiceModule;
         $resultObject->checkItems = $ServiceInfo->check_hd();
                             //file_put_contents("C:/tmp.txt", var_export($authobj, true));
            if($authobj==null)
            {
                $resultObject->err_msg = "x101";
                return $resultObject;   
            }
            if($authobj->DOCTOR_ID == null)
            {
                $resultObject->err_msg = "x102";
                return $resultObject;  
            }

            if($authobj!=null&&$authobj->DOCTOR_ID != null)
            {
                
                $resultObject->user_id = $authobj->DOCTOR_ID;
                $resultObject->user_shortname = $authobj->DOCTOR_SHORTNAME;
                $resultObject->user_lname = $authobj->ACCOUNT_LASTNAME;
                $resultObject->user_fname =  $authobj->ACCOUNT_FIRSTNAME;
                $resultObject->user_mname =  $authobj->ACCOUNT_MIDDLENAME;
               
                
                return $resultObject;
            }
		return $resultObject;		
    }
    
    /**
	* @return boolean
	*/
    public function exitAutorization() 
    {
       return AdminService::exitAutorization();
    }
}

?>
<?php

require_once "Connection.php";
require_once "Utils.php";

class ChiefExpensesModule_TreatmentProc
{
    /**
     * @var int
     */
     var $num;
    /**
     * @var string
     */
     var $type;
     
    
    /**
     * @var string
     */
    var $proc_id;
    /**
     * @var string
     */
    var $proc_name;
    /**
     * @var string
     */
    var $proc_shifr;
    /**
     * @var string
     */
    var $proc_closedate;
    /**
     * @var number
     */
    var $proc_expenses_plane;
    /**
     * @var number
     */
    var $proc_expenses_fact;
    /**
     * @var string
     */
    var $doc_id;
    /**
     * @var string
     */
    var $doc_lname;
    /**
     * @var string
     */
    var $doc_fname;
    /**
     * @var string
     */
    var $doc_sname;
    
    
    /**
     * @var string
     */
    var $pat_id;
    /**
     * @var string
     */
    var $pat_lname;
    /**
     * @var string
     */
    var $pat_fname;
    /**
     * @var string
     */
    var $pat_sname;
    
    
    /**
     * @var string
     */
    var $ass_id;
    /**
     * @var string
     */
    var $ass_lname;
    /**
     * @var string
     */
    var $ass_fname;
    /**
     * @var string
     */
    var $ass_sname;
    
    
    
    /**
     * @var string
     */
    var $accflow_id;
    /**
     * @var number
     */
    var $accflow_summ;
    /**
     * @var string
     */
    var $accflow_note;
    /**
     * @var int
     */
    var $accflow_is_cashpayment;
    /**
     * @var string
     */
    var $accflow_operationtimestamp;
    /**
     * @var string
     */
    var $accflow_createdate;
    /**
     * @var string
     */
    var $accflow_group_id;
    /**
     * @var string
     */
    var $accflow_group_name;
    /**
     * @var int
     */
    var $accflow_group_color;
    /**
     * @var string
     */
    var $accflow_group_gridsequence;
}

class ChiefExpensesModule
{
    
    /**
     * @param ChiefExpensesModule_TreatmentProc $p1
     * @param ChiefExpensesModuleTreatmentProcOLAPData $p2
     * @return void
     */
    public function registertypes($p1, $p2){}
    
    
     /**
     * @param string $startDate
     * @param string $endDate
     * @return ChiefExpensesModuleTreatmentProcOLAPData[]
     */
    public function getTreatmentProcOLAP($startDate, $endDate)
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
       	$QueryText = "select
        t.id,
        t.shifr,
        t.name,
        t.nameforplan,
        t.price,
        t.doctor,
            d.lastname,
            d.firstname,
            d.middlename,
        t.dateclose,
        t.invoiceid,
        t.expenses,
        t.assistant,
            a.lastname,
            a.firstname,
            a.middlename,
        t.proc_count,
        t.healthproc_fk,
        t.expenses_fact,
        af.id,
        t.patient_fk,
            p.lastname,
            p.firstname,
            p.middlename,
        af.summ,
        af.operationnote,
        af.createdate,
        af.operationtimestamp,
        af.is_cashpayment,
        af.texp_group_fk,
            tg.name,
            tg.color,
            tg.gridsequence,
        d.shortname,
        a.shortname,
        p.shortname
        from treatmentproc t
            left join doctors d on(d.id=t.doctor)
            left join assistants a on(a.id=t.assistant)
            left join accountflow af on(af.texp_treatmentproc_fk=t.id)
                left join accountflow_texp_groups tg on(tg.id=af.texp_group_fk)
                left join patients p on(p.id=t.patient_fk)
        where (t.expenses!=0 or t.expenses_fact!=0) 
                and t.dateclose>'".$startDate. 
                "' and t.dateclose<'".$endDate."' 
                order by tg.gridsequence";
                                               
              
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
               
        $res_array=array();
		while ($row = ibase_fetch_row($result))
		{	
            $res=new ChiefExpensesModuleTreatmentProcOLAPData;
            $res->treatmentproc_id=$row[0];
            $res->treatmentproc_shifr=$row[1];
            $res->treatmentproc_name=$row[2];
            $res->treatmentproc_nameforplan=$row[3];
            $res->treatmentproc_price=$row[4];
            if($row[5]!=null)
            {
                $res->doctor_id=$row[5];
                $res->doctor_lastname=$row[6];
                $res->doctor_firstname=$row[7];
                $res->doctor_middlename=$row[8];
                $res->doctor_shortname=$row[33]; 
            }
            else
            {
                $res->doctor_id="NO_DOC";
            } 
            $res->treatmentproc_dateclose=$row[9];
            $res->treatmentproc_invoiceid=$row[10];
            $res->treatmentproc_expenses=$row[11]*($row[16]!=null?$row[16]:1);
            if($row[12]!=null)
            {
                $res->assistant_id=$row[12];
                $res->assistant_lastname=$row[13];
                $res->assistant_firstname=$row[14];
                $res->assistant_middlename=$row[15];
                $res->assistan_shortname=$row[34]; 
            }
            else
            {
                $res->doctor_id="NO_ASS";
            } 
            $res->treatmentproc_proc_count=$row[16];
            $res->treatmentproc_healthproc_fk=$row[17];
            $res->treatmentproc_expenses_fact=$row[18];

            $res->patient_id=$row[20];
            $res->patient_lastname=$row[21];
            $res->patient_firstname=$row[22];
            $res->patient_middlename=$row[23];             
            $res->patient_shortname=$row[35];          
            if($row[19]!=null)
            {
                $res->accountflow_summ=$row[24];
                $res->accountflow_operationnote=$row[25];
                $res->accountflow_createdate=$row[26];
                $res->accountflow_operationtimestamp=$row[27];
                $res->accountflow_is_cashpayment=$row[28];   
                $res->accountflow_id=$row[19];               
                if($row[29]!=null)
                {
                    $res->texp_group_id=$row[29];
                    $res->texp_group_name=$row[30];
                    $res->texp_group_color=$row[31];
                    $res->texp_group_gridsequence=$row[32];
                }
                else
                {
                    $res->texp_group_id="NO_TG";                 
                }
            }
            else
            {
                $res->accountflow_id="NO_AF";
                $res->texp_group_id="NO_AF_TG"; 
            }        
                 
            $res_array[]=$res;
		}			
		ibase_free_query($query);
		ibase_free_result($result);                       
        ibase_commit($trans);
        return $res_array;
    }
    
   
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @param boolean $byProcedures
    * 
    * @return ChiefExpensesModule_TreatmentProc[]
    */  
    public function getProcExpenses($startDate, $endDate, $byProcedures)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        //
        if($byProcedures)
        {
            $expenses_sql = "select
    
                                treatmentproc.id, 
                                treatmentproc.name,
                                treatmentproc.shifr,
                                treatmentproc.dateclose,
                                treatmentproc.expenses,
                                treatmentproc.expenses_fact,
                                treatmentproc.proc_count,
                                
                                doctors.id, 
                                doctors.lastname,
                                doctors.firstname,
                                doctors.middlename,
                                
                                patients.id,
                                patients.lastname,
                                patients.firstname,
                                patients.middlename,
                                
                                assistants.id,
                                assistants.lastname,
                                assistants.firstname,
                                assistants.middlename
                                
                                
                                
                                from treatmentproc
                                    inner join doctors
                                    on doctors.id = treatmentproc.doctor
                                    left join patients
                                    on patients.id = treatmentproc.patient_fk
                                    left join assistants
                                    on assistants.id = treatmentproc.assistant
                                
                                 where  treatmentproc.dateclose >= '$startDate 00:00:00'
                                        and treatmentproc.dateclose <= '$endDate 23:59:59'
                                        and treatmentproc.expenses > 0
                                 order by treatmentproc.dateclose, treatmentproc.shifr, treatmentproc.name, treatmentproc.id
            ";
            $expenses_query = ibase_prepare($trans, $expenses_sql);
            $expenses_result=ibase_execute($expenses_query);
            $rows = array();
            $i = 1;
    		while ($row = ibase_fetch_row($expenses_result))
    		{ 
                $obj = new ChiefExpensesModule_TreatmentProc;
                $obj->num = $i;
                $obj->type = 'PROCEDURE';
                $obj->proc_id = $row[0];
                $obj->proc_name = $row[1];
                $obj->proc_shifr = $row[2];
                $obj->proc_closedate = $row[3];
                $obj->proc_expenses_plane = $row[4]*$row[6];
                $obj->proc_expenses_fact = $row[5];
                $obj->doc_id = $row[7];
                $obj->doc_lname = $row[8];
                $obj->doc_fname = $row[9];
                $obj->doc_sname = $row[10];
                $obj->pat_id = $row[11];
                $obj->pat_lname = $row[12];
                $obj->pat_fname = $row[13];
                $obj->pat_sname = $row[14];
                $obj->ass_id = $row[15];
                $obj->ass_lname = $row[16];
                $obj->ass_fname = $row[17];
                $obj->ass_sname = $row[18];
                $i++;
                $rows[] = $obj;
    		}
        }
        else
        {
            $expenses_flow_sql =  " select
                    
                    treatmentproc.id, 
                    treatmentproc.name,
                    treatmentproc.shifr,
                    treatmentproc.dateclose,
                    treatmentproc.expenses,
                    treatmentproc.expenses_fact,
                    treatmentproc.proc_count,
                    
                    doctors.id, 
                    doctors.lastname,
                    doctors.firstname,
                    doctors.middlename,
                    
                    patients.id,
                    patients.lastname,
                    patients.firstname,
                    patients.middlename,
                    
                    assistants.id,
                    assistants.lastname,
                    assistants.firstname,
                    assistants.middlename,
                    
                                                accountflow.id,
                                                accountflow.summ,
                                                accountflow.operationnote,
                                                accountflow.is_cashpayment,
                                                accountflow.operationtimestamp, 
                                                accountflow.createdate,
                                        
                                                accountflow_texp_groups.id,
                                                accountflow_texp_groups.name,
                                                accountflow_texp_groups.color,
                                                accountflow_texp_groups.gridsequence
                                    
                    from accountflow
                    
                        left join accountflow_texp_groups
                        on accountflow.texp_group_fk = accountflow_texp_groups.id
                        inner join treatmentproc
                        on treatmentproc.id = accountflow.texp_treatmentproc_fk
                        inner join doctors
                        on doctors.id = treatmentproc.doctor
                        left join patients
                        on patients.id = treatmentproc.patient_fk
                        left join assistants
                        on assistants.id = treatmentproc.assistant
                    
                     where  accountflow.operationtimestamp >= '$startDate 00:00:00'
                            and accountflow.operationtimestamp <= '$endDate 23:59:59'
                            
                     order by treatmentproc.dateclose desc, accountflow.operationtimestamp, treatmentproc.shifr, treatmentproc.name, treatmentproc.id";
            $expenses_query = ibase_prepare($trans, $expenses_flow_sql);
            $expenses_result=ibase_execute($expenses_query);
            $rows = array();
            $i = 1;
            $proc_id = '';
            $procFirst = true;
    		while ($row = ibase_fetch_row($expenses_result))
    		{ 
                $obj = new ChiefExpensesModule_TreatmentProc;
                $obj->type = 'ACCOUNTFLOW';
                $obj->proc_id = $row[0];
                if($procFirst == true){
                    $proc_id=$obj->proc_id;
                    $procFirst = false;
                }
                if($proc_id != $obj->proc_id) {
                    $i++;
                }
                $obj->num = $i;
                $proc_id=$obj->proc_id;
                $obj->proc_name = $row[1];
                $obj->proc_shifr = $row[2];
                $obj->proc_closedate = $row[3];
                $obj->proc_expenses_plane = $row[4]*$row[6];
                $obj->proc_expenses_fact = $row[5];
                $obj->doc_id = $row[7];
                $obj->doc_lname = $row[8];
                $obj->doc_fname = $row[9];
                $obj->doc_sname = $row[10];
                $obj->pat_id = $row[11];
                $obj->pat_lname = $row[12];
                $obj->pat_fname = $row[13];
                $obj->pat_sname = $row[14];
                $obj->ass_id = $row[15];
                $obj->ass_lname = $row[16];
                $obj->ass_fname = $row[17];
                $obj->ass_sname = $row[18];
                
                $obj->accflow_id = $row[19];
                $obj->accflow_summ = $row[20];
                $obj->accflow_note = $row[21];
                $obj->accflow_is_cashpayment = $row[22];
                $obj->accflow_operationtimestamp = $row[23];
                $obj->accflow_createdate = $row[24];
                $obj->accflow_group_id = $row[25];
                $obj->accflow_group_name = $row[26];
                $obj->accflow_group_color = $row[27];
                $obj->accflow_group_gridsequence = $row[28];
                $rows[] = $obj;
    		}
        }
        ///
        ibase_free_query($expenses_query);
        ibase_free_result($expenses_result);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows; 
    }
  
  /** 
  * @param ChiefExpensesModule_TreatmentProc $accountObject
  * @return boolean
  */  
  public function updateTreatmentProc($accountObject)
  {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$update_sql = "update ACCOUNTFLOW set
                            ACCOUNTFLOW.OPERATIONNOTE = '".safequery($accountObject->accflow_note)."'
                        where ACCOUNTFLOW.ID = ".$accountObject->accflow_id;
             
        $update_query = ibase_prepare($trans, $update_sql);
		ibase_execute($update_query);
		ibase_free_query($update_query);
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
  }
}
class ChiefExpensesModuleTreatmentProcOLAPData
{
    /**
     * @var string
     */
      var $treatmentproc_id;
    /**
     * @var string
     */
      var $treatmentproc_shifr;
    /**
     * @var string
     */
      var $treatmentproc_name;
    /**
     * @var string
     */
      var $treatmentproc_nameforplan;
    /**
     * @var number
     */
      var $treatmentproc_price;
    /**
     * @var string
     */
      var $doctor_id;
    /**
     * @var string
     */
      var $doctor_lastname;
    /**
     * @var string
     */
      var $doctor_firstname;
    /**
     * @var string
     */
      var $doctor_middlename;
      /**
     * @var string
     */
      var $doctor_shortname;
    /**
     * @var string
     */
      var $treatmentproc_dateclose;
    /**
     * @var string
     */
      var $treatmentproc_invoiceid;
    /**
     * @var number
     */
      var $treatmentproc_expenses;
    /**
     * @var string
     */
      var $assistant_id;
    /**
     * @var string
     */
      var $assistant_lastname;
    /**
     * @var string
     */
      var $assistant_firstname;
    /**
     * @var string
     */
      var $assistant_middlename;
     /**
     * @var string
     */
      var $assistant_shortname;
    /**
     * @var int
     */
      var $treatmentproc_proc_count;
    /**
     * @var string
     */
      var $treatmentproc_healthproc_fk;
    /**
     * @var number
     */
      var $treatmentproc_expenses_fact;
    /**
     * @var string
     */
      var $accountflow_id;
    /**
     * @var string
     */
      var $patient_id;
    /**
     * @var string
     */
      var $patient_lastname;
    /**
     * @var string
     */
      var $patient_firstname;
    /**
     * @var string
     */
      var $patient_middlename;
     /**
     * @var string
     */
      var $patient_shortname;
    /**
     * @var number
     */
      var $accountflow_summ;
    /**
     * @var string
     */
      var $accountflow_operationnote;
    /**
     * @var string
     */
      var $accountflow_createdate;
    /**
     * @var string
     */
      var $accountflow_operationtimestamp;
    /**
     * @var string
     */
      var $accountflow_is_cashpayment;
    /**
     * @var string
     */
      var $texp_group_id;
    /**
     * @var string
     */
      var $texp_group_name;
    /**
     * @var int
     */
      var $texp_group_color;
    /**
     * @var int
     */
      var $texp_group_gridsequence;

}
?>
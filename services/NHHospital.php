<?php
//define('tfClientId','online');
    
require_once "Connection.php";
require_once "Utils.php";


class NHHospital_Data
{
    /**
    * @var NHHospital_Object[]
    */
    var $stationare;
    /**
    * @var bool
    */
    var $stationareOpened = false;
    
    /**
    * @var NHHospital_Object[]
    */
    var $pregnancy;
    /**
    * @var bool
    */
    var $pregnancyOpened = false;
}

class NHHospital_Object
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $patient_id;
    /**
    * @var string
    */
    var $startTimestamp;
    /**
    * @var string
    */
    var $endTimestamp;
}

class NHHospital
{
       
    /**
     * @param NHHospital_Object $p1
     */
    public function registertypes($p1)
    {}
    
    /** 
 	* @param string $patientId
 	* @param bool $selectPregnancy
	* @return NHHospital_Data
	*/    
    public function getDataByPatientId($patientId, $selectPregnancy) 
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $result = new NHHospital_Data();
        $result->stationare = array();
        $result->pregnancy = array();
        
        //get stationare
		$stat_sql = "select ID, FK_PATIENT, STARTTIMESTAMP, ENDTIMESTAMP
                            from NH_STATIONARE
                            where FK_PATIENT =$patientId 
                            order by STARTTIMESTAMP desc";
                                			
		$stat_query = ibase_prepare($trans, $stat_sql);
		$stat_result = ibase_execute($stat_query);
        	
		while ($row = ibase_fetch_row($stat_result))
		{
			$obj = new NHHospital_Object;
			$obj->id = $row[0];
			$obj->patient_id = $row[1];
			$obj->startTimestamp = $row[2];
			$obj->endTimestamp = $row[3];
            if($obj->endTimestamp == null && !$result->stationareOpened)
            {
                $result->stationareOpened = true;
            }
			$result->stationare[] = $obj;
		}	
		ibase_free_query($stat_query);
		ibase_free_result($stat_result);
        
        //get pregnancy
		$stat_sql = "select ID, FK_PATIENT, STARTTIMESTAMP, ENDTIMESTAMP
                            from NH_PREGNANCY
                            where FK_PATIENT =$patientId 
                            order by STARTTIMESTAMP desc";
                                			
		$stat_query = ibase_prepare($trans, $stat_sql);
		$stat_result = ibase_execute($stat_query);
        	
		while ($row = ibase_fetch_row($stat_result))
		{
			$obj = new NHHospital_Object;
			$obj->id = $row[0];
			$obj->patient_id = $row[1];
			$obj->startTimestamp = $row[2];
			$obj->endTimestamp = $row[3];
            if($obj->endTimestamp == null && !$result->pregnancyOpened)
            {
                $result->pregnancyOpened = true;
            }
			$result->pregnancy[] = $obj;
		}	
		ibase_free_query($stat_query);
		ibase_free_result($stat_result);
        
        
		ibase_commit($trans);
		$connection->CloseDBConnection();		
		return $result; 
    }
    
    /** 
 	* @param NHHospital_Object $data
	* @return bool
	*/    
    public function saveStationare($data) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if($data->id == null || $data->id == '')
        {
            $data->id = "null";
        }
        if($data->startTimestamp != null && $data->startTimestamp != '')
        {
            $data->startTimestamp = "'".$data->startTimestamp."'";
        }
        else
        {
            $data->startTimestamp = "null";
        }
        if($data->endTimestamp != null && $data->endTimestamp != '')
        {
            $data->endTimestamp = "'".$data->endTimestamp."'";
        }
        else
        {
            $data->endTimestamp = "null";
        }
        
        $save_sql = "update or insert into NH_STATIONARE (
                                        ID, 
                                        FK_PATIENT, 
                                        STARTTIMESTAMP, 
                                        ENDTIMESTAMP)
                    values (
                                        ".$data->id.", 
                                        ".$data->patient_id.", 
                                        ".$data->startTimestamp.",
                                        ".$data->endTimestamp.")
                    matching (ID)";
                                  //'".$psorolitedata->createdate."')";
              
            
		$save_query = ibase_prepare($trans, $save_sql);
		ibase_execute($save_query);
        ibase_free_query($save_query);
         
            
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();
        return $result;
    }
    
    /** 
 	* @param NHHospital_Object $data
	* @return bool
	*/    
    public function savePregnancy($data) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if($data->id == null || $data->id == '')
        {
            $data->id = "null";
        }
        if($data->startTimestamp != null && $data->startTimestamp != '')
        {
            $data->startTimestamp = "'".$data->startTimestamp."'";
        }
        else
        {
            $data->startTimestamp = "null";
        }
        if($data->endTimestamp != null && $data->endTimestamp != '')
        {
            $data->endTimestamp = "'".$data->endTimestamp."'";
        }
        else
        {
            $data->endTimestamp = "null";
        }
        
        $save_sql = "update or insert into NH_PREGNANCY (
                                        ID, 
                                        FK_PATIENT, 
                                        STARTTIMESTAMP, 
                                        ENDTIMESTAMP)
                    values (
                                        ".$data->id.", 
                                        ".$data->patient_id.", 
                                        ".$data->startTimestamp.",
                                        ".$data->endTimestamp.")
                    matching (ID)";
                                  //'".$psorolitedata->createdate."')";
              
            
		$save_query = ibase_prepare($trans, $save_sql);
		ibase_execute($save_query);
        ibase_free_query($save_query);
         
            
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();
        return $result;
    }
        
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deleteStationareByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from NH_STATIONARE where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }    
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deletePregnancyByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from NH_PREGNANCY where ID=$id";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }

    
    
}

?>
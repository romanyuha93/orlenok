<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
class AmbulatoryCardModuleHygcontrol
{  
	/**
    * @var string
    */
    var $PATIENTID;

    /**
    * @var string
    */
    var $CONTROLDATE;

    /**
    * @var string
    */
    var $DOCTORID;

    /**
    * @var string
    */
    var $SHORTNAME;

     /**
    * @var string
    */
    var $ISCONTROL;
 }

class AmbulatoryCardModuleAmbcard
{				
	/**
    * @var string
    */
    var $PATIENTID;  
    
     /**
    * @var string
    */
    var $OCCLUSION;
    
    /**
    * @var string
    */
    var $COMORBIDITIES;
    
    /**
    * @var string
    */
    var $HYGIENE;

     /**
    * @var string
    */
    var $ANALYSIS;
    
     /**
    * @var string
    */
    var $ProtocolANALYSIS;

    /**
    * @var string
    */
    var $VITASCALE;
      
}

class AmbulatoryCardModuleDoctor
{
    /**
    * @var string
    */
    var $ID;
     
    /**
    * @var string
    */
    var $SHORTNAME;
}

class AmbulatoryCardModuleOcclusions
{
    /**
    * @var string
    */
    var $DESCRIPTION;
}

class AmbulatoryCardModuleComorbidities
{
    /**
    * @var string
    */
    var $DESCRIPTION;
}
 
class AmbulatoryCardModuleHygiene
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $HYGDESCRIPT;
}
 

class AmbulatoryCardModule
{		
      /**
     * @param AmbulatoryCardModuleHygcontrol $p1
     * @param AmbulatoryCardModuleAmbcard $p2
     * @param AmbulatoryCardModuleDoctor $p3
     * @param AmbulatoryCardModuleOcclusions $p4
     * @param AmbulatoryCardModuleComorbidities $p5
     * @param AmbulatoryCardModuleHygiene $p6
     * 
     * @return void
     */
    public function registerTypes($p1, $p2, $p3, $p4, $p5, $p6){}
    
    
    /** 
 	* @param int $ID
    * @return AmbulatoryCardModuleAmbcard[]
	*/ 	
     public function getAmbCard($ID) 
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select PATIENTID, OCCLUSION, HYGIENE, ANALYSIS, VITASCALE, COMORBIDITIES from AMBCARD where PATIENTID = '$ID'";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleAmbcard;
          $obj->PATIENTID = $row[0];
          $obj->OCCLUSION = $row[1];
          $obj->HYGIENE = $row[2];
          $obj->ANALYSIS = $row[3];
          $obj->ProtocolANALYSIS = $this->getRentgen($ID, $trans);
          $obj->VITASCALE = $row[4];
          $obj->COMORBIDITIES = $row[5];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    private function getRentgen($patient_fk, $trans)
    {
            //return "qwer";
            $QueryText = "select distinct protocols.RENTGENTXT, diagnos2.id from protocols
                       inner join template on (protocols.id = template.protocol)
                       inner join diagnos2 on (template.id = diagnos2.template)
                       inner join treatmentcourse on (treatmentcourse.id = diagnos2.treatmentcourse)
                    where treatmentcourse.patient_fk = $patient_fk";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            ibase_free_query($query);
            $asr = null;
            while ($row = ibase_fetch_row($result))
            {
                    $diagid = $row[1];
                    $tooth = getTreatmentObjects($diagid,$trans)." - ";
                    $asr = $asr == null ? "" : "$asr; ";
                    
                    $addit = text_blob_encode($row[0]);
                    $asr = $asr . $tooth . $addit;
                
            }
            ibase_free_result($result);
            return $asr;
    }
   
    /** 
 	* @param int $ID
    * @return AmbulatoryCardModuleHygcontrol[]
	*/ 
     public function getHygcontrol($ID) 
     {  
        $connection = new Connection();
		$connection->EstablishDBConnection();	
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select 
                        HYGCONTROL.CONTROLDATE, 
                        HYGCONTROL.DOCTORID, 
                        DOCTORS.SHORTNAME, 
                        HYGCONTROL.ISCONTROL 
                     from HYGCONTROL 
                     INNER JOIN DOCTORS
                     ON HYGCONTROL.DOCTORID = DOCTORS.ID
                     WHERE HYGCONTROL.PATIENTID=$ID order by CONTROLDATE";	
                     
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleHygcontrol;
          $obj->PATIENTID = $ID;
          $obj->CONTROLDATE = $row[0];
          $obj->DOCTORID = $row[1];
          $obj->SHORTNAME = $row[2];
          $obj->ISCONTROL = $row[3];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 

    /**
 	* @param AmbulatoryCardModuleHygcontrol $hygnavchVO
    * @return boolean
 	*/
    public function updateHygNavch0($hygnavchVO) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update HYGCONTROL
					  set CONTROLDATE ='$hygnavchVO->CONTROLDATE',
					      DOCTORID = $hygnavchVO->DOCTORID
					      where (PATIENTID=$hygnavchVO->PATIENTID) and (ISCONTROL='0')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }

    /**
 	* @param int $ID
    * @return string
 	*/
    public function addAmbcard($ID) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into AMBCARD (patientid, occlusion, hygiene, analysis, vitascale, comorbidities) values ($ID, null, null, null, null, null)";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
        $return =  $this->getRentgen($ID, $trans);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $return;
    }
     
    /**
    * @param AmbulatoryCardModuleHygcontrol $hygcontrolVO
    * @return boolean
    */
    public function insHygcontrol($hygcontrolVO) 
    {
          $connection = new Connection();
          $connection->EstablishDBConnection();  
          $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
          $QueryText =           
          "insert into HYGCONTROL (patientid, controldate, doctorid, iscontrol)
          values($hygcontrolVO->PATIENTID, '$hygcontrolVO->CONTROLDATE', $hygcontrolVO->DOCTORID, '$hygcontrolVO->ISCONTROL')";
          $query = ibase_prepare($trans, $QueryText);
          ibase_execute($query);
          $success = ibase_commit($trans);
          ibase_free_query($query); 
          $connection->CloseDBConnection();
          return $success;
    }
    
    /** 
 	*  @return AmbulatoryCardModuleOcclusions[]
	*/ 
     public function getOcclusions() //sqlite
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select DESCRIPTION from OCCLUSIONS";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleOcclusions;
          $obj->DESCRIPTION = $row[0];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /** 
 	*  @return AmbulatoryCardModuleComorbidities[]
	*/ 
     public function getComorbidities()  //sqlite
     { 
		$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select DESCRIPTION from COMORBIDITIES";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleComorbidities;
          $obj->DESCRIPTION = $row[0];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /** 
 	*  @return AmbulatoryCardModuleHygiene[]
	*/ 
     public function getHygieneDesript()  //sqlite
     { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, HYGDESCRIPT from HYGIENE";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleHygiene;
          $obj->ID = $row[0];
          $obj->NUMBER = $row[1];
          $obj->HYGDESCRIPT = $row[2];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /** 
 	* @param string $nowdate
 	* @return AmbulatoryCardModuleDoctor[]
	*/ 
    public function getDoctors($nowdate) //sqlite
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText ="select ID, SHORTNAME from DOCTORS where dateworkend is null or dateworkend>='$nowdate'";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleDoctor;
          $obj->ID = $row[0];
          $obj->SHORTNAME = $row[1];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param int $ID
    * @return boolean
 	*/
    public function addHygcontrol0($ID) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();	
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into Hygcontrol (patientid, controldate, doctorid, iscontrol) values ($ID, null, null, 0)";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);	
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
        
    /** 
 	* @param int $ID
    * @return AmbulatoryCardModuleHygcontrol[]
	*/ 
    public function getHygNavch($ID) 
     { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select PATIENTID, CONTROLDATE, DOCTORID, ISCONTROL from HYGCONTROL where PATIENTID=$ID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new AmbulatoryCardModuleHygcontrol;
            $obj->PATIENTID = $row[0];
            $obj->CONTROLDATE = $row[1];
            $obj->DOCTORID = $row[2];
            $obj->ISCONTROL = $row[3];
            $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 
    
    /**
 	* @param AmbulatoryCardModuleAmbcard $ambcardVO 
    * @return boolean
 	*/
    public function updateAmb($ambcardVO) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update AMBCARD
					 set OCCLUSION='".safequery($ambcardVO->OCCLUSION)."',
                     HYGIENE= '".safequery($ambcardVO->HYGIENE)."',
				     ANALYSIS= '".safequery($ambcardVO->ANALYSIS)."',
                     VITASCALE= '".safequery($ambcardVO->VITASCALE)."',
                     COMORBIDITIES= '".safequery($ambcardVO->COMORBIDITIES)."'
					 where PATIENTID='$ambcardVO->PATIENTID'";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }
    /** 
 	* @param int $ID
    * @return AmbulatoryCardModuleHygcontrol[]
	*/ 
     public function getHygcon1($ID) 
     {  
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select 
                        HYGCONTROL.CONTROLDATE, 
                        HYGCONTROL.DOCTORID, 
                        DOCTORS.SHORTNAME, 
                        HYGCONTROL.ISCONTROL 
                     from HYGCONTROL 
                     INNER JOIN DOCTORS
                     ON HYGCONTROL.DOCTORID = DOCTORS.ID
                     WHERE HYGCONTROL.PATIENTID=$ID 
                     and HYGCONTROL.ISCONTROL = '1'
                     order by CONTROLDATE";	
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleHygcontrol;
          $obj->PATIENTID = $ID;
          $obj->CONTROLDATE = $row[0];
          $obj->DOCTORID = $row[1];
          $obj->SHORTNAME = $row[2];
          $obj->ISCONTROL = $row[3];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 
    
    /** 
 	* @param int $ID
    * @return AmbulatoryCardModuleHygcontrol[]
	*/ 
     public function getHygcon0($ID) 
     {         	 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select HYGCONTROL.CONTROLDATE, HYGCONTROL.DOCTORID, DOCTORS.SHORTNAME, HYGCONTROL.ISCONTROL 
                     from HYGCONTROL 
                     INNER JOIN DOCTORS
                     ON HYGCONTROL.DOCTORID = DOCTORS.ID
                     WHERE HYGCONTROL.PATIENTID=$ID 
                     and HYGCONTROL.ISCONTROL = '0'
                     order by CONTROLDATE";	
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new AmbulatoryCardModuleHygcontrol;
          $obj->PATIENTID = $ID;
          $obj->CONTROLDATE = $row[0];
          $obj->DOCTORID = $row[1];
          $obj->SHORTNAME = $row[2];
          $obj->ISCONTROL = $row[3];
		  $objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    } 
    
    /**
 	* @param AmbulatoryCardModuleHygcontrol $hygcontrolVO
    * @param int $ID
    * @return boolean
 	*/ 		 		
    public function deleteHygcontrol($hygcontrolVO, $ID) 
    {           
        $connection = new Connection(); 	
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText =
		"delete from HYGCONTROL
		 where PATIENTID=$ID
		 and CONTROLDATE = '$hygcontrolVO->CONTROLDATE'";
  		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
     
}
?>
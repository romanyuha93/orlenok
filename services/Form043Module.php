<?php

require_once "F43OBJECT.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

//$Form043=new Form043Module;
//$Form043->createReport('docx','1',true,true,true,true,true,true,true);

class Form043Module
{

 /** 
 * @param string $docType
 * @param string $id_course
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures043Flag
 * @param boolean $includeShifrProcedures043Flag
 * @param boolean $includeCountProcedures043Flag
 * @return string
 */  
 public function create043Course($docType, $id_course, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag)
 {
        
        $connection = new Connection();
        $connection->EstablishDBConnection();
        
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $QueryText = "select ID from toothexam where TREATMENTCOURSE_fk = $id_course";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        
        if ($row = ibase_fetch_row($result))
        {
            $id_patient= $row[0];
        }
        ibase_free_query($query);
        ibase_free_result($result);
        
        $result =  $this->generate043CourseTBS($docType, $id_patient, $id_course, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $trans);
         
         
        ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $result;
 }
 
  /** 
 * @param string $docType
 * @param int $id_patient
 * @param string $start_date
 * @param string $end_date
 * @param string $doctor_shortname 
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures043Flag
 * @param boolean $includeShifrProcedures043Flag
 * @param boolean $includeCountProcedures043Flag
 * @return string
 */  
 public function create043Diary($docType, $id_patient, $start_date, $end_date, $doctor_shortname, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag)
 {
    return $this->generate043DiaryTBS($docType, $id_patient, $start_date, $end_date, $doctor_shortname, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag);   
 }
 


 /** 
 * @param string $docType
 * @param string $id_patient
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures043Flag
 * @param boolean $includeShifrProcedures043Flag
 * @param boolean $includeCountProcedures043Flag
 * @return string
 */ 
  private function generate043CourseTBS($docType,$id_patient, $id_course, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $trans)
    {   
        $F43=new F43OBJECT($id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, "TOOTHEXAM.AFTERFLAG = 0 and TOOTHEXAM.TREATMENTCOURSE_fk=".$id_course, $trans);           
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form043Course.'.$docType, OPENTBS_ALREADY_UTF8);
        $TBS->NoErr=true;
        $TBS->MergeBlock('TOOTHEXAM,Plan',$F43->TOOTHEXAM_ARRAY);
        $TBS->MergeBlock('Diary',$F43->DIARY);
        $GLOBALS['header1']=tflocale::$medcard.' №'.$F43->PATIENT_DATA->CARDNUMBER.' '.tflocale::$from.' '.$F43->PATIENT_DATA->CARDDATE.' '.$F43->PATIENT_DATA->SHORTNAME;
        $file_name = translit(str_replace(' ','_',$F43->PATIENT_DATA->LASTNAME.'_'.$F43->PATIENT_DATA->FIRSTNAME.'_'.$F43->PATIENT_DATA->MIDDLENAME.'_'.$F43->PATIENT_DATA->CARDNUMBER.' '.$id_course));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;    
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        return $form_path; 
    }    
    
 /** 
 * @param string $docType
 * @param string $id_patient
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures043Flag
 * @param boolean $includeShifrProcedures043Flag
 * @param boolean $includeCountProcedures043Flag
 * @param boolean $is043_1
 * @return string
 */  
 public function createReport($docType, $id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $is043_1)
    {
        return $this->generateTBS($docType,$id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag,$is043_1);
        
    }
 
 /** 
 * @param string $docType
 * @param string $id_patient
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures043Flag
 * @param boolean $includeShifrProcedures043Flag
 * @param boolean $includeCountProcedures043Flag
 * @param boolean $is043_1
 * @return string
 */ 
  private function generateTBS($docType,$id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag,$is043_1)
    {   
        $F43=new F43OBJECT($id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag);     
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        if($is043_1&&file_exists(dirname(__FILE__).'/tbs/res/form043_1.'.$docType))
        {
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form043_1.'.$docType, OPENTBS_ALREADY_UTF8);  
        }
        else
        {
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form043.'.$docType, OPENTBS_ALREADY_UTF8);  
        }

        $TBS->NoErr=true;
        $TBS->MergeBlock('TOOTHEXAM,Plan',$F43->TOOTHEXAM_ARRAY);
        $TBS->MergeBlock('Diary',$F43->DIARY);
        $TBS->MergeField('Summary',$F43);
        $TBS->MergeField('CLINIC_DATA',$F43->CLINIC_DATA);
        $TBS->MergeField('PATIENT_DATA',$F43->PATIENT_DATA);
        $TBS->MergeField('PATIENT_AMB',$F43->PATIENT_AMB);
        $GLOBALS['header1']=tflocale::$medcard.' №'.$F43->PATIENT_DATA->CARDNUMBER.' '.tflocale::$from.' '.$F43->PATIENT_DATA->CARDDATE.' '.$F43->PATIENT_DATA->SHORTNAME;
        $file_name = translit(str_replace(' ','_',$F43->PATIENT_DATA->LASTNAME.'_'.$F43->PATIENT_DATA->FIRSTNAME.'_'.$F43->PATIENT_DATA->MIDDLENAME.'_'.$F43->PATIENT_DATA->CARDNUMBER));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;    
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        return $form_path; 
    }
   
  
  
    /** 
 * @param string $docType
 * @param int $id_patient
 * @param string $start_date
 * @param string $end_date
 * @param string $doctor_shortname
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures043Flag
 * @param boolean $includeShifrProcedures043Flag
 * @param boolean $includeCountProcedures043Flag
 * @return string
 */ 
  private function generate043DiaryTBS($docType, $id_patient, $start_date, $end_date, $doctor_shortname, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag)
    {   
        $DIARY=F43OBJECT::getDoctorDiary(null, $id_patient, $start_date, $end_date, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag);
        $PATIENT_DATA=new PrintDataModule_PatientData($id_patient);
        
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form043Diary.'.$docType, OPENTBS_ALREADY_UTF8);
        $TBS->NoErr=true;
        $TBS->MergeBlock('Diary',$DIARY);
        if(nonull($doctor_shortname))
        {
            $GLOBALS['footer1']=tflocale::$doctor."_________ ".$doctor_shortname."        ".tflocale::$cheif."__________ ";
        }
        else
        {
             $GLOBALS['footer1']=tflocale::$doctor."_________________________________________ ".tflocale::$cheif;
        }
       
        $GLOBALS['header1']=tflocale::$medcard.' №'.$PATIENT_DATA->CARDNUMBER.' '.tflocale::$from.' '.$PATIENT_DATA->CARDDATE.' '.$PATIENT_DATA->SHORTNAME;
//        file_put_contents("C:/tmp.txt",$GLOBALS['header1']);
        $file_name = translit(str_replace(' ','_',$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME.'_'.$PATIENT_DATA->CARDNUMBER));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;    
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        return $form_path; 
    } 
    
    /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteForm($form_path)
    {
        return deleteFileUtils($form_path);
    }
}

?>

<?php
//define('tfClientId','online');
    
require_once "Connection.php";
require_once "Utils.php";


class NH_PsoroliteData
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $patient_id;
    /**
    * @var int
    */
    var $doze;
    /**
    * @var int
    */
    var $duration;
    /**
    * @var string
    */
    var $createdate;
    /**
    * @var string
    */
    var $stopdate;
}

class NHPsorolite
{
       
    /**
     * @param NH_PsoroliteData $p1
     */
    public function registertypes($p1)
    {}
    
    /** 
 	* @param string $patientId
 	* @param string $startDate
 	* @param string $endDate
	* @return NH_PsoroliteData[]
	*/    
    public function getDataByPatientId($patientId, $startDate, $endDate) 
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$data_sql = "select 
                            ID, 
                            PATIENT_FK, 
                            DOZE, 
                            DURATION, 
                            CREATEDATE,
                            STOPDATE
                        from NH_PSOROLITE 
                        where PATIENT_FK = $patientId ";
                        
        if($startDate != -1 || $endDate != -1)
        {
            $data_sql .= "and CREATEDATE  >= '$startDate 00:00:00'
                                and CREATEDATE <= '$endDate 23:59:59' ";
        }
                                
        $data_sql .= "order by CREATEDATE desc";
                                			
		$data_query = ibase_prepare($trans, $data_sql);
		$data_result = ibase_execute($data_query);
		$rows = array();		
		while ($row = ibase_fetch_row($data_result))
		{
			$obj = new NH_PsoroliteData;
            
			$obj->id = $row[0];
			$obj->patient_id = $row[1];
			$obj->doze = $row[2];
			$obj->duration = $row[3];
			$obj->createdate = $row[4];
			$obj->stopdate = $row[5];
			$rows[] = $obj;
		}	
		ibase_free_query($data_query);
		ibase_free_result($data_result);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /** 
 	* @param NH_PsoroliteData $psorolitedata
	* @return string
	*/    
    public function addDataByPatientId($psorolitedata) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $add_sql = "insert into NH_PSOROLITE ( ID, 
                                            PATIENT_FK, 
                                            DOZE, 
                                            DURATION, 
                                            CREATEDATE)
                          values ( null, 
                                  ".$psorolitedata->patient_id.", 
                                  ".$psorolitedata->doze.", 
                                  ".$psorolitedata->duration.", 
                                  null)
                          returning id";
                                  //'".$psorolitedata->createdate."')";
              
            
		$add_query = ibase_prepare($trans, $add_sql);
		$add_result = ibase_execute($add_query);
        $row = ibase_fetch_row($add_result);
        ibase_free_query($add_query);
        ibase_free_result($add_result);
         
            
		ibase_commit($trans);
		$connection->CloseDBConnection();
        return $row[0];
    }
    
     
    /** 
 	* @param string $recordId
	* @return boolean
	*/    
    public function stopCallByRecordId($recordId) 
    {
        if(is_numeric($recordId))
        {
            $connection = new Connection();
    		$connection->EstablishDBConnection();		
    		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $upd_sql = "update NH_PSOROLITE
                            set STOPDATE = 'now'
                        where ID = $recordId";
                  
                
    		$upd_query = ibase_prepare($trans, $upd_sql);
    		ibase_execute($upd_query);
                
    		$result = ibase_commit($trans);
    		$connection->CloseDBConnection(); 
            return $result;
        }
        return false;
    }
    
}

?>
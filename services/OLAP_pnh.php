<?php

  	require_once "Connection.php";
	require_once "Utils.php";
	define('tfClientId','online');
	ini_set("max_execution_time", "90000");
	ini_set('memory_limit', '-1');
	ini_set('realpath_cache_ttl', '90000');
  // Errors
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
  //

//$locale = 'uk_UA';//uk_UA, en_US, ke_GE, pl_PL, ru_RU

$locale_1 = array('uk_UA' => 'Назад ', 
                  'en_US' => 'back ', 
                  'ka_GE' => 'უკან ',
                  'pl_PL' => 'do tyłu ', 
                  'ru_RU' => 'Назад ');

$locale_2 = array('uk_UA' => 'по клініці ', 
                  'en_US' => 'clinic ', 
                  'ka_GE' => 'კლინიკა ',
                  'pl_PL' => 'klinika ', 
                  'ru_RU' => 'по клинике ');

$locale_3 = array('uk_UA' => 'Доктор ', 
                  'en_US' => 'doctor ', 
                  'ka_GE' => 'ექიმი ',
                  'pl_PL' => 'doktorze ', 
                  'ru_RU' => 'доктор ');

$locale_4 = array('uk_UA' => 'Вибірка прийнятих пацієнтів за період з ', 
                  'en_US' => 'The sample of accepted patients for the period from ', 
                  'ka_GE' => 'მიღებული პაციენტების ნიმუში პერიოდის განმავლობაში ',
                  'pl_PL' => 'Próbka zaakceptowanych pacjentów w okresie od ', 
                  'ru_RU' => 'Выборка принятых пациентов за период c ');

$locale_5 = array('uk_UA' => 'по', 
                  'en_US' => 'to', 
                  'ka_GE' => 'оn',
                  'pl_PL' => 'na', 
                  'ru_RU' => 'по');

$locale_6 = array('uk_UA' => 'ПІБ пацієнта', 
                  'en_US' => 'Name of patient', 
                  'ka_GE' => 'პაციენტის სახელი',
                  'pl_PL' => 'Imię pacjenta', 
                  'ru_RU' => 'ФИО пациента');

$locale_7 = array('uk_UA' => 'Кількість візитів', 
                  'en_US' => 'Number of visits', 
                  'ka_GE' => 'ვიზიტების რაოდენობა',
                  'pl_PL' => 'Liczba wizyt', 
                  'ru_RU' => 'Кол-во визитов');

$locale_8 = array('uk_UA' => 'Номери рахунків', 
                  'en_US' => 'Account numbers', 
                  'ka_GE' => 'ანგარიშის ნომრები',
                  'pl_PL' => 'Numery kont', 
                  'ru_RU' => 'Номера счетов');

$locale_9 = array('uk_UA' => 'Дати візитів', 
                  'en_US' => 'Dates of visits', 
                  'ka_GE' => 'ვიზიტების თარიღები',
                  'pl_PL' => 'Daty wizyt', 
                  'ru_RU' => 'Даты визитов');

$locale_10 = array('uk_UA' => 'Сума рахунків', 
                  'en_US' => 'Amount of accounts', 
                  'ka_GE' => 'ანგარიშების რაოდენობა',
                  'pl_PL' => 'Kwota kont', 
                  'ru_RU' => 'Сумма счетов');

$locale_11 = array('uk_UA' => 'по всіх майданчиках стоматології ', 
                  'en_US' => 'for all sections of dentistry ', 
                  'ka_GE' => 'ყველა სექციაში სტომატოლოგია ',
                  'pl_PL' => 'dla wszystkich sekcji stomatologii ', 
                  'ru_RU' => 'по всем разделам стоматологии ');

$locale_12 = array('uk_UA' => 'по розділу ', 
                  'en_US' => 'on section ', 
                  'ka_GE' => 'სექციის მიხედვით ',
                  'pl_PL' => 'na odcinku ', 
                  'ru_RU' => 'по разделу ');

$locale_13 = array('uk_UA' => 'Вибірка виконаних процедур за період c', 
                  'en_US' => 'The sample of the performed procedures for the period c', 
                  'ka_GE' => 'პერიოდის შესრულებული პროცედურების ნიმუში c',
                  'pl_PL' => 'Próbka wykonanych procedur w okresie c', 
                  'ru_RU' => 'Выборка выполненных процедур за период c');

$locale_14 = array('uk_UA' => 'Назва процедури', 
                  'en_US' => 'Name of procedure', 
                  'ka_GE' => 'პროცედურის დასახელება',
                  'pl_PL' => 'Nazwa procedury', 
                  'ru_RU' => 'Название процедуры');

$locale_15 = array('uk_UA' => 'Кількість процедур', 
                  'en_US' => 'Number of procedures', 
                  'ka_GE' => 'პროცედურების რაოდენობა',
                  'pl_PL' => 'Liczba procedur', 
                  'ru_RU' => 'Кол-во процедур');

$locale_16 = array('uk_UA' => 'Сума УОП', 
                  'en_US' => 'Amount Conditional Unit of Labor', 
                  'ka_GE' => 'შრომის პირობითი ერთეული',
                  'pl_PL' => 'Kwota WJP', 
                  'ru_RU' => 'Сумма УЕТ');

$locale_17 = array('uk_UA' => 'Сума за процедури по прайсу', 
                  'en_US' => 'The amount for the procedures for the price', 
                  'ka_GE' => 'ფასი პროცედურების ფასი',
                  'pl_PL' => 'Kwota za procedury dotyczące ceny', 
                  'ru_RU' => 'Сумма за процедуры по прайсу');

$locale_18 = array('uk_UA' => 'Сума за процедури по рахунку', 
                  'en_US' => 'Invoice amount for invoice procedures', 
                  'ka_GE' => 'ინვოისის პროცედურების ინვოისი თანხა',
                  'pl_PL' => 'Kwota faktury dla procedur fakturowania', 
                  'ru_RU' => 'Сумма за процедуры по счету');

$locale_19 = array('uk_UA' => 'Розділ стоматології', 
                  'en_US' => 'Section of stomatology', 
                  'ka_GE' => 'სტომატოლოგიის სექცია',
                  'pl_PL' => 'Sekcja stomatologii', 
                  'ru_RU' => 'Раздел стоматологии');

$locale_20 = array('uk_UA' => 'Лікар', 
                  'en_US' => 'Doctor', 
                  'ka_GE' => 'ექიმი',
                  'pl_PL' => 'Doktorze', 
                  'ru_RU' => 'Врач');

$locale_21 = array('uk_UA' => 'Всього пацієнтів', 
                  'en_US' => 'Total number of patients', 
                  'ka_GE' => 'პაციენტების საერთო რაოდენობა',
                  'pl_PL' => 'Łączna liczba pacjentów', 
                  'ru_RU' => 'Всего пациенттов');

$locale_22 = array('uk_UA' => 'Візитів', 
                  'en_US' => 'Visits', 
                  'ka_GE' => 'ვიზიტები',
                  'pl_PL' => 'Odwiedziny', 
                  'ru_RU' => 'Визитов');

$locale_23 = array('uk_UA' => 'Всього рахунків на суму', 
                  'en_US' => 'Total accounts for the amount of', 
                  'ka_GE' => 'მთლიანი თანხის ოდენობა',
                  'pl_PL' => 'Łączne kwoty na kwotę', 
                  'ru_RU' => 'Всего счетов на сумму');

$locale_24 = array('uk_UA' => 'Всього процедур', 
                  'en_US' => 'Total procedures', 
                  'ka_GE' => 'სულ პროცედურები',
                  'pl_PL' => 'Całkowite procedury', 
                  'ru_RU' => 'Всего процедур');

$locale_25 = array('uk_UA' => 'Сума по прайсу', 
                  'en_US' => 'Amount on the price list', 
                  'ka_GE' => 'თანხა სიაში',
                  'pl_PL' => 'Kwota na liście cen', 
                  'ru_RU' => 'Сумма по прайсу');

$locale_26 = array('uk_UA' => 'Сума за рахунками', 
                  'en_US' => 'Invoiced Amount', 
                  'ka_GE' => 'შემოთავაზებული თანხა',
                  'pl_PL' => 'Kwota fakturowana', 
                  'ru_RU' => 'Сумма по счетам');

$locale_27 = array('uk_UA' => 'УСI ДОКТОРА', 
                  'en_US' => 'ALL DOCTORS', 
                  'ka_GE' => 'ყველა დოქტორი',
                  'pl_PL' => 'WSZYSTKIE LEKARZY', 
                  'ru_RU' => 'ВСЕ ДОКТОРА');

$locale_28 = array('uk_UA' => 'УСI РОЗДІЛИ', 
                  'en_US' => 'ALL SECTIONS', 
                  'ka_GE' => 'ყველა სექცია',
                  'pl_PL' => 'WSZYSTKIE sekcje', 
                  'ru_RU' => 'ВСЕ РАЗДЕЛЫ');

$locale_29 = array('uk_UA' => 'Запустити вибірку за період <b> прийнятих пацієнтів', 
                  'en_US' => 'Start sampling for the period of <b> accepted patients', 
                  'ka_GE' => 'დაიწყეთ შერჩევა <b> მიღებული პაციენტების პერიოდში',
                  'pl_PL' => 'Rozpocznij pobieranie próbek na okres <b> zaakceptowanych pacjentów', 
                  'ru_RU' => 'Запустить выборку за период <b>принятых пациентов');

$locale_30 = array('uk_UA' => 'або', 
                  'en_US' => 'or', 
                  'ka_GE' => 'ან',
                  'pl_PL' => 'lub', 
                  'ru_RU' => 'или');

$locale_31 = array('uk_UA' => 'виконаних процедур', 
                  'en_US' => 'completed procedures', 
                  'ka_GE' => 'დასრულებული პროცედურები',
                  'pl_PL' => 'zakończone procedury', 
                  'ru_RU' => 'выполненных процедур');

$locale_32 = array('uk_UA' => 'c фільтром по доктору (для пацієнтів) або розділу стоматології (для процедур)', 
                  'en_US' => 'With a filter for the doctor (for patients) or a section of dentistry (for procedures)', 
                  'ka_GE' => 'ფილტრის ექიმთან ერთად (პაციენტებისთვის) ან სტომატოლოგიური განყოფილება (პროცედურებისათვის)',
                  'pl_PL' => 'z filtrem dla lekarza (dla pacjentów) lub z sekcją stomatologiczną (dla procedur)', 
                  'ru_RU' => 'c фильтром по доктору (для пациентов) или разделу стоматологии (для процедур)');

$locale_33 = array('uk_UA' => 'Виберіть доктора', 
                  'en_US' => 'Choose a doctor', 
                  'ka_GE' => 'აირჩიეთ ექიმი',
                  'pl_PL' => 'Wybierz lekarza', 
                  'ru_RU' => 'Выберите доктора');

$locale_34 = array('uk_UA' => 'Виберіть розділ стоматології', 
                  'en_US' => 'Select a section of dentistry', 
                  'ka_GE' => 'აირჩიეთ სტომატოლოგიის სექცია',
                  'pl_PL' => 'Wybierz sekcję dentysty', 
                  'ru_RU' => 'Выберите раздел стоматологии');

$locale_35 = array('uk_UA' => 'Значення', 
                  'en_US' => 'Value', 
                  'ka_GE' => 'მნიშვნელობა',
                  'pl_PL' => 'Znaczenie', 
                  'ru_RU' => 'Значение');

$locale_36 = array('uk_UA' => 'Оберіть період відбору:', 
                   'en_US' => 'Select the selection period:', 
                   'ka_GE' => 'აირჩიეთ შერჩევის პერიოდი:',
                   'pl_PL' => 'Wybierz okres wyboru:', 
                   'ru_RU' => 'Выберите период отбора:');

$locale_37 = array('uk_UA' => 'з', 
                   'en_US' => 'from', 
                   'ka_GE' => 'ერთად',
                   'pl_PL' => 'z', 
                   'ru_RU' => 'c');

$locale_38 = array('uk_UA' => 'Показати', 
                   'en_US' => 'Show', 
                   'ka_GE' => 'ჩვენება',
                   'pl_PL' => 'Pokaż', 
                   'ru_RU' => 'Показать');
                   
$locale_39 = array('uk_UA' => 'згрупувати по лікарях', 
                   'en_US' => 'group by doctors', 
                   'ka_GE' => 'ექიმების ჯგუფი',
                   'pl_PL' => 'grupa lekarzy', 
                   'ru_RU' => 'сгруппировать по врачам');

 if($_GET['locale'])
        {
            $locale = $_GET['locale'];
        }
        
$concatDocs = false;

if($_GET['CLINETID'])
{
    
	$connection = new Connection();
	$connection->EstablishDBConnection("main",false);   
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    
    $client_id = $_GET['CLINETID'];
    $checked = false;
    if($client_id == $locale_1[$locale])
    {
       $checked = true; 
    }
    else
    {
               
       $sql_key = "select first 1 ACCOUNTKEY from ACCOUNT
                where ACCOUNTKEY = '$client_id'";
                                			
		$query_key = ibase_prepare($trans, $sql_key);
		$result_key = ibase_execute($query_key);
        	
		if($r_key = ibase_fetch_row($result_key))
		{
		  if($r_key[0] != null) $checked = true;
		}	
		ibase_free_query($query_key);
		ibase_free_result($result_key);     
    }


        if($checked)
        {

            echo("<head>
            <meta charset='UTF-8'/>
            </head><body>");
            
            
            
            
            //GET HEALTH TYPES
            $sql_getdic = "select ITEM_DATA, ITEM_LABEL from TF_DICTIONARIES where DICTIONARY = 'HEALTHTYPE' and ITEM_DATA != -1 order by ITEM_DATA";
            $query_getdic = ibase_prepare($trans, $sql_getdic);
            $result_getdic = ibase_execute($query_getdic);	
            
            $Healthtype = array();
    		while ($row = ibase_fetch_row($result_getdic))
            {
                $Healthtype[] = $row[1];	
            }
            ibase_free_query($query_getdic);
            ibase_free_result($result_getdic);
            //GET HEALTH TYPES
            
            //OLD $Healthtype = array('терапия', 'хирургия', 'ортопедия', 'ортодонтия', 'детская', 'эстетическая', 'эндодонтия', 'пародонтология', 'гигиена', 'рентгенология');
            
            if(!empty($_POST['radio']) and !empty($_POST['between']) and !empty($_POST['and']))
            
           	{ 
            	echo '<form action="OLAP_pnh.php?" method="get"><input type="submit" name="CLIN" value="'.$locale_1[$locale].'"><input type="hidden" name="locale" value="'.$_GET['locale'].'"><input type="hidden" name="CLINETID" value="'.$_GET['CLINETID'].'"></form>';  // Два поля hidden для возврата GET
                
                
              	$between = str_replace('-','.',$_POST ['between']);
              	$and = str_replace('-','.',$_POST ['and']);
            
            	if(($_POST['radio'])=='patient'){
            	
            		if(($_POST['doctor'])=='all'){
            			$where = '';
            			$doctor = $locale_2[$locale];	
            				}
            		else {
            		  $where = 'a.DOCTOR = '.$_POST['doctor'].' and';
            		  $selected = $_POST['doctor'];
            		  $setdoc = ibase_fetch_row(ibase_query($connection->connection,"SELECT LASTNAME FROM DOCTORS where id = $selected"));
              		  foreach($setdoc as $value){
                    	         $doctor = $locale_3[$locale].$value;
                    	         }
            		 }
            		
               	$QueryText="select d.FULLNAME, count(b.DATECLOSE), 
            	CAST(SUBSTRING((list(b.DATECLOSE)) FROM 1 FOR 3200) AS VARCHAR(3200)) AS DATECLOSE, 
            	CAST(SUBSTRING((list(c.INVOICENUMBER)) FROM 1 FOR 3200) AS VARCHAR(3200)) AS INVOICEID, 
            	sum(c.SUMM) from 
            	    (SELECT  a.DATECLOSE , a.PATIENT_FK, a.INVOICEID, CAST(SUBSTRING((list(a.DOCTOR)) FROM 1 FOR 3200) AS VARCHAR(3200)) AS DOCTOR
            	    FROM 
             	       (SELECT  INVOICEID, cast (DATECLOSE as DATE) as DATECLOSE, PATIENT_FK, DOCTOR
            	        FROM TREATMENTPROC where DATECLOSE is not NULL) as a
            	    where $where a.DATECLOSE between '$between' and '$and'
            	    group by a.DATECLOSE, a.PATIENT_FK, a.INVOICEID) as b
            	left join INVOICES as c on b.INVOICEID = c.id
            	left join PATIENTS as d on b.PATIENT_FK = d.id
            	group by d.FULLNAME";
                
               	
                

            	echo("<b>".$locale_4[$locale]."$between ".$locale_5[$locale]."$and ($doctor) </b><br/><br/>
            	<table border='1' cellpadding='5'><tr>
            	<td style='font-weight:bold'>".$locale_6[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_7[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_8[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_9[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_10[$locale]."</td>
            	</tr>");
            
            				}
            	if(($_POST['radio'])=='proc'){
                  
                  
                    if(isset($_POST['concatDoc']))
                    {
                        $concatDocs = true;
                    }
                  
            		if(($_POST['Healthtype'])=='all'){
            			$where = '';
            			$type = $locale_11[$locale];	
            				}
            		else {
            		  $where = 'a.HEALTHTYPE = '.$_POST['Healthtype'].' and';
                   	          $type = $locale_12[$locale].$Healthtype[($_POST['Healthtype'])];
            		 }
                     
                     if(($_POST['doctor'])=='all'){
            			$whereDoc = '';
            			$doctor = $locale_2[$locale];	
        				    }
            		else {
            		  $whereDoc = 'b.DOCTOR = '.$_POST['doctor'].' and';
            		  $selected = $_POST['doctor'];
            		  $setdoc = ibase_fetch_row(ibase_query($connection->connection,"SELECT LASTNAME FROM DOCTORS where id = $selected"));
              		  foreach($setdoc as $value){
                    	         $doctor = $locale_3[$locale].$value;
                    	         }
            		 }
         
              $QueryText="select
                                    a.name,
                                    sum(a.PROC_COUNT),
                                    sum(a.PRICE) as PRICE,
                                    sum(a.PRICE_UOP) as PRICE_UOP,
                                    sum(a.INVOICE) as INVOICE,
                                    cast(list(DISTINCT(a.HEALTHTYPE), '|') as VARCHAR(20)), ";
                    if($concatDocs)
                    {
                        $QueryText .= "min(a.DOCTORNAME) ";
                    }
                    else
                    {
                        $QueryText .= "cast(list(DISTINCT(a.DOCTORNAME), ', ') as VARCHAR(5000)) ";
                    }
                                    
                                
                    $QueryText .= "from
                                
                                    (SELECT
                                            b.NAME,
                                            b.id,
                                            (b.PROC_COUNT * b.PRICE) as PRICE, 
                                            (b.PROC_COUNT * b.UOP) as PRICE_UOP,
                                            b.HEALTHTYPE,
                                            cast (b.DATECLOSE as DATE) as DATECLOSE,
                                            (b.PROC_COUNT * b.PRICE * (100 - i.DISCOUNT_AMOUNT)/100) as INVOICE,
                                            i.DISCOUNT_AMOUNT,
                                            b.PROC_COUNT,
                                            (select doctors.shortname from doctors where doctors.id = b.doctor) as DOCTORNAME
                                                
                                    FROM TREATMENTPROC as b
                                    left join HEALTHPROC as c on b.HEALTHPROC_FK = c.id
                                    left join INVOICES as i on b.INVOICEID = i.id
                                    where $whereDoc b.DATECLOSE is not NULL) as a
                                    
            	where $where a.DATECLOSE between '$between' and '$and'
            	group by a.name";
                if($concatDocs)
                {
                    $QueryText .= ", a.DOCTORNAME";
                }
            
            
            
                
            	$header_tb_echo = "<b>".$locale_13[$locale]." $between ".$locale_5[$locale]." $and ($type) </b><br/><br/>
            	<table border='1' cellpadding='5'><tr>
            	<td style='font-weight:bold'>".$locale_14[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_15[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_16[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_17[$locale]."</td>
            	<td style='font-weight:bold'>".$locale_18[$locale]."</td>";
                if(($_POST['radio'])!='patient')
                {
                    $header_tb_echo .= "<td style='font-weight:bold'>".$locale_19[$locale]."</td>
                                    	<td style='font-weight:bold'>".$locale_20[$locale]."</td>
                                    	</tr>";
                }
                
                echo($header_tb_echo);
            
            				}
            		$query = ibase_prepare($trans, $QueryText);
            		$result = ibase_execute($query);
            
            		$summ=0;
                    $summ_uop=0;
            		$summ_pr=0;
            		$summ_inv=0;
            		$visit=0;
            		$total =0;
            		
            
                while ($obj = ibase_fetch_row($result)) 
                {
                    	if(($_POST['radio'])=='patient')
                        {
                            //echo("<tr><td>$obj[0]</td><td>$obj[1]</td><td>$obj[3]</td><td>$obj[2]</td>
                            //  <td>$obj[4]</td><td>$obj[5]</td><td>$obj[6]</td></tr>");
                            echo("<tr><td>$obj[0]</td><td>$obj[1]</td><td>$obj[3]</td><td>$obj[2]</td>
                              <td>$obj[4]</td></tr>");
                   		}
                    	if(($_POST['radio'])=='proc')
                        {
                            
                            $htString = $obj[5];
                            $htArray = explode('|', $htString);
                            $htResultString = '';
                            for($i = 0; $i < count($htArray); ++$i) 
                            {
                                $htResultString .= $Healthtype[($htArray[$i])];
                                if( count($htArray) > 1 && $i != (count($htArray)-1) )
                                {
                                    $htResultString .= ', ';
                                }
                            }
                            echo("<tr><td>$obj[0]</td><td>$obj[1]</td><td>$obj[3]</td><td>$obj[2]</td>
                              <td>$obj[4]</td><td>".$htResultString."</td><td>$obj[6]</td></tr>");
                            
                  		}
            			{
                			$visit=$visit+(float)$obj[1];
                			$total=$total+1;
                			$summ=$summ+(float)$obj[4];
                            $summ_uop+=(float)$obj[3];
                			$summ_pr=$summ_pr+(float)$obj[2];
                			$summ_inv=$summ_inv+(float)$obj[4];
            			}
              	}
            	
            	echo ("</table><br/><br/>");
            	if(($_POST['radio'])=='patient'){
            	echo ($locale_21[$locale].": <b>$total</b>, ".$locale_22[$locale].": <b style=color:red;>$visit</b>; ".$locale_23[$locale].": <b style=color:green;>$summ</b>");
            			}
            	if(($_POST['radio'])=='proc'){
            	echo ($locale_24[$locale].": <b>$visit</b>; ".$locale_16[$locale].": <b style=color:red;>$summ_uop</b>; ".$locale_25[$locale].": <b style=color:red;>$summ_pr</b>; ".$locale_26[$locale].": <b style=color:green;>$summ_inv</b>");
            			}
            		ibase_free_result($result);
            
            }
            else
            {
             
                
               $QueryText='SELECT ID, SHORTNAME FROM DOCTORS order by SHORTNAME';
            
               $query = ibase_prepare($trans, $QueryText);
            
            	            
            	$options = '<option value="all">'.$locale_27[$locale].'</option>';
            	$Healthtypes = '<option value="all">'.$locale_28[$locale].'</option>';
            	$result = ibase_execute($query);
            
            	
            		while ($obj = ibase_fetch_row($result))
                    		{				
                              	$options.= '<option value="'.$obj[0].'">'.$obj[1].'</option>';
                                    
                           		}
                             		
                   			ibase_free_result($result);
            
                foreach($Healthtype as $key => $value)
              {
                 $Healthtypes.= '<option value="'.$key.'">'.$value.'</option>';
            
              } 
                //print_r($Healthtype);
                
                
                echo '<script src="calendar_ru.js" type="text/javascript"></script>
                <form method="post" action="">
                '.$locale_29[$locale].'</b> <input type="radio" name="radio" value="patient" /> '.$locale_30[$locale].' <b>'.$locale_31[$locale].'</b> <input type="radio" name="radio" value="proc" checked /> <br/><br/>'.$locale_32[$locale].'<br/><br/>
                
                <b>'.$locale_33[$locale].':</b><select name="doctor" value="'.$locale_35[$locale].'">'.$options.'</select>
                
                <b>'.$locale_34[$locale].':</b><select name="Healthtype" value="'.$locale_35[$locale].'">'.$Healthtypes.'</select>
                
                
                
                
                <p><b>'.$locale_36[$locale].':</b><br>
                '.$locale_37[$locale].' <input type="text" name="between" value="" onfocus="this.select();lcs(this)"
                    onclick="event.cancelBubble=true;this.select();lcs(this)">
                
                '.$locale_5[$locale].' <input type="text" name="and" value="" onfocus="this.select();lcs(this)"
                    onclick="event.cancelBubble=true;this.select();lcs(this)">
                
                <b><input type="checkbox" name="concatDoc" value="concatDoc"> '.$locale_39[$locale].'</b><br><br/>
                
                <input type="submit" value="'.$locale_38[$locale].'">
                </p>
                </form>';
                
                }
        
        echo("</body>");
        
        
        
        
    }
    
    $connection->CloseDBConnection();
      
}
?>

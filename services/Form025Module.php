<?php

require_once "F25OBJECT.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

//$Form025=new Form025Module;
//$Form025->createReport('odt','5',true,true,true,true,true,true,true);

class Form025Module
{
 /** 
 * @param string $docType
 * @param string $id_patient
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures025Flag
 * @param boolean $includeShifrProcedures025Flag
 * @param boolean $includeCountProcedures025Flag
 * @return string
 */  
 public function createReport($docType, $id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag)
    {
        if($docType == 'docx')
        {
            return $this->generateTBS($docType,$id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag);
        }
		if($docType == 'odt')
        {
            return $this->generateTBS($docType,$id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag);
        }
    }
 
 /** 
 * @param string $docType
 * @param string $id_patient
 * @param boolean $printTestPage
 * @param boolean $includePriceInReport
 * @param boolean $includeMaterialsInReport
 * @param boolean $includeDoctorSignatureInReport
 * @param boolean $includeNoncompleteProcedures0
 
 Flag
 * @param boolean $includeShifrProcedures025Flag
 * @param boolean $includeCountProcedures025Flag
 * @return string
 */ 
private function generateTBS($docType,$id_patient,  $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag)
    {
        $F25=new F25OBJECT($id_patient, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures025Flag, $includeShifrProcedures025Flag, $includeCountProcedures025Flag);     
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        if(file_exists(dirname(__FILE__).'/tbs/res/form025_0.'.$docType))
        {
                 $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/form025_0.'.$docType, OPENTBS_ALREADY_UTF8);  
        }
        $TBS->NoErr=true;
		$TBS->MergeField("DATE_CURRENT", $F25->DATE_CURRENT);

        $TBS->MergeBlock('DIARY',$F25->DAIRY);
		$TBS->MergeBlock('DIAGNOSIS',$F25->DIAGNOSIS);

        $TBS->MergeField('CLINIC_DATA',$F25->CLINIC_DATA);
        $TBS->MergeField('PATIENT_DATA',$F25->PATIENT_DATA);
		$TBS->MergeField('TEST',$F25->TEST);


		$GLOBALS['header1']=tflocale::$medcard.' №'.$F25->PATIENT_DATA->CARDNUMBER.' '.tflocale::$from.' '.$F25->PATIENT_DATA->CARDDATE.' '.$F25->PATIENT_DATA->SHORTNAME;
		$file_name = translit(str_replace(' ','_',$F25->PATIENT_DATA->LASTNAME.'_'.$F25->PATIENT_DATA->FIRSTNAME.'_'.$F25->PATIENT_DATA->MIDDLENAME.'_'.$F25->PATIENT_DATA->CARDNUMBER));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
		return $form_path;
    }   
   
    public function deleteForm($form_path)
    {
        return deleteFileUtils($form_path);
    }
}
?>
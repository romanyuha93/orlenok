<?php

require_once "Connection.php";
require_once "Utils.php";
 
class DoctorCardModuleDoctor
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $TIN;
    
    /**
    * @var string
    */
    var $FIRSTNAME;
     
    /**
    * @var string
    */
    var $MIDDLENAME;
     
    /**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $FULLNAME;
     
    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var string
    */
    var $BIRTHDAY; 

    /**
    * @var string
    */
    var $SEX;    
    
    /**
    * @var string
    */
    var $SPECIALITY;  

    /**
    * @var string
    */
    var $DATEWORKSTART;

    /**
    * @var string
    */
    var $DATEWORKEND;

    /**
    * @var string
    */
    var $MOBILEPHONE;

    /**
    * @var string
    */
    var $HOMEPHONE;

    /**
    * @var string
    */
    var $EMAIL;

    /**
    * @var string
    */
    var $SKYPE;

    /**
    * @var string
    */
    var $HOMEADRESS; 

    /**
    * @var string
    */
    var $POSTALCODE;
    
     /**
    * @var string
    */
    var $STATE;  
    
    /**
    * @var string
    */
    var $REGION;
    
    /**
    * @var string
    */
    var $CITY;  
    
    
    /**
    * @var string
    */
    var $BARCODE;
    
    
    /**
     * @var int
     */
    var $isFired;
    
    
    /**
     * @var string
     */
    var $fingerid;
}

class DoctorCardModule
{	
    /**
     * @param DoctorCardModuleDoctor $p1
     */
    public function registertypes($p1)
    {}
    
    /** 
 	* @param string $lastname
    * @param int $firedFilter
	* @param string $subdivId
	* @return DoctorCardModuleDoctor[]
	*/    
    public function getDoctorsFiltered($lastname, $firedFilter, $subdivId) 
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, TIN, FIRSTNAME, MIDDLENAME, LASTNAME, FULLNAME, SHORTNAME, BIRTHDAY,
			SEX, SPECIALITY, DATEWORKSTART, DATEWORKEND, MOBILEPHONE, HOMEPHONE,
			EMAIL, SKYPE, HOMEADRESS, POSTALCODE, STATE, REGION, CITY, BARCODE, isfired, FINGERID from DOCTORS where (ID is not null)";
		if ($lastname!="")	
		{ 
		  $QueryText=$QueryText." and (lower(LASTNAME) starting lower('$lastname') or BARCODE = '$lastname')";
        }
        if($firedFilter == 0)
		  $QueryText=$QueryText." and isFired = 0";
          
        if($subdivId != '' && $subdivId != null && $subdivId != '-')
            $QueryText .= " and SUBDIVISIONID = $subdivId ";
            
		  //$QueryText=$QueryText." and (LASTNAME containing '$lastname')"; }				
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new DoctorCardModuleDoctor;
			$obj->ID = $row[0];
			$obj->TIN = $row[1];
			$obj->FIRSTNAME = $row[2];
			$obj->MIDDLENAME = $row[3];
			$obj->LASTNAME = $row[4];
			$obj->FULLNAME = $row[5];
			$obj->SHORTNAME = $row[6];
			$obj->BIRTHDAY = $row[7];
			$obj->SEX = $row[8];
			$obj->SPECIALITY = $row[9];
			$obj->DATEWORKSTART = $row[10];
			$obj->DATEWORKEND = $row[11];
			$obj->MOBILEPHONE = $row[12];
			$obj->HOMEPHONE = $row[13];
			$obj->EMAIL = $row[14];
			$obj->SKYPE = $row[15];
			$obj->HOMEADRESS = $row[16];
			$obj->POSTALCODE = $row[17];
			$obj->STATE = $row[18];
			$obj->REGION = $row[19];
			$obj->CITY = $row[20];
            $obj->BARCODE = $row[21];
            $obj->isFired = $row[22];
            $obj->fingerid = $row[23];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /**
 	* @param DoctorCardModuleDoctor $newDoctor
	* @return int
 	*/
    public function addDoctor($newDoctor) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = 		
		"insert into DOCTORS (id, 
                                tin, 
                                firstname, 
                                middlename, 
                                lastname, 
                                fullname, 
                                shortname, 
                                birthday, 
                                sex, 
                                speciality, 
                                dateworkstart, 
                                dateworkend, 
                                mobilephone, 
                                homephone, 
                                email, 
                                skype, 
                                homeadress, 
                                postalcode, 
                                state, 
                                region, 
                                city,
                                isfired,
                                FINGERID) 
        values(null, 
        '".safequery($newDoctor->TIN)."', 
        '".safequery($newDoctor->FIRSTNAME)."', 
        '".safequery($newDoctor->MIDDLENAME)."', 
        '".safequery($newDoctor->LASTNAME)."', 
        '".safequery($newDoctor->FULLNAME)."', 
        '".safequery($newDoctor->SHORTNAME)."', 
        '$newDoctor->BIRTHDAY', 
        $newDoctor->SEX, 
        '".safequery($newDoctor->SPECIALITY)."', 
        '$newDoctor->DATEWORKSTART',"; 
        if($newDoctor->DATEWORKEND != '')
        {
            $QueryText .= "'$newDoctor->DATEWORKEND', ";
        }
        else
        {
            $QueryText .= "null, ";
        }
        $QueryText .= "'".safequery($newDoctor->MOBILEPHONE)."', 
        '".safequery($newDoctor->HOMEPHONE)."', 
        '".safequery($newDoctor->EMAIL)."', 
        '".safequery($newDoctor->SKYPE)."', 
        '".safequery($newDoctor->HOMEADRESS)."', 
        '".safequery($newDoctor->POSTALCODE)."', 
        '".safequery($newDoctor->STATE)."', 
        '".safequery($newDoctor->REGION)."', 
        '".safequery($newDoctor->CITY)."', 
        '".safequery($newDoctor->isFired)."', ";
        if(is_numeric($newDoctor->fingerid) && $newDoctor->fingerid != 0)
        {
            $QueryText .= $newDoctor->fingerid.") ";
        }
        else
        {
            $QueryText .= " null) ";
        }
        
        	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_commit($trans);
		ibase_free_query($query);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = 'select max(ID) from DOCTORS';
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$row = ibase_fetch_row($result);
        ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);	
		$connection->CloseDBConnection();	
		return $row[0];		
    }      
            
    /**
 	* @param DoctorCardModuleDoctor $newDoctor
	* @return boolean
 	*/
    public function updateDoctor($newDoctor) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
		$QueryText = "update DOCTORS set "; 
		if ("$newDoctor->TIN"=="")
		{ $QueryText = $QueryText."TIN=null, "; }
		else { $QueryText = $QueryText."TIN=$newDoctor->TIN, "; }	
		if ("$newDoctor->FIRSTNAME"=="")
		{ $QueryText = $QueryText."FIRSTNAME=null, "; }
		else { $QueryText = $QueryText."FIRSTNAME='".safequery($newDoctor->FIRSTNAME)."', "; }	
		if ("$newDoctor->MIDDLENAME"=="")
		{ $QueryText = $QueryText."MIDDLENAME=null, "; }
		else { $QueryText = $QueryText."MIDDLENAME='".safequery($newDoctor->MIDDLENAME)."', "; }
		if ("$newDoctor->LASTNAME"=="")
		{ $QueryText = $QueryText."LASTNAME=null, "; }
		else { $QueryText = $QueryText."LASTNAME='".safequery($newDoctor->LASTNAME)."', "; }
		if ("$newDoctor->FULLNAME"=="")
		{ $QueryText = $QueryText."FULLNAME=null, "; }
		else { $QueryText = $QueryText."FULLNAME='".safequery($newDoctor->FULLNAME)."', "; }
		if ("$newDoctor->SHORTNAME"=="")
		{ $QueryText = $QueryText."SHORTNAME=null, "; }
		else { $QueryText = $QueryText."SHORTNAME='".safequery($newDoctor->SHORTNAME)."', "; }
		if ("$newDoctor->BIRTHDAY"=="")
		{ $QueryText = $QueryText."BIRTHDAY=null, "; }
		else { $QueryText = $QueryText."BIRTHDAY='".safequery($newDoctor->BIRTHDAY)."', "; }
		if ("$newDoctor->SEX"=="")
		{ $QueryText = $QueryText."SEX=null, "; }
		else { $QueryText = $QueryText."SEX=$newDoctor->SEX, "; }
		if ("$newDoctor->SPECIALITY"=="")
		{ $QueryText = $QueryText."SPECIALITY=null, "; }
		else { $QueryText = $QueryText."SPECIALITY='".safequery($newDoctor->SPECIALITY)."', "; }
		if ("$newDoctor->DATEWORKSTART"=="")
		{ $QueryText = $QueryText."DATEWORKSTART=null, "; }
		else { $QueryText = $QueryText."DATEWORKSTART='".safequery($newDoctor->DATEWORKSTART)."', "; }
		if ("$newDoctor->DATEWORKEND"=="")
		{ $QueryText = $QueryText."DATEWORKEND=null, "; }
		else { $QueryText = $QueryText."DATEWORKEND='".safequery($newDoctor->DATEWORKEND)."', "; }
		if ("$newDoctor->MOBILEPHONE"=="")
		{ $QueryText = $QueryText."MOBILEPHONE=null, "; }
		else { $QueryText = $QueryText."MOBILEPHONE='".safequery($newDoctor->MOBILEPHONE)."', "; }
		if ("$newDoctor->HOMEPHONE"=="")
		{ $QueryText = $QueryText."HOMEPHONE=null, "; }
		else { $QueryText = $QueryText."HOMEPHONE='".safequery($newDoctor->HOMEPHONE)."', "; } 
		if ("$newDoctor->EMAIL"=="")
		{ $QueryText = $QueryText."EMAIL=null, "; }
		else { $QueryText = $QueryText."EMAIL='".safequery($newDoctor->EMAIL)."', "; }
		if ("$newDoctor->SKYPE"=="")
		{ $QueryText = $QueryText."SKYPE=null, "; }
		else { $QueryText = $QueryText."SKYPE='".safequery($newDoctor->SKYPE)."', "; } 
		if ("$newDoctor->HOMEADRESS"=="")
		{ $QueryText = $QueryText."HOMEADRESS=null, "; }
		else { $QueryText = $QueryText."HOMEADRESS='".safequery($newDoctor->HOMEADRESS)."', "; }
		if ("$newDoctor->POSTALCODE"=="")
		{ $QueryText = $QueryText."POSTALCODE=null, "; }
		else { $QueryText = $QueryText."POSTALCODE='".safequery($newDoctor->POSTALCODE)."', "; } 
        if ("$newDoctor->STATE"=="")
		{ $QueryText = $QueryText."STATE=null, "; }
		else { $QueryText = $QueryText."STATE='".safequery($newDoctor->STATE)."', "; }
        if ("$newDoctor->REGION"=="")
		{ $QueryText = $QueryText."REGION=null, "; }
		else { $QueryText = $QueryText."REGION='".safequery($newDoctor->REGION)."', "; }
        if ("$newDoctor->CITY"=="")
		{ $QueryText = $QueryText."CITY=null, "; }
		else { $QueryText = $QueryText."CITY='".safequery($newDoctor->CITY)."', "; }
        if ("$newDoctor->isFired"=="")
        { $QueryText = $QueryText."isfired=0 "; }
		else { $QueryText = $QueryText."isfired='".safequery($newDoctor->isFired)."', "; }
        
        if(is_numeric($newDoctor->fingerid) && $newDoctor->fingerid != 0)
        {
            $QueryText .= "FINGERID=".$newDoctor->fingerid." ";
        }
        else
        {
            $QueryText .= "FINGERID=null ";
        }
        
		$QueryText = $QueryText." where (ID=$newDoctor->ID)";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();	
		return $success;		
    }
    
    /**
 	* @param string[] $Arguments,
	* @return boolean
 	*/ 		 		
    public function deleteDoctors($Arguments) 
     {
	/*		 
     	$connection = new Connection();   	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from DOCTORS where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{ $QueryText = $QueryText." or (ID=$Arguments[$i])"; }		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);
        
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from AVATARIMAGES where ((ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{ $QueryText = $QueryText." or (ID=$Arguments[$i])"; }	
        $QueryText = $QueryText.") and TYPE='D'";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;*/
		return true;
        
    }     
  
}

?>
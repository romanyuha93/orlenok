<?php

require_once "Connection.php";
require_once "Utils.php";


class ChiefStatisticsModule_PatientDoctorData
{
    /**
     * @var string
     */
    var $doctor_id;
    /**
     * @var string
     */
    var $doctor_lname;
    /**
     * @var string
     */
    var $doctor_fname;
    /**
     * @var string
     */
    var $doctor_sname;
    /**
     * @var string
     */
    var $doctor_short;
    
    
    /**
     * @var int
     */
    var $task_count;
    /**
     * @var int
     */
    var $task_regular_count;
    /**
     * @var int
     */
    var $patients_primary_count;
    
}




class ChiefStatisticsModule_SMSStatistic
{
    /**
     * @var string
     */
    var $period;
    
    /**
     * @var string
     */
    var $period_type;
    
    /**
     * @var string
     */
    var $period_startDate;
    /**
     * @var string
     */
    var $period_endDate;
    
    
    
    /**
     * @var int
     */
     var $sms_count;
    /**
     * @var int
     */
     var $sms_success_count;
    /**
     * @var int
     */
     var $sms_unsuccess_count;
    /**
     * @var int
     */
     var $sms_undifinied_count;
     
     
    /**
     * @var ChiefStatisticsModule_SMSData[]
     */
     var $smsData;
    

    
}

class ChiefStatisticsModule_SMSData
{
    /*
                case "-2": //???????
                case "-1": //???????? ?????
                case "0": //???????
                case "1": //????????????
				case "2": //??????????
                case "3": //??????????
                case "5": //??? ?? ?????
                case "50": //???????? ??????????
                case "96": //???? ???? ??? ???????? SMS
                case "99": //?????? ? ?????? //????? ?? ?????????????    
    */
    
    /**
     * @var int
     */
     var $sms_label;
    /**
     * @var int
     */
     var $sms_color;
    /**
     * @var int
     */
     var $sms_count;
}

class ChiefStatisticsModule
{
    /**
     * @param ChiefStatisticsModule_PatientDoctorData $p1
     * @param ChiefStatisticsModule_SMSData $p2
     * @param ChiefStatisticsModule_SMSStatistic $p3
     * @return void
     */
    public function registertypes($p1, $p2, $p3){}
    
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @return ChiefStatisticsModule_PatientDoctorData[]
    */  
    public function getPatientsStatistics($startDate, $endDate)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection("main",false);
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $rows = array();
  
		$sql_olap = "select

                    doctors.id,
                    doctors.lastname,
                    doctors.firstname,
                    doctors.middlename,
                    
                    (select count(distinct(patients.id)) from tasks
                            inner join patients
                            on tasks.patientid = patients.id
                        where tasks.factofvisit = 1
                            and patients.carddate >= '$startDate'
                            and patients.carddate <= '$endDate'
                            
                            and tasks.taskdate >= '$startDate'
                            and tasks.taskdate <= '$endDate'
                            
                            and patients.cardnumber is not null
                            and tasks.doctorid = doctors.id),
                    
                    (select count(patients.id) from tasks
                            inner join patients
                            on tasks.patientid = patients.id
                        where tasks.factofvisit = 1
                            and tasks.taskdate >= '$startDate'
                            and tasks.taskdate <= '$endDate'
                            
                            and patients.cardnumber is not null
                            and tasks.doctorid = doctors.id),
                    
                    (select count(distinct(patients.id)) from tasks
                            inner join patients
                            on tasks.patientid = patients.id
                        where tasks.factofvisit = 1
                            and patients.carddate >= '$startDate'
                            and patients.carddate <= '$endDate'
                            
                            and tasks.taskdate >= '$startDate'
                            and tasks.taskdate <= '$endDate'
                            and tasks.doctorid = doctors.id)
                    
                    
                    from doctors where (doctors.dateworkend is null) or (doctors.dateworkend >= '$startDate')";  
        $query_olap = ibase_prepare($trans, $sql_olap);
        $result_olap = ibase_execute($query_olap);
        
		while ($row = ibase_fetch_row($result_olap))
		{ 
            $obj = new ChiefStatisticsModule_PatientDoctorData;
            $obj->doctor_id = $row[0];
            $obj->doctor_lname = $row[1];
            $obj->doctor_fname = $row[2];
            $obj->doctor_sname = $row[3];
            
            $shortname = $obj->doctor_lname;
            if(mb_strlen($obj->doctor_fname) > 0)
            {
                $shortname .= ' '.mb_substr($obj->doctor_fname, 0, 1).'.';
                //$fullname .= ' '.$obj->doctor_fname;                
                if(mb_strlen($obj->doctor_sname) > 0)
                {
                    $shortname .= mb_substr($obj->doctor_sname, 0, 1).'.';
                    //$fullname .= ' '.$obj->doctor_sname;
                }
            }
            $obj->doctor_short = $shortname;
            
            $obj->patients_primary_count = $row[4];
            $obj->task_regular_count = $row[5]-$row[6];
            $obj->task_count = $row[5];
            
            $rows[] = $obj;
        }
        
        ibase_free_query($query_olap);
        ibase_free_result($result_olap);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
    }
    
    
    // ************************ SMS START **********************************
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @param string $period
    * @return ChiefStatisticsModule_SMSStatistic[]
    */  
    public function getSMSStatistics($startDate, $endDate, $period)
    {
        define('tfClientId','chiefsms');
        
        $connection = new Connection();
        $connection->EstablishDBConnection("main",false);
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $rows = array();
        /*
            <fx:Object label="day" data="1d"/>
    		<fx:Object label="week" data="7d"/>
    	    <fx:Object label="month" data="1m"/>
    		<fx:Object label="year"" data="12m"/>
        */
        $fetchLimit = '';
        if($period == "d1d")
        {
            $fetchLimit = "1 days";
        }
        else if($period == "d7d")
        {
            $fetchLimit = "7 days";
        }
        else if($period == "m1m")
        {
            $fetchLimit = "1 months";
        }
        else if($period == "m12m")
        {
            $fetchLimit = "12 months";
        }
        
        if($fetchLimit == '')
        {
            return null;
        }
        //$fetchLimit = "12 months";
        //$fetchLimit = "1 months";
        //$fetchLimit = "7 days";
        //$fetchLimit = "1 days"; 
        
        $a = strtotime($startDate)+86400; 
        $b = strtotime($endDate)+86400;
        
        $period_inc = 1;
        
        if ($b <= strtotime(gmdate("Y-m-d",$a)." +".$fetchLimit)) 
        {
            $rows[] = $this->fetchChunk($a,$b,$period_inc,$period,$trans);
            $period_inc++;
            $a += 86400;
        }
        else 
        { 
           $lowerBound = $a;
           $upperBound = strtotime(gmdate("Y-m-d",$a)." +".$fetchLimit);
           while ($upperBound < $b) 
           {
             $rows[] =  $this->fetchChunk($lowerBound,$upperBound,$period_inc,$period,$trans);
             $period_inc++;
             $lowerBound = $upperBound;
             $upperBound = strtotime(gmdate("Y-m-d",$lowerBound)." +".$fetchLimit);
             
             $lowerBound += 86400;
             $upperBound += 86400;
           }
           $rows[] = $this->fetchChunk($lowerBound,$b,$period_inc,$period,$trans);
           $period_inc++;
        }
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows; 
    }
    /**
    * @return ChiefStatisticsModule_SMSStatistic
    */ 
    private function fetchChunk($a,$b,$period_inc,$period,$trans) {
        $obj = $this->_getSMSStatistics(gmdate("Y-m-d",$a), gmdate("Y-m-d",$b), $trans);
        $obj->period = $period_inc;
        $obj->period_startDate = gmdate("Y-m-d",$a);
        $obj->period_endDate = gmdate("Y-m-d",$b);
        $obj->period_type = $period;
        return $obj;
    }
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @return ChiefStatisticsModule_SMSStatistic
    */  
    private function _getSMSStatistics($startDate, $endDate, $trans)
    {
        $rows = array();
  
		$sql_sms_olap = "select         
            (select coalesce( count(smssender.id),0 )
            from smssender
            where (smssender.responsecode = -2 or smssender.responsecode = 400)
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
                
            (select coalesce( count(smssender.id),0 )
            from smssender
            where smssender.responsecode = -1
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),

            (select coalesce( count(smssender.id),0 )
            from smssender
            where smssender.responsecode = 0
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where (smssender.responsecode = 1 or smssender.responsecode = 100)
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where (smssender.responsecode = 2 or smssender.responsecode = 200)
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where (smssender.responsecode = 3 or smssender.responsecode = 300)
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where (smssender.responsecode = 5 or smssender.responsecode = 500)
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where smssender.responsecode = 50
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where smssender.responsecode = 96
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59'),
            
            (select coalesce( count(smssender.id),0 )
            from smssender
            where smssender.responsecode = 99
            and smssender.createdate >= '$startDate 00:00:00'
            and smssender.createdate <= '$endDate 23:59:59') "; 
        $sql_sms_olap .= 'from RDB$DATABASE';  
        $query_sms_olap = ibase_prepare($trans, $sql_sms_olap);
        $result_sms_olap = ibase_execute($query_sms_olap);
        
		$row = ibase_fetch_row($result_sms_olap);
		
            $objS = new ChiefStatisticsModule_SMSStatistic;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_02_count = $row[0];
            $obj->sms_label = '-2';
            $obj->sms_color = 16711680;//red1
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_01_count = $row[1];
            $obj->sms_label = '-1';
            $obj->sms_color = 16711833;//red2
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_0_count = $row[2];
            $obj->sms_label = '0';
            $obj->sms_color = 16763904;//orange1
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_1_count = $row[3];
            $obj->sms_label = '1';
            $obj->sms_color = 16764006;//orange2
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_2_count = $row[4];
            $obj->sms_label = '2';
            $obj->sms_color = 52326;//green1
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_3_count = $row[5];
            $obj->sms_label = '3';
            $obj->sms_color = 65280;//grenn2
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_5_count = $row[6];
            $obj->sms_label = '5';
            $obj->sms_color = 16776960;//orange3
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_50_count = $row[7];
            $obj->sms_label = '50';
            $obj->sms_color = 16777113;//orange4
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_96_count = $row[8];
            $obj->sms_color = 16724736;//red3
            $obj->sms_label = '96';
            $objS->smsData[] = $obj;
            
            $obj = new ChiefStatisticsModule_SMSData;
            $obj->sms_count = $sms_99_count = $row[9];
            $obj->sms_color = 16737843;//red4
            $obj->sms_label = '99';
            $objS->smsData[] = $obj;
            
            
            $objS->sms_success_count = $sms_2_count + $sms_3_count;
            $objS->sms_unsuccess_count = $sms_02_count + $sms_01_count + $sms_96_count + $sms_99_count;
            $objS->sms_undifinied_count = $sms_0_count + $sms_1_count + $sms_5_count + $sms_50_count;
            
            $objS->sms_count = $objS->sms_success_count + $objS->sms_unsuccess_count + $objS->sms_undifinied_count;
        
        ibase_free_query($query_sms_olap);
        ibase_free_result($result_sms_olap);
        		
        return $objS;             
    }
    
    
}
?>
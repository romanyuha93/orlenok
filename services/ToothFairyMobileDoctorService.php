<?php

require_once "Connection.php";
require_once "Utils.php";

class ToothFairyMobileDoctorService_TaskPatient
{
    /**
    * @var string
    */
    var $task_id;
    
    /**
    * @var string
    */
    var $task_startTime;
    
    /**
    * @var string
    */
    var $task_endTime; 
    
    /**
     * @var int
     */ 
    var $task_interval_minutes;
    
    /**
    * @var string
    */
    var $task_workdescription;
     
    /**
    * @var string
    */
    var $task_comment;
     
    /**
    * @var string
    */
    var $patient_id;
    
    /**
    * @var string
    */
    var $patient_lname;
     
    /**
    * @var string
    */
    var $patient_fname;
    
    /**
    * @var string
    */
    var $patient_sname;    
   
    /**
    * @var string
    */
    var $patient_fullcardnumber;
    
    /**
    * @var int
    */
    var $patient_primaryflag;
     
    /**
    * @var string
    */
    var $patient_phone;
    
    /**
    * @var string
    */
    var $patient_mobile;
     
    /**
    * @var string
    */
    var $patient_birthday;
     
    /**
    * @var int
    */
    var $patient_sex;
    
	/**
    * @var string
    */
    var $task_range;
    
    /**
    * @var string
    */
    var $patient_fio;
    
    /**
    * @var string
    */
    var $patient_ages;
    
    /**
    * @var string
    */
    var $patient_notes;
    
    /**
    * @var int
    */
    var $is_plus_object;//1 - task patient object, 0 - plus task object
}
class ToothFairyMobileDoctorService_LazyPatient
{
    /**
    * @var string
    */
    var $patient_id;
    
    /**
    * @var string
    */
    var $patient_fio;
}

class ToothFairyMobileDoctorService_Room
{
    /**
    * @var string
    */
    var $room_id;
       
    /**
    * @var string
    */
    var $room_number;
     
    /**
    * @var string
    */
    var $room_name;
    
    /**
    * @var ToothFairyMobileDoctorService_WPlace[]
    */
    var $workplaces;	
}

        class ToothFairyMobileDoctorService_WPlace
        {
            /**
            * @var string
            */
            var $workplace_id;
             
            /**
            * @var string
            */
            var $workplace_number;
            
            /**
            * @var string
            */
            var $workplace_name;	
        }
        
        
class ToothFairyMobileDoctorService_Task
{
    /**
    * @var string
    */
    var $task_date;
     
    /**
    * @var string
    */
    var $task_begin;
     
    /**
    * @var string
    */
    var $task_end;
    
    /**
    * @var string
    */
    var $patient_id;
    /**
    * @var string
    */
    var $patient_fname;
    
    /**
    * @var string
    */
    var $patient_lname;
    
    /**
    * @var string
    */
    var $patient_sname;
    
    /**
    * @var string
    */
    var $patient_phone;
     
    /**
    * @var string
    */
    var $doctor_id;
    
    /**
    * @var string
    */
    var $task_room_id;
     
    /**
    * @var string
    */
    var $task_workplace_id;
    
    /**
    * @var string
    */
    var $task_wd;
     
    /**
    * @var string
    */
    var $task_cm;	
}


class ToothFairyMobileDoctorService_Patient
{
    /**
    * @var string
    */
    var $patient_id;
    
    /**
    * @var string
    */
    var $patient_comment;
    
    /**
    * @var string
    */
    var $patient_lname;
     
    /**
    * @var string
    */
    var $patient_fname;
    
    /**
    * @var string
    */
    var $patient_sname;    
   
    /**
    * @var string
    */
    var $patient_cardnumber;
     
    /**
    * @var string
    */
    var $patient_phone;
    
    /**
    * @var string
    */
    var $patient_mobile;
     
    /**
    * @var string
    */
    var $patient_birthday;
     
    /**
    * @var int
    */
    var $patient_sex;
    
}

class ToothFairyMobileDoctorService
{
    
     /**
     * 
     * @param ToothFairyMobileDoctorService_TaskPatient $p1
     * @param ToothFairyMobileDoctorService_LazyPatient $p2
     * @param ToothFairyMobileDoctorService_Room $p3
     * @param ToothFairyMobileDoctorService_WPlace $p4
     * @param ToothFairyMobileDoctorService_Patient $p5
     * @return void
     */
    public function registerTypes($p1, $p2, $p3, $p4, $p5){}
    
    
    /** 
    * @param string $doctor_id
    * @param string $filterdate
    * @param string $startWorkTime //11:00:00
    * @param string $endWorkTime //11:00:00
    * @return ToothFairyMobileDoctorService_TaskPatient[]
	*/    
    public function getPatientsShedule($doctor_id, $filterdate, $startWorkTime, $endWorkTime) 
    { 
        $endAddTaskTime = $startWorkTime;
        $nowDate = new DateTime();
        $nowDate->setTime(0,0,0);
        
        $filtrDate = new DateTime($filterdate);
        
        $startDate = new DateTime($filterdate.' '.$startWorkTime);
        $endDate = new DateTime($filterdate.' '.$endWorkTime);
        
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select
                TASKS.ID,
                TASKS.BEGINOFTHEINTERVAL,
                TASKS.ENDOFTHEINTERVAL,
                TASKS.WORKDESCRIPTION,
                TASKS.COMMENT,
                PATIENTS.ID,
                PATIENTS.LASTNAME,
                PATIENTS.FIRSTNAME,
                PATIENTS.MIDDLENAME,
                PATIENTS.CARDBEFORNUMBER,
                PATIENTS.CARDNUMBER,
                PATIENTS.CARDAFTERNUMBER,
                PATIENTS.PRIMARYFLAG,
                PATIENTS.TELEPHONENUMBER,
                PATIENTS.MOBILENUMBER,
                PATIENTS.BIRTHDAY,
                PATIENTS.SEX, 
                patients.hobby
            
            from TASKS
                left join PATIENTS on (TASKS.PATIENTID = PATIENTS.ID)
            
            where TASKS.DOCTORID = '$doctor_id'
                and TASKS.taskdate = '$filterdate'
            order by TASKS.BEGINOFTHEINTERVAL asc;";
            
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        
        $rows = array();
        $firstAddObjectFlag = false;
        $prev_objectEndDate = null;
        $prev_objectEndTime;
        
		while ($row = ibase_fetch_row($result))
		{ 
		  $objectStartDate = new DateTime($filterdate.' '.$row[1]);
		  $objectEndDate = new DateTime($filterdate.' '.$row[2]);
          
          if(($startDate < $objectStartDate)&&($firstAddObjectFlag == false)&&($nowDate <= $filtrDate))
          {
           	$objAddTask = new ToothFairyMobileDoctorService_TaskPatient;
            $objAddTask->is_plus_object = 0;
            $objAddTask->task_startTime = $startWorkTime;
            $objAddTask->task_endTime = substr($row[1], 0, 5);//$row[1]
            $rows[] = $objAddTask;
          }
            $firstAddObjectFlag = true;
          
            $obj = new ToothFairyMobileDoctorService_TaskPatient;
            $obj->task_id = $row[0];
            $obj->task_startTime = substr($row[1], 0, 5);
            $obj->task_endTime = substr($row[2], 0, 5);
            $obj->task_workdescription = $row[3];
            $obj->task_comment = $row[4];
            $obj->task_range = $obj->task_startTime.' - '.$obj->task_endTime;
            $obj->patient_id = $row[5];
            $obj->patient_lname = $row[6];
            $obj->patient_fname = $row[7];
            $obj->patient_sname = $row[8];
            $obj->patient_fullcardnumber = "{$row[9]}{$row[10]}{$row[11]}";
            $obj->patient_primaryflag = $row[12];
            $obj->patient_phone = $row[13];
            $obj->patient_mobile = $row[14];
            $obj->patient_birthday = $row[15];
            $obj->patient_sex = $row[15];
            $obj->patient_notes = $row[16];
            $obj->patient_fio = $obj->patient_lname.' '.$obj->patient_fname.' '.$obj->patient_sname;
            $patient_ages = $this->getAges($obj->patient_birthday);
            if($patient_ages != -1)
            {
                $obj->patient_ages = $this->getAges($obj->patient_birthday);
            }
            else
            {
                $obj->patient_ages = '-'; 
            }
            $obj->patient_notes = $row[17];
            $obj->task_interval_minutes = $objectEndDate->diff($objectStartDate)->d*24 + $objectEndDate->diff($objectStartDate)->h*60 + $objectEndDate->diff($objectStartDate)->i;
            $obj->is_plus_object = 1;
          //add additem start  
          if(($objectStartDate > $prev_objectEndDate)&&($prev_objectEndDate != null)&&($nowDate <= $filtrDate))
          {
           	$objAddTask = new ToothFairyMobileDoctorService_TaskPatient;
            $objAddTask->is_plus_object = 0;
            $objAddTask->task_startTime = $prev_objectEndTime;
            $objAddTask->task_endTime = $obj->task_startTime;
            $rows[] = $objAddTask;
          }
          $prev_objectEndDate = $objectEndDate;
          $prev_objectEndTime = $obj->task_endTime;
          //add additem end  
          
          $rows[] = $obj;
          
          $endAddTaskTime = $obj->task_endTime;
		};
        //add plus object
        if($nowDate <= $filtrDate)
        {
           	$objAddTask = new ToothFairyMobileDoctorService_TaskPatient;
            $objAddTask->is_plus_object = 0;
            $objAddTask->task_startTime = $endAddTaskTime;
            $rows[] = $objAddTask;
        }
        
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    /**
     * @param string $searchText
     * @return ToothFairyMobileDoctorService_LazyPatient[]
     */
    public function getLazyPatients($searchText)
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText =
			"select patients.ID, patients.LASTNAME, patients.FIRSTNAME, patients.MIDDLENAME
                 from patients
                 where lower(patients.FULLNAME) starting lower('$searchText') and PRIMARYFLAG = 1 order by patients.LASTNAME";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
            $obj = new ToothFairyMobileDoctorService_LazyPatient;
            $obj->patient_id = $row[0];
            $obj->patient_fio = $row[1].' '.$row[2].' '.$row[3];
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    
    

    /** 
    * @return ToothFairyMobileDoctorService_Room[]
 	*/
    public function getRooms() 
    { 
        $connection = new Connection();
	    $connection->EstablishDBConnection();
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);  
	    $QueryText = "select ROOMS.ID, ROOMS.NUMBER, ROOMS.NAME from ROOMS";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
        
		while($row = ibase_fetch_row($result))
		{
            $obj = new ToothFairyMobileDoctorService_Room;
            $obj->room_id = $row[0];
            $obj->room_number = $row[1];
            $obj->room_name = $row[2];
             $TemplQuery = "select WORKPLACES.ID, WORKPLACES.NUMBER, WORKPLACES.DESCRIPTION from WORKPLACES where WORKPLACES.roomid = '$row[0]'";
             $templquery = ibase_prepare($trans, $TemplQuery);
             $templresult = ibase_execute($templquery);
             while($templrow = ibase_fetch_row($templresult))
             {
                $templ = new ToothFairyMobileDoctorService_WPlace;
                $templ->workplace_id = $templrow[0];
                $templ->workplace_number = $templrow[1];
                $templ->workplace_name = $templrow[2];
                $obj->workplaces[] = $templ;
             }
             ibase_free_query($templquery);
             ibase_free_result($templresult);
            
            $rows[] = $obj;
		}	
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();		
        return $rows;
    }
    /**
 	* @param  ToothFairyMobileDoctorService_Task $task
    * @return boolean
 	*/
    public function addTask($task) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if(($task->patient_id != '')&&($task->patient_id != null))
        {
            $QueryText = "insert into TASKS (
                                        id, 
                                        taskdate, 
                                        beginoftheinterval, 
                                        endoftheinterval, 
                                        patientid, 
                                        doctorid, 
                                        roomid, 
                                        workplaceid, 
                                        noticed, 
                                        factofvisit, 
                                        dateofvisit, 
                                        timeofvisit, 
                                        workdescription, 
                                        comment)
        		values(null, '$task->task_date', '$task->task_begin', '$task->task_end', $task->patient_id, $task->doctor_id, $task->task_room_id, 
                '$task->task_workplace_id', 0, 0, null, null, '".safequery($task->task_wd)."', '".safequery($task->task_cm)."')";
            $query = ibase_prepare($trans, $QueryText);
    		ibase_execute($query);
    		ibase_free_query($query);
        }
        else
        {
            $shortname = $task->patient_lname;
            $fullname = $task->patient_lname;   
            if(mb_strlen($task->patient_fname) > 0)
            {
                $shortname .= ' '.mb_substr($task->patient_fname, 0, 1).'.';
                $fullname .= ' '.$task->patient_fname;                
                if(mb_strlen($task->patient_sname) > 0)
                {
                    $shortname .= ' '.mb_substr($task->patient_sname, 0, 1).'.';
                    $fullname .= ' '.$task->patient_sname;
                }
            }
            $QueryText = "INSERT into PATIENTS 
                        (id,
                        firstname,  
                        lastname,
                        middlename,
                        shortname,
                        fullname,
                        telephonenumber,
                        primaryflag) 
                        values 
                        (null, 
                        '".safequery($task->patient_fname)."', 
                        '".safequery($task->patient_lname)."', 
                        '".safequery($task->patient_sname)."',
                        '".safequery($shortname)."',
                        '".safequery($fullname)."',
                        
                        '".safequery($task->patient_phone)."', 
                        0) returning ID";
    		$query = ibase_prepare($trans, $QueryText);
    		$result = ibase_execute($query);
            $row = ibase_fetch_row($result);
            $PRIMPATID = $row[0];
    		ibase_free_query($query);
    		ibase_free_result($result);
            if(($PRIMPATID != null)&&($PRIMPATID != ''))
            {
        		$QueryText = "insert into TASKS (
                                            id, 
                                            taskdate, 
                                            beginoftheinterval, 
                                            endoftheinterval, 
                                            patientid, 
                                            doctorid, 
                                            roomid, 
                                            workplaceid, 
                                            noticed, 
                                            factofvisit, 
                                            dateofvisit, 
                                            timeofvisit, 
                                            workdescription, 
                                            comment)
            		values(null, '$task->task_date', '$task->task_begin', '$task->task_end', $PRIMPATID, $task->doctor_id, $task->task_room_id, 
                    '$task->task_workplace_id', 0, 0, null, null, '".safequery($task->task_wd)."', '".safequery($task->task_cm)."')";
                $query = ibase_prepare($trans, $QueryText);
        		ibase_execute($query);
        		//$success = ibase_commit($trans);
        		ibase_free_query($query);
            }
        }
        
        
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;		
    }
    
    /**
	* @param string $pass_for_del
	* @param string $user_id
 	* @param string $task_id
	* @return boolean
 	*/ 		 		
    public function deletePatients($pass_for_del, $user_id, $task_id) 
     {           
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from TASKS where ID = $task_id";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    
    /**
    * @param int 
 	* @return string
 	*/
    public function getNextPatientCardNumber($currentYear)
     {      
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select first 1 CARDNUMBER, CARDDATE from PATIENTS where PRIMARYFLAG = 1";
        if($currentYear != -1)
        {
            $QueryText .= " and CARDDATE >= '$currentYear-01-01'";
        }
        $QueryText .= " order by CARDNUMBER desc";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);	
		$cardNumber=ibase_fetch_row($result);
		ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
        if($cardNumber[0]==null) 
        {
            return 0; 
        }
        else if($currentYear=="false") 
        {
            return $cardNumber[0];
        }
        else 
        {
            return $cardNumber[0];
        }		
    }
    /**
 	* @param ToothFairyMobileDoctorService_Patient $patient
    * @return boolean
 	*/
    public function createPatientCard($patient) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update PATIENTS set 		  
    		  FIRSTNAME='".safequery($patient->patient_fname)."',
    		  MIDDLENAME='".safequery($patient->patient_sname)."',
    		  LASTNAME='".safequery($patient->patient_lname)."',
    		  FULLNAME='".safequery($patient->patient_lname." ".$patient->patient_fname." ".$patient->patient_sname)."', ";
         
         $shortname = $patient->patient_lname;
         
         if(mb_strlen($task->patient_fname) > 0)
            {
                $shortname .= ' '.mb_substr($task->patient_fname, 0, 1).'.';
                if(mb_strlen($task->patient_sname) > 0)
                {
                    $shortname .= ' '.mb_substr($task->patient_sname, 0, 1).'.';
                }
            }
        
        
        $QueryText .= "SHORTNAME='".safequery($shortname)."',";
              
        if(($patient->patient_birthday != '')&&($patient->patient_birthday != 'null'))
        {
            $QueryText .= "BIRTHDAY='$patient->patient_birthday',";
        }
        
        $QueryText .= "SEX=$patient->patient_sex,
    		  VILLAGEORCITY=1,
    		  POPULATIONGROUPE=4,
              ADDRESS='',
              POSTALCODE='',
              TELEPHONENUMBER='".safequery($patient->patient_phone)."', 
              MOBILENUMBER='".safequery($patient->patient_mobile)."',
              EMPLOYMENTPLACE='',
              JOB='',
              WORKPHONE='',
              HOBBY='".safequery($patient->patient_comment)."',
              COUNTRY='',
              STATE='',
              RAGION='',
              EMAIL='',
              CITY='',
              CARDNUMBER='$patient->patient_cardnumber',
              CARDAFTERNUMBER='',
              CARDBEFORNUMBER='',
              DISCOUNT_AMOUNT = 0,
                            
			  PRIMARYFLAG = 1
                
        where ID = $patient->patient_id";
       

		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;		
    }
    
    //***********************************************PRIVATE FUNCTIONS START***********************************************
     /**
     * ToothFairyMobileService::getAges()
     * 
     * @param string $birthdate
     * @return int
     */
    private function getAges($birthdate)
    {
        if(($birthdate != null)&&($birthdate != ''))
        {
            $date = new DateTime($birthdate);
            $now = new DateTime();
            $interval = $now->diff($date);
            return $interval->y;
        }
        else
        {
            return -1;
        }
    }
    //***********************************************PRIVATE FUNCTIONS END*************************************************
}
?>

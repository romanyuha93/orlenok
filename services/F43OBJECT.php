<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

define('dentdis', serialize(array('C', 'Cd', 'Pl', 'F', 'r', 'H', 'P', 'Pt', 'Lp', '', 'R', 'A', 'ar', 'Am', 'rez', 'pin', 'I', 'Rp', 'Z', 'Lp', 'Lp', 'Gv', 'Gv', 'Gv', 'I', 'tPL', 'Lps', 'Htd', 'Fl', 'Tb', 'OP', 'SC', 'BT', 'WSD','INT', 'Сob','Pldf')));
define('dentdiscrown', serialize(array('C', 'Cd', 'Pl', 'F', 'r', 'H', 'P', 'Pt', '', '', 'R', 'A', 'ar', 'Am', 'rez', 'pin', 'I', 'Rp', '', '', '', '', '', '', 'I', 'tPL', '', 'Htd', 'Fl', 'Tb', 'OP', 'SC', 'BT', 'WSD','INT', 'Сob','Pldf')));
define('dentdisgum', serialize(array('', '', '', '', '', '', '', '', 'Lp', '', '', '', '', '', '', '', '', '', 'Z', 'Lp', 'Lp', 'Gv', 'Gv', 'Gv', '', '', 'Lps', '', '', '', '', '', '', '','', '','')));

//UA
define('dentdiswordall', serialize(array('карiєс', 'коронка', 'пломба', 'фасетка', 'реставрація', 'гемісекція', 'пульпіт', 'періодонтит', 'пародонтит', '', 'корінь', 'відсутній зуб', 'штучний зуб', 'ампутація', 'резекція', 'штифт', 'імплантат з коронкою', 'реплантація', 'зубний камінь', 'генералізований пародонтит', 'пародонтит', 'генералізований гінгівіт', 'гінгівіт', 'гінгівіт', 'імплантат без коронки', 'тимчасова пломба', 'пародонтоз', 'дефект твердих тканин', 'флюороз', 'вкладка', 'ортодонтична патологія', 'вторинний карієс', 'скол', 'клиновидний дефект','інтактний', 'карієс під спостереженням', ' пломба дефект')));
define('dentdisword', serialize(array('карiєс', '', '', '', '', '', 'пульпіт', 'періодонтит', 'пародонтит', '', '', '', '', '', '', '', '', '', 'зубний камінь', 'генеральный пародонтит', 'пародонтит', 'генеральный гінгівіт', 'гінгівіт', 'гінгівіт', '', '', 'пародонтоз', 'дефект твердих тканин', 'флюороз', '', 'ортодонтична патологія', 'вторинний карієс', 'скол', 'клиновидний дефект','інтактний', 'карієс під спостереженням', ' пломба дефект')));
//RU
//define('dentdiswordall', serialize(array('кариес', 'коронка', 'пломба', 'фасетка', 'реставрация', 'гемисекция', 'пульпит', 'периодонтит', 'пародонтит', '', 'корень', 'отсутствует зуб', 'одиночный зуб', 'ампутация', 'резекция', 'штифт', 'имплантат с коронкой', 'реплантация', 'зубной камень', 'генерализованный пародонтит', 'пародонтит', 'генерализованный гингивит', 'гингивит', 'гингивит', 'имплантат без коронки', 'временная пломба', 'пародонтоз', 'дефект твердых тканей', 'флюороз', 'вкладка', 'ортодонтическая патология', 'вторинний кариес', 'скол', 'клиновидний дефект','интактный', 'кариес под наблюдением', ' пломба дефект')));
//define('dentdisword', serialize(array('кариес', '', '', '', '', '', 'пульпит', 'периодонтит', 'пародонтит', '', '', '', '', '', '', '', '', '', 'зубной камень', 'генеральный пародонтит', 'пародонтит', 'генеральный гингивит', 'гингивит', 'гингивит', '', '', 'пародонтоз', 'дефект твердых тканей', 'флюороз', '', 'ортодонтическая патология', 'вторичный кариес', 'скол', 'клиновидный дефект','интактный', 'кариес под наблюдением', ' пломба дефект')));


define('toothimgs' , serialize(array( 'b3u','b3u','b3u','b1u','b2u','l1u','l1u','l1u','l1u','l1u','l1u','b2u','b1u','b3u','b3u','b3u',
                            'b2d','b2d','b2d','b1d','b1d','l1d','l1d','l1d','l1d','l1d','l1d','b1d','b1d','b2d','b2d','b2d')));
define('toothimgsmilk' , serialize(array( 'Null','Null','Null','b3u','b3u','b1u','l1u','l1u','l1u','l1u','b1u','b3u','b3u','Null','Null','Null',
                                'Null','Null','Null','b2d','b2d','b1d','l1d','l1d','l1d','l1d','b1d','b2d','b2d','Null','Null','Null')));                               
//define('toothtexts' , serialize(array('8','7','6','5','4','3','2','1','1','2','3','4','5','6','7','8')));
//define('toothtextsmilk' , serialize(array('','','','V','IV','III','II','I','I','II','III','IV','V','','','')));
define('toothtexts' , serialize(array('8','7','6','5(V)','4(IV)','3(III)','2(II)','1(I)','1(I)','2(II)','3(III)','4(IV)','5(V)','6','7','8')));
define('toothtextsmilk' , serialize(array('8','7','6','5(V)','4(IV)','3(III)','2(II)','1(I)','1(I)','2(II)','3(III)','4(IV)','5(V)','6','7','8')));

define('toothid' , serialize(array('18', '17', '16', '15', '14', '13', '12', '11', '21', '22', '23', '24', '25', '26', '27', '28', '48', '47', '46', '45', '44', '43', '42', '41', '31', '32', '33', '34', '35', '36', '37', '38')));
define('toothbone', serialize(array('N','¾','½','¼')));
define('toothmobility', serialize(array('','I','II','III')));
//$st=timeMeasure();
//$F43=new F43OBJECT('2', true, true, true, true, true, true, true);
//print((timeMeasure()-$st)."\n");
//print_r($F43);

class F43OBJECT
{	
    /**
	* @var string
	*/
   var $PATIENT_ID;
    
    /**
    * @var PrintDataModule_ClinicRegistrationData
    */
    var $CLINIC_DATA;
    
    /**
    * @var PrintDataModule_PatientData
    */  
    var $PATIENT_DATA;
    
    /**
    * @var PatientAmbcard
    */  
    var $PATIENT_AMB;
    
    /**
    * @var ToothExam[]
    */  
    var $TOOTHEXAM_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_FIRST_SPECIFIED;
    
    /**
    * @var string
    */  
    var $DIAGNOS_FIRST_SPECIFIED_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_FIRST_ALL_SPECIFIED;
    
    /**
    * @var array
    */  
    var $DIAGNOS_FIRST_ALL_SPECIFIED_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_ALL_SPECIFIED;
    
    /**
    * @var array
    */  
    var $DIAGNOS_ALL_SPECIFIED_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_43_FIRST_ALL;
    
    /**
    * @var array
    */  
    var $DIAGNOS_43_FIRST_ALL_ARRAY;
    
    /**
    * @var string
    */  
    var $DIAGNOS_43_ALL;
    
    /**
    * @var array
    */  
    var $DIAGNOS_43_ALL_ARRAY;
    
    /**
    * @var string
    */  
    var $COMPLAINTS_FIRST_EXAM;
    
    /**
    * @var array
    */  
    var $COMPLAINTS_FIRST_EXAM_ARRAY;
    
    /**
    * @var string
    */  
    var $COMPLAINTS_ALL_EXAM;
    
    /**
    * @var array
    */  
    var $COMPLAINTS_ALL_EXAM_ARRAY;
    
    /**
    * @var string
    */
    var $DISEASEHISTORY_FIRST_EXAM; 
    
    /**
    * @var array
    */
    var $DISEASEHISTORY_FIRST_EXAM_ARRAY; 
    
    /**
    * @var string
    */
    var $DISEASEHISTORY_ALL_EXAM; 
    
    /**
    * @var array
    */
    var $DISEASEHISTORY_ALL_EXAM_ARRAY; 
    
    /**
    * @var string
    */  
    var $DATE_FIRST_EXAM;
    
    /**
    * @var string
    */
    var $COMORBIDITIES; 
    
    /**
    * @var array
    */
    var $COMORBIDITIES_ARRAY; 
    
    /**
    * @var date
    */  
    var $DATE_CURRENT;

    /**
    * @var Array
    */  
    var $DIARY;

    
    function __construct($patient_id, $printTestPage, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $sql_choice="TOOTHEXAM.AFTERFLAG = 0", $trans = null)     
    { 
         if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
        
        $this->PATIENT_ID=$patient_id;
        
        
        $this->CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(false, $trans);
        $this->PATIENT_DATA=new PrintDataModule_PatientData($patient_id, $trans);
        $this->PATIENT_AMB=new PatientAmbcard($patient_id, $trans);
        $this->COMORBIDITIES=$this->PATIENT_AMB->COMORBIDITIES;
        $this->TOOTHEXAM_ARRAY=$this->getToothexamArray($patient_id, $sql_choice, $trans);
        $complaints_all_exam='';
        $diseasehistory_all_exam='';
        $diagnos_all_specified='';  
        $diagnos_43_all_specified=''; 
        $diagnos_first_all_specified=''; 
        $diary=array();  
        foreach($this->TOOTHEXAM_ARRAY as $key=>$toothexam)
        {
            if($toothexam->COMPLAINTS != "")
                $complaints_all_exam.=($complaints_all_exam==''?'':'; ').$toothexam->EXAMDATE.': '.$toothexam->COMPLAINTS;
            if($toothexam->DISEASEHISTORY != "")
            $diseasehistory_all_exam.=($diseasehistory_all_exam==''?'':'; ').$toothexam->EXAMDATE.': '.$toothexam->DISEASEHISTORY;
            if($toothexam->DIAGNOSIS != "")
                $diagnos_43_all_specified.=($diagnos_43_all_specified==''?'':'; ').$toothexam->EXAMDATE.': '.$toothexam->DIAGNOSIS;
            //file_put_contents("D:/tmp.txt",  file_get_contents("D:/tmp.txt")."\n".$complaints_all_exam.var_export($diary, true));
            $diary1=F43OBJECT::getDoctorDiary($toothexam->TREATMENT_COURSE_ID, null, null, null, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $trans);
//             file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt")."\nDiary1".var_export($diary1, true));
            $diary=array_merge_recursive($diary,$diary1);                        
//            file_put_contents("C:/tmp.txt",  file_get_contents("C:/tmp.txt")."\nDiaryAfter".var_export($diary, true));
            $diagnos_treatment=ToothExam::getDiagnosTreatment($toothexam->TREATMENT_COURSE_ID, null, null, null, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $trans);

            foreach($diagnos_treatment[0] as $d=>$dd)
            {
                $diagnos_all_specified.=($diagnos_all_specified==''?'':'; ').$dd["DATE"].': '.$dd["TEXT"];
                if($key==0)
                {
                    $diagnos_first_all_specified.=($diagnos_first_all_specified==''?'':'; ').$dd["TEXT"];
                    if($d==0)
                    {
                        $this->DIAGNOS_FIRST_SPECIFIED=$dd["TEXT"];
                    }
                }
            }
             if($key==0)
             {
                $this->DIAGNOS_43_FIRST_ALL=$toothexam->DIAG_F43; 
                $this->DATE_FIRST_EXAM=$toothexam->EXAMDATE;
                $this->COMPLAINTS_FIRST_EXAM=$toothexam->COMPLAINTS;
                $this->DISEASEHISTORY_FIRST_EXAM=$toothexam->DISEASEHISTORY;                 
             }            
            $toothexam->PROC_PLAN=$diagnos_treatment[3];
            
        }
        
        uksort($diary,"datecmp");
        $this->DIARY=$diary;
        $this->DISEASEHISTORY_ALL_EXAM=$diseasehistory_all_exam;
        $this->COMPLAINTS_ALL_EXAM=$complaints_all_exam;
        $this->DIAGNOS_ALL_SPECIFIED=$diagnos_all_specified;
        $this->DIAGNOS_43_ALL=$diagnos_43_all_specified;
        $this->DIAGNOS_FIRST_ALL_SPECIFIED=$diagnos_first_all_specified;
        
        
        $date_current = time();
        $this->DATE_CURRENT=array(
        'FULL_DATE'=>ua_date('"d" M Y', $date_current ),
        'DAY'=>ua_date("d", $date_current ),
        'MONTH'=>ua_date("M", $date_current ),
        'YEAR'=>ua_date("y", $date_current ));
        
             
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
            
    }

    public function generateFieldArray($field, $len, $lenstart)
    {
        $field_array=$field.'_ARRAY';
        $this->$field_array=space_split($this->$field, $len, $lenstart);
    }
    
    public function generateCheckupArrays($len, $lenstart)
    {
        foreach($this->TOOTHEXAM_ARRAY as $toothexam)
        {
            $toothexam->CHECKUP_ARRAY=space_split($toothexam->CHECKUP, $len, $lenstart);
        }
        
    }
    
    /**
     * getDoctorDiary()
     * 
     * @param int $treatment_course_id
     * @param string $patient_id
     * @param string $start_date
     * @param string $end_date
     * @param boolean $includePriceInReport
     * @param boolean $includeMaterialsInReport
     * @param boolean $includeDoctorSignatureInReport=false, 
     * @param boolean $includeNoncompleteProcedures043Flag=false
     * @param boolean $includeShifrProcedures043Flag
     * @param boolean $includeCountProcedures043Flag
     * @param resource $trans
     * @return array
     */            
    public static function getDoctorDiary($treatment_course_id=null, $patient_id=null, $start_date=null, $end_date=null, $includePriceInReport=false, $includeMaterialsInReport=false, $includeDoctorSignatureInReport=false, $includeNoncompleteProcedures043Flag=false, $includeShifrProcedures043Flag=false, $includeCountProcedures043Flag=false, $trans=null)
    {
            $diary=array();
            $additions=ToothExam::getAdditions($treatment_course_id, $patient_id, $start_date, $end_date, $includePriceInReport, $includeDoctorSignatureInReport, $trans);

            foreach($additions[0] as $dd)
            {
                $diary[$dd["DATE"]]->ANAMNESIS[]=$dd["TEXT"];
            }                
            foreach($additions[1] as $dd)
            {
                $diary[$dd["DATE"]]->STATUS[]=$dd["TEXT"];
            }
            foreach($additions[2] as $dd)
            {
                $diary[$dd["DATE"]]->RECOMENDATIONS[]=$dd["TEXT"];
            }                
            foreach($additions[3] as $dd)
            {
                $diary[$dd["DATE"]]->EPICRISIS[]=$dd["TEXT"];
            }
                
            $diagnos_treatment=ToothExam::getDiagnosTreatment($treatment_course_id, $patient_id, $start_date, $end_date, $includePriceInReport, $includeMaterialsInReport, $includeDoctorSignatureInReport, $includeNoncompleteProcedures043Flag, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $trans);
            
            foreach($diagnos_treatment[1] as $dd)
            {
                $diary[$dd["DATE"]]->DIAG_DIARY[]=$dd["TEXT"];
            }

            foreach($diagnos_treatment[2] as $dd)
            {
                $diary[$dd["DATE"]]->PROC_DIARY[]=$dd["TEXT"];
            }
            
            $plans=ToothExam::getPlan($treatment_course_id, $patient_id, $start_date, $end_date, $includePriceInReport, $trans);
            foreach($plans as $dd)
            {
                $diary[$dd["DATE"]]->PLAN[]=$dd["TEXT"];
            }        
            uksort($diary,"datecmp");
            return $diary;
    }
        

    public static function getToothexamArray($patient_id, $sql_choice, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
        	$QueryText = "
            select	EXAMID from TOOTHEXAM	
            			where ($sql_choice) and (TOOTHEXAM.ID = $patient_id) order by TOOTHEXAM.EXAMDATE";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
			$toothexam_array=array();
			while ($row = ibase_fetch_row($result))
			{	
                $toothexam_array[]=new ToothExam($row[0], $trans);                	      
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }     
            
            return $toothexam_array;                 
     
    } 
        
}

class Tooth
{ 
    /**
    * @var array
    */
    var $CROWN;
    
    /**
    * @var array
    */
    var $GUM;
    
    /**
    * @var array
    */
    var $STATE;
    
    /**
    * @var string
    */
    var $CROWN_STR;
    
    /**
    * @var string
    */
    var $GUM_STR;
    
    /**
    * @var string
    */
    var $STATE_STR;
                       
    /**
    * @var string
    */
    var $IMAGE;
    function __construct($n, $bite_type, $source, $states=null) 
    { 
        $toothid=unserialize(toothid);
        $id=$bite_type==0?$toothid[$n]:$toothid[$n]+40;
        $type=$bite_type;
        $gum=array();
        $crown=array();
        $dentdisword=unserialize(dentdisword);        
        $gp="";
        switch(floor($id/10))
        {
            case 1: 
            case 2: 
            case 5: 
            case 6:
                $gp="GP1";
                break;  
            case 3: 
            case 4: 
            case 7: 
            case 8:
                $gp="GP2";
                break;                                                               
        }
        
        $i=1;
        
       if($type==0||($id%10)<6)
        {
            foreach(unserialize(dentdisgum) as $dd)
            {
                
                if(strstr($source,$gp.'>'.$i.'<'))
                    {
                          if($dd<>'') $gum[]=$dd;               
                    } 
                $i++;
            }
        }
        
        if(strstr(strval($source),'id="'.$id.'"'))
        {
            $xml = simplexml_load_string('<xml>'.$source.'</xml>');       
            $tmp=($xml->xpath("/xml/zub[@id='".$id."']"));
            //$type=$tmp[0]['milkflag']; 
            $i=1;
            foreach(unserialize(dentdisgum) as $dd)
            {
                
                if(strstr($tmp[0]->asXML(),'>'.$i.'<'))
                    {
                          if($dd<>'') $gum[]=$dd;      
                    } 
                $i++;
            } 
            
            $i=1;
            foreach(unserialize(dentdiscrown) as $dd)
            {
                
                if(strstr($tmp[0]->asXML(),'>'.$i.'<'))
                {
                      if($dd<>'') $crown[]=$dd;                 
                } 
                $i++;
            } 
        }   
                            
        $this->CROWN=array_unique($crown);
        $this->GUM=array_unique($gum); 
        
        $crown_str='';
        foreach($this->CROWN as $d)
        {
            $crown_str.=($crown_str==''?'':', ').$d;
        }
        $this->CROWN_STR=$crown_str;
        
        $gum_str='';
        foreach($this->GUM as $d)
        {
            $gum_str.=($gum_str==''?'':', ').$d;
        }
        $this->GUM_STR=$gum_str;
        $toothbone=unserialize(toothbone);
        $toothmobility=unserialize(toothmobility);
        
        $this->STATE=array($toothbone[0],$toothmobility[0]);
        if(nonull($states))
        {
            if(strstr($states,'num="'.$id.'"'))
            {
                $xml = simplexml_load_string($states);       
                $tmp=($xml->xpath("/toothes/tooth[@num='".$id."']"));
                if(isset($tmp[0]["boneState"]))
                {
                    $this->STATE[0]=$toothbone[(int)$tmp[0]["boneState"]]; 
                }
                if(isset($tmp[0]["mobilityState"]))
                {
                 
                  $this->STATE[1]=$toothmobility[(int)$tmp[0]["mobilityState"]]; 
                }
            }

        }
        $this->STATE_STR.=$this->STATE[0]."     ".$this->STATE[1];

         //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$id."\n".$states."\n".var_export($this->STATE, true));
        $toothimgs=($type==0?unserialize(toothimgs):unserialize(toothimgsmilk));
        $this->IMAGE=dirname(__FILE__).'/tbs/res/teeth/'.$toothimgs[$n].'.jpg';
        
    }
    
}

class PatientAmbcard
{
    /**
    * @var string
    */
    var $OCCLUSION; 
    
    /**
    * @var array
    */
    var $OCCLUSION_ARRAY; 
    
    /**
    * @var string
    */
    var $COMORBIDITIES; 
    
    /**
    * @var array
    */
    var $COMORBIDITIES_ARRAY; 
    
    /**
    * @var string
    */
    var $HYGIENE;
    
    /**
    * @var array
    */
    var $HYGIENE_ARRAY;
    
    /**
    * @var string
    */
    var $ANALYSIS;
    
    /**
    * @var array
    */
    var $ANALYSIS_ARRAY;
    
    /**
    * @var string
    */
    var $VITASCALE;
    
    /**
    * @var string;
    */
    var $HYGCONTROL_PRIMARY;	
    
    /**
    * @var string;
    */
    var $HYGCONTROLS_SECONDARY;	
    
    /**
    * @var array;
    */
    var $HYGCONTROLS_SECONDARY_ARRAY;	
    
    function __construct($patient_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }          
        
        	$QueryText = "
            select 
				OCCLUSION,
                COMORBIDITIES,
				HYGIENE, 
				ANALYSIS, 
				VITASCALE
			from AMBCARD          
			where PATIENTID = $patient_id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{				
                $this->OCCLUSION = $row[0]; 
                $this->COMORBIDITIES = $row[1];
                $this->HYGIENE = $row[2];
                $this->ANALYSIS = $row[3];
                $this->VITASCALE = $row[4];	      
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);                       

            $this->setHygcontrols($this->getHygcontrolArray($patient_id, $trans));
                    
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            
    } 
    
    
        /**
     * PatientAmbcard::getHygcontrolArray()
     * гигиенические контроли пациента
     * 
     * @param int $patient_id
     * @param resource $trans
     * @return array
     */
    private static function getHygcontrolArray($patient_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }            

        	$QueryText = "
                        select 
        				HYGCONTROL.CONTROLDATE, 
        				DOCTORS.SHORTNAME, 
        				HYGCONTROL.ISCONTROL 
        		  from HYGCONTROL 
        				INNER JOIN DOCTORS ON (HYGCONTROL.DOCTORID = DOCTORS.ID)
        		  WHERE HYGCONTROL.PATIENTID=$patient_id
                  order by CONTROLDATE";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
            $HygcontrolArray=array();
            while ($row = ibase_fetch_row($result))
			{	
                $Hygcontrol=array();
                $Hygcontrol["CONTROLDATE"]=$row[0];			
                $Hygcontrol["DOCTOR"]=$row[1];
                $Hygcontrol["ISCONTROL"]=$row[2];
                $HygcontrolArray[]=$Hygcontrol;
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);                     
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $HygcontrolArray;
    }    
    
    private function setHygcontrols($hygcontrol_array)
    {
        $hc_sec='';
        foreach($hygcontrol_array as $hc)
        {
           switch($hc["ISCONTROL"])
                {
                    case 0:
                        $this->HYGCONTROL_PRIMARY=$hc["CONTROLDATE"].' - '.$hc["DOCTOR"];
                        break;
                    case 1;
                        $hc_sec.=($hc_sec==''?'':', ').$hc["CONTROLDATE"].' - '.$hc["DOCTOR"];
                        break;                  
                }              
        }
        $this->HYGCONTROLS_SECONDARY=$hc_sec;      
    } 
    
    public function generateFieldArray($field, $len, $lenstart)
    {
        $field_array=$field.'_ARRAY';
        $this->$field_array=space_split($this->$field, $len, $lenstart);
    }       
}


class ToothExam
{   
    /**
    * @var string
    */
    var $EXAMID;
    
    /**
    * @var string
    */
    var $TREATMENT_COURSE_ID; 
    
    /**
    * @var string
    */
    var $EXAM;
    
    /**
    * @var string
    */
    var $CHANNELEXAM;
    
    /**
    * @var string
    */
    var $EXAMDATE;
    
    /**
    * @var string
    */
    var $COMPLAINTS;
    
    /**
    * @var string
    */
    var $CHECKUP;
    
    /**
    * @var string
    */
    var $DISEASEHISTORY;
    
    /**
    * @var array
    */
    var $CHECKUP_ARRAY;
    
    /**
    * @var string
    */
    var $BITETYPE;
    
    /**
    * @var string
    */
    var $DOCTOR;
    
    /**
    * @var string
    */
    var $DIAGNOSIS;
    
    /**
    * @var Tooth[]
    */
    var $TEETH;
       
    /**
    * @var array
    */
    var $TOOTH_TEXT;
       
    /**
    * @var string
    */
    var $EXAM_AFTER; 
    
    /**
    * @var array
    */
    var $ANAMNESIS; 
    
    /**
    * @var array
    */
    var $STATUS; 
    
    /**
    * @var array
    */
    var $RECOMENDATIONS; 
    
    /**
    * @var array
    */
    var $EPICRISIS; 
    
    /**
    * @var array
    */
    var $DIAG; 
    
    /**
    * @var array
    */
    var $DIAG_F43; 
    
    /**
    * @var array
    */
    var $DIAG_DIARY; 
    
    /**
    * @var array
    */
    var $PROC_DIARY; 
    
    /**
    * @var array
    */
    var $PROC_PLAN; 
   
    
    function __construct($exam_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }          
        
        $QueryText = "
            select        tb.examid,
                            tb.exam,
                        tb.examdate,
                        tb.complaints,
                        tb.checkup,
                        tb.bitetype,
                        tb.treatmentcourse_fk,
                        d.shortname,
                        tb.diseasehistory,
                        (select first 1 ta.exam from toothexam ta where ta.treatmentcourse_fk=tb.treatmentcourse_fk and ta.afterflag=1  order  by ta.examdate),
                        tb.channelsexam
                        from toothexam tb
                        LEFT JOIN doctors d ON (d.ID = tb.DOCTORID)
                        where tb.examid = $exam_id";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
			$toothexam_array=array();
			while ($row = ibase_fetch_row($result))
			{	
		        $this->EXAMID = $row[0]; 
                $this->EXAM = $row[1];
                $this->EXAMDATE = $row[2]==null?"":date("d.m.Y", strtotime($row[2]));
                $this->COMPLAINTS = $row[3];
                $this->CHECKUP = $row[4];
                $this->BITETYPE = $row[5];
                $this->TREATMENT_COURSE_ID=$row[6];
                $this->DOCTOR=$row[7];
                $this->DISEASEHISTORY=$row[8];
                $this->TOOTH_TEXT=$this->BITETYPE==0?unserialize(toothtexts):unserialize(toothtextsmilk);          
                $diagnos_array=getSortTeethF43($this->EXAM, $this->BITETYPE, unserialize(toothid), unserialize(dentdisword));
                if(nonull($row[9])) 
                {
                    $this->EXAM_AFTER=$row[9];
                } 
                $this->CHANNELEXAM = text_blob_encode($row[10]);
                $teeth=array();
                for($i=0;$i<32;$i++)
                { 
                    $tooth=new Tooth($i,$this->BITETYPE,$this->EXAM,$this->CHANNELEXAM);
                    if(nonull($this->EXAM_AFTER))
                    {
                      $toothA=new Tooth($i,$this->BITETYPE,$this->EXAM_AFTER);
                      if($toothA->CROWN_STR!=$tooth->CROWN_STR)
                      {
                        $tooth->CROWN_STR.="/".$toothA->CROWN_STR;
                      } 
                      if($toothA->GUM_STR!=$tooth->GUM_STR)
                      {
                        $tooth->GUM_STR.="/".$toothA->GUM_STR;
                      }  
                    }
                    
                    $teeth[]=$tooth;
                }
                $this->TEETH=$teeth;
                
                $diagnosis='';
                foreach($diagnos_array as $diagnos)
                {
                    $diagnosis.=($diagnosis==''?'':', ').$diagnos["ID"].' - '.$diagnos["DIAG"];
                }
                $this->DIAGNOSIS=$diagnosis;  
                         	      
			}
			ibase_free_query($query);
			ibase_free_result($result);                       

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
//             file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($this->PLAN, true));

    }
    
    
    /**
     * ToothExam::getDiagnosTreatment()
     * по данному курсу лечения возвращает массив c ключами
     * [0]-диагнозы, [1]-диагнозы для дневника, [2] - процедуры для дневника, [3] - процедуры для плана
     * 'DATE' - SIGNDATE, 'TEXT' - ADDITIONTXT (+ подпись доктора)
     * 
     * @param int $treatment_course_id
     * @param string $patient_id
     * @param string $start_date
     * @param string $end_date                    
     * @param boolean $includePriceInReport
     * @param boolean $includeMaterialsInReport
     * @param boolean $includeShifrProcedures043Flag
     * @param boolean $includeCountProcedures043Flag
     * @param resource $trans
     * @return array
     */
     
    public static function getDiagnosTreatment($treatment_course_id=null, $patient_id=null, $start_date=null, $end_date=null, $includePriceInReport=false, $includeMaterialsInReport=false, $includeDoctorSignatureInReport=false, $includeNoncompleteProcedures043Flag=false, $includeShifrProcedures043Flag=false, $includeCountProcedures043Flag=false, $trans=null)
	{		
            if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            } 

//			$QueryText = "
//    			select
//    				DIAGNOS2.ID,
//    				DOCTORS.SHORTNAME,
//    				DIAGNOS2.DIAGNOSDATE,
//    				DIAGNOS2.STACKNUMBER,	
//    				PROTOCOLS.NAME						
//    			from DIAGNOS2
//    				left join PROTOCOLS on (DIAGNOS2.PROTOCOL_FK = PROTOCOLS.ID)
//    				left join DOCTORS on (DOCTORS.ID = DIAGNOS2.DOCTOR)
//                    left join TREATMENTCOURSE on (TREATMENTCOURSE.ID = DIAGNOS2.TREATMENTCOURSE)"; 
            $QueryText = "
                			select
                				DIAGNOS2.ID,
                				DOCTORS.SHORTNAME,
                				(select first 1 examdate from toothexam where TREATMENTCOURSE_FK=DIAGNOS2.TREATMENTCOURSE),
                				DIAGNOS2.STACKNUMBER,	
                				PROTOCOLS.NAME						
                			from DIAGNOS2
                				left join PROTOCOLS on (DIAGNOS2.PROTOCOL_FK = PROTOCOLS.ID)
                				left join DOCTORS on (DOCTORS.ID = DIAGNOS2.DOCTOR)
                                left join TREATMENTCOURSE on (TREATMENTCOURSE.ID = DIAGNOS2.TREATMENTCOURSE)"; 
                $where="";
                if(nonull($treatment_course_id))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  else
                  {
                    $where.=" where";
                  }
                  $where.=" TREATMENTCOURSE.ID = $treatment_course_id";  
                }
                if(nonull($patient_id))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  else
                  {
                    $where.=" where";
                  }
                  $where.=" TREATMENTCOURSE.PATIENT_FK = $patient_id";  
                }
    			$QueryText.=$where;
                $QueryText.=" order by STACKNUMBER";	
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
            $diag_array=array();
            $diag_diary_array=array();
            $proc_diary_array=array();
            $proc_plan_array=array();
					
			while ($row = ibase_fetch_row($result))
			{       
                    $objects = getTreatmentObjects($row[0], $trans);	
                    if($row[3]!=-1)
                    {
                        if(((nonull($start_date)==false)||strtotime($row[2])>=strtotime($start_date))&&((nonull($end_date)==false)||strtotime($row[2])<=strtotime($end_date)))
                        {
                            $text=$objects.($objects==''?'':' - ').$row[4];
                            $diag_array[]=array("DATE"=>($row[2]==null?"":date("d.m.Y", strtotime($row[2]))), "TEXT"=>$text);
                            if($includeDoctorSignatureInReport)
                            {
                              $text.=  " __________".$row[1];
                            }
                            $diag_diary_array[]=array("DATE"=>($row[2]==null?"":date("d.m.Y", strtotime($row[2]))), "TEXT"=>$text);   
                        }

                    }
						
					
					$procedures = ToothExam::getTreatmentProcedures($row[0], $start_date, $end_date, $includeMaterialsInReport, $includeShifrProcedures043Flag, $includeCountProcedures043Flag, $trans);
					
                    $proc_diary=array();
                    $proc_plan=array();
                    foreach($procedures as $proc)
                    {                                              
                        if($proc->NAMEFORPLAN!="")
                         {
                           $proc_plan[]=$objects.($objects==''?'':' - ').$proc->NAMEFORPLAN;
                         }
                          if($proc->NAME!="")
                          {
                              $text=$objects.($objects==''?'':' - ').
                                    $proc->NAME.($includePriceInReport?"($proc->PRICE)":"").
                                    ($includeMaterialsInReport?(($proc->FARMS==""?"":"\r\n").$proc->FARMS):"").
                                    ($includeDoctorSignatureInReport?(" __________".$proc->DOCTORSHORTNAME):"");
                              if($includeNoncompleteProcedures043Flag||$proc->DOCTORSHORTNAME!='')
                              {
                                $proc_diary[]=array("DATE"=>($proc->DATECLOSE), "TEXT"=>$text);
                              }
                          }
                    }
                    
                    if(count($proc_diary)>0)
                    {
                        $proc_diary_array=array_merge($proc_diary_array,$proc_diary);
                    }
                    
                    if(count($proc_plan)>0)
                    {
                        $proc_plan_array=array_merge($proc_plan_array,$proc_plan);
                    }
			}
		
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return array($diag_array, $diag_diary_array, $proc_diary_array, $proc_plan_array);
	}
    
    
         /**
     * ToothExam::getTreatmentProcedures()
     * возвращает массив процедур для данного диагноза
     * 
     * @param int $diagnos_id
     * @param boolean $includeMaterialsInReport
     * @param boolean $includeShifrProcedures043Flag
     * @param boolean $includeCountProcedures043Flag
     * @param resource $trans
     * @return array
     */
     
   	public static function getTreatmentProcedures($diagnos_id,  $start_date=null, $end_date=null, $includeMaterialsInReport=false, $includeShifrProcedures043Flag=false, $includeCountProcedures043Flag=false, $trans=null)
	{		
            if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            } 
        
			$QueryTreatmentProcText = "
			select
				TREATMENTPROC.ID, 		
				TREATMENTPROC.NAME, 
				TREATMENTPROC.NAMEFORPLAN,					
				TREATMENTPROC.PRICE, 			
				DOCTORS.SHORTNAME,
				TREATMENTPROC.DATECLOSE,
                TREATMENTPROC.PROC_COUNT,
                TREATMENTPROC.SHIFR
                		
			from TREATMENTPROC 				
				left join DOCTORS	on (TREATMENTPROC.DOCTOR = DOCTORS.ID)					
			where TREATMENTPROC.DIAGNOS2 = $diagnos_id";		
            if(nonull($start_date))
            {
              $QueryTreatmentProcText.=" AND (TREATMENTPROC.DATECLOSE >= '$start_date')";  
            }
            if(nonull($end_date))
            {
              $QueryTreatmentProcText.=" AND (TREATMENTPROC.DATECLOSE <= '$end_date')";  
            }
                $QueryTreatmentProcText.=" order by TREATMENTPROC.STACKNUMBER, TREATMENTPROC.DATECLOSE";	
			$TreatmentProcQuery = ibase_prepare($trans, $QueryTreatmentProcText);
			$TreatmentProcResult = ibase_execute($TreatmentProcQuery);

			$returnObj = array();

			while ($procRow = ibase_fetch_row($TreatmentProcResult))
			{
				$obj = new stdclass();
				
				$obj->ID = $procRow[0];				
				$obj->NAME = $procRow[1]==null?'':($includeCountProcedures043Flag?($procRow[6].'x '):'').($includeShifrProcedures043Flag?$procRow[7].' ':'').$procRow[1];
				$obj->NAMEFORPLAN = $procRow[2]==null?'':($includeCountProcedures043Flag?($procRow[6].'x '):'').($includeShifrProcedures043Flag?$procRow[7].' ':'').$procRow[2];			
				$obj->PRICE = $procRow[3]*$procRow[6];
				$obj->DOCTORSHORTNAME = $procRow[4];		
				$obj->DATECLOSE = $procRow[5]!=null?date("d.m.Y", strtotime($procRow[5])):"";
            
                $farms="";
                if($includeMaterialsInReport)
                {
    				$FarmQueryText = "
    				select					
    					FARM.NAME, 					
    					TREATMENTPROCFARM.QUANTITY, 
    					UNITOFMEASURE.NAME						
    				from TREATMENTPROCFARM 					
    					left join FARM on (TREATMENTPROCFARM.FARM = FARM.ID)					
    					left join UNITOFMEASURE	on (FARM.ID = UNITOFMEASURE.FARMID)							
    				where TREATMENTPROCFARM.TREATMENTPROC = $obj->ID and UNITOFMEASURE.isbase = 1";
    
    				$FarmQuery = ibase_prepare($trans, $FarmQueryText);
    				$FarmResult = ibase_execute($FarmQuery);
    						
                    while ($farmRow = ibase_fetch_row($FarmResult))
    				{             
                        $farms.=($farms==''?'':', ').
                                $farmRow[0].' ('.$farmRow[1]*$procRow[6].' '.$farmRow[2].')';	
                    }	
    				
    				ibase_free_query($FarmQuery);
    				ibase_free_result($FarmResult);	
    					
                }			

				
                $obj->FARMS = $farms;
				$returnObj[] = $obj;
			}
			
			return $returnObj;
            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
	}
    	
     /**
     * ToothExam::getAdditions()
     * по данному курсу лечения возвращает массив c ключами
     * [0]-анамнез, [1]-статус, [2] - рекомендации, [3] - эпикриз
     * $includeDoctorSignatureInReport - включать ли подпись врача
     * 'DATE' - SIGNDATE, 'TEXT' - ADDITIONTXT (+ подпись доктора)
     * 
     * @param int $treatment_course_id
     * @param string $patient_id
     * @param string $start_date
     * @param string $end_date          
     * @param boolean $includeDoctorSignatureInReport
     * @param resource $trans
     * @return array
     */
     
    public static function getAdditions($treatment_course_id=null, $patient_id=null, $start_date=null, $end_date=null, $includePriceInReport=false, $includeDoctorSignatureInReport=false, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
            $QueryText = "
					select
							TREATMENTADDITIONS.ADDITIONTYPE,						
							TREATMENTADDITIONS.ADDITIONTXT,
							TREATMENTADDITIONS.SIGNDATE,
							DOCTORS.SHORTNAME						
					from TREATMENTADDITIONS				
						left join DOCTORS on (DOCTORS.ID = TREATMENTADDITIONS.DOCTOR)
                        left join TREATMENTCOURSE on (TREATMENTCOURSE.ID = TREATMENTADDITIONS.TREATMENTCOURSE)
					where (TREATMENTADDITIONS.SIGNFLAG = 1) and ";
                
                $where="";
                if(nonull($treatment_course_id))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  $where.=" TREATMENTCOURSE.ID = $treatment_course_id";  
                }
                if(nonull($patient_id))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  $where.=" TREATMENTCOURSE.PATIENT_FK = $patient_id";  
                }
                if(nonull($start_date))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  $where.=" TREATMENTADDITIONS.SIGNDATE >= '$start_date'";  
                }
                if(nonull($end_date))
                {
                  if($where!="")
                  {
                    $where.=" AND";
                  }
                  $where.=" TREATMENTADDITIONS.SIGNDATE <= '$end_date'";  
                }
    			$QueryText.=$where;
                $QueryText.=" order by TREATMENTADDITIONS.ADDITIONTYPE";
                 
                
                    $query = ibase_prepare($trans, $QueryText);
					$result = ibase_execute($query);
                    
					$treatment_additions_array=array(array(),array(),array(),array());
					while ($row = ibase_fetch_row($result))
					{						
					$treatment_additions_array[$row[0]][]= array
                                                            (
                                                            "DATE"=>($row[2]==null?"":date("d.m.Y", strtotime($row[2]))), 
                                                            "TEXT"=>text_blob_encode($row[1]).($includeDoctorSignatureInReport?" __________$row[3]":"")
                                                            ); 					
					}
  			ibase_free_query($query);
			ibase_free_result($result);                       
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }          
            return $treatment_additions_array;
                       
    } 
    
     /**
     * ToothExam::getPlan()
     * 
     * @param int $treatment_course_id
     * @param string $patient_id
     * @param string $start_date
     * @param string $end_date  
     * @param boolean $includePriceInReport        
     * @param resource $trans
     * @return array
     */
     
    public static function getPlan($treatment_course_id=null, $patient_id=null, $start_date=null, $end_date=null, $includePriceInReport=false, $trans) 
    {
        $plans=array();
        
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }     
            $QueryText = "
            select	    TREATMENTPLAN,
                        EXAMPLAN,
                        EXAMDATE
                        from TOOTHEXAM		
					where (AFTERFLAG = 0";
                
                if(nonull($treatment_course_id))
                {
                  $QueryText.=" AND TREATMENTCOURSE_FK = $treatment_course_id";  
                }
                if(nonull($patient_id))
                {
                  $QueryText.=" AND ID = $patient_id";  
                }
                if(nonull($start_date))
                {
                  $QueryText.=" AND EXAMDATE >= '$start_date'";  
                }
                if(nonull($end_date))
                {
                  $QueryText.=" AND EXAMDATE <= '$end_date'";  
                }
                $QueryText.=") order by EXAMID";
                 
                
                    $query = ibase_prepare($trans, $QueryText);
					$result = ibase_execute($query);
                    
					while ($row = ibase_fetch_row($result))
                	{	
                	   $plan['TEXT']=array();					
                		if(nonull($row[0]))
                		{
                			$treatmentplan=explode("\n",$row[0]);
                			foreach($treatmentplan as $key=>$tp)
                			{
                			  $plan['TEXT'][$key]['TREATMENTPLAN']= $tp; 
                			}                   
                		}
                		if(nonull($row[1]))
                		{
                			$examplan=explode("\n",$row[1]);
                			foreach($examplan as $key=>$ep)
                			{
                			  $plan['TEXT'][$key]['EXAMPLAN']= $ep; 
                			}
                		}
                		foreach($plan['TEXT'] as $key=>$pl)
                		{
                				$uns=true;
                				if(key_exists('EXAMPLAN',$pl))
                				{
                				if(nonull($pl['EXAMPLAN']))
                				{
                					$uns=false;
                				}
                				}
                				if(key_exists('TREATMENTPLAN',$pl))
                				{
                				if(nonull($pl['TREATMENTPLAN']))
                				{
                					$uns=false;
                				}
                				}
                				if($uns)
                				{
                				unset($plan['TEXT'][$key]);
                				}
                		}
                        if(count($plan['TEXT'])>0)
                        {
                            $plan['DATE']=$row[2]!=null?date("d.m.Y", strtotime($row[2])):"";
                            $plans[]=$plan;
                        }
                	}
  			ibase_free_query($query);
			ibase_free_result($result);                       
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }      
            return $plans;
                       
    } 
}
?>

<?php

require_once "Connection.php";
require_once "Utils.php";

class TreatmentPlanDictionaryModuleDictionaryObject
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;
}

class TreatmentPlanDictionaryModule
{		
    
   /**
   * @param TreatmentPlanDictionaryModuleDictionaryObject $p1
   * @return void	    
    */    
	public function registertypes($p1) 
	{
	} 
    
    /** 
 	*  @return TreatmentPlanDictionaryModuleDictionaryObject[]
	*/ 
    public function getHealth() 
    { 
   	    $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from HEALTH order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new TreatmentPlanDictionaryModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param TreatmentPlanDictionaryModuleDictionaryObject $hvo
    * @return boolean
 	*/
    public function addHealth ($hvo) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into HEALTH (ID, NUMBER, DESCRIPTION) values (null,$hvo->NUMBER, '".safequery($hvo->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param TreatmentPlanDictionaryModuleDictionaryObject $hvo
    * @return boolean
 	*/
    public function updateHealth ($hvo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update HEALTH set DESCRIPTION ='".safequery($hvo->DESCRIPTION)."' where ID = '".safequery($hvo->ID)."' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteHealth($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Health where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param TreatmentPlanDictionaryModuleDictionaryObject $hvo
    * @return boolean
 	*/
    public function updateHealthNumber ($hvo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update HEALTH set NUMBER ='".safequery($hvo->NUMBER)."' where ID = '".safequery($hvo->ID)."' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }    
  }
?>
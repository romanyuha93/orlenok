<?php

require_once "Connection.php";
require_once "Utils.php";

class TeethExamDictionariesModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;
    
     /**
    * @var string
    */
    var $SPECIALITY;
       
    /**
    * @var int
    */
    var $TYPE;
}

 
class TeethExamDictionariesModule
{	
    /**  
    * @param TeethExamDictionariesModuleDictionaryObject $p1
    */    
	public function registertypes($p1) 
	{
	}    
    
    /** 
     * @param int $type
     * @param int[] $specialities
 	*  @return TeethExamDictionariesModuleDictionaryObject[]
	*/ 
    public function getTeethExamDictionary($type, $specialities) //sqlite
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION, TYPE, SPECIALITY from TeethExamDictionaries where TYPE=".$type;
        if(nonull($specialities)&&count($specialities)>0)
        {
           $QuerySpec="";
            foreach($specialities as $spec)
          {
            	if($QuerySpec!='')
                {
                    $QuerySpec.=' or ';
                }
                $QuerySpec .= "('|'||SPECIALITY||'|' like '%|$spec->id|%' or SPECIALITY is null or SPECIALITY='')"; 
          }
            $QueryText.=" and (".$QuerySpec.")";
        }
       
        $QueryText.=" order by NUMBER";
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new TeethExamDictionariesModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
            $obj->TYPE = $row[3];
            $obj->SPECIALITY = $row[4];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param TeethExamDictionariesModuleDictionaryObject $ovo 
     *@param int $type
    * @return boolean
 	*/
    public function addTeethExamDictionary($ovo, $type) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into TeethExamDictionaries (ID, NUMBER, DESCRIPTION, TYPE, SPECIALITY) values (null,$ovo->NUMBER, '".safequery($ovo->DESCRIPTION)."', $type, ".nullcheck($ovo->SPECIALITY,true).")";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param TeethExamDictionariesModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updateTeethExamDictionary ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update TeethExamDictionaries
					 set DESCRIPTION ='".safequery($ovo->DESCRIPTION)."',
                     SPECIALITY =".nullcheck($ovo->SPECIALITY,true)."
					     where ID = '$ovo->ID' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteTeethExamDictionary($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from TeethExamDictionaries where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param TeethExamDictionariesModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updateTeethExamDictionaryNumber ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update TeethExamDictionaries set NUMBER ='$ovo->NUMBER' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;	
    }      
  }
?>
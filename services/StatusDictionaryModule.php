<?php

require_once "Connection.php";
require_once "Utils.php";

class StatusDictionaryModuleDictionaryObject
{
    /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;
    
    /**
    * @var string
    */
    var $DESCRIPTION;
     
}
 
class StatusDictionaryModule
{		
    
      /**
   * @param StatusDictionaryModuleDictionaryObject $p1
   * @return void	    
  */    
	public function registertypes($p1) 
	{
	} 
    
    /** 
 	*  @return StatusDictionaryModuleDictionaryObject[]
	*/ 
    public function getStatus() 
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from Status order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new StatusDictionaryModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param StatusDictionaryModuleDictionaryObject $ovo, 
    * @return boolean
 	*/
    public function addStatus ($ovo) 
    {
		$connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into Status (ID, NUMBER, DESCRIPTION) values (null, $ovo->NUMBER, '".safequery($ovo->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param StatusDictionaryModuleDictionaryObject $ovo, 
    * @return boolean
 	*/
    public function updateStatus ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Status set DESCRIPTION ='".safequery($ovo->DESCRIPTION)."' where ID = '$ovo->ID' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param string[] $Arguments, 
    * @return boolean
 	*/ 		 		
    public function deleteStatus($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Status where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param StatusDictionaryModuleDictionaryObject $ovo, 
    * @return boolean
 	*/
    public function updateStatusNumber ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Status set NUMBER ='$ovo->NUMBER' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;	
    }      
  }
?>
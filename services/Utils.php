<?php
require_once "Settings.php";

class tflocale
{   
//UA      

    public static $invoice='Рахунок';
    public static $invoiceShort='Рах.';
    public static $tooth='Зуб';
    public static $toothSmall='зуб';
    public static $toothMulti='и';
    public static $jaw='щелепа';
    public static $root='корінь';
    public static $gum='ясна';
    public static $upJaw='верхня щелепа';
    public static $downJaw='нижня щелепа';
    public static $state='область';
    public static $region='район';
    public static $medcard='Медична картка';
    public static $from='від';
    public static $doctor='Лікар';
    public static $cheif='Завідувач відділенням';
    public static $generatedByToothFairy='Згенеровано програмою "Зубна фея"';
    public static $MonthNames=array("Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень");
    public static	$strNumbersM = array('', 'одна', 'дві', 'три', 'чотири', 'п\'ять', 'шість', 'сім',
			'вісім', 'дев\'ять', 'десять', 'одинадцять', 'дванадцять', 'тринадцять',
			'чотирнадцять', 'п\'ятнадцять', 'шістнадцять', 'сімнадцять', 'вісімнадцять',
			'дев\'ятнадцять', 'двадцять', 30 => 'тридцять', 40 => 'сорок', 50 => 'п\'ятдесят',
			60 => 'шістдесят', 70 => 'сімдесят', 80 => 'вісімдесят', 90 => 'дев\'яносто',
			100 => 'сто', 200 => 'двісті', 300 => 'триста', 400 => 'чотириста',
			500 => 'п\'ятсот', 600 => 'шістсот', 700 => 'сімсот', 800 => 'вісімсот',
			900 => 'дев\'ятсот');
	 public static $strNumbersF = array('', 'одна', 'дві');
	 public static $moneyUnitsFull = array(
            array('копійка', 'копійки', 'копійок'),
			array('гривня', 'гривні', 'гривень'),
			array('тисяча', 'тисячі', 'тисяч'),
			array('мільйон', 'мільйона', 'мільйонів'),
			array('мільярд', 'мільярда', 'мільярдів'),
			array('трильйон', 'трильйона', 'трильйонів'),);
	 public static $moneyUnitsShort = array(
			array('коп.', 'коп.', 'коп.'),
			array('гривня', 'гривні', 'гривень'),
			array('тисяча', 'тисячі', 'тисяч'),
			array('мільйон', 'мільйона', 'мільйонів'),
			array('мільярд', 'мільярда', 'мільярдів'),
			array('трильйон', 'трильйона', 'трильйонів'),);



//RU

//    public static $invoice='Счет';
//    public static $invoiceShort='Счет';
//    public static $tooth='Зуб';
//    public static $toothSmall='зуб';
//    public static $toothMulti='ы';
//    public static $jaw='челюсть';
//    public static $root='корень';
//    public static $gum='десна';
//    public static $upJaw='верхняя челюсть';
//    public static $downJaw='нижняя челюсть';
//    public static $state='область';
//    public static $region='район';
//    public static $medcard='Медицинская карта';
//    public static $from='от';
//    public static $doctor='Врач';
//    public static $cheif='Заведующий отделением';
//    public static $generatedByToothFairy='Сгенерировано программой "Зубная фея"';
//    public static $MonthNames=array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
//    public static $strNumbersM = array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь',
//                            			'восем', 'девять', 'десять', 'одинадцать', 'двенадцать', 'тринадцать',
//                            			'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать',
//                            			'девятнадцать', 'двадцать', 30 => 'тридцать', 40 => 'сорок', 50 => 'пятьдесят',
//                            			60 => 'шестьдесят', 70 => 'семьдесят', 80 => 'восемдесят', 90 => 'девяносто',
//                            			100 => 'сто', 200 => 'двести', 300 => 'триста', 400 => 'четыреста',
//                            			500 => 'пятьсот', 600 => 'шестьсот', 700 => 'семьсот', 800 => 'восемьсот',
//                            			900 => 'девятьсот');
//  	 public static $strNumbersF = array('', 'одна', 'две');
//	 public static $moneyUnitsFull = array(            
//                                        array('копейка', 'копейки', 'копеек'),
//                            			array('рубль', 'рубли', 'рублей'),
//                            			array('тысяча', 'тысячи', 'тысяч'),
//                            			array('миллион', 'миллиона', 'миллионов'),
//                            			array('миллиард', 'миллиарда', 'миллиардов'),
//                            			array('триллион', 'триллиона', 'триллионов'),);
//	 public static $moneyUnitsShort = array(
//                                        array('коп.', 'коп.', 'коп.'),
//                            			array('рубль', 'рубли', 'рублей'),
//                            			array('тысяча', 'тысячи', 'тысяч'),
//                            			array('миллион', 'миллиона', 'миллионов'),
//                            			array('миллиард', 'миллиарда', 'миллиардов'),
//                            			array('триллион', 'триллиона', 'триллионов'),);

}




/**
 * safequery()
 * 
 * @param string $str 
 * @return string 
 */
 
function safequery($str)
{
    return str_replace("'", "''", $str);
}


/**
 * format() 
 * @param string $str 
 * @param string $str
 * @return string
 */
function format() {
    $args = func_get_args();
    if (count($args) == 0) {
        return;
    }
    if (count($args) == 1) {
        return $args[0];
    }
    $str = array_shift($args);
    $str = preg_replace_callback('/\\{(0|[1-9]\\d*)\\}/', create_function('$match', '$args = '.var_export($args, true).'; return isset($args[$match[1]]) ? $args[$match[1]] : $match[0];'), $str);
    return $str;
}

/**
 * format_for_query()
 * 
 * @param string $templ
 * @param string $params
 * @return string
 */
function format_for_query()
{
    $args = func_get_args();
    if (count($args) == 0) {
        return;
    }
    if (count($args) == 1) {
        return $args[0];
    }
    $str = array_shift($args);
    foreach($args as $i => $a)
    {
        if (is_string($a))
        {
            $args[$i] = safequery($a);
        }
    }
    $str = preg_replace_callback('/\\{(0|[1-9]\\d*)\\}/', create_function('$match', '$args = '.var_export($args, true).'; return isset($args[$match[1]]) ? $args[$match[1]] : $match[0];'), $str);
    return $str;
}


function timeMeasure()
{
    list($msec, $sec) = explode(chr(32), microtime());
    return ($sec+$msec);
}

function dateDeclineOnCases($date_str){
			$date_str = str_replace(array('ень','ий','пад'), 
				array('ня','ого','пада'),	$date_str);
				return $date_str;
}

function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '',  'ы' => 'y',   'ъ' => '',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya', 
			'ґ' => 'g', 'Ґ' => 'G'
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}
    
function number_to_roman($value)
{
  if($value<0) return "";
  if(!$value) return "0";
  $thousands=(int)($value/1000);
  $value-=$thousands*1000;
  $result=str_repeat("M",$thousands);
  $table=array(
    900=>"CM",500=>"D",400=>"CD",100=>"C",
    90=>"XC",50=>"L",40=>"XL",10=>"X",
    9=>"IX",5=>"V",4=>"IV",1=>"I");
  while($value) {
    foreach($table as $part=>$fragment) if($part<=$value) break;
    $amount=(int)($value/$part);
    $value-=$part*$amount;
    $result.=str_repeat($fragment,$amount);
  }
  return $result;
}

function ua_date($param, $time=0) {
	if(intval($time)==0)$time=time();
	$MonthNames=tflocale::$MonthNames;
	if(strpos($param,'M')===false) return date($param, $time);
		else return date(str_replace('M',$MonthNames[date('n',$time)-1],$param), $time);
}



 define('M2S_KOPS_DIGITS',	0x01);	// digital copecks
 define('M2S_KOPS_MANDATORY',	0x02);	// mandatory copecks
 define('M2S_KOPS_SHORT',	0x04);	// shorten copecks



	function money2str_ua($money, $options = 0)
	{
		$money = preg_replace('/[\,\-\=]/', '.', $money);
	 
		$numbers_m = tflocale::$strNumbersM;
	 
		$numbers_f = tflocale::$strNumbersF;
	 
		$units_ru = $options  /*& M2S_KOPS_SHORT*/? tflocale::$moneyUnitsShort:tflocale::$moneyUnitsFull;
	 
		$ret = '';
	 
		// enumerating digit groups from left to right, from trillions to copecks
		// $i == 0 means we deal with copecks, $i == 1 for roubles,
		// $i == 2 for thousands etc.
		for ($i = sizeof($units_ru) - 1; $i >= 0; $i--) {
	 
			// each group contais 3 digits, except copecks, containing of 2 digits
			$grp = ($i != 0) ? dec_digits_group($money, $i - 1, 3) :
				dec_digits_group($money, -1, 2);
	 
			// process the group if not empty
			if ($grp != 0) {
	 
				// digital copecks
				if ($i == 0 && ($options & 0x01 /*& M2S_KOPS_DIGITS*/)) {
					$ret .= sprintf('%02d', $grp). ' ';
					$dig = $grp;
	 
				// the main case
				} else for ($j = 2; $j >= 0; $j--) {
					$dig = dec_digits_group($grp, $j);
					if ($dig != 0) {
	 
						// 10 to 19 is a special case
						if ($j == 1 && $dig == 1) {
							$dig = dec_digits_group($grp, 0, 2);
							$ret .= $numbers_m[$dig]. ' ';
							break;
						}
	 
						// thousands and copecks are Feminine gender in Russian
						elseif (($i == 2 || $i == 0) && $j == 0 && ($dig == 1 || $dig == 2))
							$ret .= $numbers_f[$dig]. ' ';
	 
						// the main case
						else $ret .= $numbers_m[(int) ($dig * pow(10, $j))]. ' ';
					}
				}
				$ret .= $units_ru[$i][sk_plural_form($dig)]. ' ';
			}
	 
			// roubles should be named in case of empty roubles group too
			elseif ($i == 1 && $ret != '')
				$ret .= $units_ru[1][2]. ' ';
	 
			// mandatory copecks
			elseif ($i == 0 && ($options /*& M2S_KOPS_MANDATORY*/))
				$ret .= (($options & 0x01 /*& M2S_KOPS_DIGITS*/) ? '00' : 'нуль').
					' '. $units_ru[0][2];
		}
	 
		return trim($ret);
	}
	 
	// service function to select the group of digits
	function dec_digits_group($number, $power, $digits = 1) {
		return (int) bcmod(bcdiv($number, bcpow(10, $power * $digits, 8)),
			bcpow(10, $digits, 8));
	}
		 
	// service function to get plural form for the number
	function sk_plural_form($d) {
		$d = $d % 100;
		if ($d > 20) $d = $d % 10;
		if ($d == 1) return 0;
		elseif ($d > 0 && $d < 5) return 1;
		else return 2;
	}
	
	
	function deleteFileUtils($file_name)
	{
		$deleted_success = true;
		try{
			$deleted_success = @unlink(dirname(__FILE__).'/docs/'.$file_name);
		}catch(Exception $exc){ $deleted_success = false; }
		
		return $deleted_success;
	}
/* old
	 function str_size($str, $font='timesbd', $font_size=8, $is_full_path=false)
      {
        
        $font_path=$is_full_path?$font:dirname(__FILE__).'/tbs/fonts/'.$font.'.ttf';
        $bbox = imagettfbbox($font_size, 0, $font_path, $str);
        return abs($bbox[2]-$bbox[0]); 
        
      } 
  
      function space_split($str, $len, $lenstart, $font, $font_size, $is_full_path)
        {   
            if($lenstart==0) $lenstart==$len;
            $result=array();
            $sp_array=explode(' ',$str);
            $cur_str='';
            foreach($sp_array as $sp)
            {   if(str_size($cur_str.' '.$sp, $font, $font_size, $is_full_path)<((count($result)>0)?$len:$lenstart))
                {
                    $cur_str.=' '.$sp;
                }
                else
                {
                    $result[]=$cur_str;
                    $cur_str=$sp;
                }
            }
            $result[]=$cur_str;
            return $result;
        }
old*/
function mb_explode($delimiter, $string, $limit = -1, $encoding = 'UTF-8') {
        if(!is_array($delimiter)) {
            $delimiter = array($delimiter);
        }
        if(strtolower($encoding) === 'auto') {
            $encoding = mb_internal_encoding();
        }
        if(is_array($string) || $string instanceof Traversable) {
            $result = array();
            foreach($string as $key => $val) {
                $result[$key] = mb_explode($delimiter, $val, $limit, $encoding);
            }
            return $result;
        }
 
        $result = array();
        $currentpos = 0;
        $string_length = mb_strlen($string, $encoding);
        while($limit < 0 || count($result) < $limit) {
            $minpos = $string_length;
            $delim_index = null;
            foreach($delimiter as $index => $delim) {
                if(($findpos = mb_strpos($string, $delim, $currentpos, $encoding)) !== false) {
                    if($findpos < $minpos) {
                        $minpos = $findpos;
                        $delim_index = $index;
                    }
                }
            }
            $result[] = mb_substr($string, $currentpos, $minpos - $currentpos, $encoding);
            if($delim_index === null) {
                break;
            }
            $currentpos = $minpos + mb_strlen($delimiter[$delim_index], $encoding);
        }
        return $result;
    }
function space_split($str, $len, $lenstart=null)
{
     if($lenstart==null) $lenstart=$len;
     if(mb_strlen($str,'UTF-8')<=$lenstart) return array($str);
     $result=array();
     $sp_array=mb_explode(' ',$str);
     $cur_str='';
     foreach($sp_array as $sp)
        {   if(mb_strlen($cur_str.' '.$sp,'UTF-8')<=(count($result)==0?$lenstart:$len))
            {
                $cur_str.=' '.$sp;
            }
            else
            {
                $result[]=$cur_str;
                $cur_str=$sp;
            }
        }
     $result[]=mb_substr($cur_str,0,$len);
     return $result;
}
        
    function blob_encode($data)
    {
        $Blob = null;
        $blob_info = ibase_blob_info($data);
        if ($blob_info[0]!=0){
            $blob_hndl = ibase_blob_open($data);
            $Blob = ibase_blob_get($blob_hndl, $blob_info[0]);
            ibase_blob_close($blob_hndl);
        }        
        return base64_encode($Blob);
    } 
        
    function text_blob_encode($data)
    {
        $txtBlob=null;
        $blob_info = ibase_blob_info($data);
                if ($blob_info[0]!=0)
                {
                    $blob_hndl = ibase_blob_open($data);
                    $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                    ibase_blob_close($blob_hndl);
                }
        if($txtBlob == null)
        {
            $txtBlob = '';
        }
         return $txtBlob;      
    }  

                 
    function cmp($a, $b) 
    {
        if ($a["DIAG"]!=$b["DIAG"]) 
        {
            return strcmp($a["DIAG"], $b["DIAG"]);
        }
        else
        {
            return strcmp($a["ID"], $b["ID"]);
        }
      } 
      
    function cmp2($a, $b) 
    {
        if ($a["ID"]!=$b["ID"]) 
        {
            return strcmp($a["ID"], $b["ID"]);
        }
        else
        {
            return strcmp($a["DIAG"], $b["DIAG"]);
        }
      } 
      
      function cmp_f43($a, $b) 
      {
        return intcmp($a->f43, $b->f43);
      }  
      function cmp_tooth($a, $b) 
      {
        return intcmp($a->TOOTH_MIN, $b->TOOTH_MIN);
      }  
      function cmp_d2_diagnosdate($a, $b) 
      {
        if($a->DIAGNOSDATE != null && $a->DIAGNOSDATE != '')
        {
            $a_date = new DateTime($a->DIAGNOSDATE); 
        }
        else
        {
            //$a_date = new DateTime("01.01.1980 00:00:00");
            $a_date = new DateTime($a->EXAMDATE);
        }
        if($b->DIAGNOSDATE != null && $b->DIAGNOSDATE != '')
        {
            $b_date = new DateTime($b->DIAGNOSDATE); 
        }
        else
        {
            //$b_date = new DateTime("01.01.1980 00:00:00");
            $b_date = new DateTime($b->EXAMDATE);
        }
        
        if( ($a->DIAGNOSDATE == null || $a->DIAGNOSDATE == '') && ($b->DIAGNOSDATE != null && $b->DIAGNOSDATE != '') )
        {
            return -1;
        }
        if( ($b->DIAGNOSDATE == null || $b->DIAGNOSDATE == '') && ($a->DIAGNOSDATE != null && $a->DIAGNOSDATE != '') )
        {
            return 1;
        }
        
        if($a_date == $b_date)
        {
            return 0;
        }
        else if($a_date < $b_date)
        {
            return 1;
        }
        else
        {
            return -1;
        }
      }   
      
    function cmp_number($a, $b) 
    {
        $arA=explode('.',$a->number);
		$arB=explode('.',$b->number);
        $i=0;
		while($arA[$i]===$arB[$i]) 
		{	
            $i++;
			if(($i+1)>count($arA)||($i+1)>count($arB)) return count($arA)==count($arB)?0:(count($arA)>count($arB)?1:-1);
		}
		
		if($arA[$i]>$arB[$i])
		{
			return 1;
		}
		else
		{
			return -1;
		}
    } 

    function nonull($var)
    {
        return (($var != '')&&($var != null));
    }
    
    function nullcheck($var, $withQuotes=false, $emptyToNull=false)
    {
        if(is_null($var)==false)
        {
          if($emptyToNull && $var == '')
          {
              return 'NULL'; 
          }
          else
          {
              if($withQuotes)
              {
                return "'".safequery($var)."'";  
              }
              return $var;  
          }
        }
        return 'NULL';
    }

    
    function gp_array_dif_array($objects,$gp,$side,$is_milk)
    {
        switch($gp)
        {               
            case 1: 
                $teeth=$is_milk?array('55', '54', '53', '52', '51', '61', '62', '63', '64', '65'):
                array('18', '17', '16', '15', '14', '13', '12', '11', '21', '22', '23', '24', '25', '26', '27', '28');
                break;
                
            
            case 2:
                $teeth=$is_milk?array('85', '84', '83', '82', '81', '71', '72', '73', '74', '75'):
                array('48', '47', '46', '45', '44', '43', '42', '41', '31', '32', '33', '34', '35', '36', '37', '38');
                break;

        }
        
        $result=array('TOOTH'=>array(),'TOOTHSIDE'=>array());
        for($i=0;$i<count($objects['TOOTH']);$i++)
        {
           if(!(($objects['TOOTHSIDE'][$i])==$side&&in_array($objects['TOOTH'][$i],$teeth))) 
           {
            $result['TOOTH'][]=$objects['TOOTH'][$i];
            $result['TOOTHSIDE'][]=$objects['TOOTHSIDE'][$i];
           }
        }
        return $result;
    }
    
    function gp_array_dif($objects,$gp,$side,$is_milk)
    {
        switch($gp)
        {                  
            case 1: 
                $teeth=$is_milk?array('55', '54', '53', '52', '51', '61', '62', '63', '64', '65'):
                array('18', '17', '16', '15', '14', '13', '12', '11', '21', '22', '23', '24', '25', '26', '27', '28');
                break;
                
            
            case 2:
                $teeth=$is_milk?array('85', '84', '83', '82', '81', '71', '72', '73', '74', '75'):
                array('48', '47', '46', '45', '44', '43', '42', '41', '31', '32', '33', '34', '35', '36', '37', '38');
                break;

        }
        $result=0;
        for($i=0;$i<count($objects['TOOTH']);$i++)
        {
            if(($objects['TOOTHSIDE'][$i])==$side&&in_array($objects['TOOTH'][$i],$teeth)) $result++;
        }
        return $result<count($teeth);
    }
    
    function get_tooth_surfaces($id)
    {

        return ($id<50)?
        (($id%10>3)?array("D","M","V","O","OK"):array("D","M","V","O")):
        (($id%10>2)?array("D","M","V","O","OK"):array("D","M","V","O"));

    }
    
    function get_tooth_roots($id)
    {
        $result=array();
        switch($id)
        {
            case 16: 
            case 17:
            case 18:
            case 26:
            case 27:
            case 28:
            case 54:
            case 55:
            case 64:
            case 65:
                $result=array("DR","MR","CR");
                break;
            
            case 14: 
            case 24:
            case 46: 
            case 47:
            case 48:
            case 36:
            case 37:
            case 38:
            case 64:
            case 65:
            case 74:
            case 75:
            case 84:
            case 85:
                $result=array("DR","MR");
                break;
                
            default:
                $result=array("R");
                break;
             
        }
        return $result;

    }
    
    function array_search_all($needle, $haystack) 
    { 
        $result=array();
        $i=0;
        foreach($haystack as $item)
        {
            if($item==$needle) $result[]=$i;
            $i++;   
        }
        return $result;
    } 
    
    
    function mb_ucfirst($str, $enc = 'utf-8') { 
    		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
    }
    
    function intcmp($a,$b) {
        if((int)$a == (int)$b)return 0;
        if((int)$a  > (int)$b)return 1;
        if((int)$a  < (int)$b)return -1;
    }
    function datecmp($a, $b)
    {

        $arrA=date_parse_from_format('d.m.Y',$a);

        $arrB=date_parse_from_format('d.m.Y',$b);
        return strcmp(date('Y-m-d',strtotime($arrA["year"]."-".$arrA["month"]."-".$arrA["day"])),date('Y-m-d',strtotime($arrB["day"]."-".$arrB["month"]."-".$arrB["year"])));
    }
    
    function shortname($last,$first,$middle)
    {
            $short='';
            if(nonull($last))
            { 
                $short = $last; 
                if(nonull($first))
                {
                    $short .=' '.mb_substr($first,0,1).".";
                    if(nonull($middle))
                    {
                        $short .=mb_substr($middle,0,1).".";
                    }
                }
            }
            return $short;
    }
    
    function gen_uuid() 
    {
        return strtoupper(sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ),

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ),

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        ));
    }
    
     function setTo48($name)
     {
        if(mb_strlen($name, 'UTF-8') < 48)
        {
            $name = mb_substr($name, 0, 48);
            return $name;
        }
        
        $pieces = explode(" ", $name);
        $name48 = "";
        foreach ($pieces as $piece) 
        {
            $name48 .= mb_substr($piece, 0, 5);
            if(mb_strlen($piece) < 5)
            {
                $name48 .= " ";
            }
            else
            {
                $name48 .= ". ";   
            }
        }
        $name48 = str_replace(';','',$name48);
        $name48 = mb_substr($name48, 0, 48);
        return $name48;
     }
     
     function sql_checkPriceIndex($priceIndex)
     {
        if($priceIndex == 0 || $priceIndex == 1)
        {
            return "";
        }
        else
        {
            return $priceIndex;
        }
     }
    function getLocation($address,$city)
    {
        //geocoding ***START***
        $params = array('city' => $city, 'address'  => $address);
        try
        {
            $content = @file_get_contents(Settings::GEOCODING_URL.'/ToothFairyGeoService.php?'. http_build_query($params, '', '&'));
            if($content === false)
            {
                $position = null;
            }
            else
            {
                $geo = json_decode($content);
                $position[]=$geo->long;
                $position[]=$geo->lat;
            }
        }
        catch(exception $e)
        {
            $position = null;
        }
        //geocoding ***END***
        
        
    	return $position;
    }
?>
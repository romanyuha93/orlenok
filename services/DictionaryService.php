<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "Settings.php";

 
class DictionaryService
{		
    public static $isDebugLog = false;
   
   
	/**
    * @return Object
	*/
    public function GetDictionaries()
    {
        $connection = new Connection();
        $connection->EstablishDBConnection("main",false);
	    //$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $sql_getdic = "select ID, DICTIONARY, ITEM_DATA, ITEM_LABEL, ITEM_SHORTLABEL from TF_DICTIONARIES order by ID";
        $query_getdic = ibase_prepare($trans, $sql_getdic);
        $result_getdic = ibase_execute($query_getdic);	
        
        $resultObject = new stdClass;
        $dicName = '';
        
		while ($row = ibase_fetch_row($result_getdic))
        {
            if($dicName != $row[1])
            {
               $dicName = $row[1];
               $resultObject->{$dicName} = array();
            }
            $dicItem = new stdClass;
            $dicItem->id_db = $row[0];
            $dicItem->dictionary = $row[1];
            $dicItem->id = $row[2];
            $dicItem->label = $row[3];	
            $dicItem->shortlabel = $row[4];	
            
            $resultObject->{$dicName}[] = $dicItem;	
        }
        ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $resultObject;
    }
    
   	/**
	* @param string $dictionary
    * @param object $trans
    * @return Object[]
	*/
    public function GetByDictionary($dictionary, $trans = null)
    {
          if($trans==null)
        {
            $connection = new Connection();
            //$connection->EstablishDBConnection("main",false);
	        $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        
        $sql_getdic = "select ID, DICTIONARY, ITEM_DATA, ITEM_LABEL, ITEM_SHORTLABEL from TF_DICTIONARIES
                            where DICTIONARY = '$dictionary' order by ID";
        $query_getdic = ibase_prepare($trans, $sql_getdic);
        $result_getdic = ibase_execute($query_getdic);	
        
        $resultObject = array();
        $dicName = '';
        
		while ($row = ibase_fetch_row($result_getdic))
        {
            $dicItem = new stdClass;
            $dicItem->id_db = $row[0];
            $dicItem->dictionary = $row[1];
            $dicItem->id = $row[2];
            $dicItem->label = $row[3];	
            $dicItem->shortlabel = $row[4];	
            
            $resultObject[] = $dicItem;	
        }
        ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        
        return $resultObject;
    }
    
   	/**
	* @param Object $item
    * @return Object[]
	*/
    public function SaveByDBID($item)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection("main",false);
        
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $save_sql = "update or insert into TF_DICTIONARIES (
                                                     ID, 
                                                     DICTIONARY, 
                                                     ITEM_DATA, 
                                                     ITEM_LABEL, 
                                                     ITEM_SHORTLABEL
                                                 )
            values (
                            $item->id_db, 
                            '$item->dictionary', 
                            '$item->id', 
                            '".safequery($item->label)."', 
                            '".safequery($item->shortlabel)."'
                        )
            matching (ID) returning (ID)";
            
        $save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $item->id_db = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
        $resultDictionary = $this->GetByDictionary($item->dictionary, $trans);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        
        return $resultDictionary;
    }
   

    
}

?>
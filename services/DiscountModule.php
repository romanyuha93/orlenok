<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
//define('tfClientId','online');
//DiscountModule::printCertificateAgreement('docx','1');

class DiscountModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var double
    */
    var $SUMM;

    /**
    * @var double
    */
    var $PERCENT;
}
class DiscountModuleDiscountCard
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var double
    */
    var $SUMM;
    
    /**
    * @var boolean
    */
    var $ENABLED;
    
    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $BARCODE;
        
    /**
    * @var DiscountModulePatient
    */
    var $PATIENT;
    
        
    /**
    * @var double
    */
    var $DISCOUNT;
}
class DiscountModuleCertificate
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var double
    */
    var $SUMM;
    
    /**
    * @var double
    */
    var $RESIDUE;
    
    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $BARCODE;
    
    /**
    * @var string
    */
    var $DATEOPEN;
        
    /**
    * @var string
    */
    var $DATECLOSE;
          
    /**
    * @var string
    */
    var $FIRSTNAME;
          
    /**
    * @var string
    */
    var $MIDDLENAME;
          
    /**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $FULLNAME;
    
    /**
    * @var string
    */
    var $SHORTNAME;
        
    /**
    * @var string
    */
    var $AUTHORID;
              
    /**
    * @var string
    */
    var $SUBDIVISIONID;
}
class DiscountModulePatient
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $FULLNAME;
    
    /**
    * @var double
    */
    var $SUMM;

} 
class DiscountModuleFilter
{
   /**
    * @var string[]
    */
    var $FIELDS;
    
    /**
    * @var string
    */
    var $SIGN;
    
    /**
    * @var string
    */
    var $VALUE;

}
class DiscountModule
{	
    /**  
    * @param DiscountModuleDictionaryObject $p1
    * @param DiscountModuleDiscountCard $p2
    * @param DiscountModulePatient $p3
    * @param DiscountModuleCertificate $p4
    * @param DiscountModuleFilter $p5
    */    
	public function registertypes($p1,$p2,$p3,$p4,$p5) 
	{
	}    
    
    /** 
 	*  @return DiscountModuleDictionaryObject[]
	*/ 
    public function getDiscountDictionary() //sqlite
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, SUMM, PERCENT from DISCOUNT_SCHEMA order by SUMM";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new DiscountModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->SUMM = $row[1];
			$obj->PERCENT = $row[2];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param DiscountModuleDictionaryObject $ovo 
    * @return string
 	*/
    public function saveDiscountDictionary($ovo) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $values=array();
                $values[]=nullcheck($ovo->ID);
                $values[]=nullcheck($ovo->SUMM);                
                $values[]=nullcheck($ovo->PERCENT);

                $QueryText="update or insert into DISCOUNT_SCHEMA (ID, SUMM, PERCENT)
                                                                    values (".implode(",", $values).") returning ID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        $res="";
        if($row = ibase_fetch_row($result))
		{ 
			$res = $row[0];
		}
		ibase_commit($trans);
		ibase_free_query($query);
        ibase_free_result($result);
		$connection->CloseDBConnection();
		return $res;
    }
    
    /**
 	* @param string[] $ids
    * @return boolean
 	*/ 		 		
    public function deleteDiscountDictionary($ids) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from DISCOUNT_SCHEMA where ID in (".implode(',',$ids).")";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
        /**
     * @param string $search
     * @return DiscountModulePatient[]
     */
    public function getPatients($search)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select first 20 patients.id,
                            patients.fullname,
                            patients.ACCUMULATION 
                    from patients
                    where
                        lower(patients.fullname) like lower('%$search%')";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $patients = array();
        while ($row = ibase_fetch_row($result))
        {
            $p = new DiscountModulePatient();
            $p->ID = $row[0];
            $p->FULLNAME = $row[1];
            $p->SUMM = $row[2];
            $patients[] = $p;
        }
        ibase_free_query($query);
        ibase_free_result($result);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
        return $patients;
    } 
           /**
     * @param string $id
     * @return DiscountModulePatient
     */
    public function getPatientById($id)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select patients.id,
                            patients.fullname,
                            patients.ACCUMULATION 
                    from patients
                    where id=$id";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $p = new DiscountModulePatient();
        while ($row = ibase_fetch_row($result))
        {
            $p->ID = $row[0];
            $p->FULLNAME = $row[1];
            $p->SUMM = $row[2];
        }
        ibase_free_query($query);
        ibase_free_result($result);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
        return $p;
    }  
    /** 
    * @param string $patient_id
    * @param string $barcode
 	*  @return DiscountModuleDiscountCard[]
	*/ 
    public function getDiscountCards($patient_id,$barcode)
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select dc.ID, 
                            dc.SUMM, 
                            dc.NUMBER, 
                            dc.PATIENTID, 
                            p.FULLNAME, 
                            dc.ENABLED, 
                            dc.BARCODE,
                            coalesce((select first 1 ds.percent from discount_schema ds where ds.summ<=dc.SUMM order by ds.summ desc),0) 
                            from DISCOUNTCARD dc
                            left join PATIENTS p on p.ID=dc.PATIENTID";
        $whereArray=array();
        if(nonull($patient_id))
        {
            $whereArray[]="dc.PATIENTID=$patient_id";
        }
        if(nonull($barcode))
        {
            $whereArray[]="dc.BARCODE='$barcode'";
            $whereArray[]="dc.ENABLED=1";
        }
        if(count($whereArray)>0)
        {
            $QueryText.=" where ".implode(' and ',$whereArray);
        }
        $QueryText.=" order by dc.NUMBER";
        
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new DiscountModuleDiscountCard;
			$obj->ID = $row[0];
			$obj->SUMM = $row[1];
			$obj->NUMBER = $row[2];
            $obj->PATIENT=new DiscountModulePatient;
            $obj->PATIENT->ID= $row[3];
            $obj->PATIENT->FULLNAME= $row[4];
           	$obj->ENABLED = $row[5];
           	$obj->BARCODE = $row[6];
            $obj->DISCOUNT = $row[7];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }  
    
        /** 
    * @param DiscountModuleFilter[] $filters
    * @param resource $trans
 	*  @return DiscountModuleCertificate[]
	*/ 
    public function getCertificates($filters,$trans=null)
    { 
       	if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
		$QueryText ="select id,
                             number,
                             summ,
                             residue,
                             dateopen,
                             dateclose,
                             firstname,
                             middlename,
                             lastname,
                             barcode,
                             authorid,
                             subdivisionid
                      from certificate";
        $whereArray=array();
        //$filter=new DiscountModuleFilter;
        foreach($filters as $filter)
        {
            if($filter->SIGN=='like')
            {
                $whereArray[]="lower(".implode("||' '||",$filter->FIELDS).") ".$filter->SIGN." '".$filter->VALUE."'";
            }
            else
            {
                $whereArray[]=$filter->FIELDS[0]." ".$filter->SIGN." '".$filter->VALUE."'";
            }

        }
        if(count($whereArray)>0)
        {
            $QueryText.=" where ".implode(' and ',$whereArray);
        }
        $QueryText.=" order by number";
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText."\n".$trans);
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new DiscountModuleCertificate;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
            $obj->SUMM = $row[2];
            $obj->RESIDUE = $row[3];
            $obj->DATEOPEN = $row[4];
            $obj->DATECLOSE = $row[5];
            $obj->FIRSTNAME = $row[6];
            $obj->MIDDLENAME = $row[7];
            $obj->LASTNAME = $row[8];
            $obj->FULLNAME = "$row[8] $row[6] $row[7]";
            $obj->SHORTNAME = shortname($row[8],$row[6],$row[7]);
            $obj->BARCODE = $row[9];
            $obj->AUTHORID = $row[10];
            $obj->SUBDIVISIONID = $row[11];
			$objs[] = $obj;
		}
	
		ibase_free_query($query);
		ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }  
		return $objs; 
    }    
        
    /**
	* @param string $certificate_id
	* @param string $certificate_barcode
	* @return boolean
     */
    public function setCertificateBarcode($certificate_id, $certificate_barcode) 
	{ 
	    $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        // CHECK BARCODE START
        
        $barcodecheck_sql = "select count(*) from certificate where barcode = '$certificate_barcode'";
        $query_barcodecheck = ibase_prepare($trans, $barcodecheck_sql);
        $result_barcodecheck=ibase_execute($query_barcodecheck);
        $barcodecheck_row = ibase_fetch_row($result_barcodecheck);
        if($barcodecheck_row[0] != 0)
        {
            return false;
        }
        // CHECK BARCODE END
        
        $certificate_sql = "update certificate set BARCODE='".safequery($certificate_barcode)."'
                        where id = $certificate_id";	
		$certificate_query = ibase_prepare($trans, $certificate_sql);
		ibase_execute($certificate_query);		
		$success = ibase_commit($trans);
		ibase_free_query($certificate_query);	
		$connection->CloseDBConnection();	
		return $success;		
	}
    
     /**
	* @param string $card_id
	* @param string $card_barcode
	* @return boolean
     */
    public function setCardBarcode($card_id, $card_barcode) 
	{ 
	    $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        // CHECK BARCODE START
        
        $barcodecheck_sql = "select count(*) from DISCOUNTCARD where barcode = '$card_barcode'";
        $query_barcodecheck = ibase_prepare($trans, $barcodecheck_sql);
        $result_barcodecheck=ibase_execute($query_barcodecheck);
        $barcodecheck_row = ibase_fetch_row($result_barcodecheck);
        if($barcodecheck_row[0] != 0)
        {
            return false;
        }
        // CHECK BARCODE END
        
        $card_sql = "update DISCOUNTCARD set BARCODE='".safequery($card_barcode)."'
                        where id = $card_id";	
		$card_query = ibase_prepare($trans, $card_sql);
		ibase_execute($card_query);		
		$success = ibase_commit($trans);
		ibase_free_query($card_query);	
		$connection->CloseDBConnection();	
		return $success;		
	}
    
    /**
 	* @param DiscountModuleDiscountCard $ovo 
    * @return string
 	*/
    public function saveDiscountCard($ovo) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $values=array();
                $values[]=nullcheck($ovo->ID);
                $values[]=$ovo->SUMM;                
                $values[]=nullcheck($ovo->NUMBER,true);
                if(nonull($ovo->PATIENT))
                {
                    $values[]=nullcheck($ovo->PATIENT->ID);
                }
                else
                {
                    $values[]='null';   
                }
  
                $values[]=$ovo->ENABLED?'1':'0';
                $values[]=nullcheck($ovo->BARCODE,true);
                

                $QueryText="update or insert into DISCOUNTCARD (ID, SUMM, NUMBER, PATIENTID, ENABLED, BARCODE)
                                                                    values (".implode(",", $values).") returning ID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        $res="";
        if($row = ibase_fetch_row($result))
		{ 
			$res = $row[0];
		}
		ibase_commit($trans);
		ibase_free_query($query);
        ibase_free_result($result);
		$connection->CloseDBConnection();
		return $res;
    }
        /**
 	* @param DiscountModuleCertificate $ovo 
    * @return string
 	*/
    public function saveCertificate($ovo) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $values=array();
            $values[]=nullcheck($ovo->ID);
            $values[]=nullcheck($ovo->NUMBER,true);
            $values[]=$ovo->SUMM;                
            $values[]=$ovo->RESIDUE; 
            $values[]=nullcheck($ovo->DATEOPEN,true);
            $values[]=nullcheck($ovo->DATECLOSE,true);
            $values[]=nullcheck($ovo->FIRSTNAME,true);
            $values[]=nullcheck($ovo->MIDDLENAME,true);
            $values[]=nullcheck($ovo->LASTNAME,true);
            $values[]=nullcheck($ovo->BARCODE,true);
            $values[]=nullcheck($ovo->AUTHORID,true);     
            $values[]=nullcheck($ovo->SUBDIVISIONID,true);        

                $QueryText="update or insert into certificate 
                            (id,
                             number,
                             summ,
                             residue,
                             dateopen,
                             dateclose,
                             firstname,
                             middlename,
                             lastname,
                             barcode,
                             authorid,
                             subdivisionid)
                                                                    
                            values (".implode(",", $values).") returning ID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
        $res="";
        if($row = ibase_fetch_row($result))
		{ 
			$res = $row[0];
		}
		ibase_commit($trans);
		ibase_free_query($query);
        ibase_free_result($result);
		$connection->CloseDBConnection();
		return $res;
    }
    /**
 	* @param string[] $ids
    * @return boolean
 	*/ 		 		
    public function deleteDiscountCards($ids) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from DISCOUNTCARD where ID in (".implode(',',$ids).")";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    } 
    /**
 	* @param string[] $ids
    * @return boolean
 	*/ 		 		
    public function deleteCertificates($ids, $passForDelete) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
		      $QueryText = "delete from certificate where ID in (".implode(',',$ids).")";
		      $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
        	   ibase_free_query($query);
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;			
    }  
    
      //*************************************** PRINT Certificate Agreement START **********************************
     /** 
 * @param string $docType
 * @param string $certificateId
 * @return string
 */ 
  public static function printCertificateAgreement($docType, $certificateId)
    {   
        
         $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
        $CERTIFICATE=DiscountModule::getCertificate($certificateId,$trans); 
        $CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(true, $trans);
        
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/CertificateAgreement.'.$docType, OPENTBS_ALREADY_UTF8);  
        
       
        
       // $TBS->NoErr=true;
              
        $TBS->MergeField('CERTIFICATE',$CERTIFICATE);
        $TBS->MergeField('CLINIC_DATA',$CLINIC_DATA);
                
        $file_name = translit(str_replace(' ','_',$CERTIFICATE->NUMBER.'_'.$CERTIFICATE->FULLNAME));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;   
        //echo $TBS->Source;
        
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
        
        
        
         ibase_commit($trans);
         $connection->CloseDBConnection();
                
        return $form_path; 
    }
      /** 
  * @param string $form_path
  * @return boolean
  */  
    public static function deleteCertificateAgreement($form_path)
    {
        return deleteFileUtils($form_path);
    }
     /**
     * @param string $certificateId
     * @param resource $trans
     * @return TreatmentManagerModuleTemplate
     */
    public static function getCertificate($certificateId,$trans=null)
    {
		if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
            	$QueryText ="select id,
                             number,
                             summ,
                             residue,
                             dateopen,
                             dateclose,
                             firstname,
                             middlename,
                             lastname,
                             barcode,
                             authorid,
                             subdivisionid
                from certificate
                where id = $certificateId";
                
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $obj = null;
			if($row = ibase_fetch_row($result))
			{
                $obj = new DiscountModuleCertificate;
    			$obj->ID = $row[0];
    			$obj->NUMBER = $row[1];
                $obj->SUMM = $row[2];
                $obj->RESIDUE = $row[3];
                $obj->DATEOPEN =  date ('d/m/Y', strtotime($row[4]));
                $obj->DATECLOSE =  date ('d/m/Y', strtotime($row[5]));
                $obj->FIRSTNAME = $row[6];
                $obj->MIDDLENAME = $row[7];
                $obj->LASTNAME = $row[8];
                $obj->FULLNAME = "$row[8] $row[6] $row[7]";
                $obj->SHORTNAME = shortname($row[8],$row[6],$row[7]);
                $obj->BARCODE = $row[9];
                $obj->AUTHORID = $row[10];
                $obj->SUBDIVISIONID = $row[11];
			}
			ibase_free_query($query);
			ibase_free_result($result);
            
			 if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }     
            
            return $obj;
    }
    //*************************************** PRINT Certificate Agreement END **********************************
  }
?>
<?php
require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');

//$tmp=new ChiefHRModule;
//$tmp->printHRHistory('odt', '2012-12-30','2013-01-31');
class ChiefHRModule
{
    /**
     * @param ChiefHRModule_HRHistoryData $p1
     * @param ChiefHRModule_Doctor $p2
     * @param ChiefHRModule_HRHistoryItem $p3
     * @param ChiefHRModule_HRDayHistoryItem $p4
     * @return void
     */

    public function registertypes($p1, $p2, $p3, $p4)
    {
    }
    
    
    /**
     * @param string $doc_type
 	* @param string $startTime	
 	* @param string $endTime
     * @return string
     */
     public function printHRHistory($doc_type, $startTime, $endTime)
     {
        $crd=new PrintDataModule_ClinicRegistrationData();
        $st=strtotime($startTime);          
        $en=strtotime($endTime);
        $isBig=($en-$st)>(3600*24*30);
        if($isBig)
        {
          while($en>=$st)
          {
            $bufdate=$this->lastDayInMonth($st);
            $print_data_array[]=$this->getHRHistory(date('Y-m-d',$st), $bufdate, true, true); 
            $st=strtotime($bufdate)+86400; 
          }             
        }
        else
        {
          $print_data_array[]=$this->getHRHistory($startTime, $endTime, true, true);  
        }
        
        //print_r($print_data_array);
        
   
        $TBS = new clsTinyButStrong;
        
             $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
             $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/HRTimesheet.'.$doc_type, OPENTBS_ALREADY_XML);
             switch($doc_type)
             {
                case 'docx':
                    $GLOBALS['xml1']='</w:t></w:r></w:p><w:tcPr><w:tcW w:w="';
                    $GLOBALS['xml2']='" w:type="dxa"/><w:gridSpan w:val="';
                    $GLOBALS['xml3']='"/></w:tcPr><w:p><w:r><w:t>';
                    break;
                case 'odt':
                    $GLOBALS['xml1']='Таблица1.C"/><table:table-column table:style-name="Таблица1.D';
                    $GLOBALS['xml2']='Таблица1.D';
                    break;
             }

         

            //$TBS->NoErr=true;
            $TBS->MergeBlock('HR',$print_data_array);
            $TBS->MergeField('CLINIC_DATA',$crd);
            //echo($TBS->Source);
            
            $file_name = 'hr_'.$startTime.'_-_'.$endTime;
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().$file_name.'.'.$doc_type;  
        
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
         
            return $form_path; 
     }
    
    private function lastDayInMonth($d)
    {
       return (date('Y-m-',$d+86400).cal_days_in_month(CAL_GREGORIAN, date('m',$d+86400), date('Y',$d+86400)));
       
    }        
    
       /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteHRHistoryDoc($form_path)
    {
        return deleteFileUtils($form_path);
    }
    
    /**
    * ChiefHRModule::getHRHistory()
 	* @param string $startTime	
 	* @param string $endTime
	* @param boolean $no_tp
    * @return ChiefHRModule_HRHistoryData[]
    */
    public function getHRHistory($startTime, $endTime, $no_tp, $for_print=false)
    {
        $connection = new Connection();
  		$connection->EstablishDBConnection();
  		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $doc_arr=$this->getDoctors($startTime,$endTime,$trans);
        if($for_print)
        {
            $MonthNames=tflocale::$MonthNames;
        }
        $res=array(); 
        foreach($doc_arr as $doc)
        {
            
            $hr_data=new ChiefHRModule_HRHistoryData();
            $hr_data->DOCTOR=$doc;
            if($for_print)
            {   
               $hr_data->ITEMS=array();
                               
            }
            else
            {
              $hr_data->ITEMS=new stdclass;  
            }
            
            $summ=0;                                 
            $st=strtotime($startTime);          
            $en=strtotime($endTime);
            
            $i=0;            
            while($st<=$en)
            {      
                $hist_arr=$this->getHRHistoryTemp(date('Y-m-d',$st), $no_tp, $trans);
                
                
                if(array_key_exists($doc->ID, $hist_arr[$doc->IS_ASSISTANT?'ASS':'DOC']))
                {   

                    if($for_print)
                    {   
                        $hr_item=new ChiefHRModule_HRHistoryItem;
                        $hr_item=$hist_arr[$doc->IS_ASSISTANT?'ASS':'DOC'][$doc->ID];
                        $summ+=$hr_item->WORKTIME;
                        $hr_item->WORKTIME=round($hr_item->WORKTIME, 1);
                        $hr_data->ITEMS[date('Y',$st)]['MONTHS'][$MonthNames[date('m',$st)-1]]['DAYS'][date('d',$st)+0]=$hr_item;
                        
                    }
                    else
                    {                    
                        $hr_data->ITEMS->$i=$hist_arr[$doc->IS_ASSISTANT?'ASS':'DOC'][$doc->ID];
                        $summ+=$hr_data->ITEMS->$i->WORKTIME;
                        $hr_data->ITEMS->$i->WORKTIME=round($hr_data->ITEMS->$i->WORKTIME, 1); 
                    }
                }
                elseif($for_print)
                {
                    $hr_item=new ChiefHRModule_HRHistoryItem;
                    $hr_item->WORKTIME=0;
                    $hr_item->IS_CORRECT=true;
                    $hr_data->ITEMS[date('Y',$st)]['MONTHS'][$MonthNames[date('m',$st)-1]]['DAYS'][date('d',$st)+0]=$hr_item;
                }                                   
                
                $st+=86400;
                $i++;
             }
            $hr_data->SUMM=round($summ, 1);
  
            if($for_print)
            {   
                $res['DATA'][]=$hr_data;
            }
            else
            {
               $res[]=$hr_data;
            }
                                       
        }
        if($for_print)
        {       $res['ALLCOUNT']=$i;   
                $years=$res['DATA'][0]->ITEMS;
                foreach($years as $y=>$year)
                {
                    $md_count=0;
                    foreach($year['MONTHS'] as $m=>$month)
                    {
                        $res['DATA'][0]->ITEMS[$y]['MONTHS'][$m]['DCOUNT']=count($month['DAYS']);
                        $md_count+=$res['DATA'][0]->ITEMS[$y]['MONTHS'][$m]['DCOUNT'];
                    }
                    $res['DATA'][0]->ITEMS[$y]['MDCOUNT']=$md_count;
                }
         }
            
                    
        ibase_commit($trans);
        $connection->CloseDBConnection();     
        //print_r($res);
        return $res;
    }  
    
    /**
    * ChiefHRModule::getHRDayHistory()
 	* @param string $date
 	* @param ChiefHRModule_Doctor $doctor 
    * @param boolean $isDefaultCourseNamesOfProcedure
    * @return ChiefHRModule_HRDayHistoryItem[]
    */
    public function getHRDayHistory($date, $doctor, $isDefaultCourseNamesOfProcedure)
    {
        $connection = new Connection();
  		$connection->EstablishDBConnection();
  		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $res=array(); 
        
        $endDate=date('Y-m-d',strtotime($date)+86400);
        $doc_post=$doctor->IS_ASSISTANT?'ASSISTANT':'DOCTOR';
        $ass_post=$doctor->IS_ASSISTANT?'DOCTOR':'ASSISTANT';
        $proc_name_type=$isDefaultCourseNamesOfProcedure?'NAME':'NAMEFORPLAN';
        
        $QueryText="select  EVENTTIME, 
                            EVENTTEXT, 
                            IS_CORRECT, 
                            ASSDOC_ID, 
                            ASSDOC_SNAME, 
                            PATIENT_ID, 
                            PATIENT_SNAME
                    from (
                    select DATECLOSE as EVENTTIME,
                            $proc_name_type as EVENTTEXT,
                            (select
                                POINTTYPE from WORKTABLE
                                    where POINTTIME=(select max(POINTTIME)
                                                        from WORKTABLE
                                                        where
                                                        POINTTIME<DATECLOSE
                                                        and ".$doc_post."_FK=$doctor->ID
                                                        )
                            )
                             as IS_CORRECT,
                            $ass_post as ASSDOC_ID,
                            ".$ass_post."S.SHORTNAME as ASSDOC_SNAME,
                            PATIENT_FK as PATIENT_ID,
                            PATIENTS.SHORTNAME as PATIENT_SNAME
                            from TREATMENTPROC
                            left join ".$ass_post."S on ($ass_post=".$ass_post."S.ID)
                            left join PATIENTS on (PATIENT_FK=PATIENTS.ID)
                                where
                                    DATECLOSE>='$date'
                                    and
                                    DATECLOSE<='$endDate'
                                    and
                                    ".$doc_post."=$doctor->ID
                    union
                    select POINTTIME as EVENTTIME,
                            POINTTYPE as EVENTTEXT,
                            null as IS_CORRECT,
                            null as ASSDOC_ID,
                            null as ASSDOC_SNAME,
                            null as PATIENT_ID,
                            null as PATIENT_SNAME
                            from WORKTABLE
                                where
                                    POINTTIME>='$date'
                                    and
                                    POINTTIME<='$endDate'
                                    and
                                    ".$doc_post."_FK=$doctor->ID
                                )
                                    order by EVENTTIME";
        
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
                
        while($row = ibase_fetch_row($result))
        {
            $hr_day=new ChiefHRModule_HRDayHistoryItem;
            $hr_day->TIME=date('H:i',strtotime($row[0]));
            $hr_day->TEXT=$row[1];
            $hr_day->IS_CORRECT=$row[2]==1;
            $hr_day->ASSDOC_ID=$row[3];
            $hr_day->ASSDOC_SNAME=$row[4];
            $hr_day->PATIENT_ID=$row[5];
            $hr_day->PATIENT_SNAME=$row[6];
            $hr_day->IS_DOCTOR=$doctor->IS_ASSISTANT;
            $res[]=$hr_day;
        }
        ibase_free_query($query);
        ibase_free_result($result);                      
        ibase_commit($trans);
        $connection->CloseDBConnection();
        //print_r($res);        
        return $res;
    }
    
    /**
    * ChiefHRModule::getHRHistoryTemp()
 	* @param string $startTime	
	* @param boolean $no_tp
    * @param resource $trans
    * @return Array[]
    */
    private function getHRHistoryTemp($startTime, $no_tp, $trans)
    {
		if($no_tp)
		{
			$tpCheck="null";
		}
		else
		{
			$tpCheck="
			(select first 1 1
                           from TREATMENTPROC tp where (select first 1
                                wt2.POINTTYPE
                                from WORKTABLE wt2
                                                where wt2.POINTTIME<tp.DATECLOSE  and cast(wt2.POINTTIME as Date)='$startTime'
                                                and coalesce(wt2.ASSISTANT_FK, wt2.DOCTOR_FK)=coalesce(t.ass, t.doc)
                                                order by   POINTTIME desc
                                )=-1
                                and  cast(tp.DATECLOSE as Date)='$startTime'

                                and coalesce(tp.ASSISTANT, tp.DOCTOR)=coalesce(t.ass, t.doc))
								";
		}
		
		$QueryText="select t.worktime, t.doc, t.ass, $tpCheck
                        from (select   (sum((timestamp'$startTime'-wt.POINTTIME)*wt.POINTTYPE))*24 as worktime,
                             wt.DOCTOR_FK as doc, wt.ASSISTANT_FK as ass
                            from WORKTABLE wt
                            WHERE cast(wt.POINTTIME as Date)='$startTime'
                            group by wt.DOCTOR_FK, wt.ASSISTANT_FK) t";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $res=array('ASS'=>array(), 'DOC'=>array());
        
        while($row = ibase_fetch_row($result))
        {
            $hr_temp=new ChiefHRModule_HRHistoryItem;
            $hr_temp->WORKTIME=$row[0]<0?24+$row[0]:$row[0];
            $hr_temp->DATE=$startTime;
            $hr_temp->IS_CORRECT=$row[3]==null;
            if(nonull($row[1]))
            {
                $res['DOC'][$row[1]]=$hr_temp;
            }
            else
            {
                $res['ASS'][$row[2]]=$hr_temp;
            }
        }
        ibase_free_query($query);
        ibase_free_result($result);
        //print_r($res);  
        return $res;
    }  
    
    /**
    * ChiefHRModule::getDoctors()
 	* @param string $startTime	
 	* @param string $endTime
    * @param resource $trans
    * @return ChiefHRModule_Doctor[]
    */
    public function getDoctors($startTime,$endTime,$trans=null)//sqlite
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        $QueryText="select ID, SHORTNAME, IS_ASSISTANT from (select ID, SHORTNAME, 1 as IS_ASSISTANT, DATEWORKEND, DATEWORKSTART  from ASSISTANTS
                    union
                    select ID, SHORTNAME, 0 as IS_ASSISTANT, DATEWORKEND, DATEWORKSTART  from DOCTORS) 
                    
                    WHERE 
                    
                    (DATEWORKEND >= '$startTime' or DATEWORKEND IS null) and (DATEWORKSTART <= '$endTime' or DATEWORKSTART IS null)  order by IS_ASSISTANT desc";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $res=array();
        
        while($row = ibase_fetch_row($result))
        {
            $hr_doc=new ChiefHRModule_Doctor;
            $hr_doc->ID=$row[0];
            $hr_doc->SHORTNAME=$row[1];
            $hr_doc->IS_ASSISTANT=$row[2]==1;
            $res[]=$hr_doc;
         }
        ibase_free_query($query);
        ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res;
    }  
}

class ChiefHRModule_HRHistoryItem
{
    
    /**
    * @var boolean
    */
    var $IS_CORRECT;
    
    /**
    * @var double
    */
    var $WORKTIME;
    
    /**
    * @var string
    */
    var $DATE;
    
    

}

class ChiefHRModule_HRDayHistoryItem
{
    
    /**
    * @var string
    */
    var $TIME;
    
    /**
    * @var string
    */
    var $TEXT;
    
    /**
    * @var boolean
    */
    var $IS_CORRECT;  
    
    /**
    * @var int
    */
    var $ASSDOC_ID;
    
    /**
    * @var string
    */
    var $ASSDOC_SNAME;
    
    /**
    * @var int
    */
    var $PATIENT_ID;
    
    /**
    * @var string
    */
    var $PATIENT_SNAME;
    
    /**
    * @var boolean
    */
    var $IS_DOCTOR;

}

class ChiefHRModule_HRHistoryData
{
    
    /**
    * @var ChiefHRModule_Doctor
    */
    var $DOCTOR;
    
    /**
    * @var object
    */
    var $ITEMS;
    
    /**
    * @var double
    */
    var $SUMM;

}


class ChiefHRModule_Doctor
{
    /**
    * @var int
    */
    var $ID;
    
    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var boolean
    */
    var $IS_ASSISTANT;
}
?>
<?php

//define('tfClientId','online');
require_once "Connection.php";
require_once "Utils.php";


class Storage_FarmDoc
{				
	/**
	* @var string
	*/
	var $id;    
	
	/**
	* @var int
	*/
	var $doctype;

	/**
	* @var string
	*/
	var $supplier_fk; 
	/**
	* @var string
	*/
	var $supplier_name;         

	/**
	* @var string
	*/
	var $createtimestamp;

	/**
	* @var string
	*/
	var $docdate;
	 
	/**
	* @var number
	*/
	var $summ;  
	
	/**
	* @var string
	*/
	var $docnumber;

	/**
	* @var string
	*/
	var $accountflow_fk;
    
	/**
	* @var string
	*/
	var $subdivision_fk;
	/**
	* @var string
	*/
	var $subdivision_name;
    
	/**
	* @var string
	*/
	var $author_fk;	
	/**
	* @var string
	*/
	var $author_name;	
    
	/**
	* @var string
	*/
	var $note;	
	/**
	* @var string
	*/
	var $farmstorage_fk;
	/**
	* @var string
	*/
	var $farmstorage_name;	
    
    /**
	* @var string
	*/
	var $doctor_fk;
	/**
	* @var string
	*/
	var $patient_fk;
	/**
	* @var string
	*/
	var $patient_name;   
    /**
	* @var string
	*/
	var $room_fk;
	    
	
    
    
	/**
	* @var Storage_FarmBath[]
	*/
	var $farmbaths;	
    
    
    
    /**
     * @var string
     */
     var $orderNumber;
     /**
     * @var int
     */
     var $orderIsCashpayment;
     /**
     * @var string
     */
     var $orderOperationtimestamp; 
}

class Storage_FarmBath
{
    /**
    * @var string
    */
    var $id; 
    /**
    * @var string
    */
    var $bathtype; 
    /**
    * @var string
    */
    var $farm_fk; 
    /**
    * @var string
    */
    var $farm_name; 
    /**
    * @var string
    */
    var $farm_baseunit;
    /**
    * @var string
    */
    var $farmdoc_fk; 
    /**
    * @var number
    */
    var $quantity;  
    /**
    * @var number
    */
    var $price;
	/**
	* @var string
	*/
	var $createtimestamp;
	/**
	* @var string
	*/
	var $treatprcfarm_fk;
	/**
	* @var string
	*/
	var $subdivision_fk;
    
	/**
	* @var string
	*/
	var $author_fk;	
}

class Storage_Supplier
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $NAME;
}
class Storage_Farm
{
 	/**
    * @var string
    */
    var $id;
   	/**
    * @var string
    */
    var $name;
   	/**
    * @var string
    */
    var $unitXml;
}
class Storage_FarmRemains
{
 	/**
    * @var string
    */
    var $farm_id;
   	/**
    * @var string
    */
    var $farm_name;
   	/**
    * @var string
    */
    var $farm_unitmeasure;
   	/**
    * @var number
    */
    var $quantity;
   	/**
    * @var number
    */
    var $price;
	/**
	* @var string
	*/
	var $subdivision_fk;
	/**
	* @var string
	*/
	var $subdivision_name;
	/**
	* @var number
	*/
	var $minimumquantity;
	/**
	* @var number
	*/
	var $optimalquantity;
	/**
	* @var string
	*/
	var $storageLabel;
	/**
	* @var int
	*/
	var $storageColor;
    
}


class Storage_Patient
{
      /**
   * @var string
   */
	var $patient_id;
   /**
   * @var string
   */
	var $patient_fname; 
   /**
   * @var string
   */
	var $patient_lname; 
   /**
   * @var string
   */
	var $patient_sname;
   /**
   * @var string
   */
	var $patient_fio;
   /**
   * @var string
   */
	var $patient_label;
}

class Storage_FarmStorage
{
 	/**
    * @var string
    */
    var $id;
   	/**
    * @var string
    */
    var $label;
   	/**
    * @var int
    */
    var $color;
    
   	/**
    * @var boolean
    */
    var $isDoctor = false;
   	/**
    * @var string
    */
    var $doctor_fk;
   	/**
    * @var string
    */
    var $doctor_shortname;
    
    
	/**
	* @var string
	*/
	var $subdivision_fk;
	/**
	* @var string
	*/
	var $subdivision_name;
}


class Storage
{
    
    
    public static $isDebugLog = false;
    
    
    
    /**
     * @param Storage_FarmDoc $p1
     * @param Storage_FarmBath $p2
     * @param Storage_Supplier $p3
     * @param Storage_Farm $p4
     * @param Storage_FarmRemains $p5
     * @param Storage_Patient $p6
     * @param Storage_FarmStorage $p7
     * @return void
     */
    public function registerTypes($p1, $p2, $p3, $p4, $p5, $p6, $p7){}
    
    
    /**
     * @param Storage_FarmDoc $farmDoc
     * @param boolean $isAccountflowDoc
     * @param int $isCashpayment
     * @return Storage_FarmDoc
     */
    public function saveFarmDoc($farmDoc, $isAccountflowDoc, $isCashpayment)
    {
         $connection = new Connection();
         $connection->EstablishDBConnection();
         $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        // save doc *** START ***
        $operationType = 2; //update
        if($farmDoc->id == '' || $farmDoc->id == null)
        {
            $operationType = 1; //insert
            $farmDoc->id = 'null';
            if($isAccountflowDoc)
            {
                // query accountflow for storage document **** START ****
            	$sql_acf = "insert into accountflow (
                accountflow.summ, 
                accountflow.operationtype, 
                accountflow.operationnote,
                accountflow.is_cashpayment, 
                accountflow.group_fk ";
                /*if($farmDoc->patient_fk != null)
                {
                    $sql_acf .= " ,accountflow.patientid ";
                }*/
                
                $sql_acf .= " ) values( ";
                if($farmDoc->doctype == 1)
                {
                    $sql_acf .= " '".floatval($farmDoc->summ)."', 4, '', ";
                }
                else
                {
                    $sql_acf .= " '".-floatval($farmDoc->summ)."', 4, '', ";
                }
                         
                $sql_acf .= " $isCashpayment, null ";
                /*if($farmDoc->patient_fk != null)
                {
                    $sql_acf .= " ,".$farmDoc->patient_fk." ";
                }*/
                $sql_acf .= " ) returning (ID)";
                         
                $query_acf = ibase_prepare($trans, $sql_acf);
                $result_acf = ibase_execute($query_acf);
                $row_acf = ibase_fetch_row($result_acf);
                $farmDoc->accountflow_fk = $row_acf[0];
                // query accountflow for storage document **** END ****
            }
        }
        if($farmDoc->supplier_fk == '' || $farmDoc->supplier_fk == null)
        {
            $farmDoc->supplier_fk = 'null';
        }
        if($farmDoc->accountflow_fk == '' || $farmDoc->accountflow_fk == null)
        {
            $farmDoc->accountflow_fk = 'null';
        }
        if($farmDoc->patient_fk == '' || $farmDoc->patient_fk == null)
        {
            $farmDoc->patient_fk = 'null';
        }
        if($farmDoc->farmstorage_fk == '' || $farmDoc->farmstorage_fk == null)
        {
            $farmDoc->farmstorage_fk = 'null';
        }
        
        $sqlSaveDoc = "update or insert into FARMDOC (
                                ID, 
                                DOCTYPE, 
                                SUPPLIER_FK, 
                                DOCDATE, 
                                SUMM, 
                                DOCNUMBER, 
                                ACCOUNTFLOW_FK,
                                PATIENT_FK,
                                NOTE,
                                FARMSTORAGE_FK,
                                SUBDIVISION_FK)
                        values (
                                ".$farmDoc->id.",
                                ".$farmDoc->doctype.", 
                                ".$farmDoc->supplier_fk.",
                                '".$farmDoc->docdate."', 
                                '".$farmDoc->summ."', 
                                '".safequery($farmDoc->docnumber)."',
                                ".$farmDoc->accountflow_fk.",
                                ".$farmDoc->patient_fk.",
                                '".safequery($farmDoc->note)."',
                                ".$farmDoc->farmstorage_fk.",
                                ".nullcheck($farmDoc->subdivision_fk).")
                        matching (ID) returning (ID)";
                        
        $querySaveDoc = ibase_prepare($trans, $sqlSaveDoc);
        $resultSaveDoc = ibase_execute($querySaveDoc);
        $rowDocId = ibase_fetch_row($resultSaveDoc);
        $farmDoc->id = $rowDocId[0];
        
        ibase_free_query($querySaveDoc);
        ibase_free_result($resultSaveDoc);
        // save doc *** END ***
        
        // del bath *** START p1 ***
        if($operationType == 2)
        {
            $sqlDelBath = "delete from FARMBATH where farmbath.farmdoc_fk = ".$farmDoc->id;
        }
        // del bath *** END p1 ***
        
        // save bath *** START ***
        $sqlSaveBath = "execute block as begin ";
        foreach ($farmDoc->farmbaths as $farmBath)
        {  
            // del bath *** START p2 ***
            if($operationType == 2)
            {
                $sqlDelBath = " and farmbath.id <> ".$farmBath->id;
            }
            // del bath *** END p2 ***
            
            if($farmBath->id == '' || $farmBath->id == null)
            {
                $farmBath->id = 'null';
            }
            if($farmBath->treatprcfarm_fk == '' || $farmBath->treatprcfarm_fk == null)
            {
                $farmBath->treatprcfarm_fk = 'null';
            }
            $sqlSaveBath .= "update or insert into FARMBATH (
                                ID, 
                                BATHTYPE, 
                                FARM_FK, 
                                FARMDOC_FK, 
                                QUANTITY, 
                                PRICE, 
                                TREATPROCFARM_FK,
                                FARMSTORAGE_FK)
                            values (
                                ".$farmBath->id.", 
                                ".$farmDoc->doctype.", 
                                ".$farmBath->farm_fk.", 
                                ".$farmDoc->id.", 
                                '".$farmBath->quantity."', 
                                '".round($farmBath->price,2)."',
                                ".$farmBath->treatprcfarm_fk.",
                                ".$farmDoc->farmstorage_fk.")
                            matching (ID); ";
        }
        $sqlSaveBath .= " end";
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$sqlSaveBath);//var_export($diags,true));
        $querySaveBath = ibase_prepare($trans, $sqlSaveBath);
        ibase_execute($querySaveBath);
        ibase_free_query($querySaveBath);
        // save bath *** END ***
        
        // del bath *** START p3 ***
        if($operationType == 2)
        {
            $queryDelBath = ibase_prepare($trans, $sqlDelBath);
            ibase_execute($queryDelBath);
            ibase_free_query($queryDelBath);
        }
        // del bath *** END p3 ***

        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $farmDoc;
    } 
    
    /**
     * @param Storage_FarmDoc $farmDoc
     * @return boolean
     */
    public function removeFarmDoc($farmDoc)
    {
         $connection = new Connection();
         $connection->EstablishDBConnection();
         $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $sqlDelDoc = "delete from FARMDOC where ID = ".$farmDoc->id;
                        
        $queryDelDoc = ibase_prepare($trans, $sqlDelDoc);
        ibase_execute($queryDelDoc);
        ibase_free_query($queryDelDoc);
        
        if($farmDoc->accountflow_fk != null)
        {
            $sqlDelAflow = "delete from ACCOUNTFLOW where ID = ".$farmDoc->accountflow_fk;
            $queryDelAflow = ibase_prepare($trans, $sqlDelAflow);
            ibase_execute($queryDelAflow);
            ibase_free_query($queryDelAflow);
        }
        
        $result = ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $result;
    } 
    
    /**
     * @param string $docType
     * @param string $startDate
     * @param string $endDate
     * @param string $supplierId
     * @param string $farmstorageId
     * @param string $searchText
	 * @param string $subdivId
     * @return Storage_FarmDoc[]
     */
    public function getFarmDocs($docType, $startDate, $endDate, $supplierId, $farmstorageId, $searchText, $subdivId)
    {
         $connection = new Connection();
         $connection->EstablishDBConnection();
         $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
         if(nonull($startDate))
         {
            //$startDate.=' 00:00:00';
         }
         if(nonull($endDate))
         {
            //$endDate.=' 23:59:59';
         }
         
         	$sqlGetDocs = "
                select 
                    ID, 
                    DOCTYPE, 
                    SUPPLIER_FK,
                    (select supplier.name from supplier where supplier.id = SUPPLIER_FK), 
                    CREATETIMESTAMP, 
                    DOCDATE, 
                    SUMM, 
                    DOCNUMBER, 
                    NOTE, 
                    ACCOUNTFLOW_FK, 
                    PATIENT_FK,
                    SUBDIVISION_FK,
                    (select subdivision.name from subdivision where subdivision.id = SUBDIVISION_FK), 
                    AUTHOR_FK,
                    (select account.lastname ||' '|| account.firstname from account where account.id = AUTHOR_FK),
                    FARMSTORAGE_FK,
                    (select farmstorage.storagelabel from farmstorage where farmstorage.id = FARMSTORAGE_FK),
                    (select patients.shortname from patients where patients.id = PATIENT_FK),
                    (select accountflow.ordernumber from accountflow where accountflow.id = ACCOUNTFLOW_FK),
                    (select accountflow.is_cashpayment from accountflow where accountflow.id = ACCOUNTFLOW_FK),
                    (select accountflow.operationtimestamp from accountflow where accountflow.id = ACCOUNTFLOW_FK)

                from FARMDOC

                where ID is not null"; 
                
                $where="";
                if(nonull($subdivId) && $subdivId != '0')
                {
                  $where.=" AND SUBDIVISION_FK = $subdivId";  
                }
                if(nonull($docType))
                {
                  $where.=" AND DOCTYPE = $docType";  
                }
                if(nonull($startDate))
                {
                  $where.=" AND DOCDATE >= '$startDate'";  
                }
                if(nonull($endDate))
                {
                  $where.=" AND DOCDATE <= '$endDate'";  
                }
                if(nonull($supplierId))
                {
                    $where.=" AND SUPPLIER_FK = $supplierId";  
                }
                if(nonull($farmstorageId) && $farmstorageId != '0')
                {
                    $where.=" AND FARMSTORAGE_FK = $farmstorageId";  
                }
                
                if(nonull($searchText) && $searchText != '')
                {
                    $where .= " and lower(SUMM||DOCDATE||DOCNUMBER) containing lower('".safequery($searchText)."')";
                }
                
    			$sqlGetDocs .= $where;
                $sqlGetDocs .= " order by CREATETIMESTAMP desc";	
			
			$queryGetDocs = ibase_prepare($trans, $sqlGetDocs);
			$resultGetDocs = ibase_execute($queryGetDocs);
            
            $result = array();
            
            while ($row = ibase_fetch_row($resultGetDocs))
            {
                $doc = new Storage_FarmDoc();
                $doc->id = $row[0];
                $doc->doctype = $row[1];
                $doc->supplier_fk = $row[2];
                $doc->supplier_name = $row[3];
                $doc->createtimestamp = $row[4];
                $doc->docdate = $row[5];
                $doc->summ = $row[6];
                $doc->docnumber = $row[7];
                $doc->note = $row[8];
                $doc->accountflow_fk = $row[9];
                $doc->patient_fk = $row[10];
                $doc->subdivision_fk = $row[11];
                $doc->subdivision_name = $row[12];
                $doc->author_fk = $row[13];
                $doc->author_name = $row[14];
                $doc->farmstorage_fk = $row[15];
                $doc->farmstorage_name = $row[16];
                $doc->patient_name = $row[17];
                $doc->orderNumber = $row[18];
                $doc->orderIsCashpayment = $row[19];
                $doc->orderOperationtimestamp = $row[20];
                $result[] = $doc;	     
            }
            
			ibase_free_query($queryGetDocs);
			ibase_free_result($resultGetDocs);
			
            ibase_commit($trans);
            $connection->CloseDBConnection();
			
			return $result;
    }
    
        /**
     * @param string $docId
     * @return Storage_FarmBath[]
     */
    public function getFarmDocBaths($docId)
    {
          $connection = new Connection();
         $connection->EstablishDBConnection();
         $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
         
         	$sqlGetBaths = "
                     select
                             ID,
                             FARM_FK,
                             (select farm.name from farm where farm.id = farmbath.farm_fk),
                             (select first 1 unitofmeasure.name from unitofmeasure
                                where unitofmeasure.farmid = farmbath.farm_fk and unitofmeasure.isbase = 1),
                             QUANTITY,
                             PRICE
                        
                        from FARMBATH
                        where farmbath.farmdoc_fk = $docId
                        order by id"; 
                
			
			$queryGetBaths = ibase_prepare($trans, $sqlGetBaths);
			$resultGetBaths = ibase_execute($queryGetBaths);
            
            $result = array();
            
            while ($row = ibase_fetch_row($resultGetBaths))
            {
                $bath = new Storage_FarmBath();
                $bath->id = $row[0];
                $bath->farm_fk = $row[1];
                $bath->farm_name = $row[2];
                $bath->farm_baseunit = $row[3];
                $bath->quantity = $row[4];
                $bath->price = $row[5];
                $result[] = $bath;	     
            }
            
			ibase_free_query($queryGetBaths);
			ibase_free_result($resultGetBaths);
			
            ibase_commit($trans);
            $connection->CloseDBConnection();
			
			return $result;
    }
    
    
     /**
     * @return Storage_Supplier[]
     */
    public function getSuppliers()//sqlite
    {
        $res=array();
            
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
               
        $sql = "select
                    s.id, s.name
                from supplier s 
                order by LOWER(s.name)";
  
        $query = ibase_prepare($trans, $sql);
        $result = ibase_execute($query);
        while ($row = ibase_fetch_row($result))
        {
            $sup = new Storage_Supplier();
            $sup->ID = $row[0];
            $sup->NAME = $row[1];
            $res[]=$sup;
        }
            
        ibase_free_query($query);
		ibase_free_result($result);
		  
        ibase_commit($trans);
        $connection->CloseDBConnection();
        	
		return $res;
    }
    
    /**
    * @param string $search
    * @return Storage_Farm[]
    */
    public function getFarms($search)//sqlite
    {
        $res=array();
            
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
               
       /* $sql = "select
                    ID,
                    NAME,
                    (select '<units>'||list('<unit><name>'||unitofmeasure.name||'</name><coefficient>'||unitofmeasure.coefficient||'</coefficient><isbase>'||unitofmeasure.isbase||'</isbase></unit>', '')||'</units>' from unitofmeasure
                        where unitofmeasure.farmid = farm.id)
                    from FARM where farm.nodetype = 1
                    order by lower(name)";
  */
        $sql = "select
                    ID,
                    NAME,
                    cast((select list('<unit><name>'||unitofmeasure.name||'</name><coefficient>'||unitofmeasure.coefficient||'</coefficient><isbase>'||unitofmeasure.isbase||'</isbase></unit>', '') from unitofmeasure
                        where unitofmeasure.farmid = farm.id) as VARCHAR(1000))
                    from FARM where farm.nodetype = 1
                    and lower(NAME) containing lower('".safequery($search)."')
                    order by lower(name)";
        $query = ibase_prepare($trans, $sql);
        $result = ibase_execute($query);
        while ($row = ibase_fetch_row($result))
        {
            $farm = new Storage_Farm();
            $farm->id = $row[0];
            $farm->name = $row[1];
            $farm->unitXml = $row[2];
            $res[]=$farm;
        }
            
        ibase_free_query($query);
		ibase_free_result($result);
		  
        ibase_commit($trans);
        $connection->CloseDBConnection();
        	
		return $res;
    }
    
    /**
    * @param string $search
    * @param Storage_FarmBath[] $farmsInDoc
    * @param boolean $isSpacer
    * 
    * @param boolean $isLowerOptimal
    * @param boolean $isLowerMinimum
    * 
    * @param string $subd_id
    * @param string $storage_id
    * 
    * @return Storage_FarmRemains[]
    */
    public function getFarmsRemains($search, $farmsInDoc, $isSpacer, $isLowerOptimal, $isLowerMinimum, $subd_id, $storage_id)
    {
        $res=array();
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
           
        /*$sql = "select
                    farmremains.farm_fk,
                    (select farm.name from farm where farm.id = farmremains.farm_fk) as farmname,
                    (select first 1 unitofmeasure.name from unitofmeasure where unitofmeasure.farmid = farmremains.farm_fk and unitofmeasure.isbase = 1),
                    farmremains.quantity,
                    farmremains.price,
                    farmremains.subdivision_fk
                    
                from farmremains  order by farmname, farmremains.price desc
 
                order by lower(name)";*/
                
        $sql = "select
                        fr.farm_fk,
                        farm.name,
                        (select first 1 unitofmeasure.name from unitofmeasure where unitofmeasure.farmid = fr.farm_fk and unitofmeasure.isbase = 1),
                        fr.quantity,
                        fr.price,
                        fr.subdivision_fk,
                        (select subdivision.name from subdivision where subdivision.id = fr.subdivision_fk),
                        farm.MINIMUMQUANTITY,
                        farm.OPTIMALQUANTITY,
                        (select farmstorage.storagelabel from farmstorage where farmstorage.id = fr.FARMSTORAGE_FK),
                        (select farmstorage.storagecolor from farmstorage where farmstorage.id = fr.FARMSTORAGE_FK)
                        
                from farmremains as fr
                inner join farm on farm.id = fr.farm_fk
                        
                where fr.quantity > 0 and lower(farm.name) containing lower('".safequery($search)."')"; // where fr.quantity != 0 and lower(farm.name) containing lower('".safequery($search)."')";
        
        if($isLowerOptimal)
        {
            $sql .= " and (select sum(fs.QUANTITY) from farmremains as fs where fs.farm_fk = fr.farm_fk) < farm.OPTIMALQUANTITY";  
        }
        else if($isLowerMinimum)
        {    
            $sql .= " and (select sum(fs.QUANTITY) from farmremains as fs where fs.farm_fk = fr.farm_fk) < farm.MINIMUMQUANTITY ";                        
        }
        
        if($subd_id != null && $subd_id != '' && $subd_id != '0')
        {
            $sql .= " and fr.SUBDIVISION_FK = $subd_id";  
        }
        
        if($storage_id != null && $storage_id != '' && $storage_id != '0')
        {
            $sql .= " and fr.FARMSTORAGE_FK = $storage_id"; 
        }
        
        
        /*if($isLowerOptimal && $isLowerMinimum)       
        {
            $sql .= " and (select sum(fs.QUANTITY) from farmremains as fs where fs.farm_fk = fr.farm_fk) <= farm.OPTIMALQUANTITY ";    
        }
        else if($isLowerOptimal)
        {
            $sql .= " and (select sum(fs.QUANTITY) from farmremains as fs where fs.farm_fk = fr.farm_fk) <= farm.OPTIMALQUANTITY and 
                            (select sum(fs.QUANTITY) from farmremains as fs where fs.farm_fk = fr.farm_fk) > farm.MINIMUMQUANTITY";  
        }
        else if($isLowerMinimum)
        {    
            $sql .= " and (select sum(fs.QUANTITY) from farmremains as fs where fs.farm_fk = fr.farm_fk) <= farm.MINIMUMQUANTITY ";                        
        }*/               
                        
        $sql .= " order by farm.name, fr.price desc";    
                    
                    
        $query = ibase_prepare($trans, $sql);
        $result = ibase_execute($query);
        $curFarmId = '';
        while ($row = ibase_fetch_row($result))
        {
            $farmRemain = new Storage_FarmRemains();
            $farmRemain->farm_id = $row[0];
            if($curFarmId == $farmRemain->farm_id && $isSpacer)
            {
                $farmRemain->farm_name = "    ".$row[1];
            }
            else
            {
                $farmRemain->farm_name = $row[1]; 
            }
            
            $farmRemain->farm_unitmeasure = $row[2];
            $farmRemain->quantity = $row[3];
            $farmRemain->price = $row[4];
            $farmRemain->subdivision_fk = $row[5];
            $farmRemain->subdivision_name = $row[6];
            $farmRemain->minimumquantity = $row[7];
            $farmRemain->optimalquantity = $row[8];
            $farmRemain->storageLabel = $row[9];
            $farmRemain->storageColor = $row[10];
            $curFarmId = $farmRemain->farm_id;
            
            if(isset($farmsInDoc) && $farmsInDoc != null)
            {
                foreach($farmsInDoc as $fb)
                {
                    if($farmRemain->farm_id == $fb->farm_fk && (float)$farmRemain->price == (float)$fb->price)
                    {
                        $farmRemain->quantity = $row[3] + $fb->quantity;
                        break;
                    }
                }
            }
            
            $res[] = $farmRemain;
        }
            
        ibase_free_query($query);
		ibase_free_result($result);
		  
        ibase_commit($trans);
        $connection->CloseDBConnection();
        	
		return $res;
    }
    
    /**
   * @param string $fio
   * @param boolean $includePhoneSearch
   * @return Storage_Patient[]
   */
   public function getPatientLazyList($fio, $includePhoneSearch)
   {
    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select patients.ID, 
                    patients.FIRSTNAME, patients.MIDDLENAME, patients.LASTNAME, patients.fullname,
                    patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER, patients.MOBILENUMBER
                 from patients ";
        if(($includePhoneSearch == true)&&(is_numeric($fio)))
        {
            $QueryText .= "inner join patientsmobile
                    on patients.id = patientsmobile.patient_fk ";
        }
        $QueryText .= "where 
                    ( lower( patients.LASTNAME||' '||patients.FIRSTNAME||' '||patients.MIDDLENAME) starting lower('$fio')
                    or lower(patients.CARDBEFORNUMBER||patients.CARDNUMBER||patients.CARDAFTERNUMBER) containing lower('$fio') ";
        if(($includePhoneSearch == true)&&(is_numeric($fio)))
        {
            $QueryText .= "or lower(patientsmobile.mobilenumber) containing lower('$fio') ) ";
        }
        else
        {
            $QueryText .= " ) ";
        }
        $QueryText .= "and PRIMARYFLAG = 1 and IS_ARCHIVED = 0 order by patients.LASTNAME";
                //patients.MOBILENUMBER
        $Query = ibase_prepare($trans, $QueryText);
        $QueryResult=ibase_execute($Query);
        $rows = array();
		while ($row = ibase_fetch_row($QueryResult))
		{ 
            $obj = new Storage_Patient;
            $obj->patient_id = $row[0];
            $obj->patient_lname = $row[1];
            $obj->patient_fname = $row[2];
            $obj->patient_sname = $row[3];
            $obj->patient_fio = $row[4];
            $obj->patient_label = $row[4].' ('.$row[5].') ';
            if($includePhoneSearch == true)
            {
                //$obj->patient_label .= $row[6];
            }
            $rows[] = $obj;
		}
        ibase_free_query($Query);
        ibase_free_result($QueryResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
   }
   
   
   // ****************** FARMSTORAGES ********************** START
   /**
     * @param string $subdivId
     * @param object $trans
     * @return Storage_FarmStorage[]
     */
    public function getFarmStorages($subdivId, $trans = null)
    {
        if($trans==null)
        {
            $connection = new Connection();
	        $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
         
         	$sql_get = "select
                             ID,
                             STORAGELABEL,
                             STORAGECOLOR,
                             DOCTOR_FK,
                             (select doctors.shortname from doctors where doctors.id = DOCTOR_FK),
                             SUBDIVISION_FK,
                             (select subdivision.name from subdivision where subdivision.id =  SUBDIVISION_FK)
                        from FARMSTORAGE ";
            if($subdivId != '-' && $subdivId != '' && $subdivId != '0')
            {
                $sql_get .= " where farmstorage.subdivision_fk = $subdivId "; 
            }       
            $sql_get .= " order by STORAGELABEL";
			
			$query_get = ibase_prepare($trans, $sql_get);
			$result_get = ibase_execute($query_get);
            
            $result = array();
            
            while ($row = ibase_fetch_row($result_get))
            {
                $fstor = new Storage_FarmStorage();
                $fstor->id = $row[0];
                $fstor->label = $row[1];
                $fstor->color = $row[2];
                $fstor->doctor_fk = $row[3];
                if(!is_null($fstor->doctor_fk) && $fstor->doctor_fk != '')
                {
                    $fstor->isDoctor = true;
                }
                else
                {
                    $fstor->isDoctor = false;
                }
                $fstor->doctor_shortname = $row[4];
                $fstor->subdivision_fk = $row[5];
                $fstor->subdivision_name = $row[6];
                $result[] = $fstor;	     
            }
            
			ibase_free_query($query_get);
			ibase_free_result($result_get);
			
            
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
			
			return $result;
    }
    
    /**
     * @param string $subdivId
     * @param Storage_FarmStorage $storage
     * @return Storage_FarmStorage[]
     */
    public function saveFarmStorages($subdivId, $storage)
    {
        $connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
        $save_sql = " update or insert into FARMSTORAGE (
                                                        ID, 
                                                        STORAGELABEL, 
                                                        STORAGECOLOR, 
                                                        DOCTOR_FK, 
                                                        SUBDIVISION_FK
                                                        )
                                            values (
                                                      ".nullcheck($storage->id).",
                                                      '".safequery($storage->label)."', 
                                                      ".$storage->color.",
                                                      ".nullcheck($storage->doctor_fk).",
                                                      ".$storage->subdivision_fk.") 
                                        matching (ID) returning (ID)";
       	if(Storage::$isDebugLog)
		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($save_sql,true));
		}
               		
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $storage->ID = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
        
        $resultDictionary = $this->getFarmStorages($subdivId, $trans);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();
        
        	
        return $resultDictionary;
        
    }
    
    
    /**
    * @param string $id
    * @return int
 	*/
    public function deleteFarmStoragesByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$resultCode = 0;
        
            $sql_get = "select coalesce(sum(QUANTITY),0) from FARMREMAINS where farmremains.farmstorage_fk = $id";
			$query_get = ibase_prepare($trans, $sql_get);
			$result_get = ibase_execute($query_get);
            
            if($row = ibase_fetch_row($result_get))
            {
               if($row[0] > 0)
               {
                    $resultCode = 2;
               }	
               else
               {
                    $QueryText = "delete from FARMSTORAGE where ID = $id";		
            		$query = ibase_prepare($trans, $QueryText);
            		ibase_execute($query);		
            		ibase_free_query($query);
                    $resultCode = 1;
               }     
            }
            
			ibase_free_query($query_get);
			ibase_free_result($result_get);
        	
        
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return $resultCode;
    }
    
   // ****************** FARMSTORAGES ********************** END
    
}
?>
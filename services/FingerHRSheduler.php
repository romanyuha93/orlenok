<?php

require_once "Connection.php";
define('tfClientId','hrimport');

class CSVData{
    public $file;
    public $data;
    public $fp;
    public $caption=true;
    public function CSVData($file=''){
        if ($file!='') getData($file);
    }
    function getData($file){
        if (strpos($file, 'tp://')!==false){
            copy ($file, 'C:/ToothFairyServer/att_log/log_tmp.csv');
            if ($this->fp=fopen('C:/ToothFairyServer/att_log/loglog_tmp.csv', 'r')!==FALSE){
                $this->readCSV();
                unlink('C:/ToothFairyServer/att_log/loglog_tmp.csv');
            }
        } else {
            $this->fp=fopen($file, 'r');
            $this->readCSV();
        }
        fclose($this->fp);
    }
    private function readCSV(){
        if ($this->caption==true){
            if (($captions=fgetcsv($this->fp, 1000, ","))==false) return false;
        }
        $row=0;
        while (($data = fgetcsv($this->fp, 1000, ",")) !== FALSE) {
            for ($c=0; $c < count($data); $c++) {
                $this->data[$row][$c]=$data[$c];
                if ($this->caption==true){
                    $this->data[$row][$captions[$c]]=$data[$c];
                }
            }
            $row++;
        }
    }
}

    $connection = new Connection();
	$connection->EstablishDBConnection();
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);

    $o=new CSVData();
    $o->getData('C:/ToothFairyServer/att_log/log.csv');
    $data=$o->data;
    
    for ($c=0; $c < count($data); $c++) 
    {
        $SqlDocId = "select first 1 id from doctors where doctors.fingerid = ".$data[$c][0];
        $QueryDocId = ibase_prepare($trans, $SqlDocId);
        $ResulDocId = ibase_execute($QueryDocId);
        if($row = ibase_fetch_row($ResulDocId))
        {
            if($row[0] != null && is_numeric($row[0]))
            {
                $php_date = new DateTime($data[$c][1]);
                $SqlAddHRData = "insert into WORKTABLE (POINTTIME, POINTTYPE, ASSISTANT_FK, DOCTOR_FK)
                                    values ('".$php_date->format("Y-m-d H:i:s")."', '".$data[$c][2]."', null, ".$row[0].")";
                $QueryAddHRData = ibase_prepare($trans, $SqlAddHRData);
            	ibase_execute($QueryAddHRData);
                ibase_free_query($QueryAddHRData);
            }
        }      
    }
    
    file_put_contents("C:/ToothFairyServer/att_log/log.csv", "");
    
	ibase_commit($trans);
	$connection->CloseDBConnection();
    

?>
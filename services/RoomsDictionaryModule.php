<?php

require_once "Connection.php";
require_once "Utils.php";

class RoomsDictionaryRoom
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $NAME;
     
    /**
    * @var string
    */
    var $DESCRIPTION; 
     
    /**
    * @var int
    */
    var $ROOMCOLOR; 
    
    /**
    * @var string
    */
    var $sequincenumber; 	
}

class RoomsDictionaryWorkPlace
{
	/**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $ROOMID;
    
    /**
    * @var string
    */
    var $NUMBER;
     
    /**
    * @var string
    */
    var $DESCRIPTION;        
}

class RoomsDictionaryModule
{		
   /**
   * @param RoomsDictionaryRoom $p1  
   * @param RoomsDictionaryWorkPlace $p2
   * @return void	    
  */    
	public function registertypes($p1, $p2) 
	{
	} 
    
	/** 
 	* @return RoomsDictionaryRoom[]
	*/ 
    public function getRooms() //sqlite
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, NUMBER, NAME, DESCRIPTION, ROOMCOLOR, sequincenumber from ROOMS order by rooms.sequincenumber, rooms.id";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))		// цикл по записам результату запиту
		{ 
			$obj = new RoomsDictionaryRoom;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->NAME = $row[2];
			$obj->DESCRIPTION = $row[3];
            if(($row[4] != null)||($row[4] != 0))
            {
                $obj->ROOMCOLOR = $row[4];
            }
            else
            {
                $obj->ROOMCOLOR = 16777215;//uint white color
            }
            $obj->sequincenumber = $row[5];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
   	/** 
 	* @param int $color
 	* @param string $roomid
    * @return boolean
	*/ 
    public function changeColorRoom($color, $roomid) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update ROOMS set ROOMCOLOR=$color where ID=$roomid";
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param RoomsDictionaryRoom $room
	* @return boolean
 	*/
    public function addRoom($room) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = 'EXECUTE PROCEDURE "ADDROOM"' . "($room->NUMBER, ";
		//if ("$room->NAME"=="")
		//{ $QueryText = $QueryText."null, "; }
		//else 
		//{ 
		  $QueryText = $QueryText."'".safequery($room->NAME)."', "; 
          //}
        
        
		//if ("$room->DESCRIPTION"=="")
		//{ 
		//  $QueryText = $QueryText."null, "; }
		//else 
		//{ 
		  $QueryText = $QueryText."'".safequery($room->DESCRIPTION)."', "; 
          //}
        
        
		if (($room->ROOMCOLOR=="")||($room->ROOMCOLOR==0))
		{ $QueryText .= "16777215, "; }//uint white color
		else
		{ $QueryText .= "'$room->ROOMCOLOR', "; }	
        $QueryText .= "$room->sequincenumber)";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;		
    }    
    
    /**
 	* @param RoomsDictionaryRoom $task
	* @return boolean
 	*/ 	 	
    public function updateRoom($room) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update ROOMS set NUMBER=$room->NUMBER, NAME='".safequery($room->NAME)."', 
                        DESCRIPTION='".safequery($room->DESCRIPTION)."', "; 
        
		/*if (($room->ROOMCOLOR=="")||($room->ROOMCOLOR==0))
		{ $QueryText .= "ROOMCOLOR=16777215, "; }//uint white color
		else
		{ $QueryText .= "ROOMCOLOR='$room->ROOMCOLOR', "; }	*/
        $QueryText .= "sequincenumber = $room->sequincenumber ";
		$QueryText .= "where ID=$room->ID";			
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;	
    }
    
    /**
 	* @param string[] $Arguments
	* @return boolean
 	*/ 		 		
    public function deleteRoom($Arguments) 
     {           	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from ROOMS where (ID is null)";		
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;			
    }
	
	/**
 	* @param string $RoomID
	* @return RoomsDictionaryWorkPlace[]
 	*/  	  
    public function getWorkPlaces($RoomID)     //sqlite
     {      
     	$connection = new Connection();
     	$connection->EstablishDBConnection();				
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, ROOMID, NUMBER, DESCRIPTION from WORKPLACES where ROOMID=$RoomID";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new RoomsDictionaryWorkPlace;
			$obj->ID = $row[0];
			$obj->ROOMID = $row[1];
			$obj->NUMBER = $row[2];
			$obj->DESCRIPTION = $row[3];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
	
	/**
 	* @param RoomsDictionaryWorkPlace $workPlace
	* @return boolean
 	*/
    public function addWorkPlace($workPlace) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into WORKPLACES (id, roomid, number, description)
		values(null, $workPlace->ROOMID, $workPlace->NUMBER, ";
		if ($workPlace->DESCRIPTION=="")
		{ $QueryText = $QueryText."null)"; }
		else
		{ $QueryText = $QueryText."'".safequery($workPlace->DESCRIPTION)."')"; }		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();	
		return $success;		
    }    
    
    /**
 	* @param RoomsDictionaryWorkPlace $WorkPlace
	* @return boolean
 	*/ 	 	
    public function updateWorkPlace($workPlace) 
     { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update WORKPLACES set NUMBER=$workPlace->NUMBER, ";
		if ($workPlace->DESCRIPTION=="")
		{ $QueryText = $QueryText."DESCRIPTION=null "; }
		else
		{ $QueryText = $QueryText."DESCRIPTION='".safequery($workPlace->DESCRIPTION)."' "; }	
		$QueryText = $QueryText."where ID=$workPlace->ID";			
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();		
		return $success;	
    }
    
    /**
 	* @param string[] $Arguments
	* @return boolean
 	*/ 		 		
    public function deleteWorkPlace($Arguments) 
     {           
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from WORKPLACES where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;			
    }
}

?>
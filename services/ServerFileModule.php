<?php

require_once "Connection.php";
require_once "Settings.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once (dirname(__FILE__).'/thumbnail/ThumbLib.inc.php');


class ServerFileModule_TagValue
{
    /**    
    * @var string    
    */    
    var $id;
    
    /**    
    * @var string    
    */    
    var $tagid;
    
    /**    
    * @var string    
    */    
    var $fileid;
    
    /**    
    * @var string    
    */    
    var $tagname;
    
    /**    
    * @var string    
    */    
    var $description;
}
class ServerFileModule_Protocol
{
     /**    
    * @var string    
    */    
    var $id;
    
    /**    
    * @var string    
    */    
    var $name;
}    
    
class ServerFileModule_ServerFile
{
    /**    
    * @var string    
    */    
    var $id;
    
    /**    
    * @var string    
    */    
    var $comment;
    
    /**    
    * @var string    
    */    
    var $extention;
    
    /**    
    * @var string    
    */    
    var $name;
    
    /**    
    * @var string    
    */    
    var $serverName;
    
    /**    
    * @var string    
    */    
    var $patientId;
    
    /**    
    * @var boolean    
    */    
    var $isThumbnailable;    
    
    /**    
    * @var Object    
    */    
    var $thumbData;
    
    /**    
    * @var ServerFileModule_TagValue[]    
    */    
    var $tagValues;
    
    /**    
    * @var string    
    */    
    var $tooth;    
    
    /**    
    * @var string    
    */    
    var $protocolid;   
    
    /**    
    * @var string    
    */    
    var $protocolName;
    
    /**
     * @var string
     */
     var $type;

}
class ServerFileModule_OrthancPatientData
{
     /**    
    * @var string    
    */    
    var $patientId;
    
     /**    
    * @var string    
    */    
    var $orthanc_patientId;
    
    /**    
    * @var string    
    */    
    var $orthanc_patientName;
}   

class ServerFileModule
{
               
     /**
     * @param ServerFileModule_ServerFile $p1
     * @param ServerFileModule_TagValue $p2
     * @param ServerFileModule_Protocol $p3
     * @return void
     */
    public function registertypes($p1,$p2,$p3)
    {}
    
    /**
     * @param string $patientId
     * 
     * @return ServerFileModule_OrthancPatientData
     */
    public function getOrthancPatientData($patientId)
    {
        $connection = new Connection();
    	$connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select patients.id, patients.orthancid, patients.orthancpname
                        from patients
                        where patients.id = $patientId";
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
	    $obj = new ServerFileModule_OrthancPatientData;
        if($row = ibase_fetch_row($result))
    	{
            $obj->patientId = $row[0];       
            $obj->orthanc_patientId = $row[1];    
    		$obj->orthanc_patientName = $row[2];
        }
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
    	$connection->CloseDBConnection();
        		
    	return $obj;
    }
    
    /** 						
    * @param ServerFileModule_ServerFile $sf			
    * @param resource $trans
    * 
	* @return boolean
	*/ 
	public static function addServerFile($sf, $trans=null) 
	{
	   $res=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
        			$QueryText = "insert into serverfile (
                                                            id,
                                                            name,
                                                            extention,
                                                            servername,
                                                            comment,
                                                            isthumbnailable,
                                                            tooth,
                                                            protocolid,
                                                            type, ";
                    if($sf->type == 1)
                    {
                        $QueryText .= " doctorsid ";
                    }
                    else if($sf->type == 2)
                    {
                        $QueryText .= " assistantsid ";
                    }
                    else if($sf->type == 3)
                    {
                        $QueryText .= " employeeid ";
                    }
                    else
                    {
                        $QueryText .= " patientid ";
                    }
                    
                    $QueryText .= ")  values (
                                                          null,
                                                          '".safequery($sf->name)."',
                                                          '".$sf->extention."',
                                                          '".$sf->serverName."',
                                                          '".safequery($sf->comment)."',
                                                          ".($sf->isThumbnailable?1:0).",
                                                          ".nullcheck($sf->tooth).",
                                                          ".nullcheck($sf->protocolid).",
                                                          ".nullcheck($sf->type).", 
                                                          ".nullcheck($sf->patientId).")";
                //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."***addServerFile***".$QueryText);	
    			$query = ibase_prepare($trans, $QueryText);
    			ibase_execute($query);
    			ibase_free_query($query);

			
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
     /** 						
    * @param ServerFileModule_ServerFile $sf			
    * @param resource $trans
    * 
	* @return boolean
	*/ 
	public static function updateServerFile($sf, $trans=null) 
	{
	   $res=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
      
        			$QueryText = "update or insert into serverfile (
                                                            id,
                                                            name,
                                                            extention,
                                                            servername,
                                                            comment,
                                                            isthumbnailable,
                                                            patientid,
                                                            tooth,
                                                            protocolid)
                                                          values (
                                                          ".nullcheck($sf->id).",
                                                          '".$sf->name."',
                                                          '".$sf->extention."',
                                                          '".$sf->serverName."',
                                                          '".safequery($sf->comment)."',
                                                          ".($sf->isThumbnailable?1:0).",
                                                          ".nullcheck($sf->patientId).",
                                                          ".nullcheck($sf->tooth).",
                                                          ".nullcheck($sf->protocolid).")
                                                          matching(servername)";
                //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."***updateServerFile***".$QueryText);		
    			$query = ibase_prepare($trans, $QueryText);
    			ibase_execute($query);
    			ibase_free_query($query);

			
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
     /** 						
    * @param string $sfId
    * @param string $sfName				
    * @param resource $trans
    * 
	* @return boolean
	*/ 
	public static function renameServerFile($sfId, $sfName, $trans=null) 
	{
	   $res=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
      
        			$QueryText = "update serverfile set name='$sfName' where id=$sfId";
	
    			$query = ibase_prepare($trans, $QueryText);
    			ibase_execute($query);
    			ibase_free_query($query);

			
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
     /** 						
    * @param string $sfId	
    * @param resource $trans
    * 
	* @return boolean
	*/ 
	public static function deleteServerFile($sfId, $trans=null) 
	{
	   $res=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
                $deleted_success = true;
                $sf=ServerFileModule::getServerFile($sfId, 'id', false, false, $trans);
                if(nonull($sf))
                {
                    if($sf->isThumbnailable)
                    {
                        if(file_exists(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb'))
                        {
                            try
                            {
                    			$deleted_success = @unlink(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb');
                    		}
                            catch(Exception $exc)
                            { 
                                $deleted_success = false; 
                            }
                        }
                    }
                    if(file_exists(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName))
                    {
                        try
                        {
                			$deleted_success = @unlink(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName);
                		}
                        catch(Exception $exc)
                        { 
                            $deleted_success = false; 
                        }
                    }
                }
              
                if($deleted_success == false)
                {
                    return false;
                }
      
   			      $QueryText = "delete from serverfile where id=$sfId";
                    	
    			$query = ibase_prepare($trans, $QueryText);
    			ibase_execute($query);
    			ibase_free_query($query);

			
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
    /** 						
    * @param string $whereValue	
    * @param string $whereField	
    * @param boolean $withThumb			
    * @param resource $trans
    * 
	* @return ServerFileModule_ServerFile
	*/ 
	public static function getServerFile($whereValue, $whereField, $withThumb=true, $withTags=true, $trans=null) 
	{
	   	   $res=array();

            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select sf.id,
                                 sf.name,
                                 sf.extention,
                                 sf.servername,
                                 sf.comment,
                                 sf.isthumbnailable,
                                 sf.patientid,
                                 sf.tooth,
                                 sf.protocolid,
                                 p.id,
                                 p.name
                                 from serverfile sf
                                 left join protocols p on (p.id=sf.protocolid)
                        where sf.$whereField='$whereValue'";	
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            $sf=null;
            if ($row = ibase_fetch_row($result))
            {
                $sf=new ServerFileModule_ServerFile;
                $sf->id=$row[0];	
                $sf->name=$row[1];
                $sf->extention=$row[2];
                $sf->serverName=$row[3];
                $sf->comment=$row[4];
                $sf->isThumbnailable=$row[5]==1;
                $sf->patientId=$row[6];	
                $sf->tooth=$row[7];	
                $sf->protocolid=$row[8];	
                $sf->protocolName=$row[9];
                if($sf->isThumbnailable&&$withThumb)
                {
                   
                    $sf->thumbData=base64_encode(file_get_contents(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb'));
                }
                if($withTags)
                {
                    $sf->tagValues=ServerFileModule::getTagValues($sf->id, $trans);
                }
                
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $sf;
	}
    
    /** 						
    * @param string $patientId
    * @param string $searchText	
    * @param string $tagId
    * @param boolean $byVals
    * @param string $tooth	
    * @param string $protocolid	
    * @param string $dateFilter	
    * @param string $typeFilter	
    * @param resource $trans
    * 
	* @return ServerFileModule_ServerFile[]
	*/ 
	public static function getServerFiles($patientId, $searchText, $tagId, $byVals, $tooth, $protocolid, $dateFilter, $typeFilter, $trans=null) 
	{
	   	   $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }

			$QueryText = "select distinct
                                 sf.id,
                                 sf.name,
                                 sf.extention,
                                 sf.servername,
                                 sf.comment,
                                 sf.isthumbnailable,
                                 sf.patientid,
                                 sf.tooth,
                                 p.id,
                                 p.name
                                 from serverfile sf
                                 left join protocols p on (p.id=sf.protocolid)";
                 if($byVals)
                 {
                    $QueryText.=' left join serverfiletagvalue tv on tv.fileid=sf.id';  
                 }
                 else if(nonull($tagId))
                 {
                    $QueryText.=' left join serverfiletagvalue tv on tv.fileid=sf.id';  
                 }
                $where=array();       	
                if($patientId!="")
                {  
                    if(nonull($typeFilter))
                    {
                        if($typeFilter == 1)
                        {
                            $where[]="sf.doctorsid = $patientId";
                        }
                        else if($typeFilter == 2)
                        {
                            $where[]="sf.assistantsid = $patientId";
                        }
                        else if($typeFilter == 3)
                        {
                            $where[]="sf.employeeid = $patientId";
                        }
                        else
                        {
                            $where[]="sf.patientid = $patientId";
                        }
                        $where[]="sf.type = $typeFilter";
                    }
                    else
                    {
                        $where[]="sf.patientid = $patientId";
                    }
                    if(nonull($dateFilter))
                    {
                        $where[]="(sf.CREATETIMESTAMP >= '$dateFilter 00:00:00' and sf.CREATETIMESTAMP <= '$dateFilter 23:59:59')";
                    }
                    
                }
                else if(isset($patientId)==false)
                {
                    $where[]="sf.patientid is null";  
                }
                if(nonull($tagId))
                {
                    $where[]="tv.tagid = $tagId";  
                }
                if(nonull($searchText))
                {
                     if($byVals)
                     {
                        $where[]="coalesce(lower(sf.comment),'')||' '||coalesce(lower(tv.description),'') like lower('%$searchText%')";  
                     }
                     else
                    {
                         $where[]="coalesce(lower(sf.name),'')||' '||coalesce(lower(sf.servername),'') like lower('%$searchText%')";
                    }
                }
                if(nonull($tooth))
                {
                    $where[]="sf.tooth = '$tooth'";  
                }                
                if(nonull($protocolid))
                {
                    $where[]="sf.protocolid = $protocolid";  
                }
                if(count($where)>0)
                {
                    $QueryText.=' where '.implode(' and ',$where);
                }
                
  	//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            $res=array();
            while ($row = ibase_fetch_row($result))
            {
                $sf=new ServerFileModule_ServerFile;
                $sf->id=$row[0];	
                $sf->name=$row[1];
                $sf->extention=$row[2];
                $sf->serverName=$row[3];
                $sf->comment=$row[4];
                $sf->isThumbnailable=$row[5]==1;
                $sf->patientId=$row[6];	
                $sf->tooth=$row[7];	
                $sf->protocolid=$row[8];
                $sf->protocolName=$row[9];
                if($sf->isThumbnailable)
                {
                    if(file_exists(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb') == 1)
                    {
                        $sf->thumbData = base64_encode(file_get_contents(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb'));
                    }
                }
                
                $sf->tagValues=ServerFileModule::getTagValues($sf->id, $trans);
                
                $res[]=$sf;
                
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

			return $res;
	}
    /** 						
    * @param string $searchText	
    * @param resource $trans
    * 
	* @return ServerFileModule_Protocol[]
	*/ 
	public static function getProtocols($searchText, $trans=null) //sqlite
	{
	   	   $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }

			$QueryText = "select first 5 p.id,
                                 p.name
                                 from protocols p 
                                 where lower(p.name) like lower('%$searchText%')";
                	//  file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            $res=array();
            while ($row = ibase_fetch_row($result))
            {
                $p=new ServerFileModule_Protocol;
                $p->id=$row[0];	
                $p->name=$row[1];                
                $res[]=$p;
                
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

			return $res;
	}
    
     /** 						
    * @param string id	
    * @param resource $trans
    * 
	* @return ServerFileModule_Protocol
	*/ 
	public static function getProtocolById($id, $trans=null) //sqlite
	{
	   	   $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }

			$QueryText = "select p.id,
                                 p.name
                                 from protocols p 
                                 where p.id=$id";
                	//  file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
           $p=new ServerFileModule_Protocol;
            if ($row = ibase_fetch_row($result))
            {
                
                $p->id=$row[0];	
                $p->name=$row[1];                
                
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

			return $p;
	}
    
     /** 						
    * @param string $fileId	
    * @param resource $trans
    * 
	* @return ServerFileModule_TagValue[]
	*/ 
	public static function getTagValues($fileId, $trans=null) 
	{
	   	   $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select tv.id,
                                tv.tagid,
                                tv.fileid,
                                tv.description,
                                t.description from serverfiletagvalue tv
                                left join patientcarddictionaries t on (t.id=tv.tagid)
                                where tv.fileid=$fileId";
                                 	
                
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            $res=array();
            while ($row = ibase_fetch_row($result))
            {
                $tv=new ServerFileModule_TagValue;
                $tv->id=$row[0];	
                $tv->tagid=$row[1];
                $tv->fileid=$row[2];
                $tv->description=$row[3];
                $tv->tagname=$row[4];
                $res[]=$tv;
                
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

			return $res;
	}
    
    
         /** 						
    * @param ServerFileModule_TagValue[] $sfVals
    * @param String[] $idsToDelete			
    * @param resource $trans
    * 
	* @return boolean
	*/ 
	public static function updateServerFileTagValues($sfVals, $idsToDelete, $trans=null) 
	{
	       $res=true;
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
        if(count($idsToDelete)>0)
          {
       			$QueryText = "delete from serverfiletagvalue where id in (".implode($idsToDelete,',').")";
                            
        			$query = ibase_prepare($trans, $QueryText);
        			ibase_execute($query);
        			ibase_free_query($query);
          }
          
          foreach($sfVals as $sfVal)  			
          {
                    			$QueryText = "update or insert into serverfiletagvalue (
                                                                id,
                                                                tagid,
                                                                fileid,
                                                                description)
                                                              values (
                                                              ".nullcheck($sfVal->id).",
                                                              ".nullcheck($sfVal->tagid).",
                                                              ".nullcheck($sfVal->fileid).",
                                                              '".safequery($sfVal->description)."'
                                                              )";
                             //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);	
        			$query = ibase_prepare($trans, $QueryText);
        			ibase_execute($query);
        			ibase_free_query($query);
          }
			
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
    /** 						
    * @param string $serverName			
    * 
	* @return Object
	*/ 
	public static function getServerFileContent($serverName) 
	{         
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."***getServerFileContent***");
        return base64_encode(file_get_contents(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$serverName));
    }
    
    /** 						
    * @param string $serverName			
    * @param Object $data
    * 
	* @return Object
	*/ 
	public static function putServerFileContent($serverName, $data) 
	{         
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."***putServerFileContent***");
        file_put_contents(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$serverName,base64_decode($data));
        try 
        {
            $thumb = PhpThumbFactory::create(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$serverName);  
        } 
        catch (Exception $e) 
        {
            return null;
        } 
        $thumb->resize(160, 160)->save(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$serverName.'_thumb');
         
        return base64_encode(file_get_contents(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$serverName.'_thumb'));
    }
}
?>
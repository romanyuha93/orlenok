<?php

require_once "Connection.php";
require_once "Utils.php";

define('sqlite_folder', dirname(__FILE__).'/sqlite/');

//define('tfClientId', 'online');
//SyncDictionaryModule::syncDictionaries(array(new SyncDictionaryModuleSumm('CLINICREGISTRATIONDATA',999)));


 //echo SyncDictionaryModule::getFirebirdIdString('HEALTHPROC','');


class SyncDictionaryModule
{      
    /**
      * @param SyncDictionaryModuleSumm $p1
     * @return void
     */
    public function registerTypes($p1){}
    
    /**
     * @param SyncDictionaryModuleSumm[] $clientSumms                     
     * @param resource $trans
     * @return Object;
     */
   public static function syncDictionaries($clientSumms, $trans=null)
   {
       //file_put_contents("C:/tmp.txt", var_export($clientSumms, true));
        $res=array();
        if($trans==null)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 

		if ($handle = opendir(sqlite_folder)) 
		{
			while (false !== ($file = readdir($handle))) 
			{
				if(pathinfo(sqlite_folder.$file, PATHINFO_EXTENSION)!='sqlite')
				{
					@unlink(sqlite_folder.$file);
				}
			}
		}
        foreach($clientSumms as $clientSumm)
        {
            if(nonull($clientSumm->NAME))
            {
                $path=SyncDictionaryModule::syncDictionary($clientSumm,$trans);
                if(nonull($path))
                { 
                    $res[]=$path;
                }
            }
           
        }
        $filename=null;
        if(count($res)>0)
        {
            $filename = sqlite_folder.uniqid()."sqlite.zip";
            $zip = new ZipArchive();
            $zip->open($filename, ZIPARCHIVE::CREATE);
            foreach($res as $path)
            {
                $zip->addFile(sqlite_folder.$path,$path);
            }
            
            $zip->close();
            
        }
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        if(nonull($filename))
        {
             return base64_encode(file_get_contents($filename));
        }
        else
        {
            return null;
        }
       
   }
   
    /**
     * @param SyncDictionaryModuleSumm[] $clientSumm                 
     * @param resource $trans
     * @return string;
     */
   public static function syncDictionary($clientSumm, $trans=null)
   {
        $path=null;
        if($trans==null)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 

        $firebirdSumm=SyncDictionaryModule::getFirebirdSumm($clientSumm->NAME,$trans);
        $dbExist=file_exists(sqlite_folder.$clientSumm->NAME.'.sqlite')&&(filesize(sqlite_folder.$clientSumm->NAME.'.sqlite')>0);
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n $clientSumm->NAME firebirdSumm=".$firebirdSumm);

        if($firebirdSumm!=$clientSumm->SUMM||$dbExist==false||$clientSumm->SUMM=='NaN')
        {
        
            $className=$clientSumm->NAME;
            if($className!="MKB10"&&nonull($className))
            {
                //echo("\n $clientSumm->NAME firebirdSumm=$firebirdSumm clientSumm=$clientSumm->SUMM");
                $db = new SQLite3(sqlite_folder.$className.'.sqlite');
                $sqliteSumm=0;
                if($dbExist==false)
                {
                    $db->query('CREATE TABLE IF NOT EXISTS DICTIONARY ('.$className::sqliteCreateVarString().')');
                   
                }
                else
                {
                      $sqliteSumm=SyncDictionaryModule::getSQLiteSumm($db);
                }            
              //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$clientSumm->NAME.' '.$firebirdSumm.' '.$sqliteSumm."\n");
                //echo("\n".$clientSumm->NAME.' '.$firebirdSumm.' '.$sqliteSumm);
                if($firebirdSumm!=$sqliteSumm)
                {          
                    
                    if($sqliteSumm!=0)
                    {                        
                        $firebirdIdStr=SyncDictionaryModule::getFirebirdIdString($className, $trans);
                        SyncDictionaryModule::clearSQLiteDictionary($db,$firebirdIdStr);
                        $sqliteIdStr=SyncDictionaryModule::getSQLiteIdString($db);                        
                         
                    }
                    else
                    {
                        $sqliteIdStr="";
                    }
                    
                    $res=SyncDictionaryModule::getFirebirdDictionary($className, $sqliteIdStr, $trans);
                    SyncDictionaryModule::setSQLiteDictionary($db,$className,$res);
                    
                    
                }
                $db->close();
            }
            
            $path=$className.'.sqlite';
            
            
        }
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $path;
   }
    /**
     * @param string $className 
     * @param string $sqliteIdStr                   
     * @param resource $trans
     * @return Object[]
     */
   public static function getFirebirdDictionary($className, $sqliteIdStr, $trans=null)
   {
        $res=array();
        
        if($trans==null)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
        $blob_id_str=null;
        $blob_id=null;
        $var_datablob=$sqliteIdStr;
        $blob_id = ibase_blob_create($trans);
         ibase_blob_add($blob_id, $var_datablob);
        $blob_id_str = ibase_blob_close($blob_id);
 
 
    	$QueryText = "select ".SyncDictionaryModule::classVarString($className)." from ".$className.$sqliteIdStr;
                                        
        if(strlen($QueryText)>65535)        
        {
            
            $var_datablob=substr($sqliteIdStr, 8, strlen($sqliteIdStr)-101);            
            $blob_id = ibase_blob_create($trans);
            ibase_blob_add($blob_id, $var_datablob);
            $blob_id_str = ibase_blob_close($blob_id);
            $QueryText = "select ".SyncDictionaryModule::classVarString($className)." from ".$className." where cast(? as blob sub_type 1) not containing '('||(case when id>=0 then id else -(id+2147483648) end)||'_'||version||')'";
            $query = ibase_prepare($trans, $QueryText);
    	   $result = ibase_execute($query,$blob_id_str);
        }
        else
        {
       	    $query = ibase_prepare($trans, $QueryText);
    	   $result = ibase_execute($query);
        }
    	//file_put_contents("C:/tmp.txt", $QueryText);

    	
    	while($row = ibase_fetch_row($result))
    	{				
            $res[]=new $className($row);
    	}			
    	//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n res=".var_export($res,true));
               
    	ibase_free_query($query);
    	ibase_free_result($result);           
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $res;
   }
   
    /**
     * @param SQLite3 $db
     * @return int
     */
   public static function getSQLiteSumm($db)
   {
        $summ=0;
        $result = $db->query('SELECT sum(version)*1000000+sum(id) from DICTIONARY');
        if ($result!=null&&$row = $result->fetchArray()) 
        {
            $summ=$row[0];
        }

        return $summ;
   }
   
       /**
     * @param SQLite3 $db
     * @return string
     */
   public static function getSQLiteIdString($db)
   {
        $str='';
        $result = $db->query("select group_concat('('||(case when id>=0 then id else -(id+2147483648) end)||'_'||VERSION||')','') from dictionary");
        if ($result!=null&&$row = $result->fetchArray()) 
        {
            if(nonull($row[0]))
            {
                $str=" where '".$row[0]."' not containing '('||(case when id>=0 then id else -(id+2147483648) end)||'_'||version||')' ";
            }
            
        }

        return $str;
   }
   
   /**
    * @param SQLite3 $db
    * @param string $firebirdIdStr
    */
   public static function clearSQLiteDictionary($db,$firebirdIdStr)
   {

        $db->query("DELETE FROM DICTIONARY".$firebirdIdStr);
   }
   
   /**
    * @param SQLite3 $db
    * @param string $className  
    * @param Object[] $res
    */
   public static function setSQLiteDictionary($db, $className, $res)
   {
        foreach($res as $r)
        {
            $result=$db->query("REPLACE INTO DICTIONARY (".SyncDictionaryModule::classVarString($className).") values(".SyncDictionaryModule::valuesString($className, $r).")"); 
            if(!$result)
            {
                $lastErrorMsg=$db->lastErrorMsg();
                if(strstr($lastErrorMsg,'table DICTIONARY has no column named '))
                {
                    $dbColumn=substr($lastErrorMsg,37);
                    preg_match_all("/$dbColumn ([^,]+),/", trim($className::sqliteCreateVarString()), $matches);
                    $dbType=$matches[1][0];
                    $db->query('ALTER TABLE DICTIONARY ADD COLUMN '.$dbColumn.' '.$dbType);
                    return SyncDictionaryModule::setSQLiteDictionary($db, $className, $res);                    
                }
            }
          
        }
    
   }
   
   /**
     * @param string $className                    
     * @param resource $trans
     * @return int
     */
   public static function getFirebirdSumm($className, $trans=null)
   {
        $summ=0;
        if($trans==null)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
    
     
    	$QueryText = "select sum(version)*1000000+sum(id) from ".$className;
    	
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n ".$QueryText);
    	
        $query = ibase_prepare($trans, $QueryText);
    	$result = ibase_execute($query);
    	$res=array();
    	if($row = ibase_fetch_row($result))
    	{				
            if(nonull($row[0]))
            {
                $summ=$row[0];
            }
    	}			
    	
    	ibase_free_query($query);
    	ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }

        return $summ;
   }
   /**
     * @param string $className                   
     * @param resource $trans
     * @return string
     */
   public static function getFirebirdIdString($className, $trans=null)
   {
        $str='';
        if($trans==null)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
    

    	$QueryText = "select Cast(list(id,', ') as blob sub_type 1 character set UTF8) from ".$className;
    	$query = ibase_prepare($trans, $QueryText);
    	$result = ibase_execute($query);
    	if($row = ibase_fetch_row($result, IBASE_TEXT))
    	{				
             if(nonull($row[0]))
             {
                $str=' where id not in ('.$row[0].')';
             }
    	}			
    	
    	ibase_free_query($query);
    	ibase_free_result($result);                       
               
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }

        return $str;
   }
   public static function classVarString($className)
    {
        $fields=array_keys(get_class_vars($className));
        return implode(", ",$fields);
    }
    public static function valuesString($className, $object)
    {
        $vals=array();
        $fields=array_keys(get_class_vars($className));
       foreach($fields as $field)
       {
            $vals[]=nullcheck($object->$field,'true');
       }
       return implode(', ',$vals);
    }
}
class SyncDictionaryModuleSumm
{
     /**
    * @var string
    */
    var $NAME;
    /**
    * @var int
    */
    var $SUMM;
    
    function __construct($name, $summ=0) 
    {
       $this->NAME=$name;
       $this->SUMM=$summ;
    }
}
class PATIENTCARDDICTIONARIES
{
    
    /**
    * @var int
    */
    var $ID;
    /**
    * @var int
    */
    var $NUMBER;
    
    /**
    * @var string
    */
    var $DESCRIPTION;
    
    /**
    * @var int
    */
    var $TYPE;
    
    /**
    * @var int
    */
    var $COLOR;
    
    /**
    * @var int
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, number INTEGER, description TEXT, type INTEGER, color INTEGER, version INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}

class PROTOCOLS                      
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var int     
    */
    var $MKB10;

    /**
    * @var int     
    */
    var $F43;

    /**
    * @var textblob
    */
    var $PROTOCOLTXT;

    /**
    * @var textblob  
    */
    var $ANAMNESTXT;

    /**
    * @var textblob  
    */
    var $STATUSTXT;

    /**
    * @var textblob  
    */
    var $RECOMENDTXT;

    /**
    * @var int     
    */
    var $DOCTOR;

    /**
    * @var string  
    */
    var $CREATEDATE;

    /**
    * @var int     
    */
    var $HEALTHTYPE;

    /**
    * @var textblob
    */
    var $EPICRISISTXT;

    /**
    * @var textblob
    */
    var $RENTGENTXT;

    /**
    * @var int     
    */
    var $VERSION;

    /**
    * @var int     
    */
    var $ISQUICK;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , MKB10 INTEGER, F43 INTEGER, PROTOCOLTXT BLOB   , ANAMNESTXT BLOB   , STATUSTXT BLOB   , RECOMENDTXT BLOB   , DOCTOR INTEGER, CREATEDATE TEXT   , HEALTHTYPE INTEGER, EPICRISISTXT BLOB   , RENTGENTXT BLOB   , VERSION INTEGER, ISQUICK INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                if($field=='PROTOCOLTXT'||$field=='EPICRISISTXT'||$field=='RENTGENTXT'||$field=='ANAMNESTXT'||$field=='STATUSTXT'||$field=='RECOMENDTXT')
                {
                     $this->$field=text_blob_encode($row[$key]);
                }
                else
                {
                     $this->$field=$row[$key];
                }
               
           }

       }
    }
}

class ASSISTANTS
{
    
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $TIN;

    /**
    * @var string  
    */
    var $FIRSTNAME;

    /**
    * @var string  
    */
    var $MIDDLENAME;

    /**
    * @var string  
    */
    var $LASTNAME;

    /**
    * @var string  
    */
    var $FULLNAME;

    /**
    * @var string  
    */
    var $SHORTNAME;

    /**
    * @var string  
    */
    var $BIRTHDAY;

    /**
    * @var int     
    */
    var $SEX;

    /**
    * @var string  
    */
    var $DATEWORKSTART;

    /**
    * @var string  
    */
    var $DATEWORKEND;

    /**
    * @var string  
    */
    var $MOBILEPHONE;

    /**
    * @var string  
    */
    var $HOMEPHONE;

    /**
    * @var string  
    */
    var $EMAIL;

    /**
    * @var string  
    */
    var $SKYPE;

    /**
    * @var string  
    */
    var $HOMEADRESS;

    /**
    * @var string  
    */
    var $POSTALCODE;

    /**
    * @var string  
    */
    var $STATE;

    /**
    * @var string  
    */
    var $REGION;

    /**
    * @var string  
    */
    var $CITY;

    /**
    * @var string  
    */
    var $SALARYSCHEMA;

    /**
    * @var string  
    */
    var $BARCODE;
    
    /**
    * @var int  
    */
    var $ACCOUNTID;
    
    /**
    * @var int
    */
    var $ISFIRED; 
    
    /**
    * @var string  
    */
    var $FINGERID;
	
    /**
    * @var int     
    */
    var $VERSION;
 
	
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, TIN TEXT   , FIRSTNAME TEXT   , MIDDLENAME TEXT   , LASTNAME TEXT   , FULLNAME TEXT   , SHORTNAME TEXT   , BIRTHDAY TEXT   , SEX INTEGER, DATEWORKSTART TEXT   , DATEWORKEND TEXT   , MOBILEPHONE TEXT   , HOMEPHONE TEXT   , EMAIL TEXT   , SKYPE TEXT   , HOMEADRESS TEXT   , POSTALCODE TEXT   , STATE TEXT   , REGION TEXT   , CITY TEXT   , SALARYSCHEMA TEXT   , BARCODE TEXT  , ACCOUNTID INTEGER, ISFIRED INTEGER , FINGERID TEXT, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}

class DOCTORS
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $TIN;

    /**
    * @var string  
    */
    var $FIRSTNAME;

    /**
    * @var string  
    */
    var $MIDDLENAME;

    /**
    * @var string  
    */
    var $LASTNAME;

    /**
    * @var string  
    */
    var $FULLNAME;

    /**
    * @var string  
    */
    var $SHORTNAME;

    /**
    * @var string  
    */
    var $BIRTHDAY;

    /**
    * @var int     
    */
    var $SEX;

    /**
    * @var string  
    */
    var $SPECIALITY;

    /**
    * @var string  
    */
    var $DATEWORKSTART;

    /**
    * @var string  
    */
    var $DATEWORKEND;

    /**
    * @var string  
    */
    var $MOBILEPHONE;

    /**
    * @var string  
    */
    var $HOMEPHONE;

    /**
    * @var string  
    */
    var $EMAIL;

    /**
    * @var string  
    */
    var $SKYPE;

    /**
    * @var string  
    */
    var $HOMEADRESS;

    /**
    * @var string  
    */
    var $POSTALCODE;

    /**
    * @var string  
    */
    var $STATE;

    /**
    * @var string  
    */
    var $REGION;

    /**
    * @var string  
    */
    var $CITY;

    /**
    * @var int     
    */
    var $HEALTHTYPE;

    /**
    * @var string  
    */
    var $SALARYSCHEMA;

    /**
    * @var string  
    */
    var $BARCODE;

    /**
    * @var int  
    */
    var $ACCOUNTID;
	    	    
    /**
    * @var int
    */
    var $ISFIRED;  
    
    /**
    * @var string
    */
    var $FINGERID; 
    
    /**
    * @var int     
    */
    var $VERSION;

	
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, TIN TEXT   , FIRSTNAME TEXT   , MIDDLENAME TEXT   , LASTNAME TEXT   , FULLNAME TEXT   , SHORTNAME TEXT   , BIRTHDAY TEXT   , SEX INTEGER, SPECIALITY TEXT   , DATEWORKSTART TEXT   , DATEWORKEND TEXT   , MOBILEPHONE TEXT   , HOMEPHONE TEXT   , EMAIL TEXT   , SKYPE TEXT   , HOMEADRESS TEXT   , POSTALCODE TEXT   , STATE TEXT   , REGION TEXT   , CITY TEXT   , HEALTHTYPE INTEGER, SALARYSCHEMA TEXT   , BARCODE TEXT   , ACCOUNTID INTEGER, ISFIRED INTEGER , FINGERID TEXT, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class F43
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $F43;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $SIGN;

    /**
    * @var string  
    */
    var $COMPLAINTS;

    /**
    * @var string  
    */
    var $FLAGTOOTH;

    /**
    * @var string  
    */
    var $FLAGSURFACE;

    /**
    * @var string  
    */
    var $FLAGROOT;

    /**
    * @var int     
    */
    var $SEQNUMBER;
    
    /**
    * @var int     
    */
    var $ISQUICK;
    
    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, F43 INTEGER, NAME TEXT   , SIGN TEXT   , COMPLAINTS TEXT   , FLAGTOOTH TEXT   , FLAGSURFACE TEXT   , FLAGROOT TEXT   , SEQNUMBER INTEGER, ISQUICK INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class F39
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $DESCRIPTION;

    /**
    * @var int     
    */
    var $PARRENT;

    /**
    * @var int     
    */
    var $TYPE;

    /**
    * @var int     
    */
    var $FORM;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NUMBER INTEGER, DESCRIPTION TEXT   , PARRENT INTEGER, TYPE INTEGER, FORM INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class F39HPROC
{
    
/**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $F39ID;

    /**
    * @var int     
    */
    var $HEALTHPROCID;

    /**
    * @var int     
    */
    var $UNSIGNED;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, F39ID INTEGER, HEALTHPROCID INTEGER, UNSIGNED INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class ROOMS
{
    
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $DESCRIPTION;

    /**
    * @var int     
    */
    var $ROOMCOLOR;

    /**
    * @var int     
    */
    var $SEQUINCENUMBER;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NUMBER INTEGER, NAME TEXT   , DESCRIPTION TEXT   , ROOMCOLOR INTEGER, SEQUINCENUMBER INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class WORKPLACES
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $ROOMID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $DESCRIPTION;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, ROOMID INTEGER, NUMBER INTEGER, DESCRIPTION TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class TEMPLATE
{
    
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $PROTOCOL;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $CREATEDATE;

    /**
    * @var int     
    */
    var $DOCTOR;

    /**
    * @var int     
    */
    var $F43AFTER;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, PROTOCOL INTEGER, NAME TEXT   , CREATEDATE TEXT   , DOCTOR INTEGER, F43AFTER INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class TEMPLATEPROC
{
    
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $TEMPLATE;

    /**
    * @var int     
    */
    var $PROTOCOLPROC;

    /**
    * @var int     
    */
    var $VISIT;

    /**
    * @var int     
    */
    var $STACKNUMBER;

    /**
    * @var int     
    */
    var $HEALTHPROC;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, TEMPLATE INTEGER, PROTOCOLPROC INTEGER, VISIT INTEGER, STACKNUMBER INTEGER, HEALTHPROC INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class TEETHEXAMDICTIONARIES
{
    
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $DESCRIPTION;

    /**
    * @var int     
    */
    var $TYPE;

    /**
    * @var string  
    */
    var $SPECIALITY;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NUMBER INTEGER, DESCRIPTION TEXT   , TYPE INTEGER, SPECIALITY TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class PROTOCOLS_F43
{
/**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $PROTOCOL_FK;

    /**
    * @var int     
    */
    var $F43_FK;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, PROTOCOL_FK INTEGER, F43_FK INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class UNITOFMEASURE
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $FARMID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var double  
    */
    var $COEFFICIENT;

    /**
    * @var int     
    */
    var $ISBASE;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, FARMID INTEGER, NAME TEXT   , COEFFICIENT NUMERIC, ISBASE INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class FARMGROUP
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $CODE;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, CODE TEXT   , NAME TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class HEALTHPROCFARM
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $HEALTHPROC;

    /**
    * @var int     
    */
    var $FARM;

    /**
    * @var double  
    */
    var $QUANTITY;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, HEALTHPROC INTEGER, FARM INTEGER, QUANTITY NUMERIC, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class HEALTHPROC
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $PARENTID;

    /**
    * @var int     
    */
    var $NODETYPE;

    /**
    * @var string  
    */
    var $SHIFR;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $PLANNAME;

    /**
    * @var double  
    */
    var $UOP;

    /**
    * @var int     
    */
    var $PROCTYPE;

    /**
    * @var int     
    */
    var $TOOTHUSE;

    /**
    * @var double  
    */
    var $PRICE;

    /**
    * @var int     
    */
    var $HEALTHTYPE;

    /**
    * @var int     
    */
    var $PROCTIME;

    /**
    * @var int     
    */
    var $SEQUINCENUMBER;

    /**
    * @var double  
    */
    var $EXPENSES;

    /**
    * @var int     
    */
    var $FARMSCOUNT;

    /**
    * @var int     
    */
    var $DISCOUNT_FLAG;

    /**
    * @var int     
    */
    var $SALARY_SETTINGS;

    /**
    * @var int     
    */
    var $COMPLETEEXAMDIAG;

    /**
    * @var string  
    */
    var $HEALTHTYPES;

    /**
    * @var int     
    */
    var $ANESTHESIATYPE;
    
    
   	/**
	* @var string
	*/
	var $NAME48;
    
   	/**
	* @var int
	*/
	var $REGCODE;
    
	/**
	* @var double
	*/
	var $PRICE2;
	/**
	* @var double
	*/
	var $PRICE3;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, PARENTID INTEGER, NODETYPE INTEGER, SHIFR TEXT   , NAME TEXT   , PLANNAME TEXT   , UOP NUMERIC, PROCTYPE INTEGER, TOOTHUSE INTEGER, PRICE NUMERIC, HEALTHTYPE INTEGER, PROCTIME INTEGER, SEQUINCENUMBER INTEGER, EXPENSES NUMERIC, FARMSCOUNT INTEGER, DISCOUNT_FLAG INTEGER, SALARY_SETTINGS INTEGER, COMPLETEEXAMDIAG INTEGER, HEALTHTYPES TEXT   , ANESTHESIATYPE INTEGER, NAME48 TEXT, REGCODE INTEGER, PRICE2 NUMERIC, PRICE3 NUMERIC, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class FARM
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $PARENTID;

    /**
    * @var int     
    */
    var $NODETYPE;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $NOMENCLATUREARTICLENUMBER;

    /**
    * @var string  
    */
    var $REMARK;

    /**
    * @var int     
    */
    var $FARMGROUPID;

    /**
    * @var int     
    */
    var $MINIMUMQUANTITY;

    /**
    * @var string  
    */
    var $BARCODE;

    /**
    * @var int     
    */
    var $REQUIRED;

    /**
    * @var int     
    */
    var $OPTIMALQUANTITY;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, PARENTID INTEGER, NODETYPE INTEGER, NAME TEXT   , NOMENCLATUREARTICLENUMBER TEXT   , REMARK TEXT   , FARMGROUPID INTEGER, MINIMUMQUANTITY INTEGER, BARCODE TEXT   , REQUIRED INTEGER, OPTIMALQUANTITY INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class TEMPLATETEST
{
    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $QUESTION;

    /**
    * @var string  
    */
    var $CONFIGXML;

    /**
    * @var string  
    */
    var $SEQNUMBER;

    /**
    * @var int     
    */
    var $ISENABLED;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, QUESTION TEXT   , CONFIGXML TEXT   , SEQNUMBER TEXT   , ISENABLED INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class ACCOUNTFLOW_GROUPS
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var int     
    */
    var $GROUP_TYPE;

    /**
    * @var int     
    */
    var $RULES;

    /**
    * @var int     
    */
    var $IS_CASHPAYMENT;

    /**
    * @var int     
    */
    var $MONTHLY_IS;

    /**
    * @var double  
    */
    var $MONTHLY_DEFAULTSUMM;

    /**
    * @var int     
    */
    var $COLOR;

    /**
    * @var int     
    */
    var $GRIDSEQUENCE;

    /**
    * @var int     
    */
    var $RESIDUE;

	 /**
    * @var int     
    */
    var $ISEMPLOYEE;
	
    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , GROUP_TYPE INTEGER, RULES INTEGER, IS_CASHPAYMENT INTEGER, MONTHLY_IS INTEGER, MONTHLY_DEFAULTSUMM NUMERIC, COLOR INTEGER, GRIDSEQUENCE INTEGER, RESIDUE INTEGER, ISEMPLOYEE INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class ACCOUNTFLOW_TEXP_GROUPS
{

/**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var int     
    */
    var $IS_CASHPAYMENT;

    /**
    * @var int     
    */
    var $COLOR;

    /**
    * @var int     
    */
    var $GRIDSEQUENCE;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , IS_CASHPAYMENT INTEGER, COLOR INTEGER, GRIDSEQUENCE INTEGER, VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class ACCOUNT
{

/**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $ACCOUNTUSER;

    /**
    * @var string  
    */
    var $ACCOUNTPASSWORD;

    /**
    * @var string  
    */
    var $ACCOUNTKEY;

    /**
    * @var string  
    */
    var $FIRSTNAME;

    /**
    * @var string  
    */
    var $MIDDLENAME;

    /**
    * @var string  
    */
    var $LASTNAME;

    /**
    * @var int     
    */
    var $ACCESS;
    /**
    * @var int     
    */
    var $SPEC;
     /**
    * @var int     
    */
    var $SUBDIVISIONID;
    
    
    /**
     * @var string
     */
    var $ACCOUNTTYPE;
    /**
     * @var string
     */
    var $TAX_ID;
    /**
     * @var string
     */
    var $BIRTH_DATE;
    /**
     * @var string
     */
    var $BIRTH_PLACE;
    /**
     * @var int
     */
    var $GENDER;
    /**
     * @var string
     */
    var $EMAIL;
    /**
     * @var string
     */
    var $USER_POSITION;
    
    /**
     * @var string
     */
    var $PHONE_TYPE;
    /**
     * @var string
     */
    var $PHONE;
    /**
     * @var string
     */
    var $DOC_TYPE;
    /**
     * @var string
     */
    var $DOC_NUMBER;
    /**
    * @var int     
    */
    var $IS_OWNER;
    
    
    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, ACCOUNTUSER TEXT   , ACCOUNTPASSWORD TEXT   , ACCOUNTKEY TEXT   , FIRSTNAME TEXT   , MIDDLENAME TEXT   , LASTNAME TEXT   , ACCESS INTEGER, SPEC INTEGER, SUBDIVISIONID INTEGER, ACCOUNTTYPE TEXT, TAX_ID TEXT, BIRTH_DATE TEXT, BIRTH_PLACE TEXT, GENDER INTEGER, EMAIL TEXT, USER_POSITION TEXT , PHONE_TYPE TEXT , PHONE TEXT, DOC_TYPE TEXT , DOC_NUMBER TEXT , IS_OWNER INTEGER ,  VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class OCCLUSIONS
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $DESCRIPTION;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NUMBER INTEGER, DESCRIPTION TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class COMORBIDITIES
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $DESCRIPTION;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NUMBER INTEGER, DESCRIPTION TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class HYGIENE
{

   /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $NUMBER;

    /**
    * @var string  
    */
    var $HYGDESCRIPT;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NUMBER INTEGER, HYGDESCRIPT TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class DOCTORS_SALARY_SCHEMA          
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $DOCTORID;

    /**
    * @var string  
    */
    var $CREATEDATE;

    /**
    * @var string  
    */
    var $ENABLEDFROMDATE;

    /**
    * @var string  
    */
    var $SALARYSCHEMA;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, DOCTORID INTEGER, CREATEDATE TEXT   , ENABLEDFROMDATE TEXT   , SALARYSCHEMA TEXT   , VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class ASSISTANTS_SALARY_SCHEMA       
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $ASSISTANTID;

    /**
    * @var string  
    */
    var $CREATEDATE;

    /**
    * @var string  
    */
    var $ENABLEDFROMDATE;

    /**
    * @var string  
    */
    var $SALARYSCHEMA;

    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, ASSISTANTID INTEGER, CREATEDATE TEXT   , ENABLEDFROMDATE TEXT   , SALARYSCHEMA TEXT   , VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class CLINICREGISTRATIONDATA         
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $LEGALNAME;

    /**
    * @var string  
    */
    var $ADDRESS;

    /**
    * @var int     
    */
    var $ORGANIZATIONTYPE;

    /**
    * @var int     
    */
    var $CODE;

    /**
    * @var string  
    */
    var $DIRECTOR;

    /**
    * @var string  
    */
    var $TELEPHONENUMBER;

    /**
    * @var string  
    */
    var $FAX;

    /**
    * @var string  
    */
    var $SITENAME;

    /**
    * @var string  
    */
    var $EMAIL;

    /**
    * @var string  
    */
    var $ICQ;

    /**
    * @var string  
    */
    var $SKYPE;

    /**
    * @var string  
    */
    var $UOPTIME;

    /**
    * @var double  
    */
    var $UOPPRICE;

    /**
    * @var imageblob  
    */
    var $LOGO;

    /**
    * @var string  
    */
    var $CHIEFACCOUNTANT;

    /**
    * @var string  
    */
    var $LEGALADDRESS;

    /**
    * @var string  
    */
    var $BANK_NAME;

    /**
    * @var string  
    */
    var $BANK_MFO;

    /**
    * @var string  
    */
    var $BANK_CURRENTACCOUNT;

    /**
    * @var string  
    */
    var $LICENSE_NUMBER;

    /**
    * @var string  
    */
    var $LICENSE_SERIES;

    /**
    * @var string  
    */
    var $LICENSE_STARTDATE;

    /**
    * @var string  
    */
    var $LICENSE_ENDDATE;

    /**
    * @var string  
    */
    var $LICENSE_ISSUED;

    /**
    * @var string  
    */
    var $AUTHORITY;
    
    /**
    * @var string  
    */
    var $CITY;


    /**
    * @var string
    */
    var $SHORT_NAME;
    /**
    * @var string
    */
    var $MSP_TYPE;
    /**
    * @var string
    */
    var $OWNER_PROPERTY_TYPE;
    /**
    * @var string
    */
    var $LEGAL_FORM;
    /**
    * @var string
    */
    var $KVEDS;

    /**
    * @var string
    */
    var $ADDRESSLEGAL_ZIP;

    /**
    * @var string
    */
    var $ADDRESSLEGAL_TYPE;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_COUNTRY;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_AREA;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_REGION;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_SETTLEMENT;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_SETTLEMENT_TYPE;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_SETTLEMENT_ID;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_STREET_TYPE;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_STREET;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_BUILDING;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_APRT;
    
    /**
    * @var string
    */
    var $PHONE_TYPE;
    /**
    * @var string
    */
    var $PHONE;
    
    /**
    * @var string
    */
    var $ACR_CATEGORY;
    /**
    * @var string
    */
    var $ACR_ISSUED_DATE;
    /**
    * @var string
    */
    var $ACR_EXPIRY_DATE;
    /**
    * @var string
    */
    var $ACR_ORDER_NO;
    /**
    * @var string
    */
    var $ACR_ORDER_DATE;
    /**
     * @var int
     */
    var $CONSENT_SIGN;
    
    /**
    * @var string
    */
    var $EH_ID;
    /**
    * @var string
    */
    var $EH_STATUS;
    /**
    * @var string
    */
    var $EH_CREATED_BY_MIS_CLIENT_ID;
    
    /**
    * @var int     
    */
    var $VERSION;
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , LEGALNAME TEXT   , ADDRESS TEXT   , ORGANIZATIONTYPE INTEGER, CODE INTEGER, DIRECTOR TEXT   , TELEPHONENUMBER TEXT   , FAX TEXT   , SITENAME TEXT   , EMAIL TEXT   , ICQ TEXT   , SKYPE TEXT   , UOPTIME TEXT   , UOPPRICE NUMERIC, LOGO BLOB   , CHIEFACCOUNTANT TEXT   , LEGALADDRESS TEXT   , BANK_NAME TEXT   , BANK_MFO TEXT   , BANK_CURRENTACCOUNT TEXT   , LICENSE_NUMBER TEXT   , LICENSE_SERIES TEXT   , LICENSE_STARTDATE TEXT   , LICENSE_ENDDATE TEXT   , LICENSE_ISSUED TEXT   , AUTHORITY TEXT  , CITY TEXT  , SHORT_NAME TEXT , MSP_TYPE TEXT , OWNER_PROPERTY_TYPE TEXT , LEGAL_FORM TEXT , KVEDS TEXT , ADDRESSLEGAL_ZIP TEXT , ADDRESSLEGAL_TYPE TEXT , ADDRESSLEGAL_COUNTRY TEXT , ADDRESSLEGAL_AREA TEXT , ADDRESSLEGAL_REGION TEXT , ADDRESSLEGAL_SETTLEMENT TEXT ,  ADDRESSLEGAL_SETTLEMENT_TYPE TEXT , ADDRESSLEGAL_SETTLEMENT_ID TEXT ,  ADDRESSLEGAL_STREET_TYPE TEXT , ADDRESSLEGAL_STREET TEXT , ADDRESSLEGAL_BUILDING TEXT , ADDRESSLEGAL_APRT TEXT , PHONE_TYPE TEXT , PHONE TEXT ,  ACR_CATEGORY TEXT , ACR_ISSUED_DATE TEXT , ACR_EXPIRY_DATE TEXT , ACR_ORDER_NO TEXT , ACR_ORDER_DATE TEXT , CONSENT_SIGN INTEGER , EH_ID TEXT , EH_STATUS TEXT , EH_CREATED_BY_MIS_CLIENT_ID TEXT , VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                if($field=='LOGO')
                {
                    $imageBlob = null;
                    $blob_info = ibase_blob_info($row[$key]);
                    if ($blob_info[0]!=0)
                    {
                        $blob_hndl = ibase_blob_open($row[$key]);
                        $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                        ibase_blob_close($blob_hndl);
                    }
                    $this->$field = base64_encode($imageBlob);
                }
                else
                {
                     $this->$field=$row[$key];
                }

           }

       }
    }
}
class SUPPLIER                       
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $LEGALNAME;

    /**
    * @var string  
    */
    var $ADDRESS;

    /**
    * @var int     
    */
    var $ORGANIZATIONTYPE;

    /**
    * @var string  
    */
    var $CODE;

    /**
    * @var string  
    */
    var $DIRECTOR;

    /**
    * @var string  
    */
    var $TELEPHONENUMBER;

    /**
    * @var string  
    */
    var $FAX;

    /**
    * @var string  
    */
    var $SITENAME;

    /**
    * @var string  
    */
    var $EMAIL;

    /**
    * @var string  
    */
    var $ICQ;

    /**
    * @var string  
    */
    var $SKYPE;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , LEGALNAME TEXT   , ADDRESS TEXT   , ORGANIZATIONTYPE INTEGER, CODE TEXT   , DIRECTOR TEXT   , TELEPHONENUMBER TEXT   , FAX TEXT   , SITENAME TEXT   , EMAIL TEXT   , ICQ TEXT   , SKYPE TEXT   , VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class FARMSUPPLIER                   
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $FARMID;

    /**
    * @var int     
    */
    var $SUPPLIERID;

    /**
    * @var double  
    */
    var $PRICE;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, FARMID INTEGER, SUPPLIERID INTEGER, PRICE NUMERIC, VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class INSURANCECOMPANYS              
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $ADDRESS;

    /**
    * @var string  
    */
    var $PHONE;

    /**
    * @var string  
    */
    var $IDENTYNUM;

    /**
    * @var string  
    */
    var $NAMELEGAL;

    /**
    * @var string  
    */
    var $ADDRESSLEGAL;

    /**
    * @var string  
    */
    var $ACCOUNTINGNUMBER;

    /**
    * @var string  
    */
    var $ACCOUNTINGMFO;

    /**
    * @var string  
    */
    var $ACCOUNTINGBANK;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , ADDRESS TEXT   , PHONE TEXT   , IDENTYNUM TEXT   , NAMELEGAL TEXT   , ADDRESSLEGAL TEXT   , ACCOUNTINGNUMBER TEXT   , ACCOUNTINGMFO TEXT   , ACCOUNTINGBANK TEXT   , VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class OPERATINGSCHEDULE              
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $DOCTORID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $STARTDATE;

    /**
    * @var string  
    */
    var $ENDDATE;

    /**
    * @var int     
    */
    var $ISCURRENT;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, DOCTORID INTEGER, NAME TEXT   , STARTDATE TEXT   , ENDDATE TEXT   , ISCURRENT INTEGER, VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class OPERATINGSCHEDULERULES         
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $OPERATINGSCHEDULEID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var int     
    */
    var $ISWORKING;

    /**
    * @var textblob
    */
    var $DAYSRANGE;

    /**
    * @var int     
    */
    var $REPEATTYPE;

    /**
    * @var int     
    */
    var $SKIPDAYSCOUNT;

    /**
    * @var string  
    */
    var $STARTTIME;

    /**
    * @var string  
    */
    var $ENDTIME;

    /**
    * @var int     
    */
    var $CABINETID;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, OPERATINGSCHEDULEID INTEGER, NAME TEXT   , ISWORKING INTEGER, DAYSRANGE BLOB   , REPEATTYPE INTEGER, SKIPDAYSCOUNT INTEGER, STARTTIME TEXT   , ENDTIME TEXT   , CABINETID INTEGER, VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                if($field=='DAYSRANGE')
                {
                     $this->$field=text_blob_encode($row[$key]);
                }
                else
                {
                     $this->$field=$row[$key];
                }
           }

       }
    }
}

class CONTACTS                       
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $COMMENT;

    /**
    * @var string  
    */
    var $PHONES;

    /**
    * @var string  
    */
    var $ADDRESS;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT   , COMMENT TEXT   , PHONES TEXT   , ADDRESS TEXT   , VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class DISCOUNT_SCHEMA                
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var double  
    */
    var $SUMM;

    /**
    * @var double  
    */
    var $PERCENT;

    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, SUMM NUMERIC, PERCENT NUMERIC, VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class SUBDIVISION                    
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string  
    */
    var $NAME;
    /**
    * @var string  
    */
    var $CITY;
    /**
    * @var string  
    */
    var $ADDRESS;
    
    /**
    * @var int
    */
    var $ORDERNUMBER_CASH;
    /**
    * @var int
    */
    var $ORDERNUMBER_CLEARING;
    /**
    * @var int
    */
    var $INVOICENUMBER;
    
    
    /**
    * @var int     
    */
    var $VERSION;

    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, NAME TEXT, CITY TEXT, ADDRESS TEXT, ORDERNUMBER_CASH INTEGER, ORDERNUMBER_CLEARING INTEGER, INVOICENUMBER INTEGER, VERSION INTEGER';
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}
class MKB10
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var int     
    */
    var $PARENTID;

    /**
    * @var string  
    */
    var $SHIFR;

    /**
    * @var string  
    */
    var $NAME;

    /**
    * @var string  
    */
    var $RUSNAME;

    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'id INTEGER PRIMARY KEY, PARENTID INTEGER, SHIFR TEXT   , NAME TEXT   , RUSNAME TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}


class FARMSTORAGE
{

    /**
    * @var int     
    */
    var $ID;

    /**
    * @var string     
    */
    var $STORAGELABEL;

    /**
    * @var int  
    */
    var $STORAGECOLOR;

    /**
    * @var string  
    */
    var $DOCTOR_FK;

    /**
    * @var string  
    */
    var $SUBDIVISION_FK;
    
    /**
    * @var int     
    */
    var $VERSION;
    
    
    public static function sqliteCreateVarString()
    {
      return 'ID INTEGER PRIMARY KEY, STORAGELABEL TEXT, STORAGECOLOR INTEGER   , DOCTOR_FK TEXT   , SUBDIVISION_FK TEXT   , VERSION INTEGER';  
    }
    


    function __construct($row=null) 
    {
       if($row!=null)
       {
            $fields=array_keys(get_class_vars(get_class()));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
           }

       }
    }
}



?>
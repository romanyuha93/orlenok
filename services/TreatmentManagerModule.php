<?php
require_once "Connection.php";
require_once "Utils.php";
require_once "Form039Module.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
//define('tfClientId','debug');

class TreatmentManagerModuleProtocol
{
    /**
    * @var string
    */
    var $protocolID;
    /**
    * @var string
    */
    var $protocolName;
    /**
    * @var int
    */
    var $protocolF43;
    /**
     * @var TreatmentManagerModuleF43[]
     */
     var $f43;
    /**
    * @var int
    */
    var $protocolType;
    /**
    * @var string
    */
    var $protocolTXT;
    /**
    * @var string
    */
    var $protocolAnamnesTXT;
    /**
    * @var string
    */
    var $protocolStatusTXT;
    /**
    * @var string
    */
    var $protocolRecomendTXT;
    /**
    * @var string
    */
    var $protocolCreateDate;
    /**
    * @var TreatmentManagerModuleDoctor
    */
    var $doctor;
    /**
     * @var int
     */
     var $templates_count;
     
    /**
    * @var string
    */
    var $protocolEpicrisisTXT;
    /**
    * @var string
    */
    var $protocolRentgenTXT;
    /**
    * @var boolean
    */
    var $isQuick;
}

class TreatmentManagerModuleF43
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $f43_id;
    /**
    * @var string
    */
    var $protocol_id;
    /**
    * @var int
    */
    var $f43;
}

class TreatmentManagerModuleTemplate
{
    /**
    * @var string
    */
    var $templateID;
    /**
    * @var string
    */
    var $templateName;
    /**
    * @var string
    */
    var $protocolID;
    /**
    * @var string
    */
    var $protocolType;
    /**
    * @var string
    */
    var $templateCreateDate;
    /**
    * @var int
    */
    var $templateF43;
    /**
    * @var number
    */
    var $templateSum;
    /**
    * @var TreatmentManagerModuleDoctor
    */
    var $doctor;
    /**
    * @var TreatmentManagerModuleTemplateProcedure[]
    */
    var $procedures;
}
class TreatmentManagerModuleTemplateProcedure
{
    /**
    * @var string
    */
    var $procedurID;
    /**
    * @var string
    */
    var $procedurName;
     /**
    * @var string
    */
    var $procedurPlanName;
    /**
    * @var string
    */
    var $procedurShifr;
    /**
    * @var int
    */
    var $procedurVisit;
    /**
    * @var int
    */
    var $procedurStacknumber;
    /**
    * @var string
    */
    var $healthprocID;
    /**
    * @var number
    */
    var $procedurPrice;
    /**
     * @var int
     */
     var $healthType;
    /**
    * @var TreatmentManagerModuleTemplateProcedurFarm[]
    */
    var $farms;
    /**
     * @var string
     */
     var $healthTypes;
     
     /**
	* @var Form039Module_F39_Val[]
	*/
	var $F39;
}
class TreatmentManagerModuleTemplateProcedurFarm
{
    /**
    * @var string
    */
    var $ID;
    /**
    * @var string
    */
    var $farmID;
    /**
    * @var string
    */
    var $farmGroup;
    /**
    * @var string
    */
    var $farmName;
    /**
    * @var integer
    */
    var $farmQuantity;
    /**
    * @var number
    */
    var $farmPrice;		
    /**
    * @var string
    */
    var $farmUnitofmeasure;
    /**
    * @var string
    */
    var $farmHealthproc;
}



class TreatmentManagerModuleDoctor
{
    /**
    * @var string
    */
    var $doctorID;
    /**
    * @var string
    */
    var $doctorFName;
    /**
    * @var string
    */
    var $doctorLName;
    /**
    * @var string
    */
    var $doctorMName;
    /**
    * @var string
    */
    var $doctorLabelField;
    
    /**
    * @var int
    */
    var $doctorDismissedFlag;
    
}


// ******** COPYPASTE CLASS START ************** NOT FOR FLEX **************
    class TreatmentManagerModuleCopyPasteProcedures
    {
        /**
        * @var int
        */
        var $templateid;
        /**
        * @var int
        */
        var $visit;
        /**
        * @var int
        */
        var $stacknumber;
        /**
        * @var int
        */
        var $healthprocid;
    }
// ******** COPYPASTE CLASS END ************** NOT FOR FLEX **************


class TreatmentManagerModule
{
	/**
	* @param TreatmentManagerModuleProtocol $p1
    * @param TreatmentManagerModuleTemplate $p2
    * @param TreatmentManagerModuleTemplateProcedure $p3
    * @param TreatmentManagerModuleTemplateProcedurFarm $p4
    * @param TreatmentManagerModuleDoctor $p5
    * @param TreatmentManagerModuleF43 $p6
    * @return void
	*/
	public function registertypes($p1, $p2, $p3, $p4, $p5, $p6) {}
    
    /**
     * @param string $protocolId
     * @return TreatmentManagerModuleF43[]
     */
    public function getF43($protocolId)//sqlite
    {
   	    $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if(($protocolId != null)&&($protocolId != 'null')&&($protocolId != '-')&&($protocolId != ''))
        {
            $QueryText = "select f43.f43, f43.id, protocols_f43.protocol_fk, protocols_f43.id
            from f43
            left join protocols_f43
            on f43.id = protocols_f43.f43_fk and protocols_f43.protocol_fk = $protocolId
             where f43.f43 > 0 order by protocols_f43.id desc";
        }
        else
        {
		  $QueryText = "select f43.f43, f43.id, null from f43 
                        where f43.f43 > 0  
                        order by f43.id desc";	 
        }	
        
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new TreatmentManagerModuleF43;
			$obj->f43 = $row[0];
			$obj->f43_id = $row[1];
            $obj->protocol_id = $row[2];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
         /**
     * @param TreatmentManagerModuleF43 $itemF43
     * @param string $protocolId
     * @return TreatmentManagerModuleF43[]
     */
    public function addF43toProtocol($itemF43, $protocolId)
    {	
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryF43Text = "update or insert into
                protocols_f43(id, f43_fk, protocol_fk)
            values
                (".(($itemF43->id == null || $itemF43->id == '') ? "null," : "$itemF43->id,").
                "$itemF43->f43_id,".
                "$itemF43->protocol_id) 
            matching (id)";
		$queryF43 = ibase_prepare($trans, $QueryF43Text);
		ibase_execute($queryF43);   
        
        if(($itemF43->protocol_id != null)&&($itemF43->protocol_id != 'null')&&($itemF43->protocol_id != '-')&&($itemF43->protocol_id != ''))
        {
            $QueryText = "select f43.f43, f43.id, protocols_f43.protocol_fk, protocols_f43.id
            from f43
            left join protocols_f43
            on f43.id = protocols_f43.f43_fk and protocols_f43.protocol_fk = $protocolId";
        }
        else
        {
		  $QueryText = "select f43.f43, f43.id, null from f43";	 
        }	
        $QueryText .= " where f43.f43 > 0 order by protocols_f43.id desc";			
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new TreatmentManagerModuleF43;
			$obj->f43 = $row[0];
			$obj->f43_id = $row[1];
            $obj->protocol_id = $row[2];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
    
    /**
     * @param TreatmentManagerModuleF43 $itemF43
     * @param string $protocolId
     * @return TreatmentManagerModuleF43[]
     */
    public function removeF43fromProtocol($itemF43, $protocolId)
    {	
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from protocols_f43
                where protocols_f43.f43_fk = $itemF43->f43_id and protocols_f43.protocol_fk = $itemF43->protocol_id";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
        
		if(($itemF43->protocol_id != null)&&($itemF43->protocol_id != 'null')&&($itemF43->protocol_id != '-')&&($itemF43->protocol_id != ''))
        {
            $QueryText = "select f43.f43, f43.id, protocols_f43.protocol_fk, protocols_f43.id
            from f43
            left join protocols_f43
            on f43.id = protocols_f43.f43_fk and protocols_f43.protocol_fk = $protocolId";
        }
        else
        {
		  $QueryText = "select f43.f43, f43.id, null from f43";	 
        }	
        $QueryText .= " where f43.f43 > 0 order by protocols_f43.id desc";			
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new TreatmentManagerModuleF43;
			$obj->f43 = $row[0];
			$obj->f43_id = $row[1];
            $obj->protocol_id = $row[2];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;    
    }
    
    /**
     * @param string $mode  //active, nonactive, all
     * @return TreatmentManagerModuleProtocol[]
     */
    public function getProtocols($mode)//sqlite
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "select
                    protocols.id,
                    protocols.name,
                    protocols.f43,
                    protocols.healthtype,
                    protocols.protocoltxt,
                    protocols.anamnestxt,
                    protocols.statustxt,
                    protocols.recomendtxt,
                    protocols.createdate,
                    (select count(*) from template where template.protocol = protocols.id),
                    doctors.id,
                    doctors.firstname,
                    doctors.lastname,
                    doctors.middlename,
                    (select count(*) from protocols_f43 where protocols.id = protocols_f43.protocol_fk),
                    protocols.epicrisistxt,
                    protocols.rentgentxt,
                    protocols.isquick
                from protocols
                   left join doctors on (doctors.id = protocols.doctor) ";
            if($mode == 'active')
            {
                //$QueryText .= "where protocols.f43 != -1";
                
                $QueryText .= "where (select count(*) from protocols_f43 where protocols.id = protocols_f43.protocol_fk) > 0 ";
            }
            else if($mode == 'nonactive')
            {
                //$QueryText .= "where protocols.f43 = -1";
                $QueryText .= "where (select count(*) from protocols_f43 where protocols.id = protocols_f43.protocol_fk) <= 0 ";
            }
            $QueryText .= "order by protocols.f43 desc, protocols.id";
            
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $protocols = array();
			while($row = ibase_fetch_row($result))
			{
                $protocol = new TreatmentManagerModuleProtocol;
				$protocol->protocolID = $row[0];
                $protocol->protocolName = $row[1];
			    $protocol->protocolF43 = $row[2];
			    $protocol->protocolType = $row[3];
                /*$txtBlob = null;
                $blob_info = ibase_blob_info($row[4]);
                if ($blob_info[0]!=0)
                {
                    $blob_hndl = ibase_blob_open($row[4]);
                    $txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                    ibase_blob_close($blob_hndl);
                }
                $protocol->protocolTXT = $txtBlob;*/
                $protocol->protocolTXT = text_blob_encode($row[4]);
                $protocol->protocolAnamnesTXT = text_blob_encode($row[5]);
                $protocol->protocolStatusTXT = text_blob_encode($row[6]);
                $protocol->protocolRecomendTXT = text_blob_encode($row[7]);
                $protocol->protocolCreateDate = $row[8];
                $protocol->templates_count = $row[9];
                
                if ($row[10] != null)
                {
                    $protocol->doctor = new TreatmentManagerModuleDoctor;
                    $protocol->doctor->doctorID = $row[10];
                    $protocol->doctor->doctorFName = $row[11];
                    $protocol->doctor->doctorLName = $row[12];
                    $protocol->doctor->doctorMName = $row[13];
                    $protocol->doctor->doctorLabelField = $row[12].' '.$row[11];//lastname firstname
                }
                $protocol->protocolEpicrisisTXT = text_blob_encode($row[15]);
                $protocol->protocolRentgenTXT = text_blob_encode($row[16]);
                $protocol->isQuick = $row[17];
                $protocol->f43 = array();
                if($row[14] > 0)
                {
            		$QueryF43Text = "select f43.f43, f43.id, protocols_f43.id
                                        from f43
                                        inner join protocols_f43 on f43.id = protocols_f43.f43_fk
                                        where protocols_f43.protocol_fk = $row[0]";				
            		$queryF43 = ibase_prepare($trans, $QueryF43Text);
            		$resultF43 = ibase_execute($queryF43);	
            		while ($rowF43 = ibase_fetch_row($resultF43))
            		{
            		  $f43_obj = new TreatmentManagerModuleF43;
                      $f43_obj->f43 = $rowF43[0];
                      $f43_obj->f43_id = $rowF43[1];
                      $f43_obj->protocol_id = $row[0];
                      $f43_obj->id = $rowF43[2];
            		  $protocol->f43[] = $f43_obj;
  		            } 
                }
                
				$protocols[] = $protocol;
			}
			ibase_commit($trans);
			ibase_free_query($query);
			ibase_free_result($result);
			$connection->CloseDBConnection();
            return $protocols;
    }
    /**
     * @param TreatmentManagerModuleProtocol $protocol
     * @return string
     */
    public function saveProtocol($protocol)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "update or insert into
                protocols(
                        id,
                        name,
                        f43,
                        healthtype,
                        protocoltxt,
                        anamnestxt,
                        statustxt,
                        recomendtxt,
                        createdate,
                        doctor,
                        protocols.epicrisistxt,
                        protocols.rentgentxt,
                        protocols.isquick
                    )
                    values
                    (".
                        (($protocol->protocolID == null || $protocol->protocolID == '') ? "null," : "$protocol->protocolID,").
                        "'".safequery($protocol->protocolName)."',".
                        "$protocol->protocolF43,".
                        "$protocol->protocolType,".
                        //"null,".
                        (($protocol->protocolTXT == null || $protocol->protocolTXT == '') ? "null," : ("'".safequery($protocol->protocolTXT)."',")).
                        (($protocol->protocolAnamnesTXT == null || $protocol->protocolAnamnesTXT == '') ? "null," : ("'".safequery($protocol->protocolAnamnesTXT)."',")).
                        (($protocol->protocolStatusTXT == null || $protocol->protocolStatusTXT == '') ? "null," : ("'".safequery($protocol->protocolStatusTXT)."',")).
                        (($protocol->protocolRecomendTXT == null || $protocol->protocolRecomendTXT == '') ? "null," : ("'".safequery($protocol->protocolRecomendTXT)."',")).
                        "'".$protocol->protocolCreateDate."',".
                        $protocol->doctor->doctorID.", '".safequery($protocol->protocolEpicrisisTXT)."', '".safequery($protocol->protocolRentgenTXT)."', ".($protocol->isQuick?'1':'0').") 
                    matching (id)
                    returning (id)";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            $protocolID = ibase_fetch_row($result);
            
            
            
			ibase_commit($trans);
			ibase_free_result($result);
			ibase_free_query($query);
			$connection->CloseDBConnection();
            return $protocolID[0];
    }
    

    
    /**
     * @param string $protocolID
     * @return boolean
     */
    public function deleteProtocol($protocolID)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "delete from protocols
                where id = $protocolID";
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			ibase_free_query($query);
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
            return $success;
    }
    
    /**
     * @param int $diagnos
     * @param string $protocoID
     * @return integer
     */
    public function saveProtocolF43($diagnos, $protocoID)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "update protocols
                set f43 = $diagnos
                where id = $protocoID";
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			ibase_free_query($query);
			ibase_commit($trans);
			$connection->CloseDBConnection();
            return $diagnos;
    }
    
    /**
     * @param int $healthtype
     * @param string $protocoID
     * @return integer
     */
    public function saveProtocolType($healthtype, $protocoID)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "update protocols
                set healthtype = $healthtype
                where id = $protocoID";
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			ibase_free_query($query);
			ibase_commit($trans);
			$connection->CloseDBConnection();
            return $healthtype;
    }
    
    /**
     * @param string $sqlFieldLabel
     * @param string $text
     * @param string $protocoID
     * @return boolean
     */
    public function saveProtocolTextFields($sqlFieldLabel, $text, $protocoID)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "update protocols
                set ".$sqlFieldLabel." = '".safequery($text)."'
                where id = $protocoID";
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			$success = ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
            return $success;
    }
    
    //********************** TEMPLATES FUNCTIONS START ******************************************
    /**
     * @param string $protocolID
     * @param string $doctorID
     * @param int $priceIndex
     * @return TreatmentManagerModuleTemplate[]
     */
    public function getProtocolTemplates($protocolID, $doctorID, $priceIndex)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			/*$QueryText = "select
                    template.id,
                    template.name,
                    template.protocol,
                    template.f43after,
                    template.createdate,
                    (select  sum(healthproc.price)
                        from templateproc
                        inner join healthproc
                        on templateproc.healthproc = healthproc.id
                        where templateproc.template = template.id),
                    doctors.id,
                    doctors.firstname,
                    doctors.lastname,
                    doctors.middlename
                from template
                   left join doctors on (doctors.id = template.doctor)
                where template.protocol = $protocolID";
                */
            $QueryText = "select
                    template.id,
                    template.name,
                    template.protocol,
                    template.f43after,
                    template.createdate,
                    (select  sum(healthproc.price".sql_checkPriceIndex($priceIndex).")
                        from templateproc
                        inner join healthproc
                        on templateproc.healthproc = healthproc.id
                        where templateproc.template = template.id),
                    doctors.id,
                    doctors.firstname,
                    doctors.lastname,
                    doctors.middlename,
                    protocols.healthtype

                from template
                   left join doctors on (doctors.id = template.doctor)
                   left join protocols on (protocols.id = template.protocol)
                where template.protocol = $protocolID";
            if(($doctorID != '')&&($doctorID != null))
            {
                $QueryText .= " and template.doctor = $doctorID";
            }
            $QueryText .= " order by template.id desc";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $templates = array();
			while($row = ibase_fetch_row($result))
			{
                $template = new TreatmentManagerModuleTemplate;
				$template->templateID = $row[0];
                $template->templateName = $row[1];
                $template->protocolID =  $row[2];
			    $template->templateF43 = $row[3];
			    $template->templateCreateDate = $row[4];
			    $template->templateSum = $row[5];
                if ($row[6] != null)
                {
                    $template->doctor = new TreatmentManagerModuleDoctor;
                    $template->doctor->doctorID = $row[6];
                    $template->doctor->doctorFName = $row[7];
                    $template->doctor->doctorLName = $row[8];
                    $template->doctor->doctorMName = $row[9];
                    $template->doctor->doctorLabelField = $row[8].' '.$row[7];//lastname firstname
                }
                $template->protocolType = $row[10];
				$templates[] = $template;
			}
			ibase_commit($trans);
			ibase_free_query($query);
			ibase_free_result($result);
			$connection->CloseDBConnection();
            return $templates;
    }
    /**
     * @param string $templatelID
     * @return boolean
     */
    public function deleteTemplate($templatelID)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "delete from template
                where id = $templatelID";
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			ibase_free_query($query);
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
            return $success;
    }
    
    /**
     * @param TreatmentManagerModuleTemplate $template
     * @return TreatmentManagerModuleTemplate
     */
    public function saveTemplate($template)
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "update or insert into
                template(
                    id,
                    protocol,
                    name,
                    createdate,
                    doctor,
                    f43after
                    )
                    values
                    (".
                    (($template->templateID == null || $template->templateID == '') ? "null," : "$template->templateID,").
                    "$template->protocolID,".
                    "'".safequery($template->templateName)."',".
                    "'".$template->templateCreateDate."',".
                    (($template->doctor == null || $template->doctor == '') ? "null," : $template->doctor->doctorID.",").
                    $template->templateF43.") 
                    matching (id)
                    returning (id)";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            $templateID = ibase_fetch_row($result);
            $template->templateID = $templateID[0];
			ibase_free_result($result);
			ibase_free_query($query);
            
            //insert&update and delete farms when ADD prosedur
            $QueryDelProcsText = "delete from TEMPLATEPROC where TEMPLATEPROC.TEMPLATE = $templateID[0]";
            if($template->procedures != null)//add del update procedures
            {
                //foreach($template->procedures as $itemProcedure) INCORRECT WORK WHEN n SAME ITEMS
                for ($i=0; $i<count($template->procedures); $i++)
                {
                    $procedureID = $template->procedures[$i]->procedurID;
                    $QueryText = "update or insert into TEMPLATEPROC(
                            ID, 
                            TEMPLATE, 
                            VISIT, 
                            STACKNUMBER, 
                            HEALTHPROC)
						values(".
                            (($procedureID == null || $procedureID == '') ? "null," : "$procedureID,").
                            $templateID[0].",".
                            $template->procedures[$i]->procedurVisit.",". 
                            $template->procedures[$i]->procedurStacknumber.",".
                            $template->procedures[$i]->healthprocID.")
                        matching (id)
                        returning (id)";
                    $query = ibase_prepare($trans, $QueryText);
                    $result = ibase_execute($query);
                    $procedureIDsql = ibase_fetch_row($result);
                    $procedureID = $procedureIDsql[0];
                    ibase_free_query($query);
                    $template->procedures[$i]->procedurID = $procedureID;
                    $QueryDelProcsText .= " and TEMPLATEPROC.ID <> $procedureID";
                }
            }
            $QueryDelProc = ibase_prepare($trans, $QueryDelProcsText);
            ibase_execute($QueryDelProc);
            ibase_free_query($QueryDelProc);
                
                
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
            return $template;
    }
    
    
    //*** PASTE FUNCTION START ***
    /**
     * @param TreatmentManagerModuleTemplate[] $pastedtemplates
     * @return TreatmentManagerModuleTemplate[]
     */
    public function savePastedTemplates($pastedtemplates)//РІ Tempolates РґРѕР»Р¶РµРЅРѕ Р±С‹С‚СЊ СѓСЃС‚Р°РЅРѕРІР»РµРЅРѕ СЃРІРѕР№СЃС‚РІРѕ protocolID С‚РѕРіРѕ РїСЂРѕС‚РѕРєРѕР»Р° РєСѓРґР° РІСЃС‚Р°РІР»СЏРµС‚СЃСЏ
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            for ($i=0; $i<count($pastedtemplates); $i++)
            {
                
                //save template start
                $QueryText = "insert into
                template(
                    id,
                    protocol,
                    name,
                    createdate,
                    doctor,
                    f43after
                    )
                    values
                    (null,".
                    $pastedtemplates[$i]->protocolID.",".
                    "'".safequery($pastedtemplates[$i]->templateName)."',";
                //check createdate and add SQL string
                if(($pastedtemplates[$i]->templateCreateDate!=null)&&($pastedtemplates[$i]->templateCreateDate!=''))
                {
                    $QueryText .= "'".$pastedtemplates[$i]->templateCreateDate."',";
                }
                else
                {
                    $QueryText .= "'".date("Y-m-d")."',";
                    $pastedtemplates[$i]->templateCreateDate = date("Y-m-d");
                }
                //check doctorid and add SQL string
                $QueryText .= (($pastedtemplates[$i]->doctor == null || $pastedtemplates[$i]->doctor == '') ? "null," : $pastedtemplates[$i]->doctor->doctorID.",").
                            $pastedtemplates[$i]->templateF43.")
                               returning (id)";
                $query = ibase_prepare($trans, $QueryText);
                $result = ibase_execute($query);
                $pastedtemplateIDsql = ibase_fetch_row($result);
                ibase_free_query($query);
                ibase_free_result($result);
                $pastedtemplateID = $pastedtemplateIDsql[0];
                //save template end
                    
                //get templateprocedures to paste start
                $QueryTextTemplateProcedures = "select
                            templateproc.visit,
                            templateproc.stacknumber,
                            templateproc.healthproc
                            
                            from templateproc
                            where templateproc.template = ".$pastedtemplates[$i]->templateID;
                $queryTemplateProcedures = ibase_prepare($trans, $QueryTextTemplateProcedures);
                $resultTemplateProcedures = ibase_execute($queryTemplateProcedures);
                $TemplateProceduresToPast = array();
                while($row = ibase_fetch_row($resultTemplateProcedures))
                {
                    $procedure = new TreatmentManagerModuleCopyPasteProcedures;
                    $procedure->templateid = $pastedtemplateID;
                    $procedure->visit = $row[0];
                    $procedure->stacknumber = $row[1];
                    $procedure->healthprocid = $row[2];
                    $TemplateProceduresToPast[] = $procedure;
                }
                ibase_free_query($queryTemplateProcedures);
                //get templateprocedures to paste end
                
                //save templateprocedures start
                for ($j=0; $j<count($TemplateProceduresToPast); $j++)
                {
                    $QueryTextTemplateProceduresToPast = "insert into TEMPLATEPROC(
                            ID, 
                            TEMPLATE, 
                            VISIT, 
                            STACKNUMBER, 
                            HEALTHPROC)
						values(null,".
                            $TemplateProceduresToPast[$j]->templateid.",". 
                            $TemplateProceduresToPast[$j]->visit.",". 
                            $TemplateProceduresToPast[$j]->stacknumber.",". 
                            $TemplateProceduresToPast[$j]->healthprocid.")";
                    $queryTemplateProceduresToPast = ibase_prepare($trans, $QueryTextTemplateProceduresToPast);
                    ibase_execute($queryTemplateProceduresToPast);
                    ibase_free_query($queryTemplateProceduresToPast);
                }
                //save templateprocedures end
                
                $pastedtemplates[$i]->templateID = $pastedtemplateID;//set template new saved id
                
            }
			ibase_commit($trans);
			$connection->CloseDBConnection();
            return $pastedtemplates;
    }
    //*** PASTE FUNCTION STOP ***
    //********************** TEMPLATES FUNCTIONS END ******************************************
    
    /**
     * @param string $templateID
     * @param int $priceIndex
     * @param resource $trans
     * @param resource $withFarms
     * @return TreatmentManagerModuleTemplateProcedure[]
     */
    public static function getTemplateProcedures($templateID, $priceIndex, $trans=null, $withFarms=true)
    {
        if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
			$QueryText = "select templateproc.id,
                            templateproc.visit,
                            templateproc.stacknumber,
                            templateproc.healthproc,
                            healthproc.name, 
                            healthproc.shifr,
                            healthproc.price".sql_checkPriceIndex($priceIndex).",
                            healthproc.farmscount,
                            healthproc.planname
                            from templateproc
                            inner join healthproc
                            on templateproc.healthproc = healthproc.id
                            where templateproc.template = $templateID
                            order by templateproc.stacknumber asc";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $procedures = array();
			while($row = ibase_fetch_row($result))
			{
                $procedure = new TreatmentManagerModuleTemplateProcedure;
				$procedure->procedurID = $row[0];
                $procedure->procedurVisit = $row[1];
                $procedure->procedurStacknumber = $row[2];
                $procedure->healthprocID = $row[3];
                $procedure->procedurName = $row[4];
                $procedure->procedurPlanName = $row[8];
                $procedure->procedurShifr = $row[5];
                $procedure->procedurPrice = $row[6];
                if($withFarms)
                {
                    $procedure->F39 = Form039Module::get039ProcValues(0,$row[3],$trans);
                }               
                $healthprocFarmsCount = $row[7];
                if($withFarms&&($healthprocFarmsCount != null)&&($healthprocFarmsCount > 0))
                {
                    
                    // 0 - material costs *** tmp deprecated ***
                    // '' - material farmgroup code *** deprecated ***
                    $farmQueryText = "select
                                                HEALTHPROCFARM.ID,
                                                FARM.ID,
                                                FARM.NAME, 
                                                '', 
                                                HEALTHPROCFARM.QUANTITY,  
                                                0,
                                                (select UNITOFMEASURE.NAME from UNITOFMEASURE where UNITOFMEASURE.FARMID = HEALTHPROCFARM.FARM and UNITOFMEASURE.isbase = 1),
                                                HEALTHPROCFARM.HEALTHPROC
                                                
                                                from HEALTHPROCFARM 
                                                
                                                left join FARM
                                                    on HEALTHPROCFARM.FARM = FARM.ID
    
                                                where HEALTHPROCFARM.HEALTHPROC='$row[3]'";
            		$farmquery = ibase_prepare($trans, $farmQueryText);
            		$farmresult = ibase_execute($farmquery);
                    while ($farmRow = ibase_fetch_row($farmresult))
                    {
                        $farmobj = new TreatmentManagerModuleTemplateProcedurFarm;
                        $farmobj->ID = $farmRow[0];
                        $farmobj->farmID = $farmRow[1];
                        $farmobj->farmName = $farmRow[2];
                        $farmobj->farmGroup = $farmRow[3];
                        $farmobj->farmQuantity = $farmRow[4];
                        $farmobj->farmPrice = $farmRow[5];
                        $farmobj->farmUnitofmeasure = $farmRow[6];
                        $farmobj->farmHealthproc = $farmRow[7];
                        $procedure->farms[] = $farmobj;
                    }                            
    			}	
				$procedures[] = $procedure;
			}
			
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            
            return $procedures;
    }
    
    
    //ADDITIONAL FUNCTIONS
    /**
     * @return TreatmentManagerModuleDoctor[]
     */
    public function getDoctors()//sqlite
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "select doctors.id,
                            doctors.firstname,
                            doctors.lastname,
                            doctors.middlename,
                            doctors.dateworkend
                        from doctors
                        order by doctors.dateworkend asc";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $doctors = array();
			while($row = ibase_fetch_row($result))
			{
                    $doctor = new TreatmentManagerModuleDoctor;
                    $doctor->doctorID = $row[0];
                    $doctor->doctorFName = $row[1];
                    $doctor->doctorLName = $row[2];
                    $doctor->doctorMName = $row[3];
                    $doctor->doctorLabelField = $row[2].' '.$row[1];//lastname firstname
                    //YYYY-MM-DD
                    $doctor->doctorDismissedFlag = 0;
                    if($row[4] != null)
                    {
                        $exp_date = $row[4];
                        $todays_date = date("Y-m-d");
                        $today = strtotime($todays_date);
                        $expiration_date = strtotime($exp_date);
                        if ($expiration_date < $today) 
                        {
                            $doctor->doctorDismissedFlag = 1;
                        } 
                    }
                    $doctors[] = $doctor;
			}
			ibase_commit($trans);
			ibase_free_query($query);
			ibase_free_result($result);
			$connection->CloseDBConnection();
            return $doctors;
    }
    
    //*************************************** PROCEDURES DICTIONARY START **********************************
     /**
     * @param number $healthType
     * @param int $priceIndex
     * @return TreatmentManagerModuleTemplateProcedure[]
     */
    public function getDictionaryProcedures($healthType, $priceIndex)//sqlite
    {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "select 
                            null,
                            1,
                            0,
                            healthproc.id,
                            healthproc.name,
                            healthproc.shifr,
                            healthproc.price".sql_checkPriceIndex($priceIndex).",
                            healthproc.farmscount,
                            healthproc.healthtype,
                            healthproc.healthtypes
                            
                            from healthproc
                            where healthproc.nodetype = 1";
            if((!is_nan($healthType))&&($healthType != -1))
            {
                $QueryText .= " and healthproc.healthtypes containing '$healthType'";
            }
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $procedures = array();
			while($row = ibase_fetch_row($result))
			{
                $procedure = new TreatmentManagerModuleTemplateProcedure;
				$procedure->procedurID = $row[0];
                $procedure->procedurVisit = $row[1];
                $procedure->procedurStacknumber = $row[2];
                $procedure->healthprocID = $row[3];
                $procedure->procedurName = $row[4];
                $procedure->procedurShifr = $row[5];
                $procedure->procedurPrice = $row[6];
                $healthprocFarmsCount = $row[7];
                $procedure->healthType = $row[8];
                $procedure->healthTypes = $row[9];
                $procedure->F39 = Form039Module::get039ProcValues(0,$row[3],$trans);
                if(($healthprocFarmsCount != null)&&($healthprocFarmsCount > 0))
                {
                    
                    // 0 - material costs *** tmp deprecated ***
                    // '' - material farmgroup code *** deprecated ***
                    $farmQueryText = "select
                                                HEALTHPROCFARM.ID,
                                                FARM.ID,
                                                FARM.NAME, 
                                                '', 
                                                HEALTHPROCFARM.QUANTITY,  
                                                0,
                                                (select UNITOFMEASURE.NAME from UNITOFMEASURE where UNITOFMEASURE.FARMID = HEALTHPROCFARM.FARM and UNITOFMEASURE.isbase = 1),
                                                HEALTHPROCFARM.HEALTHPROC
                                                
                                                from HEALTHPROCFARM 
                                                
                                                left join FARM
                                                    on HEALTHPROCFARM.FARM = FARM.ID
    
                                                where HEALTHPROCFARM.HEALTHPROC='$row[3]'";
                                                
            		$farmquery = ibase_prepare($trans, $farmQueryText);
            		$farmresult = ibase_execute($farmquery);
                    while ($farmRow = ibase_fetch_row($farmresult))
                    {
                        $farmobj = new TreatmentManagerModuleTemplateProcedurFarm;
                        $farmobj->ID = $farmRow[0];
                        $farmobj->farmID = $farmRow[1];
                        $farmobj->farmName = $farmRow[2];
                        $farmobj->farmGroup = $farmRow[3];
                        $farmobj->farmQuantity = $farmRow[4];
                        $farmobj->farmPrice = $farmRow[5];
                        $farmobj->farmUnitofmeasure = $farmRow[6];
                        $farmobj->farmHealthproc = $farmRow[7];
                        $procedure->farms[] = $farmobj;
                    }                            
    			}	
				$procedures[] = $procedure;
			}
			ibase_commit($trans);
			ibase_free_query($query);
			ibase_free_result($result);
			$connection->CloseDBConnection();
            return $procedures;
    }
    //*************************************** PROCEDURES DICTIONARY END **********************************
    
    //*************************************** PRINT TAMPLATE AS PLAN START **********************************
     /** 
 * @param string $docType
 * @param string $templateId
 * @param string $patientId
 * @param int $priceIndex
 * @return string
 */ 
  public static function printTemplateAsPlan($docType,$templateId,$patientId,$priceIndex)
    {   
        
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            
        $TEMPLATE=TreatmentManagerModule::getTemplate($templateId,$priceIndex,$trans); 
        $CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(true, $trans);
        $PATIENT_DATA=new PrintDataModule_PatientData($patientId, $trans);
        $PROCEDURES=TreatmentManagerModule::getTemplateProcedures($templateId,$priceIndex,$trans,false); 
        
        $TBS = new clsTinyButStrong;
        
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/TemplateAsPlan.'.$docType, OPENTBS_ALREADY_UTF8);  
        
       
        
       // $TBS->NoErr=true;
              
        $TBS->MergeBlock('PROCEDURES',$PROCEDURES);
        $TBS->MergeField('TEMPLATE',$TEMPLATE);
        $TBS->MergeField('CLINIC_DATA',$CLINIC_DATA);
        $TBS->MergeField('PATIENT_DATA',$PATIENT_DATA);
                
        $file_name = translit(str_replace(' ','_',$TEMPLATE->templateName.'_'.$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME.'_'.$PATIENT_DATA->CARDNUMBER));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;   
        
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
        
        
        
         ibase_commit($trans);
         $connection->CloseDBConnection();
                
        return $form_path; 
    }
      /** 
  * @param string $form_path
  * @return boolean
  */  
    public static function deleteTemplateAsPlan($form_path)
    {
        return deleteFileUtils($form_path);
    }

    /**
     * @param string $templateId
     * @param int $priceIndex
     * @param resource $trans
     * @return TreatmentManagerModuleTemplate
     */
    public static function getTemplate($templateId, $priceIndex ,$trans=null)
    {
		if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
            $QueryText = "select
                    template.id,
                    template.name,
                    template.protocol,
                    template.f43after,
                    template.createdate,
                    (select  sum(healthproc.price".sql_checkPriceIndex($priceIndex).")
                        from templateproc
                        inner join healthproc
                        on templateproc.healthproc = healthproc.id
                        where templateproc.template = template.id)

                from template
                   left join doctors on (doctors.id = template.doctor)
                where template.id = $templateId";
                
            $QueryText .= " order by template.id desc";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $template = null;
			if($row = ibase_fetch_row($result))
			{
                $template = new TreatmentManagerModuleTemplate;
				$template->templateID = $row[0];
                $template->templateName = $row[1];
                $template->protocolID =  $row[2];
			    $template->templateF43 = $row[3];
			    $template->templateCreateDate = $row[4];
			    $template->templateSum = $row[5];
			}
			ibase_free_query($query);
			ibase_free_result($result);
            
			 if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }     
            
            return $template;
    }
    //*************************************** PRINT TAMPLATE AS PLAN END **********************************
}

?>
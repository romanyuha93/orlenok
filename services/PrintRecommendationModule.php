<?php 

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";

require_once (dirname(__FILE__).'/tcpdf/config/lang/ukr.php');
require_once (dirname(__FILE__).'/tcpdf/exttcpdf.php');
require_once (dirname(__FILE__).'/tcpdf/tcpdf.php');
require_once (dirname(__FILE__).'/tbs/tbs_class.php');
require_once (dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');


//$F=new PrintRecommendationModule;
//$F->printRecommendation('docx','2');

class PrintRecommendationModule
{
    /** 
    * @param string $file_name
    */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);        
    }
	
	/**
	* @param string $docType
    * @param string $courseID
    * @return string
    */
	public function printRecommendation($docType,$courseID)
	{
        $connection = new Connection();
	    $connection->EstablishDBConnection();
		
		$res=(dirname(__FILE__).'/tbs/res/');
		
		if(!file_exists($res.'Recommendation.'.$docType))
        {
            return "TEMPLATE_NOTFOUND";
		}
		
		    
	    $trans = ibase_trans(IBASE_DEFAULT, $connection->connection); 
		$QueryText = "
            select TREATMENTADDITIONS.ADDITIONTXT,
			DOCTORS.SHORTNAME,
			TREATMENTCOURSE.PATIENT_FK
			
            from TREATMENTADDITIONS
			
			left join DOCTORS
                   on DOCTORS.ID =  TREATMENTADDITIONS.DOCTOR
			left join TREATMENTCOURSE
					on TREATMENTCOURSE.ID = TREATMENTADDITIONS.TREATMENTCOURSE
			where TREATMENTADDITIONS.TREATMENTCOURSE = $courseID";
			
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		
		$DOCTOR=new stdclass();
		while ($row = ibase_fetch_row($result))
			{
				$str=(nonull($row[0]))?text_blob_encode($row[0]):'';
				$Recommendation= new stdclass();
				$Recommendation->TEXT = $str;
				$DOCTOR->SHORTNAME=$row[1];
				$patientId=$row[2];
				$RecommendationArray[]=$Recommendation;
			}			
		$Recommendation=new stdclass();
		$RecommendationArray[]=$Recommendation;
		
		ibase_free_query($query);
        ibase_free_result($result);
				
		$CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(true, $trans);		
        $PATIENT_DATA=new PrintDataModule_PatientData($patientId, $trans);
        
		
		$TBS = new clsTinyButStrong;
        $TBS->NoErr=true;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		
        if(file_exists(dirname(__FILE__).'/tbs/res/Recommendation.'.$docType))
        {
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/Recommendation.'.$docType, OPENTBS_ALREADY_UTF8);  
        }
		
		$TBS->MergeBlock("Recommendation", $RecommendationArray);
		//$TBS->MergeField("ClinicRegistrationData", $ClinicRegistrationData);
        
        $TBS->MergeField('CLINIC_DATA',$CLINIC_DATA);
        $TBS->MergeField('PATIENT_DATA',$PATIENT_DATA);		
				
		$date_current = time();
        $DATE_CURRENT1=array(
        'DAY'=>ua_date("d", $date_current ),
        'MONTH'=>ua_date("M", $date_current ),
        'YEAR'=>ua_date("y", $date_current ));
		
		$DATE_CURRENT1['MONTH']=dateDeclineOnCases($DATE_CURRENT1['MONTH']);
				
		$TBS->MergeField("DATE_CURRENT", $DATE_CURRENT1);	
		$TBS->MergeField("DOCTOR", $DOCTOR);	
		
        $date = date("d-m-Y_H-i-s", time());
		$file_name = 'PatientRecommendation_'.translit(str_replace(' ','_',$PATIENT_DATA->SHORTNAME)).'__'.$date;
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = 'recom_'.uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
        ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $form_path;
	}
	
	
	
}



?>
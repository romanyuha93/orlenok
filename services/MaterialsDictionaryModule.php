<?php

require_once "Connection.php";
require_once "Utils.php";

class MaterialsDictionaryModuleFarm
{				
    /**
	* @var string
	*/
	var $label;
    
	/**
	* @var boolean
	*/
	var $isOpen;
    
   	/**
	* @var boolean
	*/
	var $isBranch;
    
	/**
	* @var string
	*/
	var $ID;    
	
	/**
	* @var string
	*/
	var $PARENTID;

	/**
	* @var string
	*/
	var $NODETYPE;          

	/**
	* @var string
	*/
	var $NAME;
	 
	/**
	* @var string
	*/
	var $BASEUNITOFMEASUREID;  
	
	/**
	* @var string
	*/
	var $BASEUNITOFMEASURENAME;
 
	/**
	* @var string
	*/
	var $NOMENCLATUREARTICLENUMBER;

	/**
	* @var string
	*/
	var $REMARK; 	
	
	/**
	* @var string
	*/
	var $FARMGROUPID;
	
	/**
	* @var string
	*/
	var $FARMGROUPCODE;
	
	/**
	* @var string
	*/
	var $FARMGROUPNAME;
	
	/**
	* @var string
	*/
	var $MINIMUMQUANTITY;
    
		/**
	* @var string
	*/
	var $OPTIMALQUANTITY;
    
	/**
	* @var MaterialsDictionaryModuleUnitOfMeasure[]
	*/
	var $unitsOfMeasure;
	
	/**
	* @var MaterialsDictionaryModuleFarmSupplier[]
	*/
	var $farmSuppliers;
	
	/**
	* @var MaterialsDictionaryModuleFarm[]
	*/
	var $children;
}

class MaterialsDictionaryModuleSupplier
{				
	/**
	* @var string
	*/
	var $ID;    
	
	/**
	* @var string
	*/
	var $NAME;    
}

class MaterialsDictionaryModuleUnitOfMeasure
{				
	/**
	* @var string
	*/
	var $ID;  

	/**
	* @var string
	*/
	var $FARMID;  
	
	/**
	* @var string
	*/
	var $NAME;

	/**
	* @var string
	*/
	var $COEFFICIENT; 	
	
	/**
	* @var string
	*/
	var $ISBASE; 
	
	/**
	* @var string
	*/
	var $MODIFIED;		
}

class MaterialsDictionaryModuleFarmSupplier
{				
	/**
	* @var string
	*/
	var $ID;  
	
	/**
	* @var string
	*/
	var $FARMID; 

	/**
	* @var string
	*/
	var $SUPPLIERID;  
	
	/**
	* @var string
	*/
	var $SUPPLIERNAME;
	
	/**
	* @var string
	*/
	var $SUPPLIERADDRESS;
	
	/**
	* @var string
	*/
	var $SUPPLIERTELEPHONENUMBER;
	
	/**
	* @var string
	*/
	var $PRICE;
	
	/**
	* @var string
	*/
	var $MODIFIED;
}

class MaterialsDictionaryModuleMaterialCardWindowData
{				
	/**
	* @var MaterialsDictionaryModuleUnitOfMeasure[]
	*/
	var $unitsOfMeasure;  
	
	/**
	* @var MaterialsDictionaryModuleFarmSupplier[]
	*/
	var $farmSuppliers; 

	/**
	* @var MaterialsDictionaryModuleSupplier[]
	*/
	var $suppliers;  
	
	/**
	* @var MaterialsDictionaryModuleFarmGroup[]
	*/
	var $farmGroups;
}

class MaterialsDictionaryModuleFarmGroup
{				
	/**
	* @var string
	*/
	var $ID;  	
	
	/**
	* @var string
	*/
	var $CODE;
	
	/**
	* @var string
	*/
	var $NAME;
    
        /**
	* @var string
	*/
	var $label;        
}
	
class MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult
{
	/**
	* @var string
	*/
	var $farmID;
	
	/**
	* @var string
	*/
	var $baseUnitOfMeasureID;
	
	/**
	* @var MaterialsDictionaryModuleUnitOfMeasure[]
	*/
	var $unitsOfMeasure;
	
	/**
	* @var MaterialsDictionaryModuleFarmSupplier[]
	*/
	var $farmSuppliers;
}	

class MaterialsDictionaryModule
{		
    /**
    * @param MaterialsDictionaryModuleFarm $p1
    * @param MaterialsDictionaryModuleSupplier $p2
    * @param MaterialsDictionaryModuleUnitOfMeasure $p3
    * @param MaterialsDictionaryModuleFarmSupplier $p4
    * @param MaterialsDictionaryModuleMaterialCardWindowData $p5
    * @param MaterialsDictionaryModuleFarmGroup $p6   
    * @param MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult $p7   
    * @return void 
    */    
	public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7) {}
    
    
	private function getRecordsRecursive($trans, $parentID, $whereStr)
	{
		$QueryText = 
		'select distinct		
				p.id,
				p.parentid,
				p.nodetype,
				p.name,
				unitofmeasure2.id "BASEUNITOFMEASUREID",
				unitofmeasure2.name "BASEUNITOFMEASURENAME",				
				p.nomenclaturearticlenumber,
				p.remark,				
				p.farmgroupid,
				farmgroup.code,
				farmgroup.name,
				p.minimumquantity,
                p.optimalquantity
		from farm p	
			 left join farmgroup on (farmgroup.id = p.farmgroupid)
			 left join
             (
                select
                    unitofmeasure.id,
                    unitofmeasure.farmid,
                    unitofmeasure.name
                from unitofmeasure
                where (unitofmeasure.isbase = 1)
             )
             unitofmeasure2 on (unitofmeasure2.farmid = p.id)
		where ';
 
		if (($parentID == "null") || ($parentID == ""))
		{ 
			$QueryText = $QueryText."(p.PARENTID is null)"; 
		}			
		else 
		{ 
			$QueryText = $QueryText."(p.PARENTID=$parentID)";
		}	

        $QueryText .= $whereStr."  order by p.name";
       //  file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
	   // file_put_contents("/home/zvitman/tmp.txt", var_export($QueryText, true));
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);

		$rows = array();	
	
		while ($row = ibase_fetch_row($result))
		{
			$obj = new MaterialsDictionaryModuleFarm();
			
			$obj->ID = $row[0]; 
			$obj->PARENTID = $row[1];
			$obj->NODETYPE = $row[2];
			$obj->NAME = $row[3];
            $obj->label = $row[3];
			$obj->BASEUNITOFMEASUREID = $row[4];
			$obj->BASEUNITOFMEASURENAME = $row[5];			
			$obj->NOMENCLATUREARTICLENUMBER = $row[6];
			$obj->REMARK = $row[7];		
			$obj->FARMGROUPID = $row[8];
			$obj->FARMGROUPCODE = $row[9];
			$obj->FARMGROUPNAME = $row[10];
			$obj->MINIMUMQUANTITY = $row[11];
			$obj->OPTIMALQUANTITY = $row[12];
			$parentID = $obj->ID;
			
			if ($obj->NODETYPE=="0")
			{
				$obj->children = $this->getRecordsRecursive($trans, $parentID, $whereStr);	
			}
			else
			{
				$QueryText2 = "select
					ID,			    
					NAME,
					COEFFICIENT,
					ISBASE
				from UNITOFMEASURE
				where FARMID = $obj->ID";
				$query2 = ibase_prepare($trans, $QueryText2);
				$result2 = ibase_execute($query2);
				$rows2 = array();		
				
				while ($row2 = ibase_fetch_row($result2))
				{ 
					$obj2 = new MaterialsDictionaryModuleUnitOfMeasure;
					
					$obj2->ID = $row2[0];						
					$obj2->NAME = $row2[1];		
					$obj2->COEFFICIENT = $row2[2];						
					$obj2->ISBASE = $row2[3];	
					
					$rows2[] = $obj2;
                }				
			
				$obj->unitsOfMeasure = $rows2;
				
				ibase_free_query($query2);
				ibase_free_result($result2);				
			}
			
			$rows[] = $obj;
		}	
		
		ibase_free_query($query);
		ibase_free_result($result);		
		
		return $rows; 
	}
	
	/** 									
 	* @param string $parentID
    * @param string $searchFields 
    * @param string $searchText
    *
	* @return MaterialsDictionaryModuleFarm[]
	*/ 
	public function getMaterialsDictionaryModuleData($parentID, $searchFields, $searchText) //sqlite
	{
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
             $whereStr="";
            if(nonull($searchText))
            {
                  $pFields="p.".str_replace("||' '||","||' '||p.",$searchFields);
                  $whereStr= " $pFields CONTAINING ('$searchText')"; 
                  $QueryText = "
                		select  distinct
                            p.PARENTID
                        from farm p where p.PARENTID is not null and ".$whereStr;
                        
                		$query = ibase_prepare($trans, $QueryText);
                		$result = ibase_execute($query);
                		$rows = array();	
                	   $parentIDs=array();
                		while ($row = ibase_fetch_row($result))
                		{
                		    $parentIDs[]=$row[0];  
                		}
                        if(count($parentIDs)>0)
                        {
                             $whereStr =" and (p.id in (".implode(',',$parentIDs).") or (".$whereStr."))";
                        }
			else
			{
				$whereStr =" and (".$whereStr.")";
			}
            }
        
        
			$returnObj = $this->getRecordsRecursive($trans, $parentID, $whereStr);
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
			return $returnObj;
	}	
	
	/**
 	* @param MaterialsDictionaryModuleFarm $newMaterialsDictionaryModuleFarm	
	* @param MaterialsDictionaryModuleUnitOfMeasure[] $unitOfMeasureArrayCollection
	* @param string[] $unitOfMeasureToDeleteArrayCollection 	
	* @param MaterialsDictionaryModuleFarmSupplier[] $farmSupplierArrayCollection
	* @param string[] $farmSupplierToDeleteArrayCollection 
	*
	* @return MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult
 	*/
  public function updateMaterialsDictionaryModuleFarm($newMaterialsDictionaryModuleFarm, $unitOfMeasureArrayCollection, $unitOfMeasureToDeleteArrayCollection, $farmSupplierArrayCollection, $farmSupplierToDeleteArrayCollection) 
  {
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$resultObject = new MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult();
			if (($newMaterialsDictionaryModuleFarm->ID == "") || ($newMaterialsDictionaryModuleFarm->ID == "null"))
			{
				$newMaterialsDictionaryModuleFarm->ID = ibase_gen_id("GEN_FARM_ID");
				$resultObject -> farmID = $newMaterialsDictionaryModuleFarm->ID;
			}
			$QueryText = 		  
			"update or insert into FARM
			(
				ID,                        
				PARENTID,                  
				NODETYPE,                  
				NAME,        
				NOMENCLATUREARTICLENUMBER, 
				REMARK,				
				FARMGROUPID,
				MINIMUMQUANTITY,
                OPTIMALQUANTITY
			)
			values($newMaterialsDictionaryModuleFarm->ID, "; 

			if (($newMaterialsDictionaryModuleFarm->PARENTID == "") || ($newMaterialsDictionaryModuleFarm->PARENTID == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."$newMaterialsDictionaryModuleFarm->PARENTID, ";
			}	
			
			$QueryText = $QueryText."$newMaterialsDictionaryModuleFarm->NODETYPE, '".safequery($newMaterialsDictionaryModuleFarm->NAME)."', ";			
						
			if (($newMaterialsDictionaryModuleFarm->NOMENCLATUREARTICLENUMBER == "") || ($newMaterialsDictionaryModuleFarm->NOMENCLATUREARTICLENUMBER == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newMaterialsDictionaryModuleFarm->NOMENCLATUREARTICLENUMBER)."', ";
			}	
			
			if (($newMaterialsDictionaryModuleFarm->REMARK == "") || ($newMaterialsDictionaryModuleFarm->REMARK == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."'".safequery($newMaterialsDictionaryModuleFarm->REMARK)."', ";
			}	
						
			if (($newMaterialsDictionaryModuleFarm->FARMGROUPID == "") || ($newMaterialsDictionaryModuleFarm->FARMGROUPID == "null") || ($newMaterialsDictionaryModuleFarm->FARMGROUPID == "empty"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."$newMaterialsDictionaryModuleFarm->FARMGROUPID, ";
			}
			
			if (($newMaterialsDictionaryModuleFarm->MINIMUMQUANTITY == "") || ($newMaterialsDictionaryModuleFarm->MINIMUMQUANTITY == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."$newMaterialsDictionaryModuleFarm->MINIMUMQUANTITY, ";
			}
			if (($newMaterialsDictionaryModuleFarm->OPTIMALQUANTITY == "") || ($newMaterialsDictionaryModuleFarm->OPTIMALQUANTITY == "null"))
			{
				$QueryText = $QueryText."null)";
			}
			else
			{
				$QueryText = $QueryText."$newMaterialsDictionaryModuleFarm->OPTIMALQUANTITY)";
			}
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);			
			ibase_free_query($query);
			
			if (isset($unitOfMeasureToDeleteArrayCollection) && (isset($unitOfMeasureToDeleteArrayCollection[0])))
			{
				$QueryText = 			
				"delete from UNITOFMEASURE
				where (ID is null)";		
						
				for ($i=0; isset($unitOfMeasureToDeleteArrayCollection[$i]); $i++)
				{
					$QueryText = $QueryText." or (ID=$unitOfMeasureToDeleteArrayCollection[$i])";
				}
				$query = ibase_prepare($trans, $QueryText);
				ibase_execute($query);
				ibase_free_query($query);
			}
	
			$unitsOfMeasure = array();			
			
			if (isset($unitOfMeasureArrayCollection) && (isset($unitOfMeasureArrayCollection[0])))
			{
				for ($i=0; isset($unitOfMeasureArrayCollection[$i]); $i++)
				{
					$unitOfMeasureRecord = new MaterialsDictionaryModuleUnitOfMeasure();	

					$unitOfMeasureRecord->ID = $unitOfMeasureArrayCollection[$i]->ID;
					
					if (($unitOfMeasureRecord->ID == "") || ($unitOfMeasureRecord->ID == "null"))
					{
						$unitOfMeasureRecord->ID = ibase_gen_id("GEN_UNITOFMEASURE_ID");						
					} 			
				
					$unitOfMeasureRecord->FARMID = $newMaterialsDictionaryModuleFarm->ID;					
					$unitOfMeasureRecord->NAME = trim($unitOfMeasureArrayCollection[$i]->NAME);
					$unitOfMeasureRecord->COEFFICIENT = $unitOfMeasureArrayCollection[$i]->COEFFICIENT;
					$unitOfMeasureRecord->ISBASE = $unitOfMeasureArrayCollection[$i]->ISBASE;
				
					$QueryText = 		  
					"update or insert into UNITOFMEASURE
					(
						ID,           
						FARMID,       
						NAME,         
						COEFFICIENT,  
						ISBASE       
					)
					values($unitOfMeasureRecord->ID, $unitOfMeasureRecord->FARMID, '".safequery($unitOfMeasureRecord->NAME)."', $unitOfMeasureRecord->COEFFICIENT, $unitOfMeasureRecord->ISBASE)";
					
					$query = ibase_prepare($trans, $QueryText);
					ibase_execute($query);
					ibase_free_query($query);
					$unitsOfMeasure[] = $unitOfMeasureRecord;
					
					if ($unitOfMeasureRecord->ISBASE == "1")
					{
						$resultObject -> baseUnitOfMeasureID = $unitOfMeasureRecord->ID;
					}					
				}	
			}
			$resultObject -> unitsOfMeasure = $unitsOfMeasure;	
			/*if (isset($farmSupplierToDeleteArrayCollection) && (isset($farmSupplierToDeleteArrayCollection[0])))
			{
				$QueryText = 			
				"delete from FARMSUPPLIER
				where (ID is null)";	
				for ($i=0; isset($farmSupplierToDeleteArrayCollection[$i]); $i++)
				{
					$QueryText = $QueryText." or (ID = $farmSupplierToDeleteArrayCollection[$i])";
				}
				$query = ibase_prepare($trans, $QueryText);
				ibase_execute($query);
				ibase_free_query($query);
			}
	
			$farmSuppliers = array();			
			
			if (isset($farmSupplierArrayCollection) && (isset($farmSupplierArrayCollection[0])))
			{
				for ($i=0; isset($farmSupplierArrayCollection[$i]); $i++)
				{
					$farmSupplierRecord = new MaterialsDictionaryModuleFarmSupplier();
					$farmSupplierRecord->ID = $farmSupplierArrayCollection[$i]->ID;
					if (($farmSupplierRecord->ID == "") || ($farmSupplierRecord->ID == "null"))
					{
						$farmSupplierRecord->ID = ibase_gen_id("GEN_FARMSUPPLIER_ID");						
					}
					$farmSupplierRecord->FARMID = $newMaterialsDictionaryModuleFarm->ID;						
					$farmSupplierRecord->SUPPLIERID = $farmSupplierArrayCollection[$i]->SUPPLIERID;
					$farmSupplierRecord->PRICE = $farmSupplierArrayCollection[$i]->PRICE;
					$QueryText = 		  
					"update or insert into FARMSUPPLIER
					(
						ID,           
						FARMID,       
						SUPPLIERID,         
						PRICE       
					)
					values($farmSupplierRecord->ID, $farmSupplierRecord->FARMID, $farmSupplierRecord->SUPPLIERID, $farmSupplierRecord->PRICE)";
					$query = ibase_prepare($trans, $QueryText);
					ibase_execute($query);
					ibase_free_query($query);
					$farmSuppliers[] = $farmSupplierRecord;								
				}	
			}
			$resultObject -> farmSuppliers = $farmSuppliers;
            */
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
			return $resultObject;
  }
	
	/**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
	public function deleteMaterialsDictionaryModuleRecords($Arguments) 
	{ 
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);			
			
			$QueryText = "delete from FARM where (ID is null)";
			
			for ($i=0; isset($Arguments[$i]); $i++)
			{ 
				$QueryText = $QueryText." or (ID=$Arguments[$i])"; 
			}	
		
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			$success = ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
			return $success;	
	}
	
	/** 									
 	* @param string $farmID
	*
	* @return MaterialsDictionaryModuleMaterialCardWindowData
	*/ 
	public function getMaterialCardWindowData($farmID) //sqlite
	{ 
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$returnObj = new MaterialsDictionaryModuleMaterialCardWindowData();
			$returnObj->unitsOfMeasure = array();
			if (($farmID != "") && ($farmID != "null"))
			{
				$QueryText = "select
					ID,          
					FARMID,      
					NAME,        
					COEFFICIENT, 
					ISBASE	
				from UNITOFMEASURE
				where FARMID = $farmID";
				$query = ibase_prepare($trans, $QueryText);
				$result = ibase_execute($query);
				$rows = array();	
				while ($row = ibase_fetch_row($result))
				{ 
					$obj = new MaterialsDictionaryModuleUnitOfMeasure;
					
					$obj->ID = $row[0];			        
					$obj->FARMID = $row[1];    
					$obj->NAME = $row[2];
					$obj->COEFFICIENT = $row[3];
					$obj->ISBASE = $row[4];			
					$obj->MODIFIED = "0";
					
					$rows[] = $obj;
                }
				$returnObj->unitsOfMeasure = $rows;
				ibase_free_query($query);
				ibase_free_result($result);				
			}	

			$returnObj->farmSuppliers = array();			
			/*
			if (($farmID != "") && ($farmID != "null"))
			{
				$QueryText = "select
					farmsupplier.id,          
					farmsupplier.farmid,      
					supplierid,        
					supplier.name, 
					supplier.address,
					supplier.telephonenumber,
					farmsupplier.price	
				from FARMSUPPLIER				
					left join supplier on (supplier.id = farmsupplier.supplierid)
				where FARMID = $farmID  order by supplier.name";
				$query = ibase_prepare($trans, $QueryText);
				$result = ibase_execute($query);
				$rows = array();
				while ($row = ibase_fetch_row($result))
				{ 
					$obj = new MaterialsDictionaryModuleFarmSupplier;
					
					$obj->ID = $row[0];			        
					$obj->FARMID = $row[1];    
					$obj->SUPPLIERID = $row[2];
					$obj->SUPPLIERNAME = $row[3];
					$obj->SUPPLIERADDRESS = $row[4];
					$obj->SUPPLIERTELEPHONENUMBER = $row[5]; 		
					$obj->PRICE = $row[6];	
					$obj->MODIFIED = "0";
					
					$rows[] = $obj;
                }
				$returnObj->farmSuppliers = $rows;
				ibase_free_query($query);
				ibase_free_result($result);				
			}
			$QueryText = "select
				ID,			    
				NAME  			
			from SUPPLIER";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$rows = array();		
			
			while ($row = ibase_fetch_row($result))
			{ 
				$obj = new MaterialsDictionaryModuleSupplier;
				
				$obj->ID = $row[0];						
				$obj->NAME = $row[1];			
				
				$rows[] = $obj;
            }				
				
			$returnObj->suppliers = $rows;
			ibase_free_query($query);
			ibase_free_result($result);
            */
			$QueryText = 
			"select
				ID,	
				CODE,
				NAME  			
			from FARMGROUP
			order by CODE";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$rows = array();		
			
			while ($row = ibase_fetch_row($result))
			{ 
				$obj = new MaterialsDictionaryModuleFarmGroup;
				
				$obj->ID = $row[0];						
				$obj->CODE = $row[1];	
				$obj->NAME = $row[2];		
				
				$rows[] = $obj;
            }
			
			$returnObj->farmGroups = $rows;
			ibase_free_query($query);
			ibase_free_result($result);
			ibase_commit($trans);
			$connection->CloseDBConnection();
			return $returnObj;
	}	
			
	/**
 	* @param MaterialsDictionaryModuleFarmGroup $newMaterialsDictionaryModuleFarmGroup
    * @return MaterialsDictionaryModuleFarmGroup	
 	*/
  public function updateMaterialsDictionaryModuleFarmGroup($newMaterialsDictionaryModuleFarmGroup) 
  { 
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			if (($newMaterialsDictionaryModuleFarmGroup->ID == "") || ($newMaterialsDictionaryModuleFarmGroup->ID == "null"))
			{
				$newMaterialsDictionaryModuleFarmGroup->ID = ibase_gen_id("GEN_FARMGROUP_ID");
			}
			$QueryText = 		  
			"update or insert into FARMGROUP
			(
				ID, 
				CODE,
				NAME   
			)
			values($newMaterialsDictionaryModuleFarmGroup->ID, '".safequery($newMaterialsDictionaryModuleFarmGroup->CODE)."', '".safequery($newMaterialsDictionaryModuleFarmGroup->NAME)."')";	

			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);	
			
			$obj = new MaterialsDictionaryModuleFarmGroup;
			$obj->ID = $newMaterialsDictionaryModuleFarmGroup->ID;
			$obj->CODE = $newMaterialsDictionaryModuleFarmGroup->CODE;
			$obj->NAME = $newMaterialsDictionaryModuleFarmGroup->NAME;
			ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
			return $obj;
  }
  
  	/**
 	* @param string $farmId
    * @param string $newParentId
    * @return boolean	
 	*/
  public function updateMaterialsDictionaryModuleParentId($farmId, $newParentId) 
  { 
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = 		  
			"update farm set PARENTID=$newParentId where ID=$farmId";	

			$query = ibase_prepare($trans, $QueryText);
            
			ibase_execute($query);	
            ibase_free_query($query);
            
			$res=ibase_commit($trans);
			$connection->CloseDBConnection();
			return $res;
  }
}
?>

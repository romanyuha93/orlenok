<?php

require_once "Connection.php";
require_once "Utils.php";

class ContactBookModule_Object
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $name;
    /**
    * @var string
    */
    var $comment;
    /**
    * @var string
    */
    var $phones;
	/**
    * @var string
    */
    var $address;

 }
 
 
class ContactBookModule
{		
    
     /**
     * @param ContactBookModule_Object $p1
     * @return void
     */
    public function registerTypes($p1){}
  
    /** 
 	* @return ContactBookModule_Object[]
	*/ 
    public function getContacts() //sqlite
     { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	    $QueryText ="select
                        contacts.id,
                        contacts.name,
                        contacts.comment,
                        contacts.phones,
                        contacts.address
                        
                        from contacts";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new ContactBookModule_Object;
          $obj->id = $row[0];
          $obj->name = $row[1];
          $obj->comment = $row[2];
          $obj->phones = $row[3];
          $obj->address=$row[4];
		  $objs[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
          
    /**
 	* @param ContactBookModule_Object $contact
    * @return boolean
 	*/
    public function addContact($contact) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into 
                        contacts(
                        id,
                        name,
                        comment,
                        phones,
                        address) 
                      values 
                      (null,
                      '".safequery($contact->name)."',
                      '".safequery($contact->comment)."',
                      '".safequery($contact->phones)."',
                      '".safequery($contact->address)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
    * @param ContactBookModule_Object $contact
    * @return boolean
 	*/
    public function updateContact($contact) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update contacts set name = '".safequery($contact->name)."',
											comment ='".safequery($contact->comment)."',
											phones = '".safequery($contact->phones)."',
                                            address = '".safequery($contact->address)."'
					     where id ='$contact->id'"; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
    * @param string $contactId
    * @return boolean
 	*/
    public function deleteContact($contactId) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from contacts where (ID is null) or (ID=$contactId)";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;
    }
}
?>
<?php

require_once "Connection.php";
require_once "Utils.php";

class ChiefBudgetModule_Group
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $group_type;
    /**
     * @var int
     */
    var $rules;
    /**
     * @var int
     */
    var $is_cashpayment;
    /**
     * @var int
     */
    var $monthly_is;
    /**
     * @var number
     */
    var $monthly_defaultsumm;
    /**
     * @var int
     */
    var $group_color;
    /**
     * @var int
     */
    var $group_sequince;
    /**
     * @var string
     */
    var $group_residue;
    
    /**
     * @var string
     */
    var $group_residue_name;
    
    /**
     * @var int
     */
    var $isEmployee;
}
class ChiefBudgetModule_GroupSplit
{
    /**
     * @var string
     */
    var $id;
    /**
     * @var int
     */
    var $sequince;
}

class ChiefBudgetModule_BudgetData
{
    /**
     * -1 - patients
     * -2 - salary
     * -3 - expenses
     * -4 - default
     * 
     * @var string
     */
    var $group_id;
    /**
     * @var string
     */
    var $group_name;
    /**
     * @var int
     */
    var $group_is_cashpayment;
    /**
     * @var int
     */
    var $group_monthly_is;
    /**
     * @var number
     */
    var $group_monthly_defaultsumm;
    /**
     * @var int
     */
    var $group_color;
    
    
    /**
     * @var number
     */
    var $cash_expense;
    /**
     * @var number
     */
    var $cash_income;

    
    /**
     * @var number
     */
    var $noncash_income;
    /**
     * @var number
     */
    var $noncash_expense;
    
    
    /**
     * @var number
     */
    var $cash_summ;
    /**
     * @var number
     */
    var $noncash_summ;
    
    /**
     * @var number
     */
    var $summ;
    
    
    /**
     * @var number
     */
    var $income_summ;
    /**
     * @var number
     */
    var $expense_summ;
    
    /**
     * @var ChiefBudgetModule_BudgetData
     */
    var $included;
    
}

class ChiefBudgetModule_BudgetStatistics
{
    /**
     * @var string
     */
    var $period;
    
    /**
     * @var string
     */
    var $period_type;
    
    /**
     * @var string
     */
    var $period_startDate;
    /**
     * @var string
     */
    var $period_endDate;
    
    /**
     * @var number
     */
    var $cash_incomeSumm;
    /**
     * @var number
     */
    var $cash_expenseSumm;
    
    
    /**
     * @var number
     */
    var $noncash_incomeSumm;
    /**
     * @var number
     */
    var $noncash_expenseSumm;
    
    
    /**
     * @var ChiefBudgetModule_BudgetData[]
     */
    var $budget;
    
}

class ChiefBudgetModule
{
    
    /**
     * @param ChiefBudgetModule_Group $p1
     * @param ChiefBudgetModule_BudgetData $p2
     * @param ChiefBudgetModule_BudgetStatistics $p3
     * @param ChiefBudgetModule_GroupSplit $p4
     * @return void
     */
    public function registertypes($p1, $p2, $p3, $p4){}
    
    ///************************************************************************************************///
    ///                         GROUP                                                  ///
    ///                                START                                                             ///
    ///************************************************************************************************///
    
    /** 
    * @param int $groupRules
    * @param int $groupTypes
    * @param int $isCashpayment
    * @return ChiefBudgetModule_Group[]
    */  
    public function getGroups($groupRules, $groupTypes, $isCashpayment)//sqlite
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select
                        accountflow_groups.id,
                        accountflow_groups.name,
                        accountflow_groups.group_type,
                        accountflow_groups.rules,
                        accountflow_groups.is_cashpayment,
                        accountflow_groups.monthly_is,
                        accountflow_groups.monthly_defaultsumm,
                        accountflow_groups.color,
                        accountflow_groups.gridsequence,
                        accountflow_groups.residue,
                        residue.name,
                        accountflow_groups.isemployee
                    from accountflow_groups
                    left join accountflow_groups as residue
                    on accountflow_groups.residue = residue.id ";
        
        if($groupRules != -1)
        {
            $QueryText .= " where accountflow_groups.rules = $groupRules ";
        }
        else
        {
            $QueryText .= " where accountflow_groups.id is not null ";
        }       
         
        if($isCashpayment != -1)
        {
            $QueryText .= " and accountflow_groups.is_cashpayment = $isCashpayment";
        }
        
        if($groupTypes != -1)
        {
            $QueryText .= " and accountflow_groups.group_type = $groupTypes";
        }           
        $QueryText .= " order by accountflow_groups.gridsequence";                                       
        
        $Query = ibase_prepare($trans, $QueryText);
        $QueryResult=ibase_execute($Query);
        $rows = array();
		while ($row = ibase_fetch_row($QueryResult))
		{ 
            $obj = new ChiefBudgetModule_Group;
            $obj->id = $row[0];
            $obj->name = $row[1];
            $obj->group_type = $row[2];
            $obj->rules = $row[3];
            $obj->is_cashpayment = $row[4];
            $obj->monthly_is = $row[5];
            $obj->monthly_defaultsumm = $row[6];
            $obj->group_color = $row[7];
            $obj->group_sequince = $row[8];
            $obj->group_residue = $row[9];
            $obj->group_residue_name = $row[10];
            $obj->isEmployee = $row[11];
            $rows[] = $obj;
		}
        ibase_free_query($Query);
        ibase_free_result($QueryResult);
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
    }
    
    /** 
    * @param ChiefBudgetModule_Group $group
    * @return ChiefBudgetModule_Group
    */  
    public function saveGroup($group)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        if($group->id == null && $group->isEmployee == 1)
        {
            $QueryEmpText = "select ID, NAME, GROUP_TYPE, RULES, IS_CASHPAYMENT, MONTHLY_IS, MONTHLY_DEFAULTSUMM, COLOR, GRIDSEQUENCE, RESIDUE,
                       ISEMPLOYEE
                from ACCOUNTFLOW_GROUPS
                where ISEMPLOYEE = 1"; 
            $queryEmp = ibase_prepare($trans, $QueryEmpText);
    		$resultEmp = ibase_execute($queryEmp);
            
            $objEmp = new ChiefBudgetModule_Group;
    		if($rowEmp = ibase_fetch_row($resultEmp))
    		{ 
                $objEmp->id = 'GROUP_EXIST';
                $objEmp->name = $rowEmp[1];
                $objEmp->group_type = $rowEmp[2];
                $objEmp->rules = $rowEmp[3];
                $objEmp->is_cashpayment = $rowEmp[4];
                $objEmp->monthly_is = $rowEmp[5];
                $objEmp->monthly_defaultsumm = $rowEmp[6];
                $objEmp->group_color = $rowEmp[7];
                $objEmp->group_sequince = $rowEmp[8];
                $objEmp->group_residue = $rowEmp[9];
                $objEmp->isEmployee = $rowEmp[10];
                
        		ibase_free_result($resultEmp);
        		ibase_free_query($queryEmp);
                ibase_commit($trans);
        		$connection->CloseDBConnection();
                
        		return $objEmp;
    		}
        }
          
		$QueryText = "update or insert into accountflow_groups
                            (accountflow_groups.id,
                            accountflow_groups.group_type,
                            accountflow_groups.rules,
                            accountflow_groups.name,
                            accountflow_groups.monthly_is,
                            accountflow_groups.monthly_defaultsumm,
                            accountflow_groups.is_cashpayment,
                            accountflow_groups.color,
                            accountflow_groups.residue,
                            accountflow_groups.isemployee)
                        
                        values (";
           
        if(($group->id != null)&&($group->id != 'null')&&($group->id != ''))
        {
            $QueryText .=  "$group->id, "; 
        } 
        else
        {
            $QueryText .=  "null, ";
        } 
        
        //
        $QueryText .=  "$group->group_type,
                        $group->rules,
                        '".safequery($group->name)."', 
                        $group->monthly_is, ";                 
                            
        if(is_nan($group->monthly_defaultsumm) == false)
        {
            $QueryText .=  "'$group->monthly_defaultsumm',";
        }   
        else
        {
            $QueryText .=  "0, ";
        }
           
        $QueryText .=  "$group->is_cashpayment, $group->group_color, ";
        if( ($group->group_residue != null) && ($group->group_residue != '') && ($group->group_residue != 'null') )
        {
            $QueryText .=  "$group->group_residue, ";
        }
        else
        {
            $QueryText .=  "null, ";
        }
        $QueryText .=  "$group->isEmployee "; 
        $QueryText .=  ") matching (id) returning id";
             
        $query = ibase_prepare($trans, $QueryText);
        $resultQuery = ibase_execute($query);
        $rowIdOrder = ibase_fetch_row($resultQuery);
        $group->id = $rowIdOrder[0];
        ibase_free_query($query);
        ibase_free_result($resultQuery);
           
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $group;
    }
    
    /** 
    * @param ChiefBudgetModule_GroupSplit[] $groups
    * @return boolean
    */
    public function splitGroups($groups)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        foreach ($groups as $group)
        { 
            $QueryText = "update accountflow_groups set
                            accountflow_groups.gridsequence = $group->sequince
                        where accountflow_groups.id = $group->id";
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            ibase_free_query($query);
        }
           
        $success = ibase_commit($trans);
		$connection->CloseDBConnection();
        
		return $success;
    } 
    
    /**
	* @param string $groupId
	* @param string $passForDelete
	* @return boolean
 	*/  
    public function removeGroup($groupId, $passForDelete) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               ibase_free_result($result);
        	   ibase_free_query($query);
        	   $QueryText = "delete from accountflow_groups where accountflow_groups.id = '$groupId'";
        	   $query = ibase_prepare($trans, $QueryText);
        	   ibase_execute($query);
        	   ibase_free_query($query);
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
    }
    
    ///************************************************************************************************///
    ///                         GROUP                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @param string $period
    * @param boolean $accumulationMode
    * @param boolean $dynamicExpenses
    * @param boolean $byInvoiceDate
    * 
    * @param boolean $payidInProceduresMode
    * @param float $payidInProceduresPrecents
    * 
    * @return ChiefBudgetModule_BudgetStatistics[]
    */  
    public function getBudgetStatistics($startDate, $endDate, $period, $accumulationMode, $dynamicExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents)
    {
        
        $connection = new Connection();
        $connection->EstablishDBConnection("main",false);
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $rows = array();
        /*
            <fx:Object label="???" data="1d"/>
    		<fx:Object label="??????" data="7d"/>
    	    <fx:Object label="??????" data="1m"/>
    		<fx:Object label="????" data="12m"/>
        */
        $fetchLimit = '';
        if($period == "d1d")
        {
            $fetchLimit = "1 days";
        }
        else if($period == "d7d")
        {
            $fetchLimit = "7 days";
        }
        else if($period == "m1m")
        {
            $fetchLimit = "1 months";
        }
        else if($period == "m12m")
        {
            $fetchLimit = "12 months";
        }
        
        if($fetchLimit == '')
        {
            return null;
        }
        //$fetchLimit = "12 months";
        //$fetchLimit = "1 months";
        //$fetchLimit = "7 days";
        //$fetchLimit = "1 days"; 
        
        $a = strtotime($startDate);
        if($accumulationMode == true)
        { 
            $period_inc = 0;
            $rows[] = $this->fetchAccumulationChunk($a,$period_inc,$period,$dynamicExpenses,$byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents,$trans);
            $period_inc++; 
        }
        else
        {
           $period_inc = 1; 
        }
        
        $a += 86400; 
        $b = strtotime($endDate)+86400;
        
        if ($b <= strtotime(gmdate("Y-m-d",$a)." +".$fetchLimit)) 
        {
            $rows[] = $this->fetchChunk($a,$b,$period_inc,$period,$dynamicExpenses,$byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents,$trans);
            $period_inc++;
            $a += 86400;
        }
        else 
        { 
           $lowerBound = $a;
           $upperBound = strtotime(gmdate("Y-m-d",$a)." +".$fetchLimit);
           while ($upperBound < $b) 
           {
             $rows[] =  $this->fetchChunk($lowerBound,$upperBound,$period_inc,$period,$dynamicExpenses,$byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents,$trans);
             $period_inc++;
             $lowerBound = $upperBound;
             $upperBound = strtotime(gmdate("Y-m-d",$lowerBound)." +".$fetchLimit);
             
             $lowerBound += 86400;
             $upperBound += 86400;
           }
           $rows[] = $this->fetchChunk($lowerBound,$b,$period_inc,$period,$dynamicExpenses,$byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents,$trans);
           $period_inc++;
        }
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows; 
    }
    /**
    * @return ChiefBudgetModule_BudgetStatistics
    */ 
    private function fetchChunk($a,$b,$period_inc,$period,$dynamicExpenses,$byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents,$trans) {
        $obj = new ChiefBudgetModule_BudgetStatistics;
        $obj->period = $period_inc;
        $cash_income_summ = 0;
        $cash_expense_summ = 0;
        
        $noncash_income_summ = 0;
        $noncash_expense_summ = 0;
        
        $rows = $this->_getBudgetData(gmdate("Y-m-d",$a), gmdate("Y-m-d",$b), false, false, false, $dynamicExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans);
        $obj->budget = $rows;
        for($i = 0; $i < count($rows); ++$i) 
        {
            $cash_income_summ += $rows[$i]->cash_income;
            $cash_expense_summ += $rows[$i]->cash_expense;
            $noncash_income_summ += $rows[$i]->noncash_income;
            $noncash_expense_summ += $rows[$i]->noncash_expense;
        }
        $obj->cash_incomeSumm = $cash_income_summ;
        $obj->cash_expenseSumm = $cash_expense_summ;
        $obj->noncash_incomeSumm = $noncash_income_summ;
        $obj->noncash_expenseSumm = $noncash_expense_summ;
        
        $obj->period_startDate = gmdate("Y-m-d",$a);
        $obj->period_endDate = gmdate("Y-m-d",$b);
        $obj->period_type = $period;
        return $obj;
      //echo gmdate("Y-m-d",$a)." to ".gmdate("Y-m-d",$b)."<br>";
    }
    
    /**
    * @return ChiefBudgetModule_BudgetStatistics
    */ 
    private function fetchAccumulationChunk($a,$period_inc,$period,$dynamicExpenses,$byInvoiceDate,$payidInProceduresMode, $payidInProceduresPrecents,$trans) {
        $obj = new ChiefBudgetModule_BudgetStatistics;
        $obj->period = $period_inc;
        $cash_income_summ = 0;
        $cash_expense_summ = 0;
        
        $noncash_income_summ = 0;
        $noncash_expense_summ = 0;
        
        $rows = $this->_getBudgetData(null, gmdate("Y-m-d",$a), false, false, false, $dynamicExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans);
        $obj->budget = $rows;
        for($i = 0; $i < count($rows); ++$i) 
        {
            $cash_income_summ += $rows[$i]->cash_income;
            $cash_expense_summ += $rows[$i]->cash_expense;
            $noncash_income_summ += $rows[$i]->noncash_income;
            $noncash_expense_summ += $rows[$i]->noncash_expense;
        }
        $obj->cash_incomeSumm = $cash_income_summ;
        $obj->cash_expenseSumm = $cash_expense_summ;
        $obj->noncash_incomeSumm = $noncash_income_summ;
        $obj->noncash_expenseSumm = $noncash_expense_summ;
        
        $obj->period_startDate = null;
        $obj->period_endDate = gmdate("Y-m-d",$a);
        $obj->period_type = $period;
        return $obj;
      //echo gmdate("Y-m-d",$a)." to ".gmdate("Y-m-d",$b)."<br>";
    }
    
    ///************************************************************************************************///
    ///                         BUDGET                                                  ///
    ///                                START                                                             ///
    ///************************************************************************************************///
    /**
    * @param string $startDate
    * @param string $endDate
    * @param boolean $considerDiscount
    * @param boolean $doctorsDynamic
    * @param boolean $considerExpenses
    * @param boolean $dynamicExpenses
    * @param boolean $byInvoiceDate
    * 
    * @param boolean $payidInProceduresMode
    * @param float $payidInProceduresPrecents
    * 
    * @return ChiefBudgetModule_BudgetData[]
    */  
    public function getBudgetData($startDate, $endDate, $considerDiscount, $doctorsDynamic, $considerExpenses, $dynamicExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $rows = $this->_getBudgetData($startDate, $endDate, $considerDiscount, $doctorsDynamic, $considerExpenses, $dynamicExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans);
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
    }
    
    ///************************************************************************************************///
    ///                         BUDGET                                                  ///
    ///                                END                                                             ///
    ///************************************************************************************************///
    
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @param boolean $considerDiscount
    * @param boolean $doctorsDynamic
    * @param boolean $considerExpenses
    * 
    * 
    * @param boolean $payidInProceduresMode
    * @param float $payidInProceduresPrecents
    * 
    * @return ChiefBudgetModule_BudgetData[]
    */  
    private function _getBudgetData($startDate, $endDate, $considerDiscount, $doctorsDynamic, $considerExpenses, $dynamicExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans)
    {
        $rows = array();
        if($startDate == null)
        {
            $startDate = '2000-01-01';
        }
  // -1 patients query START
		$sql_patients = "select

                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 0
                                            and accountflow.summ > 0
                                            and accountflow.is_cashpayment = 0
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_income,
                                
                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 0
                                            and accountflow.summ > 0
                                            and accountflow.is_cashpayment = 1
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_income,
                                
                                (select coalesce(-sum(accountflow.summ),0) from accountflow
                                            where accountflow.operationtype = 0
                                                and accountflow.summ < 0
                                                and accountflow.is_cashpayment = 0
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expense,
                                
                                (select coalesce(-sum(accountflow.summ),0) from accountflow
                                            where accountflow.operationtype = 0
                                                and accountflow.summ < 0
                                                and accountflow.is_cashpayment = 1
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_expense ";
        $sql_patients .= 'from RDB$DATABASE';  
        
        $query_patients = ibase_prepare($trans, $sql_patients);
        $result_patients=ibase_execute($query_patients);
		
            $row = ibase_fetch_row($result_patients);
            $obj = new ChiefBudgetModule_BudgetData;
            $obj->group_id = -1;
            $obj->group_name = 'patients';
            $obj->group_color = 3381504;//green
            $obj->cash_income = $row[0];
            $obj->noncash_income = $row[1];
            $obj->cash_expense = $row[2];
            $obj->noncash_expense = $row[3];
            
            $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
            $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
            $obj->summ = $obj->cash_summ + $obj->noncash_summ;
            
            $obj->income_summ = $obj->cash_income + $obj->noncash_income;
            $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
            
            $rows[] = $obj;
            
        ibase_free_query($query_patients);
        ibase_free_result($result_patients);
  // patients query END
        
        
  // -2 doctors salary query START
        if($doctorsDynamic == true)
        {
                $obj = new ChiefBudgetModule_BudgetData;
                $obj->group_id = -5;
                $obj->group_name = 'doctorsDynamic';
                $obj->group_color = 16711680;//red
                $obj->cash_income = 0;
                $obj->cash_expense = $this->_getDoctorsDynamic($startDate, $endDate, $considerDiscount, $considerExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans);
                $obj->noncash_expense = 0;
                $obj->noncash_income = 0;
                
                $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
                $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
                $obj->summ = $obj->cash_summ + $obj->noncash_summ;
                
                $obj->income_summ = $obj->cash_income + $obj->noncash_income;
                $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
                
                // for included START
                $sql_salary = "select
    
                                    (select coalesce(sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 2
                                                and accountflow.summ > 0
                                                and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                                and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_income,
                                    
                                    (select coalesce(-sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 2
                                                and accountflow.summ < 0
                                                and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                                and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expenses ";
                $sql_salary .= 'from RDB$DATABASE';  
                $query_salary = ibase_prepare($trans, $sql_salary);
                $result_salary=ibase_execute($query_salary);
        		
                    $row = ibase_fetch_row($result_salary);
                    $includedObj = new ChiefBudgetModule_BudgetData;
                    $includedObj->group_id = -2;
                    $includedObj->group_name = 'doctors';
                    //$obj->group_color = 16711680;//red
                    $includedObj->cash_income = $row[0];
                    $includedObj->cash_expense = $row[1];
                    
                    $includedObj->cash_summ = $includedObj->cash_income - $includedObj->cash_expense;
                    $includedObj->noncash_summ = $includedObj->noncash_income - $includedObj->noncash_expense;
                    $includedObj->summ = $includedObj->cash_summ + $includedObj->noncash_summ;
                    
                    $includedObj->income_summ = $includedObj->cash_income + $includedObj->noncash_income;
                    $includedObj->expense_summ = $includedObj->cash_expense + $includedObj->noncash_expense;
                    
                    
                ibase_free_query($query_salary);
                ibase_free_result($result_salary);
                $obj->included = $includedObj;
                // for included END
                
                $rows[] = $obj;
                
                
        }
        else
        {
    		$sql_salary = "select
    
                                    (select coalesce(sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 2
                                                and accountflow.summ > 0
                                                and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                                and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_income,
                                    
                                    (select coalesce(-sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 2
                                                and accountflow.summ < 0
                                                and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                                and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expenses ";
            $sql_salary .= 'from RDB$DATABASE';  
            $query_salary = ibase_prepare($trans, $sql_salary);
            $result_salary=ibase_execute($query_salary);
    		
                $row = ibase_fetch_row($result_salary);
                $obj = new ChiefBudgetModule_BudgetData;
                $obj->group_id = -2;
                $obj->group_name = 'doctors';
                $obj->group_color = 16711680;//red
                $obj->cash_income = $row[0];
                $obj->cash_expense = $row[1];
                
                $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
                $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
                $obj->summ = $obj->cash_summ + $obj->noncash_summ;
                
                $obj->income_summ = $obj->cash_income + $obj->noncash_income;
                $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
                
                $includedObj = new ChiefBudgetModule_BudgetData;
                $includedObj->group_id = -5;
                $includedObj->group_name = 'doctorsDynamic';
                $includedObj->group_color = 16711680;//red
                $includedObj->cash_income = 0;
                $includedObj->cash_expense = $this->_getDoctorsDynamic($startDate, $endDate, $considerDiscount, $considerExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans);
                $includedObj->noncash_expense = 0;
                $includedObj->noncash_income = 0;
                
                $includedObj->cash_summ = $includedObj->cash_income - $includedObj->cash_expense;
                $includedObj->noncash_summ = $includedObj->noncash_income - $includedObj->noncash_expense;
                $includedObj->summ = $includedObj->cash_summ + $includedObj->noncash_summ;
                
                $includedObj->income_summ = $includedObj->cash_income + $includedObj->noncash_income;
                $includedObj->expense_summ = $includedObj->cash_expense + $includedObj->noncash_expense;
                $obj->included = $includedObj;
                
                $rows[] = $obj;
                
            ibase_free_query($query_salary);
            ibase_free_result($result_salary);
        }
  // doctors salary query END
  
  // -3 treatmentproces expnese query START
    
    		$sql_proc_expenses = "select 
                                    (select coalesce(sum(treatmentproc.expenses * treatmentproc.proc_count),0) from treatmentproc ";
              
            if($byInvoiceDate == true)
            {
                $sql_proc_expenses .= "inner join invoices on invoices.id = treatmentproc.invoiceid
                                       
                                       where treatmentproc.doctor is not null
                                            and invoices.createdate >= '$startDate 00:00:00'
                                            and invoices.createdate <= '$endDate 23:59:59') as planeCash, "; //left
            }
            else
            {
                $sql_proc_expenses .= "where treatmentproc.doctor is not null
                                and treatmentproc.dateclose >= '$startDate 00:00:00'
                                and treatmentproc.dateclose <= '$endDate 23:59:59') as planeCash, "; //
            }
            
            
            
           	$sql_proc_expenses .= "(select coalesce(-sum(accountflow.summ),0)
                                    from treatmentproc
                                        inner join accountflow
                                        on accountflow.texp_treatmentproc_fk = treatmentproc.id 
                                    where treatmentproc.doctor is not null
                                        and accountflow.OPERATIONTIMESTAMP >= '$startDate 00:00:00'
                                        and accountflow.OPERATIONTIMESTAMP <= '$endDate 23:59:59'
                                        and accountflow.is_cashpayment = 0)  as factCash, "; //left
            
            $sql_proc_expenses .= "(select coalesce(-sum(accountflow.summ),0)
                                    from treatmentproc
                                        inner join accountflow
                                        on accountflow.texp_treatmentproc_fk = treatmentproc.id
                                    where treatmentproc.doctor is not null
                                        and accountflow.OPERATIONTIMESTAMP >= '$startDate 00:00:00'
                                        and accountflow.OPERATIONTIMESTAMP <= '$endDate 23:59:59'
                                        and accountflow.is_cashpayment = 1)  as factNonCash "; //
                                        
            $sql_proc_expenses .= 'from RDB$DATABASE';  
                                                
                                                
            $query_proc_expenses = ibase_prepare($trans, $sql_proc_expenses);
            $result_proc_expenses=ibase_execute($query_proc_expenses);
    		
                $row = ibase_fetch_row($result_proc_expenses);
                $obj = new ChiefBudgetModule_BudgetData;
                $obj->group_id = -3;
                $obj->group_name = 'proc_expenses';
                $obj->group_color = 204;//blue
                $obj->cash_expense = $row[0];
                
                $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
                $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
                $obj->summ = $obj->cash_summ + $obj->noncash_summ;
                
                $obj->income_summ = $obj->cash_income + $obj->noncash_income;
                $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
                
                
                $obj2 = new ChiefBudgetModule_BudgetData;
                $obj2->group_id = -3;
                $obj2->group_name = 'proc_expenses_fact';
                $obj2->group_color = 204;//blue
                $obj2->cash_expense = $row[1];
                $obj2->noncash_expense = $row[2];
                
                $obj2->cash_summ = $obj2->cash_income - $obj2->cash_expense;
                $obj2->noncash_summ = $obj2->noncash_income - $obj2->noncash_expense;
                $obj2->summ = $obj2->cash_summ + $obj2->noncash_summ;
                
                $obj2->income_summ = $obj2->cash_income + $obj2->noncash_income;
                $obj2->expense_summ = $obj2->cash_expense + $obj2->noncash_expense;
                
                
                
            ibase_free_query($query_proc_expenses);
            ibase_free_result($result_proc_expenses);
            
       if($dynamicExpenses == true) {
            $obj->included = $obj2;
            $rows[] = $obj;
       }
       else {
            $obj2->included = $obj;
            $rows[] = $obj2;
       }
  // treatmentproces expnese query END
  
  
  // -4 others query START
		$sql_others = "select

                                (select coalesce(sum(accountflow.summ),0)  from accountflow
                                        where accountflow.operationtype = 1
                                            and accountflow.is_cashpayment = 0
                                            and accountflow.summ > 0
                                            and accountflow.group_fk is null
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_income,
                                
                                (select coalesce(sum(accountflow.summ),0)  from accountflow
                                        where accountflow.operationtype = 1
                                            and accountflow.is_cashpayment = 1
                                            and accountflow.summ > 0
                                            and accountflow.group_fk is null
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_income,
                                
                                (select coalesce(-sum(accountflow.summ),0)  from accountflow
                                        where accountflow.operationtype = 1
                                            and accountflow.is_cashpayment = 0
                                            and accountflow.summ < 0
                                            and accountflow.group_fk is null
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expenses,
                                
                                (select coalesce(-sum(accountflow.summ),0)  from accountflow
                                        where accountflow.operationtype = 1
                                            and accountflow.is_cashpayment = 1
                                            and accountflow.summ < 0
                                            and accountflow.group_fk is null
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_expense ";
        $sql_others .= 'from RDB$DATABASE';  
        $query_others = ibase_prepare($trans, $sql_others);
        $result_others = ibase_execute($query_others);
            
            $row = ibase_fetch_row($result_others);
            $obj = new ChiefBudgetModule_BudgetData;
            $obj->group_id = -4;
            $obj->group_name = 'others';
            $obj->group_color = 6710886;//grey
            $obj->cash_income = $row[0];
            $obj->noncash_income = $row[1];
            $obj->cash_expense = $row[2];
            $obj->noncash_expense = $row[3];
            
            $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
            $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
            $obj->summ = $obj->cash_summ + $obj->noncash_summ;
            
            $obj->income_summ = $obj->cash_income + $obj->noncash_income;
            $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
            
            $rows[] = $obj;
            
        ibase_free_query($query_others);
        ibase_free_result($result_others);
  // others query END
  
   // -5 patients query START
		$sql_cancelDebt = "select

                                0.00 as cash_income,
                                
                                0.00 as noncash_income,
                                
                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                            where accountflow.operationtype = 5
                                                and accountflow.summ > 0
                                                and accountflow.is_cashpayment = 0
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expense,
                                
                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                            where accountflow.operationtype = 5
                                                and accountflow.summ > 0
                                                and accountflow.is_cashpayment = 1
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_expense ";
        $sql_cancelDebt .= 'from RDB$DATABASE';  
        
        $query_cancelDebt = ibase_prepare($trans, $sql_cancelDebt);
        $result_cancelDebt=ibase_execute($query_cancelDebt);
		
            $row = ibase_fetch_row($result_cancelDebt);
            $obj = new ChiefBudgetModule_BudgetData;
            $obj->group_id = -5;
            $obj->group_name = 'cancel';
            $obj->group_color = 3381504;//green
            $obj->cash_income = 0;
            $obj->noncash_income = 0;
            $obj->cash_expense = $row[2];
            $obj->noncash_expense = $row[3];
            
            $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
            $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
            $obj->summ = $obj->cash_summ + $obj->noncash_summ;
            
            $obj->income_summ = $obj->cash_income + $obj->noncash_income;
            $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
            if(($obj->cash_expense != 0 && $obj->cash_expense != null) || 
                ($obj->noncash_expense != 0 && $obj->noncash_expense != null))
            {
                $rows[] = $obj;
            }
            
        ibase_free_query($query_cancelDebt);
        ibase_free_result($result_cancelDebt);
  // patients query END
  
// groups query START
		$sql_groups = "select 
                               accountflow_groups.id,
                               accountflow_groups.name,
                               accountflow_groups.is_cashpayment,
                               accountflow_groups.monthly_is,
                               accountflow_groups.monthly_defaultsumm,
                               accountflow_groups.color
                        from accountflow_groups
                        order by accountflow_groups.gridsequence";  
        $query_groups = ibase_prepare($trans, $sql_groups);
        $result_groups = ibase_execute($query_groups);
        
        //$group_rows = array();
		while ($group_rows = ibase_fetch_row($result_groups))
		{ 
            $obj = new ChiefBudgetModule_BudgetData;
            $obj->group_id = $group_rows[0];
            $obj->group_name = $group_rows[1];
            $obj->group_is_cashpayment = $group_rows[2];
            $obj->group_monthly_is = $group_rows[3];
            $obj->group_monthly_defaultsumm = $group_rows[4];
            $obj->group_color = $group_rows[5];
            
            
                    $sql_group_summs = "select

                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                           inner join accountflow_groups
                                           on accountflow_groups.id = accountflow.group_fk
                                        where accountflow.operationtype = 1
                                            and accountflow.summ > 0
                                            and accountflow.is_cashpayment = 0
                                            and accountflow_groups.id =  $obj->group_id
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_income,
                                
                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                           inner join accountflow_groups
                                           on accountflow_groups.id = accountflow.group_fk
                                        where accountflow.operationtype = 1
                                            and accountflow.summ > 0
                                            and accountflow.is_cashpayment = 1
                                            and accountflow_groups.id =  $obj->group_id
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_income,
                                
                                (select coalesce(-sum(accountflow.summ),0) from accountflow
                                           inner join accountflow_groups
                                           on accountflow_groups.id = accountflow.group_fk
                                        where accountflow.operationtype = 1
                                            and accountflow.summ < 0
                                            and accountflow.is_cashpayment = 0
                                            and accountflow_groups.id = $obj->group_id
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expense,
                                
                                (select coalesce(-sum(accountflow.summ),0) from accountflow
                                           inner join accountflow_groups
                                           on accountflow_groups.id = accountflow.group_fk
                                        where accountflow.operationtype = 1
                                            and accountflow.summ < 0
                                            and accountflow.is_cashpayment = 1
                                            and accountflow_groups.id = $obj->group_id 
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_expense ";
                    $sql_group_summs .= 'from RDB$DATABASE';  
                    
                    $query_group_summs = ibase_prepare($trans, $sql_group_summs);
                    $result_group_summs=ibase_execute($query_group_summs);
            		
                    $row_group_summs = ibase_fetch_row($result_group_summs);
                    $obj->cash_income = $row_group_summs[0];
                    $obj->noncash_income = $row_group_summs[1];
                    $obj->cash_expense = $row_group_summs[2];
                    $obj->noncash_expense = $row_group_summs[3];
                    
                    $obj->cash_summ = $obj->cash_income - $obj->cash_expense;
                    $obj->noncash_summ = $obj->noncash_income - $obj->noncash_expense;
                    $obj->summ = $obj->cash_summ + $obj->noncash_summ;
            
                    $obj->income_summ = $obj->cash_income + $obj->noncash_income;
                    $obj->expense_summ = $obj->cash_expense + $obj->noncash_expense;
            
                    ibase_free_query($query_group_summs);
                    ibase_free_result($result_group_summs);
            
            
            $rows[] = $obj;
        }
        
        
        
        ibase_free_query($query_groups);
        ibase_free_result($result_groups);
        // groups query END
        return $rows;
    }
    
    /** 
    * @param string $startDate
    * @param string $endDate
    * @param boolean $considerDiscount
    * @param boolean $considerExpenses
    * 
    * @param boolean $payidInProceduresMode
    * @param float $payidInProceduresPrecents
    * @return number
    */  
    private function _getDoctorsDynamic($startDate, $endDate, $considerDiscount, $considerExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans)
    {
        //$connection = new Connection();
        //$connection->EstablishDBConnection();
		//$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        /////////////////// 1 SELECT DOCTORS START
        $sql_doctors = "select

                            doctors.id,
                            
                            doctors_salary_schema.enabledfromdate,
                            doctors_salary_schema.salaryschema
                        
                        from doctors
                        inner join doctors_salary_schema
                        on doctors_salary_schema.doctorid = doctors.id
                        
                        where (doctors.dateworkend >= '$startDate' or doctors.dateworkend is null)
                            and doctors_salary_schema.enabledfromdate <= '$endDate'
                        order by doctors.id asc, doctors_salary_schema.enabledfromdate desc";
        $query_doctors = ibase_prepare($trans, $sql_doctors);
        $result_doctors = ibase_execute($query_doctors);
        $doctors = array();
		while ($row = ibase_fetch_row($result_doctors))
		{ 
		  $doctor = new stdclass();//TODO
          $doctor->doctor_id = $row[0];
          $doctor->salary_fromDate = $row[1];
          $doctor->salary_xml = $row[2];
		  $doctors[] = $doctor;
		}	
        ibase_free_query($query_doctors);
        ibase_free_result($result_doctors);
        /////////////////// 1 SELECT DOCTORS END
        
        /////////////////// 1 SELECT TREATMENTPROCES START
        $loop_doctor_id = 'new';
        $loop_start_date = $startDate;
        $loop_end_date = $endDate;
        $date_start = new DateTime($startDate.' 00:00:00');
        $date_end = new DateTime($endDate.' 23:59:59');
        $summ = 0;
        //$loop_end_date = new DateTime($endDate.' 23:59:59');
        //$begin_date->format("D M j G:i:s")
        foreach ($doctors as $doctor)
        {
            $date_from = new DateTime($doctor->salary_fromDate);
            $date_from->setTime(0,0,0);
            
            if($loop_doctor_id != $doctor->doctor_id)
            {
               $loop_doctor_id = $doctor->doctor_id;
               $loop_end_date = $endDate;
            }
            
            if($date_from <= $date_start)
            {
                $loop_start_date = $startDate;
            }
            else
            {
                $loop_start_date = $date_from->format("Y-m-d");
            }
            
            
            
            $salary_xml = new SimpleXMLElement($doctor->salary_xml);
            foreach ($salary_xml->children() as $item)
            {
                if(($item['precent'])&&($item['type'] != -1))
                {
                    $summ += ( (float)$item['precent']/100 ) * 
                        $this->_getDoctorSummBytype($loop_start_date, $loop_end_date, $item['type'], $loop_doctor_id, $considerDiscount, $considerExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans);
                    //$item['type']
                    //(float) $item['precent'];
                }
    
                /*if($item['positionsalary'])
                {
                    $obj->doctorSalaryStabil = (float) $item['positionsalary'];
                }*/
            }
            
            
            $fromTime = strtotime($date_from->format("Y-m-d"));//-86400   
            $fromDate = date('Y-m-d', strtotime('-1 day', $fromTime));
            $loop_end_date = $fromDate;
        }
        /////////////////// 1 SELECT TREATMENTPROCES END
        
        //ibase_commit($trans);
        //$connection->CloseDBConnection();		
        return $summ; 
    }
    
    private function _getDoctorSummBytype($startDate, $endDate, $healthType, $doctorId, $considerDiscount, $considerExpenses, $byInvoiceDate, $payidInProceduresMode, $payidInProceduresPrecents, $trans)
    {
        if($considerDiscount == true)
        { 
        
            $sql_summ = "select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                                         coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0),
                                         coalesce(sum(treatmentproc.expenses * treatmentproc.proc_count),0)
                                    from treatmentproc ";
              
            if($byInvoiceDate == true)
            {
                $sql_summ .= "inner join invoices on invoices.id = treatmentproc.invoiceid
                                       
                                       where invoices.createdate >= '$startDate 00:00:00'
                                        and invoices.createdate <= '$endDate 23:59:59'
                                        and treatmentproc.healthtype = $healthType
                                        and treatmentproc.doctor = $doctorId
                                        and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 2)) "; //left
            }
            else
            {
                $sql_summ .= "left join invoices on invoices.id = treatmentproc.invoiceid
                
                                    where treatmentproc.dateclose >= '$startDate 00:00:00'
                                        and treatmentproc.dateclose <= '$endDate 23:59:59'
                                        and treatmentproc.healthtype = $healthType
                                        and treatmentproc.doctor = $doctorId
                                        and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 2)) "; //
            }
            
            if($payidInProceduresMode == true)
            {
                $sql_summ .= "and invoices.paidincash/(case when invoices.summ=0 then 1 else invoices.summ end) > $payidInProceduresPrecents"; 
            }
        
            
           /* $sql_summ = "select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                                coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0),
                                coalesce(sum(treatmentproc.expenses * treatmentproc.proc_count),0)
                            from treatmentproc
                            left join invoices
                            on invoices.id = treatmentproc.invoiceid
                            
                        where treatmentproc.dateclose >= '$startDate 00:00:00'
                            and treatmentproc.dateclose <= '$endDate 23:59:59'
                            and treatmentproc.healthtype = $healthType
                            and treatmentproc.doctor = $doctorId
                            and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 2))"; */
        }
        else
        {
           $sql_summ = "select coalesce( sum(treatmentproc.price * treatmentproc.proc_count), 0),
                               coalesce(sum(treatmentproc.expenses * treatmentproc.proc_count),0)
                            from treatmentproc  ";
              
            if($byInvoiceDate == true)
            {
                $sql_summ .= "inner join invoices on invoices.id = treatmentproc.invoiceid
                                       
                                       where invoices.createdate >= '$startDate 00:00:00'
                                        and invoices.createdate <= '$endDate 23:59:59'
                                        and treatmentproc.healthtype = $healthType
                                        and treatmentproc.doctor = $doctorId
                                        and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 2)) "; //left
            }
            else
            {
                $sql_summ .= "left join invoices on invoices.id = treatmentproc.invoiceid
                
                                    where treatmentproc.dateclose >= '$startDate 00:00:00'
                                        and treatmentproc.dateclose <= '$endDate 23:59:59'
                                        and treatmentproc.healthtype = $healthType
                                        and treatmentproc.doctor = $doctorId
                                        and ((treatmentproc.salary_settings = 1) or (treatmentproc.salary_settings = 2)) "; //
            }
            
            if($payidInProceduresMode == true)
            {
                $sql_summ .= "and invoices.paidincash/(case when invoices.summ=0 then 1 else invoices.summ end) > $payidInProceduresPrecents"; 
            }
        }
        
                            
                            /*
                select coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count as decimal(15,2))),0) -
                    coalesce(sum(cast(treatmentproc.price*treatmentproc.proc_count*(treatmentproc.discount_flag*invoices.discount_amount/100.00) as decimal(15,2))),0)
                            from treatmentproc
                            left join invoices
                            on invoices.id = treatmentproc.invoiceid
                            
                        where treatmentproc.dateclose >= '2012-10-01 00:00:00'
                            and treatmentproc.dateclose < '2012-10-31 00:00:00'
                            */
        $query_summ = ibase_prepare($trans, $sql_summ);
        $result_summ = ibase_execute($query_summ);
        
		$summ = ibase_fetch_row($result_summ);
			
        ibase_free_query($query_summ);
        ibase_free_result($result_summ);
        if($considerExpenses == true)
        {
            return $summ[0]-$summ[1];
        }
        else
        {
            return $summ[0];
        }
    }
    
}
?>
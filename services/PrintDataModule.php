<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "ServerFileModule.php";
require_once (dirname(__FILE__).'/thumbnail/ThumbLib.inc.php');  
class PrintDataModule
{	
    public static function getDicomServerFilesArray($patientId, $dateString, $trans=null)
    {
        $res = array();
        if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        $dateString = str_replace('-', '', $dateString);
        $dateString = str_replace('.', '', $dateString);
        $dateString = str_replace("'", '', $dateString);
        
	    $sql = "select
                                 sf.name,
                                 sf.extention,
                                 sf.servername
                                 
                                 from serverfile sf
                        where sf.name containing ('".$dateString."') 
                        and 
                        sf.type = -333
                        and
                        sf.ISTHUMBNAILABLE = 1
                        and
                        sf.PATIENTID = ".$patientId;	
			
		
			//	file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($sql,true));
		
	    $query = ibase_prepare($trans, $sql);
	    $result = ibase_execute($query);
        $fileIndex = 1;
        $sfAdd = false;
        while ($row = ibase_fetch_row($result))
        {
            if ($fileIndex % 2 == 0) 
            {
                $sfAdd = false;
                $sf->imagePathB = dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png';
                $res[] = $sf;  
            }
            else
            {
                $sfAdd = true;
                $sf = new stdclass;
                $sf->imagePathA = dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png';
                $sf->imagePathB = '';
            }
            $sf->INDEX = $fileIndex;
            $sf->name=$row[0];
            $sf->extention=$row[1];
            $sf->serverName=$row[2];
                
            $sf->imagePath = dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png';
            
            copy(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName, dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png');
                                   
            $fileIndex++;
            
        }
        if($sfAdd)
        {
            $res[] = $sf;  
        }
            
	    ibase_free_query($query);
	    ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        
        return $res;
    }
    
    public static function getColposcopeServerFilesArray($patientId, $dateString, $trans=null)
    {
        $res = array();
        if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        //$dateString = str_replace('-', '', $dateString);
        //$dateString = str_replace('.', '', $dateString);
        //$dateString = str_replace("'", '', $dateString);
        
	    $sql = "select
                                 sf.name,
                                 sf.extention,
                                 sf.servername
                                 
                                 from serverfile sf
                        where 
                        (sf.createtimestamp >= '$dateString 00:00:00' and sf.createtimestamp <= '$dateString 23:59:59')
                        and 
                        sf.type = -3
                        and
                        sf.ISTHUMBNAILABLE = 1
                        and
                        sf.PATIENTID = ".$patientId;	
			
		
			//	file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($sql,true));
		
	    $query = ibase_prepare($trans, $sql);
	    $result = ibase_execute($query);
        $fileIndex = 1;
        $sfAdd = false;
        while ($row = ibase_fetch_row($result))
        {
            if ($fileIndex % 2 == 0) 
            {
                $sfAdd = false;
                $sf->imagePathB = dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png';
                $res[] = $sf;  
            }
            else
            {
                $sfAdd = true;
                $sf = new stdclass;
                $sf->imagePathA = dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png';
                $sf->imagePathB = '';
            }
            $sf->INDEX = $fileIndex;
            $sf->name=$row[0];
            $sf->extention=$row[1];
            $sf->serverName=$row[2];
                
            $sf->imagePath = dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png';
            
            copy(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName, dirname(__FILE__).'/docs/dicom/'.$fileIndex.'.png');
                                   
            $fileIndex++;
            
        }
        if($sfAdd)
        {
            $res[] = $sf;  
        }
            
	    ibase_free_query($query);
	    ibase_free_result($result);
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        
        return $res;
    }
    
}
class PrintDataModule_PatientData
{
    /**
    * @var string
    */
    var $CARDNUMBER;
    
	/**
    * @var string
    */
    var $CARDDATE;
    
	/**
    * @var string
    */
    var $FIRSTNAME;
    
	/**
    * @var string
    */
    var $MIDDLENAME;
    
	/**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $SHORTNAME;
                    
	/**
    * @var string
    */
    var $BIRTHDAY;
    
   	/**
    * @var string
    */
    var $BIRTHDAYDATE;
    
	/**
    * @var string
    */
    var $SEX;
    
	/**
    * @var string
    */
    var $POSTALCODE;
    
	/**
    * @var string
    */
    var $COUNTRY;
    
	/**
    * @var string
    */
    var $STATE;
    
	/**
    * @var string
    */
    var $CITY;
    
	/**
    * @var string
    */
    var $REGION;
    
	/**
    * @var string
    */
    var $ADDRESS;
    
	/**
    * @var string
    */
    var $TELEPHONENUMBER;
    
	/**
    * @var string
    */
    var $MOBILENUMBER;
    
	/**
    * @var string
    */
    var $EMPLOYMENTPLACE;
    
	/**
    * @var string
    */
    var $JOB;
    
	/**
    * @var string
    */
    var $WORKPHONE;
    
    	/**
    * @var string
    */
    var $EMAIL;
   	    
    	/**
    * @var string
    */
    var $AGE;
        
    	/**
    * @var string
    */
    var $POPULATIONGROUP;
        
    	/**
    * @var string
    */
    var $VILLAGEORCITY;
        
    /**
    * @var string
    */
    var $PASSPORT_SERIES;
    
    /**
    * @var string
    */
    var $PASSPORT_NUMBER;
    
    /**
    * @var string
    */
    var $PASSPORT_ISSUING;
    
    /**
    * @var string
    */
    var $PASSPORT_DATE;
    
    /**
    * @var array
    */
    var $BIRTHDAY_ARRAY;
    
       /**
    * @var array
    */
    var $BIRTHDAYFULL_ARRAY; 
    
    /**
    * @var string
    */
    var $ADDRESS_FULL;
    
    /**
    * @var array
    */
    var $ADDRESS_FULL_ARRAY;
    
    function __construct($patient_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	$QueryText = "
            select 
				CARDNUMBER,
				CARDBEFORNUMBER,
				CARDAFTERNUMBER,
				CARDDATE,
				FIRSTNAME,
				MIDDLENAME,
				LASTNAME,
                SHORTNAME,                
				BIRTHDAY,
				SEX,
				POSTALCODE,
				COUNTRY,
				STATE,
				CITY,
				RAGION,
				ADDRESS,
				TELEPHONENUMBER,
				EMPLOYMENTPLACE,
				JOB,
				WORKPHONE,
                EMAIL,
                POPULATIONGROUPE,
                VILLAGEORCITY,
                PASSPORT_SERIES,
                PASSPORT_NUMBER,
                PASSPORT_ISSUING,
                PASSPORT_DATE
            from PATIENTS 
			where ID =$patient_id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{				
                $this->CARDNUMBER = "{$row[1]}{$row[0]}{$row[2]}";
				$this->CARDDATE = $row[3]!=null?date("d.m.Y", strtotime($row[3])):"";
				$this->FIRSTNAME = $row[4];
				$this->MIDDLENAME = $row[5];
				$this->LASTNAME = $row[6];
                $this->SHORTNAME = $row[7];
               	$this->BIRTHDAYDATE = $row[8];                
				$this->BIRTHDAY = $row[8]!=null?date("d.m.Y", strtotime($row[8])):"";
                $this->BIRTHDAY_ARRAY = str_split($row[8]!=null?date("dmy", strtotime($row[8])):"");
                $this->BIRTHDAYFULL_ARRAY = str_split($row[8]!=null?date("dmY", strtotime($row[8])):"");
				$this->SEX = $row[9]+1;
				$this->POSTALCODE = $row[10]==''?'':''.$row[10];
				$this->COUNTRY = $row[11]==''?'':$row[11];
				$this->STATE = $row[12]==''?'':$row[12].' '.tflocale::$state;
				$this->CITY = $row[13]==''?'':$row[13];
				$this->REGION = $row[14]==''?'':$row[14].' '.tflocale::$region;
				$this->ADDRESS = $row[15]==''?'':$row[15];
				$this->TELEPHONENUMBER = $row[16];
				$this->EMPLOYMENTPLACE = $row[17];
				$this->JOB = $row[18];
				$this->WORKPHONE = $row[19];  
                $this->EMAIL = $row[20];
                $this->POPULATIONGROUP = getPopulationGroup($row[21]);
                $this->VILLAGEORCITY = $row[22]==0?'с':'м'; 
                $this->PASSPORT_SERIES = $row[23];
                $this->PASSPORT_NUMBER = $row[24];
                $this->PASSPORT_ISSUING = $row[25];
                $this->PASSPORT_DATE = $row[26]!=null?date("d.m.Y", strtotime($row[26])):"";       
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);

            $this->MOBILENUMBER=$this->getMobileNumbersString($patient_id, $trans);
            
            $this->ADDRESS_FULL=$this->ADDRESS.($this->ADDRESS==''?'':', ').
                                $this->CITY.($this->CITY==''?'':', ').
                                $this->REGION.($this->REGION==''?'':', ').
                                $this->STATE.($this->STATE==''?'':', ').
                                $this->COUNTRY.($this->COUNTRY==''?'':($this->MOBILENUMBER==''?'':', ')).
                                $this->MOBILENUMBER;            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
    
    /**
     * PrintDataModule_PatientData::getMobileNumbersString()
     * строка с номерами телефонов пациента
     * 
     * @param int $patient_id
     * @param resource $trans
     * @return string
     */
    public static function getMobileNumbersString($patient_id, $trans=null) 
    {
            if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }    
               
            $QueryText = "select MOBILENUMBER from PATIENTSMOBILE where PATIENT_FK =$patient_id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$mobilenumber='';
			while ($row = ibase_fetch_row($result))
			{				
				$mobilenumber.=nonull($row[0])?(($mobilenumber==''?'т.:+':', ').$row[0]):'';
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $mobilenumber;
    }
    
    public function generateAddressFullArray($len, $lenstart)
    {
        $this->ADDRESS_FULL_ARRAY=space_split($this->ADDRESS_FULL, $len, $lenstart);
    }
}

class PrintDataModule_DoctorData
{    
	/**
    * @var string
    */
    var $FIRSTNAME;
    
	/**
    * @var string
    */
    var $MIDDLENAME;
    
	/**
    * @var string
    */
    var $LASTNAME;
    
    /**
    * @var string
    */
    var $SHORTNAME;
    
    
    function __construct($doctor_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	$QueryText = "
            select 
				FIRSTNAME,
				MIDDLENAME,
				LASTNAME,
                SHORTNAME               
            from DOCTORS 
			where ID =$doctor_id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{				
				$this->FIRSTNAME = $row[0];
				$this->MIDDLENAME = $row[1];
				$this->LASTNAME = $row[2];
                $this->SHORTNAME = $row[3];
			}			
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
    
}

class PrintDataModule_ClinicRegistrationData
{
    /**
    * @var string
    */
    var $NAME;
    
    /**
    * @var string
    */
    var $LEGALNAME;	  						
    
    /**
    * @var string
    */
    var $ADDRESS;
    
    /**
    * @var string
    */
    var $ORGANIZATIONTYPE;
    
    /**
    * @var string
    */
    var $CODE;
    
    /**
    * @var string
    */
    var $DIRECTOR;
    
    /**
    * @var string
    */
    var $TELEPHONENUMBER;
    
    /**
    * @var string
    */
    var $FAX;
    
    /**
    * @var string
    */
    var $SITENAME;
    
    /**
    * @var string
    */
    var $EMAIL;
    
    /**
    * @var string
    */
    var $ICQ;
    
    /**
    * @var string
    */
    var $SKYPE;
    
    /**
    * @var string
    */
    var $CHIEFACCOUNTANT;
    
    /**
    * @var string
    */
    var $LEGALADDRESS;
    
    /**
    * @var string
    */
    var $BANK_NAME;
    
    /**
    * @var string
    */
    var $BANK_MFO;
    
    /**
    * @var string
    */
    var $BANK_CURRENTACCOUNT;
    
    /**
    * @var string
    */
    var $LICENSE_NUMBER;
    
    /**
    * @var string
    */
    var $LICENSE_SERIES;
    
    /**
    * @var string
    */
    var $LICENSE_STARTDATE;
    
    /**
    * @var string
    */
    var $LICENSE_ENDDATE;
    
    /**
    * @var string
    */
    var $LICENSE_ISSUED; 
          
     /**
    * @var string
    */
    var $SHORT_DATE;
       
    /**
    * @var object
    */
    var $LOGO;
    
    /**
    * @var string
    */
    var $AUTHORITY;
    
    /**
    * @var string
    */
    var $AUTHORITY2;
    
    /**
    * @var array
    */
    var $CODE_ARRAY;
    
    function __construct($writeLogoToDisk=false, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }    
        
        	$QueryText = "
			select 
                NAME,
				LEGALNAME, 				
				ADDRESS,
                ORGANIZATIONTYPE,
                CODE,
                DIRECTOR,
                TELEPHONENUMBER,
                FAX,
                SITENAME,
                EMAIL,
                ICQ,
                SKYPE,
                CHIEFACCOUNTANT,
                LEGALADDRESS,
                BANK_NAME,
                BANK_MFO,
                BANK_CURRENTACCOUNT,
                LICENSE_NUMBER,
                LICENSE_SERIES,
                LICENSE_STARTDATE,
                LICENSE_ENDDATE,
                LICENSE_ISSUED,
                LOGO,
                AUTHORITY
			from CLINICREGISTRATIONDATA";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			if ($row = ibase_fetch_row($result))
			{
                $this->NAME = $row[0];
                $this->LEGALNAME = $row[1];               				
				$this->ADDRESS = $row[2];
                $this->ORGANIZATIONTYPE = $row[3];
                $this->CODE = $row[4];
                $this->DIRECTOR = $row[5];
                $this->TELEPHONENUMBER = $row[6];
                $this->FAX = $row[7];
                $this->SITENAME = $row[8];
                $this->EMAIL = $row[9];
                $this->ICQ = $row[10];
                $this->SKYPE = $row[11];
                $this->CHIEFACCOUNTANT = $row[12];
                $this->LEGALADDRESS = $row[13];
                $this->BANK_NAME = $row[14];
                $this->BANK_MFO = $row[15];
                $this->BANK_CURRENTACCOUNT = $row[16];
                $this->LICENSE_NUMBER = $row[17];
                $this->LICENSE_SERIES = $row[18];
                $this->LICENSE_STARTDATE = $row[19];
                $this->LICENSE_ENDDATE = $row[20];
                $this->LICENSE_ISSUED = $row[21];
                
                $this->SHORT_DATE = date ('d/m/Y', time());
                
                $this->LOGO = blob_encode($row[22]);
                
                $this->LOGO_SRC = '';
                $this->LOGO_PATH = dirname(__FILE__).'/tbs/res/void.png';
                if($writeLogoToDisk && $this->LOGO != null ){
                    file_put_contents(dirname(__FILE__).'/docs/logo.bmp', base64_decode($this->LOGO) );
                    $this->LOGO_SRC = '<img src="'.dirname(__FILE__).'/docs/logo.bmp">';
                    $this->LOGO_PATH = dirname(__FILE__).'/docs/logo.bmp';
                }
                
                if(strstr($row[23],"\\n"))
                {
                    $authority_arr = explode("\\n",$row[23]);
                    $this->AUTHORITY = $authority_arr[0];	
                    $this->AUTHORITY2 = $authority_arr[1];
                }
                else 
                {
                    $this->AUTHORITY = $row[23];
                }
			}
			ibase_free_query($query);
			ibase_free_result($result);
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            $this->CODE_ARRAY=str_split($this->CODE,1);
    }
    

                
}


class PrintDataModule_TEST
{
    /**
    * @var string
    */
    var $GROUP_BLOOD;
    
	/**
    * @var string
    */
    var $REZUS_FACTOR;
    
	/**
    * @var string
    */
    var $PERELIV_BLOOD;
    
	/**
    * @var string
    */
    var $SUGAR_DIABET;
    
	/**
    * @var string
    */
    var $INFECTION_ILL;
    
    /**
    * @var string
    */
    var $HIRURG;
                    
	/**
    * @var string
    */
    var $ALERGIOCH;
    
   	/**
    * @var string
    */
    var $NEPERENOSIMOST;
	
	
	
	/**
    * @var string
    */
    var $q_9;   //q_9 -> question 9
    
	/**
    * @var string
    */
    var $q_10;      

	/**
    * @var string
    */
    var $q_11;
	
	/**
    * @var string
    */
    var $q_12;
	
	/**
    * @var string
    */
    var $q_14;
 
	/**
    * @var string
    */
    var $q_15; 

	/**
    * @var string
    */
    var $q_16;
	
	/**
    * @var string
    */
    var $q_17;
	
	/**
    * @var string
    */
    var $q_18;

	/**
    * @var string
    */
    var $q_19;
	
	/**
    * @var string
    */
    var $q_20;
	
	/**
    * @var string
    */
    var $q_21;
	
	/**
    * @var string
    */
    var $q_22;
	
	/**
    * @var string
    */
    var $q_23;
	
	/**
    * @var string
    */
    var $q_24;
	
	/**
    * @var string
    */
    var $q_25;
	
	/**
    * @var string
    */
    var $q_26;
	
	/**
    * @var string
    */
    var $q_27;
	
	/**
    * @var string
    */
    var $q_28;
	
	/**
    * @var string
    */
    var $q_29;
		/**
    * @var string
    */
    var $q_30;
			/**
    * @var string
    */
    var $q_31;
			/**
    * @var string
    */
    var $q_32;
			/**
    * @var string
    */
    var $q_33;
			/**
    * @var string
    */
    var $q_34;
				/**
    * @var string
    */
	var $DOCTOR;
	
    function __construct($patient_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	$QueryText = "
            SELECT  PATIENTSTESTCARDS.TESTRESULT,
					DOCTORS.SHORTNAME
			FROM PATIENTSTESTCARDS
					left join TOOTHEXAM
                   on TOOTHEXAM.ID = $patient_id
                   left join DOCTORS
                   on DOCTORS.ID =  TOOTHEXAM.DOCTORID			
			where PATIENTSTESTCARDS.PATIENTID = $patient_id 
			order by PATIENTSTESTCARDS.ID DESC, TOOTHEXAM.EXAMID desc";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);

			if ($row = ibase_fetch_row($result))
			{
			$this->DOCTOR = $row[1];
			if ($row[0]!=null)
			{
				$str=(nonull($row[0]))?text_blob_encode($row[0]):'';
				$test = new SimpleXMLElement($str);

				$this->GROUP_BLOOD = $test->results[0]->result;
				$this->REZUS_FACTOR = $test->results[1]->result;
				$this->PERELIV_BLOOD = $test->results[2]->result;
				$this->SUGAR_DIABET = $test->results[3]->result;
				$this->INFECTION_ILL = $test->results[4]->result;
                $this->HIRURG = $test->results[5]->result;
               	$this->ALERGIOCH = $test->results[6]->result;                
				$this->NEPERENOSIMOST = $test->results[7]->result;
				
				$this->q_9 = $test->results[8]->result;
				$this->q_10 = $test->results[9]->result;
				$this->q_11 = $test->results[10]->result;		
				$this->q_12 = $test->results[11]->result;
				$this->q_14 = $test->results[12]->result;
				$this->q_15 = $test->results[13]->result;
				$this->q_16 = $test->results[14]->result;
				$this->q_17 = $test->results[15]->result;
				$this->q_18 = $test->results[16]->result;
				$this->q_19 = $test->results[17]->result;
				$this->q_20 = $test->results[18]->result;
				$this->q_21 = $test->results[19]->result;
				$this->q_22 = $test->results[20]->result;
				$this->q_23 = $test->results[21]->result;
				$this->q_24 = $test->results[22]->result;
				$this->q_25 = $test->results[23]->result;
				$this->q_26 = $test->results[24]->result;
				$this->q_27 = $test->results[25]->result;
				$this->q_28 = $test->results[26]->result;
				$this->q_29 = $test->results[27]->result;
				$this->q_30 = $test->results[28]->result;
				$this->q_31 = $test->results[29]->result;
				$this->q_32 = $test->results[30]->result;
				$this->q_33 = $test->results[31]->result;
				$this->q_34 = $test->results[32]->result;
			}
            }			
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
}


	function PrintDataModule_DIAGNOSIS($patient_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	$QueryText = "SELECT EXAMDATE, NH_DIAGNOS 
						  from TOOTHEXAM WHERE ID=$patient_id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);

			while ($row = ibase_fetch_row($result))
			{
				$Diagnosis = new stdclass();
				$Diagnosis->DATA=$row[0];
				$Diagnosis->NAME=$row[1];
				$DiagnosisArray[]=$Diagnosis;
				$Diagnosis=null;
            }		

			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

				$Diagnosis=new stdclass();
				$DiagnosisArray[]=$Diagnosis;	
			
			return $DiagnosisArray;
    }
    
    
   	function PrintDataModule_DIARY($patient_id, $trans=null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	$QueryText = "select
                              DAIRYTYPE+1,
                              DAIRYTEXT,
                              DAIRYRESUME,
                              (select doctors.shortname from doctors where doctors.id = nh_dairy.fk_doctor),
                              DAIRYDATE

                          from NH_DAIRY

                          where nh_dairy.fk_patient = $patient_id
                          order by nh_dairy.createdate desc";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);

			while ($row = ibase_fetch_row($result))
			{
				$Diary = new stdclass();
				$Diary->TYPE=$row[0];
				$Diary->TEXT=text_blob_encode($row[1]);
				$Diary->RESUME=text_blob_encode($row[2]);
				$Diary->DOCTOR=$row[3];
				$Diary->DATE=$row[4];
				$DiaryArray[]=$Diary;
				$Diary=null;
            }		

			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

				$Diary=new stdclass();
				$DiaryArray[]=$Diary;	
			
			return $DiaryArray;
    }


class PrintDataModule_DateUA
{
    /**
    * @var string
    */
    var $DATE;
    
	/**
    * @var string
    */
    var $DAY;
         
	/**
    * @var string
    */
    var $MONTH;
        
	/**
    * @var string
    */
    var $YEAR;
    
    function __construct($date) 
    {
        $time=strtotime($date);
        $this->DATE = ua_date('"d" M Y', $time );
        $this->DAY = ua_date("d", $time );
        $this->MONTH = ua_date("M", $time );
        $this->YEAR = ua_date("y", $time );
    }
}
 
    function getTreatmentObjects($diagnos_id, $trans=null)
	{	
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
            $QueryText = "
			select 			
				TEETH
			from DIAGNOS2
			where ID = $diagnos_id";
			
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			
			$rows = array();
			$teeth=null;
			if ($row = ibase_fetch_row($result))
			{
				$teeth=$row[0];
			}
            ibase_free_query($query);
			ibase_free_result($result);
                
            if($teeth==null)
            {
                    
    			$QueryText = "
    			select 			
    				TREATMENTOBJECTS.TOOTH,
    				TREATMENTOBJECTS.TOOTHSIDE
    			from TREATMENTOBJECTS
    			where TREATMENTOBJECTS.DIAGNOS2 = $diagnos_id order by TREATMENTOBJECTS.TOOTH";
    			
    			$query = ibase_prepare($trans, $QueryText);
    			$result = ibase_execute($query);
    			
    
    			
    			while ($row = ibase_fetch_row($result))
    			{
    				$obj = new stdclass();
    				
    				$obj->TOOTH = $row[0];
    				$obj->TOOTHSIDE = $row[1];
    				
    				$rows[] = $obj;
    			}
                
                ibase_free_query($query);
    			ibase_free_result($result);
            }
            else
            {
                $arr=explode(',',$teeth);
                sort($arr);
                foreach($arr as $el)
                {
                    $obj = new stdclass();
    				
    				$obj->TOOTH = $el;
    				$obj->TOOTHSIDE = 'Z';
    				
    				$rows[] = $obj;
                }
            }
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
        
            return sort_tooth($rows);

	}
    
    function sort_tooth($rows)
    {
                    for($i=1;$i<count($rows);$i++)
                    {          
                        if(abs($rows[$i]->TOOTH- substr($rows[$i-1]->TOOTH,strlen($rows[$i-1]->TOOTH)-2,2))==1
                                                                &&
                        substr($rows[$i]->TOOTHSIDE,strlen($rows[$i]->TOOTHSIDE)-1,1)==substr($rows[$i-1]->TOOTHSIDE,strlen($rows[$i-1]->TOOTHSIDE)-1,1))
                        {
                            $rows[$i-1]->TOOTH=substr($rows[$i-1]->TOOTH,0,2).'-'.$rows[$i]->TOOTH;
                            unset($rows[$i]);
                            sort($rows);
                            $i--;
                        }
                    }
            
			for($i=1;$i<count($rows);$i++)
                    {   
                        if((($rows[$i-1]->TOOTH==='11-18'&&$rows[$i]->TOOTH==='21-28')
                            ||($rows[$i-1]->TOOTH==='51-55'&&$rows[$i]->TOOTH==='61-65'))
                            && substr($rows[$i]->TOOTHSIDE,strlen($rows[$i]->TOOTHSIDE)-1,1)==substr($rows[$i-1]->TOOTHSIDE,strlen($rows[$i-1]->TOOTHSIDE)-1,1))
                        {                                            
                            $rows[$i-1]->TOOTH=tflocale::$upJaw;
                            unset($rows[$i]);
                            sort($rows);
                            $i--;   
                        } 
                        else if((($rows[$i-1]->TOOTH==='31-38'&&$rows[$i]->TOOTH==='41-48')
                            ||($rows[$i-1]->TOOTH==='71-75'&&$rows[$i]->TOOTH==='81-85'))
                            && substr($rows[$i]->TOOTHSIDE,strlen($rows[$i]->TOOTHSIDE)-1,1)==substr($rows[$i-1]->TOOTHSIDE,strlen($rows[$i-1]->TOOTHSIDE)-1,1))
                        {
                            $rows[$i-1]->TOOTH=tflocale::$downJaw;
                            unset($rows[$i]);
                            sort($rows);
                            $i--;
                        }
                    }
                    
                    $allToothStr='';
                    foreach($rows as $row)
                    {
                        if($allToothStr!='')
                        {
                           $allToothStr.= ', ';
                        }
                        if(strstr($row->TOOTH,tflocale::$jaw)==false)
                        {
                            $allToothStr.= tflocale::$toothSmall;
                            if(strstr($row->TOOTH,'-')==false)
                            {
                                $allToothStr.= ' ';
                            }
                            else
                            {
                                $allToothStr.= tflocale::$toothMulti.' ';
                            }
                        }
                        
                        $allToothStr.=$row->TOOTH.' '.tooth_case('('.$row->TOOTHSIDE.')');
                    }
             
                   
			return $allToothStr;
    }
   function tooth_case($toothMatch)
    {
        switch($toothMatch)
        {
            case "(D1)": 
            case "(D2)":
                $toothMatch='('.tflocale::$gum.')';
                break; 
             
            case "(R)": 
            case "(MR)": 
            case "(CR)": 
            case "(DR)":
                $toothMatch='('.tflocale::$root.')';
                break;  
            
            case "(Z)":
                $toothMatch='';
                break;  
        }
        return $toothMatch;
    } 
    
    function getSortTeethF43($exam, $bitetype, $teethidset, $teetdisishset)
    {
          $diag_43_array=array();
        $xml = simplexml_load_string('<xml>'.$exam.'</xml>');    
        for($i=0; $i<32; $i++ )
        {
            $gp = $i>15 ? "GP2" : "GP1";
            
            
            $id=$bitetype==0?$teethidset[$i]:$teethidset[$i]+40;
            //-------------------------------------------------------------
            
            if( $bitetype==0 || ($id%10)<6 )
            {
                $j=1;
                
                foreach($teetdisishset as $dd)
                {
                    if(strstr($exam,$gp.'>'.$j.'<'))
                    {
                          if($dd!='') { $diag_43_array[] =array("ID"=>$id, "DIAG"=>$dd); }
                    } 
                    $j++;
                }
            }
        
            
            if(strstr(strval($exam),'id="'.$id.'"'))
            {
                  
                $tmp=($xml->xpath("/xml/zub[@id='".$id."']"));
                $k=1;
                foreach($teetdisishset as $dd)
                {
                    
                    if(strstr($tmp[0]->asXML(),'>'.$k.'<'))
                    {
                          if($dd!='') { $diag_43_array[] =array("ID"=>$id, "DIAG"=>$dd); }
                    } 
                    $k++;
                }
                
            }
            
        }
        
        usort($diag_43_array, 'cmp');
        
        // echo("diag array: ");
        // print_r($diag_43_array);
        // echo("xml: ". $exam);
        
        for($i=1;$i<count($diag_43_array);$i++)
        {
            if(abs($diag_43_array[$i]["ID"]- substr($diag_43_array[$i-1]["ID"],strlen($diag_43_array[$i-1]["ID"])-2,2))==1
                                                    &&
            $diag_43_array[$i]["DIAG"]==$diag_43_array[$i-1]["DIAG"])
            {
                $diag_43_array[$i-1]["ID"]=substr($diag_43_array[$i-1]["ID"],0,2).'-'.$diag_43_array[$i]["ID"];
                unset($diag_43_array[$i]);
                //sort($diag_43_array);
                usort($diag_43_array, 'cmp');
                $i--;
            }
        }
        
  
            
		for($i=1;$i<count($diag_43_array);$i++)
        {
            if((($diag_43_array[$i-1]["ID"]==='11-18'&&$diag_43_array[$i]["ID"]==='21-28')
                ||($diag_43_array[$i-1]["ID"]==='51-55'&&$diag_43_array[$i]["ID"]==='61-65'))
                && $diag_43_array[$i]["DIAG"]==$diag_43_array[$i-1]["DIAG"])
            {                                            
                $diag_43_array[$i-1]["ID"]=tflocale::$upJaw;
                unset($diag_43_array[$i]);
                //sort($diag_43_array);
                usort($diag_43_array, 'cmp');
                $i--;   
            } 
            else if((($diag_43_array[$i-1]["ID"]==='31-38'&&$diag_43_array[$i]["ID"]==='41-48')
                ||($diag_43_array[$i-1]["ID"]==='71-75'&&$diag_43_array[$i]["ID"]==='81-85'))
                && $diag_43_array[$i]["DIAG"]==$diag_43_array[$i-1]["DIAG"])
            {
                $diag_43_array[$i-1]["ID"]=tflocale::$downJaw;
                unset($diag_43_array[$i]);
                //sort($diag_43_array);
                usort($diag_43_array, 'cmp');
                $i--;
            }
        }
                    
        for($i=1;$i<count($diag_43_array);$i++)
        {
            if( $diag_43_array[$i]["DIAG"]==$diag_43_array[$i-1]["DIAG"])
            {
                $diag_43_array[$i-1]["ID"]=$diag_43_array[$i-1]["ID"].','.$diag_43_array[$i]["ID"];
                unset($diag_43_array[$i]);
                //sort($diag_43_array);
                usort($diag_43_array, 'cmp');
                $i--;
            }
        }
        usort($diag_43_array, 'cmp2');
        return($diag_43_array);
    }
    
    /**
     * @param string $fieldValue  
     * @param string $tableName                   
     * @param resource $trans
     * @param string $fieldName
     * @return boolean
     */
     
    function deleteRecordByField($fieldValue, $tableName, $trans=null, $fieldName="ID")
	{
	   $res=true;
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

			$QueryText = "delete from $tableName where $fieldName=$fieldValue";
            
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			ibase_free_query($query);

            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
        /**
     * @param string $paramName  
     * @param string $paramValue  
     * @return boolean
     */
    function setApplicationSetting($paramName, $paramValue, $trans=null)
	{
        $res='';
	    if($trans==null)
        {
            $connection = new Connection();
            if($paramName=='AUTOWRITEOFF')
            {
      		    $connection->EstablishDBConnection("main",false);
            }
            else
            {
                $connection->EstablishDBConnection();
            }
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
            $QueryText="update or insert into applicationsettings(parametername, parametervalue) values('$paramName','".safequery($paramValue)."') matching(parametername)";            
			
            $query = ibase_prepare($trans, $QueryText);
		      
            ibase_execute($query);          
            ibase_free_query($query);
            
            $res=true;
            if($local)
            { 
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
    /**
     * @param string $paramName  
     * @return string
     */
    function getApplicationSetting($paramName, $trans=null)
	{
        $res='';
	    if($trans==null)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }

			$QueryText = "select ap.parametervalue from applicationsettings ap where ap.parametername='$paramName'";
            
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
		
			if($row = ibase_fetch_row($result))
			{
				$res=$row[0];
			}
            
            ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            { 
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $res;
	} 
    
    function getPopulationGroup($val)
    {
        $res='';
        if($val>9)
        {
            $res.="Д";
            $val-=10;
        }
        $groups=array('ДГ', 'Ш', 'С', 'В', 'Р', 'ДПК');
        if($val<count($groups))
        {
            $res=$res.$groups[$val];
        }
        return $res;
    }
    
   	function addServerFile($patientId, $path, $trans=null) 
	{
	   $res=true;
       
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
            
                $sf=new ServerFileModule_ServerFile;
               $sf->name=basename($path);
               $sf->extention=pathinfo($path, PATHINFO_EXTENSION);
               $sf->serverName=gen_uuid();
               $isThumbnailable=true;
                chmod($path,0777);
                try 
                {
                    $thumb = PhpThumbFactory::create($path);  
                } 
                catch (Exception $e) 
                {
                    $isThumbnailable=false;
                } 
                if($isThumbnailable) 
                {
                    $thumb->resize(160, 160)->save(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb');
                    chmod(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName.'_thumb',0777);
                }
               $sf->patientId=$patientId;
               $sf->isThumbnailable=$isThumbnailable;
               $sf->comment='';
               
                copy($path, dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName); 
                chmod(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/'.$sf->serverName,0777);
            		
                    $QueryText = "insert into serverfile (
                                                            id,
                                                            name,
                                                            extention,
                                                            servername,
                                                            comment,
                                                            isthumbnailable,
                                                            patientid,
                                                            tooth,
                                                            protocolid)
                                                          values (
                                                          null,
                                                          '".$sf->name."',
                                                          '".$sf->extention."',
                                                          '".$sf->serverName."',
                                                          '".safequery($sf->comment)."',
                                                          ".($sf->isThumbnailable?1:0).",
                                                          ".nullcheck($sf->patientId).",
                                                          ".nullcheck($sf->tooth).",
                                                          ".nullcheck($sf->protocolid).")
                                                          returning id";
//                         file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);	
            $query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            if ($row = ibase_fetch_row($result))
			{
			 	$sf->id = $row[0];
            }			
			ibase_free_query($query);
			ibase_free_result($result);
            if($local)
            {
                $res=ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			return $res;
	}
    
    
    class PrintDataModule_File
    {
        /**
         * @var int
         */
         var $INDEX;
        /**
        * @var string
        */
        var $name;
        /**
        * @var string
        */
        var $extention;
        /**
        * @var string
        */
        var $serverName;
        
        /**    
        * @var Object    
        */    
        var $imageData;
        /**    
        * @var string    
        */    
        var $imagePath;
    }
    
    
    
	
    
?>
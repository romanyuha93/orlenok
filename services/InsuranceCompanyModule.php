<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');

//InsuranceCompanyModule::createReport('odt','1','2013-12-01','2013-12-31');

class InsuranceCompanyReport
{
    /**
    * @var InsuranceCompanyProfile
    */
    var $COMPANY;
    
    /**
    * @var PrintDataModule_ClinicRegistrationData
    */
    var $CLINIC_DATA;

    /**
    * @var InsuranceCompanyInvoice[]
    */
    var $INVOICES;
    
    /**
    * @var string
    */
    var $FROMDATE;
    
    /**
    * @var string
    */
    var $TODATE;
    
        /**
    * @var double
    */
    var $SUM;
    
     function __construct($company_id, $startDate, $endDate, $trans = null) 
   {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
				//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$trans);
        
                $this->CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(false, $trans);
                $this->COMPANY=new InsuranceCompanyProfile($company_id, $trans);
                $this->INVOICES=array();
                $this->FROMDATE= date("d.m.Y", strtotime($startDate));
                $this->TODATE= date("d.m.Y", strtotime($endDate));
                $this->SUM=0;
        	  $QueryText ="select i.id from invoices i
                                left join patientsinsurance pi on (pi.id = i.patieninsurance_fk)
                                where pi.companyid=$company_id and i.createdate>='$startDate' and  i.createdate<='$endDate'";
        		$query = ibase_prepare($trans, $QueryText);
        		$result=ibase_execute($query);
                
        		while ($row = ibase_fetch_row($result))
        		{ 
        		  
				//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$row[0]);
                  $invoice=new InsuranceCompanyInvoice($row[0], $trans);
                  $this->SUM += $invoice->SUM;
                  $this->INVOICES[]=$invoice;
        		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
}

class InsuranceCompanyPolicy
{
    /**
    * @var string
    */  
    var $ID;
    
    /**
    * @var string
    */  
    var $NUMBER;
    
        /**
    * @var string
    */  
    var $FROMDATE;
    
        /**
    * @var string
    */  
    var $TODATE;
    
        /**
    * @var string
    */  
    var $COMMENTS;

    
     function __construct($policy_id, $trans=null) 
   {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	  $QueryText ="select pi.policenumber, pi.policefromdate, pi.policetodate, pi.comments from patientsinsurance pi where pi.id=$policy_id";
        		$query = ibase_prepare($trans, $QueryText);
        		$result=ibase_execute($query);
        		if ($row = ibase_fetch_row($result))
        		{ 
                  $this->ID = $policy_id;
                  $this->NUMBER= $row[0];
                  $this->FROMDATE= date("d.m.Y", strtotime($row[1]));
                  $this->TODATE= date("d.m.Y", strtotime($row[2]));
                  $this->COMMENTS= $row[3];
               
        		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
}

class InsuranceCompanyInvoice
{
    
    /**
    * @var string
    */  
    var $ID;
    
    /**
    * @var string
    */  
    var $DATE;
    
    /**
    * @var double
    */  
    var $DISCOUNT;
    
        /**
    * @var double
    */  
    var $SUM;
    
    /**
    * @var PrintDataModule_PatientData
    */  
    var $PATIENT_DATA;
    
        /**
    * @var PrintDataModule_DoctorData
    */  
    var $DOCTOR_DATA;

    /**
    * @var InsuranceCompanyPolicy
    */  
    var $POLICY;
    
    /**
    * @var string[]
    */  
    var $DIAGNOSES;
    
        /**
    * @var InsuranceCompanyProc[]
    */  
    var $PROCEDURES;
    
     function __construct($invoice_id, $trans=null) 
   {
    
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
            
				//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($trans,true));
        
        	  $QueryText ="select i.patientid, i.doctorid, i.patieninsurance_fk, i.createdate, i.discount_amount, i.summ*((100.00-i.discount_amount)/100) from invoices i
                            where i.id=$invoice_id";
        		$query = ibase_prepare($trans, $QueryText);
        		$result=ibase_execute($query);
        		if ($row = ibase_fetch_row($result))
        		{ 
                  $this->ID = $invoice_id;
                  $this->PATIENT_DATA = new PrintDataModule_PatientData($row[0],$trans);
                  $this->DOCTOR_DATA= new PrintDataModule_DoctorData($row[1],$trans);
                  $this->POLICY= new InsuranceCompanyPolicy($row[2],$trans);
                  $this->DATE= date("d.m.Y", strtotime($row[3]));
                   $this->DISCOUNT= $row[4];
                   $this->SUM= $row[5];
                   $this->DIAGNOSES=InsuranceCompanyInvoice::getDiagnoses($invoice_id,$trans);
                   $this->PROCEDURES=InsuranceCompanyInvoice::getProcedures($invoice_id,$trans);
        		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
     
    public static function getDiagnoses($invoice_id, $trans=null) 
   {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
            $res=array();
            
        $QueryText ="select distinct p.id from treatmentproc tp
       left join diagnos2 d2 on d2.id=tp.diagnos2
        left join protocols p on  p.id=d2.protocol_fk
        where tp.invoiceid=$invoice_id";
    		
            $query = ibase_prepare($trans, $QueryText);
    		$result=ibase_execute($query);
    		while ($row = ibase_fetch_row($result))
    		{ 
              $res[]=new InsuranceCompanyProtocol($row[0],$trans);
           
    		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            
            return $res;
    }
    
     public static function getProcedures($invoice_id, $trans=null) 
   {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
            $res=array();
            
        $QueryText ="select tp.id from treatmentproc tp
                            where tp.invoiceid=$invoice_id 
                            order by tp.diagnos2, tp.stacknumber";
    		
            $query = ibase_prepare($trans, $QueryText);
    		$result=ibase_execute($query);
    		while ($row = ibase_fetch_row($result))
    		{ 
              $res[]=new InsuranceCompanyProc($row[0],$trans);
           
    		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            
            return $res;
    }
}
class InsuranceCompanyProfile
{
    /**
    * @var string
    */
    var $Id;

    /**
    * @var string
    */
    var $Name;

    /**
    * @var string
    */
    var $Phone;
    
    /**
    * @var string
    */
    var $Address;
	
	/**
    * @var string
    */
    var $IdentyNum;
	
	/**
    * @var string
    */
    var $NameLegal;
	
	/**
    * @var string
    */
    var $AddressLegal;
	
	/**
    * @var string
    */
    var $AccountingNumber;
	
	/**
    * @var string
    */
    var $AccountingMFO;
	
	/**
    * @var string
    */
    var $AccountingBank;
    
   function __construct($company_id, $trans=null) 
   {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	  $QueryText ="select ID, NAME, PHONE, ADDRESS, IDENTYNUM, NAMELEGAL, ADDRESSLEGAL, ACCOUNTINGNUMBER, ACCOUNTINGMFO, ACCOUNTINGBANK from INSURANCECOMPANYS where ID=$company_id";
        		$query = ibase_prepare($trans, $QueryText);
        		$result=ibase_execute($query);
        		if ($row = ibase_fetch_row($result))
        		{ 
                  $this->Id = $row[0];
                  $this->Name = $row[1];
                  $this->Phone = $row[2];
                  $this->Address = $row[3];
                  $this->IdentyNum=$row[4];
                  $this->NameLegal=$row[5];
                  $this->AddressLegal=$row[6];
                  $this->AccountingNumber=$row[7];
                  $this->AccountingMFO=$row[8];
                  $this->AccountingBank=$row[9];
        		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
 }
 class InsuranceCompanyProc
{
     /**
    * @var string
    */  
    var $ID;
    
    /**
    * @var string
    */  
    var $NAME;
    
    /**
    * @var string
    */  
    var $PLANNAME;
  
    /**
    * @var string
    */  
    var $SHIFR;  
    
        /**
    * @var int
    */  
    var $COUNT; 
    
    /**
    * @var double
    */  
    var $PRICE;  
    
     function __construct($proc_id, $trans=null) 
   {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }         
        
        	  $QueryText ="select tp.name, 
                          tp.nameforplan, 
                          tp.shifr, 
                          (tp.price*tp.proc_count*((100.00-tp.discount_flag*i.discount_amount)/100)), 
                          tp.proc_count 
                          from treatmentproc tp 
                          left join invoices i on i.id=tp.invoiceid
                          where tp.id=$proc_id";
        		$query = ibase_prepare($trans, $QueryText);
        		$result=ibase_execute($query);
        		if ($row = ibase_fetch_row($result))
        		{ 
                  $this->ID = $proc_id;
                  $this->NAME = $row[0];
                  $this->PLANNAME = $row[1];
                  $this->SHIFR = $row[2];
                  $this->PRICE = $row[3];
                  $this->COUNT=$row[4];
        		}				
			
			ibase_free_query($query);
			ibase_free_result($result);

            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
    }
}
 class InsuranceCompanyProtocol
{
     /**
    * @var string
    */  
    var $ID;
    
    /**
    * @var string
    */  
    var $NAME;
    
    /**
    * @var string
    */  
    var $MKB10;
    
    /**
    * @var string
    */  
    var $MKB10_UANAME;
    
    /**
    * @var string
    */  
    var $MKB10_RUNAME;

    
     function __construct($protocol_id, $trans=null) 
   {
        if(nonull($protocol_id))
        {
            if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }         
            
            	  $QueryText ="select p.name, m.shifr, m.name, m.rusname from protocols p
                                   left join mkb10 m on m.id=p.mkb10
                                    where p.id=$protocol_id";
            		$query = ibase_prepare($trans, $QueryText);
            		$result=ibase_execute($query);
            		if ($row = ibase_fetch_row($result))
            		{ 
                      $this->ID = $protocol_id;
                      $this->NAME = $row[0];
                      $this->MKB10 = $row[1];
                      $this->MKB10_UANAME = $row[2];
                      $this->MKB10_RUNAME = $row[3];
            		}				
    			
    			ibase_free_query($query);
    			ibase_free_result($result);
    
                if($local)
                {
                    ibase_commit($trans);
                    $connection->CloseDBConnection();
                }
        }
    }
} 
class InsuranceCompanyModule
{		
    
     /**
     * @param InsuranceCompanyProfile $p1
     * 
     * @return void
     */
    public function registerTypes($p1){}
  
    /** 
 	* @return InsuranceCompanyProfile[]
	*/ 
    public function getCompanys() //sqlite
     { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	    $QueryText ="select ID from INSURANCECOMPANYS";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();
		while ($row = ibase_fetch_row($result))
		{ 
		  $obj = new InsuranceCompanyProfile($row[0],$trans);
		  $objs[] = $obj;
		}	
		
		ibase_free_query($query);
		ibase_free_result($result);
        
        ibase_commit($trans);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
          
    /**
 	* @param InsuranceCompanyProfile $company 
    * @return boolean
 	*/
    public function addCompany($company) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into INSURANCECOMPANYS (ID, NAME, PHONE, ADDRESS, IDENTYNUM, NAMELEGAL, ADDRESSLEGAL, ACCOUNTINGNUMBER, ACCOUNTINGMFO, ACCOUNTINGBANK) 
        values (null,'".safequery($company->Name)."','".safequery($company->Phone)."','".safequery($company->Address)."','".safequery($company->IdentyNum)."','".safequery($company->NameLegal)."','".safequery($company->AddressLegal)."','".safequery($company->AccountingNumber)."','".safequery($company->AccountingMFO)."','".safequery($company->AccountingBank)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
        $success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
    * @param InsuranceCompanyProfile $company
    * @return boolean
 	*/
    public function updCompany($company) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update INSURANCECOMPANYS set NAME = '".safequery($company->Name)."',
											PHONE ='".safequery($company->Phone)."',
											ADDRESS = '".safequery($company->Address)."',
                                            IDENTYNUM = '".safequery($company->IdentyNum)."',
                                            NAMELEGAL = '".safequery($company->NameLegal)."',
                                            ADDRESSLEGAL = '".safequery($company->AddressLegal)."',
                                            ACCOUNTINGNUMBER = '".safequery($company->AccountingNumber)."',
                                            ACCOUNTINGMFO = '".safequery($company->AccountingMFO)."',
                                            ACCOUNTINGBANK = '".safequery($company->AccountingBank)."'
					     where ID ='$company->Id'"; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
    * @param string $companyId
    * @return boolean
 	*/
    public function deleteCompany($companyId) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from INSURANCECOMPANYS where (ID is null) or (ID=$companyId)";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;
    }
    
       /** 
 * @param string $docType
 * @param string $company_id
 * @param string $startDate
 * @param string $endDate
 * @return string
 */  
 public function createReport($docType, $company_id, $startDate, $endDate)
 {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    
    
        $TBS = new clsTinyButStrong;
      
        $template_type = $docType;
        $template_name='/tbs/res/insurance/InsuranceReport';
        if(file_exists(dirname(__FILE__).$template_name."_".$company_id.".".$template_type))
        {
            $template_name=$template_name."_".$company_id;
        }
       
       $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
       $TBS->LoadTemplate(dirname(__FILE__).$template_name.".".$template_type, OPENTBS_ALREADY_UTF8);
        
            $REPORT = new InsuranceCompanyReport($company_id, $startDate, $endDate, $trans);
        
        //$TBS->NoErr=true;
        $TBS->MergeBlock('INVOICES,',$REPORT->INVOICES);            
        $TBS->MergeField('REPORT',$REPORT);    

        //$file_name = translit(str_replace(' ','_',$REPORT->COMPANY->Name.'_'.$startDate.'_'.$endDate));
        $file_name = translit(str_replace(' ','_','COMPANY'.'_'.$startDate.'_'.$endDate));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
       

        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        
        
		ibase_commit($trans);
		$connection->CloseDBConnection();

        return $form_path;   
 }
    /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteReport($form_path)
    {
        return deleteFileUtils($form_path);
    }
  
}
?>
<?php

require_once "Connection.php";
require_once "Utils.php";

class ComorbiditiesModuleDictionaryObject
{
   /**
    * @var string
    */
    var $ID;

    /**
    * @var string
    */
    var $NUMBER;

    /**
    * @var string
    */
    var $DESCRIPTION;
}

 
class ComorbiditiesModule
{	
    /**  
    * @param ComorbiditiesModuleDictionaryObject $p1
    */    
	public function registertypes($p1) 
	{
	}    
    
    /** 
 	*  @return ComorbiditiesModuleDictionaryObject[]
	*/ 
    public function getComorbidities() //sqlite
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select ID, NUMBER, DESCRIPTION from Comorbidities order by NUMBER";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$objs = array();		
		while ($row = ibase_fetch_row($result))
		{ 
			$obj = new ComorbiditiesModuleDictionaryObject;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->DESCRIPTION = $row[2];
			$objs[] = $obj;
		}
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $objs; 
    }
    
    /**
 	* @param ComorbiditiesModuleDictionaryObject $ovo 
    * @return boolean
 	*/
    public function addComorbidities($ovo) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "insert into Comorbidities (ID, NUMBER, DESCRIPTION) values (null,$ovo->NUMBER, '".safequery($ovo->DESCRIPTION)."')";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    
    /**
 	* @param ComorbiditiesModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updateComorbidities ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Comorbidities
					 set DESCRIPTION ='".safequery($ovo->DESCRIPTION)."'
					     where ID = '$ovo->ID' ";
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;
    }
    /**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
    public function deleteComorbidities($Arguments) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from Comorbidities where (ID is null)";
    	for ($i=0; isset($Arguments[$i]); $i++)
    	{
    		$QueryText = $QueryText." or (ID=$Arguments[$i])";
    	}
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);
		$connection->CloseDBConnection();
		return $success;			
    }
    
    /**
 	* @param ComorbiditiesModuleDictionaryObject $ovo
    * @return boolean
 	*/
    public function updateComorbiditiesNumber ($ovo) 
    {
 		$connection = new Connection();
   		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "update Comorbidities set NUMBER ='$ovo->NUMBER' where ID = '$ovo->ID' "; 
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		$success = ibase_commit($trans);
		ibase_free_query($query);	
		$connection->CloseDBConnection();
		return $success;	
    }      
  }
?>
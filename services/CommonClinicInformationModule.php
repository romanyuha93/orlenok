<?php

require_once "Connection.php";
require_once "Utils.php";

class CommonClinicInformationModuleClinicData
{
	/**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $NAME;
    
    /**
    * @var string
    */
    var $LEGALNAME;
     
    /**
    * @var string
    */
    var $ADDRESS;     
    
    /**
    * @var string
    */
    var $ORGANIZATIONTYPE;
    
    /**
    * @var string
    */
    var $CODE;
    
    /**
    * @var string
    */
    var $DIRECTOR;
     
    /**
    * @var string
    */
    var $TELEPHONENUMBER;   
    
     /**
    * @var string
    */
    var $FAX;
    
    /**
    * @var string
    */
    var $SITENAME;
    
    /**
    * @var string
    */
    var $EMAIL;
     
    /**
    * @var string
    */
    var $ICQ;
    
    /**
    * @var string
    */
    var $SKYPE;
    
    /**
    * @var object
    */
    var $LOGO;
    
    /**
    * @var string
    */
    var $LEGALADDRESS;
    
    /**
    * @var string
    */
    var $BANK_NAME;
    
    /**
    * @var string
    */
    var $BANK_MFO;
    
    /**
    * @var string
    */
    var $BANK_CURRENTACCOUNT;
    
    /**
    * @var string
    */
    var $LICENSE_NUMBER;
    
    /**
    * @var string
    */
    var $LICENSE_SERIES;
    
    /**
    * @var string
    */
    var $LICENSE_ISSUED;
    
    /**
    * @var string
    */
    var $CHIEFACCOUNTANT;
    
    /**
    * @var string
    */
    var $LICENSE_STARTDATE;
    
    /**
    * @var string
    */
    var $LICENSE_ENDDATE;
    
    /**
    * @var string
    */
    var $AUTHORITY;
    
    /**
    * @var string
    */
    var $CITY;
    
    
    /**
    * @var string
    */
    var $SHORT_NAME;
    /**
    * @var string
    */
    var $MSP_TYPE;
    /**
    * @var string
    */
    var $OWNER_PROPERTY_TYPE;
    /**
    * @var string
    */
    var $LEGAL_FORM;
    /**
    * @var string
    */
    var $KVEDS;
    
    /**
    * @var string
    */
    var $ADDRESSLEGAL_ZIP;
    
    /**
    * @var string
    */
    var $ADDRESSLEGAL_TYPE;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_COUNTRY;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_AREA;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_REGION;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_SETTLEMENT;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_SETTLEMENT_TYPE;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_SETTLEMENT_ID;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_STREET_TYPE;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_STREET;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_BUILDING;
    /**
    * @var string
    */
    var $ADDRESSLEGAL_APRT;
    
    /**
    * @var string
    */
    var $PHONE_TYPE;
    /**
    * @var string
    */
    var $PHONE;
    
    /**
    * @var string
    */
    var $ACR_CATEGORY;
    /**
    * @var string
    */
    var $ACR_ISSUED_DATE;
    /**
    * @var string
    */
    var $ACR_EXPIRY_DATE;
    /**
    * @var string
    */
    var $ACR_ORDER_NO;
    /**
    * @var string
    */
    var $ACR_ORDER_DATE;
    /**
     * @var boolean
     */
    var $CONSENT_SIGN;
    /**
    * @var string
    */
    var $EH_ID;
    /**
    * @var string
    */
    var $EH_STATUS;
    /**
    * @var string
    */
    var $EH_CREATED_BY_MIS_CLIENT_ID;
    
    
    
}
class CommonClinicInformationModuleLicenseData
{
    /**
    * @var string
    */
    var $ID;
    /**
    * @var string
    */
    var $LICENSE_NUMBER;
    /**
    * @var string
    */
    var $ISSUED_BY;
    /**
    * @var string
    */
    var $ISSUED_DATE;
    /**
    * @var string
    */
    var $EXPIRY_DATE;
    /**
    * @var string
    */
    var $ACTIVE_FROM_DATE;
    /**
    * @var string
    */
    var $WHAT_LICENSED;
    /**
    * @var string
    */
    var $FK_CLINICREGISTRATIONDATA;
    /**
    * @var string
    */
    var $ORDER_NO;
}

class CommonClinicInformationModuleAddressData
{
    /**
    * @var string
    */
    var $ID;
    
    /**
    * @var string
    */
    var $ZIP;
    
    /**
    * @var string
    */
    var $ADDRESS_TYPE;
    /**
    * @var string
    */
    var $COUNTRY;
    /**
    * @var string
    */
    var $AREA;
    /**
    * @var string
    */
    var $REGION;
    /**
    * @var string
    */
    var $SETTLEMENT;
    /**
    * @var string
    */
    var $SETTLEMENT_TYPE;
    /**
    * @var string
    */
    var $SETTLEMENT_ID;
    /**
    * @var string
    */
    var $STREET_TYPE;
    /**
    * @var string
    */
    var $STREET;
    /**
    * @var string
    */
    var $BUILDING;
    /**
    * @var string
    */
    var $APRT;
    
    /**
    * @var string
    */
    var $FK_CLINICREGISTRATIONDATA;
}


class CommonClinicInformationModulePhone
{
    /**
    * @var string
    */
    var $id;
    
    /**
    * @var string
    */
    var $fk_clinicregistrationdata;//FK_CLINICREGISTRATIONDATA
    /**
    * @var string
    */
    var $phone;
    /**
    * @var string
    */
    var $phone_type;
}

class CommonClinicInformationModule
{		
    public static $isDebugLog = false;
    /**
     * @param CommonClinicInformationModuleClinicData $p1
     * @param CommonClinicInformationModuleLicenseData $p2
     * @param CommonClinicInformationModuleAddressData $p3
     * @return void
     */

    public function registertypes($p1, $p2, $p3)
    {
    }
    
    
	/** 
 	* @return CommonClinicInformationModuleClinicData[]
	*/ 
     public function getClinicRegistrationData() //sqlite
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select clinicregistrationdata.id,
                           clinicregistrationdata.name,
                           clinicregistrationdata.legalname,
                           clinicregistrationdata.address,
                           clinicregistrationdata.organizationtype,
                           clinicregistrationdata.code,
                           clinicregistrationdata.director,
                           clinicregistrationdata.telephonenumber,
                           clinicregistrationdata.fax,
                           clinicregistrationdata.sitename,
                           clinicregistrationdata.email,
                           clinicregistrationdata.icq,
                           clinicregistrationdata.skype,
                           clinicregistrationdata.logo,
                           clinicregistrationdata.chiefaccountant,
                           clinicregistrationdata.legaladdress,
                           clinicregistrationdata.bank_name,
                           clinicregistrationdata.bank_mfo,
                           clinicregistrationdata.bank_currentaccount,
                           clinicregistrationdata.license_number,
                           clinicregistrationdata.license_series,
                           clinicregistrationdata.license_issued,
                           clinicregistrationdata.license_startdate,
                           clinicregistrationdata.license_enddate,
                           clinicregistrationdata.authority,
                           clinicregistrationdata.city,
                           SHORT_NAME,
                           MSP_TYPE,
                           OWNER_PROPERTY_TYPE,
                           LEGAL_FORM,
                           KVEDS,
                           ADDRESSLEGAL_ZIP,
                           ADDRESSLEGAL_TYPE,
                           ADDRESSLEGAL_COUNTRY,
                           ADDRESSLEGAL_AREA,
                           ADDRESSLEGAL_REGION,
                           ADDRESSLEGAL_SETTLEMENT,
                           ADDRESSLEGAL_SETTLEMENT_TYPE,
                           ADDRESSLEGAL_SETTLEMENT_ID,
                           ADDRESSLEGAL_STREET_TYPE,
                           ADDRESSLEGAL_STREET,
                           ADDRESSLEGAL_BUILDING,
                           ADDRESSLEGAL_APRT,
                           PHONE_TYPE,
                           PHONE,
                           ACR_CATEGORY,
                           ACR_ISSUED_DATE,
                           ACR_EXPIRY_DATE,
                           ACR_ORDER_NO,
                           ACR_ORDER_DATE,
                           CONSENT_SIGN,
                           EH_ID,
                           EH_STATUS,
                           EH_CREATED_BY_MIS_CLIENT_ID
                    
                           from clinicregistrationdata";		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new CommonClinicInformationModuleClinicData;
			$obj->ID = $row[0];
			$obj->NAME = $row[1];
			$obj->LEGALNAME = $row[2];
			$obj->ADDRESS = $row[3];
			$obj->ORGANIZATIONTYPE = $row[4];
			$obj->CODE = $row[5];
			$obj->DIRECTOR = $row[6];
			$obj->TELEPHONENUMBER = $row[7];
			$obj->FAX = $row[8];
			$obj->SITENAME = $row[9];
			$obj->EMAIL = $row[10];
			$obj->ICQ = $row[11];
			$obj->SKYPE = $row[12];
            $imageBlob = null;
            $blob_info = ibase_blob_info($row[13]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[13]);
                $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
            }
            $obj->LOGO = base64_encode($imageBlob);
            $obj->CHIEFACCOUNTANT = $row[14];
            $obj->LEGALADDRESS = $row[15];
            $obj->BANK_NAME = $row[16];
            $obj->BANK_MFO = $row[17];
            $obj->BANK_CURRENTACCOUNT = $row[18];
            $obj->LICENSE_NUMBER = $row[19];
            $obj->LICENSE_SERIES = $row[20];
            $obj->LICENSE_ISSUED = $row[21];
            $obj->LICENSE_STARTDATE = $row[22];
            $obj->LICENSE_ENDDATE = $row[23];
            $obj->AUTHORITY = $row[24];
            $obj->CITY = $row[25];
            
            $obj->SHORT_NAME = $row[26];
            $obj->MSP_TYPE = $row[27];
            $obj->OWNER_PROPERTY_TYPE = $row[28];
            $obj->LEGAL_FORM = $row[29];
            $obj->KVEDS = $row[30];
            
            $obj->ADDRESSLEGAL_ZIP = $row[31];
            $obj->ADDRESSLEGAL_TYPE= $row[32];
            $obj->ADDRESSLEGAL_COUNTRY= $row[33];
            $obj->ADDRESSLEGAL_AREA= $row[34];
            $obj->ADDRESSLEGAL_REGION= $row[35];
            $obj->ADDRESSLEGAL_SETTLEMENT= $row[36];
            $obj->ADDRESSLEGAL_SETTLEMENT_TYPE= $row[37];
            $obj->ADDRESSLEGAL_SETTLEMENT_ID= $row[38];
            $obj->ADDRESSLEGAL_STREET_TYPE= $row[39];
            $obj->ADDRESSLEGAL_STREET= $row[40];
            $obj->ADDRESSLEGAL_BUILDING= $row[41];
            $obj->ADDRESSLEGAL_APRT= $row[42];
            $obj->PHONE_TYPE=$row[43];
            $obj->PHONE=$row[44];
            
            $obj->ACR_CATEGORY=$row[45];
            $obj->ACR_ISSUED_DATE=$row[46];
            $obj->ACR_EXPIRY_DATE=$row[47];
            $obj->ACR_ORDER_NO=$row[48];
            $obj->ACR_ORDER_DATE=$row[49];
            $obj->CONSENT_SIGN=$row[50]==1;
            $obj->EH_ID = $row[51];
            $obj->EH_STATUS = $row[52];
            $obj->EH_CREATED_BY_MIS_CLIENT_ID = $row[53];
            
            
			$rows[] = $obj;
		}				
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
    
    /**
 	* @param CommonClinicInformationModuleClinicData $clinicRegistrationData 
	* @return boolean
 	*/
    public function addClinicRegistrationData($clinicRegistrationData) 
    { 
     	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $position = null;
        if($clinicRegistrationData->ADDRESS != null && $clinicRegistrationData->CITY != null && $clinicRegistrationData->ADDRESS != '' && $clinicRegistrationData->CITY != '')
        {
            $position = getLocation($clinicRegistrationData->ADDRESS, $clinicRegistrationData->CITY);
        }
        
		$QueryText = "UPDATE OR INSERT into CLINICREGISTRATIONDATA 
        (id, 
        name, 
        legalname, 
        address, 
        organizationtype, 
        code, 
        director, 
        telephonenumber, 
        fax, 
        sitename, 
        email, 
        icq, 
        skype,
            chiefaccountant,
            legaladdress,
            bank_name,
            bank_mfo,
            bank_currentaccount,
            license_number,
            license_series,
            license_issued,
            license_startdate,
            license_enddate,
            authority,
        city, ";
        if($position != null)
        {
         $QueryText .= "LONGTITUDE,
                        LATITUDE, ";   
        }
        $QueryText .= " SHORT_NAME,
        MSP_TYPE,
        OWNER_PROPERTY_TYPE,
        LEGAL_FORM,
        KVEDS,
        ADDRESSLEGAL_ZIP,
                           ADDRESSLEGAL_TYPE,
                           ADDRESSLEGAL_COUNTRY,
                           ADDRESSLEGAL_AREA,
                           ADDRESSLEGAL_REGION,
                           ADDRESSLEGAL_SETTLEMENT,
                           ADDRESSLEGAL_SETTLEMENT_TYPE,
                           ADDRESSLEGAL_SETTLEMENT_ID,
                           ADDRESSLEGAL_STREET_TYPE,
                           ADDRESSLEGAL_STREET,
                           ADDRESSLEGAL_BUILDING,
                           ADDRESSLEGAL_APRT,
                           
        PHONE_TYPE,
        PHONE,
        ACR_CATEGORY,
        ACR_ISSUED_DATE,
        ACR_EXPIRY_DATE,
        ACR_ORDER_NO,
        ACR_ORDER_DATE,
        CONSENT_SIGN,
        logo)  
		
        values
        
        (null, 
        '".safequery($clinicRegistrationData->NAME)."', 
        '".safequery($clinicRegistrationData->LEGALNAME)."', 
        '".safequery($clinicRegistrationData->ADDRESS)."', 
        '$clinicRegistrationData->ORGANIZATIONTYPE',
        '".safequery($clinicRegistrationData->CODE)."', 
        '".safequery($clinicRegistrationData->DIRECTOR)."',
	    '".safequery($clinicRegistrationData->TELEPHONENUMBER)."', 
        '".safequery($clinicRegistrationData->FAX)."', 
        '".safequery($clinicRegistrationData->SITENAME)."',
		'".safequery($clinicRegistrationData->EMAIL)."', 
        '".safequery($clinicRegistrationData->ICQ)."',  
        '".safequery($clinicRegistrationData->SKYPE)."', 
        '".safequery($clinicRegistrationData->CHIEFACCOUNTANT)."', 
        '".safequery($clinicRegistrationData->LEGALADDRESS)."', 
        '".safequery($clinicRegistrationData->BANK_NAME)."', 
        '".safequery($clinicRegistrationData->BANK_MFO)."', 
        '".safequery($clinicRegistrationData->BANK_CURRENTACCOUNT)."', 
        '".safequery($clinicRegistrationData->LICENSE_NUMBER)."', 
        '".safequery($clinicRegistrationData->LICENSE_SERIES)."', 
        '".safequery($clinicRegistrationData->LICENSE_ISSUED)."', 
        '$clinicRegistrationData->LICENSE_STARTDATE', 
        '$clinicRegistrationData->LICENSE_ENDDATE', 
        '".safequery($clinicRegistrationData->AUTHORITY)."',
        '".safequery($clinicRegistrationData->CITY)."', ";
        
        //geoposition add
        if($position != null)
        {
           $QueryText .= $position[0].", ".$position[1].", "; 
        }
        
        //eh fields
        $QueryText .= "'".safequery($clinicRegistrationData->SHORT_NAME)."', 
                        '".safequery($clinicRegistrationData->MSP_TYPE)."', 
                        '".safequery($clinicRegistrationData->OWNER_PROPERTY_TYPE)."', 
                        '".safequery($clinicRegistrationData->LEGAL_FORM)."', 
                        '".safequery($clinicRegistrationData->KVEDS)."',  
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_ZIP)."', 
                        
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_TYPE)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_COUNTRY)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_AREA)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_REGION)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_SETTLEMENT)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_SETTLEMENT_TYPE)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_SETTLEMENT_ID)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_STREET_TYPE)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_STREET)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_BUILDING)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_APRT)."', 
                        '".safequery($clinicRegistrationData->PHONE_TYPE)."', 
                        '".safequery($clinicRegistrationData->PHONE)."', 
                        '".safequery($clinicRegistrationData->ACR_CATEGORY)."', 
                        '".safequery($clinicRegistrationData->ACR_ISSUED_DATE)."', 
                        '".safequery($clinicRegistrationData->ACR_EXPIRY_DATE)."', 
                        '".safequery($clinicRegistrationData->ACR_ORDER_NO)."', 
                        '".safequery($clinicRegistrationData->ACR_ORDER_DATE)."', 
                        ".$clinicRegistrationData->CONSENT_SIGN?"1":"0".", ";
                        
                        
                            
        
        //logo add
        $QueryText .=  "  ?)";
        $img = base64_decode($clinicRegistrationData->LOGO);
		if ("$img"=="")
		{
			$blobid = null;
		}
		else
		{
		    $blh = ibase_blob_create($connection->connection);		    
		    ibase_blob_add($blh, $img);
		    $blobid = ibase_blob_close($blh);		
		}		
		ibase_query($connection->connection, $QueryText, $blobid);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();	
		return $success;			
    }
    
    /**
 	* @param CommonClinicInformationModuleClinicData $clinicRegistrationData
	* @return boolean
 	*/
    public function updateClinicRegistrationData($clinicRegistrationData) 
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $position = null;
        if($clinicRegistrationData->ADDRESS != null && $clinicRegistrationData->CITY != null && $clinicRegistrationData->ADDRESS != '' && $clinicRegistrationData->CITY != '')
        {
            $position = getLocation($clinicRegistrationData->ADDRESS, $clinicRegistrationData->CITY);
        }
        
        
		$QueryText = "UPDATE OR INSERT into CLINICREGISTRATIONDATA 
        (id, 
        name, 
        legalname, 
        address, 
        organizationtype, 
        code, 
        director, 
        telephonenumber, 
        fax, 
        sitename, 
        email, 
        icq, 
        skype,
            chiefaccountant,
            legaladdress,
            bank_name,
            bank_mfo,
            bank_currentaccount,
            license_number,
            license_series,
            license_issued,
            license_startdate,
            license_enddate,
            authority,
        city, ";
        if($position != null)
        {
             $QueryText .= "LONGTITUDE,
                            LATITUDE, ";   
        }
        $QueryText .= " 
        SHORT_NAME,
        MSP_TYPE,
        OWNER_PROPERTY_TYPE,
        LEGAL_FORM,
        KVEDS,
        ADDRESSLEGAL_ZIP,
                           ADDRESSLEGAL_TYPE,
                           ADDRESSLEGAL_COUNTRY,
                           ADDRESSLEGAL_AREA,
                           ADDRESSLEGAL_REGION,
                           ADDRESSLEGAL_SETTLEMENT,
                           ADDRESSLEGAL_SETTLEMENT_TYPE,
                           ADDRESSLEGAL_SETTLEMENT_ID,
                           ADDRESSLEGAL_STREET_TYPE,
                           ADDRESSLEGAL_STREET,
                           ADDRESSLEGAL_BUILDING,
                           ADDRESSLEGAL_APRT,
        PHONE_TYPE,
        PHONE,
        ACR_CATEGORY,
        ACR_ISSUED_DATE,
        ACR_EXPIRY_DATE,
        ACR_ORDER_NO,
        ACR_ORDER_DATE,
        CONSENT_SIGN,
        logo)  
		
        values
        
        ($clinicRegistrationData->ID, 
        '".safequery($clinicRegistrationData->NAME)."', 
        '".safequery($clinicRegistrationData->LEGALNAME)."', 
        '".safequery($clinicRegistrationData->ADDRESS)."', 
        '$clinicRegistrationData->ORGANIZATIONTYPE',";
        
        if ($clinicRegistrationData->CODE == "" || $clinicRegistrationData->CODE == null || !is_numeric( $clinicRegistrationData->CODE))
		{ 
		   $QueryText = $QueryText."null, "; 
        }
		else 
        { 
            $QueryText = $QueryText."'".safequery($clinicRegistrationData->CODE)."', "; 
        } 
        
        $QueryText = $QueryText."'".safequery($clinicRegistrationData->DIRECTOR)."',
	    '".safequery($clinicRegistrationData->TELEPHONENUMBER)."', 
        '".safequery($clinicRegistrationData->FAX)."', 
        '".safequery($clinicRegistrationData->SITENAME)."',
		'".safequery($clinicRegistrationData->EMAIL)."', 
        '".safequery($clinicRegistrationData->ICQ)."',  
        '".safequery($clinicRegistrationData->SKYPE)."', 
        '".safequery($clinicRegistrationData->CHIEFACCOUNTANT)."', 
        '".safequery($clinicRegistrationData->LEGALADDRESS)."', 
        '".safequery($clinicRegistrationData->BANK_NAME)."', 
        '".safequery($clinicRegistrationData->BANK_MFO)."', 
        '".safequery($clinicRegistrationData->BANK_CURRENTACCOUNT)."', 
        '".safequery($clinicRegistrationData->LICENSE_NUMBER)."', 
        '".safequery($clinicRegistrationData->LICENSE_SERIES)."', 
        '".safequery($clinicRegistrationData->LICENSE_ISSUED)."', 
        '$clinicRegistrationData->LICENSE_STARTDATE',
        '$clinicRegistrationData->LICENSE_ENDDATE',
        '".safequery($clinicRegistrationData->AUTHORITY)."',
        '".safequery($clinicRegistrationData->CITY)."', ";
        
        //geoposition update
        if($position != null)
        {
           $QueryText .= $position[0].", ".$position[1].", "; 
        }
        
        //eh fields
        $QueryText .= "'".safequery($clinicRegistrationData->SHORT_NAME)."', 
                        '".safequery($clinicRegistrationData->MSP_TYPE)."', 
                        '".safequery($clinicRegistrationData->OWNER_PROPERTY_TYPE)."', 
                        '".safequery($clinicRegistrationData->LEGAL_FORM)."', 
                        '".safequery($clinicRegistrationData->KVEDS)."',  
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_ZIP)."', 
                        
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_TYPE)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_COUNTRY)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_AREA)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_REGION)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_SETTLEMENT)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_SETTLEMENT_TYPE)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_SETTLEMENT_ID)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_STREET_TYPE)."',   
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_STREET)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_BUILDING)."', 
                        '".safequery($clinicRegistrationData->ADDRESSLEGAL_APRT)."', 
                        
                        '".safequery($clinicRegistrationData->PHONE_TYPE)."', 
                        '".safequery($clinicRegistrationData->PHONE)."', 
                        '".safequery($clinicRegistrationData->ACR_CATEGORY)."', 
                        ".nullcheck($clinicRegistrationData->ACR_ISSUED_DATE, true).", 
                        ".nullcheck($clinicRegistrationData->ACR_EXPIRY_DATE, true).", 
                        '".safequery($clinicRegistrationData->ACR_ORDER_NO)."', 
                        ".nullcheck($clinicRegistrationData->ACR_ORDER_DATE, true).", ";
        if($clinicRegistrationData->CONSENT_SIGN)
        {
            $QueryText .= "1, ";
        }
        else
        {
            $QueryText .= "0, ";
        }
                        
        
        //logo update
        $QueryText .=  " ?) MATCHING (ID)";
        
        
		if(CommonClinicInformationModule::$isDebugLog)
		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($QueryText,true));
		}
        
        $img = base64_decode($clinicRegistrationData->LOGO);
		if ("$img"=="")
		{
			$blobid = null;
		}
		else
		{
		    $blh = ibase_blob_create($connection->connection);		    
		    ibase_blob_add($blh, $img);
		    $blobid = ibase_blob_close($blh);		
		}		
		ibase_query($connection->connection, $QueryText, $blobid);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();	
		return $success;
    }    
    
    
// ************************ LICENSES START ******************************
   	/** 
 	* @param string $clinic_id
 	* @return CommonClinicInformationModuleLicenseData[]
	*/ 
     public function getLicenses($clinic_id)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText ="select 
                            ID, 
                            LICENSE_NUMBER, 
                            ISSUED_BY, 
                            ISSUED_DATE, 
                            EXPIRY_DATE, 
                            ACTIVE_FROM_DATE, 
                            WHAT_LICENSED, 
                            FK_CLINICREGISTRATIONDATA,
                            ORDER_NO
                        from EH_MSP_LICENSES
                        where FK_CLINICREGISTRATIONDATA = $clinic_id";		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new CommonClinicInformationModuleLicenseData;
			$obj->ID = $row[0];
			$obj->LICENSE_NUMBER = $row[1];
			$obj->ISSUED_BY = $row[2];
			$obj->ISSUED_DATE = $row[3];
			$obj->EXPIRY_DATE = $row[4];
			$obj->ACTIVE_FROM_DATE = $row[5];
			$obj->WHAT_LICENSED = $row[6];
			$obj->FK_CLINICREGISTRATIONDATA = $row[7];
            $obj->ORDER_NO = $row[8];
            
			$rows[] = $obj;
		}				
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
   	/** 
 	* @param CommonClinicInformationModuleLicenseData $license
 	* @return CommonClinicInformationModuleLicenseData
	*/ 
     public function saveLicenses($license)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
        $save_sql = "update or insert into EH_MSP_LICENSES (
                                ID, 
                                LICENSE_NUMBER, 
                                ISSUED_BY, 
                                ISSUED_DATE, 
                                EXPIRY_DATE, 
                                ACTIVE_FROM_DATE, 
                                WHAT_LICENSED, 
                                FK_CLINICREGISTRATIONDATA,
                                ORDER_NO )
               values (
                            ".nullcheck($license->ID).", 
                            '".safequery($license->LICENSE_NUMBER)."', 
                            '".safequery($license->ISSUED_BY)."', 
                            ".nullcheck($license->ISSUED_DATE, true).",
                            ".nullcheck($license->EXPIRY_DATE, true, true).",
                            ".nullcheck($license->ACTIVE_FROM_DATE, true, true).",
                            '".safequery($license->WHAT_LICENSED)."',
                            ".$license->FK_CLINICREGISTRATIONDATA.",
                            '".safequery($license->ORDER_NO)."' )
               matching (ID) returning (ID)";
       	if(CommonClinicInformationModule::$isDebugLog)
		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($save_sql,true));
		}
               		
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $license->ID = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        return $license;
    }
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deleteLicenseByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from EH_MSP_LICENSES where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    // ************************ LICENSES END ******************************


    // ************************ ADDRESSES START ******************************
   	/** 
 	* @param string $clinic_id
 	* @return CommonClinicInformationModuleAddressData[]
	*/ 
     public function getAddresses($clinic_id)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select
         
                            ID, 
                            ZIP, 
                            ADDRESS_TYPE, 
                            COUNTRY, 
                            AREA, 
                            REGION, 
                            SETTLEMENT, 
                            SETTLEMENT_TYPE,
                            SETTLEMENT_ID, 
                            STREET_TYPE, 
                            STREET, 
                            BUILDING, 
                            APRT,
                            FK_CLINICREGISTRATIONDATA
                            
                    from EH_MSP_ADDRESSES
                    where FK_CLINICREGISTRATIONDATA = $clinic_id";		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new CommonClinicInformationModuleAddressData;
			$obj->ID = $row[0];
			$obj->ZIP = $row[1];
			$obj->ADDRESS_TYPE = $row[2];
			$obj->COUNTRY = $row[3];
			$obj->AREA = $row[4];
			$obj->REGION = $row[5];
			$obj->SETTLEMENT = $row[6];
			$obj->SETTLEMENT_TYPE = $row[7];
			$obj->SETTLEMENT_ID = $row[8];
			$obj->STREET_TYPE = $row[9];
			$obj->STREET = $row[10];
			$obj->BUILDING = $row[11];
			$obj->APRT = $row[12];
            
			$obj->FK_CLINICREGISTRATIONDATA = $row[13];
            
			$rows[] = $obj;
		}				
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
    
   	/** 
 	* @param CommonClinicInformationModuleAddressData $address
 	* @return CommonClinicInformationModuleAddressData
	*/ 
     public function saveAddress($address)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
        $save_sql = "update or insert into EH_MSP_ADDRESSES (
                                                        ID, 
                                                        FK_CLINICREGISTRATIONDATA, 
                                                        ZIP, 
                                                        ADDRESS_TYPE, 
                                                        COUNTRY, AREA, 
                                                        REGION,
                                                        SETTLEMENT, SETTLEMENT_TYPE, 
                                                        SETTLEMENT_ID, 
                                                        STREET_TYPE, 
                                                        STREET, 
                                                        BUILDING, 
                                                        APRT )
                                                values (
                                                      ".nullcheck($address->ID).", 
                                                      ".$address->FK_CLINICREGISTRATIONDATA.", 
                                                      '".safequery($address->ZIP)."', 
                                                      '".safequery($address->ADDRESS_TYPE)."', 
                                                      '".safequery($address->COUNTRY)."', 
                                                      '".safequery($address->AREA)."', 
                                                      '".safequery($address->REGION)."', 
                                                      '".safequery($address->SETTLEMENT)."', 
                                                      '".safequery($address->SETTLEMENT_TYPE)."',
                                                      '".safequery($address->SETTLEMENT_ID)."', 
                                                      '".safequery($address->STREET_TYPE)."', 
                                                      '".safequery($address->STREET)."', 
                                                      '".safequery($address->BUILDING)."', 
                                                      '".safequery($address->APRT)."')
                                                matching (ID) returning (ID)";
       	if(CommonClinicInformationModule::$isDebugLog)
		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($save_sql,true));
		}
               		
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $address->ID = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        return $address;
    }
    
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deleteAddressByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from EH_MSP_ADDRESSES where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
// ************************ ADDRESSES END ******************************


// ************************ PHONES START ******************************
   	/** 
 	* @param string $clinic_id
 	* @return CommonClinicInformationModulePhone[]
	*/ 
     public function getPhones($clinic_id)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select 
                            ID, 
                            FK_CLINICREGISTRATIONDATA, 
                            PHONE, 
                            PHONE_TYPE
                        from EH_MSP_PHONE
                    where FK_CLINICREGISTRATIONDATA = $clinic_id";		
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new CommonClinicInformationModulePhone;
			$obj->id = $row[0];
			$obj->fk_clinicregistrationdata = $row[1];
			$obj->phone = $row[2];
			$obj->phone_type = $row[3];
		
			$rows[] = $obj;
		}				
		ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();		
		return $rows;  
    }
    
   	/** 
 	* @param CommonClinicInformationModulePhone $address
 	* @return CommonClinicInformationModulePhone
	*/ 
     public function savePhones($phone)
     { 
     	$connection = new Connection();
     	$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
	
        $save_sql = "update or insert into EH_MSP_PHONE (
                                                    ID, 
                                                    FK_CLINICREGISTRATIONDATA, 
                                                    PHONE, 
                                                    PHONE_TYPE  )
                                       values (       ".nullcheck($phone->id).", 
                                                      ".$phone->fk_clinicregistrationdata.", 
                                                      '".safequery($phone->phone)."', 
                                                      '".safequery($phone->phone_type)."')
                                         matching (ID) returning (ID)";
               		
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $phone->ID = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        return $phone;
    }
    
    /**
    * @param string $id
    * @return boolean
 	*/
    public function deletePhoneByID($id) 
    {       	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from EH_MSP_PHONE where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
// ************************ ADDRESSES END ******************************
}

?>
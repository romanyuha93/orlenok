<?php
require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once "Form039Module.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');


class HealthPlanPrintModule_Diagnos
{
    /**
    * @var string
    */
    var $diagnosName;
    
    /**
    * @var string
    */
    var $toothString;
    /**
    * @var number
    */
    var $summ;
    /**
    * @var number
    */
    var $summ_plan;
    /**
    * @var number
    */
    var $summ_fact;
    
}

class HealthPlanPrintModule_Procedure
{
    /**
    * @var string
    */
    var $name;
    
    /**
    * @var string
    */
    var $name_plan;
    
    /**
    * @var string
    */
    var $shifr;
    
    /**
    * @var int
    */
    var $visit;
    
    
    /**
    * @var number
    */
    var $price;
    
    /**
    * @var int
    */
    var $proc_count;
    
    /**
    * @var string
    */
    var $dateclose;
    
    
    
    /**
    * @var int
    */
    var $isClose;
    /**
    * @var number
    */
    var $price_full;
    
}



class HealthPlanPrintModule
{
    /** 
    * @param string $docType
    * @param string $diagnos2id
    * @param string $patientId
    * @return string
    */ 
    public static function printTreatmentPlan($docType, $diagnos2id, $patientId)
    {   
            
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                
                
            $DIAGNOS=HealthPlanPrintModule::getDiagnos2($diagnos2id,$trans); 
            $CLINIC_DATA=new PrintDataModule_ClinicRegistrationData(true, $trans);
            $PATIENT_DATA=new PrintDataModule_PatientData($patientId, $trans);
            $PROCEDURES=HealthPlanPrintModule::getProcedures($diagnos2id,$trans,false); 
            
            $DIAGNOS->summ = 0;
            $DIAGNOS->summ_plan = 0;
            $DIAGNOS->summ_fact =0;
            
                for ($i = 0; $i < count($PROCEDURES); $i++)
                {
                    $DIAGNOS->summ += $PROCEDURES[$i]->price_full;
                    if($PROCEDURES[$i]->isClose == 1)
                    {
                        $DIAGNOS->summ_fact += $PROCEDURES[$i]->price_full;
                    }
                    else
                    {
                        $DIAGNOS->summ_plan += $PROCEDURES[$i]->price_full;
                    }
                    $PROCEDURES[$i]->price = number_format($PROCEDURES[$i]->price,2);
                    $PROCEDURES[$i]->price_full = number_format($PROCEDURES[$i]->price_full,2);
                }
                
            $DIAGNOS->summ = number_format($DIAGNOS->summ,2);
            $DIAGNOS->summ_fact = number_format($DIAGNOS->summ_fact,2);
            $DIAGNOS->summ_plan = number_format($DIAGNOS->summ_plan,2);
            
            
            
            $TBS = new clsTinyButStrong;
            $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
            $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/TreatmentPlan.'.$docType, OPENTBS_ALREADY_UTF8);  
            
           
            
           // $TBS->NoErr=true;
                  
            $TBS->MergeBlock('PROCEDURES',$PROCEDURES);
            $TBS->MergeField('DIAGNOS',$DIAGNOS);
            $TBS->MergeField('CLINIC_DATA',$CLINIC_DATA);
            $TBS->MergeField('PATIENT_DATA',$PATIENT_DATA);
                    
            $file_name = translit(str_replace(' ','_','TreatmentPlan_'.$PATIENT_DATA->LASTNAME.'_'.$PATIENT_DATA->FIRSTNAME.'_'.$PATIENT_DATA->MIDDLENAME.'_'.$PATIENT_DATA->CARDNUMBER));
            $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
            $form_path = uniqid().'_'.$file_name.'.'.$docType;   
            
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
            
            
            
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
                    
            return $form_path; 
    }
        
        
        
        /** 
        * @param string $form_path
        * @return boolean
        */  
        public static function deleteTreatmentPlan($form_path)
        {
            return deleteFileUtils($form_path);
        }
        
    
    //***************************************
    
        /**
         * @param string $diagnos2Id
         * @param resource $trans
         * @return HealthPlanPrintModule_Diagnos
         */
        private static function getDiagnos2($diagnos2Id, $trans=null)
        {
    		if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            $diagnos = new HealthPlanPrintModule_Diagnos;
                
            // diagnos name
            $QueryText = "select protocols.name from protocols
                        where protocols.id = (select diagnos2.protocol_fk from diagnos2 where diagnos2.id = $diagnos2Id)";
            $query = ibase_prepare($trans, $QueryText);
 			$result=ibase_execute($query);
 			if($row = ibase_fetch_row($result))
 			{
				$diagnos->diagnosName = $row[0];
 			}
 			ibase_free_query($query);
 			ibase_free_result($result);
            
            //diagnos teeth
            $QueryText = "select
                                treatmentobjects.tooth,
                                treatmentobjects.toothside
                            from treatmentobjects
                                where treatmentobjects.diagnos2 = $diagnos2Id";
            $query = ibase_prepare($trans, $QueryText);
 			$result=ibase_execute($query);
            $diagnos->toothString = '';
 			while ($row = ibase_fetch_row($result))
            { 
				$diagnos->toothString .= $row[0]."(".$row[1].") ";
 			}
 			ibase_free_query($query);
 			ibase_free_result($result);
            
                
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }   
            return $diagnos;
        }
        
        
        
    /**
     * @param string $diagnos2Id
     * @param resource $trans
     * @return TreatmentManagerModuleTemplateProcedure[]
     */
    private static function getProcedures($diagnos2Id, $trans=null)
    {
			if(!$trans)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select
                            treatmentproc.name,
                            treatmentproc.nameforplan,
                            treatmentproc.shifr,
                            treatmentproc.visit,
                            treatmentproc.price,
                            treatmentproc.proc_count,
                            treatmentproc.dateclose
                        from treatmentproc
                            where treatmentproc.diagnos2 = $diagnos2Id
                            order by treatmentproc.stacknumber";
			$query = ibase_prepare($trans, $QueryText);
			$result=ibase_execute($query);
            $procedures = array();
            
			while($row = ibase_fetch_row($result))
			{
                $procedure = new HealthPlanPrintModule_Procedure;
				$procedure->name = $row[0];
				$procedure->name_plan = $row[1];
                $procedure->shifr = $row[2];
                $procedure->visit = $row[3];
                $procedure->price = $row[4];
                $procedure->proc_count = $row[5];
                $procedure->price_full = $procedure->price * $procedure->proc_count;
                $procedure->dateclose = $row[6];
                if($procedure->dateclose != null && $procedure->dateclose != '')
                {
                    $procedure->isClose = 1;
                }
                else
                {
                    $procedure->isClose = 0;
                }
				$procedures[] = $procedure;
			}
            
			ibase_free_query($query);
			ibase_free_result($result);
            
			if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
            return $procedures;
    }

}

?>
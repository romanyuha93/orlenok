<?php

require_once (dirname(__FILE__).'/tbs/tbs_class.php');
require_once (dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
require_once "PrintDataModule.php";

require_once "Connection.php";
require_once "Utils.php";
require_once "CalendarModule.php";

//$c = new ScheduleReportsModule();
//echo "<pre>";
//print_r($c-> createGraphic("odt" , 11, '2017-11-20', '2017-12-31'));
//echo "</pre>";

class ScheduleReportsModule
{

    private function normalizeTime($time)
    {
        $normStr = substr( $time, 0, -3);
        if($normStr[0]=="0" ){
            $normStr = substr($normStr, 1);
        }
        
        return $normStr;
    }
    
    
  
    private function getDoctorById($doctorId, $trans=null)
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }  
        
        $QueryText =
            "select d.ID, TIN, FIRSTNAME, MIDDLENAME, LASTNAME, FULLNAME, SHORTNAME, BIRTHDAY,
                SEX, SPECIALITY, DATEWORKSTART, DATEWORKEND, MOBILEPHONE, HOMEPHONE,
                EMAIL, SKYPE, HOMEADRESS, POSTALCODE, STATE, REGION, CITY, BARCODE ,
                a.IMAGE
            from DOCTORS d
            left join avatarimages a on (d.ID = a.ID)
            where d.ID = $doctorId";
                  
        
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $obj = new stdclass;
        while ($row = ibase_fetch_row($result))
        {
            
			$obj->ID = $row[0];
			$obj->TIN = $row[1];
			$obj->FIRSTNAME = $row[2];
			$obj->MIDDLENAME = $row[3];
			$obj->LASTNAME = $row[4];
			$obj->FULLNAME = $row[5];
			$obj->SHORTNAME = $row[6];
			$obj->BIRTHDAY = $row[7];
			$obj->SEX = $row[8];
			$obj->SPECIALITY = $row[9];
			$obj->DATEWORKSTART = $row[10];
			$obj->DATEWORKEND = $row[11];
			$obj->MOBILEPHONE = $row[12];
			$obj->HOMEPHONE = $row[13];
			$obj->EMAIL = $row[14];
			$obj->SKYPE = $row[15];
			$obj->HOMEADRESS = $row[16];
			$obj->POSTALCODE = $row[17];
			$obj->STATE = $row[18];
			$obj->REGION = $row[19];
			$obj->CITY = $row[20];
            $obj->BARCODE = $row[21];
            
            $imgBlob = null;
            
            $blob_info = ibase_blob_info($row[22]);
            if ($blob_info[0]!=0)
            {
                $blob_hndl = ibase_blob_open($row[22]);
                $imgBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                ibase_blob_close($blob_hndl);
            }
            $obj->IMG = base64_encode($imgBlob);
            
        }
        
        ibase_free_query($query);
        ibase_free_result($result);
        
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        
        return $obj;
    }
    
    
    
    
    
    
   
    private function getWorkTimesByAllDoctors($dateStart, $dateEnd, $trans = null)
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }  
        
        
        $result = array();
        $module = new CalendarModule();
        
        $QueryText = format_for_query("select id from doctors");
        $query = ibase_prepare($trans, $QueryText);
        $res=ibase_execute($query);
        $rows = array();
        while ($row = ibase_fetch_row($res))
        {
            $rows[] = (int)$row[0];
        }
        
        $schedules = $module->getScheduleOnDoctors($rows, $dateStart, $dateEnd, $trans);
        ibase_free_query($query);
        ibase_free_result($res);
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        return $schedules;
    }
    
    
    
    private function getRooms($trans = null)
    {
        if(!$trans)
        {
            $connection = new Connection();
  		    $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
        
		$QueryText = "select ID, NUMBER, NAME, ROOMCOLOR from ROOMS";
		$query = ibase_prepare($trans, $QueryText);
		$result=ibase_execute($query);
		$rows = array();		
		while ($row = ibase_fetch_row($result))
		{
			$obj = new stdclass;
			$obj->ID = $row[0];
			$obj->NUMBER = $row[1];
			$obj->NAME = $row[2];
            		$obj->ROOMCOLOR = $row[3];
			$rows[] = $obj;
		}
		
		ibase_free_query($query);
		ibase_free_result($result);
		
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }		
		return $rows; 
    }
    
    
    private function getRoomById($roomsArr, $roomId)
    {
        for($i=0; $i<count($roomsArr); $i++){
            $currRoom = $roomsArr[$i];
            if($currRoom->ID == $roomId)
            {
                return $currRoom->NUMBER;
            }
        }
        
        return "CABINET_NOT_FOUND";
    }

   private function getRoomNameById($roomsArr, $roomId)
    {
        for($i=0; $i<count($roomsArr); $i++){
            $currRoom = $roomsArr[$i];
            if($currRoom->ID == $roomId)
            {
                return $currRoom->NAME;
            }
        }
        
        return "CABINET_NOT_FOUND";
    }
    
    /** 
  * @param string $file_name
  * @return boolean
  */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);
        
    }
    
    
    /** 
    * docx,odt
    * @param string $docType
    * @param string $doctorId
    * @param string $startDate
    * @param string $endDate
    * @return string
    */  
    public function createGraphic($docType, $doctorId, $startDate, $endDate)
    {
        $res=dirname(__FILE__).'/tbs/res/schedule/';
        
        if(($docType == 'docx') && (!file_exists($res.'Schedule.docx') ) )
        {
            return "TEMPLATE_NOTFOUND";
        }
        else if(($docType == 'odt') && (!file_exists($res.'Schedule.odt') ) )
        {
            return "TEMPLATE_NOTFOUND";
        }
        
        //===================================================  Проверки  ==================
        
        if(strtotime($startDate) > strtotime($endDate) ){
            return "INPUT_TIME_INTERVALS_ERROR";
        }
        
        
        //===================================================  Конекшн к базе  ============
       	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        //===================================================  Данные клиники  ============
        
        $ClinicData = new PrintDataModule_ClinicRegistrationData(true, $trans);
        
        
        
        //===================================================  Данные врача  ==============
        
        $Data = new stdclass;
        $Data->fromDate = ua_date("d M Y", strtotime($startDate) );
        $Data->toDate = ua_date("d M Y", strtotime($endDate) );
        $GLOBALS['onlyOneDoctor']= '0';
        $Data->onlyOneDoctor = 0;
        //$Data->emptyVar="";
        
        $allWorkDaysFromSchedules = array();
        // ищем всех докторов, у которых есть графики
        
        $schedules = $this->getWorkTimesByAllDoctors($startDate, $endDate, $trans);
		//file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($schedules, true));
                
        $rooms = $this->getRooms($trans);
        
        
        $Doctor = new stdclass;
        if(($doctorId != null ) && ($doctorId != '-' ))
        {
            $Data->onlyOneDoctor = 1; 
             $GLOBALS['onlyOneDoctor']= '1';
            // Если выбран конкретный доктор, тогда с общего объекта удаляем всех остальных докторов
            for($j=0; $j<count($schedules); $j++){
                 if( $schedules[$j]!=null  &&  $schedules[$j][0]!=null ){
                     $_Doctor = $schedules[$j][0]->Doctor;
                     if($_Doctor->ID != $doctorId){
                         //echo("_UNSET_".$_Doctor->ID ."<br/>"); 
                         $schedules[$j][0]=null;
                     }else{
                         //echo("_SET_".$_Doctor->ID ."<br/>"); 
                     }
                 }
            }
            
            $Doctor = $this->getDoctorById($doctorId, $trans);
            
        }
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        // Строим массив, в котором содержатся даты, для которых есть хоть один график.
        for($i=0; $i<count($schedules); $i++)
        {
            if( $schedules[$i]!=null  &&  $schedules[$i][0]!=null )
            {
                $daySchedule = $schedules[$i][0]->daySchedule;
                
                if($daySchedule!=null){
                    foreach($daySchedule as $key=>$value){
                        $allWorkDaysFromSchedules[] = $value->day;
                    }
                }
            }
        }
        $allWorkDaysFromSchedules = array_unique($allWorkDaysFromSchedules);
        if(count($allWorkDaysFromSchedules)==0)
        {
            return "NO_SCHEDULES_FOR_PERIOD";
        }
        
        
        
        //===================================================  Календарь  =================
        //echo("StartDate: ". $startDate. "<br/>");
        //echo("EndDate: ". $endDate. "<br/>");
        
        $weekTime = 60*60*24*7;
        $dayTime = 60*60*24;
        $dayNumberStart =  date('w',strtotime($startDate)) ;
        $localDayStart = $dayNumberStart!=0 ? $dayNumberStart -1 : 6;
        //echo("From monday: ". $localDayStart. "<br/>");
        
        $mondayOfStartWeek = Date('Y-m-d', strtotime($startDate) - ($dayTime)* $localDayStart );
        
        $dayNumberEnd =  date('w', strtotime($endDate)) ;
        $localDayEnd = $dayNumberEnd!=0 ? $dayNumberEnd -1 : 6;
        //echo("From monday: ". $localDayEnd. "<br/>");
        
        
        $sundayOfEndWeek = Date('Y-m-d', strtotime($endDate) + ($dayTime)* (7-$localDayEnd)-1  );
        
        $weeksCount = (strtotime($sundayOfEndWeek) - strtotime($mondayOfStartWeek)) / ($weekTime);
        //echo(" weeksCount: " . $weeksCount . "<br/>");
        
        
        
        
        
        $weeks = array();
        for($i=0; $i<$weeksCount; $i++)
        {
            $startWeekTime = strtotime($mondayOfStartWeek)+ $i*$weekTime;
            $currWeek = array("days"=>array() );
            for($n=0; $n<7; $n++)
            {
                $currDay = new stdclass();
                
                $startDaysTime = $startWeekTime + $dayTime*$n;
                $dateString = date("Y-m-d", $startDaysTime);
                $currDay -> dateString = $dateString;
                $currDay->localizedDate = ua_date('d M Y', $startDaysTime );
                $currDay->localizedDateNumber = (int) ua_date('d', $startDaysTime );
                $currDay->localizedDateMonthYear = ua_date('M Y', $startDaysTime ); 
                
                if(  ($startDaysTime < strtotime($startDate) )  ||  ($startDaysTime > strtotime($endDate)) ){
                    $currDay->typeOfDay = "";
                }else
                
                if(in_array($dateString, $allWorkDaysFromSchedules) )
                {
                    $currDay->typeOfDay = 'isWorked';
                    
                    // в один день может быть и будет несколько записей для докторов
                    $doctorsArr = array( ); 
                    
                    for($j=0; $j<count($schedules); $j++)
                    {
                        // Проверяем, есть ли у доктора график в выбранный день.
                        if($schedules[$j]!=null  &&  $schedules[$j][0]!=null  && ($schedules[$j][0]->daySchedule ) )
                        {
                            $doctorObj = $schedules[$j][0];
                            
                            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($doctorObj, true)."\n------------------------------------------------------------------------------------\n");
                            
                            $doctorShedulesObj = $doctorObj->daySchedule;
                            $docWorkInThisDay = false;
                            foreach($doctorShedulesObj as $key => $value){
                                if($value->day == $dateString){
                                    $docWorkInThisDay = true;
                                }
                            }
                            
                            if($docWorkInThisDay)
                            {
                                $OneDoctorOnDay = new stdclass();
                                // если только один доктор, тогда не выводим в ячейках его имя
                                if($Data->onlyOneDoctor == 1){
                                    $OneDoctorOnDay->name = "" ;
                                }else{
                                    $OneDoctorOnDay->name = $doctorObj->Doctor->SHORTNAME ;
                                }
                                    // у одного доктора теоретически может быть несколько графиков в один и тот же день.
                                
                                $schedulesArr = array(); 
                                foreach($doctorShedulesObj as $key => $value)
                                {
                                    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($value, true)."\n------------------------------------------------------------------------------------\n");
                                    if($value->day == $dateString){
                                        $timeIntervalsObj = $value->timeIntervals;
                                        for($t=0; $t<count($timeIntervalsObj); $t++ ){
                                            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($timeIntervalsObj[$t], true)."\n");
                                            $ScheduleData = new stdclass;
                                            $ScheduleData->startTime = $this->normalizeTime($timeIntervalsObj[$t]->startTime);
                                            $ScheduleData->endTime = $this->normalizeTime($timeIntervalsObj[$t]->endTime);
                                            //$ScheduleData->cabinetId = $this->getRoomById($rooms, $timeIntervalsObj[$t]->cabinetID );
					                        //$ScheduleData->cabinetName = $this->getRoomNameById($rooms, $timeIntervalsObj[$t]->cabinetID );
                                            $ScheduleData->cabinetId = $timeIntervalsObj[$t]->cabinetNumber;
					                        $ScheduleData->cabinetName = $timeIntervalsObj[$t]->cabinetName;
                                            $schedulesArr[] = $ScheduleData;
                                        }
                                    }
                                }
                                $OneDoctorOnDay->schedules = $schedulesArr;  
                                  
                                $doctorsArr[]=$OneDoctorOnDay;
                            }
                        }
                    }
                    
                    $currDay->doctors = $doctorsArr;
                    
                    
                }else{
                    $currDay->typeOfDay = "isNotWorked";
                    
                }
                $currWeek["days"][] = $currDay;
                
            }
            
            $weeks[] = $currWeek;
        }

//        echo(" stratWeek: " . $mondayOfStartWeek ."  endWeek: " . $sundayOfEndWeek );
        
        
        $TBS = new clsTinyButStrong();
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        
        $TBS->LoadTemplate($res.'Schedule.'.$docType, OPENTBS_ALREADY_XML);
        
        
        
        
        $TBS->MergeBlock("weeks", $weeks);
        $TBS->MergeField("Doctor", $Doctor);
        $TBS->MergeField("Data", $Data);
        $TBS->MergeField("ClinicData", $ClinicData);
        
        
        
        //$date = date("d-m-Y_H-i-s", time());
        $file_name = '_Shedule_from_' . $startDate . '_to_' . $endDate ;
        $file_name = preg_replace('/[^A-Za-z0-9_-]/', '', $file_name);
        $file_name = uniqid().$file_name.'.'.$docType;
        
//        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$file_name );  // создает файл шаблона
        
        
        
//        return $file_name;
        return $doctorObj;
    
    }

}

?>

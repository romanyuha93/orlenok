<?php 

require_once "Connection.php";
require_once "Utils.php";

//define('tfClientId','online');


class NHDiagnosInspectionExamOut 
{
	/**
   * @var string
   */
   	var $nh_diagnos;
   
    /**
   * @var int
   */
	var $examid;
   /**
   * @var string
   */
	var $examdate;  
  
   /**
   * @var string
   */
	var $doctor;//фамилия + имя доктора
    
	/**
   * @var int
   */
	var $docid;
	
   /**
   * @var int
   */
	var $afterflag;
   /**
   * @var string
   */
	var $course_fk;
   /**
   * @var string
   */
	var $course_date;
    
   /**
   * @var string
   */
	var $diseasehistory;
    
    /**
   * @var string
   */
	var $treatmentplan;
    
    /**
   * @var string
   */
	var $examplan;
    
    /**
    * @var NHDiagnosProtocol[]
    */
    var $selectedProtocols;
}

class NHDiagnosInspectionExamForDoc
{
   /**
   * @var string
   */
	var $name;
   /**
   * @var int
   */
	var $docid;  
 
    /**
   * @var string
   */
	var $spec; 
}

class NHDiagnosInspectionExamSOut //InspectionExamModuleDiagnosSOut
{
   /**
   * @var int
   **/
    var $birthyears;
   /**
   * @var string
   */
	var $fname;
   /**
   * @var string
   */
	var $lname;
   /**
   * @var string
   */
	var $kartnum; 
    /**
   * @var NHDiagnosInspectionExamForDoc[]
   */
	var $docList; 
   /**
   * @var NHDiagnosInspectionExamOut[]
   */
	var $InspectionOut;  
}


class NHDiagnosInspectionExamIn 
{
/**
   * @var string
   */
   var $nh_diagnos;
   /**
   * @var int
   */
   var $examid;
   /**
   * @var int
   */
	var $pat_id;
   /**
   * @var string
   */
	var $doc_id;
   /**
   * @var string
   */
   var $examdate;
   /**
   * @var int
   */
	var $afterflag;
   /**
   * @var string
   */
	var $course_fk;
    
   /**
   * @var string
   */
	var $diseasehistory;
    
   /**
   * @var string
   */
	var $treatmentplan;
    
    /**
   * @var string
   */
	var $examplan;
}



class NHDiagnosProtocol
{
   /**
   * @var string
   */
    var $diagnos2_id;
   /**
   * @var string
   */
    var $protocol_id;
   /**
   * @var string
   */
	var $protocol_name; 
    
   /**
   * @var NHDiagnosTemplate
   */
	var $selectedTemplate;
   /**
   * @var NHDiagnosTemplate[]
   */
	var $templates;
}
class NHDiagnosTemplate
{
   /**
   * @var string
   */
	var $template_id; 
   /**
   * @var string
   */
	var $template_name; 
}




class NHDiagnos_DairyRecord
{
   /**
   * @var string
   */
   var $id;
   /**
   * @var int
   */
   var $dairyType;
   /**
   * @var string
   */
   var $dairyText;
   /**
   * @var string
   */
   var $dairyResume;
   
   /**
   * @var string
   */
   var $doctor_id;
   /**
   * @var string
   */
   var $doctor_sname;
   
   /**
   * @var string
   */
   var $patient_id;
   
   /**
   * @var string
   */
   var $createDate;
   /**
   * @var string
   */
   var $dairyDate;
   
   
}


class NHDiagnos{
    /**
  *	@param NHDiagnosInspectionExamOut $p1
  *	@param NHDiagnosInspectionExamIn $p2
  *	@param NHDiagnosInspectionExamSOut $p3  
  *	@param NHDiagnosInspectionExamForDoc $p4
  * @param NHDiagnosProtocol $p5
  * @param NHDiagnosTemplate $p6
  * @param NHDiagnos_DairyRecord $p7
  * @return void
  */
	public function registertypes($p1, $p2, $p3, $p4, $p5, $p6, $p7) {}  
  
  
  /**
  * @param NHDiagnosInspectionExamIn $item
  * @param boolean $isUpdate
  * @param boolean $addAfterExam
  * @param NHDiagnosProtocol[] $protocols
  * @return boolean
  */
  
public function saveExam($item, $isUpdate, $addAfterExam, $protocols)
  {
    $connection = new Connection();
	$connection->EstablishDBConnection();
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    
    if($isUpdate == false)
    {
        //create treatmencourse 
        $QueryText = "insert into treatmentcourse(id, doctor_fk, patient_fk) values (null, ".$item->doc_id.", ".$item->pat_id.") RETURNING id";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        if($row = ibase_fetch_row($result))
        {
            $tcid=$row[0];
        }
         
        ibase_free_query($query);
        ibase_free_result($result);
        
        
        
        //create diagnos2 for general procedures
        $QueryText = "update or insert into DIAGNOS2(ID,TREATMENTCOURSE,DOCTOR,DIAGNOSDATE,STACKNUMBER,DIAGNOSLABEL) 
        values (null, $tcid,".$item->doc_id.",null,-1,'nHealth')";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
    	ibase_free_query($query);
        
        //save all diagnos2 for protocols
        if($protocols != null)
        {
            $i = 0;
            foreach ($protocols as $protocol)
            {
                //protocol_id
                //selectedTemplate->template_id
                 $QueryText = "update or insert into DIAGNOS2(ID, TREATMENTCOURSE, DOCTOR, STACKNUMBER, PROTOCOL_FK, TEMPLATE, DIAGNOSLABEL) 
                values (null, $tcid,".$item->doc_id.",".$i.", ".$protocol->protocol_id.", ".$protocol->selectedTemplate->template_id.", 'nHealth')";
                $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
            	ibase_free_query($query);
                $i++;
            }
        }
        
        
        //create toothexam with afterflag = 0
	    $QueryText = "insert into TOOTHEXAM(NH_DIAGNOS, ID, DOCTORID, EXAMDATE,  AFTERFLAG, TREATMENTCOURSE_FK, DISEASEHISTORY, TREATMENTPLAN, EXAMPLAN)
                values('".safequery($item->nh_diagnos)."', $item->pat_id, '".safequery($item->doc_id)."', '".safequery($item->examdate)."', 0, $tcid, '".safequery($item->diseasehistory)."', '".safequery($item->treatmentplan)."', '".safequery($item->examplan)."')"; 
    	$query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        
        if($addAfterExam)
        {
            //create toothexam with afterflag = 1
        	$QueryText = "insert into TOOTHEXAM(NH_DIAGNOS, ID, DOCTORID,  EXAMDATE,  AFTERFLAG,  TREATMENTCOURSE_FK, DISEASEHISTORY, TREATMENTPLAN, EXAMPLAN)
                     values('".safequery($item->nh_diagnos)."', $item->pat_id, '".safequery($item->doc_id)."', '".safequery($item->examdate)."', 1,  $tcid, '', '', '')"; 
					 $query = ibase_prepare($trans, $QueryText);
        	ibase_execute($query);
            ibase_free_query($query);
        }
    }
    else
        if(($item->examid != null)&&($item->examid != ''))
        {
           $QueryText = "update toothexam set
			NH_DIAGNOS = '".safequery($item->nh_diagnos)."',
			EXAMDATE = '".safequery($item->examdate)."',
			DOCTORID = '".safequery($item->doc_id)."'
                where examid = $item->examid";
        	$query = ibase_prepare($trans, $QueryText);
        	ibase_execute($query);
            ibase_free_query($query); 
            
            
            $QueryDelText = "delete from DIAGNOS2 where DIAGNOS2.STACKNUMBER > -1 and DIAGNOS2.TREATMENTCOURSE = ".$item->course_fk;
            
            //save all diagnos2 for protocols
            if($protocols != null)
            {
                foreach ($protocols as $protocol)
                {
                    if($protocol->diagnos2_id != null && $protocol->diagnos2_id != '')
                    {
                        $QueryDelText .= " and DIAGNOS2.ID <> ".$protocol->diagnos2_id;
                    }
                    else
                    {
                        $protocol->diagnos2_id = 'null';
                    }
                    $QueryText = "update or insert into DIAGNOS2(
                                                ID, 
                                                TREATMENTCOURSE, 
                                                DOCTOR, 
                                                PROTOCOL_FK, 
                                                TEMPLATE) 
                                    values (
                                                ".$protocol->diagnos2_id.", 
                                                ".$item->course_fk.",
                                                ".$item->doc_id.", 
                                                ".$protocol->protocol_id.", 
                                                ".$protocol->selectedTemplate->template_id.")";
                    
				    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($QueryText,true));
                    $query = ibase_prepare($trans, $QueryText);
                    ibase_execute($query);
                	ibase_free_query($query);
                }
            }
            
            
            $QueryDel = ibase_prepare($trans, $QueryDelText);
            ibase_execute($QueryDel);
            
            
            
        }

	$success = ibase_commit($trans);	
	$connection->CloseDBConnection();
	return $success;
  }
  
    
  /**
  * @param int $item
  * @param string $passForDelete
  * @return boolean
  */
  public function deleteExam($item, $passForDelete)
  { 
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
            	$QueryText = "delete from TOOTHEXAM where EXAMID = '$item'";
            	$query = ibase_prepare($trans, $QueryText);
            	ibase_execute($query);		
            	$success = ibase_commit($trans);
            	ibase_free_query($query);	
            	$connection->CloseDBConnection();
            	return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
  }
  
  /**
  * @param string $passForDelete
  * @return boolean
  */
  public function deleteCheck($passForDelete)
  { 
        $answer = false;
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForTreatDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        
                
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
            	$answer = true;
			}
		}
        
       	ibase_free_query($query);
       	ibase_commit($trans);	
		$connection->CloseDBConnection();
		return $answer;
  }
 
    /**
    * @param int $id
    * @param boolean $isLite
    * @return NHDiagnosInspectionExamSOut
    */    
    public function getExam($id, $isLite) //sqlite
    { 
    	$connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    	$QueryText = "select 
                        PATIENTS.FIRSTNAME, 
                        PATIENTS.LASTNAME, 
                        PATIENTS.CARDNUMBER, 
                        PATIENTS.CARDBEFORNUMBER,
                        PATIENTS.CARDAFTERNUMBER, 
                        TOOTHEXAM.EXAMDATE, 
                        TOOTHEXAM.EXAMID,
                        DOCTORS.LASTNAME || ' ' || DOCTORS.FIRSTNAME, 
                        datediff(year, patients.birthday, date 'now'),
                        toothexam.afterflag,
                        toothexam.treatmentcourse_fk,
                        treatmentcourse.createdate,                        
                        TOOTHEXAM.DISEASEHISTORY,
                        TOOTHEXAM.TREATMENTPLAN,
                        TOOTHEXAM.EXAMPLAN,					
			TOOTHEXAM.NH_DIAGNOS,
			TOOTHEXAM.DOCTORID
                   from PATIENTS 
                   left join TOOTHEXAM
                   on TOOTHEXAM.ID = PATIENTS.ID
                   left join treatmentcourse
                   on treatmentcourse.id = toothexam.treatmentcourse_fk
                   left join DOCTORS
                   on DOCTORS.ID = TOOTHEXAM.DOCTORID
                   where PATIENTS.ID = $id
                  order by treatmentcourse.createdate desc, toothexam.treatmentcourse_fk desc,  TOOTHEXAM.EXAMID desc";
    	$query = ibase_prepare($trans, $QueryText);
    	$result=ibase_execute($query);
        $retdata = new NHDiagnosInspectionExamSOut;
        while ($row = ibase_fetch_row($result))
    	{
			if($row[6] != null && $row[15] != null)
            {
				//echo($row[0]);
        	    $obj = new NHDiagnosInspectionExamOut;
                $obj->examdate = $row[5];
                $obj->examid = $row[6];
                $obj->doctor = $row[7];
                $obj->afterflag = $row[9];
                $obj->course_fk = $row[10];
                $obj->course_date = $row[11];
                $obj->diseasehistory = $row[12];
                $obj->treatmentplan = $row[13];
                $obj->examplan = $row[14];
				$obj->nh_diagnos = $row[15];
				$obj->docid = $row[16];
                $obj->selectedProtocols = NHDiagnos::getSelectedProtocolsByTreatmentId($obj->course_fk, $trans);
 		        $retdata->InspectionOut[] = $obj;
            }
            
            $retdata->fname = $row[0];
            $retdata->lname = $row[1];
            $retdata->kartnum = "{$row[3]}{$row[2]}{$row[4]}";
            $retdata->birthyears = $row[8];
    	}	
    	ibase_commit($trans);
    	ibase_free_query($query);
    	ibase_free_result($result);
        if($isLite==false)
        {
           	//**************** DOCTORS LIST START ****************
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        	$QueryText = "select ID, LASTNAME || ' ' || FIRSTNAME, SPECIALITY 
                            FROM DOCTORS 
                            WHERE doctors.isfired is null or doctors.isfired != 1";
        	$query = ibase_prepare($trans, $QueryText);
        	$result=ibase_execute($query);
         
            while ($row = ibase_fetch_row($result))
        	{
        	    $obj = new NHDiagnosInspectionExamForDoc;
                $obj->docid = $row[0];
                $obj->name = $row[1];
                $obj->spec=$row[2];
        		$retdata->docList[] = $obj;
        	}	
        	ibase_commit($trans);
        	ibase_free_query($query);
        	ibase_free_result($result);
           	//**************** DOCTORS LIST END ****************
		}
            
    	$connection->CloseDBConnection();		
    	return $retdata;       
    }

    
    /**
     * @param string $searchText
     * @param string[] $specialities
     * @return NHDiagnosProtocol[]
     */
    public function getLazyProtocols($searchText, $specialities)//sqlite
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $protocols = array();
            
            //protocols
            $sql_protocol = "select
                protocols.id,
                protocols.name
            from protocols
            where lower(protocols.name) containing lower('".safequery($searchText)."') order by protocols.name";
            
            $query_protocol = ibase_prepare($trans, $sql_protocol);
            $result_protocol = ibase_execute($query_protocol);
            while ($row = ibase_fetch_row($result_protocol))
            {
                $p = new NHDiagnosProtocol();
                $p->protocol_id = $row[0];
                $p->protocol_name = $row[1];
                $p->templates = array();
                $p->templates = NHDiagnos::getTemplatesByProtocolId($p->protocol_id, $trans);
                
                $protocols[] = $p;
            }
            
            //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($protocols,true));
            
            ibase_free_query($query_protocol);
            ibase_free_result($result_protocol);
            
            ibase_commit($trans);
            
            $connection->CloseDBConnection();
            
            return $protocols;
    }
        /**
     * @param string $protocolId
     * @param object $trans
     * @return NHDiagnosTemplate[]
     */
    public static function getTemplatesByProtocolId($protocolId, $trans = null)//sqlite
    {
         if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
            $templates = array();
            
            //templates
            $sql_templates = "select
                        template.id,
                        template.name
                    from template 
                    where template.protocol = $protocolId
                     order by template.name";
            
            $query_templates = ibase_prepare($trans, $sql_templates);
            $result_templates = ibase_execute($query_templates);
            while ($row = ibase_fetch_row($result_templates))
            {
                $t = new NHDiagnosTemplate();
                $t->template_id = $row[0];
                $t->template_name = $row[1];
                
                $templates[] = $t;
            }
            ibase_free_query($query_templates);
            ibase_free_result($result_templates);
            
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }	
        
        return $templates;
    }
    
      /**
     * @param string $treatId
     * @param object $trans
     * @return NHDiagnosProtocol[]
     */
    public static function getSelectedProtocolsByTreatmentId($treatId, $trans = null)//sqlite
    {
         if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
            $selectedProtocols = array();
            
            //templates
            $sql = "select
                    diagnos2.id,
                    diagnos2.protocol_fk,
                    (select protocols.name from protocols where protocols.id = diagnos2.protocol_fk),
                    diagnos2.template,
                    (select template.name from template where template.id = diagnos2.template)
                    
                    from diagnos2
                    
                    where diagnos2.treatmentcourse = ".$treatId."
                    and diagnos2.protocol_fk is not null
                    and diagnos2.template is not null";
            
            $query = ibase_prepare($trans, $sql);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $p = new NHDiagnosProtocol();
                $p->diagnos2_id = $row[0];
                $p->protocol_id = $row[1];
                $p->protocol_name = $row[2];
                
                $t = new NHDiagnosTemplate();
                $t->template_id = $row[3];
                $t->template_name = $row[4];
                
                $p->selectedTemplate = $t;
                
                $selectedProtocols[] = $p;
            }
            ibase_free_query($query);
            ibase_free_result($result);
            
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }	
        
        return $selectedProtocols;
    }
    
    
    
   //**************** DAIRY FUNCTIONS START ****************
   /** 
 	* @param string $patientId
 	* @param boolean $isFullHistory
	* @return NHDiagnos_DairyRecord[]
	*/    
    public function getDairyByPatientId($patientId, $isFullHistory) 
    { 
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $resultArray = array();
        
		$sql = "select ";
        if($isFullHistory)
        {
            $sql .= " first 100 ";
        }

        $sql .= " ID,
                              DAIRYTYPE,
                              DAIRYTEXT,
                              DAIRYRESUME,
                              FK_DOCTOR,
                              (select doctors.shortname from doctors where doctors.id = nh_dairy.fk_doctor),
                              FK_PATIENT,
                              CREATEDATE,
                              DAIRYDATE

                          from NH_DAIRY

                          where nh_dairy.fk_patient = $patientId
                          
                          order by nh_dairy.dairydate desc";
                                			
		$query = ibase_prepare($trans, $sql);
		$result = ibase_execute($query);
        	
		while ($row = ibase_fetch_row($result))
		{
			$obj = new NHDiagnos_DairyRecord;
			$obj->id = $row[0];
			$obj->dairyType = $row[1];
			$obj->dairyText = text_blob_encode($row[2]);
			$obj->dairyResume = text_blob_encode($row[3]);
			$obj->doctor_id = $row[4];
			$obj->doctor_sname = $row[5];
            $obj->patient_id = $row[6];
            $obj->createDate = $row[7];
            $obj->dairyDate = $row[8];
			$resultArray[] = $obj;
		}	
		ibase_free_query($query);
		ibase_free_result($result);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();		
		return $resultArray; 
    }
    
   /**
   * @param NHDiagnos_DairyRecord $data
   * @return NHDiagnos_DairyRecord
   */
   public function saveDairy($data)
   {
        $connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if($data->id == null || $data->id == '')
        {
            $data->id = "null";
        }
        if($data->doctor_id == null && $data->doctor_id == '')
        {
            $data->doctor_id = "null";
        }
        
        $save_sql = " update or insert into NH_DAIRY (
                                    ID, 
                                    DAIRYTYPE, 
                                    DAIRYTEXT, 
                                    DAIRYRESUME, 
                                    FK_DOCTOR, 
                                    FK_PATIENT, 
                                    DAIRYDATE
                                    )
                        values 
                                (
                                ".$data->id.",
                                ".$data->dairyType.", 
                                '".safequery($data->dairyText)."', 
                                '".safequery($data->dairyResume)."', 
                                ".$data->doctor_id.", 
                                ".$data->patient_id.",
                                '".$data->dairyDate."' 
                                )
                         matching (ID) returning (ID)";
        
		if(NHUltra::$isDebugLog)
		{
				file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($save_sql,true));
		}
            
		$save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        $rowID = ibase_fetch_row($result_query);
        $data->id = $rowID[0];
        ibase_free_query($save_query);
        ibase_free_result($result_query);
        
    	ibase_commit($trans);	
    	$connection->CloseDBConnection();
    	return $data;
   }
   
   /**
   * @param string $id
   * @return boolean
   */
   public function removeDairy($id)
   {     	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from NH_DAIRY where ID=$id";		
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);		
		ibase_free_query($query);	
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
    }
    
   //**************** DAIRY FUNCTIONS END ****************
 
}

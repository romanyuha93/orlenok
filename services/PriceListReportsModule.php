<?php
require_once (dirname(__FILE__).'/tbs/tbs_class.php');
require_once (dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
require_once "Connection.php";
require_once "Utils.php";
require_once "PriceListModule.php";

//$p = new PriceListReportsModule();
//$p->createReport("docx", true);
//define('tfClientId','online');


class PriceListReportsModule
{
   
    
    private function fillPriceListArrayRecursive($PriceRecords)
    {
        $PriceRecordsVisualArray = array();
        
        foreach($PriceRecords as $PriceRecord) 
        {
            //echo($PriceRecord->CAPTIONLEVEL." --> ". $PriceRecord->PLANNAME ."<br/>");
            $PriceRecordsVisualArray[] = $PriceRecord;
            $Children = $PriceRecord->children;
            if($Children != null )
            {
                $ChildrenArray = $this->fillPriceListArrayRecursive($Children);
                foreach($ChildrenArray as $Child ){
                    $PriceRecordsVisualArray[] = $Child;
                }
            }
        }
        return $PriceRecordsVisualArray;
    }
    
    
    
    private function getClinicRegistrationData()  
    {
        // needed for calling function "getPriceListRecordsRecursive"
        $priceListModule = new PriceListModule(); 
        
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = 
            "select 
                NAME,
                ADDRESS,
                CODE,
                TELEPHONENUMBER,
                DIRECTOR,
                CHIEFACCOUNTANT,
                LOGO
            from CLINICREGISTRATIONDATA";
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $row = ibase_fetch_row($result);
         
        $obj = new stdclass();
        $obj->ESTABLISHMENTNAME = $row[0];    
        $obj->ADDRESS = $row[1];    
        $obj->CODE = $row[2];        
        $obj->TELEPHONENUMBER = $row[3];    
        $obj->DIRECTOR = $row[4];
        $obj->CHIEFACCOUNTANT = $row[5];
        
        $imageBlob = null;
        $blob_info = ibase_blob_info($row[6]);
        if ($blob_info[0]!=0){
            $blob_hndl = ibase_blob_open($row[6]);
            $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
            ibase_blob_close($blob_hndl);
        }
        $obj->LOGO = base64_encode($imageBlob);
        
        ibase_free_query($query);
        ibase_free_result($result);
        
        
        $outData = new stdclass();
        $functionResult = $priceListModule->getPriceListRecordsRecursive($trans, "null", 1,null,null,null,null);    
        $outData->PriceRecords = $functionResult;
        $outData->ClinicRegistrationData = $obj;
        
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $outData;
    }
    
    
    
    
    
    /** 
  * @param string $file_name
  * @return boolean
  */ 
    public function deleteFile($file_name)
    {
        return deleteFileUtils($file_name);
        
    }
    
    /** 
    * docx,odt
    * @param string $docType
    * @param boolean $isCoursename
    * @param int $priceIndex
    * @return string
    */  
    public function createReport($docType, $isCoursename, $priceIndex)
    {
        $res=dirname(__FILE__).'/tbs/res/';
        
        if(($docType == 'docx') && (!file_exists($res.'PriceList.docx') ) )
        {
            return "TEMPLATE_NOTFOUND";
        }
        else if(($docType == 'odt') && (!file_exists($res.'PriceList.odt') ) )
        {
            return "TEMPLATE_NOTFOUND";
        }
        
        if( ($docType != 'docx' && $docType != 'odt' )  ){
            return "TEMPLATE_NOTFOUND";  // введен неизвестный формат
        }
        
        
        $priceListModulePrintData = $this->getClinicRegistrationData();
       
        $CRD = $priceListModulePrintData->ClinicRegistrationData;
        
        $ClinicRegistrationData = new stdclass();
        $ClinicRegistrationData->ClinicAddress = $CRD->ADDRESS;
        $ClinicRegistrationData->Director = $CRD->DIRECTOR;
        $ClinicRegistrationData->ChiefAccountant = $CRD->CHIEFACCOUNTANT;
        $ClinicRegistrationData->EstablishmentName = $CRD->ESTABLISHMENTNAME;
        $ClinicRegistrationData->ShortDate = date ('d/m/Y', time());
        
        if($CRD->LOGO != null){
            file_put_contents(dirname(__FILE__).'/docs/logo.bmp', base64_decode($CRD->LOGO) );
            $ClinicRegistrationData->Logo = '<img src="'.dirname(__FILE__).'/docs/logo.bmp">';
            $ClinicRegistrationData->LogoPath = dirname(__FILE__).'/docs/logo.bmp';
        }else{
            $ClinicRegistrationData->Logo = '';
            $ClinicRegistrationData->LogoPath = dirname(__FILE__).'/tbs/res/void.png';
        }
        
        
        $PriceRecords = $priceListModulePrintData->PriceRecords;
        
        
        // recursive adding all PriceRecords to Array "PriceRecordsArray"
        $PriceRecordsArray = $this->fillPriceListArrayRecursive($PriceRecords);
        
        $PriceRecordsVisualArray = array();
        for($i=0; $i < count($PriceRecordsArray); $i++ )
        {
            $PriceRecord = $PriceRecordsArray[$i];
            //echo($PriceRecord->UID." --> ".$PriceRecord->CAPTIONLEVEL." --> ". $PriceRecord->PLANNAME ."<br/>");
            $PriceRecord = $PriceRecordsArray[$i];
            
            // for encoding symbols like & < > in PLANNAME
            if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
            {
                $PriceRecord->PLANNAME = str_replace('&', '&amp;', $PriceRecord->NAME);
                $PriceRecord->PLANNAME = str_replace('>', '&gt;', $PriceRecord->NAME);
                $PriceRecord->PLANNAME = str_replace('<', '&lt;', $PriceRecord->NAME);
            }
            else
            {
                $PriceRecord->PLANNAME = str_replace('&', '&amp;', $PriceRecord->PLANNAME);
                $PriceRecord->PLANNAME = str_replace('>', '&gt;', $PriceRecord->PLANNAME);
                $PriceRecord->PLANNAME = str_replace('<', '&lt;', $PriceRecord->PLANNAME);
            }
            $PriceRecordVisual = new stdclass();
            
            
            if($docType=='docx')
            {
                
                if($PriceRecord->CAPTIONLEVEL > 0 ){
                    //$PriceRecordVisual->MiddleCellTextAlign = "center";
                    if($PriceRecord->CAPTIONLEVEL == 1){
                    
                        $PriceRecordVisual->PlanName = 
                            '<w:p w:rsidR="000C69F8" w:rsidRPr="000C69F8" w:rsidRDefault="000C69F8" w:rsidP="000C69F8">'.
                                '<w:pPr>'.
                                    '<w:jc w:val="center"/>'.
                                '</w:pPr>'.
                                '<w:r w:rsidRPr="000C69F8">'.
                                    '<w:rPr>'.
                                        '<w:b/>'.
                                        '<w:sz w:val="28"/>'.
                                        //'<w:szCs w:val="28"/>'.
                                        //'<w:lang w:val="en-US"/>'.
                                    '</w:rPr><w:t>';
                        if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->NAME;
                        }
                        else
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->PLANNAME;
                        }
                        $PriceRecordVisual->PlanName .= '</w:t></w:r>'. 
                                                        '</w:p>';
                    }else{
                        $PriceRecordVisual->PlanName = 
                            '<w:p w:rsidR="000C69F8" w:rsidRPr="000C69F8" w:rsidRDefault="000C69F8" w:rsidP="000C69F8">'.
                                '<w:pPr>'.
                                    '<w:jc w:val="center"/>'.
                                '</w:pPr>'.
                                '<w:r w:rsidRPr="000C69F8">'.
                                    '<w:rPr>'.
                                        '<w:b/>'.
                                        '<w:sz w:val="18"/>'.
                                        //'<w:szCs w:val="18"/>'.
                                        //'<w:lang w:val="en-US"/>'.
                                    '</w:rPr><w:t>';
                        if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->NAME;
                        }
                        else
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->PLANNAME;
                        }
                         $PriceRecordVisual->PlanName .= '</w:t></w:r>'. 
                                                         '</w:p>';
                    }
                }else{
                    //$PriceRecordVisual->MiddleCellTextAlign = "left";
                    if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
                    {
                        $PriceRecordVisual->PlanName = $PriceRecord->NAME;
                    }
                    else
                    {
                        $PriceRecordVisual->PlanName = $PriceRecord->PLANNAME;
                    }
                }
                
            }
            if($docType == 'odt')
            {
            
                if($PriceRecord->CAPTIONLEVEL > 0 ){
                    //$PriceRecordVisual->MiddleCellTextAlign = "center";
                    if($PriceRecord->CAPTIONLEVEL == 1){
                        $PriceRecordVisual->PlanName = 
                            '<text:span text:style-name="T_block_none"/>'.
                            ''.
                            '</text:p><text:p text:style-name="P_CENTER"> <text:span text:style-name="T_bold_12">';
                        if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->NAME;
                        }
                        else
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->PLANNAME;
                        }
                        $PriceRecordVisual->PlanName .= '</text:span></text:p> <text:p><text:span text:style-name="T_block_none"/>';
                            
                            
                    }else{
                        $PriceRecordVisual->PlanName = 
                            '<text:span text:style-name="T_block_none"/>'.
                            '</text:p><text:p text:style-name="P_CENTER"> <text:span text:style-name="T_bold_8">';
                        if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->NAME;
                        }
                        else
                        {
                            $PriceRecordVisual->PlanName .= $PriceRecord->PLANNAME;
                        }
                        $PriceRecordVisual->PlanName .= '</text:span></text:p> <text:p><text:span text:style-name="T_block_none"/>';
                    }
                }else{
                    //$PriceRecordVisual->MiddleCellTextAlign = "left";
                    if(($isCoursename == true)&&($PriceRecord->NODETYPE != 0))
                    {
                        $PriceRecordVisual->PlanName = $PriceRecord->NAME;
                    }
                    else
                    {
                        $PriceRecordVisual->PlanName = $PriceRecord->PLANNAME;
                    }
                }
            
            }
            
            $PriceRecordVisual->CaptionLevel = $PriceRecord->CAPTIONLEVEL;
            $PriceRecordVisual->Shifr = $PriceRecord->SHIFR;
            
            if($PriceRecord->CAPTIONLEVEL == 0){
                if($priceIndex == 2)
                {
                    $PriceRecordVisual->Price = $PriceRecord->PRICE2;
                }
                else if($priceIndex == 3)
                {
                    $PriceRecordVisual->Price = $PriceRecord->PRICE3;
                }
                else
                {
                    $PriceRecordVisual->Price = $PriceRecord->PRICE;
                }
            }else{
                $PriceRecordVisual->Price = "";
            }
            
            $PriceRecordsVisualArray[] = $PriceRecordVisual;
            
        }
        
        
        $TBS = new clsTinyButStrong();
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        
        
        if($docType == 'odt')
        {
            //TODO openoffice
            $TBS->LoadTemplate($res.'PriceList.odt', OPENTBS_ALREADY_XML);
            $src = $TBS->Source;
            //$pos = strrpos($src, "<office:automatic-styles>");
            $xmlInjStr = '<office:automatic-styles>'.
            '<style:style style:name="T_bold_12" style:family="text"><style:text-properties fo:font-size="12pt" fo:font-weight="bold" style:font-size-asian="12pt" style:font-weight-asian="bold" style:font-size-complex="12pt" style:font-weight-complex="bold"/></style:style>'.
            '<style:style style:name="T_bold_8" style:family="text"><style:text-properties fo:font-size="8pt" fo:font-weight="bold" style:font-size-asian="8pt" style:font-weight-asian="bold" style:font-size-complex="8pt" style:font-weight-complex="bold"/></style:style>'.
            '<style:style style:name="T_block_none" style:family="text"><style:text-properties fo:font-size="2pt" fo:font-weight="bold" style:font-size-asian="2pt" style:font-weight-asian="bold" style:font-size-complex="2pt" style:font-weight-complex="bold"/></style:style>'.
            '<style:style style:name="P_CENTER" style:family="paragraph" style:parent-style-name="Table_20_Contents">
            <style:paragraph-properties fo:text-align="center" style:justify-single-word="false"/>
                <style:text-properties style:font-name="Times New Roman" fo:font-size="12pt" fo:language="uk" fo:country="UA" fo:font-weight="bold" style:font-size-asian="12pt" style:font-weight-asian="bold" style:font-size-complex="12pt" style:font-weight-complex="bold"/>
            </style:style>';
            $TBS->Source = str_replace('<office:automatic-styles>', $xmlInjStr, $src);
        }
        else if($docType == 'docx')
        {
            $TBS->LoadTemplate($res.'PriceList.docx', OPENTBS_ALREADY_XML);
        }
        
        
        
        $TBS->MergeField("ClinicRegistrationData", $ClinicRegistrationData);
        $TBS->MergeBlock("PriceRecord", $PriceRecordsVisualArray);
        
        
        $date = date("d-m-Y_H-i-s", time());
        $file_name = '_PriceList__' . $date;
        $file_name = preg_replace('/[^A-Za-z0-9_-]/', '', $file_name);
        $file_name = uniqid().$file_name.'.'.$docType;
        
        if($docType == 'docx' || $docType == 'odt'){
            $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$file_name );
        }
        
       
        return $file_name;
    }

}




?>
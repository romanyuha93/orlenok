<?php
	require_once "Connection.php";
	require_once "Settings.php";
	require_once "Utils.php";
	require_once "AdminService.php";
	class ServiceModule
	{
		const GLIC = '3328302631';
		/**
		* @param string $login
		* @param string $pass
		* @return string[]
		*/
		public function check($login, $pass)
		{
			$successLogin = false;
			$successPass = false;
			$authobj=AdminService::checkAutorization($login, $pass);
			if(nonull($authobj->ACCOUNT_KEY))
			{
				$successLogin = true;
				$successPass = true;
			}
			$rows = array();
			if($successLogin && $successPass)
				{
				AdminService::syncAndClearJornal();
				$_con = new Connection();
				$_con->EstablishDBConnection("main",false);
				$_trans = ibase_trans(IBASE_DEFAULT, $_con->connection);
				$logo_sql ="select clinicregistrationdata.logo from clinicregistrationdata";
				$logo_query = ibase_prepare($_trans, $logo_sql);
				$logo_result = ibase_execute($logo_query);
				$row_logo = ibase_fetch_row($logo_result);
				$imageBlob = null;
				$blob_info = ibase_blob_info($row_logo[0]);
				if ($blob_info[0] != 0)
				{
					$blob_hndl = ibase_blob_open($row_logo[0]);
					$imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
					ibase_blob_close($blob_hndl);
				}
				ibase_commit($_trans);
				ibase_free_query($logo_query);
				ibase_free_result($logo_result);
				$_con->CloseDBConnection();
				$authobj->LOGO=base64_encode($imageBlob);
			}
			else if($successLogin)
			{
				$authobj->LOGO = "LOGIN_FAULT";
			}
			else if($successPass)
			{
				$authobj->LOGO = "PASS_FAULT";
			}
			else
			{
				$authobj->LOGO = "LOGINPASS_FAULT";
			}
			return $authobj;
		}
		/**
		* @return string[]
		*/
		public function check_hd()
		{
			$_con = new Connection();
			$_con->EstablishDBConnection("main",false);
			$_trans = ibase_trans(IBASE_DEFAULT, $_con->connection);
			$db_version_sql = "select PARTVERSION from APPLICATIONVERSION where PARTNAME = 'DATABASE'";
			$db_version_sql2 = "execute procedure xxyyzz('97.114.109.124.49.48.124.100.105.99.111.109.124.50.124.100.105.114.124.51.124.114.101.103.124.51.124.35.103.107.101.97.108.107.113.120.115.107.105.112.116.".
				"118.115.116.117.113.115.111.120.110.122.119.116.101.110.49.53.49.118.73.109.88.50.49.51.84.108.79.67.76.84.78.50.49.53.53.122.86.73.75.73.95.".
				"84.82.73.65.76.45.55.97.50.100.55.52.54.102.98.101.55.97.102.55.50.51.57.49.101.51.102.51.50.101.53.101.52.97.57.100.100.55.124.49.48.".
				"124.97.114.109.126.120.118.102.100.97.117.108.102.119.118.116.112.118.113.110.120.116.117.111.113.113.120.122.120.112.79.99.49.53.49.65.84.113.104.50.49.".
				"51.107.105.88.75.76.80.114.50.49.53.53.122.86.73.75.73.95.84.82.73.65.76.45.97.51.101.54.49.101.51.57.99.48.98.51.101.102.51.49.52.".
				"54.102.51.102.98.102.99.100.55.54.49.48.49.49.100.124.50.124.100.105.99.111.109.126.115.98.98.106.106.105.115.97.98.101.108.106.104.108.108.106.101.".
				"99.97.113.119.112.122.110.117.118.71.49.53.49.120.106.109.65.50.49.51.88.73.82.68.118.77.70.50.49.53.53.122.86.73.75.73.95.84.82.73.65.".
				"76.45.52.55.54.48.55.101.50.48.55.49.101.101.101.97.49.56.101.54.56.48.55.100.53.57.49.57.51.98.102.98.53.100.124.51.124.100.105.114.126.".
				"116.97.108.101.107.108.110.104.115.97.108.102.108.105.111.115.113.117.112.114.116.114.122.112.84.118.106.49.53.49.68.105.99.97.50.49.51.86.66.65.120.".
				"101.74.114.50.49.53.53.122.86.73.75.73.95.84.82.73.65.76.45.48.52.57.100.99.52.55.102.49.101.53.50.98.101.98.52.97.54.52.49.50.48.".
				"49.102.97.98.54.56.100.51.52.50.124.51.124.114.101.103')";
			$db_version_query2 = ibase_prepare($_trans, $db_version_sql2);
			ibase_execute($db_version_query2);
			$db_version_query = ibase_prepare($_trans, $db_version_sql);
			$db_version_result = ibase_execute($db_version_query);
			$db_version_row = ibase_fetch_row($db_version_result);
			ibase_commit($_trans);
			ibase_free_query($db_version_query);
			ibase_free_result($db_version_result);
			$_con->CloseDBConnection();
			$rows = array();
			$rows[] = $db_version_row[0];
			$rows[] = Settings::SERVICES_VERSION_STRING;
			$rows[] = Settings::SMS_COST;
			$rows[] = Settings::LOCATION;
			$rows[] = Settings::SYNC_URL;
			$rows[] = "98916916881016179zHWh580WdO161dWPolHk6592zVIKI_TRIAL-fba19ec95851a233337b8b6cdc3dbc2c";//android2_m1
			$rows[] = "908108101617182531zkAo151keB213yumLvil2165zVIKI_TRIAL-1605d7f2b754747b36a6c04b1721b019";//trial
			$rows[] = "1798910168161791010zmPy148RFG325XGSmyAy9884zVIKI_TRIAL-c9409137b7c99bdb47a171404eef859c";//trial_iphone
			return $rows;
		}
	}
?>
<?php
require_once "Connection.php";
require_once "Utils.php";

class HRData_HRModule
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $employee_id;
    /**
    * @var string
    */
    var $employee_lname;
    /**
    * @var string
    */
    var $employee_fname;
    /**
    * @var string
    */
    var $employee_sname;
    /**
    * @var string
    */
    var $employee_code;
    
    /**
    * @var string
    */
    var $hrdata_id;
    /**
    * @var int
    */
    var $hrdata_type;
    /**
    * @var string
    */
    var $hrdata_time;
}

class HREmployee_HRModule
{ 
   /**
   * @var string
   */
	var $employee_id;
   /**
   * @var string
   */
	var $employee_fname; 
   /**
   * @var string
   */
	var $employee_lname; 
   /**
   * @var string
   */
	var $employee_sname;
   /**
   * @var string
   */
	var $employee_fio;
   /**
   * @var string
   */
	var $employee_label;
   /**
   * // assist, doc
   * @var string
   */
	var $employee_type;
}


class HRModule
{
       
    /**
     * @param HRData_HRModule $p1
     * @param HREmployee_HRModule $p2
     */
    public function registertypes($p1, $p2)
    {}
    
    /** 
 	* @param string $startDate
 	* @param string $endDate
	* @return HRData_HRModule[]
	*/    
    public function getHRData($startDate, $endDate) 
    { 
    	$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$hrdata_sql = "select
                            assistants.id,
                            assistants.lastname,
                            assistants.firstname,
                            assistants.middlename,
                            assistants.barcode,
                            
                            doctors.id,
                            doctors.lastname,
                            doctors.firstname,
                            doctors.middlename,
                            doctors.barcode,
                            
                            worktable.id,
                            worktable.pointtype,
                            worktable.pointtime
                            
                            from worktable
                            left join assistants
                            on worktable.assistant_fk = assistants.id
                            left join doctors
                            on worktable.doctor_fk = doctors.id
                            
                            where worktable.pointtime  >= '$startDate 00:00:00'
                                and worktable.pointtime <= '$endDate 23:59:59'
                                
                            order by worktable.pointtime desc";
                                			
		$hrdata_query = ibase_prepare($trans, $hrdata_sql);
		$hrdata_result = ibase_execute($hrdata_query);
		$rows = array();		
		while ($row = ibase_fetch_row($hrdata_result))
		{
			$obj = new HRData_HRModule;
            if($row[0])
            {
    			$obj->employee_id = $row[0];
    			$obj->employee_lname = $row[1];
    			$obj->employee_fname = $row[2];
    			$obj->employee_sname = $row[3];
    			$obj->employee_code = $row[4]; 
            }
            else
            {
    			$obj->employee_id = $row[5];
    			$obj->employee_lname = $row[6];
    			$obj->employee_fname = $row[7];
    			$obj->employee_sname = $row[8];
    			$obj->employee_code = $row[9]; 
            }
            
			$obj->hrdata_id = $row[10];
			$obj->hrdata_type = $row[11];
			$obj->hrdata_time = $row[12];
			$rows[] = $obj;
		}	
		ibase_commit($trans);
		ibase_free_query($hrdata_query);
		ibase_free_result($hrdata_result);
		$connection->CloseDBConnection();		
		return $rows; 
    }
    
    /** 
 	* @param string $barcode
 	* @param string $empid
	* @return string
	*/    
    public function fixHRData($barcode, $empid) 
    {
        $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $employee_type = 'assist';
		$obj = new HRData_HRModule;  
        	
        // select assistant + last hrdata
		$ass_sql = "select first 1
                            assistants.id,
                            assistants.lastname,
                            assistants.firstname,
                            assistants.middlename,
                            assistants.barcode,
                            
                            worktable.id,
                            worktable.pointtype,
                            worktable.pointtime
                            
                            from assistants
                                left join worktable
                            on worktable.assistant_fk = assistants.id
                            where assistants.isfired != 1";
        if($empid == '-')
        {
            $ass_sql .= "and assistants.barcode = '$barcode' ";
        }
        else if($barcode == 'assist')
        {
            $ass_sql .= "and assistants.id = '$empid' ";
        }        
        else
        {
            $ass_sql .= "and assistants.id is null ";
        }      
        $ass_sql .= "order by worktable.pointtime desc";
                              			
		$ass_query = ibase_prepare($trans, $ass_sql);
		$ass_result = ibase_execute($ass_query);
        $row = ibase_fetch_row($ass_result);
        if($row[0])
		{
			$obj->employee_id = $row[0];
			$obj->employee_lname = $row[1];
			$obj->employee_fname = $row[2];
			$obj->employee_sname = $row[3];
			$obj->employee_code = $row[4]; 
            
			$obj->hrdata_id = $row[5];
			$obj->hrdata_type = $row[6];
			$obj->hrdata_time = $row[7];
            
    		ibase_free_query($ass_query);
    		ibase_free_result($ass_result);
		}	
        else // select doctor + last hrdata
        {
		  $doc_sql = "select first 1
                            doctors.id,
                            doctors.lastname,
                            doctors.firstname,
                            doctors.middlename,
                            doctors.barcode,
                            
                            worktable.id,
                            worktable.pointtype,
                            worktable.pointtime
                            
                            from doctors
                                left join worktable
                            on worktable.doctor_fk = doctors.id 
                            where doctors.isfired != 1";
                if($empid == '-')
                {
                    $doc_sql .= "and doctors.barcode = '$barcode' ";
                }
                else if($barcode == 'doc')
                {
                    $doc_sql .= "and doctors.id = '$empid' ";
                }         
                else
                {
                    $doc_sql .= "and doctors.id is null ";
                }     
            $doc_sql .= "order by worktable.pointtime desc";
                            
    		$doc_query = ibase_prepare($trans, $doc_sql);
    		$doc_result = ibase_execute($doc_query);
            if($row = ibase_fetch_row($doc_result))
            {
                $employee_type = 'doc';
    			$obj->employee_id = $row[0];
    			$obj->employee_lname = $row[1];
    			$obj->employee_fname = $row[2];
    			$obj->employee_sname = $row[3];
    			$obj->employee_code = $row[4]; 
                
    			$obj->hrdata_id = $row[5];
    			$obj->hrdata_type = $row[6];
    			$obj->hrdata_time = $row[7];
                
        		ibase_free_query($doc_query);
        		ibase_free_result($doc_result);
            }
        }
        
        // save new hr data
        if($obj->employee_id)
        {
            $addhr_sql = "insert into 
                worktable(pointtype, assistant_fk, doctor_fk)
                values
                (";
              if(($obj->hrdata_type == 1)||($obj->hrdata_type == '1'))
              {
                $addhr_sql .= '-1,';
              }
              else
              {
                $addhr_sql .= '1,';
              }
              if($employee_type == 'doc')
              {
                $addhr_sql .= 'null,'.$obj->employee_id;
              }
              else
              {
                $addhr_sql .= $obj->employee_id.',null';
              }
            $addhr_sql .= ')';
            
    		$addhr_query = ibase_prepare($trans, $addhr_sql);
    		ibase_execute($addhr_query);
            
    		ibase_commit($trans);
    		$connection->CloseDBConnection();
            return 'ok';
        }
        else
        {
            return 'notexist';
        }
        
    }
    
    
   /**
   * @param string $fio
   * @return HREmployee_HRModule[]
   */
   public function getEmployeeLazyList($fio)//sqlite
   {
    $connection = new Connection();
        $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $rows = array();
        // assistants
		$employee_sql = "select assistants.id,
                assistants.firstname,  
                assistants.middlename,
                assistants.lastname,
                assistants.fullname,
                assistants.mobilephone
                
                 from assistants
                
                where
                   ( lower( assistants.lastname||' '||assistants.firstname||' '||assistants.middlename) starting lower('$fio')
                     or lower(assistants.mobilephone) containing lower('$fio') )
                     and isfired != 1 ";
                    
        $employee_query = ibase_prepare($trans, $employee_sql);
        $employee_result = ibase_execute($employee_query);
		while ($row = ibase_fetch_row($employee_result))
		{ 
            $obj = new HREmployee_HRModule;
            $obj->employee_id = $row[0];
            $obj->employee_fname = $row[1];
            $obj->employee_sname = $row[2];
            $obj->employee_lname = $row[3];
            $obj->employee_fio = $row[3].' '.$row[1].' '.$row[2];
            $obj->employee_label = 'A: '.$obj->employee_fio;
            $obj->employee_type = 'assist';
            //$obj->patient_label .= $row[6];
            $rows[] = $obj;
		}
        ibase_free_query($employee_query);
        ibase_free_result($employee_result);
         // doctors
		$employee_sql = "select doctors.id,
                doctors.firstname,  
                doctors.middlename,
                doctors.lastname,
                doctors.fullname,
                doctors.mobilephone
                
                 from doctors
                
                where
                   ( lower( doctors.lastname||' '||doctors.firstname||' '||doctors.middlename) starting lower('$fio')
                     or lower(doctors.mobilephone) containing lower('$fio') ) 
                     and isfired != 1";
                    
        $employee_query = ibase_prepare($trans, $employee_sql);
        $employee_result = ibase_execute($employee_query);
		while ($row = ibase_fetch_row($employee_result))
		{ 
            $obj = new HREmployee_HRModule;
            $obj->employee_id = $row[0];
            $obj->employee_fname = $row[1];
            $obj->employee_sname = $row[2];
            $obj->employee_lname = $row[3];
            $obj->employee_fio = $row[3].' '.$row[1].' '.$row[2];
            $obj->employee_label = 'D: '.$obj->employee_fio;
            $obj->employee_type = 'doc';
            //$obj->patient_label .= $row[6];
            $rows[] = $obj;
		}
        ibase_free_query($employee_query);
        ibase_free_result($employee_result);
        
        
        ibase_commit($trans);
        $connection->CloseDBConnection();		
        return $rows;             
   }
   
   /**
	* @param string $id
	* @param string $passForDelete
	* @return boolean
 	*/ 		 		
	public function deleteHRDataById($id, $passForDelete) 
	{           
		$connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select users.passwrd from users where users.name = 'passForDel'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
		if($row = ibase_fetch_row($result))
		{
			if($row[0] == $passForDelete)
			{
               ibase_free_result($result);
        	   ibase_free_query($query);
               
       	       $QueryText = "delete from worktable where id = $id";		
               
        	   $query = ibase_prepare($trans, $QueryText);
        	   ibase_execute($query);
        	   ibase_free_query($query);
 		       
        	   $success = ibase_commit($trans);
        	   $connection->CloseDBConnection();
        	   return $success;
			}
		}
		ibase_free_result($result);
		ibase_free_query($query);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		return false;
	}
    
    
    /**
	* @param string $employee_id
	* @param string $employee_type
	* @param string $employee_barcode
	* @return boolean
     */
    public function setEmployeeBarcode($employee_id, $employee_type, $employee_barcode) 
	{ 
	    $connection = new Connection();
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        
        // CHECK BARCODE START
        

        $barcodecheck_sql = "select

            (select count(*) from doctors where doctors.barcode = '$employee_barcode') as doc_count,
            (select count(*) from assistants where assistants.barcode = '$employee_barcode') as ast_count ";
        $barcodecheck_sql .= 'from RDB$DATABASE';   
        $query_barcodecheck = ibase_prepare($trans, $barcodecheck_sql);
        $result_barcodecheck=ibase_execute($query_barcodecheck);
        $barcodecheck_row = ibase_fetch_row($result_barcodecheck);
        if($barcodecheck_row[0] != 0 || $barcodecheck_row[1] != 0)
        {
            return false;
        }
        // CHECK BARCODE END
        
        
        
        if($employee_type == 'DOCTOR')
        {
            $employee_sql = "update DOCTORS set ";
        }
        else if($employee_type == 'ASSISTANT')
        {
            $employee_sql = "update ASSISTANTS set ";
        }
        else
        {
            return false;
        }
        $employee_sql .= "BARCODE='".safequery($employee_barcode)."'
                        where id = $employee_id";	
		$employee_query = ibase_prepare($trans, $employee_sql);
		ibase_execute($employee_query);		
		$success = ibase_commit($trans);
		ibase_free_query($employee_query);	
		$connection->CloseDBConnection();	
		return $success;		
        
        
        /*
        
        select

                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 0
                                            and accountflow.summ > 0
                                            and accountflow.is_cashpayment = 0
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_income,
                                
                                (select coalesce(sum(accountflow.summ),0) from accountflow
                                        where accountflow.operationtype = 0
                                            and accountflow.summ > 0
                                            and accountflow.is_cashpayment = 1
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_income,
                                
                                (select coalesce(-sum(accountflow.summ),0) from accountflow
                                            where accountflow.operationtype = 0
                                                and accountflow.summ < 0
                                                and accountflow.is_cashpayment = 0
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as cash_expense,
                                
                                (select coalesce(-sum(accountflow.summ),0) from accountflow
                                            where accountflow.operationtype = 0
                                                and accountflow.summ < 0
                                                and accountflow.is_cashpayment = 1
                                            and accountflow.operationtimestamp >= '$startDate 00:00:00'
                                            and accountflow.operationtimestamp <= '$endDate 23:59:59') as noncash_expense ";
        $sql_patients .= 'from RDB$DATABASE';
        
        */
	}
}

?>
<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "PrintDataModule.php";
require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
//define('sqlite_folder', dirname(__FILE__).'/sqlite/');
//ChiefOlapModule::getOLAP('tp.dateclose','2013-08-01','2013-11-30',null,null,null,array());

class ChiefOlapModuleHealthproc
{
 	/**
    * @var string
    */
    var $ID; 
    
   	/**
    * @var string
    */
    var $NAME;
}
class ChiefOlapModule
{
    
    /**
     * @param ChiefOlapModuleRecord $p1
     * @param ChiefOlapModuleRoom $p2
     * @param ChiefOlapModuleCongestionRecord $p3
     * @param ChiefOlapModuleHealthproc $p4
     * @return void
     */
    public function registertypes($p1, $p2, $p3, $p4){}
    
    /**
     * ChiefOlapModule::getOLAP()
     * @param string $rangeField
     * @param string $startDate
     * @param string $endDate 
     * @param string $startBirthDay 
     * @param string $endBirthDay
     * @param string $healthprocId           
     * @param resource $trans
     * @return ChiefOlapModuleRecord[]
     */

    public static function getOLAP($rangeField, $startDate, $endDate, $startBirthDay, $endBirthDay, $healthprocId, $trans=null)
	{		
             $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }


                //$dbPath=sqlite_folder.uniqid().'_ChiefOlap.sqlite';

               // $db = new SQLite3($dbPath);        
              //  $db->querySingle("CREATE TABLE OLAP (".ChiefOlapModuleRecord::sqliteCreateVarString().")");    
        
               
            $fieldsArray=array('tp.id', 
                                'tp.name', 
                                'tp.shifr', 
                                'tp.healthtype', 
                                'tp.uop', 
                                'tp.price*tp.proc_count', 
                                '(tp.price*tp.proc_count*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100))', 
                                'tp.price*tp.proc_count*(tp.discount_flag*coalesce(i.discount_amount, p.discount_amount)/100)', 
                                'tp.dateclose', 
                                'cast(tp.dateclose as date)', 
                                'tp.proc_count*tp.expenses', 
                                'tp.proc_count', 
                                'tp.expenses_fact', 
                                '(tp.proc_count*tp.expenses-tp.expenses_fact)', 
                                'd.id', 
                                'd.shortname', 
                                'substring(d.speciality from 1 for 1)', 
                                'ds.percent', 
                                'ds.basesalary', 
                                '(case when (tp.salary_settings = 1 or tp.salary_settings = 2) 
                                then cast(
                                ds.percent*tp.proc_count*(tp.price*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100)-tp.expenses)/100
                                 as decimal(15,8)) else 0 end)', 
                                'cast(ds.basesalary+(case when (tp.salary_settings = 1 or tp.salary_settings = 2) then ds.percent*tp.proc_count*(tp.price*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100)-tp.expenses)/100  else 0 end) as decimal(15,8))', 
                                'ass.basesalary', 
                                '(case when (tp.salary_settings = 1 or tp.salary_settings = 3) 
                                then cast(
                                ass.percent*tp.proc_count*(tp.price*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100)-tp.expenses)/100
                                 as decimal(15,8)) else 0 end)', 
                                'cast(ass.basesalary+(case when (tp.salary_settings = 1 or tp.salary_settings = 3) then ass.percent*tp.proc_count*(tp.price*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100)-tp.expenses)/100  else 0 end) as decimal(15,8))', 
                                'cast((select 24*sum(cast(t.taskdate||\' \'||t.endoftheinterval as timestamp)-cast(t.taskdate||\' \'||t.beginoftheinterval as timestamp)) from tasks t
                                                                                                                                    where
                                                                                                                                    t.taskdate=cast(tp.dateclose as date)
                                                                                                                                    and t.factofvisit=1
                                                                                                                                    and t.doctorid=tp.doctor and t.patientid=tp.patient_fk)/(select count(tp2.id) from treatmentproc tp2 where cast(tp2.dateclose as date)=cast(tp.dateclose as date) and tp2.doctor=tp.doctor and tp2.patient_fk=tp.patient_fk) as decimal(15,8))', 
                                'p.id', 
                                'p.fullname', 
                                'p.birthday', 
                                '(Select * from AGE(p.birthday,tp.dateclose))', 
                                'p.sex', 
                                'p.villageorcity', 
                                '(case  when p.populationgroupe>9 then p.populationgroupe-10 else p.populationgroupe end)', 
                                '(case  when p.populationgroupe>9 then 1 else 0 end)', 
                                'p.discount_amount', 
                                '(tp.patient_fk||\'_\'||cast(tp.dateclose as date))', 
                                '1', 
                                '1', 
                                'wf.id', 
                                'wf.description', 
                                'gr.id', 
                                'gr.description', 
                                'hp.id', 
                                'hp.name', 
                                'hp.proctype', 
                                'hp.price*tp.proc_count', 
                                'i.id', 
                                'i.invoicenumber', 
                                'i.createdate', 
                                'cast((tp.price*tp.proc_count*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100)) as decimal(15,8))', 
                                '(tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))', 
                                'af.id', 
                                'af.ordernumber', 
                                '(case when i.summ is null or i.summ=0 then 0 else cast(af.summ*tp.price*tp.proc_count*((100.000000-tp.discount_flag*coalesce(i.discount_amount,p.discount_amount))/100)/i.summ as decimal(15,8)) end)',                                'af.operationtimestamp', 
                                'cast(i.createdate as date)', 
                                'cast(af.operationtimestamp as date)', 
                                'pi.id', 
                                'pi.policenumber', 
                                'pi.policefromdate', 
                                'pi.policetodate', 
                                'ic.id', 
                                'ic.name', 
                                ' null', 
                                ' null'
                                );
            $joinsArray=array('left join doctors d on d.id=tp.doctor', 
                                'left join patients p on p.id=tp.patient_fk', 
                                'left join patientcarddictionaries wf on wf.id=p.wherefromid', 
                                'left join patientcarddictionaries gr on gr.id=p.groupid', 
                                'left join healthproc hp on hp.id=tp.healthproc_fk', 
                                'left join invoices i on tp.invoiceid=i.id', 
                                'left join accountflow af on af.oninvoice_fk=i.id', 
                                'left join patientsinsurance pi on (pi.id=i.patieninsurance_fk)', 
                                'left join insurancecompanys ic on ic.id=pi.companyid', 
                                'left join getdoctorpercent(tp.doctor,tp.healthtype,tp.dateclose) ds on (ds.percent is not null)',
                                'left join getassistantpercent(tp.assistant,tp.healthtype,tp.dateclose) ass on (ass.percent is not null)');

                        
                   $QueryText = "select 
                                        ".implode(",
                                        ",$fieldsArray)." 
                                    from treatmentproc tp";           

                    $QueryText.=' 
                    '.implode(' 
                    ',$joinsArray);

                    $where=array();       	
                    
                    if(nonull($startDate))
                    {
                        $where[]="$rangeField >= '$startDate 00:00:00'";  
                    }
                    if(nonull($endDate))
                    {
                        $where[]="$rangeField <= '$endDate 23:59:59'";  
                    }
                   
                    if(nonull($startBirthDay))
                    {
                        $where[]="(Select * from AGE(p.birthday,tp.dateclose)) >= $startBirthDay";  
                    }
                    if(nonull($endBirthDay))
                    {
                        $where[]="(Select * from AGE(p.birthday,tp.dateclose)) <= $endBirthDay";  
                    }
                    if(nonull($healthprocId))
                    {
                        $where[]="tp.healthproc_fk = $healthprocId";  
                    }
                    if(count($where)>0)
                    {
                        $QueryText.=' where '.implode(' and ',$where);
                    }

         
    			$query = ibase_prepare($trans, $QueryText);
    			$result = ibase_execute($query);
                
                while ($row = ibase_fetch_row($result))
                {
                    $rec=new ChiefOlapModuleRecord($row);
                    $res[]=$rec;
                    //$db->query("INSERT INTO OLAP (".ChiefOlapModule::classVarString('ChiefOlapModuleRecord').") values(".ChiefOlapModule::valuesString('ChiefOlapModuleRecord', $rec).")");			     

                }
                
    			ibase_free_query($query);
    			ibase_free_result($result);
                
                                //$db->close();
                //$filename=null;
           
                //$filename = sqlite_folder."ChiefOlapSqlite.zip";
                //if(file_exists($filename))
                //{
                //    @unlink($filename);
                //}
               // $zip = new ZipArchive();
               // $zip->open($filename, ZIPARCHIVE::CREATE);
               // $zip->addFile($dbPath,'ChiefOlap.sqlite');
               // $zip->close();
               // if(file_exists($dbPath))
               // {
               //     @unlink($dbPath);
                //}
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }

        //return base64_encode(file_get_contents($filename));
        return $res;
	}
       

   
   
   
   public static function classVarString($className)
    {
        $fields=array_keys(get_class_vars($className));
        return implode(", ",$fields);
    }
    public function valuesString($className, $object)
    {
        $vals=array();
        $fields=array_keys(get_class_vars($className));
       foreach($fields as $field)
       {
            $vals[]=nullcheck($object->$field,'true');
       }
       return implode(', ',$vals);
    }
    
    /**
     * ChiefOlapModule::getCabinetCongestion
     * @param string $startDate
     * @param string $endDate            
     * @param resource $trans
     * @return ChiefOlapModuleRoom[]
     */
     
    public static function getCabinetCongestion($startDate, $endDate, $trans=null)
	{		
             $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            
			$QueryText = "select r.id, 
                                r.name, 
                                r.number,
                                r.description, 
                                r.roomcolor 
                                from rooms r 
                                order by r.sequincenumber, r.number";           
            //file_put_contents("C:/tmp.txt",$QueryText);
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $rec=new ChiefOlapModuleRoom; 
                $rec->ID=$row[0];
                if(nonull($row[1]))
                {
                   $rec->NAME=$row[1]; 
                } 
                else
                {
                    $rec->NAME='№'.$row[2]; 
                }
                $rec->DESCRIPTION=$row[3];
                $rec->COLOR=$row[4];
                $rec->records=ChiefOlapModule::getCabinetCongestionRecords($startDate, $endDate, $rec->ID, $trans);
                $res[]=$rec;			     
            }
             $allRec=new ChiefOlapModuleRoom; 
             $allRec->COLOR=8947848;
             $allRec->records=ChiefOlapModule::getCabinetCongestionRecords($startDate, $endDate, null, $trans);
             $res[]=$allRec;
             
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
    
        /**
     * @param string $search
     * @param resource $trans
     * @return ChiefOlapModuleHealthproc[]
     */
    public static function searchHealthproc($search, $trans=null)//sqlite
    {
            $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }   
            
            $QueryText = "select first 10 hp.id, hp.name 
                            from healthproc hp 
                            where hp.nodetype=1 and lower(hp.name||' '||hp.PLANNAME) like '%$search%' 
                            order by LOWER(hp.name)";
  
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            while ($row = ibase_fetch_row($result))
            {
                $hp = new ChiefOlapModuleHealthproc();
                $hp->ID=$row[0];
                $hp->NAME=$row[1];
                $res[]=$hp;

            }
            
            ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
//			 file_put_contents("C:/tmp.txt",var_export($res, true));
			return $res;
    }	
    
        /**
     * ChiefOlapModule::getCabinetCongestionRecords
     * @param string $startDate
     * @param string $endDate  
     * @param string $roomId          
     * @param resource $trans
     * @return ChiefOlapModuleRoom[]
     */
     
    public static function getCabinetCongestionRecords($startDate, $endDate, $roomId, $trans=null)
	{		
             $res=array();
            if($trans==null)
            {
                $connection = new Connection();
      		    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            $QueryText = "select
                        d.newdate,
                        cast(24*coalesce(sum(cast(t.taskdate||' '||t.endoftheinterval as timestamp)-cast(t.taskdate||' '||t.beginoftheinterval as timestamp)),0) as decimal(10,2))
                        from daterange('$startDate','$endDate') d
                        left join tasks t on (t.taskdate=d.newdate and t.patientid is not null and t.factofvisit=1".(nonull($roomId)?" and t.roomid=$roomId":"").")
                        group by d.newdate";           
            //file_put_contents("C:/tmp.txt",$QueryText);
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
            
            while ($row = ibase_fetch_row($result))
            {
                $rec=new ChiefOlapModuleCongestionRecord;
                $rec->DATE=$row[0];
                $rec->WORKHOURS=round($row[1],2);
                $rec->ROOMID=$roomId;
                $res[]=$rec;			     
            }
            
			ibase_free_query($query);
			ibase_free_result($result);
			
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
			
			return $res;
	}
   /**
     * ChiefOlapModule::printOLAP()
     * 
     * @param string[] $fields
     * @param string[] $fieldNames
     * @param object[] $olapData
     * @param string $docType            
     * @return string
     */
          
    public static function printOLAP($fields, $fieldNames, $olapData, $docType)
	{
                 
       $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/ChiefOLAP.'.$docType, OPENTBS_ALREADY_UTF8);
        //$TBS->NoErr=true;
        
        $template=array();
        
        if($docType=='ods')
        {
            $rowBlock='table:table-row';
            $numOpe='odsNum';
            $dateOpe='odsDate'; 
        }
        else if($docType=='xlsx')
        {
            $rowBlock='row';
            $numOpe='xlsxNum';
            $dateOpe='xlsxDate'; 
        }
        else
        {
            $rowBlock='';
            $numOpe='';
            $dateOpe='';  
        }
                          
        foreach($fields as $key=>$field)
        {
            switch($field->type)
            {
                case 'numeric':
                {
                    $template[]='[OLAP.'.$field->name.';ope='.$numOpe.']';    
                    break;                    
                }
                case 'string':
                case 'date':
                default:
                {
                    $template[]='[OLAP.'.$field->name.']';
                    break;
                }                                                    
            }                                
                
         }
        if(count($template)>0)
        {
           $template[0]='[OLAP;block='.$rowBlock.']'.$template[0];                     
        } 
        $TBS->SetOption('protect',false);
        $TBS->MergeBlock('FIELDS',$fieldNames);            
        $TBS->MergeBlock('TEMPLATE',$template);
        $TBS->MergeBlock('OLAP',$olapData);
        
        /*
        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($fields, true));
        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($fieldNames, true));
        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($template, true));
        file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($olapData, true));
        */ 
        
        $file_name = 'olap';
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;    
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        return $form_path; 
	}
 

}
class ChiefOlapModuleCongestionRecord
{

    /**
     * @var string
     */
     var $ROOMID;    
     
    /**
     * @var string
     */
     var $DATE;         
    
    /**
     * @var double
     */
     var $WORKHOURS;
     
}
class ChiefOlapModuleRoom
{

    /**
     * @var string
     */
     var $ID;
           
     /**
     * @var string
     */
     var $NAME;
        
     /**
     * @var string
     */
     var $DESCRIPTION;
        
        /**
     * @var int
     */
     var $COLOR;  
     
    /**
     * @var ChiefOlapModuleCongestionRecord[]
     */
     var $records;
     
}
class ChiefOlapModuleRecord
{

/**
 * @var string
 */
 var $TREATMENTPROC_ID;         

/**
 * @var string
 */
 var $TREATMENTPROC_NAME;         

/**
 * @var string
 */
 var $TREATMENTPROC_SHIFR;         

/**
 * @var string
 */
 var $TREATMENTPROC_HEALTHTYPE;
 
/**
 * @var double
 */
 var $TREATMENTPROC_UOP;         

/**
 * @var double
 */
 var $TREATMENTPROC_PRICE;      
    
/**
 * @var double
 */
 var $TREATMENTPROC_PRICE_DISCOUNT;
 
 /**
 * @var double
 */
 var $TREATMENTPROC_PRICE_DISCOUNT_SUMM; 
 
/**
 * @var string
 */
 var $TREATMENTPROC_DATECLOSE; 
         
/**
 * @var string
 */
 var $TREATMENTPROC_DAYCLOSE;  
 
/**
 * @var double
 */
 var $TREATMENTPROC_EXPENSES;         

/**
 * @var double
 */
 var $TREATMENTPROC_PROC_COUNT;         

/**
 * @var double
 */
 var $TREATMENTPROC_EXPENSES_FACT;         

/**
 * @var double
 */
 var $TREATMENTPROC_EXPENSES_DELTA;
 
/**
 * @var string
 */
 var $DOCTOR_ID;         

/**
 * @var string
 */
 var $DOCTOR_SHORTNAME;  
 
 /**      
 * @var string
 */
 var $DOCTOR_SPECIALITY;   
 
/**
 * @var double
 */
 var $DOCTOR_PERCENT;         

/**
 * @var double
 */
 var $DOCTOR_BASESALARY;         

/**
 * @var double
 */
 var $DOCTOR_PERCENTSUMM;         

/**
 * @var double
 */
 var $DOCTOR_FULLSALARY;         

/**
 * @var double
 */
 var $ASSISTANT_BASESALARY;         

/**
 * @var double
 */
 var $ASSISTANT_PERCENTSUMM;         

/**
 * @var double
 */
 var $ASSISTANT_FULLSALARY; 
 
/**
 * @var double
 */
 var $TREATMENTPROC_CALCTIME;         

/**
 * @var string
 */
 var $PATIENT_ID;         

/**
 * @var string
 */
 var $PATIENT_FULLNAME;         

/**
 * @var string
 */
 var $PATIENT_BIRTHDAY;         

/**
 * @var int
 */
 var $PATIENT_AGE;
 
/**
 * @var string
 */
 var $PATIENT_SEX;         

/**
 * @var string
 */
 var $PATIENT_VILLAGEORCITY;         

/**
 * @var string
 */
 var $PATIENT_POPULATIONGROUPE;         

/**
 * @var string
 */
 var $PATIENT_ISDISPENSARY;   
 
 
/**
 * @var string
 */
 var $PATIENT_DISCOUNT_AMOUNT;         
 
 
/**
 * @var string
 */
 var $PATIENT_VISIT_ID;    
 
 
/**
 * @var int
 */
 var $PATIENT_VISIT;    
 

/**
 * @var int
 */
 var $PATIENT_COUNT;   
 
   
/**
 * @var string
 */
 var $WHEREFROM_ID;         

/**
 * @var string
 */
 var $WHEREFROM_DESCRIPTION;         

/**
 * @var string
 */
 var $GROUP_ID;         

/**
 * @var string
 */
 var $GROUP_DESCRIPTION;         

/**
 * @var string
 */
 var $HEALTHPROC_ID;         

/**
 * @var string
 */
 var $HEALTHPROC_NAME;         

/**
 * @var string
 */
 var $HEALTHPROC_PROCTYPE;            

/**
 * @var double
 */
 var $HEALTHPROC_PRICE;             

/**
 * @var string
 */
 var $INVOICES_ID;         

/**
 * @var string
 */
 var $INVOICES_INVOICENUMBER;         

/**
 * @var string
 */
 var $INVOICES_CREATEDATE;         

/**
 * @var double
 */
 var $INVOICES_PAIDINCASH;         

/**
 * @var double
 */
 var $TREATMENTPROC_DISCOUNT_AMOUNT;
 
/**
 * @var string
 */
 var $ACCOUNTFLOW_ID;         

/**
 * @var string
 */
 var $ACCOUNTFLOW_ORDERNUMBER;         

/**
 * @var double
 */
 var $ACCOUNTFLOW_SUMM;         

/**
 * @var string
 */
 var $ACCOUNTFLOW_OPERATIONTIMESTAMP;         

/**
 * @var string
 */
 var $INVOICES_CREATE_DAY;         

/**
 * @var string
 */
 var $ACCOUNTFLOW_OPERATIONDAY;         

/**
 * @var string
 */
 var $PATIENTSINSURANCE_ID;         

/**
 * @var string
 */
 var $PATIENTSINSURANCE_POLICENUMBER;         

/**
 * @var string
 */
 var $PATIENTSINSURANCE_POLICETODATE;         

/**
 * @var string
 */
 var $PATIENTSINSURANCE_POLICEFROMDATE;
          
/**
 * @var string
 */
 var $INSURANCECOMPANYS_ID; 
 
/**
 * @var string
 */
 var $INSURANCECOMPANYS_NAME; 

/**
* @var int
*/
var $COUNT; 
     
/**
* @var boolean
*/
var $isOpen; 
   
    //public static function sqliteCreateVarString()
//    {
//      return '  TREATMENTPROC_ID TEXT,
//                TREATMENTPROC_NAME TEXT,
//                TREATMENTPROC_SHIFR TEXT,
//                TREATMENTPROC_HEALTHTYPE TEXT,
//                TREATMENTPROC_UOP NUMERIC,
//                TREATMENTPROC_PRICE NUMERIC,
//                TREATMENTPROC_PRICE_DISCOUNT NUMERIC,
//                TREATMENTPROC_PRICE_DISCOUNT_SUMM NUMERIC,
//                TREATMENTPROC_DATECLOSE TEXT,
//                TREATMENTPROC_DAYCLOSE TEXT,
//                TREATMENTPROC_EXPENSES NUMERIC,
//                TREATMENTPROC_PROC_COUNT NUMERIC,
//                TREATMENTPROC_EXPENSES_FACT NUMERIC,
//                TREATMENTPROC_EXPENSES_DELTA NUMERIC,
//                DOCTOR_ID TEXT,
//                DOCTOR_SHORTNAME TEXT,
//                DOCTOR_SPECIALITY TEXT,
//                DOCTOR_PERCENT NUMERIC,
//                DOCTOR_BASESALARY NUMERIC,
//                DOCTOR_PERCENTSUMM NUMERIC,
//                DOCTOR_FULLSALARY NUMERIC,
//                ASSISTANT_BASESALARY NUMERIC,
//                ASSISTANT_PERCENTSUMM NUMERIC,
//                ASSISTANT_FULLSALARY NUMERIC,
//                TREATMENTPROC_CALCTIME NUMERIC,
//                PATIENT_ID TEXT,
//                PATIENT_FULLNAME TEXT,
//                PATIENT_BIRTHDAY TEXT,
//                PATIENT_AGE INTEGER,
//                PATIENT_SEX TEXT,
//                PATIENT_VILLAGEORCITY TEXT,
//                PATIENT_POPULATIONGROUPE TEXT,
//                PATIENT_ISDISPENSARY TEXT,
//                PATIENT_DISCOUNT_AMOUNT TEXT,
//                PATIENT_VISIT_ID TEXT,
//                PATIENT_VISIT INTEGER,
//                PATIENT_COUNT INTEGER,
//                WHEREFROM_ID TEXT,
//                WHEREFROM_DESCRIPTION TEXT,
//                GROUP_ID TEXT,
//                GROUP_DESCRIPTION TEXT,
//                HEALTHPROC_ID TEXT,
//                HEALTHPROC_NAME TEXT,
//                HEALTHPROC_PROCTYPE TEXT,
//                HEALTHPROC_PRICE NUMERIC,
//                INVOICES_ID TEXT,
//                INVOICES_INVOICENUMBER TEXT,
//                INVOICES_CREATEDATE TEXT,
//                INVOICES_PAIDINCASH NUMERIC,
//                TREATMENTPROC_DISCOUNT_AMOUNT NUMERIC,
//                ACCOUNTFLOW_ID TEXT,
//                ACCOUNTFLOW_ORDERNUMBER TEXT,
//                ACCOUNTFLOW_SUMM NUMERIC,
//                ACCOUNTFLOW_OPERATIONTIMESTAMP TEXT,
//                INVOICES_CREATE_DAY TEXT,
//                ACCOUNTFLOW_OPERATIONDAY TEXT,
//                PATIENTSINSURANCE_ID TEXT,
//                PATIENTSINSURANCE_POLICENUMBER TEXT,
//                PATIENTSINSURANCE_POLICETODATE TEXT,
//                PATIENTSINSURANCE_POLICEFROMDATE TEXT,
//                INSURANCECOMPANYS_ID TEXT,
//                INSURANCECOMPANYS_NAME TEXT,
//                COUNT INTEGER,
//                isOpen INTEGER';
//    }
                
    function __construct($row=null) 
    {
       
       if($row!=null)
       {
           
            $fields=array_keys(get_class_vars("ChiefOlapModuleRecord"));
           foreach($fields as $key=>$field)
           {
                $this->$field=$row[$key];
                
           }

       }
    }


}
?>
<?php

require_once(dirname(__FILE__).'/tbs/tbs_class.php');
require_once(dirname(__FILE__).'/tbs/tbs_plugin_opentbs.php');
require_once "F39OBJECT.php";
require_once "Connection.php";
require_once "Utils.php";


class Form039Module
{
	/**
	* @param Form039Module_F39_Record $p1
    * @param Form039Module_F39_Val $p2
	*/
	public function registertypes($p1, $p2) 
	{
	}
    
    
    
   /** 
 * @param string $docType
 * @param string $doctor_id
 * @param string $date
 * @param int $form
 * @return string
 */  
 public static function create039($docType, $doctor_id, $startDate, $endDate, $form)
 {
        switch($form)
        {
            case 392:
                $fpath='form039_2';
                break;
            case 393:
                $fpath='form039_3';
                break;
            case 394:
                $fpath='form039_4';
                break;
            default:
                $fpath='form039_2';
                break;
        }
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate(dirname(__FILE__).'/tbs/res/'.$fpath.'.'.$docType, OPENTBS_ALREADY_UTF8);
        $FORM=new F39OBJECT($doctor_id, $startDate, $endDate, $form);
        $TBS->NoErr=true;
        $TBS->MergeBlock('TABLE_DATA,',$FORM->TABLE_DATA);            
        $TBS->MergeField('FORM',$FORM);    
        $file_name = translit(str_replace(' ','_',$FORM->DOCTOR_DATA->LASTNAME.' '.$FORM->DOCTOR_DATA->FIRSTNAME.' '.$FORM->DOCTOR_DATA->MIDDLENAME.'_'.$fpath.'_'.$startDate.'_'.$endDate));
        $file_name = preg_replace('/[^A-Za-z0-9_]/', '', $file_name);
        $form_path = uniqid().'_'.$file_name.'.'.$docType;
        $TBS->Show(OPENTBS_FILE, dirname(__FILE__).'/docs/'.$form_path);
        return $form_path;   
 }

    /** 
 * @param string $type  
 * @param boolean $isClient  
 * @param resource $trans  
 * @return Form039Module_F39_Record[]
 */  
 public static function get039Fields($type, $isClient=true, $trans=null)//sqlite
 {

    
    if(!$trans)
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $local=true;
    }
    else
    {
        $local=false;
    }  
    
    
    $QueryText = "select ID FROM F39";
    if(nonull($type))
    {
        $QueryText.=" where DESCRIPTION='$type'";
    }
    else
    {
        $QueryText.=" where PARRENT is null";
    }
    if($isClient)
    {
        $QueryText.=" and TYPE>-1";
    }
    else
    {
        $QueryText.=" and NUMBER is not null";
    }

	$query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    $res=array();
	while($row = ibase_fetch_row($result))
	{	
        $res[]=new Form039Module_F39_Record($row[0],$isClient,$trans); 

	}			

	ibase_free_query($query);
	ibase_free_result($result);
            
    if($local)
    {
        ibase_commit($trans);
        $connection->CloseDBConnection();
    }
    return $res;     
 }
  /** 
 * @param int $type
 * @param string $id    
 * @param resource $trans  
 * @return Form039Module_F39_Val[]
 */  
 public static function get039ProcValues($type, $id, $trans=null)//sqlite
 {

     $res=array();
     if(nonull($id))
     {
            if(!$trans)
            {
                $connection = new Connection();
        	    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }

            if($type==0)
            {
                $QueryText = "select F39ID,UNSIGNED FROM F39PROC where HEALTHPROCID=$id";
            }
            else
            {
                $QueryText = "select F39ID,UNSIGNED FROM F39TPROC where TREATMENTPROCID=$id";
            }
            
            $query = ibase_prepare($trans, $QueryText);
        	$result = ibase_execute($query);

        	
            while($row = ibase_fetch_row($result))
        	{	
                $res[]=new Form039Module_F39_Val($row[0],$row[1]); 
        
        	}  

        
        
        	ibase_free_query($query);
        	ibase_free_result($result);
                                
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
     }
    
    return $res;     
 }
   /** 
 * @param int $type
 * @param string $id
 * @param Form039Module_F39_Val[] $vals    
 * @param resource $trans  
 * @return Form039Module_F39_Val[]
 */  
 public static function save039ProcValues($type, $id, $vals, $trans=null)
 {

     $res=array();
     if(nonull($id))
     {
            if(!$trans)
            {
                $connection = new Connection();
        	    $connection->EstablishDBConnection();
                $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
                $local=true;
            }
            else
            {
                $local=false;
            }
            $tableid='';  
            switch($type)
            {
                case 0:
                    $tableid='HEALTHPROCID';
                    break;
                case 1:
                    $tableid='TREATMENTPROCID';
                    break; 
            }
           //  file_put_contents("C:/tmp.txt", '');
            foreach($vals as $val)
            {
                $QueryText = "insert into F39PROC (F39ID,UNSIGNED, $tableid) values($val->ID,".($val->UNSIGNED?1:0).",$id)";
   	           ///  file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".$QueryText);
                $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
                ibase_free_query($query);
            }
   
            $res=Form039Module::get039ProcValues($type,$id,$trans);        
            if($local)
            {
                ibase_commit($trans);
                $connection->CloseDBConnection();
            }
     }
    
    return $res;     
 } 
    /** 
  * @param string $form_path
  * @return boolean
  */  
    public function deleteForm($form_path)
    {
        return deleteFileUtils($form_path);
    }
}
class Form039Module_F39_Val
{
    /**
    * @var string
    */  
    var $ID;
    
    /**
    * @var boolean;
    */  
    var $UNSIGNED;
    
    function __construct($id, $unsigned)     
    {
        $this->ID=$id;
        $this->UNSIGNED=$unsigned;
    }
}
class Form039Module_F39_Record
{
    /**
    * @var string
    */  
    var $ID;
    
        /**
    * @var string
    */  
    var $DESCRIPTION;
    
    /**
    * @var int
    */  
    var $NUMBER;
    
    /**
    * @var Form039Module_F39_Record[]
    */  
    var $CHILDREN;
    
    /**
    * @var int
    */  
    var $TYPE;
    
    /**
    * @var boolean
    */  
    var $OLDSELECTED;
            
    /**
    * @var boolean
    */  
    var $SELECTED;
    
    function __construct($id, $isClient, $trans)     
    {
          if(!$trans)
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $local=true;
    }
    else
    {
        $local=false;
    }  
    
    
    $QueryText = 'select f39.number, f39.description, f39."TYPE" FROM F39 where id='.$id;

	$query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
	if($row = ibase_fetch_row($result))
	{	
        $this->ID=$id;
        $this->NUMBER=$row[0];
        $this->DESCRIPTION=$row[1];
        $this->TYPE=$row[2];
        if($this->TYPE>0)
        {
            $this->CHILDREN=Form039Module_F39_Record::getChildren($id,$isClient,$trans); 
        }

	}			

	ibase_free_query($query);
	ibase_free_result($result);
            
    if($local)
    {
        ibase_commit($trans);
        $connection->CloseDBConnection();
    } 
    }
    
    public static function getChildren($parrent_id,$isClient,$trans=null)
 {
         if(!$trans)
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $local=true;
    }
    else
    {
        $local=false;
    } 
    
    $QueryText = "select ID FROM F39 where PARRENT=$parrent_id";
    if($isClient)
    {
        $QueryText.=" and TYPE>-1";
    }
    else
    {
        $QueryText.=" and NUMBER is not null";
    }
	$query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    $res=array();
	while($row = ibase_fetch_row($result))
	{	
         $res[]=new Form039Module_F39_Record($row[0],$isClient,$trans); 
	}			

	ibase_free_query($query);
	ibase_free_result($result);
            
    if($local)
    {
        ibase_commit($trans);
        $connection->CloseDBConnection();
    } 
    return $res;  
 }           
}
?>

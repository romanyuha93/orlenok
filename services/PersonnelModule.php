<?php

require_once "Connection.php";
require_once "Utils.php";

class PersonnelModuleCabinet
{				
	/**
	* @var string
	*/
	var $ID;   	
	
	/**
	* @var string
	*/
	var $NAME;
}

class PersonnelModuleSchedule
{
	/**
	* @var string
	*/
	var $ID;   	
	
	/**
	* @var string
	*/
	var $DOCTORID;  
	
	/**
	* @var string
	*/
	var $NAME;
	
	/**
	* @var string
	*/
	var $STARTDATE;
	
	/**
	* @var string
	*/
	var $ENDDATE;
	
	/**
	* @var string
	*/
	var $ISCURRENT;
	
	/**
	* @var PersonnelModuleRule[]
	*/
	var $rulesInForSaveFormat;
	
	/**
	* @var object[]
	*/
	var $rules;
}

class PersonnelModuleRule
{
	/**
	* @var string
	*/
	var $ID;   

	/**
	* @var string
	*/
	var $OPERATINGSCHEDULEID; 	
	
	/**
	* @var string
	*/
	var $NAME;
	
	/**
	* @var string
	*/
	var $ISWORKING;
	
	/**
	* @var string
	*/
	var $DAYSRANGE;
	
	/**
	* @var string
	*/
	var $REPEATTYPE;
	
	/**
	* @var string
	*/
	var $SKIPDAYSCOUNT;
	
	/**
	* @var string
	*/
	var $STARTTIME;
	
	/**
	* @var string
	*/
	var $ENDTIME;
	
	/**
	* @var string
	*/
	var $CABINETID;
}

class PersonnelModuleData
{
	/**
	* @var PersonnelModuleSchedule[]
	*/
	var $schedules;

	/**
	* @var PersonnelModuleCabinet[]
	*/
	var $cabinets;
}

class PersonnelModule
{		
  /**
   * @param PersonnelModuleCabinet $p1  
   * @param PersonnelModuleSchedule $p2
   * @param PersonnelModuleRule $p3
   * @param PersonnelModuleData $p4
   * @return void	    
  */    
	public function registertypes($p1, $p2, $p3, $p4) 
	{
	} 
	
    /** 									
 	* @param string $doctorID
	* @return PersonnelModuleData
	*/ 
	public function getPersonnelModuleData($doctorID) //sqlite
    {
        return $this->getCurrentPersonnelModuleData($doctorID, false);
    }
	/** 									
 	* @param string $doctorID
    * @param bool $onlyCurrent
	* @return PersonnelModuleData
	*/ 
	public function getCurrentPersonnelModuleData($doctorID, $onlyCurrent)  //sqlite
	{
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$returnObj = new PersonnelModuleData();
			$returnObj->schedules = array();
			$QueryText = "
			select
				ID,  
				DOCTORID,							    
				NAME,
				STARTDATE,
				ENDDATE,
				ISCURRENT				
			from OPERATINGSCHEDULE
			where DOCTORID = $doctorID ";
            $QueryText .= $onlyCurrent ? " and ISCURRENT = 1" : "order by ID desc";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$rows = array();	
			while ($row = ibase_fetch_row($result))
			{ 
				$obj = new PersonnelModuleSchedule;
				$obj->ID = $row[0];				
				$obj->DOCTORID = $row[1];		
				$obj->NAME = $row[2];		
				$obj->STARTDATE = $row[3];			
				$obj->ENDDATE = $row[4];	
				$obj->ISCURRENT = $row[5];
				$QueryText2 = "
				select
					ID,  			    
					OPERATINGSCHEDULEID,
					NAME,
					ISWORKING,
					DAYSRANGE,
					REPEATTYPE,
					SKIPDAYSCOUNT,
					STARTTIME,
					ENDTIME,
					CABINETID
				from OPERATINGSCHEDULERULES
				where OPERATINGSCHEDULEID=$obj->ID";
				$query2 = ibase_prepare($trans, $QueryText2);
				$result2 = ibase_execute($query2);
				$rows2 = array();
				
				while ($row2 = ibase_fetch_row($result2))
				{ 
					$obj2 = new PersonnelModuleRule;
					
					$obj2->ID = $row2[0];					
					$obj2->OPERATINGSCHEDULEID = $row2[1];		
					$obj2->NAME = $row2[2];			
					$obj2->ISWORKING = $row2[3];	
					
					$txtBlob = null;					
					$blob_info = ibase_blob_info($row2[4]);
					
					if ($blob_info[0] != 0)
					{
						$blob_hndl = ibase_blob_open($row2[4]);
						$txtBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
						ibase_blob_close($blob_hndl);							
					}					
					
					$obj2->DAYSRANGE = $txtBlob;						
					
					$obj2->REPEATTYPE = $row2[5];	
					$obj2->SKIPDAYSCOUNT = $row2[6];
					$obj2->STARTTIME = $row2[7];	
					$obj2->ENDTIME = $row2[8];
					$obj2->CABINETID = $row2[9];					
				
					$rows2[] = $obj2;
                }			
				
				$obj->rulesInForSaveFormat = $rows2;
				
				$rows[] = $obj;
            }	
			$returnObj->schedules = $rows;
			ibase_free_query($query);
			ibase_free_result($result);	
			
			$returnObj->cabinets = array();			
			
			$QueryText = "
			select
				ID,  			    
				NAME	
			from ROOMS";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);
			$rows = array();
			while ($row = ibase_fetch_row($result))
			{ 
				$obj = new PersonnelModuleCabinet;
				$obj->ID = $row[0];					
				$obj->NAME = $row[1];
				$rows[] = $obj;	
            }	
			
			$returnObj->cabinets = $rows;
			ibase_free_query($query);
			ibase_free_result($result);	
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
			return $returnObj;
	}

	/**
 	* @param PersonnelModuleSchedule $schedule
	* @param string[] $rulesToDeleteArrayCollection
	* @return PersonnelModuleSchedule
 	*/
  public function updateOrInsertPersonnelModuleSchedule($schedule, $rulesToDeleteArrayCollection) 
  { 
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
								
			if (($schedule->ID == "") || ($schedule->ID == "null"))
			{			
				$isInsert = "1";
			}
			else
			{
				$isInsert = "0";
			}
			$QueryText = "		  
			update or insert into OPERATINGSCHEDULE
			(
				ID,
				DOCTORID,
				NAME,
				STARTDATE,
				ENDDATE,
				ISCURRENT
			)
			values("; 

			if (($schedule->ID == "") || ($schedule->ID == "null"))
			{
				$QueryText = $QueryText."null, ";
			}
			else
			{
				$QueryText = $QueryText."$schedule->ID, ";
			}	
			
			$QueryText = $QueryText."$schedule->DOCTORID, '".safequery($schedule->NAME)."', '".safequery($schedule->STARTDATE)."', '".safequery($schedule->ENDDATE)."', $schedule->ISCURRENT)";			
			$QueryText = $QueryText." returning ID";
			$query = ibase_prepare($trans, $QueryText);
			$result = ibase_execute($query);	
			
			if ($row = ibase_fetch_row($result))				    	
			{ 
				$schedule->ID = $row[0];					
			}	
					
			ibase_free_query($query);
			ibase_free_result($result);		

			if ($isInsert == "1")
			{
				$QueryText = "		  
				update OPERATINGSCHEDULE
				set ISCURRENT=0
				where (DOCTORID = $schedule->DOCTORID) and (ID <> $schedule->ID)";
				$query = ibase_prepare($trans, $QueryText);
				ibase_execute($query);
				ibase_free_query($query);
			}
			if (isset($rulesToDeleteArrayCollection) && (isset($rulesToDeleteArrayCollection[0])))
			{
				$QueryText = "			
				delete from OPERATINGSCHEDULERULES
				where (ID is null)";	
				for ($i=0; isset($rulesToDeleteArrayCollection[$i]); $i++)
				{
					$ruleID = $rulesToDeleteArrayCollection[$i]->ID;
					$QueryText = $QueryText." or (ID=$ruleID)";
				}		
				$query = ibase_prepare($trans, $QueryText);
				ibase_execute($query);
				ibase_free_query($query);
			}
			if (isset($schedule->rulesInForSaveFormat) && (isset($schedule->rulesInForSaveFormat[0])))
			{
				for ($i=0; isset($schedule->rulesInForSaveFormat[$i]); $i++)
				{
					$ruleRecord = $schedule->rulesInForSaveFormat[$i];
					$QueryText = "		  
					update or insert into OPERATINGSCHEDULERULES
					(
						ID,
						OPERATINGSCHEDULEID,
						NAME,
						ISWORKING,
						DAYSRANGE,
						REPEATTYPE,
						SKIPDAYSCOUNT,
						STARTTIME,
						ENDTIME,
						CABINETID      
					)
					values(";
					
					if (($ruleRecord->ID == "") || ($ruleRecord->ID == "null"))
					{
						$QueryText = $QueryText."null, ";
					}
					else
					{
						$QueryText = $QueryText."$ruleRecord->ID, ";
					}	
					
					$QueryText = $QueryText."$schedule->ID, '".safequery($ruleRecord->NAME)."', $ruleRecord->ISWORKING, '".safequery($ruleRecord->DAYSRANGE)."', ";				
					
					if (($ruleRecord->REPEATTYPE == "") || ($ruleRecord->REPEATTYPE == "null"))
					{
						$QueryText = $QueryText."null, ";
					}
					else
					{
						$QueryText = $QueryText."$ruleRecord->REPEATTYPE, ";
					}	
					
					if (($ruleRecord->SKIPDAYSCOUNT == "") || ($ruleRecord->SKIPDAYSCOUNT == "null"))
					{
						$QueryText = $QueryText."null, ";
					}
					else
					{
						$QueryText = $QueryText."$ruleRecord->SKIPDAYSCOUNT, ";
					}	
					
					$QueryText = $QueryText."'".safequery($ruleRecord->STARTTIME)."', '".safequery($ruleRecord->ENDTIME)."', ";
					
					
					if (($ruleRecord->CABINETID == "") || ($ruleRecord->CABINETID == "null"))
					{
						$QueryText = $QueryText."null)";
					}
					else
					{
						$QueryText = $QueryText."$ruleRecord->CABINETID)";
					}	
					
					$QueryText = $QueryText." returning ID";
					$query = ibase_prepare($trans, $QueryText);
					$result = ibase_execute($query);	
					
					if ($row = ibase_fetch_row($result))				    	
					{ 
						$ruleRecord->ID = $row[0];											
					}	
				
					ibase_free_query($query);
					ibase_free_result($result);
				}	
			}
			$success = ibase_commit($trans);
			$connection->CloseDBConnection();
			return $schedule;
  }
	
	/**
 	* @param string[] $Arguments
    * @return boolean
 	*/ 		 		
	public function deletePersonnelModuleRecords($Arguments) 
	{ 
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "delete from OPERATINGSCHEDULE where (ID is null)";
			for ($i=0; isset($Arguments[$i]); $i++)
			{ 
				$QueryText = $QueryText." or (ID=$Arguments[$i])"; 
			}
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			$success = ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
			return $success;	
	}
	
	/**
 	* @param string $ID
    * @return boolean
 	*/ 		 		
	public function setScheduleAsCurrent($ID) 
	{
			$connection = new Connection();
			$connection->EstablishDBConnection();
			$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
			$QueryText = "
			update OPERATINGSCHEDULE 
			set ISCURRENT = 1
			where (ID = $ID)";
			$query = ibase_prepare($trans, $QueryText);
			ibase_execute($query);
			$success = ibase_commit($trans);
			ibase_free_query($query);
			$connection->CloseDBConnection();
			return $success;		
	}
}
?>
<?php

require_once "Connection.php";
require_once "Settings.php";
define('tfClientId','online');


    $connection = new Connection();
	$connection->EstablishDBConnection();
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);


    $php_now = new DateTime();
    
    // *********************************** FILE_DOCTORS ********* START **************************
    file_put_contents(Settings::EXPORT_DOCTORSTREAT_FILE, "");
    $proc_sql = "select
                        invoices.createdate,
                        invoices.invoicenumber,
                        (select patients.fullname from patients where patients.id = treatmentproc.patient_fk),
                        (select patients.cardbefornumber||patients.cardnumber||patients.cardafternumber from patients where patients.id = treatmentproc.patient_fk),
                        (select doctors.fullname from doctors where doctors.id = treatmentproc.doctor),
                        (select doctors.tin from doctors where doctors.id = treatmentproc.doctor),
                        (select assistants.fullname from assistants where assistants.id = treatmentproc.assistant),
                        (select assistants.tin from assistants where assistants.id = treatmentproc.assistant),
                        treatmentproc.name,
                        treatmentproc.shifr,
                        treatmentproc.price,
                        treatmentproc.proc_count,
                        treatmentproc.price * treatmentproc.proc_count,
                        treatmentproc.price * treatmentproc.proc_count * invoices.discount_amount,
                        (select rooms.name from rooms where rooms.id = treatmentproc.cabinet_fk),
                        (select subdivision.name from subdivision where subdivision.id = invoices.subdivisionid)
                
                from treatmentproc
                        inner join invoices on invoices.id = treatmentproc.invoiceid
                        
                where invoices.createdate >= '".$php_now->format("Y-m-d")." 00:00:00'
                        and invoices.createdate <= '".$php_now->format("Y-m-d")." 23:59:59'";
                    
    $proc_query = ibase_prepare($trans, $proc_sql);
    $proc_result = ibase_execute($proc_query);
    $csv_doc_row = '';
    while ($row = ibase_fetch_row($proc_result))
    { 
            $csv_doc_row .= $row[0].','.$row[1].','.$row[2].','.$row[3].','.$row[4].','.$row[5].','.$row[6].','.$row[7].','.$row[8].','.$row[9].','.$row[10].','.
                            $row[11].','.$row[12].','.$row[13].','.$row[14].','.$row[15].','.$row[16]."\n";
    }
    
    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($csv_row,true));
    
    file_put_contents(Settings::EXPORT_DOCTORSTREAT_FILE, $csv_doc_row);
    // *********************************** FILE_DOCTORS ********* END **************************
    
    
    // *********************************** FILE_ACCOUNT ********* START **************************
    file_put_contents(Settings::EXPORT_ACCOUNT_FILE, "");
    $invoc_sql = "select

                    (select max(accountflow.operationtimestamp) from accountflow where accountflow.oninvoice_fk = invoices.id),
                    invoices.invoicenumber,
                    (select account.lastname||' '||account.firstname||' '||account.middlename from account where account.id = invoices.authorid),
                    (select account.tax_id from account where account.id = invoices.authorid),
                    (select patients.fullname from patients where patients.id = invoices.patientid),
                    (select patients.cardbefornumber||patients.cardnumber||patients.cardafternumber from patients where patients.id = invoices.patientid),
                    invoices.is_cashpayment,
                    invoices.is_fiscal,
                    (select coalesce(sum(accountflow.summ), '0.00') from accountflow where accountflow.oninvoice_fk = invoices.id)
                        
                from invoices
                        
                    where invoices.createdate >= '".$php_now->format("Y-m-d")." 00:00:00'
                        and invoices.createdate <= '".$php_now->format("Y-m-d")." 23:59:59'";
                    
    $invoc_query = ibase_prepare($trans, $invoc_sql);
    $invoc_result = ibase_execute($invoc_query);
    $csv_inv_row = '';
    while ($row = ibase_fetch_row($invoc_result))
    { 
            $csv_inv_row .= $row[0].','.$row[1].','.$row[2].','.$row[3].','.$row[4].','.$row[5].','.$row[6].','.$row[7].','.$row[8]."\n";
    }
    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($csv_row,true));
    
    file_put_contents(Settings::EXPORT_ACCOUNT_FILE, $csv_inv_row);
    // *********************************** FILE_ACCOUNT ********* END **************************
    
	ibase_commit($trans);
	$connection->CloseDBConnection();
    

?>
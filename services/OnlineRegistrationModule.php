<?php

//define('tfClientId', 'online');
require_once "Connection.php";
require_once "Utils.php";
require_once "CalendarModule.php";
require_once "PatientCardModule.php";

//ini_set('display_errors', 'on');

class OnlineRegistrationRecord
{
    /**
    * @var string
    */
    var $SURNAME;

    /**
    * @var string
    */
    var $NAME; 

    /**
    * @var string
    */    
    var $MIDLENAME;
    
    /**
    * @var string
    */    
    var $FULLNAME;
    
    /**
    * @var string
    */    
    var $SHORTNAME;        
    
    /**
    * @var string
    */    
    var $PHONE;
    
    /**
    * @var string
    */
    var $EMAIL;
    
    /**
    * @var string
    */
    var $TASKTASKDATE;
     
    /**
    * @var string
    */
    var $TASKBEGINOFTHEINTERVAL;
     
    /**
    * @var string
    */
    var $TASKENDOFTHEINTERVAL;
    
    /**
    *@var int
    */
    var $ROOMID;
    
    /**
    *@var int
    */
    var $ROOMNUMBER;
    
    /**
    *@var string
    */
    var $ROOMNAME;
    
    /**
    *@var int
    */
    var $DOCTORID;
}

class OnlineRegistrationDoctorRecord
{
    /**
    *@var number
    */
    var $DOCTORID;
    
    /**
    * @var string
    */
    var $SHORTNAME;
    
    /**
    * @var string
    */
    var $IMG;
}

class OnlineRegistrationSettings 
{
    /**
    *@var boolean
    */
    var $isRequiredSurname;
    
    /**
    *@var bool
    */
    var $isRequiredName;
    
    /**
    *@var bool
    */
    var $isRequiredMiddlename;
    
    /**
    *@var bool
    */
    var $isRequiredPhone;
    
    /**
    *@var bool
    */
    var $isRequiredEmail;
    
    /**
    *@var string
    */
    var $borderColor;
    
    /**
    *@var string
    */
    var $backgroundColor;
    
    /**
    *@var string
    */
    var $language;
    
    /**
    *@var bool
    */
    var $ReadImagesFromDB;
    
    /**
    *@var object[]
    */    
    var $DaysAndTimeClinicWork;
    
    /**
    *@var int
    */    
    var $CountMinutesForOnlineReceptin;
    
    /**
    *@var object
    */    
    var $CountDayFromRegistration;
    
    /**
    *@var bool
    */
    var $isConsiderSchedule;
    
    /**
    *@var string
    */
    var $countryPhoneCode;
}

class OnlineRegistrationModule
{
    /**
     * @param OnlineRegistrationRecord $p1
     * @param OnlineRegistrationDoctorRecord $p2
     * @param OnlineRegistrationSettings $p3
     */
    public function registertypes($p1, $p2, $p3)
    {
    }
    
    /**
      * OnlineRegistration::createDaysArray()
      * 
      * @param string $startDate YYYY-MM-DD
      * @param string $endDate YYYY-MM-DD
      * @return string[]
      */
     private function createDaysArray($startDate, $endDate)
     {
        $s = strtotime($startDate);
        $e = strtotime($endDate);
        $days = abs($s - $e) / 86400 + 1; 
        $result = array();
        for ($i=0; $i<$days; $i++)
        {
            $result[] = date("Y-m-d", $s + 86400*$i);
        }
        return $result;
     }
 
    /**
     * OnlineRegistration::getSettings
     * 
     * 
    *@return OnlineRegistrationSettings
    */
    
    public function getSettingsXML() {
	
        $result = new OnlineRegistrationSettings;
        $xml = simplexml_load_file(dirname($_SERVER['SCRIPT_FILENAME']).'/toothfairyonline/schedule/schedule.xml');
        $requiredFields = $xml->IsNecessarilyFieldsForRecording[0]->attributes();
        $colors = $xml->Colors[0]->attributes();
        $language = $xml->Language[0]->attributes();
        $whereReadImages = $xml->WhereReadImages[0]->attributes();
        $countMinutes = $xml->countMinutesForOnlineReceptin[0]->attributes();
        $countDays = $xml->countDayFromRegistration[0]->attributes();
        $isConSched = $xml->isConsiderSchedule[0]->attributes();

        $result->isRequiredSurname = $requiredFields["surname"] == "true";
        $result->isRequiredName = $requiredFields["name"] == "true";
        $result->isRequiredMiddlename = $requiredFields["middlename"] == "true";
        $result->isRequiredPhone = $requiredFields["phone"] == "true";
        $result->isRequiredEmail = $requiredFields["email"] == "true";
        
        
        $result->borderColor = (string)$colors["borderColor"];
        $result->backgroundColor = (string)$colors["backgroundColor"];
        
        $result->language = (string)$language["lang"];
        
        $result->ReadImagesFromDB = $whereReadImages["fromDb"] == "true";
        
        //get work days and work time by days when clinic works
        $result->DaysAndTimeClinicWork=array();
        $obj = new stdClass();
        $day = $xml->Sunday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        $day = $xml->Monday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        $day = $xml->Tuesday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        $day = $xml->Wednesday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        $day = $xml->Thursday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        $day = $xml->Friday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        $day = $xml->Saturday[0]->attributes();
        $obj->isWork = $day["isWork"] == "true";
        $obj->beginTime = (string)$day["beginTime"];
        $obj->endTime = (string)$day["endTime"];
        $result->DaysAndTimeClinicWork[] = $obj;
        
        //how many minutes allocate to online reception
        $result->CountMinutesForOnlineReceptin = (int)$countMinutes["minutes"];
        
        //how many days from current will active days in the calendar
        $result->CountDayFromRegistration = (int)$countDays["count"];
        
        $result->isConsiderSchedule = $isConSched["isConsider"] == "true";

        return $result;
    }
    
    
    
    
    /**
     * OnlineRegistration::getSettings
    * @param resource $trans
     * 
    *@return OnlineRegistrationSettings
    */
    public function getSettings($trans=null)
    {
        if(!$trans)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection("main", false);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
        
        $settings = new OnlineRegistrationSettings;
        $returnGet = null;
        
        $QueryText = "select PARAMETERNAME, PARAMETERVALUE from APPLICATIONSETTINGS where parametername like 'ORM_%' or parametername='COUNTRYPHONECODE'";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $settingsArray = array();
        while($row = ibase_fetch_row($result))
        {
            $settingsArray[$row[0]] = $row[1];
        }
        	    
         // установка полей, обязательных для заполнения 
        $isNecessarilyFieldsForRecordingStr = @$settingsArray["ORM_IsNecessarilyFieldsForRecording"];
        if($isNecessarilyFieldsForRecordingStr != null){
            $xmlObj = new SimpleXMLElement($isNecessarilyFieldsForRecordingStr);
            $requiredFields = $xmlObj->attributes(); 
            $settings->isRequiredSurname = $requiredFields["surname"] == "true";
            $settings->isRequiredName = $requiredFields["name"] == "true";
            $settings->isRequiredMiddlename = $requiredFields["middlename"] == "true";
            $settings->isRequiredPhone = $requiredFields["phone"] == "true";
            $settings->isRequiredEmail = $requiredFields["email"] == "true";
        }
        // установка цветов
        $colorsStr = @$settingsArray["ORM_Colors"];
        if($colorsStr != null){
            $xmlObj = new SimpleXMLElement($colorsStr);
            $colors = $xmlObj->attributes(); 
            $settings->borderColor = (string)$colors["borderColor"];
            $settings->backgroundColor = (string)$colors["backgroundColor"];
        }
        // язык
        $settings->language = @$settingsArray["ORM_Language"];
        //$settings->language = "ru_RU";
        
        // количество минут, отведенных на работу врача с одним пациентом
        $settings->CountMinutesForOnlineReceptin = (int) @$settingsArray["ORM_CountMinutesForOnlineReceptin"];
        
        // количество дней доступных для выбора пациентом, начиная с текущего
        $settings->CountDayFromRegistration = (int) @$settingsArray["ORM_CountDayFromRegistration"];
        
        // откуда загружать фото врачей
        $settings->ReadImagesFromDB = @$settingsArray["ORM_ReadImagesFromDB"] == "true";
        
        // нужно ли учитывать графики работы врачей
        $settings->isConsiderSchedule = @$settingsArray["ORM_IsConsiderSchedule"] == "true";
        
        if(key_exists("COUNTRYPHONECODE",$settingsArray))
        {
            $settings->countryPhoneCode = @$settingsArray["COUNTRYPHONECODE"];
        }
        else
        {
             $settings->countryPhoneCode = "38";
        }
        
        //график работы по отдельным дням
        $daysNames = array( 0 => "ORM_Sunday",
                            1 => "ORM_Monday",
                            2 => "ORM_Tuesday",
                            3 => "ORM_Wednesday",
                            4 => "ORM_Thursday",
                            5 => "ORM_Friday",
                            6 => "ORM_Saturday" );
        
        
        $settings->DaysAndTimeClinicWork=array();        
        foreach($daysNames as $dayName)
        {
            $dayStr = @$settingsArray[$dayName];
            if($dayStr != null)
            {
                $xmlObj = new SimpleXMLElement($dayStr);
                $day = $xmlObj->attributes(); 
                $oneDayTemp = new stdClass();
                $oneDayTemp->isWork = $day["isWork"] == "true";
                $oneDayTemp->beginTime = (string)$day["beginTime"];
                $oneDayTemp->endTime = (string)$day["endTime"];
                $settings->DaysAndTimeClinicWork[] = $oneDayTemp;
            }
        }
        
        ibase_free_query($query);
        ibase_free_result($result);
        
        //======================================
        
        //echo (count($settingsArray) );
        // если нет настроек, добавляем стандартные, и отправляем их клиенту
        if(count($settingsArray)<14)
        {
            $standartSettings = new OnlineRegistrationSettings;
            $standartSettings->language = "ru_RU";
            $standartSettings->isRequiredName = true;
            $standartSettings->isRequiredSurname = true;
            $standartSettings->isRequiredMiddlename = false;
            $standartSettings->isRequiredEmail = false;
            $standartSettings->isRequiredPhone = true;
            $standartSettings->ReadImagesFromDB = true;
            $standartSettings->borderColor = "0000FF";
            $standartSettings->backgroundColor = "FFFFFF";
            
            $standartSettings->CountDayFromRegistration = "30";
            $standartSettings->CountMinutesForOnlineReceptin = 60;
            $standartSettings->isConsiderSchedule = true;
            
            
            $dayNames = array(
                "Monday", 
				"Tuesday", 
				"Wednesday", 
				"Thursday",
				"Friday",
				"Saturday",
				"Sunday"
            );
            
            $DaysAndTimeClinicWorkArr = array();
            for($i=0; $i<7; $i++){
                $day = array();
                $isWork = $i<5 ? "true" : "false";
                $day[$dayNames[$i] ] = '<root isWork="'.$isWork.'" beginTime="8:00" endTime="18:00" />';
                $DaysAndTimeClinicWorkArr[] = $day;
            }
            
            $standartSettings->DaysAndTimeClinicWork = $DaysAndTimeClinicWorkArr;
            
            $savedSuccessfull = $this->saveSettings($standartSettings, $trans);
            
            if($savedSuccessfull)
            {
                $returnGet = $this->getSettings($trans); 
            }
            
        }
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
            $trans = null;
        }
        
        if($returnGet)
        {
            return $returnGet;
        }
        	
//file_put_contents("C:/tmp.txt", var_export($settings,true));    
        return $settings;
    }
    
    /**
    * OnlineRegistration::setSettings
    * 
    *
    *@param OnlineRegistrationSettings $settings
    * @param resource $trans
    *@return boolean
    */
    
    public function saveSettings($settings, $trans=null)
	{
        if(!$trans)
        {
            $connection = new Connection();
    	    $connection->EstablishDBConnection("main", false);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        } 
        
        // добавление/изменение цветов бекграунда и рамки
        $settings_sql = "update or insert into 
                            applicationsettings(parametername, parametervalue) 
                         values('ORM_Colors', '<Colors borderColor=\"$settings->borderColor\" backgroundColor=\"$settings->backgroundColor\"/>') 
                         matching(parametername)";
        $settings_query = ibase_prepare($trans, $settings_sql);
        ibase_execute($settings_query);
        ibase_free_query($settings_query);
        
        
        // установка полей, обязательных для заполнения
        //echo("  >>>> isReqSurname: ".$settings->isRequiredSurname);
        $settings_sql = "update or insert into 
                            applicationsettings(parametername, parametervalue) 
                         values('ORM_IsNecessarilyFieldsForRecording', 
                                '<IsNecessarilyFieldsForRecording surname=\"".($settings->isRequiredSurname?"true":"false")."\" name=\"".($settings->isRequiredName?"true":"false")."\" middlename=\"".($settings->isRequiredMiddlename?"true":"false")."\" phone=\"".($settings->isRequiredPhone?"true":"false")."\" email=\"".($settings->isRequiredEmail?"true":"false")."\"/>') 
                         matching(parametername)";
        $settings_query = ibase_prepare($trans, $settings_sql);
        ibase_execute($settings_query);
        ibase_free_query($settings_query);
        
        
        // установка времени работы по дням
        
        $DaysAndTimeClinicWork = $settings->DaysAndTimeClinicWork;
        //print_r($DaysAndTimeClinicWork);
        
        foreach($DaysAndTimeClinicWork as $currDay)
        {
            foreach($currDay as $dayName => $dayParamsXML)
            {
                $settings_sql = "update or insert into 
                                    applicationsettings(parametername, parametervalue) 
                                 values('ORM_".ucfirst($dayName)."', '$dayParamsXML') 
                                 matching(parametername)";
                $settings_query = ibase_prepare($trans, $settings_sql);
                ibase_execute($settings_query);
                ibase_free_query($settings_query);
            }
            
        }
        
        
        
        
        foreach(get_object_vars($settings) as $key => $value)
        {
            if( $key == "isConsiderSchedule" || 
                $key == "ReadImagesFromDB"
            )
            {
            $settings_sql = "update or insert into 
                                applicationsettings(parametername, parametervalue) 
                            values('ORM_".ucfirst($key)."', '".($value?"true":"false")."') 
                                matching(parametername)";
            $settings_query = ibase_prepare($trans, $settings_sql);
            ibase_execute($settings_query);
            ibase_free_query($settings_query);
            }
            elseif( $key == "language" || 
                $key == "CountDayFromRegistration" ||
                $key == "CountMinutesForOnlineReceptin"
            )
            {
            $settings_sql = "update or insert into 
                                applicationsettings(parametername, parametervalue) 
                            values('ORM_".ucfirst($key)."', '$value') 
                                matching(parametername)";
            $settings_query = ibase_prepare($trans, $settings_sql);
            ibase_execute($settings_query);
            ibase_free_query($settings_query);
            }
        }
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
            $trans = null;
        }
        return true;
	}
    
    
 
    /**
    *@return OnlineRegistrationDoctorRecord[]
    */
    public function getDoctors()
    {
        $connection = new Connection();
   	    $connection->EstablishDBConnection("main", false);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "";
        
        if($this->getSettings($trans)->ReadImagesFromDB)
        {
            $QueryText =
                        "select
                            d.ID,
                            d.SHORTNAME,
                            a.image
                        from
                            DOCTORS d
                            left join avatarimages a on (d.id = a.id)
                        where
                            ((d.dateworkend is null) or (d.dateworkend > current_date))
                        order by
                            d.SHORTNAME";
        }
        else
        {
            $QueryText =
                        "select
                            d.ID,
                            d.SHORTNAME
                        from
                            DOCTORS d
                        where
                            ((d.dateworkend is null) or (d.dateworkend > current_date))
                        order by
                            d.SHORTNAME";
        }
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
        while ($row = ibase_fetch_row($result))
        { 
            $obj = new OnlineRegistrationDoctorRecord;
            $obj->DOCTORID = $row[0];
            $obj->SHORTNAME = $row[1];
            
            $imgBlob = null;
            if($this->getSettings($trans)->ReadImagesFromDB)
            {
                $blob_info = ibase_blob_info($row[2]);
                if ($blob_info[0]!=0)
                {
                    $blob_hndl = ibase_blob_open($row[2]);
                    $imgBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
                    ibase_blob_close($blob_hndl);
                }
            }
            else
            {
                $pathToFile = dirname($_SERVER['SCRIPT_FILENAME'])."/toothfairyonline/doctors/".$row[0].".jpg";
                if(is_file($pathToFile))
                {
                    $buffer = file_get_contents($pathToFile);
                    $imgBlob = $buffer;
                }
            }
            
            $obj->IMG = base64_encode($imgBlob);
            
            $rows[] = $obj;
        }
        ibase_commit($trans);
        ibase_free_query($query);
        ibase_free_result($result);
        $connection->CloseDBConnection();
        return $rows;
    }
    
    /**
    *@param int $doctorId
    *@param string $startDate
    *@param string $endDate
    *@param resource $trans
    *@return object[]
    */
    public function getDoctorWorkDateTimes($doctorId, $startDate, $endDate, $trans = null) 
    {
        $result = array();
        $module = new CalendarModule();
        $arrSchedules = $module->getScheduleOnDoctors(array((int)$doctorId), $startDate, $endDate, $trans);        
        if(is_array($arrSchedules))
        {
            if(is_array($arrSchedules[0])) 
            {
                if(count($arrSchedules[0]) > 0 && is_object($arrSchedules[0][0]))
                {
                    if(is_array($arrSchedules[0][0]->daySchedule))
                    {
                        foreach($arrSchedules[0][0]->daySchedule as $day) 
                        {
                            $result[$day->day] = array('startTime' => $day->timeIntervals[0]->startTime,'endTime' => $day->timeIntervals[0]->endTime,'cabinetID' => $day->timeIntervals[0]->cabinetID);
                        }
                    }
                }
            }
        }
        return $result;
    }
    
    /**
    *@param string $doctorId
    *@param string $startDate
    *@param string $endDate
    *@return object[]
    */
    public function getNotWorkDays($doctorId, $startDate, $endDate) 
    {
        $connection = new Connection();
   	    $connection->EstablishDBConnection("main", false);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $result = array(); 
        if(is_null($doctorId)) 
        {
            $result = $this->getWorkTimesByAllDoctors($startDate, $endDate, $trans);
        } 
        else 
        {
            $days = $this->createDaysArray($startDate, $endDate);
            $worksDateTime = $this->getDoctorWorkDateTimes($doctorId, $startDate, $endDate, $trans);
            foreach($days as $day) 
            {
                if(!key_exists($day, $worksDateTime)) 
                {
                    $result[] = $day;
                }
            }
        }
                
        ibase_commit($trans);
        $connection->CloseDBConnection();
        return $result;
    }
    
    /**
    *@param string $dateStart
    *@param string $dateEnd
    *@param resource $trans
    *@return object[]
    */
    public function getWorkTimesByAllDoctors($dateStart, $dateEnd, $trans = null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
   	        $connection->EstablishDBConnection("main", false);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        $result = array();
        $module = new CalendarModule();
        
        $QueryText = format_for_query("select
                            d.ID
                        from
                            DOCTORS d
                          
                        where
                            ((d.dateworkend is null) or (d.dateworkend > current_date))");
        $query = ibase_prepare($trans, $QueryText);
        $res=ibase_execute($query);
        $rows = array();
        while ($row = ibase_fetch_row($res))
        { 
            $rows[] = (int)$row[0];
        }
        
        $schedules = $module->getScheduleOnDoctors($rows, $dateStart, $dateEnd, $trans);
        
        ibase_free_query($query);
        ibase_free_result($res);
                      
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }  
        
        return $schedules;
    }      
    
    /**
    *@param string $doctorId
    *@param string $date
    *@return object[]
    */
    public function getWorkTimes($doctorId, $date) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection("main", false);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
        $result = array();
          
        $module = new CalendarModule();
        if(is_null($doctorId)) 
        {
            $QueryText = format_for_query("select
                            d.ID
                        from
                            DOCTORS d
                          
                        where
                            ((d.dateworkend is null) or (d.dateworkend > current_date))");
            $query = ibase_prepare($trans, $QueryText);
            $res=ibase_execute($query);
            $rows = array();
            while ($row = ibase_fetch_row($res))
            { 
                $rows[] = (int)$row[0];
            }
            
            $schedules = $module->getScheduleOnDoctors($rows, $date, $date, $trans);
            for($i = 0; $i < count($schedules); $i++)
            {
                for($j = 0; $j < count($schedules[$i]); $j++)
                {
                    $QueryText = format_for_query("select BEGINOFTHEINTERVAL, ENDOFTHEINTERVAL from tasks where doctorid = '{0}' and taskdate = '{1}'", $schedules[$i][$j]->Doctor->ID, $date);
                    $query = ibase_prepare($trans, $QueryText);
                    $res=ibase_execute($query);
                    while ($row = ibase_fetch_row($res))
                    { 
                        $interv = new CalendarModuleInterval($row[0], $row[1]);
                        $schedules[$i][$j]->daySchedule[0]->subInterval($interv);
                    }
                }
            }
            
            ibase_free_query($query);
            ibase_free_result($res);
            
            ibase_commit($trans);
            $connection->CloseDBConnection();
            
            return $schedules;
            
        } 
        else 
        {
            $arrSchedules = $module->getScheduleOnDoctors(array((int)$doctorId), $date, $date, $trans);
           
            if(count($arrSchedules[0]) > 0)
            {
                $schedule = $arrSchedules[0][0]->daySchedule[0];
                
                foreach($schedule->timeIntervals as $int) 
                {
                    $QueryText = format_for_query("select beginoftheinterval, endoftheinterval from tasks where taskdate = '{0}' and doctorid = '{1}' and roomid = '{2}'", $date, $doctorId, $int->cabinetID);
                    $query = ibase_prepare($trans, $QueryText);
                    $res=ibase_execute($query);
                    while ($row = ibase_fetch_row($res))
                    {
                        $interval = new CalendarModuleInterval($row[0], $row[1]);
                        $schedule->subInterval($interval);
                    }
                    ibase_free_query($query);
                    ibase_free_result($res);
                }
                
                ibase_commit($trans);
                $connection->CloseDBConnection(); 
                                   
                return $schedule;
            }
            else 
            {

                ibase_commit($trans);
                $connection->CloseDBConnection(); 
                return null;
            }
        }
    }
    
    
    /** 
 	* @param string $fullName
 	* @param string $mobileNumber
    * @param resource $trans
    * @return int[]
	*/    
    public function checkExistsPatient($fullName, $mobileNumber, $trans = null) 
    {
        if(!$trans)
        {
            $connection = new Connection();
   	        $connection->EstablishDBConnection("main", false);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        // нужно перевести в нижний регистр именно с  mb_strtolower, иначе символи переведутся некорректно.
        $fullName = mb_strtolower($fullName);
        
	    $QueryText =
			"select p.id from PATIENTS p
                 where lower(p.fullname) like lower('$fullName')
                or p.mobilenumber containing ('$mobileNumber')
                or  (select list(pm.mobilenumber,'|') from patientsmobile pm where pm.patient_fk=p.id) like '%$mobileNumber%'";
        
        //file_put_contents("C:/tmp.txt", $QueryText);

        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $rows = array();
		while ($row = ibase_fetch_row($result))
		{
            $rows[] = $row[0];
		}
        
        ibase_free_query($query);
        ibase_free_result($result);
                    
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }  
        	
        return $rows;
    }
    
    
    
    /**
     * @param OnlineRegistrationRecord $task
     * @param boolean $isConsiderSchedule
     * @return OnlineRegistrationRecord
    */
    public function addTableTask($task, $isConsiderSchedule) 
    {
        $connection = new Connection();
        $connection->EstablishDBConnection("main", false);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        // ищем пациента в БД за несколькими параметрами: имя (в нижнем регистре) и номер телефона , 
        // если находим - записываем пользователя
        $ids = $this->checkExistsPatient($task->FULLNAME, $task->PHONE, $trans);
        
        // зарегистрированого пользователя не нашлось
        if(count($ids)==0 )
        {
            $QueryText = format_for_query("insert into PATIENTS (
                                                id, 
                                                firstname, 
                                                middlename, 
                                                lastname, 
                                                fullname, 
                                                shortname, 
                                                mobilenumber, 
                                                primaryflag, 
                                                email)
                                                VALUES (null, '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '0', '{6}')  RETURNING id",
                                                                $task->NAME,
                                                                        $task->MIDLENAME,
                                                                                $task->SURNAME,
                                                                                    $task->FULLNAME,
                                                                                            $task->SHORTNAME,
                                                                                                    $task->PHONE,
                                                                                                                $task->EMAIL);
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
            $id = '';
            while ($row = ibase_fetch_row($result))
            {
                $id = $row[0];
            }
            
            // Номер мобильного телефона добавляем и в таблицу Patients и в PatientsMobile
            //add mphone start
            $mphone_sql = "insert into 
                            patientsmobile(patient_fk, parent_fk, mobilenumber, senderactive, comments) 
                            values ($id, null, '$task->PHONE', 1, '')";
                                            
            $mphone_query = ibase_prepare($trans, $mphone_sql);
            ibase_execute($mphone_query);
            ibase_free_query($mphone_query);
            //add mphone end
            
        
        }else{ // есть пользователь
            $id = $ids[0];
        }
        
        $QueryText = format_for_query("select id from workplaces");
        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $workPlaceId = '';
        while ($row = ibase_fetch_row($result))
        {
            $workPlaceId = $row[0];
            break;
        }
        ibase_free_query($query);
        ibase_free_result($result);
        
        if($isConsiderSchedule)
        {
          $QueryText = format_for_query("insert into TASKS (id, TASKDATE, BEGINOFTHEINTERVAL, ENDOFTHEINTERVAL, PATIENTID, ROOMID, TASKTYPE, NOTICED, FACTOFVISIT, DOCTORID, WORKDESCRIPTION, COMMENT, WORKPLACEID) values (null, '{0}', '{1}', '{2}', '{3}', '{4}', '2', '0', '0', '{5}', '', '', {6}) returning TASKDATE, DOCTORID", $task->TASKTASKDATE, $task->TASKBEGINOFTHEINTERVAL, $task->TASKENDOFTHEINTERVAL, $id, $task->ROOMID, $task->DOCTORID, $workPlaceId);
        }else
        {
            if($task->DOCTORID == -1)
            {
                $QueryText = format_for_query("insert into TASKS (id, TASKDATE, BEGINOFTHEINTERVAL, ENDOFTHEINTERVAL, PATIENTID, ROOMID, TASKTYPE, NOTICED, FACTOFVISIT, DOCTORID, WORKDESCRIPTION, COMMENT, WORKPLACEID) values (null, '{0}', '{1}', '{2}', '{3}', (select first 1 id from rooms), '2', '0', '0', (select first 1 id from doctors), '', '', {6}) returning TASKDATE, DOCTORID", $task->TASKTASKDATE, $task->TASKBEGINOFTHEINTERVAL, $task->TASKENDOFTHEINTERVAL, $id, $task->ROOMID, $task->DOCTORID, $workPlaceId);
            }
            else
            {
                 $QueryText = format_for_query("insert into TASKS (id, TASKDATE, BEGINOFTHEINTERVAL, ENDOFTHEINTERVAL, PATIENTID, ROOMID, TASKTYPE, NOTICED, FACTOFVISIT, DOCTORID, WORKDESCRIPTION, COMMENT, WORKPLACEID) values (null, '{0}', '{1}', '{2}', '{3}', (select first 1 id from rooms), '2', '0', '0', '{5}', '', '', {6}) returning TASKDATE, DOCTORID", $task->TASKTASKDATE, $task->TASKBEGINOFTHEINTERVAL, $task->TASKENDOFTHEINTERVAL, $id, $task->ROOMID, $task->DOCTORID, $workPlaceId);
            }
        }

        $query = ibase_prepare($trans, $QueryText);
        $result=ibase_execute($query);
        $resTask=new OnlineRegistrationRecord;
        if ($row = ibase_fetch_row($result))
        {
            $resTask->TASKTASKDATE = $row[0];
            $resTask->DOCTORID = $row[1];
        }
        ibase_free_query($query);
        ibase_free_result($result);
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        if($success)
        {
             return $resTask;
        }
        else
        {
            return null;
        }
       
    }
    
    
  
}

?>
<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "Settings.php";


/*define('tfClientId','online');
$eHealthService = new EHealthService;
echo json_encode($eHealthService->createLegalEntity('as'),JSON_UNESCAPED_UNICODE);
*/
 
class EHealthService
{		
    public static $isDebugLog = false;
   
   
    public function SyncDictionaries()
    {   
        //DB CONNCETION OPEN
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
        $nowTime = time();
            
        //get sync last date
        $lastSyncTime = 0;
        $sql_getSyncTime = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthDictionariesLastSync'";
        $query_getSyncTime = ibase_prepare($trans, $sql_getSyncTime);
        $result_getSyncTime = ibase_execute($query_getSyncTime);	
        if ($row = ibase_fetch_row($result_getSyncTime))
        {
            $lastSyncTime = $row[0];			
        }
        ibase_free_query($query_getSyncTime);
        ibase_free_result($result_getSyncTime);
        
        if($nowTime - $lastSyncTime >  24 * 60 * 60)
        {
        
            $ch = curl_init();       
            curl_setopt($ch, CURLOPT_URL, Settings::EHEALTH_URL."/api/dictionaries");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
            
            //DICTIONARIES
            $result = json_decode($response);	
            
            
            //Dictionaries summ check
            $db_summ = 0;
            $cloud_summ = strlen($response);
            
            //get summ from db
            $sql_getsum = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthDictionariesSumm'";
            $query_getsum = ibase_prepare($trans, $sql_getsum);
            $result_getsum = ibase_execute($query_getsum);	
            if ($row = ibase_fetch_row($result_getsum))
            {
                $db_summ = $row[0];			
            }
            ibase_free_query($query_getsum);
            ibase_free_result($result_getsum);
            
            //check summ
            if($db_summ != $cloud_summ)
            {
                //DELETE
                /*
                $sql_clear = 'delete from EH_DICTIONARIES';
                $query_clear = ibase_prepare($trans, $sql_clear);
                ibase_execute($query_clear);
                ibase_free_query($query_clear);
                */
                
                
                for($i = 0; $i < count($result->data); ++$i)
                {
                    $dic = $result->data[$i];
                    $valuesJsonEcoded = json_encode($dic->values, JSON_UNESCAPED_UNICODE);
                    $valuesJson = json_decode($valuesJsonEcoded, true);
                    
                    foreach($valuesJson as $data => $label) 
                    {
                        //INSERT
                        //$sql_item = "insert into EH_DICTIONARIES (DICTIONARY, ITEM_DATA, ITEM_LABEL) values ('".$dic->name."', '".$data."', '".$label."')";
                        $sql_item = "update or insert into EH_DICTIONARIES (DICTIONARY, ITEM_DATA, ITEM_LABEL) values ('".$dic->name."', '".$data."', '".safequery($label)."') matching (DICTIONARY, ITEM_DATA)";
                        $query_item = ibase_prepare($trans, $sql_item);
                        ibase_execute($query_item);
                        ibase_free_query($query_item);
                    }
                }
                //set new summ
                $sql_setsum = "update or insert into APPLICATIONSETTINGS(PARAMETERNAME, PARAMETERVALUE) values('eHealthDictionariesSumm', $cloud_summ) matching (PARAMETERNAME)";
                $query_setsum = ibase_prepare($trans, $sql_setsum);
                ibase_execute($query_setsum);
                ibase_free_query($query_setsum);
            }
            
            //set new last sync
            $sql_setSyncTime = "update or insert into APPLICATIONSETTINGS(PARAMETERNAME, PARAMETERVALUE) values('eHealthDictionariesLastSync', $nowTime) matching (PARAMETERNAME)";
            $query_setSyncTime = ibase_prepare($trans, $sql_setSyncTime);
            ibase_execute($query_setSyncTime);
            ibase_free_query($query_setSyncTime);
                
        }
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();	
    }
    
	/**
    * @return Object
	*/
    public function GetDictionaries()
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        //$connection->EstablishDBConnection("main",false);
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $sql_getdic = "select DICTIONARY, ITEM_DATA, ITEM_LABEL from EH_DICTIONARIES order by DICTIONARY";
        $query_getdic = ibase_prepare($trans, $sql_getdic);
        $result_getdic = ibase_execute($query_getdic);	
        
        $resultObject = new stdClass;
        $dicName = '';
        
		while ($row = ibase_fetch_row($result_getdic))
        {
            if($dicName != $row[0])
            {
               $dicName = $row[0];
               $resultObject->{$dicName} = array();
            }
            $dicItem = new stdClass;
            $dicItem->data = $row[1];
            $dicItem->label = $row[2];	
            
            $resultObject->{$dicName}[] = $dicItem;	
        }
        ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $resultObject;
    }
   
   
    
    //REGIONS
    public function SyncRegions()
    {   
        //DB CONNCETION OPEN
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);	
        $nowTime = time();
        $token = '';
            
        //get sync last date
        $lastSyncTime = 0;
        $sql_getSyncTime = "select
                            (select first 1 PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthMISAccessToken'),
                            (select first 1 PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthRegionsLastSync')";
        $sql_getSyncTime .= ' from RDB$DATABASE';
        $query_getSyncTime = ibase_prepare($trans, $sql_getSyncTime);
        $result_getSyncTime = ibase_execute($query_getSyncTime);	
        if ($row = ibase_fetch_row($result_getSyncTime))
        {
            $token = $row[0];
            $lastSyncTime = $row[1];			
        }
        ibase_free_query($query_getSyncTime);
        ibase_free_result($result_getSyncTime);
        $authorization = "Authorization: Bearer $token";
        
        if($nowTime - $lastSyncTime >  24 * 60 * 60)
        {
        
            $ch = curl_init();       
            curl_setopt($ch, CURLOPT_URL, Settings::EHEALTH_URL."/api/uaddresses/search/regions");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
            $response = curl_exec($ch);
            curl_close($ch);
            
            //DICTIONARIES
            $result = json_decode($response);	
            
            
            //Dictionaries summ check
            $db_summ = 0;
            $cloud_summ = strlen($response);
            
            //get summ from db
            $sql_getsum = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthRegionsSumm'";
            $query_getsum = ibase_prepare($trans, $sql_getsum);
            $result_getsum = ibase_execute($query_getsum);	
            if ($row = ibase_fetch_row($result_getsum))
            {
                $db_summ = $row[0];			
            }
            ibase_free_query($query_getsum);
            ibase_free_result($result_getsum);
            
            //check summ
            if($db_summ != $cloud_summ)
            {
                
                for($i = 0; $i < count($result->data); ++$i)
                {
                    $dic = $result->data[$i];
                    
                    $sql_item = "update or insert into EH_ADR_REGIONS (NAME, KOATUU, REGION_ID) values ('".safequery($dic->name)."', '".$dic->koatuu."', '".$dic->id."') matching (KOATUU, REGION_ID)";
                    $query_item = ibase_prepare($trans, $sql_item);
                    ibase_execute($query_item);
                    ibase_free_query($query_item);
                }
                
                //set new summ
                $sql_setsum = "update or insert into APPLICATIONSETTINGS(PARAMETERNAME, PARAMETERVALUE) values('eHealthRegionsSumm', $cloud_summ) matching (PARAMETERNAME)";
                $query_setsum = ibase_prepare($trans, $sql_setsum);
                ibase_execute($query_setsum);
                ibase_free_query($query_setsum);
                
            }
            
            
            //set new last sync
            $sql_setSyncTime = "update or insert into APPLICATIONSETTINGS(PARAMETERNAME, PARAMETERVALUE) values('eHealthRegionsLastSync', $nowTime) matching (PARAMETERNAME)";
            $query_setSyncTime = ibase_prepare($trans, $sql_setSyncTime);
            ibase_execute($query_setSyncTime);
            ibase_free_query($query_setSyncTime);
                
        }
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();	
    }  
    
    
	/**
    * @return Object[]
	*/
    public function GetRegions()
    {
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $sql_getdic = "select NAME, KOATUU, REGION_ID from EH_ADR_REGIONS";
        $query_getdic = ibase_prepare($trans, $sql_getdic);
        $result_getdic = ibase_execute($query_getdic);	
        
        $resultObject = array();
        
		while ($row = ibase_fetch_row($result_getdic))
        {
            $dicItem = new stdClass;
            $dicItem->name = $row[0];
            $dicItem->koatuu = $row[1];	
            $dicItem->region_id = $row[2];	
            
            $resultObject[] = $dicItem;	
        }
        ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        
        return $resultObject;
    }
    
    
	/**
     * @param string $regionName
     * @param string $searchText
     * @return Object[]
	 */
    public function SearchDistricts($regionName, $searchText)
    {
        //DB CONNCETION OPEN
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $token = '';
            
        //get token
        $sql_getSyncTime = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthMISAccessToken'";
        $query_getSyncTime = ibase_prepare($trans, $sql_getSyncTime);
        $result_getSyncTime = ibase_execute($query_getSyncTime);	
        if ($row = ibase_fetch_row($result_getSyncTime))
        {
            $token = $row[0];			
        }
        ibase_free_query($query_getSyncTime);
        ibase_free_result($result_getSyncTime);
        $authorization = "Authorization: Bearer $token";
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        
        $ch = curl_init();
        
        $url = Settings::EHEALTH_URL."/api/uaddresses/search/districts?region=".urlencode($regionName)."&district=".urlencode($searchText);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        $result = json_decode($response);
        $resultObject = array();
        for($i = 0; $i < count($result->data); ++$i)
        {
            $dicItem = $result->data[$i];
            $resultObject[] = $dicItem;
                        
        }
        return $resultObject;
    }
    
   	/**
     * @param string $regionName
     * @param string $districtName
     * @param string $searchText
     * @return Object[]
	 */
    public function SearchSettelment($regionName, $districtName, $searchText)
    {
        //DB CONNCETION OPEN
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $token = '';
            
        //get token
        $sql_getSyncTime = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthMISAccessToken'";
        $query_getSyncTime = ibase_prepare($trans, $sql_getSyncTime);
        $result_getSyncTime = ibase_execute($query_getSyncTime);	
        if ($row = ibase_fetch_row($result_getSyncTime))
        {
            $token = $row[0];			
        }
        ibase_free_query($query_getSyncTime);
        ibase_free_result($result_getSyncTime);
        $authorization = "Authorization: Bearer $token";
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        
        $ch = curl_init();
        
    
        //curl_setopt($ch, CURLOPT_URL, );
        $url = Settings::EHEALTH_URL."/api/uaddresses/search/settlements?region=".urlencode($regionName)."&settlement_name=".urlencode($searchText);
        
		/*if(EHealthService::$isDebugLog)
		{
		      file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export(Settings::EHEALTH_URL."/api/uaddresses/search/settlements?region=".$regionName."&settlement_name=".$searchText,true));
		}*/
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        $result = json_decode($response);
        $resultObject = array();
        for($i = 0; $i < count($result->data); ++$i)
        {
            $dicItem = $result->data[$i];
            $resultObject[] = $dicItem;
                        
        }
        return $resultObject;
    }
    
    
   	/**
     * @param string $clinic_id
     * @return string
	 */
    public function getJson($clinic_id) //sqlite
    { 
   	    $connection = new Connection();
	    $connection->EstablishDBConnection();	
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
		$mspObject = new stdClass;
        
        // GET MSP GENERAL DATA INFO **** START ****
		$sql_msp ="select ID,
                           NAME,
                           LEGALNAME,
                           CODE,
                           EMAIL,
                           SHORT_NAME,
                           MSP_TYPE,
                           OWNER_PROPERTY_TYPE,
                           LEGAL_FORM,
                           KVEDS,
                           ADDRESSLEGAL_ZIP,
                           ADDRESSLEGAL_TYPE,
                           ADDRESSLEGAL_COUNTRY,
                           ADDRESSLEGAL_AREA,
                           ADDRESSLEGAL_REGION,
                           ADDRESSLEGAL_SETTLEMENT,
                           ADDRESSLEGAL_SETTLEMENT_TYPE,
                           ADDRESSLEGAL_SETTLEMENT_ID,
                           ADDRESSLEGAL_STREET_TYPE,
                           ADDRESSLEGAL_STREET,
                           ADDRESSLEGAL_BUILDING,
                           ADDRESSLEGAL_APRT,
                           PHONE_TYPE,
                           PHONE,
                           ACR_CATEGORY,
                           ACR_ISSUED_DATE,
                           ACR_EXPIRY_DATE,
                           ACR_ORDER_NO,
                           ACR_ORDER_DATE,
                           CONSENT_SIGN,
                           CONSENT_TEXT
                    
                           from clinicregistrationdata
                           where id = $clinic_id";		
		$query_msp = ibase_prepare($trans, $sql_msp);
		$result_msp = ibase_execute($query_msp);
		while ($row = ibase_fetch_row($result_msp))
		{
			$mspObject->name = $row[2];
            $mspObject->short_name = $row[5];
			$mspObject->public_name = $row[1];
            $mspObject->type = $row[6];
            $mspObject->owner_property_type = $row[7];
            $mspObject->legal_form = $row[8];
			$mspObject->edrpou = "$row[3]";
            
            
            $mspObject->kveds = array();
            //to array kveds
            $kvedsArray = explode("|", $row[9]);
            $mspObject->kveds = $kvedsArray;
            
            
            $mspObject->addresses = array();
            //to array adress
            $address = new stdClass;
            $address->type= $row[11];
            $address->country= $row[12];
            
            $address->area= $row[13];
            $address->region= $row[14];
            $address->settlement= $row[15];
            $address->settlement_type= $row[16];
            $address->settlement_id= $row[17];
            
            $address->street_type= $row[18];
            $address->street= $row[19];
            $address->building= $row[20];
            $address->apartment= $row[21];
            $address->zip = $row[10];
			$mspObject->addresses[] = $address;
            
            // GET ADDITIONAL ADDRESSES ******* START *******
            $sql_addresses ="select
         
                            ZIP, 
                            ADDRESS_TYPE, 
                            COUNTRY, 
                            AREA, 
                            REGION, 
                            SETTLEMENT, 
                            SETTLEMENT_TYPE,
                            SETTLEMENT_ID, 
                            STREET_TYPE, 
                            STREET, 
                            BUILDING, 
                            APRT
                            
                    from EH_MSP_ADDRESSES
                    where FK_CLINICREGISTRATIONDATA = $clinic_id";		
    		$query_addresses = ibase_prepare($trans, $sql_addresses);
    		$result_addresses = ibase_execute($query_addresses);
    		while ($addresses_row = ibase_fetch_row($result_addresses))
    		{
                $mspAddress = new stdClass;
    			$mspAddress->type = $addresses_row[1];
    			$mspAddress->country = $addresses_row[2];
    			$mspAddress->area = $addresses_row[3];
                $mspAddress->region = $addresses_row[4];
                $mspAddress->settlement = $addresses_row[5];
                $mspAddress->settlement_type = $addresses_row[6];
                $mspAddress->settlement_id = $addresses_row[7];
                $mspAddress->street_type = $addresses_row[8];
                $mspAddress->street = $addresses_row[9];
                $mspAddress->building = $addresses_row[10];
                $mspAddress->apartment = $addresses_row[11];
    			$mspAddress->zip = $addresses_row[0];
                
		        $mspObject->addresses[] = $mspAddress;
    		}
    		ibase_free_query($query_addresses);
    		ibase_free_result($result_addresses);
            // GET ADDITIONAL ADDRESSES *******  END  *******
            
            
            
            
            $mspObject->phones = array();
            //to array phones
            $phone = new stdClass;
            $phone->type=$row[22];
            $phone->number='+'.$row[23];
			$mspObject->phones[] = $phone;
            
            // GET MSP ADDITIONAL PHONES **** START ****
            $sql_mspPhones ="select 
                            PHONE, 
                            PHONE_TYPE
                        from EH_MSP_PHONE
                    where FK_CLINICREGISTRATIONDATA = $clinic_id";		
  		    $query_mspPhones = ibase_prepare($trans, $sql_mspPhones);
      		$result_mspPhones = ibase_execute($query_mspPhones);
      		while ($mspPhone_row = ibase_fetch_row($result_mspPhones))
      		{
                $mspPhone = new stdClass;
     			//$obj->ID = $row[0];
     			$mspPhone->type = $mspPhone_row[1];
     			$mspPhone->number = '+'.$mspPhone_row[0];
                   
                $mspObject->phones[] = $mspPhone;
      		}		
      		ibase_free_query($query_mspPhones);
      		ibase_free_result($result_mspPhones);
            // GET MSP ADDITIONAL PHONES **** END ****
            
            
            
            
            
            
            
            
            
			$mspObject->email = $row[4];
            
             // GET MSP OWNER DATA INFO **** START ****
            $mspOwner = new stdClass;
    		$sql_owner ="select first 1  
                                ID,
                                FIRSTNAME,
                                MIDDLENAME,
                                LASTNAME,
                                TAX_ID,
                                BIRTH_DATE,
                                BIRTH_PLACE,
                                GENDER,
                                EMAIL,
                                USER_POSITION,
                                PHONE_TYPE,
                                PHONE,
                                DOC_TYPE,
                                DOC_NUMBER
                    from ACCOUNT
                    where IS_OWNER = 1";		
    		$query_owner = ibase_prepare($trans, $sql_owner);
    		$result_owner = ibase_execute($query_owner);
    		while ($owner_row = ibase_fetch_row($result_owner))
    		{
    			//$obj->ID = $row[0];
    			$mspOwner->first_name = $owner_row[1];
    			$mspOwner->last_name = $owner_row[3];
    			$mspOwner->second_name = $owner_row[2];
    			$mspOwner->tax_id = $owner_row[4];
                $mspOwner->birth_date = $owner_row[5];//.'T00:00:00.000Z';
                $mspOwner->birth_place = $owner_row[6];
                $mspOwner->gender = $owner_row[7]==0?'MALE':'FEMALE';
                $mspOwner->email = $owner_row[8];
                
                //to array documents
                $mspOwner->documents = array();
                $document = new stdClass;
                $document->type=$owner_row[12];
                $document->number=$owner_row[13];
    			$mspOwner->documents[] = $document;
                
                //to array phones
                $mspOwner->phones = array();
                $phone = new stdClass;
                $phone->type=$owner_row[10];
                $phone->number='+'.$owner_row[11];
    			$mspOwner->phones[] = $phone;
                
                // GET OWNERS ADDITIONAL PHONES **** START ****
        		$sql_ownerPhones ="select 
                            PHONE, 
                            PHONE_TYPE
                        from ACCOUNTSMOBILE
                    where ACCOUNT_FK = $owner_row[0]";		
        		$query_ownerPhones = ibase_prepare($trans, $sql_ownerPhones);
        		$result_ownerPhones = ibase_execute($query_ownerPhones);
        		while ($ownerPhone_row = ibase_fetch_row($result_ownerPhones))
        		{
                    $ownerPhone = new stdClass;
        			//$obj->ID = $row[0];
        			$ownerPhone->type = $ownerPhone_row[1];
        			$ownerPhone->number = '+'.$ownerPhone_row[0];
                   
   			        $mspOwner->phones[] = $ownerPhone;
        		}		
        		ibase_free_query($query_ownerPhones);
        		ibase_free_result($result_ownerPhones);
                // GET OWNERS ADDITIONAL PHONES **** END ****
                
                $mspOwner->position = $owner_row[9];
    		}		
            $mspObject->owner = $mspOwner;
    		ibase_free_query($query_owner);
    		ibase_free_result($result_owner);
            // GET MSP GENERAL DATA INFO **** END ****
            
            $mspObject->medical_service_provider = new stdClass;
            // GET MSP LICENSES DATA INFO **** START ****
    		$sql_lic ="select 
                            ID, 
                            LICENSE_NUMBER, 
                            ISSUED_BY, 
                            ISSUED_DATE, 
                            EXPIRY_DATE, 
                            ACTIVE_FROM_DATE, 
                            WHAT_LICENSED, 
                            ORDER_NO
                        from EH_MSP_LICENSES
                        where FK_CLINICREGISTRATIONDATA = $clinic_id";		
    		$query_lic = ibase_prepare($trans, $sql_lic);
    		$result_lic = ibase_execute($query_lic);
            $licenses = array();
    		while ($lic_row = ibase_fetch_row($result_lic))
    		{
                $lic = new stdClass;
    			//$obj->ID = $row[0];
    			$lic->license_number = $lic_row[1];
    			$lic->issued_by = $lic_row[2];
    			$lic->issued_date = $lic_row[3];//.'T00:00:00.000Z';
    			$lic->expiry_date = $lic_row[4];//.'T00:00:00.000Z';
                $lic->active_from_date = $lic_row[5];
                //$lic->kved = $lic_row[5];
                $lic->what_licensed = $lic_row[6];
                $lic->order_no = $lic_row[7];
               
                $licenses[] = $lic;
    		}		
            $mspObject->medical_service_provider->licenses = $licenses;
    		ibase_free_query($query_lic);
    		ibase_free_result($result_lic);
            // GET MSP LICENSES DATA INFO **** END ****
            
            //accreditation
            $mspAccred = new stdClass;
            $mspAccred->category = $row[24];
            $mspAccred->issued_date = $row[25];//.'T00:00:00.000Z';
            $mspAccred->expiry_date = $row[26];//.'T00:00:00.000Z';
            $mspAccred->order_no = $row[27];
            $mspAccred->order_date = $row[28];//.'T00:00:00.000Z';
            $mspObject->medical_service_provider->accreditation = $mspAccred;
            
            //SECURITY OBJECT
            $security = new stdClass;
            $security->redirect_uri = Settings::EHEALTH_REDIRECT_URL;//'http://example.com';
            $mspObject->security = $security;
        
            //OFFER OBJECT
            $offer = new stdClass;
            $offer->consent = $row[29]==1;
            //$offer->consent_text = $row[30];
            $offer->consent_text = "Зобов'язуюсь дотримуватись Регламенту функціонування системи eHealth. Підтверджую достовірність вказаних мною даних та добровільність надання цих даних. Усвідомлюю, що у випадку надання мною невірної інформації, мені може бути відмовлено у реєстрації в системі eHealth.";
            $mspObject->public_offer = $offer;
		}		
		ibase_free_query($query_msp);
		ibase_free_result($result_msp);
        // GET MSP GENERAL DATA INFO **** END ****
        
        
        $json = json_encode($mspObject, JSON_UNESCAPED_UNICODE);
		ibase_commit($trans);
		$connection->CloseDBConnection();
		if(EHealthService::$isDebugLog)
		{     
			  $nowDate = new DateTime();
		      $timestamp = $nowDate->format("Y-m-d H:i:s");
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n</br><b>-".$timestamp."-MAKE-JSON---------------------------------------------------------------------------</b></br>"); 
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n".$json);     
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n</br><b>-------------------------------------------------------------------------------------------------------------------------</b></br>"); 
		}
        
        return $json;
    }
    
   	/**
    * @return Object
	*/
    public function createLegalEntity()
    {
        //$file = file_get_contents('rAze5qQoQ0.p7s', true);
        
        $file = file_get_contents(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/rAze5qQoQ0', true);
        $file64 = base64_encode($file);
           
		if(EHealthService::$isDebugLog)
		{
			  $nowDate = new DateTime();
		      $timestamp = $nowDate->format("Y-m-d H:i:s");
              file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n</br><b>-".$timestamp."--TO-EHEALTH-------------------------------------------------------------------------</b></br>"); 
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n".$file64);     
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n</br><b>--------------------------------------------------------------------------------------------------------------------------</b></br>");
		}
                        
        $body = '{"signed_legal_entity_request":"'.$file64.'","signed_content_encoding":"base64"}';
        
        $ch = curl_init();
        
        //get token from db **** START ****
        $connection = new Connection();
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $token = '';
            
        //get token
        $sql_getSyncTime = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthMISAccessToken'";
        $query_getSyncTime = ibase_prepare($trans, $sql_getSyncTime);
        $result_getSyncTime = ibase_execute($query_getSyncTime);	
        if ($row = ibase_fetch_row($result_getSyncTime))
        {
            $token = $row[0];			
        }
        ibase_free_query($query_getSyncTime);
        ibase_free_result($result_getSyncTime);
        $authorization = "Authorization: Bearer $token";
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        //get token from db **** END ****
        
        curl_setopt($ch, CURLOPT_URL, Settings::EHEALTH_URL."/api/legal_entities");

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

        $response = curl_exec($ch);
        
		if(EHealthService::$isDebugLog)
		{
			  $nowDate = new DateTime();
		      $timestamp = $nowDate->format("Y-m-d H:i:s");
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n</br><b>-".$timestamp."--FROM-EHEALTH---------------------------------------------------------------------</b></br>"); 
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n".$response);     
		      file_put_contents(dirname(__FILE__)."/log.txt", file_get_contents(dirname(__FILE__)."/log.txt")."\n</br><b>---------------------------------------------------------------------------------------------------------------------------</b></br>");     
		      //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($response,true));
		}
        
        //@unlink(dirname(__FILE__).'/'.Settings::FILE_STORAGE.'/rAze5qQoQ0');
        
        $resultObject = new stdClass;
        $resultObject->jsonString = $response;
        $resultObject->json = json_decode($response);
        
        return $resultObject;
    }
    
    
   	/**
     * @param string $eh_id
    * @return Object
	*/
    public function getLegalEntityStatus($eh_id)
    {
        $ch = curl_init();
        
        //get token from db **** START ****
        $connection = new Connection();
        //$connection->EstablishDBConnection("main",false);
	    $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $token = '';
            
        //get token
        $sql_getSyncTime = "select PARAMETERVALUE from APPLICATIONSETTINGS where PARAMETERNAME = 'eHealthMISAccessToken'";
        $query_getSyncTime = ibase_prepare($trans, $sql_getSyncTime);
        $result_getSyncTime = ibase_execute($query_getSyncTime);	
        if ($row = ibase_fetch_row($result_getSyncTime))
        {
            $token = $row[0];			
        }
        ibase_free_query($query_getSyncTime);
        ibase_free_result($result_getSyncTime);
        $authorization = "Authorization: Bearer $token";
        
        $success = ibase_commit($trans);
        $connection->CloseDBConnection();
        //get token from db **** END ****
        
        //curl_setopt($ch, CURLOPT_URL, Settings::EHEALTH_URL."/api/legal_entities?id=".$eh_id);
        curl_setopt($ch, CURLOPT_URL, Settings::EHEALTH_URL."/api/legal_entities/".$eh_id);
	    /*if(EHealthService::$isDebugLog)
		{
		      file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export(Settings::EHEALTH_URL."/api/legal_entities?id=".$eh_id,true));
		}*/
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        
        $response = curl_exec($ch);
        
	    /*if(EHealthService::$isDebugLog)
		{
		      file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n".var_export($response,true));
		}*/
        
        return json_decode($response);
    }
    
   	/**
    * @param string $clinic_id
    * @param string $eh_id
    * @param string $status
    * @param string $mis_client_id
    * @param string $client_id
    * @param string $client_secret
    * @return Boolean
	*/
    public function updateClinicInfoStatus($clinic_id, $eh_id, $status, $mis_client_id, $client_id, $client_secret)
    {
   	    $connection = new Connection();
	    $connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $sql = "update CLINICREGISTRATIONDATA
                    set
                        EH_ID = ".nullcheck($eh_id,true).",
                        EH_STATUS = ".nullcheck($status,true).",
                        EH_CREATED_BY_MIS_CLIENT_ID = ".nullcheck($mis_client_id,true).",
                        EH_CLIENT_ID = ".nullcheck($client_id,true).",
                        EH_CLIENT_SECRET = ".nullcheck($client_secret,true)."
                    where (ID = ".$clinic_id.")";
        $query = ibase_prepare($trans, $sql);
        ibase_execute($query);
        ibase_free_query($query);
        
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();
        
        return $result;
    }
    
    
}

?>
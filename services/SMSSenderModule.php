<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "smsclient.class.php";

$queueCapacity = 10;

class Task
{
    /**
     * @var int
     */ 
    var $id;
    /**
     * @var string
     */ 
    var $date;
    /**
     * @var string
     */ 
    var $startTime;
    /**
     * @var string
     */ 
    var $endTime;
    /**
     * @var int
     */ 
    var $patientid;
    /**
     * @var string
     */ 
    var $phone;
    /**
     * @var string
     */ 
    var $shortName;
    /**
     * @var string
     */ 
    var $fullName;
    /**
     * @var string
     */ 
    var $firstname;
    /**
     * @var string
     */ 
    var $lastname;
    /**
     * @var string
     */ 
    var $patronymic;
}

class SMSMessage
{
    /**
     * @var int
     */ 
    var $id;
    /**
     * @var string
     */ 
    var $sms_id;
    /**
     * @var string
     */ 
    var $phone;
    /**
     * @var int
     */ 
    var $response;
    /**
     * @var string
     */ 
    var $message;
}

class SMSSenderModuleMessage extends SMSMessage
{
    /**
     * @var string
     */ 
    var $errormsg;
    /**
     * @var int
     */ 
    var $patientid;
    /**
     * @var string
     */ 
    var $createDate;
    /**
     * @var string
     */ 
    var $scheduledTime;
    /**
     * @var string
     */ 
    var $serverSendTime;
    /**
     * @var string
     */ 
    var $smsSendTime;
    /**
     * @var string
     */ 
    var $deliveryTime;
}

class SMSSenderModulePatient
{
    /**
     * @var int
     */ 
    var $id;
    /**
     * @var string
     */ 
    var $name;    
    /**
     * @var string
     */ 
    var $phone;
}

class SMSSenderModuleFilter
{
    const MORE = 0;
    const MORE_EQUAL = 1;
    const LESS = 2;
    const LESS_EQUAL = 3;
    const EQUAL = 4;
    const LIKE = 5;
    const BETWEEN = 6;
    const IN = 7;   
    
    /**
     * @var int
     */ 
    var $type;
    /**
     * @var string
     */ 
    var $field; 
    /**
     * @var string
     */ 
    var $value;    
    /**
     * @var string
     */ 
    var $secontvalue;
}

/**
 * getSettings() получение из базы всех настроек в ассоциативный массив
 * 
 * @param resource $trans
 * @return array
 */
function getSettings($trans)
{
    $QueryText = "select PARAMETERNAME, PARAMETERVALUE from APPLICATIONSETTINGS where parametername like 'SMS%'";
    $query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    $settings = array();
    while($row = ibase_fetch_row($result))
    {
        $settings[$row[0]] = $row[1];
    }
    ibase_free_query($query);
    return $settings;
}

/**
 * findNewTasks()
 * 
 * @param resource $trans
 * @param string $startDate
 * @param string $endDate
 * @param string $startTime
 * @return Task[]
 */
function findNewTasks($trans, $startDate, $endDate, $startTime)
{
    $QueryText = "select 
                    tasks.id, 
                    tasks.taskdate, 
                    tasks.beginoftheinterval, 
                    tasks.endoftheinterval, 
                    patientsmobile.mobilenumber,
                    PATIENTS.FULLNAME, 
                    PATIENTS.SHORTNAME, 
                    patients.firstname, 
                    patients.lastname, 
                    patients.middlename, 
                    patients.id
        from tasks left join smssender on (tasks.id = smssender.taskid)
            inner join PATIENTS on (tasks.PATIENTID = PATIENTS.ID)
            left join patientsmobile on (patientsmobile.patient_fk = patients.id)
        where smssender.id is null and ((tasks.taskdate > '$startDate'
            or (tasks.taskdate = '$startDate' and tasks.beginoftheinterval >= '$startTime'))
                and tasks.taskdate <= '$endDate') 
            and tasks.smsstatus = 1
            and patientsmobile.senderactive = 1
            and patientsmobile.mobilenumber != ''
            and patientsmobile.mobilenumber is not null";
    $query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    $tasks = array();
    while($row = ibase_fetch_row($result))
    {
        $task = new Task;
        $task->id = $row[0];
        $task->date = $row[1];
        $task->startTime = $row[2];
        $task->endTime = $row[3];
        $phone = preg_replace('/\D/', '', $row[4]);
        $task->fullName = $row[5];
        $task->shortName =$row[6];
        $task->firstname =$row[7];
        $task->lastname = $row[8];
        $task->patronymic=$row[9];
        $task->patientid =$row[10];
    //    if (strlen($phone) == 10)
//            $phone = '38'.$phone;
//        if (strlen($phone) == 11)
//            $phone = '3'.$phone;
//        if (strlen($phone) == 12)
//        {
            $task->phone = $phone;
            $tasks[] = $task;
      //  }
    }
    ibase_free_query($query);
    return $tasks;
}

/**
 * findNewDipanserTasks()
 * 
 * @param resource $trans
 * @param string $endDate
 * @return Task[]
 */
function findNewDipanserTasks($trans, $endDate)
{
    $QueryText = "select 
                    tasksdispanser.id,
                    tasksdispanser.taskdate,
                    patientsmobile.mobilenumber,
                    PATIENTS.FULLNAME, 
                    PATIENTS.SHORTNAME, 
                    patients.firstname, 
                    patients.lastname, 
                    patients.middlename, 
                    patients.id
        from tasksdispanser
            left join smssender on (tasksdispanser.id = smssender.tasksdispanserid)
            inner join PATIENTS on (tasksdispanser.PATIENT_ID = PATIENTS.ID)
            left join patientsmobile on (patientsmobile.patient_fk = patients.id)
        where smssender.id is null
            and tasksdispanser.taskdate <= '$endDate'
            and tasksdispanser.smsstatus = 1
            and tasksdispanser.task_id is null
            and patientsmobile.senderactive = 1";
    $query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    $tasks = array();
    while($row = ibase_fetch_row($result))
    {
        $task = new Task;
        $task->id = $row[0];
        $task->date = $row[1];
        $phone = preg_replace('/\D/', '', $row[2]);
        $task->fullName = $row[3];
        $task->shortName =$row[4];
        $task->firstname =$row[5];
        $task->lastname = $row[6];
        $task->patronymic=$row[7];
        $task->patientid =$row[8];
    //    if (strlen($phone) == 10)
//            $phone = '38'.$phone;
//        if (strlen($phone) == 11)
//            $phone = '3'.$phone;
//        if (strlen($phone) == 12)
//        {
            $task->phone = $phone;
            $tasks[] = $task;
      //  }
    }
    ibase_free_query($query);
    return $tasks;
}

/**
 * createSMSFromTasks()
 * 
 * @param resource $trans
 * @param Task[] $tasks
 * @param string $template
 * @param boolean $isSMSGinMobile 
 * @return void
 */
function createSMSFromTasks($trans, $tasks, $template, $isSMSGinMobile)
{
    foreach($tasks as $task)
    {
        ///$task = new Task;
        $serch = array('%date%', '%time%', '%short%', '%full%', '%firstname%', '%lastname%', '%patronymic%');
        $replace = array(date('d.m.y', strtotime($task->date)), date('H:i', strtotime($task->startTime)), $task->shortName, $task->fullName, $task->firstname, $task->lastname, $task->patronymic);
        $message = str_replace($serch, $replace, $template);
        
        $initialResponseCode = 1;
        if($isSMSGinMobile)
        {
            $initialResponseCode = 100;
        }
        
        
        $QueryText = format_for_query("insert into smssender(taskid, patientid, phone, smsmessage, SCHEDULEDTIME, responsecode) 
                values({0}, {1}, {2}, '{3}', '{4}', {5})",
            $task->id, $task->patientid, $task->phone, $message, date('Y-m-d H:i:s'), $initialResponseCode);
        //echo "\n$QueryText\n";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
    }
    ibase_commit_ret($trans);
}

/**
 * createSMSFromDispanserTasks()
 * 
 * @param resource $trans
 * @param Task[] $tasks
 * @param string $template
 * @return void
 */
function createSMSFromDispanserTasks($trans, $tasks, $template)
{
    foreach($tasks as $task)
    {
        ///$task = new Task;
        $serch = array('%date%', '%time%', '%short%', '%full%', '%firstname%', '%lastname%', '%patronymic%');
        $replace = array(date('d.m.y', strtotime($task->date)), date('H:i', strtotime($task->startTime)), $task->shortName, $task->fullName, $task->firstname, $task->lastname, $task->patronymic);
        $message = str_replace($serch, $replace, $template);
        
        $QueryText = format_for_query("insert into smssender(tasksdispanserid, patientid, phone, smsmessage, SCHEDULEDTIME) values({0}, {1}, {2}, '{3}', '{4}')",
            $task->id, $task->patientid, $task->phone, $message, date('Y-m-d H:i:s'));
        //echo "\n$QueryText\n";
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
    }
    ibase_commit_ret($trans);
}


/**
 * getActiveSMS()
 * 
 * @param resource $trans
 * @param boolean $isSMSGinMobile 
 * @return SMSMessage[]
 */
function getActiveSMS($trans, $isSMSGinMobile)
{
    global $queueCapacity;
    $QueryText = "select first $queueCapacity smssender.id, smssender.phone, smssender.smsmessage, smssender.responsecode  from smssender where smssender.serversendtime is null ";
    if($isSMSGinMobile)
    {
        $QueryText .= ' and smssender.responsecode > 99'; 
    }        
    else
    {
        $QueryText .= ' and (smssender.responsecode < 100 or smssender.responsecode is null)'; 
    }
    $query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    $msgs = array();
    while($row = ibase_fetch_row($result))
    {
        $msg = new SMSMessage();
        $msg->id = $row[0];
        $msg->phone = $row[1];
        $msg->message = $row[2];
        $msg->response = $row[3];
        $msgs[] = $msg;
    }
    
    
    ibase_free_query($query);
    ibase_free_result($result);
    return $msgs;                
}

/**
 * sendSMS()
 * 
 * @param SMSClient $client
 * @param SMSMessage $sms
 * @param string $senderName
 * @param resource $trans
 * @param bool $translit
 * @return void
 */
function sendSMS($client, $sms, $senderName, $trans, $translit = false)
{
    $text = $translit ? $client->translit($sms->message) : $sms->message;
    $sms->sms_id = $client->sendSMS(array($sms->phone), $text, $senderName != null ? $senderName : "" );
    $QueryText = "";
    if ($sms->sms_id != null && !$client->hasErrors())
    {
        $len = mb_strlen($text, 'utf-8');
        $smscount = ceil($len / ($translit ? ($len <= 160 ? 160 : 153) : ($len <= 70 ? 70 : 67)));
        $QueryText = "update smssender set smsid = '$sms->sms_id', serversendtime = current_timestamp, responsecode = 1".($translit ? (", smsmessage = '".safequery($text)."'") : ''). ", smscount = $smscount where id = $sms->id";
    }
    else
    {
        $QueryText = "update smssender set serversendtime = current_timestamp, responsecode = -1, errormessage = '".safequery($client->getErrorString())."' where id = $sms->id";
    }
    //echo "\n".$QueryText;
    $query = ibase_prepare($trans, $QueryText);
    ibase_execute($query);
    ibase_free_query($query);
    ibase_commit_ret($trans);
}

/**
 * checkSMS()
 * 
 * @param SMSClient $client
 * @param resource $trans
 * @return void
 */
function checkSMS($client, $trans)
{
    global $queueCapacity;
    if ($queueCapacity == null) $queueCapacity = 10;
    $QueryText = "select first $queueCapacity id, smsid, responsecode,PHONE from smssender where 
        smsid is not null 
        and (responsecode is null 
        or responsecode = 0 
        or responsecode = 1
        or responsecode = 2
        or responsecode = 5
        or responsecode = 50
        or responsecode = 96
        or responsecode = -99)
        and serversendtime > dateadd(-1 day to current_timestamp)
        
        and responsecode != 100
        and responsecode != 200
        and responsecode != 500
        
        order by responsecode, serversendtime";
    $query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    while($row = ibase_fetch_row($result))
    {
        $id = $row[0];
        $sms_id = $row[1];
        $response = $row[2];
        $recipient = $row[3];
        
        $status = $client->receiveSMS($recipient, $sms_id);
        if ($client->hasErrors())
        {
            $QueryText = "update smssender set responsecode = -1, ERRORMESSAGE = ".safequery($client->getErrorString())." where id = $id";
        }
        else
        
        {
            $code = $status;

            switch ($code)
            {
                case "2":
                case "SENT": //Отправлено
                case "MESSAGE_IS_SENT": //Отправлено\
                 case "accepted": //Отправлено
                    $QueryText = "update smssender set responsecode = '2',SMSSENDTIME = current_timestamp where id = $id";
                	break;
                case "3":
                case "DELIVERED": //Доставлено
                case "MESSAGE_IS_DELIVERED": //Доставлено 
                case "delivered": //Доставлено
                    $QueryText = "update smssender set responsecode = '3',SMSSENDTIME = current_timestamp, DELIVERYTIME = current_timestamp where id = $id";
                	break;
                case "0":
                case "PENDING": //Ожидает (Сообщение в очереди на отправку.)
                case "MESSAGE_IN_QUEUE": //сообщение поставлено в очередь на отправку
                case "queued": //Сообщение находится в очереди
                    $QueryText = "update smssender set responsecode = '0' where id = $id";
                    break;
                case "1":
                case "STOPED": //Отправляется (Сообщение остановлено системой. Чаще всего это происходит, если у клиента недостаточно средств для отправки сообщения.)
                case "NO_DATA ": //не передан XML
                case "MESSAGE_TOO_LONG": //ошибка формата переданного XML
                case "NO_MESSAGE ": //пустое сообщение для оправки
                case "MAX_MESSAGES_COUNT": //превышено максимальное количество респондентов в одном запросе
                case "smsc submit": //Сообщение доставлено в SMSC
                      $QueryText = "update smssender set responsecode = '1' where id = $id";
                      break;
                case "5":
                case "EXPIRED": //Нет на связи (Срок жизни сообщения вышел, сообщения не доставлено получателю.)
                case "MESSAGE_NOT_DELIVERED": //превышена максимальная длина сообщения: 201 для кириллицы, 459 для латиницы
                      $QueryText = "update smssender set responsecode = '5' where id = $id";
                      break;
                case "50":
                case "UNDELIV": //Частично доставлено (Сообщение не может быть доставлено. Чаще всего эта ошибка возникает, когда номер абонента не верен, либо абонент отключен.)
                case "REQUEST_FORMAT": // неправильный тип запроса  
                case "delivery error": //Ошибка доставки SMS (абонент в течение времени доставки находился вне зоны действия сети или номер абонента заблокирован)
                    $QueryText = "update smssender set responsecode = '50' where id = $id";
                      break;
                case "96":
                case "ERROR": //Сбой сети при доставке SMS (Системная ошибка при отправке сообщения.)
                case "API_DISABLED": //для учетной записи пользователя запрещена работа с API
                case "AUTH_DATA": //ошибка авторизации: несуществующий пользователь, неправильная пара логин-пароль
                case "NOT_ENOUGH_MONEY": //недостаточно средств для отправки сообщения респондентам в запросе 
                    $QueryText = "update smssender set responsecode = '96' where id = $id";
                      break;
                case "99":
                case "ALFANAMELIMITED": //Ошибка в номере //Номер не обслуживается (Сообщение не может быть отправлено абоненту Life :), так как Альфаимя ограничено.)
                case "INVALID_FROM": //несуществующее имя отправителя для данной учетной записи
                case "USER_NOT_MODERATED": //запрещена отправка сообщений без проверки для учетной записи пользователя
                case "INCORRECT_FROM": //некорректное имя отправителя
                case "smsc reject": //Сообщение отвергнуто SMSC (номер заблокирован или не существует)
                    $QueryText = "update smssender set responsecode = '99' where id = $id";
                      break;
                case "USERSTOPED":
                case "UNKNOWN_ERROR":
                case "incorrect id": // Неверный идентификатор сообщения
                               default:// Сообщение остановлено пользователем через WEB интерфейс. 
                    $QueryText = "update smssender set responsecode = '-99' where id = $id";
                	break;
            }
        }
        $query2 = ibase_prepare($trans, $QueryText);
        ibase_execute($query2);
        ibase_commit_ret($trans);
        ibase_free_query($query2);
    }
    ibase_free_query($query);
    ibase_free_result($result);
}

/**
 * clearSMS()
 * Закрытие статусов для просроченых смс
 * 
 * @param SMSClient $client
 * @param resource $trans
 * @return void
 */
function clearSMS($trans)
{
    $QueryText = "update smssender set smssender.responsecode = -2, smssender.errormessage = 'Send time was elapsed'
        where (smssender.serversendtime < dateadd(-1 day to current_timestamp)
            or (select tasks.taskdate from tasks where tasks.id = smssender.taskid) < current_date)
            and (responsecode is null or responsecode = 0 or responsecode = 1)";
    $query = ibase_prepare($trans, $QueryText);
	ibase_execute($query);
    ibase_free_query($query);
}

/**
 * preparePatientsBirthDay()
 * 
 * @param resource $trans
 * @param string $template
 * @param boolean $isSMSGinMobile 
 * @return void
 */
function preparePatientsBirthDay($trans, $template, $isSMSGinMobile)
{
    $day = date('d');
    $month = date('m');
    $QueryText = "select 
                    p.shortname, 
                    p.fullname, 
                    patientsmobile.mobilenumber,
                    p.firstname, 
                    p.lastname, 
                    p.middlename, 
                    p.id
        from patients p   
            inner join patientsmobile
            on patientsmobile.patient_fk = p.id
        where
             patientsmobile.mobilenumber is not null
             and patientsmobile.mobilenumber != ''
             and patientsmobile.senderactive = 1
             and p.birthday is not null
            and extract(day from p.birthday)=$day
            and extract(month from p.birthday)=$month";
    $query = ibase_prepare($trans, $QueryText);
	$result = ibase_execute($query);
    while($row = ibase_fetch_row($result))
    {
        $shortName =$row[0];
        $fullName = $row[1];
        $phone = preg_replace('/\D/', '', $row[2]);
        $firstname = $row[3];
        $lastname = $row[4];
        $patronymic = $row[5];
        $patientid = $row[6];
//        if (strlen($phone) == 10)
//            $phone = '38'.$phone;
//        if (strlen($phone) == 11)
//            $phone = '3'.$phone;
//        if (strlen($phone) == 12)
//        {
            $serch = array('%date%', '%time%', '%short%', '%full%', '%firstname%', '%lastname%', '%patronymic%');
            $replace = array(date('d.m.y'), date('H:i'), $shortName, $fullName, $firstname, $lastname, $patronymic);
            $message = str_replace($serch, $replace, $template);
            
            $initialResponseCode = 1;
            if($isSMSGinMobile)
            {
                $initialResponseCode = 100;
            }
            
            $QueryTextIns = format_for_query("insert into smssender(patientid, phone, smsmessage, SCHEDULEDTIME, responsecode) values({0}, {1}, '{2}', '{3}', {4})",
                $patientid, $phone, $message, date('Y-m-d'), $initialResponseCode);
            //echo "\n$QueryText\n";
            $queryIns = ibase_prepare($trans, $QueryTextIns);
            ibase_execute($queryIns);
            ibase_free_query($queryIns);
        //}
    }
    ibase_free_query($query);
    
    $now = date('Y-m-d');
    $QueryText = "update or insert into applicationsettings(parametername, parametervalue) values('SMSDOBLastCall', '$now') matching(parametername)";
    $query = ibase_prepare($trans, $QueryText);
	ibase_execute($query);
    ibase_free_query($query);
    ibase_commit_ret($trans);
}

class SMSSenderSetting
{
    /**
    * @var string 
    */
    var $SMSLogin;
    /**
    * @var string 
    */
    var $SMSPassword;
    /**
    * @var bool 
    */
    var $SMSEnable;
    /**
    * @var bool 
    */
    var $SMSEnableOnTask;
    /**
    * @var bool 
    */
    var $SMSEnableOnDOB;
    /**
    * @var string 
    */
    var $SMSSendDays;
    /**
    * @var string 
    */
    var $SMSStartTime;
    /**
    * @var string 
    */
    var $SMSEndTime;
    /**
    * @var int 
    */
    var $SMSSendBeforeInDays;
    /**
    * @var int 
    */
    var $SMSSendBeforeInHours;
    /**
    * @var string 
    */
    var $SMSTaskTemplate;
    /**
    * @var string 
    */
    var $SMSDispanserTaskTemplate;
    /**
    * @var string 
    */
    var $SMSDOBTemplate;
    /**
    * @var string 
    */
    var $SMSSenderName;
    /**
    * @var int 
    */
    var $SMSQueueCapacity; 
    /**
    * @var bool 
    */
    var $SMSTranslit;
    /**
    * @var bool 
    */
    var $SMSModuleUseGinMobile;
}

class SMSSenderModule
{   
    /**
     * SMSSenderModule::registerType()
     * 
     * @param SMSSenderSetting $p1
     * @param SMSSenderModuleMessage $p2
     * @param SMSSenderModulePatient $p3
     * @param SMSSenderModuleFilter $p4
     * @return void
     */
    public function registerType($p1, $p2, $p3, $p4) {}
    
    /**
     * SMSSenderModule::sendSMS()
     * 
     * @param string $phone
     * @param string $message
     * @return int - id in SMSSender table in db
     */
    public function sendSMS($phone, $message)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $settings = getSettings($trans);
            
            $QueryText = format_for_query("insert into smssender(phone, smsmessage, SCHEDULEDTIME) values('{0}', '{1}', '{2}') returning id",
            $phone, $message, date('Y-m-d H:i:s'));
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $row = ibase_fetch_row($result);
            if ($row == null)
            {
                trigger_error("Error during executing: $QueryText", E_USER_ERROR);
            }
            $sms = new SMSMessage();
            $sms->id = (int) $row[0];
            $sms->message = $message;
            $sms->phone = $phone;
            ibase_free_query($query);
            ibase_free_result($result);
            
            $client = new SMSClient($settings['SMSLogin'], $settings['SMSPassword']); 
            sendSMS($client, $sms, $settings['SMSSenderName'], $trans);
            
            if (ibase_errmsg() != FALSE)
        	{
        	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
        	}      
        	ibase_commit($trans);
        	$connection->CloseDBConnection();   
            if ($client->hasErrors())
            {
                trigger_error($client->getErrors(), E_USER_ERROR);
            }       
            return $sms->id;
    }
    
    /**
     * sendSMSGate()
     * 
     * @param string $login
     * @param string $pass
     * @param string $gateIp
     * @param string $gatePort
     * @param SMSMessage $sms
     * @param resource $trans
     * @param bool $translit
     * @return void
     */
    function sendSMSGate($login, $pass, $gateIp, $gatePort, $gateAdditional, $sms, $trans, $translit = false)
    {
        $text = $translit ? $client->translit($sms->message) : $sms->message;
        $textToGate = str_replace(' ', '%20', $text);
        $sms->sms_id = uniqid();
        
        //2.Setting the request options, including specific URL.             
        $gateUrl = "http://".$gateIp;
        if($gatePort != null && $gatePort != '')
        {
            $gateUrl .= ":".$gatePort;
        }
        $gateUrl .= "/sendsms?username=".$login."&password=".$pass."&phonenumber=".$sms->phone."&message=".$textToGate;//."&port=gsm-1.2";//."&port=gsm-4.2&report=String&timeout=5";
        if($gateAdditional != null && $gateAdditional != '')
        {
            $gateUrl .= "&port=".$gateAdditional;
        }
        
        //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n$gateUrl");
        
        
        $curl = curl_init(); 
        curl_setopt($curl, CURLOPT_URL, $gateUrl);
        curl_setopt($curl, CURLOPT_USERAGENT, 'api');
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 10); 
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        @curl_exec($curl);   
        curl_close($curl); 
         
        
        $QueryText = "";
        
        $result = true;
        if($result)
        {
            $len = mb_strlen($text, 'utf-8');
            $smscount = ceil($len / ($translit ? ($len <= 160 ? 160 : 153) : ($len <= 70 ? 70 : 67)));
            $QueryText = "update smssender set smsid = '$sms->sms_id', serversendtime = 'now', responsecode = 3".($translit ? (", smsmessage = '".safequery($text)."'") : ''). ", smscount = $smscount where id = $sms->id";
        }
        else
        {
            $QueryText = "update smssender set serversendtime = current_timestamp, responsecode = -1, errormessage = 'not sended' where id = $sms->id";
        }
        //echo "\n".$QueryText;
        $query = ibase_prepare($trans, $QueryText);
        ibase_execute($query);
        ibase_free_query($query);
        ibase_commit_ret($trans);
    }
    
    /**
     * SMSSenderModule::getSMSSettings()
     * 
     * @return SMSSenderSetting
     */
    public function getSMSSettings()
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $settings = getSettings($trans);
            $options = new SMSSenderSetting;
            $options->SMSLogin = @$settings['SMSLogin'];
            $options->SMSPassword = @$settings['SMSPassword'];
            $options->SMSEnableOnTask = @$settings['SMSEnableOnTask'] == '1';
            $options->SMSEnableOnDOB = @$settings['SMSEnableOnDOB'] == '1';
            $options->SMSSendDays = @$settings['SMSSendDays'];
            $options->SMSStartTime = @$settings['SMSStartTime'];
            $options->SMSEndTime = @$settings['SMSEndTime'];
            $options->SMSSendBeforeInDays = @$settings['SMSSendBeforeInDays'];
            $options->SMSSendBeforeInHours = @$settings['SMSSendBeforeInHours'];
            $options->SMSTaskTemplate = @$settings['SMSTaskTemplate'];
            $options->SMSDOBTemplate = @$settings['SMSDOBTemplate'];
            $options->SMSSenderName = @$settings['SMSSenderName'];
            $options->SMSQueueCapacity = @$settings['SMSQueueCapacity']; 
            $options->SMSDispanserTaskTemplate = @$settings['SMSDispanserTaskTemplate'];
            $options->SMSEnable = @$settings['SMSEnable'] == '1';
            $options->SMSDOBLastCall = @$settings['SMSDOBLastCall'];
            $options->SMSTranslit = @$settings['SMSTranslit'] == '1';   
            $options->SMSModuleUseGinMobile = @$settings['SMSModuleUseGinMobile'] == '1';
            $options->SMSModuleMode = @$settings['SMSModuleMode']; 
            $options->SMSModuleGateIP = @$settings['SMSModuleGateIP']; 
            $options->SMSModuleGatePort = @$settings['SMSModuleGatePort']; 
            $options->SMSModuleGateAdditional = @$settings['SMSModuleGateAdditional']; 
            if (ibase_errmsg() != FALSE)
        	{
        	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
        	}      
        	ibase_commit($trans);
        	$connection->CloseDBConnection();
            return $options;
    }
    
    /**
     * SMSSenderModule::getSMSSettings()
     * 
     * @param SMSSenderSetting $settings
     * @return string
     */
    public function saveSMSSettings($settings)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            foreach (get_object_vars($settings) as $key => $value)
            {
                if ($key == "SMSEnable") continue;
                $QueryText = format_for_query("update or insert into applicationsettings(parametername, parametervalue) values('{0}', '{1}') matching(parametername)", 
                    $key, is_bool($value) ? ($value ? '1' : '0') : $value);
                $query = ibase_prepare($trans, $QueryText);
                ibase_execute($query);
                ibase_free_query($query);
            }
            ibase_commit($trans);
            if (ibase_errmsg() != FALSE)
        	{
        	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
        	}
            $connection->CloseDBConnection();
            return 'ok';//$this->checkBalance($settings->SMSLogin, $settings->SMSPassword);
    }
    
    /**
     * SMSSenderModule::checkBalance()
     * 
     * @param string $login
     * @param string $password
     * @return string
     */
    public function checkBalance($login, $password)
    {
        if ($login == null or $login == "")
        {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $settings = getSettings($trans);            
            if (ibase_errmsg() != FALSE)
        	{
        	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
        	}      
        	ibase_commit($trans);
        	$connection->CloseDBConnection();
            $login = @$settings['SMSLogin'];
            $password = @$settings['SMSPassword'];
         }
        $client = new SMSClient($login, $password);     
        $balance = $client->getBalance();
        if ($client->hasErrors())
        {
            $errors = $client->getErrors();
            return $errors[0];
        }       
        return $balance;
    }
    
    /**
     * SMSSenderModule::getSMSStatusByTaskId()
     * Get SMS status from SMSSender table.
     * 
     * @param int $taskid
     * @return int
     */
    public function getSMSStatusByTaskId($taskid)
    {
            $connection = new Connection();
            $connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $QueryText = "select responsecode from smssender where taskid = $taskid";
            $query = ibase_prepare($trans, $QueryText);
            $result = ibase_execute($query);
            $status = 0;
            if ($row = ibase_fetch_row($result))
            {
                $status = $row[0] == null ? 0 : $row[0];
            }     
            if (ibase_errmsg() != FALSE)
        	{
        	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
        	}      
        	ibase_commit($trans);
        	$connection->CloseDBConnection();
                
            return $status;
    }
    
    /**
     * SMSSenderModule::getSMSMessages()
     * 
     * @param string $fromDate
     * @param string $toDate
     * @return SMSSenderModuleMessage[]
     */
    public function getSMSMessages($fromDate, $toDate)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select 
                smssender.id,
                smssender.smsid,
                smssender.phone,
                smssender.responsecode,
                smssender.smsmessage,
                smssender.createdate,
                smssender.scheduledtime,
                smssender.serversendtime,
                smssender.smssendtime,
                smssender.deliverytime,
                smssender.errormessage
            from smssender
            where 
                cast(smssender.createdate as date) between '$fromDate' and '$toDate'
            order by id desc";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $messages = array();
        while ($row = ibase_fetch_row($result))
        {
            $msg = new SMSSenderModuleMessage;
            $msg->id = $row[0];
            $msg->sms_id = $row[1];
            $msg->phone = $row[2];
            $msg->response = $row[3];
            $msg->message = $row[4];
            $msg->createDate = $row[5];
            $msg->scheduledTime = $row[6];
            $msg->serverSendTime = $row[7];
            $msg->smsSendTime = $row[8];
            $msg->deliveryTime = $row[9];
            $msg->errormsg = $row[10];
            
            $messages[] = $msg;
        }    
        ibase_free_query($query);
        ibase_free_result($result);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
        return $messages;
    }
    
    /**
     * SMSSenderModule::sendMassMessage()
     * 
     * @param string $template
     * @param SMSSenderModuleFilter[] $filters
     * @return int - count off added messages
     */
    public function sendMassMessage($template, $filters)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = " select
                        patients.id,
                        patients.shortname,
                        patients.fullname,
                        patientsmobile.mobilenumber,
                        patients.firstname,
                        patients.lastname,
                        patients.middlename
                    from patients
                        inner join patientsmobile
                        on patientsmobile.patient_fk = patients.id
                    where patientsmobile.mobilenumber is not null
                        and patientsmobile.mobilenumber != ''
                        and patientsmobile.senderactive = 1
                        and patients.primaryflag = 1";
        $where = '';
        foreach ($filters as $f)
        {
            $QueryText .= " AND " . $this->toWhereString($f);
        }
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $count = 0;
        while ($row = ibase_fetch_row($result))
        {
            $patId = $row[0];
            $shortName =$row[1];
            $fullName = $row[2];
            $phone = preg_replace('/\D/', '', $row[3]);
            $firstname = $row[4];
            $lastname = $row[5];
            $patronymic = $row[6];
//            if (strlen($phone) == 10)
//                $phone = '38'.$phone;
//            if (strlen($phone) == 11)
//                $phone = '3'.$phone;
//            if (strlen($phone) == 12)
//            {
                $serch = array('%date%', '%time%', '%short%', '%full%', '%firstname%', '%lastname%', '%patronymic%');
                $replace = array(date('d.m.y'), date('H:i'), $shortName, $fullName, $firstname, $lastname, $patronymic);
                $message = str_replace($serch, $replace, $template);
                
                $QueryTextIns = format_for_query("insert into smssender(phone, smsmessage, SCHEDULEDTIME, patientid) values({0}, '{1}', '{2}', {3})",
                    $phone, $message, date('Y-m-d'), $patId);
                //echo "\n$QueryText\n";
                $queryIns = ibase_prepare($trans, $QueryTextIns);
                ibase_execute($queryIns);
                ibase_free_query($queryIns);
                
                $count++;
            //}
        
            if ($count == 1000)
                ibase_commit_ret($trans);
        }     
        if (ibase_errmsg() != FALSE)
    	{
    	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
    	}      
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
            
        return $count;
    }
    
    /**
     * SMSSenderModule::calcPatients()
     * 
     * @param SMSSenderModuleFilter[] $filters
     * @return object[] - first - patient count, second - translit option
     */
    public function prepareMassMailing($filters)
    {
        $where = '';
        foreach ($filters as $f)
        {
            $where .= " AND " . $this->toWhereString($f);
        }
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select count(patientsmobile.mobilenumber),
                       coalesce((select first 1 a.parametervalue from applicationsettings a where a.parametername = 'SMSTranslit'),0)
                                    from patients 
                                    inner join patientsmobile
                                    on patientsmobile.patient_fk = patients.id
                                where patientsmobile.mobilenumber is not null
                                    and patientsmobile.mobilenumber != ''
                                    and patientsmobile.senderactive = 1
                                    and patients.primaryflag = 1" . $where;
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $row = ibase_fetch_row($result);
        ibase_free_query($query);
        ibase_free_result($result);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
        return $row;
    }
    
    /**
     * SMSSenderModule::getSMSServiceStatus()
     * 
     * @return bool
     */
    public function getSMSServiceStatus()
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select first 1 a.parametervalue from applicationsettings a where a.parametername = 'SMSEnable'";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $status = is_null($row = ibase_fetch_row($result)) ? false : ($row[0] == '1');
        ibase_free_query($query);
        ibase_free_result($result);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
        return $status;
    }
    
    /**
     * SMSSenderModule::setSMSServiceStatus()
     * 
     * @param bool $status
     * @return void
     */
    public function setSMSServiceStatus($status)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = format_for_query("update or insert into applicationsettings(parametername, parametervalue) values('{0}', '{1}') matching(parametername)", 
                    'SMSEnable', ($status ? '1' : '0'));
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        ibase_free_query($query);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
    }
    
    /**
     * SMSSenderModule::deleteSMS()
     * 
     * @param Number[] $IDs
     * @return void
     */
    public function deleteSMS($IDs)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $delset = null;
        foreach($IDs as $id)
        {
            $delset = is_null($delset) ? "" : $delset.", ";
            $delset .= $id;
        }
        $QueryText = "delete from smssender where id in ($delset)";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        ibase_free_query($query);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
    }
    
    /**
     * @return boolean
     */
    public function clearTrunSMS()
    {   	 
     	$connection = new Connection();    	
		$connection->EstablishDBConnection();		
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from smssender where serversendtime is null";	
		$query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);	
		ibase_free_query($query);		
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();
		return $success;
        
       // $QueryText = "delete from smssender where serversendtime is null";
        
    }
    
    /**
     * SMSSenderModule::resendSMS()
     * 
     * @param Number[] $IDs
     * @return void
     */
    public function resendSMS($IDs)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $set = null;
        foreach($IDs as $id)
        {
            $set = is_null($set) ? "" : $set.", ";
            $set .= $id;
        }
        $QueryText = "update smssender set
                smssender.smsid = null,
                smssender.responsecode = null,
                smssender.scheduledtime = current_timestamp,
                smssender.serversendtime = null,
                smssender.smssendtime = null,
                smssender.deliverytime = null,
                smssender.errormessage = null
            where id in ($set)";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        ibase_free_query($query);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
    }
    
    /**
     * SMSSenderModule::changePhoneNumber()
     * 
     * @param int $id
     * @param string $phone
     * @return bool
     */
    public function changePhoneNumber($id, $phone)
    {
//        if (strlen($phone) == 10)
//            $phone = '38'.$phone;
//        if (strlen($phone) == 11)
//            $phone = '3'.$phone;
//        $success = (strlen($phone) == 12); 
        $success = true; 
        if (!$success) return false;
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "update smssender set phone = $phone where id = $id returning new.patientid, old.phone";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        if ($row = ibase_fetch_row($result))
        {            
            ibase_free_query($query);
            
            $QueryText = "update patientsmobile set
                                patientsmobile.mobilenumber = '$phone'
                            where patientsmobile.mobilenumber = '$row[1]' and patientsmobile.patient_fk = $row[0]";
            
            //$QueryText = "update patients set patients.mobilenumber = '$phone' where id = " . $row[0];
            $query = ibase_prepare($trans, $QueryText);
            $success = ibase_execute($query) && $success;
            ibase_free_query($query);
        }
        else
        {
            $success = false;
        }
        if ($success)
            ibase_commit($trans); 
        else
            ibase_rollback($trans);    
    	$connection->CloseDBConnection();
        return $success;
    }
    
    
    /**
     * SMSSenderModule::getPatients()
     * @param string $search
     * @return SMSSenderModulePatient[]
     */
    public function getPatients($search)
    {
        $connection = new Connection();
        $connection->EstablishDBConnection();
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        $QueryText = "select first 20 patients.id,
                            patients.fullname,
                            patientsmobile.mobilenumber
                    from patients
                        inner join patientsmobile
                        on patientsmobile.patient_fk = patients.id
                    where
                        patientsmobile.mobilenumber is not null 
                        and patientsmobile.mobilenumber != ''
                        and patientsmobile.senderactive = 1
                        and patients.primaryflag = 1
                        and lower(patients.fullname) like lower('%$search%')";
        $query = ibase_prepare($trans, $QueryText);
        $result = ibase_execute($query);
        $patients = array();
        while ($row = ibase_fetch_row($result))
        {
            $p = new SMSSenderModulePatient();
            $p->id = $row[0];
            $p->name = $row[1];
            $p->phone = $row[2];
            $patients[] = $p;
        }
        ibase_free_query($query);
        ibase_free_result($result);
    	ibase_commit($trans);
    	$connection->CloseDBConnection();
        return $patients;
    }
    
    /**
     * SMSSenderModule::toWhereString()
     * 
     * @param SMSSenderModuleFilter $filter
     * @return string
     */
    private function toWhereString($filter)
    {
        $conv = array(
            SMSSenderModuleFilter::BETWEEN       => 'BETWEEN',
            SMSSenderModuleFilter::EQUAL         => '=',
            SMSSenderModuleFilter::LESS          => '<',
            SMSSenderModuleFilter::LESS_EQUAL    => '<=',
            SMSSenderModuleFilter::LIKE          => 'LIKE',
            SMSSenderModuleFilter::MORE          => '>',
			SMSSenderModuleFilter::MORE_EQUAL    => '>=',
            SMSSenderModuleFilter::IN            => 'IN');
        $str = "($filter->field {$conv[$filter->type]} $filter->value" . ($filter->type == SMSSenderModuleFilter::BETWEEN ? " AND $filter->secontvalue" : '') . ')';
        return $str;
    }
    
    public  function translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',    'є' => 'ye', 
			'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',   'і' => 'i', 
			'и' => 'i',   'й' => 'j',   'к' => 'k',   'ї' => 'yi', 
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'kh',   'ц' => 'ts',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '"',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',   'Є' => 'Ye',
			'Ё' => 'Yo',   'Ж' => 'Zh',  'З' => 'Z',   'І' => 'I',
			'И' => 'I',   'Й' => 'J',   'К' => 'K',   'Ї' => 'Yi',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'Ts',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '"',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$result = strtr($string, $converter);
		
		//upper case if needed
		//if(mb_strtoupper($string) == $string)
		//	$result = mb_strtoupper($result);
			
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
	}
}
?>
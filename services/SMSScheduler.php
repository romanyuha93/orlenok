<?php
define('tfClientId','online');
require_once "Connection.php";
require_once "Utils.php";
require_once "smsclient.class.php";
require_once "SMSSenderModule.php";

//error_reporting(E_ALL);

//ini_set("display_errors", "On");

//скрипт выполняемый каждые 5 минут 

//try
{
    $connection = new Connection();
	$connection->EstablishDBConnection();
	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    
    //получение настроек
    $settings = getSettings($trans);
    if (array_key_exists('SMSQueueCapacity', $settings) && $settings['SMSQueueCapacity'] != null)
        $queueCapacity = $settings['SMSQueueCapacity'];
        
    
    $isSMSEnable = (array_key_exists("SMSEnable", $settings) && $settings["SMSEnable"] == "1");
    $isSMSTaskEnable = (array_key_exists("SMSEnableOnTask", $settings) && $settings["SMSEnableOnTask"] == "1");
    $isSMSDOBEnable = (array_key_exists('SMSEnableOnDOB', $settings) && $settings['SMSEnableOnDOB'] == '1'
        && (!array_key_exists('SMSDOBLastCall', $settings) || $settings['SMSDOBLastCall'] != date('Y-m-d')));
    $isSMSGinMobile = (array_key_exists('SMSModuleUseGinMobile', $settings) && $settings['SMSModuleUseGinMobile'] == '1');
    $smsMode = 0;
    if (array_key_exists('SMSModuleMode', $settings) && $settings['SMSModuleMode'] != null)
    {
        $smsMode = $settings['SMSModuleMode'];
    }
    
    //file_put_contents("C:/tmp.txt", file_get_contents("C:/tmp.txt")."\n$smsMode = ".$smsMode);
    

    
    $tasks = null;
    
    if ($isSMSEnable && (!$isSMSGinMobile || $smsMode == 0 || $smsMode == 2))
    {
        //обработка просроченых смс
        clearSMS($trans);
    
        if ($isSMSDOBEnable)
        {
            preparePatientsBirthDay($trans, $settings['SMSDOBTemplate'], false);
        }
        
        $now = time();
        $dayOfWeek = date("N") - 1;
        if($smsMode == 0)
        {
            $client = new SMSClient($settings['SMSLogin'], $settings['SMSPassword']);                                    //connect to sms service                    -- remove +
            $balance = $client->getBalance();                                                                            //get balance from sms service              -- remove +      
            if ($balance === null || $client->hasErrors())
            {
                error_log('SMS_MODULE_ERROR: Connection error. '.$client->getErrorString(), 0);
                return;
            }
            else if($balance == 0)
            {
                error_log('SMS_MODULE_ERROR: Balance 0', 0);
                return;
            }     
            checkSMS($client, $trans);                                                                                  //check sms status from sms service         -- remove +                                                  
        }
        
        
        
        
        if(isset($settings["SMSANDADMINDISPANCERDAYS"])==false)
        {
            $settings["SMSANDADMINDISPANCERDAYS"]=7;
        }
        if (isset($settings["SMSSendDays"]) && 
                strlen($settings["SMSSendDays"]) == 7 && 
                $settings["SMSSendDays"][$dayOfWeek] == "1" && 
                strtotime($settings["SMSStartTime"]) <= strtotime(date("H:i", $now)) && 
                strtotime($settings["SMSEndTime"]) >= strtotime(date("H:i", $now)))
        {
            $startDate = date("Y-m-d", $now);
            $endDate = date("Y-m-d", $now + 86400 * $settings['SMSSendBeforeInDays']);
            $startTime = date("H:i:s", $now + 3600 * $settings['SMSSendBeforeInHours']);
            //обработка таблицы tasks, выборка необходимых тасков
            $tasks = findNewTasks($trans, $startDate, $endDate, $startTime);
            //вставка полученых тасков
            createSMSFromTasks($trans, $tasks, $settings['SMSTaskTemplate'], false);
                
            $endDateDispanser = date("Y-m-d", $now + 86400 * $settings['SMSANDADMINDISPANCERDAYS']);
                
            $tasksDispanser = findNewDipanserTasks($trans, $endDateDispanser);
                
            createSMSFromDispanserTasks($trans, $tasksDispanser, $settings['SMSDispanserTaskTemplate']);
                
            
            //обработка таблицы SMSSENDER
            
            //выборка для отправки сообщений
            $msgs = getActiveSMS($trans, false);
            
            //var_dump($msgs);
            foreach($msgs as $msg)
            {
                
                if($smsMode == 0)
                {
                    sendSMS($client, $msg, $settings['SMSSenderName'], $trans, @$settings['SMSTranslit'] == '1');       //send messages via sms service             -- send via url and set send result
                }
                else if($smsMode == 2)
                {
                    //ini_set('max_execution_time', 1000);
                    sendSMSGate($settings['SMSLogin'], $settings['SMSPassword'], $settings['SMSModuleGateIP'], $settings['SMSModuleGatePort'], $settings['SMSModuleGateAdditional'], $msg, $trans, @$settings['SMSTranslit'] == '1'); 
                }
            }
        }
    }
    
    if (ibase_errmsg() != FALSE)
	{
	   //echo "\nIBase Error: ".ibase_errmsg();
	   trigger_error(ibase_errmsg(), E_USER_ERROR);				
	}      
	ibase_commit($trans);
	$connection->CloseDBConnection();
}
/*catch (Exception $e)
{
    var_dump($e);
}*/
//sleep(20);
?>
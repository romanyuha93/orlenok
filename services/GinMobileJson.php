<?php
define('tfClientId','online');
require_once "Connection.php";
require_once "ServiceModule.php";
require_once "Utils.php";
require_once "smsclient.class.php";
require_once "SMSSenderModule.php";

	//http://localhost/toothfairy/services/GinMobileJson.php?func=check
	//http://192.168.0.102/toothfairy/services/GinMobileJson.php?func=check
    if($_GET["func"] == "check" && $_GET["lic"])
    {
  		$resultObject = new stdClass;
        $resultObject->error = "";
        
        if (!ServiceModule::GLIC || ServiceModule::GLIC == null)
        {
            error_log("In SM no license!");
            $resultObject->error = "no license!";
            echo json_encode($resultObject, JSON_UNESCAPED_UNICODE);
            return;
        }
        $_lic = $_GET["lic"];
        if($_lic == ServiceModule::GLIC)
          
        {
    		 $connection = new Connection();
        	 $connection->EstablishDBConnection();
        	 $trans = ibase_trans(IBASE_DEFAULT, $connection->connection); 
             
    		 //get settings 
        	 $QueryText =
        			"select
                        PARAMETERNAME,
                        PARAMETERVALUE from
                        APPLICATIONSETTINGS
                     where
                        parametername = 'SMSSendDays'
                        or parametername = 'SMSStartTime'
                        or parametername = 'SMSEndTime'";
                    
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
                
            $settings = array();
			$settings["GetInterval"] = 600;
			$settings["SMSSendInterval"] = 30;
            while($row = ibase_fetch_row($result))
            {
                $settings[$row[0]] = $row[1];
            }
    		//get settings end
    		
            $resultObject->lic = $_lic;
            $resultObject->settings = $settings;
        }	
        else
        {
            $resultObject->error = "notvalid device license!";
        }
  		echo json_encode($resultObject, JSON_UNESCAPED_UNICODE);
    }
    
    if($_GET["func"] == "getSMS" && $_GET["lic"])
    {
        
  		$resultObject = new stdClass;
        $resultObject->error = "";
        
        if (!ServiceModule::GLIC || ServiceModule::GLIC == null)
        {
            error_log("In SM no license!");
            $resultObject->error = "no license!";
            echo json_encode($resultObject, JSON_UNESCAPED_UNICODE);
            return;
        }
            
        $_lic = $_GET["lic"];
        if($_lic == ServiceModule::GLIC)
        {
            $connection = new Connection();
        	$connection->EstablishDBConnection();
        	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $settings = getSettings($trans);
            if (array_key_exists('SMSQueueCapacity', $settings) && $settings['SMSQueueCapacity'] != null)
                $queueCapacity = $settings['SMSQueueCapacity'];
                
            
            $isSMSEnable = (array_key_exists("SMSEnable", $settings) && $settings["SMSEnable"] == "1");
            $isSMSTaskEnable = (array_key_exists("SMSEnableOnTask", $settings) && $settings["SMSEnableOnTask"] == "1");
            $isSMSDOBEnable = (array_key_exists('SMSEnableOnDOB', $settings) && $settings['SMSEnableOnDOB'] == '1'
                    && (!array_key_exists('SMSDOBLastCall', $settings) || $settings['SMSDOBLastCall'] != date('Y-m-d')));
            $isSMSGinMobile = (array_key_exists('SMSModuleUseGinMobile', $settings) && $settings['SMSModuleUseGinMobile'] == '1');
            
            $smsMode = 0;
            if (array_key_exists('SMSModuleMode', $settings) && $settings['SMSModuleMode'] != null)
            {
                $smsMode = $settings['SMSModuleMode'];
            }
                    
            
            $tasks = null;
            $smss = array();
            
            if ($isSMSEnable && ($isSMSGinMobile || $smsMode == 1))
            {
                
                clearSMS($trans);
                
                if ($isSMSDOBEnable)
                {
                    preparePatientsBirthDay($trans, $settings['SMSDOBTemplate'], true);
                }
                                                
                $now = time();
                $dayOfWeek = date("N") - 1;
                
                
                /*TO-DO balance check
                $client = new SMSClient($settings['SMSLogin'], $settings['SMSPassword']);
                
                $balance = $client->getBalance();
                
                if ($balance === null || $client->hasErrors())
                {
                    error_log('SMS_MODULE_ERROR: Connection error. '.$client->getErrorString(), 0);
                    return;
                }
                else if($balance == 0)
                {
                    error_log('SMS_MODULE_ERROR: Balance 0', 0);
                    return;
                }
                
                //checkSMS($client, $trans);
                */
                if(isset($settings["SMSANDADMINDISPANCERDAYS"])==false)
                {
                    $settings["SMSANDADMINDISPANCERDAYS"]=7;
                }
                if (isset($settings["SMSSendDays"]) && 
                        strlen($settings["SMSSendDays"]) == 7 && 
                        $settings["SMSSendDays"][$dayOfWeek] == "1" && 
                        strtotime($settings["SMSStartTime"]) <= strtotime(date("H:i", $now)) && 
                        strtotime($settings["SMSEndTime"]) >= strtotime(date("H:i", $now)))
                {
                    if ($isSMSTaskEnable)
                    {
                        $startDate = date("Y-m-d", $now);
                        $endDate = date("Y-m-d", $now + 86400 * $settings['SMSSendBeforeInDays']);
                        $startTime = date("H:i:s", $now + 3600 * $settings['SMSSendBeforeInHours']);
                        //выбрать Task у которых нет СМС в очереди 
                        $tasks = findNewTasks($trans, $startDate, $endDate, $startTime);
                        //заполняем таблицу SMSSENDER, т.е. создаем очеред еа отправку
                        createSMSFromTasks($trans, $tasks, $settings['SMSTaskTemplate'], true);
                        
                        
                        // TO_DO создает много лишних СМС по диспансеризации sms_id
                        //$endDateDispanser = date("Y-m-d", $now + 86400 * $settings['SMSANDADMINDISPANCERDAYS']);
                        //$tasksDispanser = findNewDipanserTasks($trans, $endDateDispanser);
                        //createSMSFromDispanserTasks($trans, $tasksDispanser, $settings['SMSDispanserTaskTemplate']);
                        
                    }
                    
                    //получаем смс с очереди (с SMSSENDER), подставляем sms_id и ставим статус "отправляется"
                    $smss = getActiveSMS($trans, true);
                    //echo json_encode($smss);
                    
                    foreach($smss as $sms)
                    {
                        //sendSMS($client, $msg, $settings['SMSSenderName'], $trans, @$settings['SMSTranslit'] == '1');
                        $translit = ($settings['SMSTranslit'] == '1');
                        
                        $sms->message = $translit ? translit($sms->message) : $sms->message;
                        $sms->phone = "+".$sms->phone;
                        
                        $QueryText = "";
                        if ($sms->sms_id == null)
                        {
                            $sms->sms_id = uniqid();//$client->sendSMS(array($sms->phone), $text, $senderName != null ? $senderName : "" );
                            //$sms->response = 100;
                            $sms->livetime = $now + 3600 * $settings['SMSSendBeforeInHours'];
                            $len = mb_strlen($sms->message, 'utf-8');
                            $smscount = ceil($len / ($translit ? ($len <= 160 ? 160 : 153) : ($len <= 70 ? 70 : 67)));
                            //$QueryText = "update smssender set smsid = '$sms->sms_id', serversendtime = current_timestamp, responsecode = $sms->response".($translit ? (", smsmessage = '".safequery($sms->message)."'") : ''). ", smscount = $smscount where id = $sms->id";
                            $QueryText = "update smssender set smsid = '$sms->sms_id', serversendtime = current_timestamp ".($translit ? (", smsmessage = '".safequery($sms->message)."'") : ''). ", smscount = $smscount where id = $sms->id";
                        }
                        
                        $query = ibase_prepare($trans, $QueryText);
                        ibase_execute($query);
                        ibase_free_query($query);
                    }
                }
            }
               
            //get settings 
          	$QueryText =
                			"select
                                PARAMETERNAME,
                                PARAMETERVALUE from
                                APPLICATIONSETTINGS
                             where
                                parametername = 'SMSSendDays'
                                or parametername = 'SMSStartTime'
                                or parametername = 'SMSEndTime'";
                            
            $query = ibase_prepare($trans, $QueryText);
            $result=ibase_execute($query);
                        
            $settings = array();
			$settings["GetInterval"] = 600;
			$settings["SMSSendInterval"] = 30;
            while($row = ibase_fetch_row($result))
            {
                $settings[$row[0]] = $row[1];
            }
            //get settings end
            $resultObject->sms = $smss;
            $resultObject->settings = $settings;
    		
        	ibase_commit($trans);
        	$connection->CloseDBConnection();
         }
         else
         {
            $resultObject->error = "notvalid device license!";
         }
         echo json_encode($resultObject, JSON_UNESCAPED_UNICODE);
    }
    
    //http://localhost/toothfairy/services/GinMobileJson.php?func=setSMSStatus&smsid=56cc5c47a4f97&status=50
    /* 
        1 - в очереди
        2 - отправлено
        3 - доставлено
        50- недоставлено
    */
    if($_GET["func"] == "setSMSStatus" && $_GET["lic"] && $_GET["smsid"] && $_GET["status"])
    {
  		$resultObject = new stdClass;
        $resultObject->error = "";
        $resultObject->status = "";
        $resultObject->smsid = "";
        
        if (!ServiceModule::GLIC || ServiceModule::GLIC == null)
        {
            error_log("In SM no license!");
            $resultObject->error = "no SM license!";
            echo json_encode($resultObject, JSON_UNESCAPED_UNICODE);
            return;
        }
            
        $_lic = $_GET["lic"];
        if($_lic == ServiceModule::GLIC)
        {
            $_smsid = $_GET["smsid"];
            $_status = $_GET["status"];
            
            $connection = new Connection();
        	$connection->EstablishDBConnection();
        	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            if($_status == 300)
            {
                
                $QueryText = "update smssender set responsecode = '$_status', deliverytime = '".date('Y-m-d H:i:s')."' where smsid = '$_smsid'";
            }
            else
            {
                $QueryText = "update smssender set responsecode = '$_status' where smsid = '$_smsid'";
            }
            $query = ibase_prepare($trans, $QueryText);
            ibase_execute($query);
            ibase_free_query($query);
            
        	if(ibase_commit($trans))
            {
                $resultObject->status = $_status;
                $resultObject->smsid = $_smsid;
            }
        	$connection->CloseDBConnection();
        }
        else
        {
            $resultObject->error = "notvalid device license!";
        }
        echo json_encode($resultObject, JSON_UNESCAPED_UNICODE);
     }
    
    
    
    /*if($_GET["func"] == "devFunc_clearDB")
    {
            
        $connection = new Connection();
    	$connection->EstablishDBConnection();
    	$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
    		//users
            $queryText = "delete from smssender";
    		$sqlText = ibase_prepare($trans, $queryText);
    		ibase_execute($sqlText);
    		ibase_free_query($sqlText);
            
   		$success = ibase_commit($trans);
   		$connection->CloseDBConnection();
   		return "ok";
    }
    */
    
    	
    
?>

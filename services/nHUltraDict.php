<?php

require_once "Connection.php";
require_once "Utils.php";
require_once "Settings.php";

class NHUltraDict_Item
{
    /**
    * @var string
    */
    var $id;
    /**
    * @var string
    */
    var $item_label;
    /**
    * @var string
    */
    var $item_template;
    /**
    * @var int
    */
    var $row_index;
}

 
class nHUltraDict
{		
    public static $isDebugLog = false;
   
   
	/**
    * @param object $trans
    * @return NHUltraDict_Item[]
	*/
    public function GetTemplates($trans = null)
    {
        if($trans==null)
        {
            $connection = new Connection();
            $connection->EstablishDBConnection("main",false);
	        //$connection->EstablishDBConnection();
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $local=true;
        }
        else
        {
            $local=false;
        }
        
        $sql_getdic = "select ID, ITEM_LABEL, ITEM_TEMPLATE, ROW_INDEX
                        from NH_ULTRASONOGRAPHY_DICT order by ROW_INDEX, ID";
        $query_getdic = ibase_prepare($trans, $sql_getdic);
        $result_getdic = ibase_execute($query_getdic);	
        
        $resultObject = array();
        
		while ($row = ibase_fetch_row($result_getdic))
        {
            $dicItem = new NHUltraDict_Item;
            $dicItem->id = $row[0];
            $dicItem->item_label = $row[1];
            $dicItem->item_template = $row[2];
            $dicItem->row_index = $row[3];
            
            $resultObject[] = $dicItem;	
        }
        ibase_free_query($query_getdic);
        ibase_free_result($result_getdic);
        
        if($local)
        {
            ibase_commit($trans);
            $connection->CloseDBConnection();
        }
        
        
        return $resultObject;
    }
    
    
   	/**
	* @param NHUltraDict_Item $item
    * @return NHUltraDict_Item[]
	*/
    public function SaveItemByID($item)
    {
        $connection = new Connection();
        //$connection->EstablishDBConnection("main",false);
        $connection->EstablishDBConnection();
        
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        if($item->id == null || $item->id == '')
        {
            $item->id = 'null';
        }
        
        $save_sql = "update or insert into NH_ULTRASONOGRAPHY_DICT (
                                            ID, 
                                            ITEM_LABEL, 
                                            ITEM_TEMPLATE,
                                            ROW_INDEX
                                    )
                             values (
                                         ".$item->id.", 
                                         '".safequery($item->item_label)."', 
                                         '".safequery($item->item_template)."',
                                         ".$item->row_index."
                             )
                             matching (ID)";
            
        $save_query = ibase_prepare($trans, $save_sql);
		$result_query = ibase_execute($save_query);
        
        ibase_free_query($save_query);
        
        $resultObject = $this->GetTemplates($trans);
        
		ibase_commit($trans);
		$connection->CloseDBConnection();	
        
        return $resultObject;
    }
   
   	/**
	* @param string $id
    * @return boolean
	*/
    public function DeleteItemByID($id)
    {
        $connection = new Connection();
        //$connection->EstablishDBConnection("main",false);
        $connection->EstablishDBConnection();
        
        $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
        
        $del_sql = "delete from NH_ULTRASONOGRAPHY_DICT
                            where (ID = $id)";
            
        $del_query = ibase_prepare($trans, $del_sql);
		ibase_execute($del_query);
        
		$result = ibase_commit($trans);
		$connection->CloseDBConnection();	
        
        return $result;
    }

   	/**
	* @param int $row_index
    * @return boolean
	*/
    public function CheckRowIndex($row_index)
    {
        if($row_index > 2 && $row_index < 23)
        {
            $connection = new Connection();
            //$connection->EstablishDBConnection("main",false);
            $connection->EstablishDBConnection();
            
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            
            $sql_check = "select ID from NH_ULTRASONOGRAPHY_DICT
                                where (ROW_INDEX = $row_index)";
                
            $query_check = ibase_prepare($trans, $sql_check);
            $result_check = ibase_execute($query_check);	
            $result  = true;
    		if ($row = ibase_fetch_row($result_check))
            {
                $result = false;	
            }
            ibase_free_query($query_check);
            ibase_free_result($result_check);
                
    		ibase_commit($trans);
    		$connection->CloseDBConnection(); 
            return $result;
        }
        else
        {
            return false;
        }	
        
    }
    
}

?>
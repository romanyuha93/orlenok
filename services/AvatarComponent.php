<?php

require_once "Connection.php";
//WebCameraExample
class AvatarComponentImageObject
{
    /**
    * @var int
    */
    var $ID;
    
    /**
    * @var string
    */
    var $TYPE;

    /**
    * @var object
    */
    var $IMAGE;         
}


class AvatarComponent
{	
    /**
     * @param AvatarComponentImageObject $p1
     * 
     * @return void
     */
    public function registerTypes($p1){}
    
    /**
    * @param int $pictureID
    * @param string $type
    * @return AvatarComponentImageObject
    */
	public function getImage($pictureID, $type)
	{
	    $connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "select ID, IMAGE from AVATARIMAGES where ID = ".$pictureID." and TYPE = '".$type."'";
		$query = ibase_prepare($trans, $QueryText);
		$result = ibase_execute($query);
        $returnImgObj = new AvatarComponentImageObject;
		$row = ibase_fetch_row($result);	
        $imageBlob = null;
        $blob_info = ibase_blob_info($row[1]);
        if ($blob_info[0]!=0)
        {
            $blob_hndl = ibase_blob_open($row[1]);
            $imageBlob = ibase_blob_get($blob_hndl, $blob_info[0]);
            ibase_blob_close($blob_hndl);
        }
        $returnImgObj->IMAGE = base64_encode($imageBlob);
        $returnImgObj->ID = $row[0];
        ibase_commit($trans);
		ibase_free_query($query);
		ibase_free_result($result);
		$connection->CloseDBConnection();
		return $returnImgObj; 
	}
    
    /**
 	* @param AvatarComponentImageObject $imageObject
    * @return boolean
 	*/
    public function addImage($imageObject) 
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = 		
		"UPDATE OR INSERT into AVATARIMAGES (ID, TYPE, IMAGE) values (".$imageObject->ID.", '".$imageObject->TYPE."', ?) MATCHING (ID, TYPE)";
		$img = base64_decode($imageObject->IMAGE);
		if ("$img"=="")
		{
			$blobid = null;
		}
		else
		{
		    $blh = ibase_blob_create($connection->connection);		    
		    ibase_blob_add($blh, $img);
		    $blobid = ibase_blob_close($blh);		
		}		
		ibase_query($connection->connection, $QueryText, $blobid);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();	
		return $success;		
    }
    
        
    /**
    * @param int $pictureID
    * @param string $type
    * @return boolean
 	*/
    public function deleteImage($pictureID, $type)
    {
     	$connection = new Connection();
		$connection->EstablishDBConnection();
		$trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
		$QueryText = "delete from avatarimages where id='$pictureID' and type='$type'";	
        $query = ibase_prepare($trans, $QueryText);
		ibase_execute($query);
		ibase_free_query($query);
		$success = ibase_commit($trans);
		$connection->CloseDBConnection();	
		return $success;		
    }
}

?>
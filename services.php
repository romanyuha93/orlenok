<?php

require_once "services/ScheduleReportsModule.php";
require_once "SettingsOnlineForm.php";
define('tfClientId','online');
//ini_set("max_execution_time", "90000");
//ini_set('memory_limit', '-1');
//ini_set('realpath_cache_ttl', '90000');

//
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// Errors
if (!empty($_POST['doctorId'])){
    $arrayDate = GetArrayDisableDate($_POST['doctorId'],$SettingsToothFairyOnlineForm['periodDays']);
    print_r($arrayDate);
}elseif (!empty($_POST['recordDate']) && !empty($_POST['docId'])){
    $arrayTime = GetFreeTime($_POST['recordDate'],$_POST['docId'],$SettingsToothFairyOnlineForm['periodMinutes']);
    print_r($arrayTime);
}elseif (isset($_POST['Run']) && $_POST['Run'] == true){
    file_put_contents('logcrm.txt',json_encode($_POST).PHP_EOL,FILE_APPEND);
    $responseInsertPatient = InsertPatient( $_POST, $SettingsToothFairyOnlineForm['periodMinutes'],$SettingsToothFairyOnlineForm['secretReCaptcha']);
    print_r($responseInsertPatient);
}elseif (isset($_POST['getCheckStatus']) && !empty($_POST['getCheckStatus'])){  // TODO
    print_r(getCheckStatus($SettingsToothFairyOnlineForm['anyDateCheck']));
}

$doctors = GetDoctors();    // получаем докторов
$speciality = GetSpesiality();// получаем специальность
function Query($QueryText = null){
    if(nonull($QueryText))
    {
            $connection = new Connection();
            $connection->EstablishDBConnection("main",false);
            $trans = ibase_trans(IBASE_DEFAULT, $connection->connection);
            $query = ibase_prepare($trans, $QueryText);
            $err=ibase_errmsg();
            if(nonull($err))
            {
                return $err;
            }
            else if($err == "")
            {
                $result = ibase_execute($query);
                $obj1 = null;
                while ($obj = ibase_fetch_object($result))
         		{
                    $obj1[] = $obj;
                 //echo "<pre>";
                //print_r($obj);
                //echo "</pre>";
         		}
                    return $obj1;

                ibase_free_result($result);
            }
            else
            {
                ibase_execute($query);
            }
                    ibase_free_query($query);
            ibase_commit($trans);
            $connection->CloseDBConnection();
    }
}

// TODO: Доделать валидацию данных

    function GetDoctors(){  // Doctor list получаем докторов которые имеют график  // DONE
        $Today = date('d.m.Y');
        $QueryText = "SELECT doctors.id, doctors.shortname, doctors.speciality
              FROM doctors, operatingschedule
              WHERE operatingschedule.iscurrent = 1
              AND operatingschedule.doctorid = doctors.id
              AND operatingschedule.enddate >= '$Today'
              AND doctors.isfired = 0";
        $result = Query($QueryText);

       return $result;
    }
	
	// get speciality for doctors. return array
function GetSpesiality(){
	$QueryText = "SELECT item_label FROM TF_dictionaries";
        $result = Query($QueryText);
		$arr_specs=array();
		foreach ($result as $resdata){
		$arr_specs[]=$resdata->ITEM_LABEL;
		}
       return  $arr_specs;
}


    function SetFreeTime($date = null, $startTime = null, $endTime = null, $doctorId = null, $periodMin = null)
    {
        if($date != null && $startTime != null && $endTime != null) {
            $QueryText = "SELECT id, taskdate,beginoftheinterval, endoftheinterval, doctorid, roomid, workplaceid
                  FROM tasks
                  WHERE taskdate = '$date' 
                  AND doctorid = '$doctorId'";
            $busyTask = Query($QueryText);  // получаем занятое время
            $arrTimePeriod = GetArrTime($date, $startTime, $endTime, $periodMin);  // получаем массив времени поделеный по периодам

            if (nonull($busyTask) == true) {  // если у доктора нет записей в этот день
                foreach ($arrTimePeriod as $scheduleTime) { // перебираем весь массив периодов и оставляем только актуальное время
                    foreach ($busyTask as $busyTime) { // перебираем массив и берем от даты и время начала и конца уже занятого времени
                        $dateEdit = new DateTime($busyTime->TASKDATE . ' ' . $busyTime->BEGINOFTHEINTERVAL . ' -' . $periodMin . ' minutes'); // корегтируем время стартового периода занятого времени
                        $beginTime = $dateEdit->format('d-m-Y H:i:s');
                        if (strtotime($scheduleTime) > strtotime($beginTime) && strtotime($scheduleTime) < strtotime($busyTime->TASKDATE . ' ' . $busyTime->ENDOFTHEINTERVAL)
                            || strtotime($date) == strtotime(date('Y-m-d')) && strtotime($scheduleTime) < strtotime(date('H:i:s'))) {    // если дата совпадвет с сегодняшней тогда выбераем на удаление все время до актуального
                            $deletPeriods[] = $scheduleTime;  // записываем даты которые нужно удалить из массива $arrTimePeriod.
                            $deletElement = array_search($scheduleTime, $arrTimePeriod);
                            unset($arrTimePeriod[$deletElement]);
                            break;
                        }
                    }
                }
                return $arrTimePeriod;
            }else{
                return $arrTimePeriod;
            }
            }
    }

    function GetArrTime ($date = null, $startTime = null, $endTime = null, $periodMin = null)  // private function
    {  // DONE
        if($date != null && $startTime != null && $endTime != null){
            $date = date('d-m-Y',strtotime($date));  // format date

            $FreeArrTime [] = $date." ".$startTime;
            $arrPop = $startTime;
            while ($arrPop < $endTime){
                $dateTime = new DateTime($date . " " . $arrPop);
                $dateTime->add(new DateInterval("PT" . $periodMin . "M"));
                $FreeArrTime[] = $dateTime->format('d-m-Y H:i:s');
                $addTimeX[] = $dateTime->format('H:i:s');
                $arrPop = array_pop($addTimeX);
            }
            array_pop($FreeArrTime);  // удаляем последний елемент, конец графика
            return $FreeArrTime;
        }
    }

    function GetDoctorSchedule ($doctorId = null, $periodDays = '60')  // TODO: если в графике два разных кабинета
    {
        $startDate = date('Y-m-d');
        $endDate = new DateTime($startDate);
        $endDate->add(new DateInterval('P'.$periodDays.'D'));
        $c = new ScheduleReportsModule();
        $daySchedule = $c-> createGraphic("odt" , $doctorId, $startDate, $endDate->format('Y-m-d'));

        return $daySchedule;
    }

    function GetArrayDisableDate ($doctorId = null, $periodDays = null)
    {
        if($doctorId != null && $doctorId != 'null') {
            $getSchedule = GetDoctorSchedule($doctorId);
            for ($i = 0; $i <= $periodDays; $i++) {   // получаем массив периода дат
                $date = new DateTime();
                $date->add(new DateInterval('P' . $i . 'D'));
                $periodDate [] = $date->format('d.m.Y');
            }
            foreach ($getSchedule->daySchedule as $keyWorkDay => $valueWorkDay) {  // перебираем массив робочих дат
                $workDay = date('d.m.Y', strtotime($valueWorkDay->day));
                $deleteElement = array_search($workDay, $periodDate);
                unset($periodDate[$deleteElement]);
            }
            $arrDatesDisable = "";
            foreach ($periodDate as $disableDay) {
                $arrDatesDisable .= "'" . $disableDay . "', ";
            }
            $result = "$('#sandbox-container input').datepicker({
                language: 'uk',
                todayHighlight: true,
                autoclose: true,
                datesDisabled : [".$arrDatesDisable."],
                startDate: '".date('d/m/Y')."',
                endDate: '".date('d/m/Y', strtotime('+'.$periodDays.' day'))."'});";

            return $result;
        }else{
            return 'null';
        }
    }

    function GetFreeTime ($recordDate = null, $doctorId = null, $periodMin = null)  // получаем свободное время роботы доктора
    {
        if($doctorId != null && $doctorId != 'null') {
            $date = $recordDate;
            $doctorSchedule = GetDoctorSchedule($doctorId);
            $allScheduleRules = [];
            foreach ($doctorSchedule->daySchedule as $scheduleDays) { //  перебираем масив правил
                if (strtotime($scheduleDays->day) == strtotime($date)) {  // если выбраная дата совпадает с правилом
                    foreach ($scheduleDays->timeIntervals as $scheduleRule) {  // перебираем все правила для выбраной даты
                        $getResultScheduleTime = SetFreeTime($date, $scheduleRule->startTime, $scheduleRule->endTime, $doctorId, $periodMin);  // основываясь на времени работы правила собираем масив времени
                        foreach ($getResultScheduleTime as $timeResult) {  // если получаем больше одного массива, перебираем каждый масив времени
                            if (!in_array($timeResult, $allScheduleRules)) {  //  и сравниваем для исключения задвоения
                                $allScheduleRules[] = $timeResult;
                            }
                        }
                    }
                }
            }
            $result = '';
            foreach ($allScheduleRules as $valueTime) {
                $result .= "<option value='" . date('H:i:s', strtotime($valueTime)) . "'>" . date('H:i', strtotime($valueTime)) . "</option>";
            }
                return $result;
        }else{
            return 'null';
        }
    }

    function InsertPatient($post = null, $timePeriod = null, $secret = null)
    {
        if (!isset($post['g_recaptcha_response']))
        {
            return "undefined recaptcha response";
        }else {
            $reCaptchaResponse = GooReCaptcha($post['g_recaptcha_response'],$secret);
            if ($reCaptchaResponse) {
                if ($post != null && $post['Run'] == true) {
                    $lastName = $post['lastName'];
                    $firstName = $post['firstName'];
                    $middleName = $post['middleName'];
                    $birthday = $post['birthday'];
                    $fullName = $middleName . " " . $firstName . " " . $lastName;
                    $shortName = $lastName . " " . mb_substr($firstName, 0, 1, "UTF-8") . ". " . mb_substr($middleName, 0, 1, "UTF-8") . ".";
                    $phoneNumber = "38" . $post['phoneNumber'];   // добавляем к телефону код страны
            
                    $doctorId = $post['SelectDoctor'];
                    $taskDate = $post['SelectDate'];
                    $time = $post['SelectTime'];

                    $timePlusPeriod = strtotime($taskDate . " " . $time . " +" . $timePeriod . " minutes");

                    $QueryTestTask = "SELECT * FROM tasks WHERE taskdate='$taskDate' AND doctorid='$doctorId'";
                    $getTestTask = Query($QueryTestTask);
                    if ($getTestTask != NULL) {
                        foreach ($getTestTask as $valueTask)  // проверяем свободно ли время для записи
                        {
                            if ((strtotime($taskDate . " " . $time) < strtotime($taskDate . " " . $valueTask->BEGINOFTHEINTERVAL) || strtotime($taskDate . " " . $time) >= strtotime($taskDate . " " . $valueTask->ENDOFTHEINTERVAL))
                                && (strtotime($taskDate . " " . $timePlusPeriod) <= strtotime($taskDate . " " . $valueTask->BEGINOFTHEINTERVAL) || strtotime($taskDate . " " . $timePlusPeriod) > strtotime($taskDate . " " . $valueTask->ENDOFTHEINTERVAL))) {
                                continue;
                            } else {
                                return ModuleWindow("Пожалуйста, выберите другое время.<br> Это время уже занято.", "error");
                            }
                        }
                    } // end if
                    // ??? connection to database;
                    $connection = new Connection();
                    $connection->EstablishDBConnection();
                    //
                    // если время свободно и пациент уже существует в базе (проверяем по номеру телефона)
                    $QueryPatientsMobile = "SELECT patients.* FROM patients, patientsmobile WHERE patients.id = patientsmobile.patient_fk AND patientsmobile.mobilenumber = '$phoneNumber'
                        AND patients.birthday = '$birthday'";
                    $getPatientsCard = Query($QueryPatientsMobile);
                    if ($getPatientsCard != NULL)  // если пациент есть в базе
                    {
                        $getDoctorScheduleRule = GetScheduleRule($doctorId, $taskDate);
                        foreach ($getDoctorScheduleRule->timeIntervals as $oneInterval)  // выбираем интервал приемных часов который будет записан пациент
                        {
                            if (strtotime($taskDate . " " . $time) >= strtotime($taskDate . " " . $oneInterval->startTime)
                                && strtotime($taskDate . " " . $time) < strtotime($taskDate . " " . $oneInterval->endTime)) {
                                $roomID = $oneInterval->cabinetID;  // записуем id кабинета в котором работает доктор в этом интервале
                            }
                        }
                        $taskType = NameComparison($post, $getPatientsCard); // получаем тип записи на прием
                        $patientId = $getPatientsCard[0]->ID;
                        $workPlaceId = GetRandomWorkplaces($roomID);
                        $endInterval = date('H:i:s', $timePlusPeriod);
                        $QueryInsertTask =
                            "INSERT INTO TASKS(TASKDATE, BEGINOFTHEINTERVAL, ENDOFTHEINTERVAL, PATIENTID, DOCTORID, ROOMID, WORKPLACEID, NOTICED, TASKTYPE, SMSSTATUS)
                              VALUES ('" . $taskDate . "', '" . $time . "', '" . $endInterval . "', '" . $patientId . "', '" . $doctorId . "', '" . $roomID . "', '" . $workPlaceId . "', '0', '".$taskType."', '1')";
                        $responseInsertTask = ibase_query($QueryInsertTask);
                        if ($responseInsertTask == 1) {
                            ibase_close();
                            return ModuleWindow("Дякуємо, Ви записані на прийом!", "success");
                        } else {
                            return "Error: record was not insert!";
                        }
                    } else {
                        // get next id
                        $QueryIdNewPatient = "SELECT MAX(ID)+1 AS ID FROM patients";
                        $responseIdNewPatient = Query($QueryIdNewPatient);
                        $patientId = $responseIdNewPatient[0]->ID;
                        // insert new card patient
                        $QueryInsertNewPatient = "INSERT INTO PATIENTS(ID, CARDNUMBER, FIRSTNAME, MIDDLENAME, LASTNAME, FULLNAME, SHORTNAME, BIRTHDAY, MOBILENUMBER, PRIMARYFLAG, SUBDIVISIONID)                            VALUES ('$patientId', NULL, '$firstName', '$middleName', '$lastName', '$fullName', '$shortName', '$birthday', '$phoneNumber', '0', '1')";
                        $QueryInsertNewPatient = ibase_query($QueryInsertNewPatient);
                        // end insert new card patient
                        if ($QueryInsertNewPatient == 1) {

                            // insert new patient
                            $getDoctorScheduleRule = GetScheduleRule($doctorId, $taskDate);
                            foreach ($getDoctorScheduleRule->timeIntervals as $oneInterval)  // выбираем интервал приемных часов в который будет записан пациент
                            {
                                if (strtotime($taskDate . " " . $time) >= strtotime($taskDate . " " . $oneInterval->startTime)
                                    && strtotime($taskDate . " " . $time) < strtotime($taskDate . " " . $oneInterval->endTime)) {
                                    $roomID = $oneInterval->cabinetID;  // записуем id кабинета в котором работает доктор в этом интервале
                                }
                            }
                            $workPlaceId = GetRandomWorkplaces($roomID);
                            $endInterval = date('H:i:s', $timePlusPeriod);
                            $QueryInsertTask =
                                "INSERT INTO TASKS(TASKDATE, BEGINOFTHEINTERVAL, ENDOFTHEINTERVAL, PATIENTID, DOCTORID, ROOMID, WORKPLACEID, NOTICED, TASKTYPE, SMSSTATUS) 
                                  VALUES ('" . $taskDate . "', '" . $time . "', '" . $endInterval . "', '" . $patientId . "', '" . $doctorId . "', '" . $roomID . "', '" . $workPlaceId . "', '0', '2', '1')";
                            $responseInsertTask = ibase_query($QueryInsertTask);
                            // end insert new patient
                            // insert mobile number of the new patient
                            if ($responseInsertTask == 1) {
                                $QueryInsertPatientMobNumb = "INSERT INTO PATIENTSMOBILE(PATIENT_FK, MOBILENUMBER, SENDERACTIVE) VALUES('$patientId', '$phoneNumber', '1')";
                                $responseInsertPatientMobNumb = ibase_query($QueryInsertPatientMobNumb);
                                ibase_close();
                                return ModuleWindow("Дякуємо, Ви записані на прийом!", 'success');

                            } else {
                                return "Error: record was not insert!";
                            }
                            //end insert mobile number of the new patient
                        }
                    }
                } else {
                    return "Oops! Something went wrong!";
                }
            }else{
                return $reCaptchaResponse;
            }
        } // end else
    }

    function GetScheduleRule ($doctorId = null, $Date = null)  // находим правило для доктора по дате
    {
        if ($doctorId != null && $Date != null) {
            $c = new ScheduleReportsModule();
            $daySchedule = $c->createGraphic("odt", $doctorId, $Date, $Date);

            return $daySchedule->daySchedule[0];
        }else{
            echo "Error: doctorId or startDate is NULL in GetScheduleRule";
        }
    }

    function GetRandomWorkplaces ($roomId = null)
    {
        if ($roomId != null)
        {
            $QueryText = "SELECT id, roomid, description FROM workplaces WHERE roomid = '$roomId'";
            $allWorkplaces = Query($QueryText);
            foreach ($allWorkplaces as $workplace)
            {
                $arrayWorkPlaceId [] = $workplace->ID;
            }
            $randomWorkPlace = array_rand($arrayWorkPlaceId,1);
            return $arrayWorkPlaceId[$randomWorkPlace];
        }else{
            return "Error: roomId is NULL in GetRandomWorkplaces";
        }
    }

    function GooReCaptcha ($captchaCode = null, $secret = null)
    {
        if ($captchaCode != null)
        {
            // Получаем HTTP от recaptcha
            $recaptcha = $captchaCode;
            // $secret - СЕКРЕТНЫЙ КЛЮЧ, который нам присвоил гугл
            // Формируем utl адрес для запроса на сервер гугла
            $url = "//www.google.com/recaptcha/api/siteverify?secret=".$secret ."&response=".$recaptcha;

            // Инициализация и настройка запроса
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
            // Выполняем запрос и получается ответ от сервера гугл
            $curlData = curl_exec($curl);

            curl_close($curl);
            // Ответ приходит в виде json строки, декодируем ее
            $curlData = json_decode($curlData, true);

            // Смотрим на результат
            if($curlData['success']) {
                // Сюда попадем если капча пройдена, дальше выполняем обычные
                return true;
            } else {
                // Капча не пройдена, сообщаем пользователю, все закрываем стираем и так далее
                return "Не удача отправки каптчи!";
            }
        }else{
            return $captchaCode;
        }
    }

    function ModuleWindow($massage = null, $massageType = null)
    {
        if ($massage != null)
        {
            if ($massageType == 'success')
            {
                return '<p class="massageResponse alert alert-success">'.$massage.'</p>';
            }else if ($massageType == 'error') {
                return '<p class="massageResponse alert alert-danger">'.$massage.'</p>';
            }
        }else{
            return "ModuleWindow is null";
        }
    }

    // сравниваем на совпадение< имена с существующими в базе
    function NameComparison ($formData = null, $PatientsCard = null){
        if($formData != null && $PatientsCard != null){
            $FormLastName = mb_strtoupper (trim($formData['lastName']));
            $FormFirstName = mb_strtoupper (trim($formData['firstName']));
            $FormMiddleName = mb_strtoupper (trim($formData['middleName']));
            $BaseLastName = mb_strtoupper (trim($PatientsCard[0]->LASTNAME));
            $BaseFirstName = mb_strtoupper (trim($PatientsCard[0]->FIRSTNAME));
            $BaseMiddleName = mb_strtoupper (trim($PatientsCard[0]->MIDDLENAME));
            // если все имена совпадают тип 2 - "С САЙТА!", если нет тогда тип 4 - "С САЙТА! ФИО не совпадает с введенным, пациентом!"
            if ($FormLastName == $BaseLastName && $FormFirstName == $BaseFirstName && $FormMiddleName == $BaseMiddleName){
                return '2';
            }else{
                return '4';
            }
        }else{
            return "Error in NameComparison";
        }
    }

    function getCheckStatus ($checkStatus){
        if($checkStatus == true){
            return true;
        }else{
            return false;
        }
    }


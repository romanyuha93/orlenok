<?php
require_once "services.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Онлайн запис</title>
    <!--[if IE]-->
    <!--<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>-->
    <!--[endif]-->

    <script type="text/javascript" src="jq/jquery-3.2.1.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!--     bootstrap 3 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!--    end bootstrap 3 -->

    <!--     datapicer css-->
    <link rel="stylesheet" href="bootstrap/datepicker/css/bootstrap-datepicker3.standalone.min.css">
    <link rel="stylesheet" href="bootstrap/datepicker/css/bootstrap-datepicker3.min.css">
    <script type="text/javascript" src="bootstrap/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="bootstrap/datepicker/locales/bootstrap-datepicker.uk.min.js"></script>
    <!--     end datapicer css-->

    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<div class="container" width="300px">
    <div class="row">
       <a href="https://orlenok.org/"> <img class="center-block img-responsive" src="img/orlenok-logo-white.png" width="500"> </a>
        <form method="POST" action="index.php" id="regForm">
            <p>Прізвище <span style="color:#ffffff;">*</span> 
                <input class="form-control" type="text" id="lastname" name="lastname" placeholder="Прізвище пацієнта (Українською мовою)"
                       maxlength="25" required>
            </p>

            <p>Ім'я <span style="color:#ffffff;">*</span>
                <input class="form-control" type="text" id="firstname" name="firstname" placeholder="Ім'я пацієнта (Українською мовою)"
                       maxlength="25" required>
            </p>

            <p>Ім'я по батькові <span style="color:#ffffff;">*</span> 
                <input class="form-control" type="text" id="middlename" name="middlename" placeholder="Ім'я по батькові пацієнта (Українською мовою)"
                       maxlength="25" required>
            </p>

            <p>Моб. телефон <span style="color:#ffffff;">*</span>
                <input class="form-control" type="text" id="phonenumber" name="phonenumber" pattern="[0-9]{10}"
                       placeholder="Наприклад: 0674443322" maxlength="10" required>
            </p>

            <p>День народження паціента <span style="color:#ffffff;">*</span>
                <input class="form-control" type="text" onfocus="(this.type='date')" id="birthday" name="birthday" placeholder="Наприклад: 05.03.1993" max="31-12-9999" required>
            </p>

            <p style="
    display: none;
">E-mail <!--<span style="color:#ffffff;">*</span>-->
                <input class="form-control" id="email" type="email" name="email" placeholder="Адреса електронної пошти"
                       required>
            </p>

            <p>Лікар <span style="color:#ffffff;">*</span>
                <select name="doctor" id="selectDoctor" class="form-control" required>
                    <option value="null" selected> Оберіть лікаря</option>
                  <?php
				  					/*echo '<pre>';
										print_r ($speciality);
											echo '</pre>';*/
											
                    foreach ($doctors as $arr => $doctor) {
					$spec_arr=explode("|", $doctor->SPECIALITY);
					$spec_label=array();
					foreach ($spec_arr as $spec_single){
					$spec_label[]=$speciality[$spec_single+1];
					}
				
                        ?>
                        <option value="<?= $doctor->ID ?>"><?= $doctor->SHORTNAME ?><?php echo '( '. implode(", ", $spec_label).' )'; ?></option>
                        <?php
                    }
                    ?>
                </select></p>
<!--
                <p>Опис роботи <span style="color:#ffffff;">*</span>
                <input class="form-control" id="serviceslist" name="serviceslist"
                       placeholder="Виберіть процедуру">
            </p>

-->           

            <div id="dateAndTime">
                <div id="selectDateDiv">
                    <p>
                        Виберіть дату <span style="color:#ffffff;">*</span>
                    </p>
                    <div class="input-group date" id="sandbox-container">
                        <input disabled type="text" id="selectDate" class="form-control"
                               value="Спочатку оберіть лікаря" >
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
                
            <p>
                <span style="color:#ff0000;background: #ffffff">УВАГА!!!! Вівторок, Середа, Четвер с 9.00 до 13.00 приймаются тільки здорові діти. Для хворих дітей прийом після 13.00.</span>
            </p>

                <br>
                <p>Час <span style="color:#ffffff;">*</span>
                    <select disabled name="time" id="selectTime" class="form-control">
                        <option value="null">Спочатку виберіть дату</option>
                    </select>
                </p>
            </div>

            <div
                 class="g-recaptcha"
                 data-sitekey="<?= $SettingsToothFairyOnlineForm['keyReCaptcha'] ?>"
                 data-callback="correctCaptcha">
            </div>

            <p>
                <button id="button" name="button" class="btn btn-success" value="b1">Записатися на прийом</button>
            </p>
            <p>
                <span style="color:#ffffff;">*</span> - Обов'язкові поля для заповнення
            </p>
        </form>
    </div>
</div>  <!--end container-->
<div id="modal_form"><!-- Module window -->
    <span id="modal_close" class="glyphicon glyphicon-remove-sign"></span> <!-- Кнoпкa зaкрыть -->
</div>  <!--End Module window-->
<div id="overlay"></div><!-- Пoдлoжкa для Module window -->

<script type="text/javascript" src="services.js"></script>
<script type="text/javascript">
    var correctCaptcha = function (response) {
        $("#reCapCode").remove();
        $("button").after('<input type="hidden" id="reCapCode" value="' + response + '">');
    };</script>

</body>
</html>


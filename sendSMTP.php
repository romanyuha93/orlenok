<?php
require_once "SettingsOnlineForm.php";

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'phpmailer/Exception.php';
require 'phpmailer/PHPMailer.php';
require 'phpmailer/SMTP.php';

settingsFormForJS($SettingsToothFairyOnlineForm);
function settingsFormForJS($SettingsToothFairyOnlineForm)
{
    $rawData = file_get_contents('php://input');
    $data = json_decode($rawData);  // декодируем JSON

//    $jsonEncode = json_encode($data);  // кодируем в JSON
//    print_r($data);
//    exit();

    $to = $SettingsToothFairyOnlineForm['emailClinic'];      // Получателя, берем из SettingsOnlineForm.php
    $smtpSettings = $SettingsToothFairyOnlineForm['SMTP'];

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    $mail->CharSet = 'UTF-8';
    try {
        //Server settings
//        $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $smtpSettings['smtpServer'];                       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $smtpSettings['userName'];                 // SMTP username
        $mail->Password = $smtpSettings['userPassword'];                           // SMTP password
        $mail->SMTPSecure = $smtpSettings['smtpSecure'];                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $smtpSettings['smtpPort'];                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('support@vikisoft.kiev.ua', 'ОнлайнФорма');  //Отправитель
        foreach ($to as $clinic_email) {
            $mail->addAddress($clinic_email);     // Получатели
        }
//         $mail->addAddress($to[1], 'Joni English');               // Name is optional

//         $mail->addReplyTo('seocraft@medtehnika.org.ua', 'ОнлайнФорма');  // Кому ответить
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'У Вас новая запись на прием!';
        $mail->Body    = '<h3>К вам на прем хочет записаться:</h3><br><b>'.
            $data->lastName." ".
            $data->firstName." ".
            $data->middleName."<br>".
            "<span style='color: rgb(68, 140, 255);'>Телефон: </span>". $data->phoneNumber ."<br>".
            "<span style='color: rgb(68, 140, 255);'>Email: </span>". $data->E_mail ."<br>".
            "<span style='color: rgb(68, 140, 255);'>К доктору: </span>" . $data->SelectDoctor."</b>";
//        $mail->AltBody = '';// альтернативный текст

        $mail->send();
        echo ModuleWindow("Спасибо, в ближайшее время Вам перезвонят!", 'success');
    } catch (Exception $e) {
        echo ModuleWindow('Ошибка, обратитесь к администратору.', 'error');
//        echo ModuleWindow('Message could not be sent. Mailer Error: <br>'.$mail->ErrorInfo, 'error');
    }
}

function ModuleWindow($massage = null, $massageType = null)
{
    if ($massage != null)
    {
        if ($massageType == 'success')
        {
            return '<p class="massageResponse alert alert-success">'.$massage.'</p>';
        }else if ($massageType == 'error') {
            return '<p class="massageResponse alert alert-danger">'.$massage.'</p>';
        }
    }else{
        return "ModuleWindow is null";
    }
}

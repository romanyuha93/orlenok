$(document).ready(function(){
    $.post( // TODO
        'services.php',
        { getCheckStatus: 'yes'},
        function (data) {
            console.log(data);
        }
    );

    $('#anyDate').click(function () {   // Анимация исчезновения при наждатии галочки "любая дата"
        var checked = $('#anyDate').is(":checked");
        if (checked){
            $("#dateAndTime").fadeToggle(500);
        }else if (checked === false){
            $("#dateAndTime").fadeToggle(500);
        }
    });

    $('#selectDoctor').change(function() {  // подгружаем селект дат
        var id = $('#selectDoctor').val();
        if (id !== 'null'){
            $(".error_span").remove();  // удаляем ошибку валидации при выборе
            $("#selectDoctor").css("border-color","#ccc");
        }
        $.post(
            'services.php',
            { doctorId: id },
            function(data) {
                if(data !== 'null')
                {
                    $("#selectDateDiv").html('<p>Виберіть дату</p>\
                    <div class="input-group date" id="sandbox-container" name="date" >\
                        <input type="text" id="selectDate" class="form-control" value="Тицьніть для выбору дати" name="date" style="border-color: #ccc;">\
                        <div class="input-group-addon">\
                            <span class="glyphicon glyphicon-th"></span>\
                        </div>\
                    </div>').show();

                    $("#selectDate").prop("disabled", false);
                    eval(data);
                }else
                {
                    $("#selectDateDiv").html('<p>Виберіть дату</p>\
                    <div class="input-group date" id="sandbox-container" name="date" >\
                        <input disabled type="text" id="selectDate" class="form-control" value="Спочатку оберіть лікаря" name="date" style="border-color: #ccc;">\
                        <div class="input-group-addon">\
                                <span class="glyphicon glyphicon-th"></span>\
                            </div>\
                    </div>').show();

                    $("#selectDateDiv").prop("disabled", true);
                    $("#selectTime").html('<option value="null">Спочатку виберіть дату</option>');
                    $("#selectTime").prop("disabled", true);
                }
                });
    });

    $('#selectDateDiv, #selectDate').change(function() {  // подгружаем время
        var date = $('#selectDate').val();
        var doctorId = $('#selectDoctor').val();
        $.post(
            'services.php',
            { recordDate: date , docId: doctorId},
            function(time) {
                if (time !== '') {
                    $(".error_span").remove(); // удаляем ошибку валидации при выборе
                    $("#selectTime").html(time);
                    $("#selectTime").prop("disabled", false);
                }else{
                    $("#selectTime").html('<option value="null">Виберіть иншу дату</option>');
                    $("#selectTime").prop("disabled", true);
                }
            });
    });

   $('#button').click(function ( e ) {
        e.preventDefault();
        // console.log('Err!');

        var lastName = $('#lastname').val();
        var firstName = $('#firstname').val();
        var middleName = $('#middlename').val();
        var phoneNumber = $('#phonenumber').val();
        var birthday = $('#birthday').val();
        var E_mail = $('#email').val();
        var SelectDoctor = $('#selectDoctor').val();
        var DoctorName = $('#selectDoctor option:selected').text();
        var SelectDate = $('#selectDate').val();
        var SelectTime = $('#selectTime').val();
        var ReCaptcha2 = $('#reCapCode').val();
        var Run = true;
        var anyDate = $('#anyDate').is(":checked"); // если галочка "любая дата" выбрана то true

       if (lastName.length < 2)
        {
            validate_error('#lastname');
        }else if(firstName.length < 2) {
            validate_error('#firstname');
        }else if(middleName.length < 2) {
            validate_error('#middlename');
        }else if(validate_phone(phoneNumber) === false ) {
            validate_error('#phonenumber');
        }else if(validate_email(E_mail) === false ) {
            validate_error('#email');
        }else if(SelectDoctor === 'null' ) {
            validate_error('#selectDoctor');
        }else if(validate_date(SelectDate) === false && anyDate === false) {
            validate_error('#SelectDate');
        }else if(SelectTime === 'null'  && anyDate === false) {
            validate_error('#selectTime');
        }else if(ReCaptcha2 === undefined ) {
            $(".error_span").remove();
            $('.g-recaptcha').after("<span class='error_span' style='color: red;' >Тицьніть «Я не робот»</span>");
        }else {
            // console.log (ReCaptcha2);
            // grecaptcha.reset();
            $('input').css("border-color","#ccc");
            $('select').css("border-color","#ccc");
            $(".error_span").remove();

           var postFileService = 'services.php';

            if(anyDate === true){       // отправка на эмайл
                postFileService = 'sendSMTP.php';

                var jsonData = {
                    lastName: lastName,
                    firstName: firstName,
                    middleName: middleName,
                    phoneNumber: phoneNumber,
                    birthday: birthday,
                    E_mail: E_mail,
                    SelectDoctor: DoctorName,
                    SelectDate: SelectDate,
                    SelectTime: SelectTime,
                    g_recaptcha_response: ReCaptcha2,
                    Run: Run
                };

                var encodeJsonData = JSON.stringify(jsonData);

                $.post
                (

                        postFileService,
                        encodeJsonData,
                        // 'json',

                    function (data) {
                        $(".massageResponse").remove();
                        $('#modal_close').after("<p class='massageResponse'>" + data + "</p>");
                        eval("$('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку\n" +
                            "\t\t \tfunction(){ // пoсле выпoлнения предъидущей aнимaции\n" +
                            "\t\t\t\t$('#modal_form') \n" +
                            "\t\t\t\t\t.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;\n" +
                            "\t\t\t\t\t.animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз\n" +
                            "\t\t});");
                    }

                );

                // обнуляем введеные данные формы
                grecaptcha.reset();
                $("#reCapCode").remove();
                document.getElementById("regForm").reset();
                $("#selectDateDiv").html('<p>Виберіть дату</p>\
                    <div class="input-group date" id="sandbox-container" name="date" >\
                        <input disabled type="text" id="selectDate" class="form-control" value="Спочатку оберіть лікаря" name="date" style="border-color: #ccc;">\
                        <div class="input-group-addon">\
                       <span class="glyphicon glyphicon-th"></span>\
                   </div>\
                    </div>').show();
                $("#selectDateDiv").prop("disabled", true);
                $("#selectTime").html('<option value="null">Спочатку виберіть дату</option>');
                $("#selectTime").prop("disabled", true);

            }else {     // запись в зубную фею

                $.post
                (
                    postFileService,
                    {
                        lastName: lastName,
                        firstName: firstName,
                        middleName: middleName,
                        birthday: birthday,
                        phoneNumber: phoneNumber,
                        E_mail: E_mail,
                        SelectDoctor: SelectDoctor,
                        SelectDate: SelectDate,
                        SelectTime: SelectTime,
                        g_recaptcha_response: ReCaptcha2,
                        Run: Run
                    },
                    function (data) {  // открытие модального окна
                        $(".massageResponse").remove();
                        $('#modal_close').after("<p class='massageResponse'>" + data + "</p>");
                        eval("$('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку\n" +
                            "\t\t \tfunction(){ // пoсле выпoлнения предъидущей aнимaции\n" +
                            "\t\t\t\t$('#modal_form') \n" +
                            "\t\t\t\t\t.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;\n" +
                            "\t\t\t\t\t.animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз\n" +
                            "\t\t});");
                    }
                );

                // обнуляем введеные данные формы
                grecaptcha.reset();
                $("#reCapCode").remove();
                document.getElementById("regForm").reset();
                $("#selectDateDiv").html('<p>Виберіть дату</p>\
                    <div class="input-group date" id="sandbox-container" name="date" >\
                        <input disabled type="text" id="selectDate" class="form-control" value="Спочатку оберіть лікаря" name="date" style="border-color: #ccc;">\
                        <div class="input-group-addon">\
                       <span class="glyphicon glyphicon-th"></span>\
                   </div>\
                    </div>').show();
                $("#selectDateDiv").prop("disabled", true);
                $("#selectTime").html('<option value="null">Спочатку виберіть дату</option>');
                $("#selectTime").prop("disabled", true);
            }

        }

        function validate_error (idElement)  {
            $('.form-control').css("border-color","#ccc");
            $(".error_span").remove();
            switch (idElement) {
                case '#phonenumber':
                    $(idElement).focus();
                    $(idElement).after("<span class='error_span' style='color: red;' >Неправильний формат телефону</span>");
                    $(idElement).css("border-color","red");
                    break;
                case '#email':
                    $(idElement).focus();
                    $(idElement).after("<span class='error_span' style='color: red;' >Неправильний формат пошти</span>");
                    $(idElement).css("border-color","red");
                    break;
                case '#selectDoctor':
                    $(idElement).after("<span class='error_span' style='color: red;' >Оберіть лікаря</span>");
                    $(idElement).css("border-color","red");
                    break;
                case '#SelectDate':
                    $('#sandbox-container').after("<span class='error_span' style='color: red;' >Виберіть иншу дату</span>");
                    $(idElement).css("border-color","red");
                    break;
                case '#selectTime':
                    $(idElement).after("<span class='error_span' style='color: red;' >Виберіть инший час</span>");
                    break;
                default:
                    $(idElement).focus();
                    $(idElement).after("<span class='error_span' style='color: red;' >Вкажіть дані!</span>");
                    $(idElement).css("border-color","red");
                    break;
            }
        }

        function validate_phone (idElement) {
            var paternPhone = /^\d{10}$/;
            return paternPhone.test(idElement);
        }

        function validate_email (idElement) {
           if(idElement !== '') {
               var paternEmail = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/;
               return paternEmail.test(idElement);
           }else{
               return true;
           }
        }

        function validate_date (idElement) {
            var paternDate = /([0-2]\d|3[01])\.(0\d|1[012])\.(\d{4})/;
            return paternDate.test(idElement);
        }

    });

    /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
    $('#modal_close, #overlay').click( function(){ // лoвим клик пo крестику или пoдлoжке
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
                function(){ // пoсле aнимaции
                    $(this).css('display', 'none'); // делaем ему display: none;
                    $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                }
            );
    });


























});